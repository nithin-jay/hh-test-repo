{% comment %}

Widget - Hero Slider

{% endcomment %}

{% comment %} Set Headline BG Color {% endcomment %}
{% set_var headlineBgColor="" %}
{% if model.config.inputHeadlineColor %}
  {% if model.config.inputHeadlineColor == "White" %}
    {% set_var headlineBgColor="white" %}
  {% endif %}
  {% if model.config.inputHeadlineColor == "Yellow" %}
    {% set_var headlineBgColor="#ffd100" %}
  {% endif %}
{% else %}
  {% set_var headlineBgColor="white" %}
{% endif %}
{% if model.config.colorHeadlineCustomCheck == true %}
  {% set_var headlineBgColor=model.config.colorHeadlineCustom %}
{% endif %}

{% comment %} Set BG Color {% endcomment %}
{% set_var bgColor="" %}
{% if model.config.inputBackgroundColor %}
  {% if model.config.inputBackgroundColor == "White" %}
    {% set_var bgColor="white" %}
  {% endif %}
  {% if model.config.inputBackgroundColor == "Black" %}
    {% set_var bgColor="#333132" %}
  {% endif %}
  {% if model.config.inputBackgroundColor == "Red" %}
    {% set_var bgColor="rgba(228, 0, 43, 1)" %}
  {% endif %}
{% else %}
  {% set_var bgColor="rgba(228, 0, 43, 1)" %}
{% endif %}
{% if model.config.colorBackgroundCustomCheck == true %}
  {% set_var bgColor=model.config.colorBackgroundCustom %}
{% endif %}

{% comment %} Set BG Color {% endcomment %}
{% set_var bgColor2="" %}
{% if model.config.inputBackgroundColor2 %}
  {% if model.config.inputBackgroundColor2 == "White" %}
    {% set_var bgColor2="white" %}
  {% endif %}
  {% if model.config.inputBackgroundColor2 == "Black" %}
    {% set_var bgColor2="#333132" %}
  {% endif %}
  {% if model.config.inputBackgroundColor2 == "Red" %}
    {% set_var bgColor2="rgba(228, 0, 43, 1)" %}
  {% endif %}
{% else %}
  {% set_var bgColor2="rgba(228, 0, 43, 1)" %}
{% endif %}
{% if model.config.colorBackgroundCustomCheck2 == true %}
  {% set_var bgColor2=model.config.colorBackgroundCustom2 %}
{% endif %}

{% comment %} Set BG Color {% endcomment %}
{% set_var bgColor3="" %}
{% if model.config.inputBackgroundColor3 %}
  {% if model.config.inputBackgroundColor3 == "White" %}
    {% set_var bgColor3="white" %}
  {% endif %}
  {% if model.config.inputBackgroundColor3 == "Black" %}
    {% set_var bgColor3="#333132" %}
  {% endif %}
  {% if model.config.inputBackgroundColor3 == "Red" %}
    {% set_var bgColor3="rgba(228, 0, 43, 1)" %}
  {% endif %}
{% else %}
  {% set_var bgColor3="rgba(228, 0, 43, 1)" %}
{% endif %}
{% if model.config.colorBackgroundCustomCheck3 == true %}
  {% set_var bgColor3=model.config.colorBackgroundCustom3 %}
{% endif %}

<section class="widget-hero container-fluid">
  <div class="container slider-navigation-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
          <div class="slider-navigation" {% if headlineBgColor %} style="background-color:{{ headlineBgColor }}" {% endif %}>
              <h1>{{ model.config.textH1| truncatechars(17) }}</h1>
              {% comment %} show navigation if greater than 1 {% endcomment %}
              {% if model.config.inputLayout == '2' or model.config.inputLayout == '3' %}
              <div class="slider-navigation--controls">
                  <i class="fas fa-arrow-left"></i> <span class="slider-paging"></span> <i class="fas fa-arrow-right"></i><i class="slider-autoplay fa fa-pause" aria-hidden="true"></i>
              </div>
              {% endif %}
          </div>
      </div>
    </div>
  </div>
  <div class="hero-slider">
    <div class="Sirv hero-slide hero-slide-1" {% if model.config.imageBackground.imageUrl|split(".mozu")|first|split("-")|last == "sb" or model.config.imageBackground.imageUrl|split(".mozu")|first|split("-")|last == "tp1" or model.config.imageBackground.imageUrl|split(".mozu")|first|split("-")|last == "tp2" %} 
    data-bg-src="{{ themeSettings.sirvImageUrl }}{{ model.config.imageBackground.imageUrl|split("files/")|last }}" 
    {% else %} data-bg-src="{{model.config.imageBackground.imageUrl}}" 
    {% endif %}>
      <a class="{% if model.config.textLink == blank %} no-anchor{% endif %} {% if model.config.promoID %}ign-ga-promo-data{% endif %} {% if model.config.promoName %} ign-banner-datalayer-promotion {% endif %}" href="{{ model.config.textLink }}" {% if model.config.PromotionDate %}data-promo-datalayer-creative="{{model.config.PromotionDate}}"{% endif %} {% if model.config.promoID %}data-promo-id="{{model.config.promoID}}"{% endif %}{% if model.config.promoName %}data-promo-name="{{model.config.promoName}}"{% endif %}data-promo-creative="{{ model.config.textLink }}" data-promo-position="1">
          <div class="container">
              <div class="row">
                  <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
                      <div class="hero-card" {% if bgColor %} style="background-color:{{ bgColor }}" {% endif %}>

                          <div class="row">
                              {% if model.config.imageLogo %}
                                <div class="col-xs-3">
                                      <img {% if model.config.imageLogo.imageUrl|split(".mozu")|first|split("-")|last == "sb" or model.config.imageLogo.imageUrl|split(".mozu")|first|split("-")|last == "tp1" or model.config.imageLogo.imageUrl|split(".mozu")|first|split("-")|last == "tp2" or model.config.imageLogo.imageUrl|split(".mozu")|first|split("-")|last == "stg1" %} data-src="{{ themeSettings.sirvImageUrl }}{{ model.config.imageLogo.imageUrl|split("files/")|last }}" {% else %} data-src="{{ model.config.imageLogo.imageUrl }}" {% endif %} alt="{{ model.config.imageLogo.altText }}" class="Sirv hero-card--icon"/>
                                </div>
                              {% endif %}
                              <div class="{% if model.config.imageLogo %}col-xs-9{% else %}col-xs-12{% endif %}">
                                  {% if model.config.imageLogoCopy %}
                                    <img {% if model.config.imageLogoCopy.imageUrl|split(".mozu")|first|split("-")|last == "sb" or model.config.imageLogoCopy.imageUrl|split(".mozu")|first|split("-")|last == "tp1" or model.config.imageLogoCopy.imageUrl|split(".mozu")|first|split("-")|last == "tp2" or model.config.imageLogoCopy.imageUrl|split(".mozu")|first|split("-")|last == "stg1" %} src="{{ themeSettings.sirvImageUrl }}{{ model.config.imageLogoCopy.imageUrl|split("files/")|last }}" {% else %} data-src="{{ model.config.imageLogoCopy.imageUrl }}" {% endif %} alt="{{ model.config.imageLogoCopy.altText }}" class="Sirv hero-card--icon2 "/>
                                  {% endif %}
                                  {% if model.config.textTitle %}
                                    <p class="hero-card--title{% if model.config.fontWeightTitle %} {{ model.config.fontWeightTitle }}{% endif %}" {% if model.config.textTitleColor and model.config.textTitleColorCheck %} style="color:{{ model.config.textTitleColor }}" {% endif %}>{{ model.config.textTitle }}</p>
                                  {% endif %}
                                  {% if model.config.textSubtitle %}
                                    <p class="hero-card--subtitle{% if model.config.fontWeightSubtitle %} {{ model.config.fontWeightSubtitle }}{% endif %}" {% if model.config.textSubtitleColor and model.config.textSubtitleColorCheck %} style="color:{{ model.config.textSubtitleColor }}" {% endif %}>{{ model.config.textSubtitle }}</p>
                                  {% endif %}
                                  {% if model.config.textCta %}
                                    <span class="btn-secondary--white" {% if model.config.textCtaColor and model.config.textCtaColorCheck %} style="color:{{ model.config.textCtaColor }}; background-color:{{ model.config.textCtaBackground }}; border-color:{{ model.config.textCtaBorder }}" {% endif %}>{{ model.config.textCta }}</span>
                                    {% if model.config.textCtaColor and model.config.textCtaColorCheck %}
                                        <style>.hero-slide-1 .btn-secondary--white:hover:after {background:{{ model.config.textCtaColor }}!important }</style>
                                    {% endif%}
                                  {% endif %}
                                  {% if model.config.textDisclaimer %}
                                    <p class="hero-card--disclaimer" {% if model.config.textDisclaimerColor and model.config.textDisclaimerColorCheck %} style="color:{{ model.config.textDisclaimerColor }}" {% endif %}>{{ model.config.textDisclaimer }}</p>
                                  {% endif %}
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </a>
    {% comment %} Mobile BG Image Slide 1 {% endcomment %}
    {% if model.config.imageBackgroundMobile.imageUrl %}
      <style> 
        @media only screen and (max-width: 767px) {
          .hero-slide-1 {
              background-image: url({{ model.config.imageBackgroundMobile.imageUrl }})!important;
          }
        }
      </style>
    {% endif %}
    </div> <!-- end slide 1-->


    {% comment %} show slide 2 if greater than 1 {% endcomment %}
    {% if model.config.inputLayout == '2' or model.config.inputLayout == '3' %}
    <div class="hero-slide hero-slide-2" {% if model.config.imageBackground2.imageUrl|split(".mozu")|first|split("-")|last == "sb" or model.config.imageBackground2.imageUrl|split(".mozu")|first|split("-")|last == "tp1" or model.config.imageBackground2.imageUrl|split(".mozu")|first|split("-")|last == "tp2" %}style="background-image: url('{{ themeSettings.sirvImageUrl }}{{ model.config.imageBackground2.imageUrl|split("files/")|last }}')" {% else %} style="background-image: url('{{ model.config.imageBackground2.imageUrl }}')" {% endif %}>
      <a class="{% if model.config.textLink2 == blank %} no-anchor{% endif %} {% if model.config.promoID2 %}ign-ga-promo-data{% endif %} {% if model.config.promoName2 %} ign-banner-datalayer-promotion {% endif %}" href="{{ model.config.textLink2 }}" {% if model.config.PromotionDate2 %}data-promo-datalayer-creative="{{model.config.PromotionDate2}}"{% endif %} {% if model.config.promoID2 %}data-promo-id="{{model.config.promoID2}}"{% endif %}{% if model.config.promoName2 %}data-promo-name="{{model.config.promoName2}}"{% endif %}data-promo-creative="{{ model.config.textLink2 }}" data-promo-position="2">
          <div class="container">
              <div class="row">
                  <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
                      <div class="hero-card" {% if bgColor %} style="background-color:{{ bgColor2 }}" {% endif %}>

                          <div class="row">
                              {% if model.config.imageLogo2 %}
                                <div class="col-xs-3">
                                      <img {% if model.config.imageLogo2.imageUrl|split(".mozu")|first|split("-")|last == "sb" or model.config.imageLogo2.imageUrl|split(".mozu")|first|split("-")|last == "tp1" or model.config.imageLogo2.imageUrl|split(".mozu")|first|split("-")|last == "tp2" or model.config.imageLogo.imageUrl|split(".mozu")|first|split("-")|last == "stg1" %} data-src="{{ themeSettings.sirvImageUrl }}{{ model.config.imageLogo2.imageUrl|split("files/")|last }}" {% else %} data-src="{{ model.config.imageLogo2.imageUrl }}" {% endif %} alt="{{ model.config.imageLogo2.altText }}" class="Sirv hero-card--icon" /> />
                                </div>
                              {% endif %}
                              <div class="{% if model.config.imageLogo2 %}col-xs-9{% else %}col-xs-12{% endif %}">
                                  {% if model.config.imageLogoCopy2 %}
                                    <img {% if model.config.imageLogoCopy2.imageUrl|split(".mozu")|first|split("-")|last == "sb" or model.config.imageLogoCopy2.imageUrl|split(".mozu")|first|split("-")|last == "tp1" or model.config.imageLogoCopy2.imageUrl|split(".mozu")|first|split("-")|last == "tp2" or model.config.imageLogo.imageUrl|split(".mozu")|first|split("-")|last == "stg1" %} data-src="{{ themeSettings.sirvImageUrl }}{{ model.config.imageLogoCopy2.imageUrl|split("files/")|last }}" {% else %} data-src="{{ model.config.imageLogoCopy2.imageUrl }}" {% endif %} alt="{{ model.config.imageLogoCopy2.altText }}" class="Sirv hero-card--icon2" />
                                  {% endif %}
                                  {% if model.config.textTitle2 %}
                                    <p class="hero-card--title{% if model.config.fontWeightTitle2 %} {{ model.config.fontWeightTitle2 }}{% endif %}" {% if model.config.textTitleColor2 and model.config.textTitleColorCheck2 %} style="color:{{ model.config.textTitleColor2 }}" {% endif %}>{{ model.config.textTitle2 }}</p>
                                  {% endif %}
                                  {% if model.config.textSubtitle2 %}
                                    <p class="hero-card--subtitle{% if model.config.fontWeightSubtitle2 %} {{ model.config.fontWeightSubtitle2 }}{% endif %}" {% if model.config.textSubtitleColor2 and model.config.textSubtitleColorCheck2 %} style="color:{{ model.config.textSubtitleColor2 }}" {% endif %}>{{ model.config.textSubtitle2 }}</p>
                                  {% endif %}
                                  {% if model.config.textCta2 %}
                                    <span class="btn-secondary--white" {% if model.config.textCtaColor2 and model.config.textCtaColorCheck2 %} style="color:{{ model.config.textCtaColor2 }}; background-color:{{ model.config.textCtaBackground2 }}; border-color:{{ model.config.textCtaBorder2 }}" {% endif %}>{{ model.config.textCta2 }}</span>
                                    {% if model.config.textCtaColor2 and model.config.textCtaColorCheck2 %}
                                        <style>.hero-slide-2 .btn-secondary--white:hover:after {background:{{ model.config.textCtaColor2 }}!important }</style>
                                    {% endif%}
                                  {% endif %}
                                  {% if model.config.textDisclaimer2 %}
                                    <p class="hero-card--disclaimer" {% if model.config.textDisclaimerColor2 and model.config.textDisclaimerColorCheck2 %} style="color:{{ model.config.textDisclaimerColor2 }}" {% endif %}>{{ model.config.textDisclaimer2 }}</p>
                                  {% endif %}
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </a>
      {% comment %} Mobile BG Image Slide 2 {% endcomment %}
    {% if model.config.imageBackground2Mobile.imageUrl %}
      <style> 
        @media only screen and (max-width: 767px) {
          .hero-slide-2 {
              background-image: url({{ model.config.imageBackground2Mobile.imageUrl }})!important;
          }
        }
      </style>
    {% endif %}
    </div>
    {% endif %}
    {% comment %} end slide 2 {% endcomment %}

    {% comment %} show slide 3 if greater than 2 {% endcomment %}
    {% if model.config.inputLayout == '3' %}
    <div class="hero-slide hero-slide-3" {% if model.config.imageBackground3.imageUrl|split(".mozu")|first|split("-")|last == "sb" or model.config.imageBackground3.imageUrl|split(".mozu")|first|split("-")|last == "tp1" or model.config.imageBackground3.imageUrl|split(".mozu")|first|split("-")|last == "tp2" %} style="background-image: url('{{ themeSettings.sirvImageUrl }}{{ model.config.imageBackground3.imageUrl|split("files/")|last }}')" {% else %} style="background-image: url('{{ model.config.imageBackground3.imageUrl }}')" {% endif %} >
      <a class="{% if model.config.textLink3 == blank %} no-anchor{% endif %} {% if model.config.promoID3 %}ign-ga-promo-data{% endif %} {% if model.config.promoName3 %} ign-banner-datalayer-promotion {% endif %}" href="{{ model.config.textLink3 }}" {% if model.config.PromotionDate3 %}data-promo-datalayer-creative="{{model.config.PromotionDate3}}"{% endif %} {% if model.config.promoID3 %}data-promo-id="{{model.config.promoID3}}"{% endif %}{% if model.config.promoName3 %}data-promo-name="{{model.config.promoName3}}"{% endif %}data-promo-creative="{{ model.config.textLink3 }}" data-promo-position="3">
          <div class="container">
              <div class="row">
                  <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
                      <div class="hero-card" {% if bgColor %} style="background-color:{{ bgColor3 }}" {% endif %}>

                          <div class="row">
                              {% if model.config.imageLogo3 %}
                                <div class="col-xs-3">
                                      <img {% if model.config.imageLogoCopy3.imageUrl|split(".mozu")|first|split("-")|last == "sb" or model.config.imageLogoCopy3.imageUrl|split(".mozu")|first|split("-")|last == "tp1" or model.config.imageLogoCopy3.imageUrl|split(".mozu")|first|split("-")|last == "tp2" or model.config.imageLogo.imageUrl|split(".mozu")|first|split("-")|last == "stg1" %} data-src="{{ themeSettings.sirvImageUrl }}{{ model.config.imageLogoCopy3.imageUrl|split("files/")|last }}" {% else %} data-src="{{ model.config.imageLogoCopy3.imageUrl }}" {% endif %} alt="{{ model.config.imageLogo3.altText }}" class="hero-card--icon"/>
                                </div>
                              {% endif %}
                              <div class="{% if model.config.imageLogo3 %}col-xs-9{% else %}col-xs-12{% endif %}">
                                  {% if model.config.imageLogoCopy3 %}
                                    <img {% if model.config.imageLogoCopy3.imageUrl|split(".mozu")|first|split("-")|last == "sb" or model.config.imageLogoCopy3.imageUrl|split(".mozu")|first|split("-")|last == "tp1" or model.config.imageLogoCopy3.imageUrl|split(".mozu")|first|split("-")|last == "tp2" or model.config.imageLogo.imageUrl|split(".mozu")|first|split("-")|last == "stg1" %} data-src="{{ themeSettings.sirvImageUrl }}{{ model.config.imageLogoCopy3.imageUrl|split("files/")|last }}" {% else %} data-src="{{ model.config.imageLogoCopy3.imageUrl }}" {% endif %} alt="{{ model.config.imageLogoCopy3.altText }}" class="hero-card--icon2"/>
                                  {% endif %}
                                  {% if model.config.textTitle3 %}
                                    <p class="hero-card--title{% if model.config.fontWeightTitle3 %} {{ model.config.fontWeightTitle3 }}{% endif %}" {% if model.config.textTitleColor3 and model.config.textTitleColorCheck3 %} style="color:{{ model.config.textTitleColor3 }}" {% endif %}>{{ model.config.textTitle3 }}</p>
                                  {% endif %}
                                  {% if model.config.textSubtitle3 %}
                                    <p class="hero-card--subtitle{% if model.config.fontWeightSubtitle3 %} {{ model.config.fontWeightSubtitle3 }}{% endif %}" {% if model.config.textSubtitleColor3 and model.config.textSubtitleColorCheck3 %} style="color:{{ model.config.textSubtitleColor3 }}" {% endif %}>{{ model.config.textSubtitle3 }}</p>
                                  {% endif %}
                                  {% if model.config.textCta3 %}
                                    <span class="btn-secondary--white" {% if model.config.textCtaColor3 and model.config.textCtaColorCheck3 %} style="color:{{ model.config.textCtaColor3 }}; background-color:{{ model.config.textCtaBackground3 }}; border-color:{{ model.config.textCtaBorder3 }}" {% endif %}>{{ model.config.textCta3 }}</span>
                                    {% if model.config.textCtaColor3 and model.config.textCtaColorCheck3 %}
                                        <style>.hero-slide-3 .btn-secondary--white:hover:after {background:{{ model.config.textCtaColor3 }}!important }</style>
                                    {% endif%}
                                  {% endif %}
                                  {% if model.config.textDisclaimer3 %}
                                    <p class="hero-card--disclaimer" {% if model.config.textDisclaimerColor3 and model.config.textDisclaimerColorCheck3 %} style="color:{{ model.config.textDisclaimerColor3 }}" {% endif %}>{{ model.config.textDisclaimer3 }}</p>
                                  {% endif %}
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </a>
      {% comment %} Mobile BG Image Slide 3 {% endcomment %}
    {% if model.config.imageBackground3Mobile.imageUrl %}
      <style> 
        @media only screen and (max-width: 767px) {
          .hero-slide-3 {
              background-image: url({{ model.config.imageBackground3Mobile.imageUrl }})!important;
          }
        }
      </style>
    {% endif %}
    </div>
    {% endif %}
    {% comment %} end slide 3 {% endcomment %}

  </div>
</section>

{% require_script "widgets/hero-slider" %}
{% require_script "widgets/images-with-responsiveness" %}
