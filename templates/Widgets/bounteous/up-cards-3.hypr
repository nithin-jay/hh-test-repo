{% comment %}

Widget - 3-Up Cards

{% endcomment %}

<section class="widget-three-up container widget-padding">

    {% if model.config.widgetTitle %}
      <div class="widget-title">
        <h2 class="h-title{% if model.config.fontWeight %} {{ model.config.fontWeight }}{% endif %}">{{ model.config.widgetTitle }}</h2>

        {% if model.config.widgetSubtitle %}
          <p class="b1">{{ model.config.widgetSubtitle }}</p>
        {% endif %}
      </div>
    {% endif %}

    <div class="slider-navigation--controls">
        <i class="fas fa-arrow-left"></i> <span class="slider-paging"></span> <i class="fas fa-arrow-right"></i>
    </div>

    <div class="row widget-three-up-slider">
        <div class="col-md-4 three-up-card three-up-card--1">
            <a class="border-shadow{% if model.config.textLink == blank %} no-anchor{% endif %} {% if model.config.promoID %}ign-ga-promo-data{% endif %} {% if model.config.promoName %} ign-banner-datalayer-promotion {% endif %}" href="{{ model.config.textLink }}" {% if model.config.PromotionDate %} data-promo-datalayer-creative="{{ model.config.PromotionDate }}"{% endif %} {% if model.config.promoID %} data-promo-id="{{ model.config.promoID }}"{% endif %}{% if model.config.promoName %} data-promo-name="{{ model.config.promoName }}"{% endif %} data-promo-creative="{{ model.config.textLink }}">
                <img class="Sirv" {% if model.config.imageProduct.imageUrl|split(".mozu")|first|split("-")|last == "sb" or model.config.imageProduct.imageUrl|split(".mozu")|first|split("-")|last == "tp1" or model.config.imageProduct.imageUrl|split(".mozu")|first|split("-")|last == "tp2" or model.config.imageProduct.imageUrl|split(".mozu")|first|split("-")|last == "stg1" %}  data-src="{{ themeSettings.sirvImageUrl }}{{ model.config.imageProduct.imageUrl|split("files/")|last }}"{% else %} data-src="{{ model.config.imageProduct.imageUrl }}"{% endif %} alt="{{ model.config.imageProduct.altText }}"/>
                <div class="three-up-card--copy">
                    {% if model.config.textBadge %}
                      <p class="card--badge" {% if model.config.textBadgeColor and model.config.textBadgeColorCheck %} style="color:{{ model.config.textBadgeColor }}; background-color:{{ model.config.textBadgeBg }};" {% endif %}>{{ model.config.textBadge }}</p>
                    {% else %}
                      <span class="card--badge spacer"></span>
                    {% endif %}

                    {% if model.config.textTitle %}
                      <p class="three-up-card--title{% if model.config.fontWeightTitle %} {{ model.config.fontWeightTitle }}{% endif %}" {% if model.config.textTitleColor and model.config.textTitleColorCheck %} style="color:{{ model.config.textTitleColor }}" {% endif %}>{{ model.config.textTitle }}</p>
                    {% endif %}

                    {% if model.config.textCta %}
                      <p class="btn-secondary" {% if model.config.textCtaColor and model.config.textCtaColorCheck %} style="color:{{ model.config.textCtaColor }}; background-color:{{ model.config.textCtaBackground }}; border-color:{{ model.config.textCtaBorder }}" {% endif %}>{{ model.config.textCta }}{% if model.config.textCtaIcon %} <i class="fas {{model.config.textCtaIcon}}"></i>{% endif %}</p>
                      {% if model.config.textCtaColor and model.config.textCtaColorCheck %}
                        <style>.three-up-card--1 .btn-secondary:hover:after {background:{{ model.config.textCtaColor }}!important }</style>
                      {% endif%}
                    {% endif %}
                </div>
            </a>
        </div>
        <div class="col-md-4 three-up-card three-up-card--2">
            <a class="border-shadow{% if model.config.textLink2 == blank %} no-anchor{% endif %} {% if model.config.promoID2 %}ign-ga-promo-data{% endif %} {% if model.config.promoName2 %} ign-banner-datalayer-promotion {% endif %}" href="{{ model.config.textLink2 }}" {% if model.config.PromotionDate2 %} data-promo-datalayer-creative="{{ model.config.PromotionDate2 }}"{% endif %} {% if model.config.promoID2 %} data-promo-id="{{ model.config.promoID2 }}"{% endif %}{% if model.config.promoName2 %} data-promo-name="{{ model.config.promoName2 }}"{% endif %} data-promo-creative="{{ model.config.textLink2 }}">
                <img class="Sirv" {% if model.config.imageProduct2.imageUrl|split(".mozu")|first|split("-")|last == "sb" or model.config.imageProduct2.imageUrl|split(".mozu")|first|split("-")|last == "tp1" or model.config.imageProduct2.imageUrl|split(".mozu")|first|split("-")|last == "tp2" or model.config.imageProduct2.imageUrl|split(".mozu")|first|split("-")|last == "stg1" %}  data-src="{{ themeSettings.sirvImageUrl }}{{ model.config.imageProduct2.imageUrl|split("files/")|last }}"{% else %} data-src="{{ model.config.imageProduct2.imageUrl }}"{% endif %} alt="{{ model.config.imageProduct2.altText }}"/>
                <div class="three-up-card--copy">
                    {% if model.config.textBadge2 %}
                      <p class="card--badge" {% if model.config.textBadgeColor2 and model.config.textBadgeColorCheck2 %} style="color:{{ model.config.textBadgeColor2 }}; background-color:{{ model.config.textBadgeBg2 }};" {% endif %}>{{ model.config.textBadge2 }}</p>
                    {% else %}
                      <span class="card--badge spacer"></span>
                    {% endif %}

                    {% if model.config.textTitle2 %}
                      <p class="three-up-card--title{% if model.config.fontWeightTitle2 %} {{ model.config.fontWeightTitle2 }}{% endif %}" {% if model.config.textTitleColor2 and model.config.textTitleColorCheck2 %} style="color:{{ model.config.textTitleColor2 }}" {% endif %}>{{ model.config.textTitle2 }}</p>
                    {% endif %}

                    {% if model.config.textCta2 %}
                      <p class="btn-secondary" {% if model.config.textCtaColor2 and model.config.textCtaColorCheck2 %} style="color:{{ model.config.textCtaColor2 }}; background-color:{{ model.config.textCtaBackground2 }}; border-color:{{ model.config.textCtaBorder2 }}" {% endif %}>{{ model.config.textCta2 }}{% if model.config.textCtaIcon2 %} <i class="fas {{model.config.textCtaIcon2}}"></i>{% endif %}</p>
                      {% if model.config.textCtaColor2 and model.config.textCtaColorCheck2 %}
                        <style>.three-up-card--2 .btn-secondary:hover:after {background:{{ model.config.textCtaColor2 }}!important }</style>
                      {% endif%}
                    {% endif %}
                </div>
            </a>
        </div>
        <div class="col-md-4 three-up-card three-up-card--3">
            <a class="border-shadow{% if model.config.textLink3 == blank %} no-anchor{% endif %} {% if model.config.promoID3 %}ign-ga-promo-data{% endif %} {% if model.config.promoName3 %} ign-banner-datalayer-promotion {% endif %}" href="{{ model.config.textLink3 }}" {% if model.config.PromotionDate3 %} data-promo-datalayer-creative="{{ model.config.PromotionDate3 }}"{% endif %} {% if model.config.promoID3 %} data-promo-id="{{ model.config.promoID3 }}"{% endif %}{% if model.config.promoName3 %} data-promo-name="{{ model.config.promoName3 }}"{% endif %} data-promo-creative="{{ model.config.textLink3 }}">
                <img class="Sirv" {% if model.config.imageProduct3.imageUrl|split(".mozu")|first|split("-")|last == "sb" or model.config.imageProduct3.imageUrl|split(".mozu")|first|split("-")|last == "tp1" or model.config.imageProduct3.imageUrl|split(".mozu")|first|split("-")|last == "tp2" or model.config.imageProduct3.imageUrl|split(".mozu")|first|split("-")|last == "stg1" %}  data-src="{{ themeSettings.sirvImageUrl }}{{ model.config.imageProduct3.imageUrl|split("files/")|last }}"{% else %} data-src="{{ model.config.imageProduct3.imageUrl }}"{% endif %} alt="{{ model.config.imageProduct3.altText }}"/>
                <div class="three-up-card--copy">
                    {% if model.config.textBadge3 %}
                      <p class="card--badge" {% if model.config.textBadgeColor3 and model.config.textBadgeColorCheck3 %} style="color:{{ model.config.textBadgeColor3 }}; background-color:{{ model.config.textBadgeBg3 }};" {% endif %}>{{ model.config.textBadge3 }}</p>
                    {% else %}
                      <span class="card--badge spacer"></span>
                    {% endif %}

                    {% if model.config.textTitle3 %}
                      <p class="three-up-card--title{% if model.config.fontWeightTitle3 %} {{ model.config.fontWeightTitle3 }}{% endif %}" {% if model.config.textTitleColor3 and model.config.textTitleColorCheck3 %} style="color:{{ model.config.textTitleColor3 }}" {% endif %}>{{ model.config.textTitle3 }}</p>
                    {% endif %}

                    {% if model.config.textCta3 %}
                      <p class="btn-secondary" {% if model.config.textCtaColor3 and model.config.textCtaColorCheck3 %} style="color:{{ model.config.textCtaColor3 }}; background-color:{{ model.config.textCtaBackground3 }}; border-color:{{ model.config.textCtaBorder3 }}" {% endif %}>{{ model.config.textCta3 }}{% if model.config.textCtaIcon3 %} <i class="fas {{model.config.textCtaIcon3}}"></i>{% endif %}</p>
                      {% if model.config.textCtaColor3 and model.config.textCtaColorCheck3 %}
                        <style>.three-up-card--3 .btn-secondary:hover:after {background:{{ model.config.textCtaColor3 }}!important }</style>
                      {% endif%}
                    {% endif %}
                </div>
            </a>
        </div>
    </div>
</section>

{% require_script "widgets/up-cards-3" %}
{% require_script "widgets/images-with-responsiveness" %}