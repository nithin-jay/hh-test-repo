<div class="mz-orderlisting" data-mz-id="{{ model.parentCheckoutNumber }}">
<!--Item List -->
	<div class="myaccount-order-list-cont">
		<div class="myaccount-top-header">
			<div class="myaccount-orderlist-heading">
				<h4 class="myaccount-order-number">{{ labels.orderNumber }}: {{ model.orderNumber }} </h4>
            	<p class="myaccount-order-status">
					{% if model.status == "Cancelled" or model.status == "CANCELLED"  %}
						<i class="material-icons">close</i>
						<span>{{ labels.orderCancelled }}</span>
					{% endif %}
					{% if model.status == "Completed" %}
						{% if model.orderType == "shipToHomeOrder" %}
							<i class="far fa-shipping-fast"></i>
							{{labels.shipmentDelivered}}
						{% endif %}
						{% if model.orderType == "shipToStoreOrder" %}
							<i class="material-icons">check_circle</i>
							<span>{{ labels.orderCompletePickup }}</span>
						{% endif %}
						{% if model.orderType == "mixOrder" %}
							<i class="material-icons">check_circle</i>
							<span>{{ labels.orderHistoryCompleteLabel }}</span>
						{% endif %}
					{% endif %}
					{% if model.status == "Accepted" or model.status == "Processing" %}
						<i class="far fa-hourglass"></i>
						<span>{{labels.orderHistoryProcessingLabel}}</span>
					{% endif %}
            	</p>
			</div>  	
				
			<div class="myaccount-orderlist-headerdetail">
				<ul>
					<li>
	                    {% if model.submittedDate %}
	                       {{ labels.orderOnDate }} {{ model.submittedDate|date("Y-m-d") }}
	                    {% else %}
	                        {{ labels.orderOnDate }} {{ model.auditInfo.createDate|date("Y-m-d") }}
	                    {% endif %}
	                </li>
	                <li>
	                	<span>{{ labels.itemQuantity }}:</span> 
	                	<br/>
	                	<span class="item-quantity">{{ model.totalItemsQuantity }}</span>
	                </li>
	                <li class="nav nav-pills" role="tablist" aria-orientation="vertical">
						<span class="myaccount-order-total">{{ labels.orderTotal }} : </span>
						<span class="myaccount-order-total">
							{% if model.isFrenchSite %}
								 {{ model.total|currency|replace(",", " ")|replace(".", ",")|replace("$", "") }} {{ labels.frCurrency }}
							{% else %}
								 {{ model.total|currency }}
							{% endif %}
						</span>
	                </li>
	            </ul>
			</div>
		</div>
		<div class="myaccount-footer-list">
			{% if model.orderType == "mixOrder" %}
				<div class="order-info-sub-section col-1g-4 col-md-4 col-sm-12 col-xs-12 flex-grow">
					<i class="far fa-map-marker-alt"></i>
					<div class="sub-section-info address">
						<span>{{labels.shippingAddress}}</span>
						{% include "modules/common/address-summary" with model=model.fulfillmentInfo.fulfillmentContact %}
					</div>
				</div>
				<div class="badge-store-content mixOrder flex-grow">
					<div class="badge-content">
						<span>{{ labels.yourPickupStore }}:</span>
						{% if model.locationData %}
							<p>{{ model.locationData.name}}</p>
						{% endif %}
					</div>
				</div>
			{% else %}
				{% if model.orderType != "shipToHomeOrder" %}
					<div class="badge-store-content ship-to-home-order flex-grow">
						<div class="badge-content">
							<span>{{ labels.yourPickupStore }}:</span>
							{% if model.locationData %}
								<p>{{ model.locationData.name}}</p>
							{% endif %}
						</div>
					</div>
				{% endif %}
				{% if model.orderType == "shipToHomeOrder" %}
					<div class="order-info-sub-section shipToHomeOrder col-1g-4 col-md-4 col-sm-12 col-xs-12 flex-grow">
						<i class="far fa-map-marker-alt"></i>
						<div class="sub-section-info address">
							<span>{{labels.shippingAddress}}</span>
							{% include "modules/common/address-summary" with model=model.fulfillmentInfo.fulfillmentContact %}
						</div>
					</div>
				{% endif %}
			{% endif %}
			<div class="return-link-section">
				<a href="#vieworderdetail-{{ model.id }}" class="btn-text viewOrderDetails myaccount-view-detail-link">{{labels.viewOrderDetails}} <i class="fas fa-arrow-right" aria-hidden="true"></i></a><br>
					{% if themeSettings.displayStartReturnLink and model.validDate and model.status == "Completed" and model.isReturnItemsPresent and model.type == "Online" and model.orderType == "shipToHomeOrder" %}
						<a class="btn-text startReturn" data-mz-start-return-id="{{ model.id }}" data-mz-action="startOrderReturn">{{labels.startReturn}} <i class="fas fa-arrow-right" aria-hidden="true"></i></a>
					{% endif %}
			</div>
		</div>
	</div>
	<!-- End Item List -->
	
</div>


<!-- View Order Detail container Start -->
	<div class="myaccount-vieworderdetail-container hidden" id="vieworderdetail-{{ model.id }}">
		{% include "modules/my-account/my-account-view-order-detail" with model=model isFrenchSite=model.isFrenchSite %}
	</div> 
<!-- View Order Detail container End -->

<div id="cancelOrderModalContainer"></div>
<div id="returnOrderModalContainer"></div>
