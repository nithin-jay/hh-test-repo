{% extends "page" %}

{% block title-tag-content %}{% firstof pageContext.metaTitle model.name %}  - {% parent %}{% endblock title-tag-content %}

{% block body-tag-classes %} static-page commercial-maintenance {% endblock body-tag-classes %}

{% block body-content %}
<script>
	var recaptcha1;
	var myCallBack = function() {		
	    recaptcha1 = grecaptcha.render('g-recaptcha', {
	    'sitekey': '{{ themeSettings.sitekey }}',	    
	    'lang' : {% if apiContext.headers.x-vol-locale == "fr-CA" %}'fr'{% else %}'en'{% endif %}
	    });
	};
</script>
{% if apiContext.headers.x-vol-locale == "fr-CA" %}
<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit&hl=fr"></script>
{% else %}
<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit"></script>
{% endif %}
{% require_script "pages/commercial-maintenance" %} 
<div class="container" >
	<div class="row commertial-maintenance-container">
		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 commercial-maintenance-navigation">
			{% if pageContext.isEditMode %} ****Static navigaton**** {% endif %}
			{% dropzone "commercial-maintenance-navigation-image" scope="page" %}
			{% if pageContext.isEditMode %} ****commercial-maintenance bottom image**** {% endif %}
			{% dropzone "commercial-maintenance-bottom-image" scope="page" %}
		</div>
		<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 commercial-maintenance-content">
			<div class="commercial-maintenance-banner-information">
				{% if pageContext.isEditMode %} **** commercial maintenance banner image **** {% endif %}
				{% dropzone "commercial-maintenance-banner-image" scope="page" %}
				{% if pageContext.isEditMode %} **** commercial maintenance banner information **** {% endif %}
				{% dropzone "commercial-maintenance-banner-information" scope="page" %}
			</div>
			<div id="goto-thankyou"></div>
			<div class="commercial-maintenance-store" id="commercial-maintenance-store">
				<h1>{{ labels.selectCommercialMaintenanceStore }}</h1>
				<p class="store_message">{{ labels.storeFindercommercialMaintenanceStore }}</p>
				<a class="mz-button btn-small commercial-maintenance-store-btn" id="find-home-store" href="/store-locator?storeService=commercial maintenance"> 
					<span>{{ labels.selectCommercialMaintenanceStore }}</span>
					<i class="material-icons">play_arrow</i>
				</a>
				<p class="mz-validationmessage" data-mz-validationmessage-for="commercialMaintenanceStore"></p>
			</div>
			
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 commercial-maintenance-form">
				<form name="commercial-maintenance-form" id="commercial-maintenance-form">
					<h2>{{ labels.contactInformation }}</h2>
					<div class="form-group">
				  	  	<label for="firstName">{{ labels.firstName }} </label>
					  	<input class="mz-commercial-maintenance-firstName form-control" type="text" id="firstName" data-mz-value="firstName" value="" name="firstName">
					  	<span class="mz-validationmessage" data-mz-validationmessage-for="firstName"></span>
					</div>
					<div class="form-group">
				  	  	<label for="lastName">{{ labels.lastName }} </label>
					  	<input class="mz-commercial-maintenance-lastName form-control" type="text" id="lastName" data-mz-value="lastName" value="" name="lastName">
					  	<span class="mz-validationmessage" data-mz-validationmessage-for="lastName"></span>
					</div>
					<div class="form-content preferred-method">{{ labels.preferredMethodForContact }} </div>
					<div class="form-group">
						<fieldset>
 							<legend class="hidden">{{ labels.phoneNumber }}</legend>
				  	  		<label for="phoneNumberRadio"><input type="radio" id="phoneNumberRadio" value="phoneNumber" name="preferredContactMethod"/> {{ labels.phoneNumber }} {{ labels.phoneNumberDemo }}</label>
				  	  		<label class="hidden" for="phoneNumber">{{ labels.phoneNumber }}</label>
					  		<input class="mz-commercial-maintenance-phoneNumber hidden" type="text" name="phoneNumber" placeholder="Phone number" data-mz-validate-phone-number="" id="phoneNumber" data-mz-value="phoneNumber" value=""/>
				        	<div class="phone-numbercontainer">
				        		<label for="number1" class="hidden">{{ labels.phoneNumber }}</label>
						        <input type="number" pattern="\d*" id="number1" class="form-control digit-length"  min="0" max="999" maxlength="3"/>
						        <label for="number2" class="hidden">{{ labels.phoneNumber }}</label>
						        <input type="number" pattern="\d*" id="number2" class="form-control digit-length"  min="0" max="999" maxlength="3"/>
						        <label for="number3" class="hidden">{{ labels.phoneNumber }}</label>
						        <input type="number" pattern="\d*" id="number3" class="form-control digit-length"  min="0" max="9999" maxlength="4"/>
							</div>
				        	<span class="mz-validationmessage" data-mz-validationmessage-for="phoneNumber"></span>
				        </fieldset>
					</div>
					<div class="form-group">
						<fieldset>
 							<legend class="hidden">{{ labels.emailAddress }}</legend>
					  	  	<label for="emailAddressTxt"><input type="radio" name="preferredContactMethod" id="emailAddressTxt" value="emailAddress" checked="checked" /> {{ labels.emailAddress }} </label>
						  	<label class="hidden" for="emailAddress">{{ labels.emailAddress }}</label>
						  	<input class="mz-commercial-maintenance-email form-control" type="email" id="emailAddress" data-mz-value="emailAddress" value="" name="emailAddress">
						  	<span class="mz-validationmessage" data-mz-validationmessage-for="emailAddress"></span>
						</fieldset>
					</div>
					{% if apiContext.headers.x-vol-locale == "fr-CA" %}
						<div class="form-group">
					  	  	<label for="type-of-industry">{{ labels.typeOfIndustry }}</label>
						  	{% with themeSettings.typeOfIndustryFR as typeOfIndustryFR %}
					            <select class="form-control" id="type-of-industry" name="industry">
					                <option value="0">---</option> 
									{% for index in typeOfIndustryFR|split("#") %}
											<option value="{{ index }}">
												{{ index }}
											</option>
									{% endfor %}
					            </select>
				            {% endwith %}
						  	<span class="mz-validationmessage" data-mz-validationmessage-for="typeOfIndustry"></span>
						</div>
					{% else %}
						<div class="form-group">
					  	  	<label for="type-of-industry">{{ labels.typeOfIndustry }}</label>
						  	{% with themeSettings.typeOfIndustry as typeOfIndustry %}
					            <select class="form-control" id="type-of-industry" name="industry">
					                <option value="0">---</option> 
									{% for index in typeOfIndustry|split("#") %}
											<option value="{{ index }}">
												{{ index }}
											</option>
									{% endfor %}
					            </select>
				            {% endwith %}
						  	<span class="mz-validationmessage" data-mz-validationmessage-for="typeOfIndustry"></span>
						</div>
					{% endif %}
					<div class="form-group">
				  	  	<label for="nameOfBusiness">{{ labels.nameOfBusiness }} </label>
					  	<input class="form-control" type="text" id="nameOfBusiness" data-mz-value="nameOfBusiness" value="" name="nameOfBusiness">
					</div>
					<div class="form-group">
				  	  	<label for="commercialMaintenanceOffers"><input type="checkbox" name="commercialMaintenanceOffers" id="commercialMaintenanceOffers" value="commercialMaintenanceOffers" /> {{ labels.CommercialMaintenanceOffers }} </label>
				  	  	<span class="mz-validationmessage" data-mz-validationmessage-for="commercialMaintenanceOffers"></span>
					</div>
					<div class="form-group">
						<div id="g-recaptcha" class="g-recaptcha"></div>
						<span class="mz-validationmessage" data-mz-validationmessage-for="captcha"></span>
					</div>
					<div class="btn-block">
						<button type="button" class="mz-button mz-button-large mz-commercial-maintenance-button" id="commercial-maintenance-button">{{ labels.submit }}</button>
						<p class="error-email">{{ labels.errorSendingEmail }}</p>
					</div>
					<p class="submit-note">{{ labels.commercialMaintenanceSubmitNote }}</p>
				</form>	
			</div>
			<div class="sucess-email-template">
				<div class="thank-you-msg">
					<p class="header">{{ labels.thankYouInquiry }}</p>
					<p class="msg">{{ labels.thankyouMessageCommMaintenance }}</p>
				</div>
				<div class="store-details-box clearfix">
					<p class="agenda-bold">{{ labels.yourCommercialMaintenanceStore }}</p>
					<div class="col-sm-12 store-info">
						<div class="col-sm-6 store-info-details"></div>
						<div class="col-sm-6">
							<p class="sign-up agenda-bold">{{ labels.signUpSave }}</p>
							<p>{{ labels.signupNote }}</p>
							<a href="https://pc.{% if apiContext.headers.x-vol-locale == "fr-CA" %}fr{% else %}en{% endif %}.homehardware.ca/registration.aspx" class="mz-button btn-small signup-btn">{{ labels.signupNow }}</a>
						</div>
					</div>
				</div>
				<div class="button-wrap">
					<a href="/" class="mz-button btn-small shopping-btn">{{ labels.continueShopping  }}</a>
				</div>
				
			</div>
		</div>
	</div>
</div>

{% endblock body-content %}