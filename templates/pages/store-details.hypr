{% extends "page" %}  {% block meta-tags %} 
<meta name="viewport" content="width=device-width,initial-scale=1">
{% with model.properties.address2|add(", ")|add(model.properties.city)|add(", ")|add(model.properties.province.0)|add(", ")|add(model.properties.postalCode) as storeAddress %} 
<meta name="description" content="{{labels.storeMetaDescription|string_format(model.properties.storeName,storeAddress)|safe}}">
<meta property="og:description" content="{{labels.storeMetaDescription|string_format(model.properties.storeName,storeAddress)|safe}}" />
{% endwith %} 
<meta name="keywords" content="{{ PageContext.MetaKeywords }}">
<meta name="correlationId" content="{{ pageContext.correlationId }}">
<!-- Open Graph --> 
<meta property="og:title" content="{{ labels.storeDetails }} - {{ labels.homeHardware }}" />
<meta property="og:url" content="{{pagecontext.url}}" />
<meta property="og:locale" content="{{ apiContext.headers.x-vol-locale }}" />
{% if themeSettings.homeFurnitureSiteId == apiContext.headers.x-vol-site %} 
<meta property="og:site_name" content="{{ labels.homeFurniture }}" />
{% else %} 
<meta property="og:site_name" content="{{ labels.homeHardware }}" />
{% endif %} {% endblock meta-tags %} {% block canonical %} <link rel="canonical" href="{% if themeSettings.homeFurnitureSiteId == apiContext.headers.x-vol-site %}{{ pageContext.secureHost }}{% else %}{{ pageContext.secureHost }}{% endif %}{% if apiContext.headers.x-vol-locale == "fr-CA" %}/fr{% else %}{% if themeSettings.homeFurnitureSiteId != apiContext.headers.x-vol-site %}/en{% endif %}{% endif %}/store/{{PageContext.cmsContext.page.path}}"> <link rel="dns-prefetch" href="{% if themeSettings.homeFurnitureSiteId == apiContext.headers.x-vol-site %}{{ pageContext.secureHost }}{% else %}{{ pageContext.secureHost }}{% endif %}{% if apiContext.headers.x-vol-locale == "fr-CA" %}/fr{% else %}{% if themeSettings.homeFurnitureSiteId != apiContext.headers.x-vol-site %}/en{% endif %}{% endif %}"> {% endblock canonical %} {% block title-tag-content %}{% firstof pageContext.metaTitle %} {% with model.properties.city|add(", ")|add(model.properties.province.0)|add(", ")|add(model.properties.postalCode) as storeAddress %}{{ labels.storeTitle|string_format(model.properties.storeName,storeAddress)|safe }}{% endwith %}{% endblock title-tag-content %}  {% block body-tag-classes %} store-details {% endblock body-tag-classes %}  {% block body-content %}  {% require_script "pages/store-details" %}  {% with model.properties.address1|add(",")|add(model.properties.address2)|add(",")|add(model.properties.city)|add(",")|add(model.properties.province.0) |add(",")|add(model.properties.postalCode) as direction %} 
<div class="store-details-top-section" data-mz-latitude="{{model.properties.latitude}}" data-mz-longitude="{{model.properties.longitude}}" data-mz-store="{{model.name}}">
   <div class="container">
      <div class="row">
         <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
            <h1 class="store-name">{{model.properties.storeName|safe}}</h1>
            <ul class="store-address-details">
               <li class="street-adr"> {% if model.properties.address1 and (model.properties.address1 !="N/A" and model.properties.address1 != "NA") %} {{model.properties.address1}} {% endif %} {% if model.properties.address2 and (model.properties.address2 !="N/A" and model.properties.address2 != "NA") %} {{model.properties.address2}} {% endif %} </li>
               <li> {% if model.properties.city %} <span>{{model.properties.city}}</span>, {% endif %} {% if model.properties.province.length > 0 %} <span>{{model.properties.province|first}}</span>, {% endif %} {% if model.properties.postalCode > 0 %} <span>{{model.properties.postalCode}}</span> {% endif %} </li>
            </ul>
         </div>
         <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 store-details-buttons">
            {% if pageContext.purchaseLocation.code == model.name %} <img class="home-store-icon Sirv" data-src="{{ themeSettings.sirvResourceImageUrl }}/my-store.svg" width="24px" height="24px"> 
            <h2 class="home-store">{{labels.myStore}}</h2>
            {% else %} <button class="make-my-store" id="makeThisMyStoreBtn" data-mz-store="{{model.name}}">{{labels.makeMyStore}}</button> {% endif %} <a href="{% if apiContext.headers.x-vol-locale == "fr-CA" %}/fr{% else %}{% if themeSettings.homeFurnitureSiteId != apiContext.headers.x-vol-site %}/en{% endif %}{% endif %}/flyer?storeType={{model.properties.storeType|first}}&locationId={{model.name}}" class="store-flyer" target="_self" title="{{labels.seeStoreFlyer}}">{{labels.seeStoreFlyer}}</a> 
         </div>
      </div>
      <div class="row">
         <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 store-info">
            <div class="other-store-details">
               <dl>
                  {% if model.properties.phone %} 
                  <dt><strong>{{labels.phone}}:</strong></dt>
                  <dd> {% for storeTel in model.properties.phone|split('#') %} {% if storeTel %} <a href="tel:+1{{storeTel}}" class="link store-tel" title="{{storeTel}}">{{storeTel}}</a> {% endif %} {% endfor %} </dd>
                  {% endif %} {% if model.properties.fax %} 
                  <dt><strong>{{labels.fax}}:</strong></dt>
                  <dd>{{model.properties.fax}}</dd>
                  {% endif %} {% if model.properties.email %} 
                  <dt><strong>{{labels.email}}:</strong></dt>
                  <dd><a href="mailto:{{model.properties.email}}?subject={{labels.emailSubject}}" class="link" title="{{labels.emailThisStore}}">{{labels.emailThisStore}}</a></dd>
                  {% endif %} 
               </dl>
            </div>
            <div class="hours-details" id="store-info">
            </div>
            {% if model.properties.storeType.length > 0%} 
            <div class="store-type"> {% if themeSettings.frSiteId == apiContext.headers.x-vol-site %} {% if model.properties.storeType|first == labels.enHH %} <span> <strong>{{labels.storeType}}:</strong> {{ labels.frHH }} </span> {% endif %} {% if model.properties.storeType|first == labels.enHHBC %} <span> <strong>{{labels.storeType}}:</strong> {{ labels.frHHBC }} </span> {% endif %} {% if model.properties.storeType|first == labels.enHBC %} <span> <strong>{{labels.storeType}}:</strong> {{ labels.frHBC }} </span> {% endif %} {% else %} <span> <strong>{{labels.storeType}}:</strong> {{model.properties.storeType|first}} </span> {% endif %} {% comment %} <span class="material-icons info-icon" data-toggle="popover" data-content="{{labels.storeTypeInfo}}" data-original-title="" title="" data-placement="top">info</span> {% endcomment %} </div>
            {% endif %} {% if model.properties.storeImage %} 
            <div class="store-image"> <span><strong>{{labels.storeImageNote}}:</strong> {{ model.properties.storeImage|safe }} </span> </div>
            {% endif %} 
         </div>
         <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 store-map">
            <a href="{{labels.googleMapDirection}}{% if model.properties.latitude %}{{model.properties.latitude}}{% endif %},{% if model.properties.longitude %}{{model.properties.longitude}}{% endif %}" class="get-direction hidden-xs" target="_blank" title="{{labels.getDirections}}">{{labels.getDirections}}</a> 
            <div class="map"> {% include "modules/location/store-map" %} </div>
            <div class="store-map-link"> <a href="{{labels.googleMapDirection}}{% if model.properties.latitude %}{{model.properties.latitude}}{% endif %},{% if model.properties.longitude %}{{model.properties.longitude}}{% endif %}" class="get-direction" target="_blank" title="{{labels.getDirections}}">{{labels.getDirections}}</a> </div>
         </div>
      </div>
   </div>
</div>
<div class="store-details-bottom-section">
   <div class="container">
      {% if themeSettings.homeFurnitureSiteId == apiContext.headers.x-vol-site %} {% if model.properties.homeFurnitureServices.length > 0 %} 
      <div class="row">
         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="services">
               <h2 class="section-title">{{labels.productsAtThisLocation}}</h2>
               <ul class="services-list">
                  {% for service in model.properties.homeFurnitureServices %} 
                  <li>{{service}}</li>
                  {% endfor %} 
               </ul>
            </div>
         </div>
      </div>
      {% endif %} {% endif %} {% if model.properties.storeServices.length > 0 or model.properties.commercialMaintenance or model.properties.homeRentals %} 
      <div class="row">
         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="services">
               <h2 class="section-title">{{labels.servicesAtLocation}}</h2>
               <ul class="services-list">
                  {% if themeSettings.frSiteId == apiContext.headers.x-vol-site %} {% for service in model.properties.storeServices %} {% if service == labels.enAirLiquidDepot %} 
                  <li>{{ labels.airLiquidDepot }}</li>
                  {% endif %} {% if service == labels.enBackyardProjectPackages %} 
                  <li>{{ labels.backyardProjectPackages }}</li>
                  {% endif %} {% if service == labels.enBottledWater %} 
                  <li>{{ labels.bottledWater }}</li>
                  {% endif %} {% if service == labels.enColorMatch %} 
                  <li>{{ labels.colorMatch }}</li>
                  {% endif %} {% if service == labels.enDelivery %} 
                  <li>{{ labels.delivery }}</li>
                  {% endif %} {% if service == labels.enFreeEstimates %} 
                  <li>{{ labels.freeEstimates }}</li>
                  {% endif %} {% if service == labels.enGiftWrapping %} 
                  <li>{{ labels.giftWrapping }}</li>
                  {% endif %} {% if service == labels.enGlassCutting %} 
                  <li>{{ labels.glassCutting }}</li>
                  {% endif %} {% if service == labels.enHomeAndCottageDesign %} 
                  <li>{{ labels.homeAndCottageDesign }}</li>
                  {% endif %} {% if service == labels.enHomeGiftCard  %} 
                  <li>{{ labels.homeGiftCard }}</li>
                  {% endif %} {% if service == labels.enHomeHandymanRepairs %} 
                  <li>{{ labels.homeHandymanRepairs }}</li>
                  {% endif %} {% if service == labels.enHomeInstalls %} 
                  <li>{{ labels.homeInstalls }}</li>
                  {% endif %} {% if service == labels.enKeyCutting %} 
                  <li>{{ labels.keyCutting }}</li>
                  {% endif %} {% if service == labels.enKitchenAndBathroomDesign %} 
                  <li>{{ labels.kitchenAndBathroomDesign }}</li>
                  {% endif %} {% if service == labels.enKnifeSharpening %} 
                  <li>{{ labels.knifeSharpening }}</li>
                  {% endif %} {% if service == labels.enLockRepair %} 
                  <li>{{ labels.lockRepair }}</li>
                  {% endif %} {% if service == labels.enPropane %} 
                  <li>{{ labels.propane }}</li>
                  {% endif %} {% if service == labels.enPurolator %} 
                  <li>{{ labels.purolator }}</li>
                  {% endif %} {% if service == labels.enReKeyLocks %} 
                  <li>{{ labels.reKeyLocks }}</li>
                  {% endif %} {% if service == labels.enScreenRepair %} 
                  <li>{{ labels.screenRepair }}</li>
                  {% endif %} {% if service == labels.enSkateSharpening %} 
                  <li>{{ labels.skateSharpening }}</li>
                  {% endif %} {% if service == labels.enSpecialOrders %} 
                  <li>{{ labels.specialOrders }}</li>
                  {% endif %} {% if service == labels.enToolRental %} 
                  <li>{{ labels.toolRental }}</li>
                  {% endif %} {% if service == labels.enWeddingRegistry %} 
                  <li>{{ labels.weddingRegistry }}</li>
                  {% endif %} {% if service == labels.enWindowRepair %} 
                  <li>{{ labels.windowRepair }}</li>
                  {% endif %} {% if service == labels.enFedEx %} 
                  <li>{{ labels.fedEx }}</li>
                  {% endif %} {% endfor %} {% if model.properties.commercialMaintenance %} 
                  <li>{{ labels.commercialMaintenance }}</li>
                  {% endif %} {% if model.properties.homeRentals %} 
                  <li>{{ labels.homeRentals }}</li>
                  {% endif %} {% if model.properties.customServices %} {% for customService in model.properties.customServices|split(',') %} 
                  <li> {{ customService }}</li>
                  {% endfor %} {% endif %} {% else %} {% for service in model.properties.storeServices %} 
                  <li>{{service}}</li>
                  {% endfor %} {% if model.properties.commercialMaintenance %} 
                  <li>{{ labels.commercialMaintenance }}</li>
                  {% endif %} {% if model.properties.homeRentals %} 
                  <li>{{ labels.homeRentals }}</li>
                  {% endif %} {% if model.properties.customServices %} {% for customService in model.properties.customServices|split(',') %} 
                  <li> {{ customService }}</li>
                  {% endfor %} {% endif %} {% endif %} 
               </ul>
            </div>
         </div>
      </div>
      {% endif %} {% if model.properties.ownerMessage or model.properties.facebook or model.properties.twitter or model.properties.instagram or model.properties.pinterest or model.properties.websiteAddress %} 
      <div class="row">
         <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="owner-details">
               {% if model.properties.ownerMessage %} 
               <h2 class="section-title">{{labels.msgFromOwner}}</h2>
               <p class="owner-msg">{{model.properties.ownerMessage|safe}}</p>
               {% endif %}  
               <div class="store-social-details">
                  {% if model.properties.facebook or model.properties.twitter or model.properties.instagram or model.properties.pinterest or model.properties.googlePlus %} 
                  <div class="social-media">
                     <strong>{{labels.followUs}}:</strong> 
                     <ul class="social-links">
                        {% if model.properties.facebook %} 
                        <li><a href="https://{{model.properties.facebook}}" class="facebook" target="_blank" title="facebook"><img class="Sirv" data-src="{{ themeSettings.sirvResourceImageUrl }}/facebook.svg" class="img-responsive" alt="facebook"></a></li>
                        {% endif %} {% if model.properties.twitter %} 
                        <li><a href="https://{{model.properties.twitter}}" class="twitter" target="_blank" title="twitter"><img class="Sirv" data-src="{{ themeSettings.sirvResourceImageUrl }}/twitter.svg" class="img-responsive" alt="twitter"></a></li>
                        {% endif %} {% if model.properties.instagram %} 
                        <li><a href="https://{{model.properties.instagram}}" class="instagram" target="_blank" title="instagram"><img class="Sirv" data-src="{{ themeSettings.sirvResourceImageUrl }}/instagram.png" class="img-responsive" alt="instagram"></a></li>
                        {% endif %} {% if model.properties.pinterest %} 
                        <li><a href="https://{{model.properties.pinterest}}" class="pinterest" target="_blank" title="pinterest"><img class="Sirv" data-src="{{ themeSettings.sirvResourceImageUrl }}/pinterest.svg"  class="img-responsive" alt="pinterest"></a></li>
                        {% endif %} {% if model.properties.googlePlus %} 
                        <li><a href="https://{{model.properties.googlePlus}}" class="google-plus" target="_blank" title="google-plus"><img class="Sirv" data-src="{{ themeSettings.sirvResourceImageUrl }}/google-plus.png"  class="img-responsive" alt="google-plus"></a></li>
                        {% endif %} 
                     </ul>
                  </div>
                  {% endif %} {% if model.properties.websiteAddress %} {% for urlparam in model.properties.websiteAddress|split(':') %} {% if urlparam == "https" or urlparam == "http" %} {% set_var websiteUrlParam=urlparam %} {% endif %} {% endfor %} 
                  <div class="website-link"> <strong>{{labels.website}}:</strong> {% if websiteUrlParam %} <a href="{{model.properties.websiteAddress}}" class="link" target="_blank" title="{{labels.websiteUrl}}">{{model.properties.websiteAddress}}</a> {% else %} <a href="http://{{model.properties.websiteAddress}}" class="link" target="_blank" title="{{labels.websiteUrl}}">http://{{model.properties.websiteAddress}}</a> {% endif %} </div>
                  {% endif %} 
               </div>
            </div>
         </div>
      </div>
      {% endif %} 
   </div>
</div>
<div id="store-change-confirmation-modal"  class="modal fade store-change-confirmation" role="dialog" tabindex="-1"> {% include "modules/location/store-change-confirmation" %} </div>
<div id="non-ecom-store-confirmation-modal"  class="modal fade store-change-confirmation" role="dialog" tabindex="-1"> {% include "modules/location/non-ecom-store-change-confirmation" %} </div>
{% endwith %}   <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.44.2/mapbox-gl.js'></script> 
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.44.2/mapbox-gl.css' rel='stylesheet' />
{% endblock body-content %}