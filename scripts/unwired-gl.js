//Instance in dot-notation
var unwired = {};
unwired.key = null;
//List of currently supported layers and their names
unwired.supportedLayers = {"streets" : "Streets", "earth" : "Satellite", "hybrid" : "Hybrid"};
var protocol = "https";
var subDomain = "tiles";
var baseDomain =  "locationiq.org";
var finalHost = protocol + "://" + subDomain + "." + baseDomain;

// Control implemented as ES5 prototypical class
function unwiredLayerControl(options) {
    this.options = options;
      
}

unwiredLayerControl.prototype = {
    options: {
        layers: ["streets", "earth", "hybrid"]
      },
    
    onAdd: function(map) {
        this._map = map;
        this._checkedLayer = "streets";
        this._container = document.createElement('div');
        this._container.className = 'unwiredgl-menu mapboxgl-ctrl mapboxgl-ctrl-group';
        this._container.id = 'unwiredgl-menu';
        this._container.addEventListener('contextmenu', function (e) {
            return event.preventDefault ? event.preventDefault() : event.returnValue = false;
          });    
        var layers = this.options.layers;
        this._layerButtons = {};        
        this._map.addControl(new mapboxgl.AttributionControl({
            compact: true
        }));        
        //add click listener for each of them
        var i = 0,switchButton,switchLabel;
        for (i = 0; i < layers.length; ++i) {
            //check if we street is supported
            if (typeof unwired.supportedLayers[layers[i]] === 'undefined') {
                continue;
            }
        
            switchButton = window.document.createElement('input');
            switchButton.id = layers[i];
            switchButton.setAttribute('type', 'radio');
            switchButton.setAttribute('value', layers[i]);
            this._layerButtons[layers[i]] = switchButton;
            //first layer will be the default one
            if (i === 0) {
                this._checkedLayer = layers[i];
                switchButton.checked = true;
            } 
            
            switchLabel = window.document.createElement("Label");
            switchLabel.setAttribute("for",layers[i]);
            switchLabel.innerHTML = unwired.supportedLayers[layers[i]];
            switchLabel.addEventListener('click', this._switchLayer.bind(this, layers[i]) );            
            this._container.appendChild(switchButton);
            this._container.appendChild(switchLabel);
        }
        
        return this._container;
    },

    onRemove: function () {
        this._container.parentNode.removeChild(this._container);
        this._map = undefined;
    },

    _switchLayer: function (layer) {
        this._map.setStyle(unwired.getLayerConfigUrl(layer));
        this._radioSelector(layer);
    },

    _radioSelector: function (selection) {
        this._layerButtons[this._checkedLayer].checked = false;
        this._layerButtons[selection].checked = true;
        this._checkedLayer = selection;
    }    
        
};

unwired.getLayer = unwired.getLayerConfigUrl = function (layer) {
    return finalHost + '/v2/style-' + layer + '.json?key=' + unwired.key;
};
