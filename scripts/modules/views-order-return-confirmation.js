define(["modules/api",
    'modules/jquery-mozu',
    'underscore',
    'hyprlive',
    'modules/backbone-mozu'
], function (api,$, _, Hypr, Backbone) {
    var byteArray;
    var ReturnConfirmationlView = Backbone.MozuView.extend({
        templateName: 'modules/my-account/my-account-order-return-confirmation',
        additionalEvents: {
            "click .downloadLable": "downloadLable",
            "click .openPrintPreview": "openPrintPreview",
            "click .openReturnHistory": "showReturnHistory"
        },
        render: function () {
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            this.model.set("currentLocale", locale, {silent: true});
            this.checkReplaceViaCourierItems();
            Backbone.MozuView.prototype.render.apply(this, arguments);
        },
        checkReplaceViaCourierItems: function(){
            var replaceViaCourier = _.findWhere(this.model.attributes.data.items, {replaceType : 'replaceViaCourier'});
            if(replaceViaCourier) this.model.set('shipViaCourierItemPresent', true);
        },
        downloadLable: function(){
            var self = this;
            self.$el.parent().siblings(".content-loading").css('display','block');
            self.getShippingLabel().then(function(res){
                self.$el.parent().siblings(".content-loading").css('display','none');
                byteArray = res.byteArray;
                var bin = atob(byteArray);
                var link = document.createElement('a');
                link.innerHTML = 'Download PDF file';
                link.download = 'ShippingLabel.pdf';
                link.href = 'data:image/pdf;base64,' + bin;
                link.click();
            },function(err){
                console.log("Failed to get api response",err);
                self.$el.parent().siblings(".content-loading").css('display','none');
            });
        },
        openPrintPreview: function(){
            var self = this;
            self.$el.parent().siblings(".content-loading").css('display','block');
            self.getShippingLabel().then(function(res){
                self.$el.parent().siblings(".content-loading").css('display','none');
                byteArray = res.byteArray;
                var bin = atob(byteArray);
                var obj = document.createElement('object');
                obj.style.width = '100%';
                obj.style.height = '842pt';
                obj.type = 'application/pdf';
                obj.data = 'data:application/pdf;base64,' + bin;
                var iframe = "<iframe width='100%' height='100%' src='" + obj.data + "'></iframe>";
                var x = window.open();
                x.document.open();
                x.document.write('<title>Shipping Label</title>');
                x.document.write(iframe);
                x.document.close();
            },function(err){
                console.log("Failed to get api response",err);
                self.$el.parent().siblings(".content-loading").css('display','none');
            });
        },
        getShippingLabel: function(){
            var shipmentNumber = this.model.get('data').webSessionId;
            var returnNumber = this.model.get('data').returnNumber;
            var payLoad = {
                "shipmentNumber" : shipmentNumber,
                "returnNumber" : returnNumber
            },
            requestBody = {
                url: Hypr.getThemeSetting("getShippingLabelURL"),
                data: JSON.stringify(payLoad),
                type: "POST",
                cors: true,
                contentType: 'application/json'
            };
            return $.ajax(requestBody);
        },
        showReturnHistory: function(){
            $('#orderReturnModal').modal('hide');
            $("#account-nav").find("#returnHistory-tab").trigger('click');
            $("#orderReturnModal").on('hide.bs.modal', function(){
                $('.modal-backdrop').remove();
            });
        }
    });

    return ReturnConfirmationlView;
});