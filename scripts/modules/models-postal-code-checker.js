define(["underscore", "modules/backbone-mozu", "hyprlive", "modules/api",
"modules/jquery-mozu"], function (_, Backbone, Hypr, api, $) {
    var PostalCodeChecker = Backbone.MozuModel.extend({
        checkPostalCode: function (postalcode) {
            var self = this,
                postalCode = postalcode.trim().split(" ").join("");
            return new Promise(function (resolve, reject) {
                var isValidPostalCode = self.validatePostalCode(postalCode);

                if (isValidPostalCode) {
                    // function call for ajax
                    self.getPostalCode(postalCode).then(function (response) {
                        var status = response.status,
                            expiryDate = new Date(),
                            cookieStatus = status === "SUCCESS" ? "Valid" : "Invalid";
                        expiryDate.setYear(expiryDate.getFullYear() + 1);

                        $.cookie("sthPostalCode",JSON.stringify({ postalCode: postalCode.toUpperCase(), status: cookieStatus }),{ path: "/" , expires: expiryDate});

                        if (status === "SUCCESS") {
                            resolve("Postal code is allowed");
                        } else {
                            reject("Postal code not allowed");
                        }
                    }, function(error){
                        console.log('Error while checking postal code', error);
                        reject("Error while checking postal code");
                    });
                } else {
                    reject("Invalid Postal Code");
                }
            });
        },
        validatePostalCode: function (postalCode) {
            var regex = /^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ]( )?\d[ABCEGHJKLMNPRSTVWXYZ]\d$/i;
            return regex.test(postalCode) ? true : false;
        },
        getPostalCode: function (postalCode) {
            return api.request("POST", "/hh/api/checkPostalCodeRestriction", { postalCode: postalCode });
        }
    });

    return PostalCodeChecker;
});
