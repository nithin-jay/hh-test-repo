define([
    'modules/jquery-mozu',
    'underscore',
    'hyprlive',
    'modules/backbone-mozu',
    "hyprlivecontext",
    'pages/myaccount'

], function ($, _, Hypr, Backbone, OrderHistoryView) {
    var returnItemModalView = OrderHistoryView.extend({
        templateName: 'modules/my-account/my-account-return-item-modal',
        additionalEvents: {

        },
        autoUpdate: [
            'otherReason'
        ],
        initialize: function(options){
            this.parent = options.parent;
        },
        render: function () {
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            this.model.set("currentLocale", locale, {silent: true});
            Backbone.MozuView.prototype.render.apply(this, arguments);
        },
        onReasonChange: function (event) {
            var selectedReasonCode = $(event.currentTarget).find("option:selected").val();
            this.model.set('selectedReasonCode', selectedReasonCode);
            if ("Other" === selectedReasonCode) {
                this.$el.find('.submit').prop("disabled", true);
                this.$el.find('.specify-reason-container').removeClass('hidden');
            } else if ("default" === selectedReasonCode) {
                this.$el.find('.submit').prop("disabled", true);
            } else {
                this.$el.find('.submit').prop("disabled", false);
                this.$el.find('.specify-reason-container').addClass('hidden');
                this.$el.find('.other-reason').val('');
            }
        },
        onReasonEnter: function (event) {
            var enteredReason = $(event.currentTarget).val().trim();
            if (enteredReason === '') {
                this.$el.find('.submit').prop("disabled", true);
            } else {
                this.$el.find('.submit').prop("disabled", false);
            }
        },
        initiateReturnItem: function () {
           this.initiateCancelOrder();
        }

    });

    return returnItemModalView;
});