require([
	"modules/jquery-mozu"
	],function($){
	
	$(document).ready(function() {
		//paint-calculator function
		$('#btn_calculate').click(function(){
			var m = 0;
	       	 var j = 0;
	       	 var f = 0;
	       	 var n = new RegExp("[^0-9.]");
	       	 var i = "";
	       	 var q = "";
	       	 var locale = require.mozuData('apicontext').headers['x-vol-locale'];
	    	 locale = locale.split('-')[0];
	       	 if (locale == "en") {
	       	  i = "Please Enter Numeric Characters Only.";
	       	  q = "Please Enter both height and width.";
	       	 } else {
	       	  i = "Veuillez ecrire les caracteres numeriques seulement.";
	       	  q = "Entrez S'il vous plait la hauteur et la largeur.";
	       	 }
	       	 if (n.test($("#wall1w").val()) || $("#wall1w").val() == ".") {
	       	  alert(i);
	       	  $("#wall1w").focus();
	       	  return;
	       	 }
	       	 if (n.test($("#wall1h").val()) || $("#wall1h").val() == ".") {
	       	  alert(i);
	       	  $("#wall1h").focus();
	       	  return;
	       	 }
	       	 if (n.test($("#wall2w").val()) || $("#wall2w").val() == ".") {
	       	  alert(i);
	       	  $("#wall2w").focus();
	       	  return;
	       	 }
	       	 if (n.test($("#wall2h").val()) || $("#wall2h").val() == ".") {
	       	  alert(i);
	       	  $("#wall2h").focus();
	       	  return;
	       	 }
	       	 if (n.test($("#wall3w").val()) || $("#wall3w").val() == ".") {
	       	  alert(i);
	       	  $("#wall3w").focus();
	       	  return;
	       	 }
	       	 if (n.test($("#wall3h").val()) || $("#wall3h").val() == ".") {
	       	  alert(i);
	       	  $("#wall3h").focus();
	       	  return;
	       	 }
	       	 if (n.test($("#wall4w").val()) || $("#wall4w").val() == ".") {
	       	  alert(i);
	       	  $("#wall4w").focus();
	       	  return;
	       	 }
	       	 if (n.test($("#wall4h").val()) || $("#wall4h").val() == ".") {
	       	  alert(i);
	       	  $("#wall4h").focus();
	       	  return;
	       	 }
	       	 if (n.test($("#door1w").val()) || $("#door1w").val() == ".") {
	       	  alert(i);
	       	  $("#door1w").focus();
	       	  return;
	       	 }
	       	 if (n.test($("#door1h").val()) || $("#door1h").val() == ".") {
	       	  alert(i);
	       	  $("#door1h").focus();
	       	  return;
	       	 }
	       	 if (n.test($("#door2w").val()) || $("#door2w").val() == ".") {
	       	  alert(i);
	       	  $("#door2w").focus();
	       	  return;
	       	 }
	       	 if (n.test($("#door2h").val()) || $("#door2h").val() == ".") {
	       	  alert(i);
	       	  $("#door2h").focus();
	       	  return;
	       	 }
	       	 if (n.test($("#window1w").val()) || $("#window1w").val() == ".") {
	       	  alert(i);
	       	  $("#window1w").focus();
	       	  return;
	       	 }
	       	 if (n.test($("#window1h").val()) || $("#window1h").val() == ".") {
	       	  alert(i);
	       	  $("#window1h").focus();
	       	  return;
	       	 }
	       	 if (n.test($("#window2w").val()) || $("#window2w").val() == ".") {
	       	  alert(i);
	       	  $("#window2w").focus();
	       	  return;
	       	 }
	       	 if (n.test($("#window2h").val()) || $("#window2h").val() == ".") {
	       	  alert(i);
	       	  $("#window2h").focus();
	       	  return;
	       	 }
	       	 var l = $("#wall1w").val() * $("#wall1h").val();
	       	 if (l === 0 && ($("#wall1w").val() > 0 || $("#wall1h").val() > 0)) {
	       	  alert(q);
	       	  var newWallToFocus = ($("#wall1w").val() >= 1) ? $("#wall1h") : $("#wall1w");
	       	  newWallToFocus.focus();
	       	  return;
	       	 }
	       	 var k = $("#wall2w").val() * $("#wall2h").val();
	       	 if (k === 0 && ($("#wall2w").val() > 0 || $("#wall2h").val() > 0)) {
	       	  alert(q);
	       	  var newWall2ToFocus = ($("#wall2w").val() >= 1) ? $("#wall2h") : $("#wall2w");
	       	  newWall2ToFocus();
	       	  return;
	       	 }
	       	 var h = $("#wall3w").val() * $("#wall3h").val();
	       	 if (h === 0 && ($("#wall3w").val() > 0 || $("#wall3h").val() > 0)) {
	       	  alert(q);
	       	  var newWall3ToFocus = ($("#wall3w").val() >= 1) ? $("#wall3h") : $("#wall3w");
	       	  newWall3ToFocus();
	       	  return;
	       	 }
	       	 var e = $("#wall4w").val() * $("#wall4h").val();
	       	 if (e === 0 && ($("#wall4w").val() > 0 || $("#wall4h").val() > 0)) {
	       	  alert(q);
	       	  var newWall4ToFocus = ($("#wall4w").val() >= 1) ? $("#wall4h") : $("#wall4w");
	       	  newWall4ToFocus.focus();
	       	  return;
	       	 }
	       	 m = l + k + h + e;
	       	 var g = $("#door1w").val() * $("#door1h").val();
	       	 if (g === 0 && ($("#door1w").val() > 0 || $("#door1h").val() > 0)) {
	       	  alert(q);
	       	  var newDoor1ToFocus = ($("#door1w").val() >= 1) ? $("#door1h") : $("#door1w");
	       	  newDoor1ToFocus.focus();
	       	  return;
	       	 }
	       	 var d = $("#door2w").val() * $("#door2h").val();
	       	 if (d === 0 && ($("#door2w").val() > 0 || $("#door2h").val() > 0)) {
	       	  alert(q);
	       	  var newDoor2ToFocus = ($("#door2w") >= 1).val() ? $("#door2h") : $("#door2w");
	       	  newDoor2ToFocus();
	       	  return;
	       	 }
	       	 var o = g + d;
	       	 var b = $("#window1w").val() * $("#window1h").val();
	       	 if (b === 0 && ($("#window1w").val() > 0 || $("#window1h").val() > 0)) {
	       	  alert(q);
	       	  var newWindow1ToFocus = ($("#window1w").val() >= 1) ? $("#window1h") : $("#window1w");
	       	  newWindow1ToFocus.focus();
	       	  return;
	       	 }
	       	 var a = $("#window2w").val() * $("#window2h").val();
	       	 if (a === 0 && ($("#window2w").val() > 0 || $("#window2h").val() > 0)) {
	       	  alert(q);
	       	  var newWindow2ToFocus = ($("#window2w").val() >= 1) ? $("#window2h") : $("#window2w");
	       	  newWindow2ToFocus.focus();
	       	  return;
	       	 }
	       	 var p = b + a;
	       	 j = m - o - p;
	       	 f = j / $("#askcans").val();
	       	 $("#wall1area").val(l);
	       	 $("#wall2area").val(k);
	       	 $("#wall3area").val(h);
	       	 $("#wall4area").val(e);
	       	 $("#total1").val(m);
	       	 $("#total2").val(j);
	       	 $("#givecans").val(f);
	       	 $("html, body").animate({
	       	  scrollTop: $('#calculatedValues').offset().top
	       	 }, 1500);
	   	
		});
		
		//paint-calculator function
		$('#btn_clear').click(function(){
			 $("#wall1w").val("");
	       	 $("#wall1h").val("");
	       	 $("#wall2w").val("");
	       	 $("#wall2h").val("");
	       	 $("#wall3w").val("");
	       	 $("#wall3h").val("");
	       	 $("#wall4w").val("");
	       	 $("#wall4h").val("");
	       	 $("#door1w").val("");
	       	 $("#door1h").val("");
	       	 $("#door2w").val("");
	       	 $("#door2h").val("");
	       	 $("#window1w").val("");
	       	 $("#window1h").val("");
	       	 $("#window2w").val("");
	       	 $("#window2h").val("");
	       	 $("#wall1area").val("");
	       	 $("#wall2area").val("");
	       	 $("#wall3area").val("");
	       	 $("#wall4area").val("");
	       	 $("#total1").val("");
	       	 $("#total2").val("");
	       	 $("#givecans").val("");
		});
	});

	
});
