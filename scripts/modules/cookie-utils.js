define([
    'modules/jquery-mozu', 
    'vendor/jquery/moment'
  ], 
  function ($, moment) {
    var CookieUtils = {

        setPreferredStore: function(selectedStore) {
            var expiryDate = moment().add(3, 'months').toDate();
            document.cookie = "preferredStore=" + JSON.stringify({code: selectedStore.code}) + ";expires=" + expiryDate + ";path=/;Secure;SameSite=None";
            localStorage.setItem('preferredStore', JSON.stringify(selectedStore));
        },

        setCookie: function(cookieName, cookieValue) {
            var expiryDate = moment().add(3, 'months').toDate();
			var cookieValueEncoded = encodeURI(JSON.stringify(cookieValue));
            document.cookie = cookieName + "=" + cookieValueEncoded + ";expires=" + expiryDate + ";path=/;Secure;SameSite=None";
        }

    };
    return CookieUtils;
});
