define([
    "underscore",
    "modules/backbone-mozu",
    "modules/api",
    "modules/jquery-mozu",
    'hyprlive'
], function (_, Backbone, api, $, Hypr) {
    function getSthActiveWarehouseLatLon(){
        var warehouseLatLongs = Hypr.getThemeSetting('warehouseLatLong'),
            activeWarehousesForSTH = Hypr.getThemeSetting("warehousesForSTH"),
            sthActiveWarehouseLatLong = [];
        _.each(activeWarehousesForSTH, function(warehouseCode){
            var warehouse =_.findWhere(warehouseLatLongs, {warehouseCode:warehouseCode});
            if(warehouseCode === warehouse.warehouseCode) sthActiveWarehouseLatLong.push(warehouse);
        });
        return sthActiveWarehouseLatLong;
    }
    var warehouseLatLongs =  getSthActiveWarehouseLatLon();
    var modelsShippingTimeLine = Backbone.MozuModel.extend({
        initialize: function(){
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            this.set("currentLocale", locale);
        },
        getShippingTimeline: function (customerPostalCode, warehouseInventory, pageType_or_quantity, shipItems) {
            var self = this;
            return new Promise(function (resolve, reject) {
                self.getCustomerLatLongValues(customerPostalCode).then(function (customerZipLatLong) {
                    self.calculateDistance(customerZipLatLong.lat, customerZipLatLong.lon);
                    if (pageType_or_quantity == "cart" || pageType_or_quantity == "checkout") {
                        self.findClosestWarehouse(shipItems, warehouseInventory, pageType_or_quantity);
                    } else {
                        self.getClosestWarehouse(warehouseInventory, pageType_or_quantity);
                    }
                    self.fetchShippingTimelines(customerPostalCode).then(function (response) {
                        resolve(response);
                    }, function (error) {
                        reject(error);
                    });
                }, function (error) {
                    reject(error);
                });
            });
        },
        getCustomerLatLongValues: function (customerPostalCode) {
            return new Promise(function (resolve, reject) {
                var searchConfigure = {
                    "async": true,
                    "crossDomain": true,
                    "url": "https://us1.locationiq.com/v1/search.php?key=" + Hypr.getThemeSetting('mapApiKey') + "&postalcode=" + customerPostalCode + "&format=json&countrycodes=ca",
                    "method": "GET"
                };
                $.ajax(searchConfigure).done(function (response) {
                    if (response && response.length > 0) {
                        resolve(response[0]);
                    }
                }).fail(function (error) {
                    reject(error);
                });
            });
        },
        calculateDistance: function (customerLat, customerLon) {
            var self = this;
            _.each(warehouseLatLongs, function (warehouse) {
                var distance = self.getDistanceFromLatLongInKm(customerLat, customerLon, warehouse.lat, warehouse.lon);
                warehouse.distance = distance;
            });
            console.log('warehouseLatLongs', warehouseLatLongs);
        },
        getDistanceFromLatLongInKm: function (lat1, lng1, lat2, lng2) {
            var earthRadius = 6371; // Radius of the earth in km
            var dLat = this.deg2rad(lat2 - lat1); // deg2rad below
            var dLng = this.deg2rad(lng2 - lng1);
            var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            var distance = earthRadius * c * 0.62137119; // Distance in Mi
            return distance;
        },
        /*this function converts deg to rad*/
        deg2rad: function (deg) {
            return deg * (Math.PI / 180);
        },
        getClosestWarehouse: function (warehouseInventory, quantity) {
            var self = this,
                sortedWarehouseWithDistance = _.sortBy(warehouseLatLongs, "distance"),
                closestWarehouse = _.find(sortedWarehouseWithDistance, function (warehouse) {
                    return self.getBufferedInventory(_.findWhere(warehouseInventory, {locationCode: warehouse.warehouseCode}).stockAvailable) >= quantity;
                });
            if(closestWarehouse){
                this.set('closestWarehouse', closestWarehouse);
            } else {
                this.set('closestWarehouse', sortedWarehouseWithDistance[0]);
            }
        },
        findClosestWarehouse: function (shipItems, warehouseInventory, pageType) {
            var self = this,
                sortedWarehouseWithDistance = _.sortBy(warehouseLatLongs, "distance");
            if(sortedWarehouseWithDistance.length > 1){
                var inventories = _.groupBy(warehouseInventory, function (inventory) {
                        return inventory.productCode;
                    }),
                    inventoryMapping = this.checkWarehouseInventory(sortedWarehouseWithDistance, inventories, shipItems, pageType),
                    reverseByDistance = sortedWarehouseWithDistance.reverse();//longest first
                if(inventoryMapping.length > 0){
                    for (var i = 0; i < reverseByDistance.length; i++) {
                        var isItemPresentInlongestWH = _.findWhere(inventoryMapping, {warehouseCode: reverseByDistance[i].warehouseCode});
                        if (isItemPresentInlongestWH) {
                            self.set('closestWarehouse', reverseByDistance[i]);
                            break;
                        }
                    }
                } else {
                    self.set('closestWarehouse', sortedWarehouseWithDistance[0]);
                }
            } else {
                self.set('closestWarehouse', sortedWarehouseWithDistance[0]);
            }
        },
        checkWarehouseInventory: function (sortedWarehouseWithDistance, inventories, shipItems, pageType) {
            var self = this,
                itemAvailableInWarehouse = [];
            _.each(shipItems, function (item) {
                var quantity = pageType == "cart" ? item.get('quantity') : item.quantity,
                    productCode = pageType == "cart" ? item.get('product').get('productCode') : item.product.productCode,
                    itemInventory = inventories[productCode];
                for (var i = 0; i < sortedWarehouseWithDistance.length; i++) {
                    var inventory = _.findWhere(itemInventory, {locationCode: sortedWarehouseWithDistance[i].warehouseCode});
                    var bufferInventory = self.getBufferedInventory(inventory.stockAvailable);
                    if (bufferInventory >= quantity) {
                        itemAvailableInWarehouse.push({
                            productCode: productCode,
                            warehouseCode: sortedWarehouseWithDistance[i].warehouseCode
                        });
                        console.log('inventoryAvail', productCode + ' ' + sortedWarehouseWithDistance[i].warehouseCode);
                        break;
                    }
                }
            });
            return itemAvailableInWarehouse;
        },
        getBufferedInventory: function (fetchedWarehouseCount) {
            var isWarehouseBufferEnabled = Hypr.getThemeSetting('enableWarehouseBuffer'),
                warehouseBufferValue = Hypr.getThemeSetting('warehouseBufferValue'),
                bufferedWarehouseInventory = 0,
                warehouseCount = fetchedWarehouseCount && fetchedWarehouseCount > 0 ? fetchedWarehouseCount : 0;
            if (isWarehouseBufferEnabled && warehouseBufferValue > 0 && warehouseCount > 1) {
                bufferedWarehouseInventory = Math.round((warehouseCount / 100) * warehouseBufferValue);
                bufferedWarehouseInventory = bufferedWarehouseInventory < 0.5 ? 1 : bufferedWarehouseInventory;
                warehouseCount = warehouseCount - bufferedWarehouseInventory;
            }
            return warehouseCount;
        },
        fetchShippingTimelines: function (customerPostalCode) {
            var self = this;
            return new Promise(function (resolve, reject) {
                var payload = self.getTimelineApiPayload(customerPostalCode),
                    requestBody = {
                        url: Hypr.getThemeSetting("getShippingTimelines"),
                        data: JSON.stringify(payload),
                        type: "POST",
                        cors: true,
                        contentType: 'application/json'
                    };
                $.ajax(requestBody).done(function (response) {
                    var currentLocale = self.get("currentLocale"),
                        shippingDays = response.shippingDateAndTime.expectedTransitTime,
                        bufferedTimeline = shippingDays + (currentLocale === "en-US" ? '-' : ' à ') + (shippingDays + Hypr.getThemeSetting('shippingTimelineBuffer'));
                    response.shippingTimeline = bufferedTimeline;
                    resolve(response);
                }).fail(function (error) {
                    reject(error);
                });
            });
        },
        getTimelineApiPayload: function (customerPostalCode) {
            var closestWarehouse = this.get('closestWarehouse'),
                payLoad = {
                    "sourcePostalCode": closestWarehouse.zipCode.trim().replace(/ /g, "").toUpperCase(),
                    "destinationPostalCode": customerPostalCode.trim().replace(/ /g, "").toUpperCase(),
                    "weight": 1,
                    "length": 1,
                    "width": 1,
                    "height": 1
                };
            return payLoad;
        }
    });

    return modelsShippingTimeLine;
});
