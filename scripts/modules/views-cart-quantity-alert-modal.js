define([
    'modules/jquery-mozu',
    'underscore',
    'modules/backbone-mozu',
    'modules/models-cart'
], function ($, _, Backbone, CartModels) {
    var cartQuantityAlertModal = Backbone.MozuView.extend({
        templateName: 'modules/cart/cart-quantity-alert-modal',
        render: function () {
            var apiContext = this.model.get("apiContext"),
                locale = apiContext.headers['x-vol-locale'],
                currentSite = apiContext.headers['x-vol-site'],
                pageContext = require.mozuData('pagecontext');
            this.model.set("currentLocale", locale, {silent: true});
            this.model.set("currentSite", currentSite, {silent: true});
            if (pageContext.pageType === "cart") {
                this.model.set("totalActionsPerformed", 0);
                this.model.set("totalItemCount", this.model.get("quantityUpdateItems").length + this.model.get("removeItems").length);
            }
            Backbone.MozuView.prototype.render.apply(this, arguments);
            this.bindModalCloseEvent(pageContext.pageType);
        },
        bindModalCloseEvent: function (pageType) {
            var self = this;
            if (pageType === "cart") {
                $('#quantityAlertModal').on('hidden.bs.modal', function () {
                    if (self.model.attributes.quantityUpdateItems) $(document).trigger('refreshCart');
                });
            }
        },
        updateCartItemQuantity: function (event) {
            var self = this,
                $el = $(event.currentTarget),
                cartItem = self.getCartItem($el, true);
            cartItem.apiUpdateQuantity(cartItem.get('quantity')).then(function (response) {
                self.checkAllActionsPerformed(cartItem.get('id'));
            }, function (error) {
                console.log("Error while updating product to cart", error);
            });
        },
        removeItemFromCart: function (event) {
            var self = this,
                $el = $(event.currentTarget),
                cartItem = self.getCartItem($el, true);
            cartItem.apiModel.del().then(function (response) {
                self.checkAllActionsPerformed(cartItem.get('id'));
            }, function (error) {
                console.log("Error while removing product to cart", error);
            });
        },
        moveToWishList: function (event) {
            var self = this,
                $el = $(event.currentTarget),
                cartItem = self.getCartItem($el, false),
                product = cartItem.get('product');
            product.apiAddToWishlist({
                customerAccountId: require.mozuData('user').accountId,
                quantity: 1
            }).then(function (response) {
                self.checkAllActionsPerformed(cartItem.get('id'));
            }, function (error) {
                console.log("Error While adding product to cart", error);
            });
        },
        acceptRemovedItem: function (event) {
            var $el = $(event.currentTarget),
                itemId = $el.data('mz-item-id');
            this.checkAllActionsPerformed(itemId);
        },
        getCartItem: function ($el, isUpdateItem) {
            var itemId = $el.data('mz-item-id'),
                items = isUpdateItem ? this.model.attributes.quantityUpdateItems : this.model.attributes.removeItems,
                item = _.findWhere(items, {'id': itemId});
            return new CartModels.CartItem(item);
        },
        checkAllActionsPerformed: function (itemId) {
            var self = this,
                totalPerformedActionCount = this.model.get('totalActionsPerformed') + 1,
                totalActionCount = this.model.get('totalItemCount');
            self.model.set('totalActionsPerformed', totalPerformedActionCount);
            self.$el.find("[data-id='" + itemId + "']").find('.buttons').addClass('hidden');
            self.$el.find("[data-id='" + itemId + "']").find('.success-icon').removeClass('hidden');
            if (totalPerformedActionCount === totalActionCount) {
                _.delay(function () {
                    self.$el.find('#quantityAlertModal').modal('hide');
                }, 1000);
            }
        }
    });

    return cartQuantityAlertModal;
});