﻿define(['shim!vendor/typeahead.js/typeahead.bundle[modules/jquery-mozu=jQuery]>jQuery', 'hyprlive', 'underscore', 'modules/api',
      'hyprlivecontext'], function($, Hypr, _, api,
        HyprLiveContext) {
	var searchQuery = HyprLiveContext.locals.pageContext.search.query;
    // bundled typeahead saves a lot of space but exports bloodhound to the root object, let's lose it
    var Bloodhound = window.Bloodhound.noConflict();

    var ua = navigator.userAgent.toLowerCase();
    // bloodhound wants to make its own AJAX requests, and since it's got such good caching and tokenizing algorithms, i'm happy to help it
    // so instead of using the SDK to place the request, we just use it to get the URL configs and the required API headers
    var qs = '%QUERY',
        eqs = encodeURIComponent(qs),
        suggestPriorSearchTerms = Hypr.getThemeSetting('suggestPriorSearchTerms'),
        getApiUrl = function(groups) {
            return api.getActionConfig('suggest', 'get', { query: qs, groups: groups }).url;
        },
        termsUrl = getApiUrl('terms'),
        productsUrl = getApiUrl('pages'),
        ajaxConfig = {
            headers: api.getRequestHeaders()
        },
        i,
        nonWordRe = /\W+/,
        makeSuggestionGroupFilter = function(name) {
            return function(res) {
                var suggestionGroups = res.suggestionGroups,
                    thisGroup;
                for (i = suggestionGroups.length - 1; i >= 0; i--) {
                    if (suggestionGroups[i].name === name) {
                        thisGroup = suggestionGroups[i];
                        break;
                    }
                }
                if(name == 'Pages' && thisGroup.suggestions.length === 0) {
                    $('.tt-dataset-cat').remove();
                }
                if(name == 'Terms') {
                	if(suggestionGroups[0].suggestions.length > 0) {
                		window.autocomplete = suggestionGroups[0].suggestions[0].suggestion.term;
                	} else {
                    	window.autocomplete = $('.tt-input').val();
                    }                	
                }
                return thisGroup.suggestions;
            };
        },

        makeTemplateFn = function(name) {
            var tpt = Hypr.getTemplate(name);
            return function(obj) {
                return tpt.render(obj);
            };
        },

    // create bloodhound instances for each type of suggestion

    AutocompleteManager = {
        datasets: {
            pages: new Bloodhound({
                datumTokenizer: function(datum) {
                    return datum.suggestion.term.split(nonWordRe);
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: productsUrl,
                    wildcard: eqs,
                    filter: makeSuggestionGroupFilter("Pages"),
                    rateLimitWait: 400,
                    ajax: ajaxConfig
                }
            })
        }
    };

    $.each(AutocompleteManager.datasets, function(name, set) {
        set.initialize();
    });
    
    var catarray = [];
    var count = 0;
    var deferCount = 0;
    var showProducts = function(categories) {
    	_.each(categories, function(category) {
            catarray.push(category);
        });
    	_.defer(function() {
    		deferCount++;
    		if(deferCount == count) {
    			deferCount = 0;
    			count = 0;
    			$('.tt-dataset-cat').remove();
    			var categoryArray = [];
    		    for(var i = 0;i < catarray.length; i++){
    		    	var isMatch = _.findWhere(categoryArray, {categoryId: catarray[i].categoryId});
    		        if(!isMatch){
    		        	categoryArray.push(catarray[i]);
    		        }
    		    }
    		    var counter = 0;
       		    var element = '<div class="tt-dataset-cat"><a href="javascript:void(0);">' + window.autocomplete +'</a><ul>'; 
    		    
    		    _.each(categoryArray, function(category) {
    		    	if(counter <= 3 && category.parentCategory.categoryCode !== Hypr.getThemeSetting('dynamicCategoryId')){
    		    		element += '<li class="search-category" data-category-id=' + category.categoryId + '><a href="javascript:void(0);">'+ Hypr.getLabel('inText') + category.content.name + '</a></li>';
    		    	}
    		    });
    		    
    		    element += '</ul></div>';
                $('.tt-dataset-pages').before(element);
                catarray = [];
    		    
    		}
            
         });
    };
    
    var dataSetConfigs = [
        {
            name: 'pages',
            displayKey: function(datum) {
            	count ++;
            	showProducts(datum.suggestion.categories);
            },
            templates: {
                suggestion: makeTemplateFn('modules/search/autocomplete-page-result'),
                empty: ''
            },
            source: AutocompleteManager.datasets.pages.ttAdapter()
        }
    ];

    if (suggestPriorSearchTerms) {
        AutocompleteManager.datasets.terms = new Bloodhound({
            datumTokenizer: function(datum) {
                return datum.suggestion.term.split(nonWordRe);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: termsUrl,
                wildcard: eqs,
                filter: makeSuggestionGroupFilter("Terms"),
                rateLimitWait: 100,
                ajax: ajaxConfig
            }
        });
        AutocompleteManager.datasets.terms.initialize();
        dataSetConfigs.push({
            name: 'terms',
            displayKey: function(datum) {
                return datum.suggestion.term;
            },
            source: AutocompleteManager.datasets.terms.ttAdapter()
        });
    }

    $(document).ready(function() {
        var $field = AutocompleteManager.$typeaheadField = $('[data-mz-role="searchquery"]');
        var locale = require.mozuData('apicontext').headers['x-vol-locale'];
        AutocompleteManager.typeaheadInstance = $field.typeahead({
            minLength: 3
        }, dataSetConfigs).data('ttTypeahead');
        // user hits enter key while menu item is selected;
        $field.on('typeahead:selected', function (e, data, set) {
        	if (data.suggestion.productCode) {
        		if(locale == "en-US"){        		
        			window.location.href = '/en/p/' + data.suggestion.productCode;
            	} else{
            		window.location.href = '/fr/p/' + data.suggestion.productCode;
            	}
        	}
        });
        $(document).on('click', '.tt-dataset-cat ul>li>a', function() { 
    		var categoryId = $(this).parent().data('categoryId');    		
    		if(locale == "en-US"){        		
    			window.location.href = '/en/search?query=' + window.autocomplete + '&categoryId=' + categoryId;
        	} else{
        		window.location.href = '/fr/search?query=' + window.autocomplete + '&categoryId=' + categoryId;
        	}
        });
        
        $(document).on('click', '.tt-dataset-cat>a', function() { 
    		var categoryId = $(this).parent().data('categoryId');    		
    		if(locale == "en-US"){        		
    			window.location.href = '/en/search?query=' + window.autocomplete;
        	} else{
        		window.location.href = '/fr/search?query=' + window.autocomplete;
        	}
        });

        var stopwords = [];
        if(locale == "fr-CA"){
            stopwords = ["un","une","an","&","et","sont","êtes","etes","comme","à","a","au","aux","e","chez","es","est", "mais","sauf","par","de","pour","car","si","dans","selon","is","le","la","il","elle","lui","ça","ca","aucun","sans","pas","ne","of","sur","ou","tel","telle","qui","ce","cette","ces","que","lequel","laquelle","afin","dont","les","des","leur","leurs","ensuite","alors","puis","après","apres","ainsi","donc","là","la","voici","voilà","voila","quel","quelle","quels","quelles","lesquels","lesquelles","celle","ce","celles","ceux","celui","ces","ils","elles","on","ceci","avec" ];
        }else {
            stopwords = ["a","an","and","&","are","as","at","be","but","by","for","if","in","into","is","it","no","not","of","on","or","such","that","the","their","then","there","these","they","this","to","was","will","with"];
        }
        $('.mz-searchbox').on('submit', function(e) {
            e.preventDefault();
            var $ele = $(e.currentTarget).find('#search-input');
            // var term = $ele.val().trim();
            // var isProductCode = term && (term.length < 9) && (/\d{7}$/.test(term) || /\d{4}-\d{3}/.test(term));
            // // console.log('mz-searchbox submit: isProductCode -->', isProductCode);
            // if(isProductCode) {
            //     var isFurnitureSite = false;
            //     var contextSiteId = require.mozuData('apicontext').headers['x-vol-site'];
            //     if(Hypr.getThemeSetting("homeFurnitureSiteId") === contextSiteId){
            //         isFurnitureSite = true;
            //     }
            //     var redirectPath = '/p/' + term.replace(/-/g, '');
            //     if (isFurnitureSite) {
            //         window.location.href = redirectPath;
            //     } else {
            //         window.location.href = ((locale == "fr-CA") ? '/fr' : '/en') + redirectPath;
            //     }
            //     return;
            // }
            var searchKeyword = $ele.val();

            if (ua.indexOf('chrome') > -1) {
                if (/edg|edge/i.test(ua)){
                    $.cookie('stopwords', searchKeyword, {path:'/'});
                }else{
                    document.cookie = "stopwords="+searchKeyword+";path=/;Secure;SameSite=None";
                }
            } else {
                $.cookie('stopwords', searchKeyword, {path:'/'});
            }
            /** Allowed pattern (Flyer Item#): 1730-103 to 185 */
            var isFlyerPattern = false;
            var queryParts = searchKeyword.split('to');
            if (queryParts.length == 2) {
                var leftPart = queryParts[0].trim();
                var rightPart = queryParts[1].trim();
                if (/\d{4}-\d{3}/.test(leftPart) && /\d{3}/.test(rightPart)) {
                    isFlyerPattern = true;
                }
            }
            removeStopwords(searchKeyword.toLowerCase(), isFlyerPattern);
            $(".mz-searchbox-input").val(searchKeyword);
            $("#search-input").val(searchKeyword);
        });

        function removeStopwords(searchKeyword, isFlyerPattern) {
            var result = [];
            var keywords = searchKeyword.split(' ');
            _.each(keywords, function(keyword) {
                if((isFlyerPattern && keyword === 'to') || (stopwords.indexOf(keyword) === -1)) {
                    result.push(keyword);
                }
            });
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            locale = locale.split('-')[0];
            var currentLocale = '';
            if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
                currentLocale = locale === 'fr' ? '/fr' : '/en';
            }
            var removedchar = result.join('+');

            if (ua.indexOf('chrome') > -1) {
                if (/edg|edge/i.test(ua)){
                    $.cookie('QueryWithoutStopwords', removedchar, {path:'/'});
                }else{
                    document.cookie = "QueryWithoutStopwords="+removedchar+";path=/;Secure;SameSite=None";
                }
            } else {
                $.cookie('QueryWithoutStopwords', removedchar, {path:'/'});
            }
            setTimeout(function(){document.location.href = currentLocale+"/search?query="+removedchar;},500);

        }
       
        
    });

    return AutocompleteManager;
});
