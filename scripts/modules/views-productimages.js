﻿define(['modules/jquery-mozu', 'underscore', "modules/backbone-mozu", 'hyprlive'], function ($, _, Backbone, Hypr) {

    var ProductPageImagesView = Backbone.MozuView.extend({
        templateName: 'modules/product/product-images',
        sliderFunction: function() {
        	$(document).ready(function(){
        		$("figure.mz-productimages-thumbs").removeClass("hidden");
        	});
            var divLength = $('.slider-first-item').size();  
            var sildeLoop = null;
            if(divLength > 1) {
                sildeLoop =  true;
            }
            else {
                sildeLoop = false;
            }
            $(".regular").slick({
                dots: false,
                infinite: false,
                slidesToShow: 4,
                slidesToScroll: 4,
                centerPadding: '0',
                variableWidth:true,
                lazyLoad: 'demand',
                responsive: [
                 {
                   breakpoint: 1025,
                   settings: {
                     slidesToShow: 3,
                     slidesToScroll: 3
                   }
                 },
                 {
                     breakpoint: 520,
                     settings: {
                       slidesToShow: 3,
                       slidesToScroll: 3
                     }
                   },
                 {
                     breakpoint: 375,
                     settings: {
                       slidesToShow: 2,
                       slidesToScroll: 2
                     }
                   }
               ]
            });
            
            
            $(".regular-related-products").not('.slick-initialized').slick({
                dots: false,
                infinite: false,
                slidesToShow: 4,
                slidesToScroll: 4,
                variableWidth:true,
                lazyLoad: 'demand',
                responsive: [
				{
				    breakpoint: 991,
				    settings: {
				      slidesToShow: 2,
				      slidesToScroll: 2
				    }
				 },
                 {
                   breakpoint: 520,
                   settings: {
                     slidesToShow: 1,
                     slidesToScroll: 1
                   }
                 }
               ]
            });
        },
        render: function () {

        	var me = this,isSpinAvailable = me.model.get("isSpinAvailable"),
        	locale = require.mozuData('apicontext').headers['x-vol-locale'];
            me.model.set("currentLocale", locale, {silent: true});
            me.model.set("isVideoAvailable", window.vzaarUrl, {silent: true});
            /**
             * Below code creates sirvProductSubFolder name as per the Spin/normal images 
             * Url Example : - https://homehardware.sirv.com/products/126/1262155/spin/spin.spin
             * Here as 1262155 is the product code so product second level folder name will be 126. 
             * */
            var sirvProductSubFolder = me.model.get("productCode").substring(0,3);
            me.model.set("sirvProductSubFolder", sirvProductSubFolder, {silent: true});

       
            
        	if(!isSpinAvailable) {

            	window.Sirv.on("viewer:ready",function(){
                // window.Sirv.viewer.filters.add(smvGalleryFilterItemsByLang);
                // window.Sirv.viewer.filters.add(smvGallerySortItemsByName); 

              
                    // if($("div").hasClass("smv-slides-box")) {
                    // 	$("div.smv").addClass("smv-selectors-bottom");
                    // 	var viewerElement = document.querySelector("div.smv-slides-box");
                    // 	var thumbnailElement = document.querySelector("div.smv-thumbnails");
                    // 	var exclusiveTagElement = document.querySelector("div.flag-container");
                    // 	if(null === thumbnailElement){
                    // 		$(".mz-productimages-main").addClass("without-thumbnails");
                    // 	} 
                    // 	if(null !== exclusiveTagElement){
                    // 	  viewerElement.insertAdjacentElement("afterend", exclusiveTagElement);
                    // 	  $(".mz-productimages-main").removeClass("without-thumbnails").addClass("withExclusiveTag");
                    // 	}
                    // 	$(".flag-container").removeClass("hidden");
                    // }
                    // console.log('SIRV LANGUAGE FILTERS=====');
                    // console.log(window.Sirv.viewer.filters);

                });
        	}
            	window.Sirv.on("spin:ready",function(){
                    // if($("div").hasClass("smv-slides-box")) {
                    // 	$("div.smv").addClass("smv-selectors-bottom");
                    // 	var viewerElement = document.querySelector("div.smv-slides-box");
                    // 	var exclusiveTagElement = document.querySelector("div.flag-container");
                    // 	var thumbnailElement = document.querySelector("div.smv-thumbnails");
                    // 	if(null !== exclusiveTagElement){
                    //   	  viewerElement.insertAdjacentElement("afterend", exclusiveTagElement);
                    //   	$(".mz-productimages-main").removeClass("without-thumbnails").addClass("withExclusiveTag");
                    //   	}
                    // 	if(null === thumbnailElement){
                    // 		$(".mz-productimages-main").addClass("without-thumbnails");
                    // 	}
                    // 	$(".flag-container").removeClass("hidden");
                    // }
                });

     
    	   Backbone.MozuView.prototype.render.apply(this, arguments);
            $("#sirvComponents").removeClass("hidden");
            this.sliderFunction();
        }
    });


    return {
        ProductPageImagesView: ProductPageImagesView
    };

});