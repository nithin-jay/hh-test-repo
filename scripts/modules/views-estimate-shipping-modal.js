define([
    'modules/jquery-mozu',
    'modules/backbone-mozu',
    'modules/models-postal-code-checker',
    'modules/models-oversize-charge-calculator',
    'modules/models-shipping-time-lines',
    'bignumber'
], function ($, Backbone, PostalCodeValidatorModel, OversizeChargeCalculatorModel, ShippingTimelineModel, BigNumber) {
    var postalCodeValidatorModel = new PostalCodeValidatorModel(),
        oversizeChargeCalculatorModel = new OversizeChargeCalculatorModel(),

        EstimateShippingModalView = Backbone.MozuView.extend({
            templateName: 'modules/common/estimate-shipping-modal',
            additionalEvents: {
                "input .postal-code-input": "onPostalCodeEnter"
            },
            initialize: function () {
                this.getOverSizeConfiguration();
            },
            render: function () {
                var locale = require.mozuData('apicontext').headers['x-vol-locale'];
                this.model.set("currentLocale", locale, {silent: true});
                Backbone.MozuView.prototype.render.apply(this, arguments);
                this.calculateWidth();
            },
            calculateWidth: function () {
                var containerWidth = $('#shipToHomeContainer').width();
                this.$el.find('.update-shipping-postal-code').width(containerWidth);
            },
            getProductPrice: function () {
                var estimated_productPrice = this.model.get('productPrice'),
                estimated_price = estimated_productPrice.salePrice ? estimated_productPrice.salePrice : estimated_productPrice.price;
                this.model.set('estimated_price', estimated_price * this.model.get('quantity'));
            },
            getOverSizeConfiguration: function () {
                var self = this,
                    shippingData = self.model.get('shippingData');
                oversizeChargeCalculatorModel.getOversizeConfigurations().then(function (res) {
                    self.model.set('overSizeConfig', res);
                    if (shippingData.status === 'Valid') {
                        self.getProductPrice();
                        self.successHandler(shippingData.postalCode, false);
                    } else {
                        self.errorHandler('Postal code not allowed', shippingData.postalCode);
                    }
                }, function (err) {
                    console.log('Error while getting oversize config.', err);
                });
            },
            closeShippingModal: function () {
                this.$el.find('.update-shipping-postal-code').addClass('hidden');
                var data = {
                    shippingData: this.model.get('shippingData'),
                    shippingTimeline: this.model.get('sthShippingTimeline')
                };
                if (this.model.get('shippingData')) $(document).trigger('refreshPdp', data);
            },
            onPostalCodeEnter: function (event) {
                var userEnteredPostalCode = $(event.currentTarget).val().trim().toUpperCase();
                this.model.set('userEnteredPostalCode', userEnteredPostalCode);
            },
            updatePostalCode: function () {
                var self = this,
                    postalCode = this.model.get('userEnteredPostalCode') ? this.model.get('userEnteredPostalCode') : this.$el.find('.postal-code-input').val().trim();
                this.$el.find('.update-button').addClass('is-loading');
                if (postalCode) {
                    postalCodeValidatorModel.checkPostalCode(postalCode).then(function (res) {
                        self.successHandler(postalCode, true);
                    }, function (error) {
                        console.log(error);
                        self.errorHandler(error, postalCode);
                    });
                } else {
                    self.errorHandler('Invalid Postal Code');
                }
            },
            successHandler: function (postalCode, isPostalCodeUpdated) {
                var shippingCostWithoutOverSizeCharge = this.calculateShippingCost(),
                    isProductOverSize = this.checkIfProductOverSize(),
                    finalShippingCost = isProductOverSize ? this.getFinalShippingCost(shippingCostWithoutOverSizeCharge) : shippingCostWithoutOverSizeCharge;
                this.model.set('shippingData', {postalCode: postalCode.trim(), status: 'Valid'});
                this.model.set('isOversizeProduct', isProductOverSize);
                this.model.set('shippingCost', finalShippingCost);
                this.model.unset('invalidPostalCode');
                this.model.unset('errorCase');
                this.render();
                if(isPostalCodeUpdated){
                    this.getShippingTimeline(postalCode.trim());
                } else {
                    this.$el.find('.content-loading').addClass('hidden');
                }
            },
            getFinalShippingCost: function (shippingCostWithoutOverSizeCharge) {
                var overSizeConfig = this.model.get('overSizeConfig'),
                    overSizeCharge = overSizeConfig.oversizeLimit.surcharge * this.model.get('quantity'),
                    oversizeShippingCost = parseFloat(shippingCostWithoutOverSizeCharge) + overSizeCharge;
                this.model.set('overSizeCharge', overSizeCharge);
                return oversizeShippingCost;
            },
            checkIfProductOverSize: function () {
                var overSizeConfig = this.model.get('overSizeConfig'),
                    productMeasurements = this.model.get('productMeasurements'),
                    isOversizeDimension = false;
                if (overSizeConfig && overSizeConfig.oversizeLimit) {
                    isOversizeDimension = oversizeChargeCalculatorModel.isOversizeDimension(productMeasurements.packageHeight, overSizeConfig.oversizeLimit.height);
                    isOversizeDimension =
                        isOversizeDimension || oversizeChargeCalculatorModel.isOversizeDimension(productMeasurements.packageWidth, overSizeConfig.oversizeLimit.width);
                    isOversizeDimension =
                        isOversizeDimension || oversizeChargeCalculatorModel.isOversizeDimension(productMeasurements.packageLength, overSizeConfig.oversizeLimit.length);
                }
                return isOversizeDimension;
            },
            calculateShippingCost: function () {
                var productPrice = this.model.get('productPrice'),
                    price = productPrice.salePrice ? productPrice.salePrice : productPrice.price,
                    priceWithEHF = parseFloat(BigNumber.sum(price.toFixed(2), this.model.get('ehfValue')).toFixed(2)) * this.model.get('quantity'),
                    shippingCost = parseFloat(new BigNumber(priceWithEHF).multipliedBy(0.1)).toFixed(2);
                this.model.set('shippingCostWithoutSurcharge', shippingCost);
                return shippingCost;
            },
            errorHandler: function (error, postalCode) {
                var errorMessage = error;
                switch (errorMessage) {
                    case 'Invalid Postal Code':
                        this.model.set("errorCase", "invalidFormat");
                        this.model.set('invalidPostalCode', postalCode);
                        break;
                    case 'Postal code not allowed':
                        this.model.set('shippingData', {postalCode: postalCode, status: 'Invalid'});
                        this.model.unset("errorCase", "invalidFormat");
                        this.model.unset('invalidPostalCode', postalCode);
                        break;
                    case 'Error while checking postal code':
                        this.model.set("errorCase", "serverError");
                        break;
                }
                this.render();
                this.$el.find('.content-loading').addClass('hidden');
            },
            getShippingTimeline: function (postalCode) {
                var self = this,
                    shippingModel = new ShippingTimelineModel(),
                    wareHouseInventory = this.model.get('wareHouseInventory'),
                    quantity = this.model.get('quantity');
                shippingModel.getShippingTimeline(postalCode, wareHouseInventory, quantity).then(function (timelineData) {
                    console.log('getShippingTimeline', timelineData);
                    self.model.set('sthShippingTimeline', timelineData.shippingTimeline);
                    self.$el.find('.content-loading').addClass('hidden');
                }, function (error) {
                    console.log('Error while getting shipping Timelines', error);
                    self.$el.find('.content-loading').addClass('hidden');
                });
            }
        });

    return EstimateShippingModalView;
});