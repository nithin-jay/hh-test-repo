define([
    'modules/jquery-mozu', 
    'underscore',
    'hyprlive', 
    'modules/api',
    'modules/backbone-mozu',
    'hyprlivecontext',
    'vendor/timezonesupport/timezonesupport',
    'slick'   
    
],
function ($, _, Hypr, api, Backbone, HyprLiveContext, timeZoneSupport) {
	
	var apiContext = require.mozuData('apicontext'),
	pageContext = require.mozuData('pagecontext'),
    contextSiteId = apiContext.headers["x-vol-site"],
    homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId"),
	currentCategoryFlow,productImpression={}, productImpressionArray=[],
	nonStaticPages = ['home', 'search-results','my-account','signup', 'no-search-results', 'category', 'product', 'store-details', 'store-locator', 'wishlist', 'cart', 'checkout', 'confirmation'],
    isHomeFurnitureSite = false, sorting = [];	
	
	var CertonaRecommendationsView = Backbone.MozuView.extend({
		templateName: 'Widgets/cms/certona-recommended-products',
		getCertonaScheme: function(scheme) {
			var currentItems = _.find(window.certonaHome.resonance.schemes, function(object){
				return object.scheme === scheme;
			});
			var queryStringCertona = '';
			if(currentItems){
				currentItems.items.forEach(function(item,index){
	              if(index != currentItems.items.length-1){
	            	  queryStringCertona += "productCode eq "+item.ID+" or ";
	              }else{
	            	  queryStringCertona += "productCode eq "+item.ID;
	              }
	              sorting.push(item.ID);
	            });
			}
			return queryStringCertona;
		},
		getParentCategory: function(parentCategoryId, currentCategoryId, allCategories, impression){
			var parentCategoryData = [],me=this;
			allCategories = JSON.parse(sessionStorage.getItem('allCategories'));
            _.find(allCategories,function(val){
				if(val.categoryId === currentCategoryId){
        	    	var parentCategoryName = val.content.name;
        	    	currentCategoryFlow = parentCategoryName + '/' + currentCategoryFlow;
        	    	if(parentCategoryId != val.parentCategory.categoryId){
        	    		//if current category is not NEW LBM And Hardlines then do the same process for current category
        	    		me.getParentCategory(parentCategoryId, val.parentCategory.categoryId, allCategories, impression);
        	    	}else{
        	    		//if current category is NEW LBM And Hardlines then do stop the process and set the ccategory flow in model
	                  	var price = $(impression).find('.mz-price.is-saleprice').length>0 ? $(impression).find('.mz-price.is-saleprice').text() : $(impression).find('.mz-price').text();
	                  	if(price && price.indexOf('/') > 0) {
                      	  price = price.split('/')[0];
                        }
	                  	var listAttr = '';
						if($(impression).find('.mz-productlisting-title') && $(impression).find('.mz-productlisting-title').length>0) {
                      	  listAttr = $(impression).find('.mz-productlisting-title').attr('href').split('?page=')[1];
                      	  if(listAttr && listAttr.indexOf('#ccode') > 0){
                      		  listAttr = listAttr.split('#ccode')[0];
                      	  }
                        }else if($(impression).find('.product-title') && $(impression).find('.product-title').length>0) { //for related products
                      	  listAttr = $(impression).find('.product-title').children().attr('href').split('?page=')[1];
                        }else if($(impression).find('.productTile-title-link') && $(impression).find('.productTile-title-link').length>0){ //for image slider widget
                        	listAttr = $(impression).find('.productTile-title-link').attr('href').split('?page=')[1];
                        }
                       var nonStaticPageListAttr = "";
                       _.find(nonStaticPages,function(pagename){
                            if(pageContext.cmsContext.template.path === pagename){
                                nonStaticPageListAttr = listAttr;
                            }
                        });

                        if(!nonStaticPageListAttr){
                            var pageHeading = "";

                            if($('body').find('h1.title-for-print').length !== 0){
                               pageHeading = $('body').find('h1.title-for-print').text();
                           }else if($('body').find("h1.main-page-title").length !== 0){
                               pageHeading = $('body').find("h1.main-page-title").text(); 
                           }else{
                               pageHeading = pageContext.cmsContext.page.path;
                           }
                            nonStaticPageListAttr = pageHeading+ ' - ' + listAttr.substr(listAttr.indexOf('page-')+5);
                        }
                        
                        if(nonStaticPageListAttr) {
                        	if(nonStaticPageListAttr.indexOf('&rrec=true') !== -1){
                        		nonStaticPageListAttr = nonStaticPageListAttr.split('&rrec=true')[0];
                        	}
                        }
	                  	productImpression = {
		                  	'name': $(impression).attr('data-mz-productname'),
		                  	'id' : $(impression).attr('data-mz-product'),
		                  	'price' : $.trim(price.replace('$', '').replace(',', '.')),
		                  	'brand' : $.trim($(impression).find('.brand-name').text()),
		                  	'category' : currentCategoryFlow,
		                  	'position' : $(impression).attr('data-mz-position'),
		                  	'list' : nonStaticPageListAttr
	                    };
	                    productImpressionArray.push(productImpression);
        	    	}
				}
            });
        },
		getProductsAndShowWidgets: function(queryStringCertona, currentSceme, widgetDisplayedCount) {
			var me = this;
			if(queryStringCertona) {
				api.get("search",{filter: queryStringCertona}).then(function(certonaResponse){
					var newData = [];
					var today = new Date();
					var torontoTimeZone = timeZoneSupport.findTimeZone('America/Toronto');
					if(certonaResponse && certonaResponse.data) {
						var newSortingArray= sorting.filter(function(item) {
							return certonaResponse.data.items.some(function(certonaItem) { return item === certonaItem.productCode; });
						});
						if(newSortingArray.length > 0){
							_.each(certonaResponse.data.items, function(item){
								var promoItemZonedTime = timeZoneSupport.getZonedTime(new Date(item.updateDate), torontoTimeZone);
								var promoItemDate = new Date(promoItemZonedTime.year, promoItemZonedTime.month-1, promoItemZonedTime.day, promoItemZonedTime.hours, promoItemZonedTime.minutes, promoItemZonedTime.seconds, promoItemZonedTime.milliseconds);
								if(today <= promoItemDate){
									item.isPromoEnabled = true;
									item.promoTillDate = me.formatDate(promoItemDate);
								}
								newData[newSortingArray.indexOf(item.productCode)] = item;
							});
						}
						me.model.set('items', newData);
						me.model.set('currentSceme', currentSceme); 
						me.model.set("isWidget",true);
						me.model.set("isCertonaProduct", true);
						var promoName = $(".certona-regular-"+currentSceme).parent() ? $(".certona-regular-"+currentSceme).parent().attr('data-promo-name') : '';
						me.model.set('promoName', promoName);
						me.setElement($(".certona-regular-"+currentSceme));
						me.render();
						if(newSortingArray.length > 0){
							var explaination="";
							var selectedScheme = _.find(window.certonaHome.resonance.schemes, function(scheme){
								return scheme.scheme === currentSceme;
							});
							if(selectedScheme) {explaination = selectedScheme.explanation;}
							if(currentSceme !== 'addtocart1_rr' && currentSceme !== 'oos_rr' && currentSceme !== 'pla_rr' && currentSceme !== 'category_rr' && currentSceme !== 'search_rr') {
								setTimeout(function(currentSceme) {
									var productImpressions = $(".certona-regular-"+currentSceme).find('.ign-data-product-impression');
									if(productImpressions && productImpressions.length > 0) {
										window.sendGtmDataLayer(productImpressions, true);
									}
								}, 2500, currentSceme); //pass the currentScheme to setTimeout function
								$(".certona-title-"+currentSceme).text(explaination);
								$(".certona-regular-"+currentSceme).removeClass('hidden');
								$(".certona-regular-"+currentSceme).not('.slick-initialized').slick({ 
									 dots: false,
									 infinite: false,
									 slidesToShow: 4,
									 slidesToScroll: 4,
									 variableWidth:true,
									 responsive: [
									  {
										breakpoint: 1024,
										settings: {
										  slidesToShow: 4,
										  slidesToScroll: 4
										}
									  },
									  {
										breakpoint: 600,
										settings: {
										  infinite: true,
										  arrows: false,
										  slidesToShow: 1,
										  slidesToScroll: 1
										}
									  }
									]
								 });
							} else if(currentSceme === 'oos_rr') {
								setTimeout(function(currentSceme) {
									var productImpressions = $(".certona-regular-"+currentSceme).find('.ign-data-product-impression');
									if(productImpressions && productImpressions.length > 0) {
										window.sendGtmDataLayer(productImpressions, true);
									}
								}, 2500, currentSceme); //pass the currentScheme to setTimeout function
								$(".certona-title-"+currentSceme).text(explaination);
								$(".certona-regular-"+currentSceme).removeClass('hidden');
								$(".certona-regular-"+currentSceme).not('.slick-initialized').slick({ 
									 dots: false,
									 infinite: false,
									 slidesToShow: 3,
									 slidesToScroll: 3,
									 variableWidth:true,
									 responsive: [
										 {
										breakpoint: 1440,
										settings: {
										  slidesToShow: 2,
										  slidesToScroll: 2
										}
									  },
									  {
										breakpoint: 1024,
										settings: {
										  slidesToShow: 2,
										  slidesToScroll: 2
										}
									  },
									  {
										breakpoint: 600,
										settings: {
										  infinite: true,
										  arrows: false,
										  slidesToShow: 1,
										  slidesToScroll: 1
										}
									  }
									]
								 });
							} else if(currentSceme === 'category_rr' || currentSceme === 'search_rr') {							
								setTimeout(function(currentSceme) {
									var productImpressions = $(".certona-regular-"+currentSceme).find('.ign-data-product-impression');
									var allCategories = JSON.parse(sessionStorage.getItem('allCategories'));
									if(productImpressions && productImpressions.length > 0) {
										if(require.mozuData('pagecontext').pageType === 'search' || require.mozuData('pagecontext').pageType === 'category') {
											//send datalayer
											if(productImpressions && productImpressions.length > 0) {
												// Product impressions can be sent on the pageview event.
												
												var parentCategoryId = Hypr.getThemeSetting('newLBMAndHardlinesCategoryId');
												
												_.each(productImpressions, function(impression){
													var productCategory;
													if($(impression).attr('data-mz-categoryId')){
														productCategory = parseInt($(impression).attr('data-mz-categoryId').split('|')[0], 10);
													}
													_.find(allCategories,function(val){
														if(val.categoryId === productCategory){
														  var currentCategoryName = val.content.name;
														  var currentParentId = parseInt($(impression).attr('data-mz-categoryId').split('|')[1], 10);
														  currentCategoryFlow = currentCategoryName;
														  if(parentCategoryId != currentParentId){
															//if current category is not NEW LBM And Hardlines then do the same process for current category
															me.getParentCategory(parentCategoryId, currentParentId, allCategories, impression);
														  }else {
															var price = $(impression).find('.mz-price.is-saleprice').length>0 ? $(impression).find('.mz-price.is-saleprice').text() : $(impression).find('.mz-price').text();
															if(price && price.indexOf('/') > 0) {
																price = price.split('/')[0];
															}
															var listAttr = '';
															if($(impression).find('.mz-productlisting-title') && $(impression).find('.mz-productlisting-title').length>0) {
																listAttr = $(impression).find('.mz-productlisting-title').attr('href').split('?page=')[1];
																if(listAttr.indexOf('#ccode') > 0){
																	listAttr = listAttr.split('#ccode')[0];
																}
															}else if($(impression).find('.product-title') && $(impression).find('.product-title').length>0) { //for related products
																listAttr = $(impression).find('.product-title').children().attr('href').split('?page=')[1];
															}else if($(impression).find('.productTile-title-link') && $(impression).find('.productTile-title-link').length>0){ //for image slider widget
																listAttr = $(impression).find('.productTile-title-link').attr('href').split('?page=')[1];
															}
															
															var pagecontext = require.mozuData('pagecontext');
															var nonStaticPageListAttr = "";
															_.find(nonStaticPages,function(pagename){
																 if(pagecontext.cmsContext.template.path === pagename){
																	 nonStaticPageListAttr = listAttr;
																 }
															 });
									 
															 if(!nonStaticPageListAttr){
																var pageHeading = "";
									
																if($('body').find('h1.title-for-print').length !== 0){
																   pageHeading = $('body').find('h1.title-for-print').text();
															   }else if($('body').find("h1.main-page-title").length !== 0){
																   pageHeading = $('body').find("h1.main-page-title").text(); 
															   }else{
																   pageHeading = pagecontext.cmsContext.page.path;
															   }
																 nonStaticPageListAttr = pageHeading + ' - ' + listAttr.substr(listAttr.indexOf('page-')+5);
															 }
															
															
															productImpression = {
															  'name': $(impression).attr('data-mz-productname'),
															  'id' : $(impression).attr('data-mz-product'),
															  'price' : $.trim(price.replace('$', '').replace(',', '.')),
															  'brand' : $.trim($(impression).find('.brand-name').text()),
															  'category' : currentCategoryFlow,
															  'position' : $(impression).attr('data-mz-position'),
															  'list' : nonStaticPageListAttr
															};
															productImpressionArray.push(productImpression);
														  }
														}
													 });
												});
												dataLayer.push({ 
													'event': 'impressionview',
													'ecommerce': {
														'currencyCode': 'CAD',
														'impressions': productImpressionArray
													}
												});
											}
										} else {
											window.sendGtmDataLayer(productImpressions, true);
										}
									}
								}, 3000, currentSceme); //pass the currentScheme to setTimeout function
								$(".certona-title-"+currentSceme).text(explaination);
								$(".certona-regular-"+currentSceme).removeClass('hidden');
								$(".certona-regular-"+currentSceme).not('.slick-initialized').slick({ 
									 dots: false,
									 infinite: false,
									 slidesToShow: 3,
									 slidesToScroll: 3,
									 variableWidth:true,
									 responsive: [
									  {
										breakpoint: 1024,
										settings: {
										  slidesToShow: 3,
										  slidesToScroll: 3
										}
									  },
									  {
										breakpoint: 600,
										settings: {
										  infinite: true,
										  arrows: false,
										  slidesToShow: 1,
										  slidesToScroll: 1
										}
									  }
									]
								 });
							} else if(currentSceme === 'pla_rr') { 
								setTimeout(function(currentSceme) {
									var productImpressions = $(".certona-regular-"+currentSceme).find('.ign-data-product-impression');
									if(productImpressions && productImpressions.length > 0) {
										window.sendGtmDataLayer(productImpressions, true);
									}
								}, 2500, currentSceme);
								 //pass the currentScheme to setTimeout function
								$(".certona-title-"+currentSceme).text(explaination);
								$(".certona-regular-"+currentSceme).removeClass('hidden');
								$(".certona-regular-"+currentSceme).not('.slick-initialized').slick({ 
									 dots: false,
									 infinite: false,
									 slidesToShow: 6,
									 slidesToScroll: 6,
									 variableWidth:true,
									 responsive: [
										 {
										breakpoint: 1440,
										settings: {
										  slidesToShow: 6,
										  slidesToScroll: 6
										}
									  },
									  {
										breakpoint: 1025,
										settings: {
										  slidesToShow: 4,
										  slidesToScroll: 4
										}
									  },
									  {
										breakpoint: 600,
										settings: {
										  infinite: true,
										  arrows: false,
										  slidesToShow: 1,
										  slidesToScroll: 1
										}
									  }
									]
								 });
								$(".certona-container").find('.brand-name').addClass('add-ellipsis');
							}
							  //Slider End
	
							  $(".certona-container").find('.mz-productlisting-title').addClass('add-ellipsis');
							if(currentSceme === 'product4_rr') {
								var firstCheckedBox = $('#compareproducts').find('.mz-productlisting:first .compare-checkbox');
								if (firstCheckedBox) {
									firstCheckedBox.prop("checked", true);
								}  
							}
						}else {
							$(".certona-regular-"+currentSceme).closest('div[id^=mz-drop-zone-certona]').addClass('hidden');
						}
						
						widgetDisplayedCount--;
						sorting = [];
						if(widgetDisplayedCount !== 0){
							currentSceme = $($('.certona-products-container')[widgetDisplayedCount-1]).data('scheme-name');
							queryStringCertona = me.getCertonaScheme(currentSceme);
							me.getProductsAndShowWidgets(queryStringCertona, currentSceme, widgetDisplayedCount);
						}
					}
				});
			} else {
				widgetDisplayedCount--;
				sorting = [];
				if(widgetDisplayedCount !== 0){
					currentSceme = $($('.certona-products-container')[widgetDisplayedCount-1]).data('scheme-name');
					queryStringCertona = me.getCertonaScheme(currentSceme);
					me.getProductsAndShowWidgets(queryStringCertona, currentSceme, widgetDisplayedCount);
				}
			}
		},
		formatDate: function(date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();
	
			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;
	
			return [year, month, day].join('-');
		},
		initialize: function() {
			var me = this;
			var locale = require.mozuData('apicontext').headers['x-vol-locale'];
			var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
			me.model.set('currentLocale', locale);
			me.model.set('currentSite', currentSite);
			var currentSceme;
			var certonaSchemeCount = $('.certona-products-container').length;
			var isMobile = navigator.userAgent.match(/(iPhone)|(iPod)|(android)|(webOS)/i);
			console.log("********Certona Response***********");
			console.log(window.certonaHome);
			var queryStringCertona = "", currentScheme = "", sorting = [], widgetDisplayedCount = 1;
			if(certonaSchemeCount > 1){
				widgetDisplayedCount = certonaSchemeCount;
				currentSceme = $($('.certona-products-container')[certonaSchemeCount-1]).data('scheme-name');
				queryStringCertona = me.getCertonaScheme(currentSceme);
			}else{
				if(certonaSchemeCount === 1) {
					currentSceme = $('.certona-products-container').data('scheme-name');
					queryStringCertona = me.getCertonaScheme(currentSceme);
				}
			}
			if(pageContext.cmsContext.template.path === "product" && window.certonaHome.resonance.schemes){
				_.each(window.certonaHome.resonance.schemes,function(certonaScheme){
					if(certonaScheme.scheme === "product3_rr" && certonaScheme.display === 'yes'){
					$("#frequentlyBought").find(".mainTitle").text(certonaScheme.explanation);
					if(!isMobile){
						$("#frequentlyBought").find(".mainTitle").parent().addClass('product-tab-header');	
					}
					if(!certonaScheme.explanation) {
						$("#frequentlyBought").addClass("hidden");
						$("#frequentlyBought").parent().addClass("no-border");
					}
				} else {
					if(certonaScheme.scheme === "product3_rr" && certonaScheme.display === 'no') {
						$("#frequentlyBought").addClass("hidden");
						$("#frequentlyBought").parent().addClass("no-border");
					}
				}
				if(certonaScheme.scheme === "product2_rr" && certonaScheme.display === 'yes'){
					$("#morelikethis").find(".mainTitle").text(certonaScheme.explanation);
					if(!isMobile){
						$("#morelikethis").find(".mainTitle").parent().addClass('product-tab-header');	
					}
					if(!certonaScheme.explanation) {
						$("#morelikethis").addClass("hidden");
						$("#morelikethis").parent().addClass("no-border");
					}
				} else {
					if(certonaScheme.scheme === "product2_rr" && certonaScheme.display === 'no'){
						$("#morelikethis").addClass("hidden");
						$("#morelikethis").parent().addClass("no-border");
					}
				}
				if(certonaScheme.scheme === "product4_rr" && certonaScheme.display === 'yes'){
					$("#compareproducts").find(".mainTitle").text(certonaScheme.explanation);
					if(!isMobile){
						$("#compareproducts").find(".mainTitle").parent().addClass('product-tab-header');	
					}
				} else {
					if(certonaScheme.scheme === "product2_rr" && certonaScheme.display === 'no'){
						$("#compareproducts").addClass("hidden");
						$("#compareproducts").parent().addClass("no-border");
					}
				}
				if(certonaScheme.scheme === "product1_rr" && certonaScheme.display === 'yes'){
					$("#recentlyViewed").find(".mainTitle").text(certonaScheme.explanation);
					if(!isMobile){
						$("#recentlyViewed").find(".mainTitle").parent().addClass('product-tab-header');	
					} 
					$("#recentlyViewed").removeClass("hidden");
					if(!certonaScheme.explanation) {
						$("#recentlyViewed").addClass("hidden");
						$("#recentlyViewed").parent().addClass("no-border");
					}
				}else{
					if(certonaScheme.scheme === "product1_rr" && certonaScheme.display === 'no'){
						$("#recentlyViewed").addClass("hidden");
						$("#recentlyViewed").parent().addClass("no-border");
					}
				}
					if(certonaScheme.scheme === "pla_rr" && certonaScheme.display === 'yes'){
						$(".pla-mobile-container").find(".mainTitle").text(certonaScheme.explanation);
					}
				});
			}
			me.getProductsAndShowWidgets(queryStringCertona, currentSceme, widgetDisplayedCount);
			$('#product-certona-container').addClass('hidden');
		}

	});
	if (!certonaRecommendations) {
		var certonaRecommendations = window.certonaRecommendations = function (res) {
			window.certonaHome = res;
			var certonaRecommendationsView = new CertonaRecommendationsView({
		        model: new Backbone.Model()
		    });
			certonaRecommendationsView.render();
		};
	}
});
