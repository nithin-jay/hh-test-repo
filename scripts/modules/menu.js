require([
    'modules/jquery-mozu', 
    'hyprlive', 
    'underscore', 
    'modules/api', 
    'modules/backbone-mozu',
    'pages/mystore-details',
    'modules/models-customer'
], function ($, Hypr, _, api, Backbone, StoreViews,CustomerModels) {
    var pageContext = require.mozuData('pagecontext');
    $(document).ready(function() {
        // $('.dropdown-open').on('click', function(e) {
        //     e.preventDefault();
        //     if($(window).width() < 768) {
        //         $('.primary-menu-nav').toggle();
        //         $(this).attr('aria-expanded', function (i, attr) {
        //             return attr == 'true' ? 'false' : 'true';
        //         }); 
        //     }
        // });

        $('#search-trigger-mobile').on('click', function() {
            console.log('toggle mobile search');
            $('#search-wrapper-mobile').toggle();
            $('.nav-search-icon .fa-times').toggle();
            $('.nav-search-icon .fa-search').toggle();
        });
        
        $('.my-store-nav').on('click', function() {
            // console.log('clicked');
            if( $(window).width() < 768 ) {
                console.log('mobile');
                $('.store-dropdown--content').toggle();
                $('.store-dropdown--button .fa-times').toggle();
                $('.store-dropdown--button .fa-map-marker-alt').toggle();
            }
        });

        // var closeSearchBox = function() {
        // 	$('.mz-searchbox-input, .search-close').css({'display': 'none'}); 
        // };

    //    var toggleMenu = function() {
    //         $("#toggle-menu").toggleClass('mobilemenuclose');
    //         $("#toggle-menu").toggleClass('mobilemenutoggle');
    //     };
    //     var lockMobileBodyOnActiveMenu = function(){
    //     	if(!$('body').hasClass('is-locked')){
    //     		$('body').addClass('is-locked');
    //     	}else {
    //     		$('body').removeClass('is-locked');
    //     	}
    //     	if(!$('html').hasClass('is-locked')){
    //     		$('html').addClass('is-locked');
    //     	}else {
    //     		$('html').removeClass('is-locked');
    //     	}
    //     };
    //     $('.mobilemenutoggle').on('click', function() {
    //     	lockMobileBodyOnActiveMenu();
    //         toggleMenu();
    //         $('.mz-sitenav').toggleClass('top-menu');
    //         $('.mz-pageheader').removeClass('is-search-open');
    //         $('.mystore-details').removeClass('in');
    //         $(".sitenav-containar").addClass('add-border');
    //         $('.nav-search-icon').hide();
    //         $("#mobile-search #searchbox").toggleClass('hidden');
    //         $('.mz-navright-list').toggleClass('menu-open');
    //         $('.navbar-collapse').scrollTop(0);
    //         closeSearchBox();
    //     }); 
        
    //     $('.mobilemenuclose').on('click', function() {
    //         toggleMenu();
    //         $('.mz-sitenav').toggleClass('top-menu');
    //         $('.mz-pageheader').removeClass('is-search-open');
    //         $('.mystore-details').removeClass('in');
    //         $('.nav-search-icon').hide(); 
    //         $("#mobile-search #searchbox").toggleClass('hidden');
    //         $("#mobile-search #searchbox").addClass("active");
    //         $('.mz-navright-list').toggleClass('menu-open');
    //         closeSearchBox();
    //     });

    //     var truncateName = function (classname,charcount){
    //         $(classname).each(function(){
    //             var subsubName = $(this).text();
    //             var truncatedstr = subsubName;
    //             if(subsubName.length > charcount && $(window).width() > 767){
    //                 truncatedstr = subsubName.slice(0,charcount) + '...';
    //                 $(this).text(truncatedstr);
    //             }
    //             else{
    //                 truncatedstr = subsubName;
    //                 $(this).text(truncatedstr);
    //             }
    //         });
    //     };
    //     truncateName(".desk-subsublink",25);
    //     truncateName(".desk-subsubsublink",30);
    //     truncateName(".nav-more-link",23);


    //     var DesktopPanelLessThan35Links = Hypr.getThemeSetting('Desktop-panel-less-than-35-links');
    //     var DesktopContaintLessThan35Links = Hypr.getThemeSetting('Desktop-containt-less-than-35-links');
    //     var DesktopPanelGreaterThan35LessThan70Links = Hypr.getThemeSetting('Desktop-panel-greater-than-35-less-than-70-links');
    //     var DesktopContaintGreaterThan35LessThan70Links = Hypr.getThemeSetting('Desktop-containt-greater-than-35-less-than-70-links');
    //     var DesktopPanelGreaterThan70Links = Hypr.getThemeSetting('Desktop-panel-greater-than-70-links');
    //     var DesktopContaintGreaterThan70Links = Hypr.getThemeSetting('Desktop-containt-greater-than-70-links');
    //     var SmallDesktopPanelLessThan35Links = Hypr.getThemeSetting('SmallDesktop-panel-less-than-35-links');
    //     var SmallDesktopContaintLessThan35Links = Hypr.getThemeSetting('SmallDesktop-containt-less-than-35-links');
    //     var SmallDesktopPanelGreaterThan35LessThan70Links = Hypr.getThemeSetting('SmallDesktop-panel-greater-than-35-less-than-70-links');
    //     var SmallDesktopContaintGreaterThan35LessThan70Links = Hypr.getThemeSetting('SmallDesktop-containt-greater-than-35-less-than-70-links');
    //     var SmallDesktopPanelGreaterThan70Links = Hypr.getThemeSetting('SmallDesktop-panel-greater-than-70-links');
    //     var SmallDesktopContaintGreaterThan70Links = Hypr.getThemeSetting('SmallDesktop-containt-greater-than-70-links');


    //     var desktopPanelHeight = function(e){
    //         var linkcount = $(e.currentTarget).find(".linkcount").length;
    //         if(linkcount < 35){
    //             $(".primary-menu-tabs").height(DesktopPanelLessThan35Links);
    //             $(e.currentTarget).find(".subsubsublinks-contents").height(DesktopContaintLessThan35Links);
    //         }
    //         else if(linkcount > 35){
    //             if(linkcount > 70){
    //                 $(".primary-menu-tabs").height(DesktopPanelGreaterThan70Links);
    //                 $(e.currentTarget).find(".subsubsublinks-contents").height(DesktopContaintGreaterThan70Links);
    //             }
    //             else{
    //                 $(".primary-menu-tabs").height(DesktopPanelGreaterThan35LessThan70Links);
    //                 $(e.currentTarget).find(".subsubsublinks-contents").height(DesktopContaintGreaterThan35LessThan70Links);
    //             }
    //         }
    //         $(e.currentTarget).find(".subsubsublinks-contents").css({"column-fill":"auto"});
    //     };

    //     var smallDesktopPanelHeight = function(e){
    //         var linkcount = $(e.currentTarget).find(".linkcount").length;
    //         if(linkcount < 35){
    //             $(".primary-menu-tabs").height(SmallDesktopPanelLessThan35Links);
    //             $(e.currentTarget).find(".subsubsublinks-contents").height(SmallDesktopContaintLessThan35Links);
    //         }
    //         else if(linkcount > 35){
    //             if(linkcount > 70){
    //                 $(".primary-menu-tabs").height(SmallDesktopPanelGreaterThan70Links);
    //                 $(e.currentTarget).find(".subsubsublinks-contents").height(SmallDesktopContaintGreaterThan70Links);
    //             }
    //             else{
    //                 $(".primary-menu-tabs").height(SmallDesktopPanelGreaterThan35LessThan70Links);
    //                 $(e.currentTarget).find(".subsubsublinks-contents").height(SmallDesktopContaintGreaterThan35LessThan70Links);
    //             }
    //         }
    //         $(e.currentTarget).find(".subsubsublinks-contents").addClass("autocolumnfill");
    //     };

    //     var tablatePanelHeight = function(e){
    //         $(".primary-menu-nav").removeClass("remove-bg");
    //         $(".primary-menu-tabs").css({'min-height':'unset'});
    //         var tabheight = $(".primary-menu-tabs").height();
    //         var subsubcontentHeight = $(e.currentTarget).find(".subsubsublinks-contents").height();
    //         if(tabheight - 84 > subsubcontentHeight){
    //             $(e.currentTarget).find(".subsubsublinks-contents").addClass("autocolumnfill");
    //             $(e.currentTarget).find(".subsubsublinks-contents").height(tabheight - 84);
    //         }
    //         else{
    //             $(e.currentTarget).find(".subsubsublinks-contents").addClass("unsetcolumnfill");
    //         }
    //         var menupanelheight = $(e.currentTarget).find(".subsubsublinks-contents").height();
    //         $(e.currentTarget).find(".desktop-menu-panel").css({'min-height':menupanelheight});
    //         $(".primary-menu-tabs").css({'min-height':menupanelheight + 84});
    //     };

    //     $(".noneditor-menu-tab").hover(function(e){
    //         if($(window).width() > 767){
    //             if($(e.currentTarget).find(".mz-sitenav-sublink").hasClass( "first-category" )){
    //                 $(".primary-menu-panel").css('display','none');
    //                 $(e.currentTarget).find(".mz-sitenav-sublink").addClass("selected");
    //             }
    //             else{
    //                 $(e.currentTarget).find(".primary-menu-panel").css('display','block');
    //                 $(".subsubsublinks-contents").removeClass("unsetcolumnfill");
    //                 $(".primary-menu-tabs").removeClass("autoheight-minheight");
    //                 $(".desktop-menu-panel").removeClass("autoheight-minheight");
    //                 $(".primary-menu-nav").removeClass("remove-bg"); 
    //                 $(e.currentTarget).find(".mz-sitenav-sublink").addClass("selected");
    //                 $(".noneditor-item-link").addClass("active");
    //             }
    //         }
    //         if($(window).width() > 767 && $(window).width() < 992){
    //             tablatePanelHeight(e);
    //             if($(e.currentTarget).find(".mz-sitenav-sublink").hasClass( "first-category" )){
    //                 $(".primary-menu-panel").css('display','none');
    //                 $('.primary-menu-nav').addClass('remove-bg');
    //                 $(e.currentTarget).find(".mz-sitenav-sublink").addClass("selected");
    //                 var locale = require.mozuData('apicontext').headers['x-vol-locale'];
	// 		        var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
	// 		        locale = locale.split('-')[0];
	// 		        var currentLocale = '';
	// 		        if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
	// 			        currentLocale = locale === 'fr' ? '/fr' : '/en';
	// 		        }
    //                 window.location.href=currentLocale + "/brands";
    //             }
    //         }
    //         if($(window).width() > 991 && $(window).width() < 1025){
    //             smallDesktopPanelHeight(e);
    //         }
    //         if($(window).width() > 1024){
    //             desktopPanelHeight(e);
    //         }

    //     },function(e){
    //         if($(window).width() > 767){
    //             $(e.currentTarget).find(".primary-menu-panel").css('display','none');
    //             $(".subsubsublinks-contents").addClass("unsetcolumnfill");
    //             $(e.currentTarget).find(".mz-sitenav-sublink").removeClass("selected");
    //             $(".noneditor-item-link").removeClass("active");
    //             $(".primary-menu-nav").addClass("remove-bg");
    //             $(".primary-menu-tabs").addClass("autoheight-minheight");
    //             $(".desktop-menu-panel").addClass("autoheight-minheight");
    //         }
    //     });

    //     $(".primary-menu-nav").hover(function(e){
    //         $(".dropdown-open").removeClass("product-black-text").addClass("product-red-text");
    //     },function(e){
    //         $(".dropdown-open").removeClass("product-red-text").addClass("product-black-text");
    //     });
 
    //     $(".editor-menu-tab").on('click',function(e){
    //         $(".editor-menu-tab").css('background','#f2f2f2');
    //         $(this).css('background','white');
    //         $(".subsubsublinks-contents").addClass("unsetcolumnfill");
    //         $(".mz-sitenav-sublink").removeClass("selected");
    //         $(".editor-item-link").removeClass("active");
    //         $(".primary-menu-nav").removeClass("remove-bg");
    //         $(e.currentTarget).find(".mz-sitenav-sublink").addClass("selected");
    //         $(".noneditor-item-link").addClass("active");
    //         if($(window).width() > 767 && $(window).width() < 992){
    //             tablatePanelHeight(e);
    //         }
    //         if($(window).width() > 991 && $(window).width() < 1024){
    //             smallDesktopPanelHeight(e);
    //         }
    //         if($(window).width() > 1023){
    //             desktopPanelHeight(e);
    //         }
    //     });


    //     $( window ).load(function(){
    //         if($(window).width() > 767 && $(window).width() < 992){
    //             $(".primary-menu-nav").hide();
    //             $(".dropdown-open").addClass("tab-product-color");
    //             $(".tab-product-color").addClass("tab-black");
    //         }
    //     });


        $(".editor-item-link").on('click',function(){
            $(".primary-menu-nav").toggle(); 
            $(".tab-product-color").toggleClass("tab-black tab-red");      
        });

        // $(document).on('touchstart',".tab-dropdown-open", function(e){
        //     if($(window).width() > 767){
        //         e.stopImmediatePropagation();
        //         $(".primary-menu-nav").toggle(); 
        //         $(".tab-product-color").toggleClass("tab-black tab-red");
        //     }      
        // });
         //show first category selected on home page and non-category pages 
    	// $('.primary-menu-panel .mz-sitenav-sub-sub:first-child').addClass("active");
        $('.editor-tab-link, .mz-sitenav-mobsublink').on('click', function(e) {
            if($(e.currentTarget).hasClass( "first-category" )){
                $(".primary-menu-panel").css('display','none');
            }
            else{
            e.preventDefault();
            $('.mz-sitenav-sublink').removeClass("selected");
            $('.primary-menu-panel').css({'display': 'block'}); 
            $('.primary-menu').addClass('is-primary-selected');
            $('.primary-menu-nav').addClass('top-0');
            $(this).addClass("selected");
            var category = $(this).attr('data-linkname');
            $("div.mz-sitenav-sub-sub").removeClass("active"); 
            $("[data-linkname='" + category + "']").addClass('active');
            $("div.mz-sitenav-sub-sub").removeClass("slide-submenu"); //for mobile view
            $("[data-linkname='" + category + "']").addClass( "slide-submenu" ); //for mobile view
            $(".navbar-collapse").addClass("is-category-selected");
            $(".feturedCategoriesOfMobile").css({'display': 'none'});
            $('.navbar-collapse').scrollTop(0);
            }
        });

        // $('.mz-sitenav-sub-sublink').on('click', function(e) {
        //     // e.preventDefault();
        //      $(e.currentTarget).closest('.subsubsublinks-contents').find('.sub-sub-sub-link').addClass('sub-sub-sub-active');
              
        //      if($(e.currentTarget).hasClass("third-level")){
        //          $('.primary-menu').removeClass('is-primary-selected').addClass('is-secondary-selected show-sub-sub-sub');
        //          $(e.currentTarget).closest('.mz-sitenav-section').find('.secondary-menu-pannel').css({'display': 'block'});
        //          $(".featured-content").hide();
        //      }
        //      else{
        //          $('.primary-menu').addClass('is-primary-selected').removeClass('is-secondary-selected show-sub-sub-sub');
        //      }
        //      $('.primary-menu-nav').addClass('top-0');
        //      $(e.currentTarget).closest(".mz-sitenav-section").find(".mz-sitenav-sub-sub-sub").css({'display': 'block'});
        //      $('.primary-menu-panel').addClass('remove-dropdown');
        //      $("div.mz-sitenav-sub-sub-sub").removeClass("active"); 
        //      $(".navbar-collapse").addClass("is-category-selected");
        //      $(".feturedCategoriesOfMobile").css({'display': 'none'});
        //      $('.navbar-collapse').scrollTop(0);   
        //  });

        // $('.back-link').on('click', function(e) { //for mobile view
        //     e.preventDefault();
        //     $('.primary-menu').removeClass('is-primary-selected');
        //     $(".mz-sitenav-sub-sub").removeClass( "slide-submenu" );
        //     $(".navbar-collapse").removeClass("is-category-selected");
        //     $('.primary-menu-nav').removeClass('top-0');
        //     $(".feturedCategoriesOfMobile").css({'display': 'block'});
        // });

        // $('.backtosubsubcategory').on('click', function(e) {
        //     e.preventDefault();
        //     $('.primary-menu').addClass('is-primary-selected').removeClass('is-secondary-selected');
        //     $('.primary-menu').removeClass('show-sub-sub-sub');
        //     $('.secondary-menu-pannel').css({'display': 'none'});
        //     $(".featured-content").show();
        // });
        
        // $("#search-button, #search-icon").click(function(e){
        //     e.preventDefault();
        //     $("#search-button, #search-form").show();
        // });
    });
});
