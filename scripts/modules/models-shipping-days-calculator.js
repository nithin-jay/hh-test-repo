define([
    "underscore",
    "modules/backbone-mozu",
    "hyprlive",
    "vendor/jquery/moment"
], function (_, Backbone, Hypr, moment) {

    var ShippingDaysCalculator = Backbone.MozuModel.extend({
        getShipToStoreExpectedDate: function (date) {
            var arrivalDate = date;
            arrivalDate.setDate(arrivalDate.getDate() + Hypr.getThemeSetting('ShipToStoreExpectedArrivalDays'));
            return arrivalDate;
        },
        getShipToHomeExpectedDate: function (date, businessDays) {
            var today = new Date(date),
                business_days = businessDays ? businessDays : this.getDaysToAddForShipToHome(date),
                deliveryDate = today, //will be incremented by the for loop
                total_days = business_days; //will be used by the for loop
            for (var days = 1; days <= total_days; days++) {
                deliveryDate = new Date(today.getTime() + (days * 24 * 60 * 60 * 1000));
                if (deliveryDate.getDay() === 0 || deliveryDate.getDay() == 6) {
                    //it's a weekend day so we increase the total_days of 1
                    total_days++;
                }
            }
            return deliveryDate;
        },
        getDaysToAddForShipToHome: function (dateToCalculate) {
            var timeToConsiderForSTH = Hypr.getThemeSetting('timeToConsiderForShipTOHomeExpectedArrival'),
                currentEstTime = this.getCurrentEstDate(dateToCalculate),
                endTime = moment({
                    h: parseInt(timeToConsiderForSTH.split(':')[0], 10),
                    m: parseInt(timeToConsiderForSTH.split(':')[1], 10)
                }),
                currentTime = moment({
                    h: currentEstTime.hour(),
                    m: currentEstTime.minute()
                });
            return currentTime.isBefore(endTime) ? Hypr.getThemeSetting('shipToHomeExpectedArrivalDaysBefore') : Hypr.getThemeSetting('shipToHomeExpectedArrivalDaysAfter');
        },
        getCurrentEstDate: function (dateToCalculate) {
            var date = dateToCalculate.toLocaleString("en-US", {timeZone: "America/New_York"});
            return moment(date);
        }
    });


    return ShippingDaysCalculator;

});