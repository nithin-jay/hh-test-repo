﻿/**
 * Unidirectional dispatch-driven collection views, for  your pleasure.
 */


define([
    'modules/jquery-mozu',
    'backbone',
    'underscore',
    'modules/url-dispatcher',
    'modules/intent-emitter',
    'modules/get-partial-view',
    'modules/product-quick-view',
    "modules/api",
    "modules/models-product",
    'modules/models-orders',
    'modules/recently-viewed',
    'hyprlive',
    'vendor/timezonesupport/timezonesupport',
    'modules/api'
], function($, Backbone, _, UrlDispatcher, IntentEmitter, getPartialView, quickViewBind, Api, ProductModels, 
        OrderModels, recentlyViewed, Hypr, timeZoneSupport, api) {
    
    function factory(conf) {

        var _$body = conf.$body;
        var _dispatcher = UrlDispatcher;
        var ROUTE_NOT_FOUND = 'ROUTE_NOT_FOUND';
        var soldItemAttr = Hypr.getThemeSetting('soldItemAttr');
        var currentStore, isBOHStore;
        var locale = require.mozuData('apicontext').headers['x-vol-locale'];
        var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
        var pageContext = require.mozuData('pagecontext');
        function moreLessConfig(){
            /*Code for moretag on facets - Start*/
            var facetHeight = Hypr.getThemeSetting('facetDefaultHeight');
            var facetHeightWithPX = Hypr.getThemeSetting('facetDefaultHeight')+'px';
            $('.mz-facetingform').find('.mz-l-sidebaritem .facet-details > ul').each(function(){
                if($(this).height() > facetHeight) {
                    $(this).parent().find('.moretag').css('display', 'inline-block');
                    if($(this).closest('.mz-l-sidebaritem').hasClass('for-caregory')){
                        $(this).css({'height': '205px', 'overflow-y': 'hidden'});
                    }else {
                        $(this).css({'height': facetHeightWithPX, 'overflow-y': 'hidden'});
                    }
                    
                }
            });
            $('.mz-facetingform').find('.mz-l-sidebaritem #Price > ul').css({'height': 'auto', 'overflow-y': 'hidden'});
            
            /*Code for moretag on facets - End*/
        }
        function updateUi(response) {
            var url = response.canonicalUrl;
            if(url && url.indexOf('/liquidation/c/') >= 0){
                url = url.replace(url.slice(url.indexOf('/c/'), url.indexOf('/c/') + (url.indexOf('?')-url.indexOf('/c/'))), '');
                url.replace('&', '%26');
            }
            _$body.html(response.body);
            if (url) _dispatcher.replace(url);
            _$body.removeClass('mz-loading');
            
            $('.result-view-toggle li a').on('click', function (e){
                e.preventDefault();
                $(this).closest('.result-view-toggle').find('li').removeClass('is-current');
                $(this).closest('li').addClass('is-current');
                
                if($(this).data('toggle-type') == 'list') {
                    window.gridStatus = "list";
                    $(document).find('.mz-productlist').addClass('list');
                } else {
                    window.gridStatus = "";
                    $(document).find('.mz-productlist').removeClass('list');
                }  
                $(document).find('#expert-advice-tab .mz-productlist').removeClass('list');
           });
            
            if (window.gridStatus == "list") {
                $('.result-view-toggle').find('li').removeClass('is-current');
                $('.result-view-toggle').find('li:first-child').addClass('is-current');
                $(document).find('.mz-productlist').addClass('list');
            } else {
                $('.result-view-toggle').find('li').removeClass('is-current');
                $('.result-view-toggle').find('li:nth-child(2)').addClass('is-current');
                $(document).find('.mz-productlist').removeClass('list');
            }
            $(document).find('#expert-advice-tab .mz-productlist').removeClass('list');
            
            if ($("#mz-drop-zone-expert-advice-bropzone > .mz-cms-row > .mz-cms-col-12-12").html() && $("#mz-drop-zone-expert-advice-bropzone > .mz-cms-row > .mz-cms-col-12-12").html().length > 0) {
                $("#mz-drop-zone-expert-advice-bropzone").addClass('isdropped');
            } else {
                $("#mz-drop-zone-expert-advice-bropzone").removeClass('isdropped');
            }
            
            moreLessConfig();
            
            var productsElements = $(".mz-productlist .mz-productlisting .saved-price").find(".promo-till");
            var torontoTimeZone = timeZoneSupport.findTimeZone('America/Toronto');
            var today = new Date();
            _.each(productsElements,function(item){
                var promotEndDate = item.getAttribute("data-promo-end-date");
                var promoItemZonedTime = timeZoneSupport.getZonedTime(new Date(promotEndDate), torontoTimeZone);
                var promoItemDate = new Date(promoItemZonedTime.year, promoItemZonedTime.month-1, promoItemZonedTime.day, promoItemZonedTime.hours, promoItemZonedTime.minutes, promoItemZonedTime.seconds, promoItemZonedTime.milliseconds);
                if(today <= promoItemDate){
                    $(item).closest('.saved-price').removeClass('hidden');  
                    $(item).text(formatDate(promoItemDate));    
                }
            });

            var productImpressions = $('[data-ign-gtm-impression]');
            window.recentlyViewed();
            window.quickViewBind();
            
            //Selected facet name.
            window.sendGtmDataLayer(productImpressions);
        }

        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();
    
            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
    
            return [year, month, day].join('-');
        }

        function showError(error) {
            // if (error.message === ROUTE_NOT_FOUND) {
            //     window.location.href = url;
            // }
            _$body.find('[data-mz-messages]').text(error.message);
        }
         function changePositionOfPageSizeAndSort(url, defaultSort){
             var decodedUrlSearch = decodeURIComponent(window.location.search);
             var urlParams = new URLSearchParams(decodedUrlSearch);
             if(urlParams.get('pageSize')){
                url = window.location.pathname + '?' + url.slice(url.indexOf('facetValueFilter')) + '%26pageSize=' + urlParams.get('pageSize');
             }
             if(urlParams.get('sortBy') && defaultSort !== 'default'){
                url = window.location.pathname + '?' + url.slice(url.indexOf('facetValueFilter')) + '%26sortBy=' + urlParams.get('sortBy');
             }
             return url;
         }
         
         function makePageSizeUrl(urlParams, elm) {
             var url;
             if(urlParams.get('pageSize')){
                var searchPageSizeQuery = window.location.search.replace('pageSize='+urlParams.get('pageSize'), 'pageSize='+elm.value);
                url = window.location.pathname + searchPageSizeQuery;
             }else{
                url = window.location.pathname + window.location.search + '%26pageSize=' + elm.value;
             }
             return url;
         }

        var facetSearch = '';

        function intentToUrl(e) {
            var elm = e.target;
            var url;
            if ($(elm).data('mz-action') == "sortAndFilter") {
                e.preventDefault();
                $(document).find('.filters').addClass('open');
                moreLessConfig();
            }
            var decodedUrlSearch = decodeURIComponent(window.location.search);
            var urlParams = new URLSearchParams(decodedUrlSearch);
            if ($(window).width() < 767) {
                if ($(elm).data('mz-action') == "applyFacets") {
                    var desktopUrl, selected, sortByValue;
                    if(window.location.href.indexOf('/liquidation') >= 0){
                        desktopUrl = window.location.pathname + "?facetValueFilter=tenant~clearance-on-sale%3a" + pageContext.purchaseLocation.code;
                        selected = $(document).find('input[data-mz-facet-value]:checked');
                        selected.each(function(index) {   
                            var facetName = $(selected[index]).data('mz-facet');
                            var facetValue = $(selected[index]).data('mz-facet-value');
                            if (facetName !=  'sortBy') {
                                if(facetName == 'Price' || facetName == 'tenant~rating') {
                                    desktopUrl = desktopUrl + '%2c' + facetValue + '%2c';
                                }else {
                                    desktopUrl = desktopUrl + '%2c' + facetName + '%3a' + facetValue + '%2c';
                                }
                            }   
                        });
                        if ($("input[name='sortBy']:checked").length == 1) {
                            sortByValue = $("input[name='sortBy']:checked").data('mz-facet-value');
                            sortByValue = sortByValue.replace(' ', '+');
                            if(sortByValue){
                                desktopUrl = desktopUrl + '%26sortBy=' + sortByValue.replace('+','%20');
                            }
                        }
                        if(urlParams.get('pageSize')){
                            desktopUrl = desktopUrl + '%26pageSize=' + urlParams.get('pageSize');
                        }
                    }else {
                        desktopUrl = window.location.pathname + "?facetValueFilter=";
                        selected = $(document).find('input[data-mz-facet-value]:checked');   
                           
                        if ($("input[name='sortBy']:checked").length == 1) {
                            sortByValue = $("input[name='sortBy']:checked").data('mz-facet-value');
                            sortByValue = sortByValue.replace(' ', '+');
                            desktopUrl = window.location.pathname + '?sortBy=' + sortByValue + '&facetValueFilter=';
                        }
                        
                        selected.each(function(index) {   
                            var facetName = $(selected[index]).data('mz-facet');
                            var facetValue = $(selected[index]).data('mz-facet-value');
                            if (facetName !=  'sortBy') {
                                if(facetName == 'Price' || facetName == 'tenant~rating') {
                                    desktopUrl = desktopUrl + facetValue + ',';
                                }else {
                                    desktopUrl = desktopUrl + facetName + '%3a' + facetValue + ',';
                                }
                            }                       
                        });
                    }
                    url = desktopUrl;
                    $('html, body').scrollTop(145);
                    if(url.indexOf('/liquidation') >= 0){ 
                        url = url.replace('&', '%26');
                    }
                    return url;
                } else if ($(elm).data('mz-action') == "clearFacets") {
                    $(document).find('.filters').removeClass('open');
                    if(window.location.href.indexOf('/liquidation') >= 0){
                        url = window.location.pathname + "?facetValueFilter=tenant~clearance-on-sale%3a" + pageContext.purchaseLocation.code;
                        if(urlParams.get('pageSize')){
                            url = url + '%26pageSize=' + urlParams.get('pageSize');
                        }
                    }else{
                        url = window.location.pathname;
                    }
                    $('html, body').scrollTop(145);
                    if(url.indexOf('/liquidation') >= 0){ 
                        url = url.replace('&', '%26');
                    }
                    return url;
                } else if($(elm).data('mz-facet') == soldItemAttr || elm.tagName.toLowerCase() === "select") {
                    if (elm.tagName.toLowerCase() === "select") {
                        elm = elm.options[elm.selectedIndex];
                    }
                    url = elm.getAttribute('data-mz-url') || elm.getAttribute('href') || '';
                    if (url && url[0] != "/") {
                        var parser1 = document.createElement('a');
                        parser1.href = url;
                        url = window.location.pathname + parser1.search;
                    }
                    if(window.location.href.indexOf('/liquidation') >= 0){
                        if(elm.tagName.toLowerCase() === "option"){
                            url = makePageSizeUrl(urlParams, elm);
                        }else if($(elm).data('mz-facet') == soldItemAttr){
                            if(url.indexOf('tenant%7edynattribute-23%3ae-commerce+item') >= 0){
                                url.replace('tenant%7edynattribute-23%3ae-commerce+item', '');
                            }else{
                                url = changePositionOfPageSizeAndSort(url);
                            }
                        }
                    }
                    $('html, body').scrollTop(145);
                    if(url.indexOf('/liquidation') >= 0){ 
                        url = url.replace('&', '%26');
                    }
                    return url;
                }else if($(elm).hasClass('mz-pagenumbers-next') || $(elm).hasClass('mz-pagenumbers-prev')){
                    url = window.location.pathname + $(elm).attr('href'); 
                    if(url.indexOf('/liquidation/c/') >= 0){ //if url has /c/{liquidationCategoryCode} then remove it
                        url = url.replace(url.slice(url.indexOf('/c/'), url.indexOf('/c/') + (url.indexOf('?')-url.indexOf('/c/'))), '');
                    }
                    if(window.location.href.indexOf('/liquidation') >= 0){
                        url = changePositionOfPageSizeAndSort(url);
                    }
                    if(url.indexOf('/liquidation') >= 0){ 
                        url = url.replace('&', '%26');
                    }
                    $('html, body').scrollTop(145);
                    return url;
                }
                
            } else {
                
                if (elm.tagName.toLowerCase() === "select") {
                    elm = elm.options[elm.selectedIndex];
                }
                if (window.location.href.indexOf('/liquidation') >= 0 && elm.tagName.toLowerCase() === "option"){
                    url = makePageSizeUrl(urlParams, elm);
                }else if(window.location.href.indexOf('/liquidation') >= 0 && $(elm).parent().data('mz-value') && $(elm).parent().data('mz-value') === 'sortBy'){
                    if($(elm).data('mz-sort-value')){
                        if(urlParams.get('sortBy')){
                            var searchOldQuery = urlParams.get('sortBy').indexOf(' ') >= 0 ? urlParams.get('sortBy').replace(' ', '%20') : urlParams.get('sortBy');
                            var searchSortByQuery = window.location.search.replace('sortBy='+searchOldQuery, 'sortBy='+$(elm).data('mz-sort-value').replace('+','%20'));
                            url = window.location.pathname + searchSortByQuery;
                        }else{
                            url = window.location.pathname + window.location.search + '%26sortBy=' + $(elm).data('mz-sort-value').replace('+','%20');
                        }
                    }else {  
                        url = window.location.pathname + elm.getAttribute('data-mz-url');
                        url = changePositionOfPageSizeAndSort(url, 'default');
                        url.replace('&', '%26');
                    }
                }else {
                    url = elm.getAttribute('data-mz-url') || elm.getAttribute('href') || '';
                    if (url && url[0] != "/") {
                        var parser = document.createElement('a');
                        parser.href = url;
                        url = window.location.pathname + parser.search;
                        if(url.indexOf('/liquidation/c/') >= 0){
                            url = url.replace(url.slice(url.indexOf('/c/'), url.indexOf('/c/') + (url.indexOf('?')-url.indexOf('/c/'))), '');
                        }
                        if(window.location.href.indexOf('/liquidation') >= 0){
                            url = changePositionOfPageSizeAndSort(url);
                        }
                        if(url.indexOf('/liquidation') >= 0){ 
                            url = url.replace('&', '%26');
                        }
                    }else {
                        //when facet is removed
                        if(window.location.href.indexOf('/liquidation') >= 0){
                            url = changePositionOfPageSizeAndSort(url);
                            url.replace('&', '%26');
                        }
                    }
                }
                $('html, body').scrollTop(145);
                var productData = [], selectedProducts= [];
                var compareItems = $('.mz-compareitem').find(".compare-item-container");
                _.each(compareItems, function(items) {
                    var prodCode = $(items).find(".closeButton").attr('data-mz-productcode');
                    var prodImage = $(items).find(".mz-product-image").attr('data-src');
                    selectedProducts = {
                        productCode: prodCode,
                        imgUrl: prodImage
                    };
                    productData.push(selectedProducts);
                });
                viewCompareProducts(productData);
                return url;
            } 
        } 

        function viewCompareProducts(productData) {
            setTimeout(function() {
            var removeProductionFromPreview = function(productCode){
                var indexOfProductToRemove;
                productData.forEach(function(product, index){ 
                    if(product.productCode === productCode) {
                        indexOfProductToRemove = index;
                        $('.compare-checkbox[data-mz-productcode='+product.productCode+']').attr('checked', false);
                    }
                });
                productData.splice(indexOfProductToRemove, 1);
                CompareProductView.model.set('data', productData);
                var productModelLength = CompareProductView.model.get('data').length;
                if (productModelLength < 4) {
                    _.each($('.compare-checkbox'), function(checkbox) {
                        if(!$(checkbox).prop('checked')) {
                            $(checkbox).attr('disabled',false);
                            $(checkbox).closest('.compare-section').next().removeClass('disabled');
                        }
                    });
                }
                CompareProductView.render();
            };
            var ComapreProductModal = Backbone.MozuView.extend({
                templateName: 'modules/product/compare-product-modal',
                initialize:function(){
                },
                removeProduct: function(e) {
                    var indexOfProductToRemove;
                    this.model.get('data').forEach(function(product, index){ 
                        if(product.productCode === $(e.currentTarget).attr('data-mz-productcode')) {
                            indexOfProductToRemove = index;
                            $('.compare-checkbox[data-mz-productcode='+product.productCode+']').attr('checked', false);
                        }
                    });
                    this.model.get('data').splice(indexOfProductToRemove, 1);
                    var modelLength = this.model.get('data').length;
                    removeProductionFromPreview($(e.currentTarget).attr('data-mz-productcode'));
                    if (indexOfProductToRemove === 0 && modelLength === 0) {
                        this.model.set('data',[]);
                        productData = [];
                        $("#compare-product-modal").modal('hide');
                    }
                    this.render();
                }
                // removeAllButton: function(){
                //     productData = [];
                //     this.model.set('data', []);
                //     var checkedBoxes = $('.compare-checkbox:checked');
                //     _.each(checkedBoxes, function(checkedBox){
                //         $(checkedBox).attr('checked', false);
                //     });
                //     CompareProductView.model.set('data', productData);
                //     CompareProductView.render();
                // }
            });
            var compareProductView = Backbone.MozuView.extend({
                templateName: 'modules/product/compare-products-preview',
                clearAll: function() {
                    this.model.set('data',[]);
                    productData = [];
                    var checkedBoxes = $('.compare-checkbox:checked');
                    _.each(checkedBoxes, function(checkedBox){
                        $(checkedBox).attr('checked', false);
                    });
                    _.each($('.compare-checkbox'), function(checkbox) {
                        $(checkbox).attr('disabled',false);
                        $(checkbox).closest('.compare-section').next().removeClass('disabled');
                    });
                    this.render();
                },
                render: function() {
                    Backbone.MozuView.prototype.render.apply(this, arguments);
                },
                removeProduct: function(e) {
                    removeProductionFromPreview($(e.currentTarget).attr('data-mz-productcode'));
                    this.render();
                },
                viewCompareDetails: function() {
                    if(this.model.get('data').length === 1){
                        this.model.set('error', Hypr.getLabel('selectTwoProductsError'));
                        this.render();
                    }else {
                        this.model.set('error', '');
                        this.render();
                        $('#compare-product-modal').modal().show();
                        var filterProductCode = _.map(this.model.get('data'), function(product) { return "productCode eq " + product.productCode; }).join(' or ');
                
                        var productModel = Backbone.MozuModel.extend({
                            mozuType: 'products'
                        });
                        
                        var ProductModel = new productModel();
                        
                        ProductModel.set('filter',  filterProductCode);
                        ProductModel.set("responseFields", "items(productCode, properties, content, options, price, measurements)");
                        ProductModel.fetch().then(function(responseObject) {
                            var productResponse = responseObject.apiModel.data.items;
                            var productResponseInSequence = []; 
                            _.each(productResponse, function(product, index) {
                                productResponseInSequence[productData.indexOf(_.find(productData, function(p){return p.productCode === product.productCode;}))] = product;
                            });
                            CompareProductModalView.model.set('data', productResponseInSequence);
                            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
                            locale = locale.split('-')[0];
                            var currentLocale = locale === 'fr' ? '/fr' : '/en';
                            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
                            CompareProductModalView.model.set('currentLocale', currentLocale);
                            CompareProductModalView.model.set('currentSite', currentSite);
                            
                            var propertyData = [];
                            _.each(productResponse, function(product) {
                                if (product.properties && Hypr.getThemeSetting("showProductDetailProperties")){
                                    _.each(product.properties, function(property){
                                        if(
                                        property.values && !property.isHidden && property.attributeDetail.inputType != "YesNo" && 
                                        property.attributeFQN != Hypr.getThemeSetting("modelAttr") && property.attributeFQN != Hypr.getThemeSetting("brandCode") && 
                                        property.attributeFQN != Hypr.getThemeSetting("itemDescription") && 
                                        property.attributeFQN != Hypr.getThemeSetting("hhSellingUOM") && 
                                        property.attributeFQN != Hypr.getThemeSetting("hhLumberInd") && 
                                        property.attributeFQN != Hypr.getThemeSetting("hhHardwareInd") && 
                                        property.attributeFQN != Hypr.getThemeSetting("hhFurnitureInd") &&  
                                        property.attributeFQN != Hypr.getThemeSetting("supplierDirectItem") && 
                                        property.attributeFQN != Hypr.getThemeSetting("hhWarrantyExchangeInd") && 
                                        property.attributeFQN != Hypr.getThemeSetting("limitedQtyInd") && 
                                        property.attributeFQN != Hypr.getThemeSetting("displayItem") && 
                                        property.attributeFQN != Hypr.getThemeSetting("hhBulkyCourierInd") && 
                                        property.attributeFQN != Hypr.getThemeSetting("hhFragileCourierInd") && 
                                        property.attributeFQN != Hypr.getThemeSetting("comments") && 
                                        property.attributeFQN != Hypr.getThemeSetting("productVideo") && 
                                        property.attributeFQN != Hypr.getThemeSetting("relatedProducts") && 
                                        property.attributeFQN != Hypr.getThemeSetting("productRating") && 
                                        property.attributeFQN != Hypr.getThemeSetting("productUpsell") && 
                                        property.attributeFQN != Hypr.getThemeSetting("fineClass") && 
                                        property.attributeFQN != Hypr.getThemeSetting("ehfFees") && 
                                        property.attributeFQN != Hypr.getThemeSetting("actualProductCode") && 
                                        property.attributeFQN != Hypr.getThemeSetting("productWeight") && 
                                        property.attributeFQN != Hypr.getThemeSetting("pilarType") && 
                                        property.attributeFQN != Hypr.getThemeSetting("marketingDescription") && 
                                        property.attributeFQN != Hypr.getThemeSetting("ingredients") && 
                                        property.attributeFQN != Hypr.getThemeSetting("promoItemType") && 
                                        property.attributeFQN != Hypr.getThemeSetting("imageId") && 
                                        property.attributeFQN != Hypr.getThemeSetting("mktKeyword") && property.attributeFQN != Hypr.getThemeSetting("homeExclusiveInd") && 
                                        property.attributeFQN != Hypr.getThemeSetting("onlyAtHH") && property.attributeFQN != Hypr.getThemeSetting("availability") && 
                                        property.attributeFQN != Hypr.getThemeSetting("priceListEntityType") && 
                                        property.attributeFQN != Hypr.getThemeSetting("gender") && 
                                        property.attributeFQN != Hypr.getThemeSetting("disclaimerAttr") && 
                                        property.attributeFQN != Hypr.getThemeSetting("onsaleAttr") && 
                                        property.attributeFQN != Hypr.getThemeSetting("soldItemAttr")
                                        ){
                                            var propertyName = property.attributeDetail.name.split('_');
                                            if(propertyName[0] != "DynAttribute" && propertyName[0] != "DynAttribut") {
                                                var data = {};
                                                if(propertyData.length > 0){
                                                    var isPropertyPresent = false;
                                                    _.each(propertyData, function(currentProperty) {
                                                        if(currentProperty.attributeFQN === property.attributeFQN) {
                                                            isPropertyPresent = true;
                                                        }
                                                    });
                                                    if(!isPropertyPresent) {
                                                        data = {'attributeFQN': property.attributeFQN, 'value' : property.attributeDetail.name};
                                                        propertyData.push(data);
                                                    }
                                                    
                                                }else {
                                                    data = {'attributeFQN': property.attributeFQN, 'value' : property.attributeDetail.name};
                                                    propertyData.push(data);
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                            CompareProductModalView.model.set('propertyData', propertyData);
                            if (localStorage.getItem('preferredStore')){
                                CompareProductModalView.model.set({"preferredStore":$.parseJSON(localStorage.getItem('preferredStore'))});
                            }
                            CompareProductModalView.render();
                        });
                    }
                }
            });
            $('#compare-product-modal').on('hidden.bs.modal', function () {
                CompareProductModalView.model.set('data', []);
                CompareProductModalView.model.set('propertyData', []);
                CompareProductModalView.render();
            });
            _.each((productData), function(prod){
                _.each($('.compare-checkbox'), function(checkbox) {
                    if ($(checkbox).attr('data-mz-productcode') == prod.productCode){
                        $(checkbox).attr("availProd", "true");
                    }
                    $('.compare-checkbox[availProd=true]').attr('checked', true);
                });
            });
            var productModelLength = productData.length;
            if (productModelLength == 4) {
                _.each($('.compare-checkbox'), function(checkbox) {
                    if(!$(checkbox).prop('checked')) {
                        $(checkbox).attr('disabled',true);
                        $(checkbox).closest('.compare-section').next().addClass('disabled');
                    }
                });
            } else {
                _.each($('.compare-checkbox'), function(checkbox) {
                    if(!$(checkbox).prop('checked')) {
                        $(checkbox).attr('disabled',false);
                        $(checkbox).closest('.compare-section').next().removeClass('disabled');
                    }
                });
            }
            $(document).on('change',".compare-checkbox", function(e){
                e.preventDefault();
                var checkedBoxes = $('.compare-checkbox:checked');
                var numItems = $('#compare-product-container').find('.mz-compareitem').length;
                var indexOfProductToRemove;
                if (!$(e.currentTarget).attr('checked')) {
                    $(e.currentTarget).prop( "checked", true );
                }
                if (checkedBoxes.length == 4 || numItems == 4) {
                    if($(e.currentTarget).is(":checked")) {
                        _.each($('.compare-checkbox'), function(checkbox) {
                            if(!$(checkbox).prop('checked')) {
                                $(checkbox).attr('disabled',true);
                                $(checkbox).closest('.compare-section').next().addClass('disabled');
                            }
                        });
                    }
                } else {
                    _.each($('.compare-checkbox'), function(checkbox) {
                        if(!$(checkbox).prop('checked')) {
                            $(checkbox).attr('disabled',false);
                            $(checkbox).closest('.compare-section').next().removeClass('disabled');
                        }
                    });
                }
                if(checkedBoxes.length > 0){
                    if (checkedBoxes.length > 4 || numItems == 4) {                        
                        if(!$(e.currentTarget).is(":checked")){
                            productData.forEach(function(product, index){ 
                                if(product.productCode === $(e.currentTarget).attr('data-mz-productcode')) {
                                    indexOfProductToRemove = index;
                                }
                            });
                            productData.splice(indexOfProductToRemove, 1);
                        }
                    } else if((checkedBoxes.length <= 4 && numItems < 4) || (numItems < 4)) {
                        if($(e.currentTarget).is(":checked")){
                            var product = {
                                productCode: $(e.currentTarget).attr('data-mz-productcode'),
                                imgUrl: $(e.currentTarget).attr('data-mz-img')
                            };
                            productData.push(product);
                            $(e.currentTarget).attr("availProd", "true");
                            $(e.currentTarget).attr('checked', true);
                            productData = _.uniq(productData, function(product){ return product.productCode; });
                            if (numItems == 3) {
                                _.each($('.compare-checkbox'), function(checkbox) {
                                    if(!$(checkbox).prop('checked')) {
                                        $(checkbox).attr('disabled',true);
                                        $(checkbox).closest('.compare-section').next().addClass('disabled');
                                    }
                                });
                            }
                        }else{
                            productData.forEach(function(product, index){ 
                                if(product.productCode === $(e.currentTarget).attr('data-mz-productcode')) {
                                    indexOfProductToRemove = index;
                                }
                            });
                            productData.splice(indexOfProductToRemove, 1);
                        }
                    }
                }else {
                    productData = [];
                }
                CompareProductView.model.set('data', productData);
                CompareProductView.render();
            });
            var CompareProductView = new compareProductView({
                el:$('#compare-product-container'),
                model: new Backbone.Model()
            });
            var CompareProductModalView = new ComapreProductModal({
                el: $('#compare-product-modal'),
                model: new Backbone.Model()
            });
            CompareProductView.model.set('data', productData);
            CompareProductView.render();
            },2000);
        }

        var navigationIntents = IntentEmitter(
            _$body,
            [
                'click [data-mz-pagingcontrols] a',
                'click [data-mz-pagenumbers] a',
                'click a[data-mz-facet-value]',
                'click [data-mz-action="clearFacets"]',
                'change input[data-mz-facet-value]',
                'change [data-mz-value="pageSize"]',
                'click [data-mz-value="sortBy"]',
                'click [data-mz-action="applyFacets"]',
                'click [data-mz-action="customPrice"]',
                'click [data-mz-action="sortAndFilter"]'
            ],
            intentToUrl
        );
        
        navigationIntents.on('data', function(url, e) {
            if (url && _dispatcher.send(url)) {
                _$body.addClass('mz-loading');
                e.preventDefault();
            }
        });

        _dispatcher.onChange(function(url) {
            getPartialView(url, conf.template).then(updateUi, showError);
        });

    }
    return {
        createFacetedCollectionViews: factory
    };

});
