define([
    'modules/jquery-mozu',
    'modules/backbone-mozu'
], function ($, Backbone) {
    var OrderReturnModalView = Backbone.MozuView.extend({
        templateName: 'modules/my-account/my-account-order-return-modal',
        initialize: function(options){
            this.parent = options.parent;
        },
        render: function () {
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            this.model.set("currentLocale", locale, {silent: true});
            Backbone.MozuView.prototype.render.apply(this, arguments);
            this.calculateHeight();
        },
        calculateHeight: function(){
            var windowHeight = $(window).height() - 10;
            this.$el.find('.modal-content').css('min-height', windowHeight + 'px');
            var screenSize = window.matchMedia("(min-width: 1024px)");
            if(screenSize.matches){
                //hide body scroll
                $('.history').css('overflow-x', 'unset');
            }
        },
        clearRmaData : function(){ 
            this.parent.model.clearReturn();
            $('.modal').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        }
    });

    return OrderReturnModalView;
});