define([
    'modules/jquery-mozu',
    'modules/backbone-mozu',
    'hyprlive',
    'underscore',
    'bignumber',
    'modules/views-order-return-review-step'
], function ($, Backbone, Hypr, _, BigNumber, ReturnReviewStepView) {
    var SelectReturnMethodStepView = Backbone.MozuView.extend({
        templateName: 'modules/my-account/my-account-order-return-select-return-method-step',
        additionalEvents: {},
        initialize: function () {
            this.setDataRmaItem();
            this.setReturnMethod();
            this.setItemSubTotal();
            if (this.model.get('availableCourierMethods') === "courier") {
                this.$el.parent().siblings(".content-loading").css('display', 'block');
                this.getCouriers();
            } else {
                this.model.set('courierRate', 0);
                if( this.model.get('replaceViaCourierCount') > 0){
                    this.setRmaReplaceTypeToReplaceInStore();
                    this.setItemSubTotal();
                }
            }
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            this.model.set("currentLocale", locale, {silent: true});
        },
        render: function () {
            Backbone.MozuView.prototype.render.apply(this, arguments);
        },
        setDataRmaItem: function () {
            var self = this,
                orderItems = this.model.get('items');
            _.each(this.model.get('rma').get('items').models, function (rmaItem) {
                var orderItem = _.findWhere(orderItems.models, {'id': rmaItem.get('orderItemId')}),
                    productMeasurement = orderItem.get('product').get('measurements');
                var itemShippingCost = rmaItem.get('fulfillmentMethod') === "Ship" && rmaItem.get('rmaReason') !== "NoLongerWanted" ? self.getItemShippingCharge(rmaItem, orderItems, productMeasurement) : 0;
                rmaItem.set('itemShippingCost', itemShippingCost);
            });
        },
        setReturnMethod: function () {
            var rmaItems = this.model.get('rma').get('items').models,
                shipItemCount = 0,
                customerPayShippingCount = 0,
                replaceViaCourierCount = 0;
            _.each(rmaItems, function (rmaItem) {
                if (rmaItem.get('fulfillmentMethod') === "Ship" && (rmaItem.get('rmaReturnType') === "Refund" || (rmaItem.get('rmaReturnType') === "Replace" && rmaItem.get('replaceType') === "replaceViaCourier"))) {
                    shipItemCount++;
                    if(rmaItem.get('rmaReturnType') === "Replace" && rmaItem.get('replaceType') === "replaceViaCourier") replaceViaCourierCount++;
                    if (rmaItem.get('rmaReason') === "NoLongerWanted") customerPayShippingCount++;
                }
            });
            var courierType = shipItemCount == rmaItems.length ? "courier" : 'store-drop';
            this.model.set('availableCourierMethods', courierType);
            this.model.set('replaceViaCourierCount', replaceViaCourierCount);
            this.model.set('isCourierReplace', replaceViaCourierCount > 0 ? true : false);
            this.model.set('isCustomerPayShipping', customerPayShippingCount == rmaItems.length ? true : false);
        },
        //Refer formula Total Shipping Cost x (Product Weight / TOTAL PRODUCTS WEIGHT) by https://help.vtex.com/tutorial/how-is-shipping-cost-calculated-when-theres-an-item-with-shipping-benefit-on-the-cart--3EaQjxkErC6OKAWCWYSMiw
        getItemShippingCharge: function (rmaItem, orderItems, productMeasurement) {
            var orderShippingTotal = this.model.get('shippingTotal'),
                orderShippingTaxTotal = this.model.get('shippingTaxTotal'),
                totalShippingWithTax = parseFloat(BigNumber.sum(orderShippingTotal.toFixed(2), orderShippingTaxTotal.toFixed(2)).toFixed(2)),
                totalProductWeight = this.getTotalWeight(orderItems),
                itemWeight = productMeasurement.weight.value,
                weightCalculation = parseFloat(new BigNumber(itemWeight).dividedBy(totalProductWeight).toFixed()),
                finalShipping = parseFloat(new BigNumber(totalShippingWithTax).multipliedBy(weightCalculation).toFixed());
            console.log('itemShipping', finalShipping);
            return finalShipping;
        },
        getTotalWeight: function (orderItems) {
            var totalWeight = 0;
            _.each(orderItems.models, function (orderItem) {
                var fulfillmentMethod = orderItem.get('fulfillmentMethod');
                if (fulfillmentMethod === "Ship") {
                    var productWeight = orderItem.get('product').get('measurements').weight.value,
                        totalProductWeight = productWeight * orderItem.get('quantity');
                    totalWeight = parseFloat(BigNumber.sum(totalWeight, totalProductWeight).toFixed());
                }
            });
            console.log('totalWeight', totalWeight);
            return totalWeight;
        },
        getCouriers: function () {
            var self = this,
                shipmentNumbers = _.uniq(_.map(this.model.get('rma').get('items').models, function (rma) {
                    return rma.get('shipmentNumber');
                })),
                getRatesSteps = self.getRatesSteps(shipmentNumbers);

            Promise.all(getRatesSteps).then(function (response) {
                self.successHandler(response);
            }, function (error) {
                self.model.set('courierRate', 0);
                self.model.set('isCourierReplace', false);
                self.model.set('availableCourierMethods', 'store-drop');
                self.setRmaReplaceTypeToReplaceInStore();
                self.setItemSubTotal();
                self.render();
                console.log('Error while getting shipping rates', error);
            });
        },
        fetchRates: function (shipmentNumber) {
            var payload = this.getPayload(shipmentNumber);
            return $.ajax({
                url: Hypr.getThemeSetting("getShippingRates"),
                data: JSON.stringify(payload),
                type: "POST",
                cors: true,
                contentType: 'application/json'
            });
        },
        getRatesSteps: function (shipmentNumbers) {
            var self = this;
            var tasks = shipmentNumbers.map(function (shipmentNumber) {
                return self.fetchRates(shipmentNumber);
            });
            return tasks;
        },
        getPayload: function (shipmentNumber) {
            var rmaItems = this.model.get('rma').get('items').models,
                products = [];
            _.each(rmaItems, function (rmaItem) {
                products.push({
                    "productCode": rmaItem.get('productCode'),
                    "quantity": rmaItem.get('rmaQuantity')
                });
            });
            return {
                'shipmentNumber': shipmentNumber,
                'products': products
            };
        },
        successHandler: function (serviceRates) {
            var courierData = [];
            _.each(serviceRates, function (serviceRate) {
                if (serviceRate.serviceAndPrice.statusCode === "200") courierData.push(serviceRate.serviceAndPrice);
            });
            var serviceRateSortByLowPrice = _.sortBy(courierData, 'price');

            if (courierData.length && serviceRateSortByLowPrice.length) {
                if (this.model.get('isCustomerPayShipping')) {
                    this.model.set('courierRate', serviceRateSortByLowPrice[0].price);
                } else {
                    this.model.set('courierRate', 0);
                }
                this.model.set('shipmentNumberForShippingLabel', serviceRateSortByLowPrice[0].shipmentNumber);
                this.model.set('returnLocationCode', serviceRateSortByLowPrice[0].warehouseCode);
            } else {
                this.model.set('courierRate', 0);
                this.model.set('availableCourierMethods', 'store-drop');
                this.model.set('isCourierReplace', false);
                this.setRmaReplaceTypeToReplaceInStore();
                this.setItemSubTotal();
            }
            this.render();
            this.$el.parent().siblings(".content-loading").css('display', 'none');
        },
        setRmaReplaceTypeToReplaceInStore: function(){ 
            _.each(this.model.get('rma').get('items').models, function (rmaItem) {
                //if(rmaItem.get('rmaReturnType') === "Replace") rmaItem.set('replaceType', 'replaceInStore');
                if(rmaItem.get('rmaReturnType') === "Replace") rmaItem.set('replaceType', 'replaceViaCourier');
            });
        },
        setItemSubTotal: function () {
            var self = this,
                orderItems = this.model.get('items'),
                refundSubtotal = 0;
            _.each(this.model.get('rma').get('items').models, function (rmaItem) { 
                if(rmaItem.get('replaceType') != "replaceViaCourier"){
                    var rmaItemSubtotal = self.getItemSubtotal(rmaItem, rmaItem.get('rmaQuantity')),
                        orderItem = _.findWhere(orderItems.models, {'id': rmaItem.get('orderItemId')}),
                        itemTaxTotal = self.getItemTax(orderItem, rmaItemSubtotal),
                        itemPriceWithTax = parseFloat(BigNumber.sum(rmaItemSubtotal, itemTaxTotal).toFixed(2)),
                        itemTotalShipping = parseFloat(new BigNumber(rmaItem.get('itemShippingCost')).multipliedBy(rmaItem.get('rmaQuantity')).toFixed(2));
                    refundSubtotal = parseFloat(BigNumber.sum(refundSubtotal.toFixed(2), itemPriceWithTax, itemTotalShipping).toFixed(2));
                }
            });
            this.model.set('refundSubtotal', refundSubtotal);
            console.log('refundSubtotal', refundSubtotal);
        },
        getItemSubtotal: function (rmaItem, rmaQuantity) {
            var product = rmaItem.get('product'),
                productPriceObj = product.get('price'),
                productPrice = productPriceObj.get('salePrice') ? productPriceObj.get('salePrice') : productPriceObj.get('price'),
                productOptions = product.get('options') && product.get('options').models.length ? product.get('options').toJSON() : 0,
                ehfOption = _.findWhere(productOptions, {'attributeFQN': Hypr.getThemeSetting("ehfFees")}),
                ehfValue = ehfOption ? ehfOption.shopperEnteredValue : 0,
                itemSubtotalWithoutQuantity = BigNumber.sum(productPrice.toFixed(2), ehfValue).toFixed(2);
            rmaItem.set('itemPriceWithEHF', itemSubtotalWithoutQuantity);
            var itemSubtotal = parseFloat(new BigNumber(itemSubtotalWithoutQuantity).multipliedBy(rmaQuantity).toFixed(2));
            console.log('itemTotalWithEHF', itemSubtotal);
            return itemSubtotal;
        },
        getItemTax: function (orderItem, rmaItemSubtotal) {
            var itemTaxData = orderItem.get('taxData').taxRates,
                itemTaxTotal = 0;
            _.each(itemTaxData, function (tax) {
                var taxRate = parseFloat(tax.taxRate),
                    itemTax = parseFloat(new BigNumber(rmaItemSubtotal).multipliedBy(taxRate).toFixed(2));
                itemTaxTotal = parseFloat(BigNumber.sum(itemTaxTotal.toFixed(2), itemTax).toFixed(2));
                console.log(tax, itemTax);
            });
            return itemTaxTotal;
        },
        continueToReturnReview: function () {
            this.setReturnLocation();
            var returnReviewStepView = new ReturnReviewStepView({
                el: this.$el,
                model: this.model
            });
            returnReviewStepView.render();
        },
        setReturnLocation: function () {
            var selectedCourierType = this.model.get('selectedCourierType'),
                rma = this.model.get('rma');
            if (selectedCourierType === 'canadaPost') {
                rma.set('locationCode', this.model.get('returnLocationCode'));
            } else {
                rma.set('locationCode', this.model.get('priceListCode'));
            }
        },
        setCourierType: function (e) {
            var courierType = $(e.currentTarget).val();
            this.model.set('selectedCourierType', courierType);
            if (courierType === 'canadaPost') {
                this.model.set('shippingCost', this.model.get('courierRate'));
            } else {
                this.model.set('shippingCost', 0);
            }
            this.render();
        }
    });

    return SelectReturnMethodStepView;
});