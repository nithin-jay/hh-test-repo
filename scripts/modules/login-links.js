/**
 * Adds a login popover to all login links on a page.
 */
define([
        'modules/jquery-mozu',
        'modules/api', 
        'hyprlive', 
        "modules/backbone-mozu",
        "modules/views-messages",
        'underscore', 
        'modules/models-cart',
        'hyprlivecontext',
        'modules/models-product',
        'modules/cookie-utils',
        "pages/preferred-store",
        'vendor/jquery-placeholder/jquery.placeholder',
        'shim!bootstrap[modules/jquery-mozu=jQuery]>jQuery=jQuery>jQuery'
    ],
function ($, api, Hypr, Backbone, messageViewFactory, _, CartModels, HyprLiveContext, ProductModels, CookieUtils) {
	$.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results===null){
           return null;
        }
        else{
           return decodeURI(results[1]) || 0;
        }
    };
	    
    var user = require.mozuData('user'),
        apiContext = require.mozuData('apicontext'),
        contextSiteId = apiContext.headers["x-vol-site"],
        homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId"),
        isHomeFurnitureSite = false,
        ua = navigator.userAgent.toLowerCase();

	var relation1InsertRecord, relation1Link, goto2020Link, b_token;
	var locale = require.mozuData('apicontext').headers['x-vol-locale'], siteId = apiContext.headers["x-vol-site"];
	locale = locale.split('-')[0];
	if(locale === "fr"){
		locale = "French";
	} else {
		locale = "English";
	}  
    
    if (!require.mozuData('pagecontext').isEditMode) { 
        //set preferred language
        var expiryDate = new Date();
        expiryDate.setYear(expiryDate.getFullYear() + 1);
        var tenantId = apiContext.headers["x-vol-tenant"],
        locationPath = window.location.pathname, landingSiteId = '', search = '';
        if($.cookie("hhLanguage")) {
            if(window.location.search) {
                search = window.location.search.indexOf('returnurl') === -1 && window.location.search.indexOf('returnUrl') === -1 ? window.location.search : '';
            }
            if(siteId === Hypr.getThemeSetting('enSiteId') && $.cookie("hhLanguage") === 'fr') { //current site is english
                landingSiteId = Hypr.getThemeSetting('frSiteId');
                if(locationPath.indexOf('/en') !== -1){
                    locationPath = locationPath.replace('/en', '/fr');
                }else{
                    locationPath = '/fr' + locationPath;
                }
                if(Hypr.getThemeSetting('selectedEnvironment') === 'DEV') { 
                    window.location.href = Hypr.getThemeSetting('hhWebsiteLink') + locationPath + search;
                }else if(Hypr.getThemeSetting('selectedEnvironment') === 'PROD') {
                    window.location.href = Hypr.getThemeSetting('hhWebsiteLink') + locationPath + search;
                }
            }else if(siteId === Hypr.getThemeSetting('frSiteId') && $.cookie("hhLanguage") === 'en'){
                landingSiteId = Hypr.getThemeSetting('enSiteId');
                if(locationPath.indexOf('/fr') !== -1){
                    locationPath = locationPath.replace('/fr', '/en');
                }else{
                    locationPath = '/en' + locationPath;
                }
                if(Hypr.getThemeSetting('selectedEnvironment') === 'DEV') { 
                    window.location.href = Hypr.getThemeSetting('hhWebsiteLink') + locationPath + search;
                }else if(Hypr.getThemeSetting('selectedEnvironment') === 'PROD') {
                    window.location.href = Hypr.getThemeSetting('hhWebsiteLink') + locationPath + search;
                }
            }
        }else if(siteId !== Hypr.getThemeSetting('homeFurnitureSiteId')){
            if(locale === "French"){
            	if (ua.indexOf('chrome') > -1) {
                    if (/edg|edge/i.test(ua)){
                        $.cookie("hhLanguage", 'fr', {path:'/', expires: expiryDate});
                    }else{
                        document.cookie = "hhLanguage=fr;expires="+expiryDate+";path=/;Secure;SameSite=None";
                    }
                } else {
                    $.cookie("hhLanguage", 'fr', {path:'/', expires: expiryDate});
                }
            }else {
            	if (ua.indexOf('chrome') > -1) {
                    if (/edg|edge/i.test(ua)){
                        $.cookie("hhLanguage", 'en', {path:'/', expires: expiryDate});
                    }else{
                        document.cookie = "hhLanguage=en;expires="+expiryDate+";path=/;Secure;SameSite=None";
                    }
                } else {
                    $.cookie("hhLanguage", 'en', {path:'/', expires: expiryDate});
                }
            }
        }
    }

	if(Hypr.getThemeSetting('selectedEnvironment') === 'PROD') {
		relation1InsertRecord = Hypr.getThemeSetting('relation1InsertRecordPROD');
		b_token =  Hypr.getThemeSetting('bTokenRelation1Prod');
		
		if(locale === "English") {
			goto2020Link = Hypr.getThemeSetting('enGoto2020LinkPROD');
			relation1Link = Hypr.getThemeSetting('relation1LinkPRODEN');
		} else if (locale === "French"){
			goto2020Link = Hypr.getThemeSetting('frGoto2020LinkPROD');
			relation1Link = Hypr.getThemeSetting('relation1LinkPRODFR');
		}		
	} else {
		relation1InsertRecord = Hypr.getThemeSetting('relation1InsertRecordDEV');
		relation1Link = Hypr.getThemeSetting('relation1LinkDEV');
		b_token =  Hypr.getThemeSetting('bTokenRelation1Dev');
		if(locale === "French") {
			goto2020Link = Hypr.getThemeSetting('frGoto2020LinkDEVLogin');
		} else {
			goto2020Link = Hypr.getThemeSetting('enGoto2020LinkDEVLogin');
		}
	}
	
    
    
    /* check current site is home furniture site or not */          
        if(homeFurnitureSiteId === contextSiteId){
            isHomeFurnitureSite = true;
        }

    if (!(user.isAnonymous)) {
        $('.mz-user-firstname').html(' ');
        var cust = api.get('customer', {
            id: user.accountId
        }).then(function(response) {
        	$('.mz-user-firstname').html(response.data.firstName);
        });
    }
        
    var usePopovers = function() {
        return !Modernizr.mq('(max-width: 480px)');
    },
    isTemplate = function(path) {
        return require.mozuData('pagecontext').cmsContext.template.path === path;
    },
    returnFalse = function () {
        return false;
    },
    returnUrl = function() {
        var returnURL = $('input[name=returnUrl]').val();
        if(!returnURL) {
            returnURL = '/';
        }
        return returnURL;
    },
    $docBody,

    polyfillPlaceholders = !('placeholder' in $('<input>')[0]);

    var DismissablePopover = function () { };

    $.extend(DismissablePopover.prototype, {
        boundMethods: [],
        setMethodContext: function () {
            for (var i = this.boundMethods.length - 1; i >= 0; i--) {
                this[this.boundMethods[i]] = $.proxy(this[this.boundMethods[i]], this);
            }
        },
        dismisser: function (e) {
            if (!$.contains(this.popoverInstance.$tip[0], e.target) && !this.loading) {
                // clicking away from a popped popover should dismiss it
                this.$el.popover('destroy');
                this.$el.on('click', this.createPopover);
                this.$el.off('click', returnFalse);
                this.bindListeners(false);
                $docBody.off('click', this.dismisser);
            }
        },
        setLoading: function (yes) {
            this.loading = yes;
            this.$parent[yes ? 'addClass' : 'removeClass']('is-loading');
        },
        onPopoverShow: function () {
            var self = this;
            _.defer(function () {
                $docBody.on('click', self.dismisser);
                self.$el.on('click', returnFalse);
            });
            this.popoverInstance = this.$el.data('bs.popover');
            this.$parent = this.popoverInstance.tip();
            this.bindListeners(true);
            this.$el.off('click', this.createPopover);
            if (polyfillPlaceholders) {
                this.$parent.find('[placeholder]').placeholder({ customClass: 'mz-placeholder' });
            }
        },
        createPopover: function (e) {
            // in the absence of JS or in a small viewport, these links go to the login page.
            // Prevent them from going there!
            var self = this;
            if (usePopovers()) {
                e.preventDefault();
                // If the parent element's not positioned at least relative,
                // the popover won't move with a window resize
                //var pos = $parent.css('position');
                //if (!pos || pos === "static") $parent.css('position', 'relative');
                this.$el.popover({
                    //placement: "auto right",
                    animation: true,
                    html: true,
                    trigger: 'manual',
                    content: this.template,
                    container: 'body'
                }).on('shown.bs.popover', this.onPopoverShow)
                .popover('show');

            }
        },
        retrieveErrorLabel: function (xhr) {
            var message = "";
            if (xhr.message) {
                message = Hypr.getLabel(xhr.message);
            } else if ((xhr && xhr.responseJSON && xhr.responseJSON.message)) {
                message = Hypr.getLabel(xhr.responseJSON.message);
            }

            if (!message || message.length === 0) {
                this.displayApiMessage(xhr);
            } else {
                var msgCont = {};
                msgCont.message = message;
                this.displayApiMessage(msgCont);
            }
        },
        displayApiMessage: function (xhr) {
            this.displayMessage(xhr.message ||
                (xhr && xhr.responseJSON && xhr.responseJSON.message) ||
                Hypr.getLabel('unexpectedError'));
        },
        displayApiCustomMessage: function(xhr, isSignupForm) {
            this.displayMessage(xhr.message ||
                (xhr && xhr.responseJSON && xhr.responseJSON.message) ||
                Hypr.getLabel('unexpectedError'), isSignupForm);
        },
        displayMessage: function (msg) {
            this.setLoading(false);

            var MessageModel = Backbone.MozuModel.extend({});
                var MessageCollection = new Backbone.Collection();

                var messageView = messageViewFactory({
                    el: $('[data-ign-message-bar]'),
                    model: MessageCollection
                });

                var messageModel = null, message = null;

                if (msg === "Missing or invalid parameter: resetPasswordInfo UserName or EmailAddress must be provided" || msg.search("Item not found:") === 0 || msg === "You should receive an email with instructions to reset your password shortly.") {
                    if (msg === "You should receive an email with instructions to reset your password shortly.") {
                        message = Hypr.getLabel('successEmail');
                    } else if (msg === "Missing or invalid parameter: resetPasswordInfo UserName or EmailAddress must be provided") {
                        message = Hypr.getLabel('requireUsername');
                    } else if ($('[data-ign-message-bar]:contains("Item not found:")')) {
                        message = Hypr.getLabel('itemNotFound');
                    } else {
                        message = msg;
                    }
                } 
                else if (msg === "Login failed. Please specify a user.") {
                    message = msg;
                }
                else {
                    if (msg === "Missing or invalid parameter: username ") {
                        message = Hypr.getLabel('requireEmail');
                    } else if (msg === "Missing or invalid parameter: emailAddress Email address is not valid") {
                        message = Hypr.getLabel('invalidEmail');
                    } else if (msg === "One or more errors occurred.") {
                        message = Hypr.getLabel('invalidEmail');
                    } else if (msg === "Missing or invalid parameter: password Password must be a minimum of 6 characters with at least 1 number and 1 alphabetic character") { 
                        message = Hypr.getLabel('invalidPassword');
                    } else if (msg === "Missing or invalid parameter: password Password cannot be empty") {
                        message = Hypr.getLabel('emptyPassword');
                    } else if (msg === "Missing or invalid parameter: EmailAddress EmailAddress already associated with a login") {
                        message = Hypr.getLabel('associateEmail');
                    } else if (msg === "Validation Error: update exception") {
                        message = Hypr.getLabel('associateEmail');
                    } else if (msg === "One or more errors occurred.") {
                        message = Hypr.getLabel('invalidEmail');
                    } else if ("[data-ign-message-bar] li:contains('Login as')") {
                    	 message = Hypr.getLabel('loginFailed');
                    } else {
                        message = msg;
                        window.validated = true;
                    }
                }

                messageModel = new MessageModel();
                messageModel.set({message: message});
                MessageCollection.add(messageModel);
                messageView.render();

           // this.$parent.find('[data-mz-role="popover-message"]').html('<span class="mz-validationmessage">' + msg + '</span>');
        },
        displayCustomMessage: function(msg, isSignupForm, validationFor) {
                this.setLoading(false);
                if (isSignupForm) {
                    this.$parent.find('[data-mz-validationmessage-for="' + validationFor + '"]').html(msg);
                    this.$parent.find('[data-mz-signup-' + validationFor.toLowerCase() + ']').addClass('is-invalid');
                    this.$parent.find('[data-mz-role="popover-message"]').html('<span class="mz-validationmessage"></span>');
                } else {
                    this.$parent.find('[data-mz-validationmessage-for="' + validationFor + '"]').html(msg);
                    this.$parent.find('[data-mz-login-' + validationFor.toLowerCase() + ']').addClass('is-invalid');
                    this.$parent.find('[data-mz-role="popover-message"]').html('<span class="mz-validationmessage"></span>');
                }
            },
        init: function (el) {
            this.$el = $(el);
            this.loading = false;
            this.setMethodContext();
            if (!this.pageType){
                this.$el.on('click', this.createPopover);
            }
            else {
               this.$el.on('click', _.bind(this.doFormSubmit, this));
            }
        },
        doFormSubmit: function(e){
            e.preventDefault();
            this.$parent = this.$el.closest(this.formSelector);
            this[this.pageType]();
        }
    });

    var LoginPopover = function() {
        DismissablePopover.apply(this, arguments);
        this.login = _.debounce(this.login, 150);
        this.retrievePassword = _.debounce(this.retrievePassword, 150);
    };
    LoginPopover.prototype = new DismissablePopover();
    $.extend(LoginPopover.prototype, {
        boundMethods: ['handleEnterKey', 'handleLoginComplete', 'displayResetPasswordMessage', 'dismisser', 'displayMessage', 'displayApiMessage', 'createPopover', 'slideRight', 'slideLeft', 'login', 'retrievePassword', 'onPopoverShow'],
        template: Hypr.getTemplate('modules/common/login-popover').render(),
        bindListeners: function (on) {
            var onOrOff = on ? "on" : "off";
            this.$parent[onOrOff]('click', '[data-mz-action="forgotpasswordform"]', this.slideRight);
            this.$parent[onOrOff]('click', '[data-mz-action="loginform"]', this.slideLeft);
            this.$parent[onOrOff]('click', '[data-mz-action="submitlogin"]', this.login);
            this.$parent[onOrOff]('click', '[data-mz-action="submitforgotpassword"]', this.retrievePassword);
            this.$parent[onOrOff]('keypress', 'input', this.handleEnterKey);
        },
        onPopoverShow: function () {
            DismissablePopover.prototype.onPopoverShow.apply(this, arguments);
            this.panelWidth = this.$parent.find('.mz-l-slidebox-panel').first().outerWidth();
            this.$slideboxOuter = this.$parent.find('.mz-l-slidebox-outer');

            if (this.$el.hasClass('mz-forgot')){
                this.slideRight();
            }
        },
        handleEnterKey: function (e) {
            if (e.which === 13) {
                var $parentForm = $(e.currentTarget).parents('[data-mz-role]');
                switch ($parentForm.data('mz-role')) {
                    case "login-form":
                        this.login();
                        break;
                    case "forgotpassword-form":
                        this.retrievePassword();
                        break;
                }
                return false;
            }
        },
        slideRight: function (e) {
            if (e) e.preventDefault();
            this.$slideboxOuter.css('left', -this.panelWidth);
        },
        slideLeft: function (e) {
            if (e) e.preventDefault();
            this.$slideboxOuter.css('left', 0);
        },
        validatelogin: function(emailAddr, pass) {
            var emailReg = Backbone.Validation.patterns.email;
            var validation = false, msg1, msg2; 

            var MessageModel = Backbone.MozuModel.extend({});
            var MessageCollection = new Backbone.Collection();

            var messageView = messageViewFactory({
                el: $('[data-ign-message-bar]'),
                model: MessageCollection
            });

            var messageModel = null;

            if (!emailAddr || !emailReg.test( emailAddr )) {
                validation = false;
                msg1 = this.displayCustomMessage(Hypr.getLabel('emailAddressMissing'), false, "email");
                messageModel = new MessageModel();
                messageModel.set({message: Hypr.getLabel('emailAddressMissing')});
                MessageCollection.add(messageModel);
            } else if(emailAddr){
                validation = true;
                this.displayCustomMessage("", false, 'email');
                this.$parent.find('[data-mz-login-email]').removeClass('is-invalid');
            }
            if (!pass) { 
                validation = false;
                msg2 = this.displayCustomMessage(Hypr.getLabel("passwordMissing"), false, "password");
                messageModel = new MessageModel();
                messageModel.set({message: Hypr.getLabel('passwordMissing')});
                MessageCollection.add(messageModel);
            }
            else if(pass && validation){
                validation = true;
                this.displayCustomMessage("", false, 'password');
                this.$parent.find('[data-mz-login-password]').removeClass('is-invalid');
            }else{
                this.displayCustomMessage("", false, 'password');
                this.$parent.find('[data-mz-login-password]').removeClass('is-invalid');
            }
            messageView.render();

            return validation ? true : false;
        },
        login: function () {
            console.log("In login links");
        	var self = this,
            emailAddr = this.$parent.find('[data-mz-login-email]').val(),
            pass = this.$parent.find('[data-mz-login-password]').val();
            
          //NGCOM-623
            //If a returnUrl has been specified in the url query and there
            //is no returnUrl value provided by the server,
            //we'll use the one specified in the url query. If a returnURl has been
            //provided by the server, it will live in an invisible input in the
            //login links box.
            
			var returnUrl, returnUrlParam = "";
			if (window.location.search.indexOf('returnurl') > 0) {
				returnUrlParam = (window.location.search.split('returnurl=')[1] || '');
            }
			if (returnUrlParam && !this.$parent.find('input[name=returnUrl]').val()){
			  returnUrl = returnUrlParam;
			} else {
			  returnUrl = (window.location.search.split('returnurl=')[1] || '');
			}
            if (this.validatelogin(emailAddr, pass)) {
                this.setLoading(true);
                api.action('customer', 'loginStorefront', {
                    email: this.$parent.find('[data-mz-login-email]').val(),
                    password: this.$parent.find('[data-mz-login-password]').val()
                }).then(this.handleLoginComplete.bind(this, returnUrl,emailAddr,pass), this.displayApiMessage);
            }
        },
        
        anonymousorder: function() {
            var email = "";
            var billingZipCode = "";
            var billingPhoneNumber = "";

            switch (this.$parent.find('[data-mz-verify-with]').val()) {
                case "zipCode":
                    {
                        billingZipCode = this.$parent.find('[data-mz-verification]').val();
                        email = null;
                        billingPhoneNumber = null;
                        break;
                    }
                case "phoneNumber":
                    {
                        billingZipCode = null;
                        email = null;
                        billingPhoneNumber = this.$parent.find('[data-mz-verification]').val();
                        break;
                    }
                case "email":
                    {
                        billingZipCode = null;
                        email = this.$parent.find('[data-mz-verification]').val();
                        billingPhoneNumber = null;
                        break;
                    }
                default:
                    {
                        billingZipCode = null;
                        email = null;
                        billingPhoneNumber = null;
                        break;
                    }

            }

            this.setLoading(true);
            // the new handle message needs to take the redirect.
            api.action('customer', 'orderStatusLogin', {
                ordernumber: this.$parent.find('[data-mz-order-number]').val(),
                email: email,
                billingZipCode: billingZipCode,
                billingPhoneNumber: billingPhoneNumber
            }).then(function () { window.location.href = (HyprLiveContext.locals.siteContext.siteSubdirectory||'') +  "/my-anonymous-account?returnUrl="+(HyprLiveContext.locals.siteContext.siteSubdirectory||'')+"/myaccount"; }, _.bind(this.retrieveErrorLabel, this));
        },
        retrievePassword: function () {
            this.setLoading(true);
            api.action('customer', 'resetPasswordStorefront', {
                EmailAddress: this.$parent.find('[data-mz-forgotpassword-email]').val()
            }).then(_.bind(this.displayResetPasswordMessage,this), this.displayApiMessage);
        },
        handleLoginComplete: function (returnUrl, email, pass) {
            console.log("handleLoginComplete");
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            locale = locale.split('-')[0];
            var currentLocale = '';
            var me = this;
            if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
                currentLocale = locale === 'fr' ? '/fr' : '/en';
            }
            
            api.request('POST', '/api/commerce/customer/authtickets/', {
                password: pass,
                username: email
            }).then(function (response) {
                var accountId = response.customerAccount.id;
                var productCode  = $.urlParam('productCode');
                console.log("productCode",productCode);
                var userStore = isHomeFurnitureSite ? Hypr.getThemeSetting('hfPreferredStore') : Hypr.getThemeSetting('preferredStore');   
                console.log("userStore",userStore);             
                api.request('GET', '/api/commerce/customer/accounts/'+accountId+'/attributes/'+userStore).then(function(data){
                    console.log("data",data);
                    var customerStore = data.values[0]; 
                    if(localStorage.getItem('preferredStore')){
                        var currentStore = $.parseJSON(localStorage.getItem('preferredStore'));
                        if(customerStore != 'N/A' && customerStore != currentStore.code){
                            var cartModelData = new CartModels.Cart();
                            var cartCount = null;
                            cartModelData.on('sync', function () {
                                cartCount = cartModelData.count();
                            });
                            var itemsInCart = [];
                            cartModelData.apiGet().then(function (response) {
                                if (cartCount > 0) {
                                    itemsInCart = response.data.items;
                                }
                                $.ajax({
                                    url: "/set-purchase-location", 
                                    data :{ "purchaseLocation": customerStore}, 
                                    type:"GET",
                                    success:function(response){
                                        api.get("location",{code:customerStore}).then(function(response){
                                            var selectedStore = response.data;
							                CookieUtils.setPreferredStore(selectedStore);
                                            $.removeCookie('preferredStore_userConfirmationRequired', { path: '/' });
                                            //$.cookie("sessionStore",JSON.stringify(selectedStore.code),{path:'/'});
                                            if(itemsInCart.length > 0){
                                                var previousItemQuantity = [];
                                                _.each(itemsInCart, function(item){
                                                    var prodObj = {
                                                        'quantity' : item.quantity,
                                                        'productCode' : item.product.productCode,
                                                        'price': item.product.price.price,
                                                        'salePrice': item.product.price.salePrice
                                                    };
                                                    previousItemQuantity.push(prodObj);
                                                });
                                                if (ua.indexOf('chrome') > -1) {
                                                    if (/edg|edge/i.test(ua)){
                                                        $.cookie("previousItems",JSON.stringify(previousItemQuantity),{path:'/'});
                                                    }else{
                                                        document.cookie = "previousItems="+JSON.stringify(previousItemQuantity)+";path=/;Secure;SameSite=None";
                                                    }
                                                } else {
                                                    $.cookie("previousItems",JSON.stringify(previousItemQuantity),{path:'/'});
                                                }
                                            }
                                            if ( returnUrl ){
                                                window.location.href= returnUrl;
                                            }else if (productCode){                                        	
                                                window.location.href = currentLocale+"/p/" + productCode + "#addToWishlist";
                                            }else {
                                                window.location.href = "/myaccount";
                                            }
                                        });
                                    },
                                    error: function (error) {
                                        console.log(error);
                                    }
                                });
                            });
                        }else{
                            if(customerStore == currentStore.code) {
                                $.removeCookie('preferredStore_userConfirmationRequired', { path: '/' });
                            }
                            if ( returnUrl ){
                                window.location.href= returnUrl;
                            }else if (productCode){                            	
                            	window.location.href = currentLocale+"/p/" + productCode + "#addToWishlist";                            	
                            }else{
                                window.location.href = "/myaccount";
                            }
                        }
                    }else{
                    	if(customerStore != 'N/A'){
                    		$.ajax({
                                url: "/set-purchase-location", 
                                data :{ "purchaseLocation": customerStore}, 
                                type:"GET",
                                success:function(response){
                                    api.get("location",{code:customerStore}).then(function(response){
                                        var selectedStore = response.data;
							            CookieUtils.setPreferredStore(selectedStore);
                                        $.removeCookie('preferredStore_userConfirmationRequired', { path: '/' });
                                        //$.cookie("sessionStore",JSON.stringify(selectedStore.code),{path:'/'});
                                        if ( returnUrl ){
                                            window.location.href= returnUrl;
                                        }else if (productCode){                                        	
                                        	window.location.href = currentLocale+"/p/" + productCode + "#addToWishlist";
                                        }else {
                                            window.location.href = "/myaccount";
                                        }
                                    });
                                },
                                error: function (error) {
                                    console.log(error);
                                }
                            });
                    	}else{
                            if ( returnUrl ){
                                window.location.href= returnUrl;
                            }else if (productCode){                            	
                            	window.location.href = currentLocale+"/p/" + productCode + "#addToWishlist";                            	
                            }else{
                                window.location.href = "/myaccount";
                            }
                        }
                    }
                });
           });
            
            //delete instorePickUpStep from cookie
            if($.cookie('inStorePickUpStep')){
            	$.removeCookie('inStorePickUpStep', { path: '/' });
            }
            if($.cookie('recentlyViewd')){
            	$.removeCookie('recentlyViewd', { path: '/' });
            }
        },
        displayResetPasswordMessage: function () {
        	$('.forgot-password-container').hide();
            $('.forgot-success-message').removeClass('hidden').html(Hypr.getLabel('resetEmailSentMessage'));
        }
    });
    
    
    var SignupPopover = function() {
        DismissablePopover.apply(this, arguments);
        this.signup = _.debounce(this.signup, 150);
    };
    SignupPopover.prototype = new DismissablePopover();
    $.extend(SignupPopover.prototype, LoginPopover.prototype, {
        boundMethods: ['handleEnterKey', 'dismisser', 'displayMessage', 'displayApiMessage', 'createPopover', 'signup', 'onPopoverShow'],
        template: Hypr.getTemplate('modules/common/signup-popover').render(),
        bindListeners: function (on) {
            var onOrOff = on ? "on" : "off";
            this.$parent[onOrOff]('click', '[data-mz-action="signup"]', this.signup);
            this.$parent[onOrOff]('keypress', 'input', this.handleEnterKey);
        },
        handleEnterKey: function (e) {
            if (e.which === 13) { this.signup(); }
        },
        
       
        validate: function (payload, firstName, lastName, emailAddr, pass, aeroplanNumber, aeroplanLastName) {
        	
            var emailReg = Backbone.Validation.patterns.email;
            var nameReg = /\s/g;
            
            var validation = false, msg1, msg2, msg3, msg4, msg5, msg6, msg7;
            var isEmailValid = false;
            var isFnameValid = false;
            var isLnameValid = false;
            var isAeroplanNumber = false;
            var isAeroplanLastName = false;
            var aeroplanAvailableClass = $('#signupAeroplanSection').hasClass('aeroplanAvailable');

            var MessageModel = Backbone.MozuModel.extend({});
            var MessageCollection = new Backbone.Collection();

            var messageView = messageViewFactory({
                el: $('[data-mz-message-bar]'),
                model: MessageCollection
            });

            var messageModel = null;

            if (!firstName || nameReg.test(firstName) ) {
                isFnameValid = false;
                msg4 = this.displayCustomMessage(Hypr.getLabel('fnameMissing'), true, "firstname");
                messageModel = new MessageModel();
                messageModel.set({message: Hypr.getLabel('fnameMissing')});
                this.$parent.find('[data-mz-signup-firstname]').addClass('is-invalid');
                MessageCollection.add(messageModel);
            } else {
            	isFnameValid = true;
                this.displayCustomMessage("", true, 'firstname');
                this.$parent.find('[data-mz-signup-firstname]').removeClass('is-invalid');
            }
            if (!lastName || nameReg.test(lastName)) {
                isLnameValid = false;
                msg5 = this.displayCustomMessage(Hypr.getLabel('lnameMissing'), true, "lastname");
                messageModel = new MessageModel();
                messageModel.set({message: Hypr.getLabel('lnameMissing')});
                this.$parent.find('[data-mz-signup-lastname]').addClass('is-invalid');
                MessageCollection.add(messageModel);
            } else {
            	isLnameValid = true;
                this.displayCustomMessage("", true, 'lastname');
                this.$parent.find('[data-mz-signup-lastname]').removeClass('is-invalid');
            }
            if (!emailAddr.val() || !emailReg.test(emailAddr.val() )) {
            	isEmailValid = false;
                msg1 = this.displayCustomMessage(Hypr.getLabel('emailMissing'), true, 'emailAddress');
                messageModel = new MessageModel();
                messageModel.set({message: Hypr.getLabel('emailMissing')});
                MessageCollection.add(messageModel);
            } else {
            	isEmailValid = true;
                this.displayCustomMessage("", true, 'emailAddress');
                this.$parent.find('[data-mz-signup-emailaddress]').removeClass('is-invalid');
            }

            if (!payload.password) {
                validation = false;
                msg2 = this.displayCustomMessage(Hypr.getLabel('passwordMissing'), true, 'password');
                messageModel = new MessageModel();
                messageModel.set({message: Hypr.getLabel('passwordMissing')});
                MessageCollection.add(messageModel);
            } else {
                    var passwordRegex = new RegExp("^(?=.*[a-z])(?=.*[0-9])(?=.{6,})");
                    var isvalidpassword = passwordRegex.test(payload.password);
                    if(!isvalidpassword) {
                        validation = false;
                        msg2 = this.displayCustomMessage(Hypr.getLabel('invalidPassword'), true, 'password');
                        messageModel = new MessageModel();
                        messageModel.set({message: Hypr.getLabel('invalidPassword')});
                        MessageCollection.add(messageModel);
                        $(".passwordlimitmsg").hide();
                    }
                    else{
                        validation = true;
                        this.displayCustomMessage("", true, 'password');
                        this.$parent.find('[data-mz-signup-password]').removeClass('is-invalid');
                        $(".passwordlimitmsg").show();
                    }
            }

            if (payload.password !== this.$parent.find('[data-mz-signup-confirmpassword]').val()) {
                validation = false;
                msg3 = this.displayCustomMessage(Hypr.getLabel('passwordsDoNotMatch'), true, 'confirmPassword');
                messageModel = new MessageModel();
                messageModel.set({message: Hypr.getLabel('passwordsDoNotMatch')});
                MessageCollection.add(messageModel);
            } else {
                var confirmpasswordRegex = new RegExp("^(?=.*[a-z])(?=.*[0-9])(?=.{6,})");
                    var isvalidconfirmpassword = confirmpasswordRegex.test(this.$parent.find('[data-mz-signup-confirmpassword]').val());
                    if(!isvalidconfirmpassword) {
                        validation = false;
                        msg3 = this.displayCustomMessage(Hypr.getLabel('invalidPassword'), true, 'confirmPassword');
                        messageModel = new MessageModel();
                        messageModel.set({message: Hypr.getLabel('invalidPassword')});
                        MessageCollection.add(messageModel);
                    }
                    else{
                        validation = true;
                        this.displayCustomMessage("", true, 'confirmPassword');
                        this.$parent.find('[data-mz-signupaeroplanAvailableClass-confirmpassword]').removeClass('is-invalid');
                        this.$parent.find('[data-mz-signup-confirmpassword]').removeClass('is-invalid');
                    }
            }
            if(aeroplanAvailableClass){
            	if (!aeroplanNumber || aeroplanNumber.length != 9) {
                    isAeroplanNumber = false;
                    msg6 = this.displayCustomMessage(Hypr.getLabel('aeroplanNumberMissingInvalid'), false, 'aeroplanNumber');
                    messageModel = new MessageModel();
                    messageModel.set({message: Hypr.getLabel('aeroplanNumberMissingInvalid')});
                    $('#signupAeroplanNumberInput').addClass('is-invalid');
                    $('.digit-length').addClass('is-invalid');
                    MessageCollection.add(messageModel);
                } else {
                	isAeroplanNumber = true;
                    this.displayCustomMessage("", false, 'aeroplanNumber');
                    $('#signupAeroplanNumberInput').removeClass('is-invalid');
                    $('.digit-length').removeClass('is-invalid');
                }
            	
            	if (!aeroplanLastName) {
            		isAeroplanLastName = false;
	                msg7 = this.displayCustomMessage(Hypr.getLabel('lnameMissing'), false, "aeroplanLastName");
	                messageModel = new MessageModel();
	                messageModel.set({message: Hypr.getLabel('lnameMissing')});
	                $('#signupAeroplanLastName').addClass('is-invalid');
	                MessageCollection.add(messageModel);
	            } else {
	            	isAeroplanLastName = true;
	                this.displayCustomMessage("", false, 'aeroplanLastName');
	                $('#signupAeroplanLastName').removeClass('is-invalid');
	            }
            }

            //messageView.render();
            if(aeroplanAvailableClass){
                if(!(isFnameValid && isLnameValid && validation && isEmailValid && isAeroplanNumber && isAeroplanLastName)){
                    $("html, body").animate({ scrollTop: 400 }, "slow");
                }
                return isFnameValid && isLnameValid && validation && isEmailValid && isAeroplanNumber && isAeroplanLastName ? true : false;
            }
            else{
                
                if(!(isFnameValid && isLnameValid && validation && isEmailValid)){
                    $("html, body").animate({ scrollTop: 400 }, "slow");
                }
            	return isFnameValid && isLnameValid && validation && isEmailValid ? true : false;
            }
        },
        signup: function () {
        	var aeroplanMemberNumberValue = $('#signupAeroplanNumberInput').val();
            var currentStore;
            if(localStorage.getItem('preferredStore')){
               currentStore = $.parseJSON(localStorage.getItem('preferredStore'));
           
                var contractor = this.$parent.find('input[name=contractormember]:checked').val();
                var aeroplan = this.$parent.find('input[name=aeroplanmember]:checked').val();
                var marketingPreference = this.$parent.find('input[name=subscribenewsletter]:checked').val();
                var aeroplanNumber;
                if(aeroplan) {
                    aeroplanNumber = aeroplanMemberNumberValue ? aeroplanMemberNumberValue : "N/A";
                }else {
                    aeroplanNumber = 'N/A';
                }
                var self = this,
                    emailAddr = this.$parent.find('[data-mz-signup-emailaddress]'),
                    pass = this.$parent.find('[data-mz-signup-password]'),
                    email = this.$parent.find('[data-mz-signup-emailaddress]').val(),
                    firstName = this.$parent.find('[data-mz-signup-firstname]').val(),
                    lastName = this.$parent.find('[data-mz-signup-lastname]').val(),
                    password = this.$parent.find('[data-mz-signup-password]').val(),
                    aeroplanLastName = aeroplan ? this.$parent.find('[data-mz-aeroplan-lastname]').val() : '',
                    acceptsMarketing = marketingPreference ? marketingPreference : false,
                    isContractor = contractor ? contractor : false,
                    isAeroplanMember = aeroplan ? aeroplan : false,
                    defaultStore = isHomeFurnitureSite ? Hypr.getThemeSetting('hhDefaultStore') : Hypr.getThemeSetting('hfDefaultStore'),
                    payload = {
                        account: {
                            acceptsMarketing: acceptsMarketing,
                            emailAddress: email,
                            userName: email,
                            firstName: firstName,
                            lastName: lastName,
                            aeroplanNumber: aeroplanNumber,
                            aeroplanLastName: aeroplanLastName,
                            contacts: [{
                                email: email,
                                firstName: firstName,
                                lastNameOrSurname: lastName
                            }],
                            attributes:[
                                {
                                fullyQualifiedName: isHomeFurnitureSite ? Hypr.getThemeSetting('hfPreferredStore') : Hypr.getThemeSetting('preferredStore'),
                                values: [currentStore.code]
                                },
                                {
                                    fullyQualifiedName: Hypr.getThemeSetting('isAeroplanMember'), 
                                    values: [isAeroplanMember]
                                },
                                {
                                    fullyQualifiedName: Hypr.getThemeSetting('aeroplanMemberNumber'), 
                                    values: [aeroplanNumber]
                                },
                                {
                                    fullyQualifiedName: Hypr.getThemeSetting('isContractor'),
                                    values: [isContractor]
                                },
                                {
                                    fullyQualifiedName: Hypr.getThemeSetting('recentlyViewed'),
                                    values: ['N/A']
                                },
                                {
                                fullyQualifiedName: isHomeFurnitureSite ? Hypr.getThemeSetting('preferredStore') : Hypr.getThemeSetting('hfPreferredStore'),
                                values: [defaultStore]
                                }
                            ]
                        },
                        password: password
                    };
                var MessageCollection = new Backbone.Collection();
                var messageView = messageViewFactory({
                    el: $('[data-ign-message-bar]'),
                    model: MessageCollection
                });
                messageView.render();    
                if (this.validate(payload, firstName, lastName, emailAddr, pass, aeroplanNumber, aeroplanLastName)) {   
                    //var user = api.createSync('user', payload);
                    var acceptsMarketingStatus = payload.account.acceptsMarketing;
                    var isContractorStatus = _.findWhere(payload.account.attributes, {fullyQualifiedName: Hypr.getThemeSetting('isContractor')}).values[0];
                    if(isContractorStatus){
                        isContractorStatus = "Yes";
                    } else {
                        isContractorStatus = "No";
                    }
                    this.setLoading(true);
                    return api.action('customer', 'createStorefront', payload).then(function (response) {
                        dataLayer.push({
                            'event':'createAccount',
                            'userId':response.data.customerAccount.userId
                        });
                        var returnUrl = "";
                        if (window.location.search.indexOf('returnurl') > 0) {
                            returnUrl = (window.location.search.split('returnurl=')[1] || '');
                        }
                        if (returnUrl) {
                            window.location.href = returnUrl;
                        } else {                		
                            /*Code for Relation1*/
                            
                            if(locale === "fr"){
                                locale = "French";
                            } else {
                                locale = "English";
                            }
                            var headers = {};
                            headers.Authorization = b_token;
                            var postalCode = $.parseJSON(localStorage.getItem('preferredStore')).address.postalOrZipCode;                    	              		
                            var now = new Date();
                            var timeStamp = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + (now.getDate()) + "-" + now.getHours() + ':' + ((now.getMinutes() < 10) ? ("0" + now.getMinutes()) : (now.getMinutes())) + ':' + ((now.getSeconds() < 10) ? ("0" + now.getSeconds()) : (now.getSeconds()));
                            var data = {
                                EmailAddress: emailAddr.val(),
                                FirstName: firstName,
                                PostalCode: postalCode,
                                Preferred_Language: locale,
                                Is_Contractor: isContractorStatus,
                                Source: "HH Ecommerce Website",
                                ConsentType: 'explicit'
                            };
                            var dataForRelation1 = "emailaddress="+emailAddr.val()+"&timeStamp="+timeStamp;
                            if(acceptsMarketingStatus) {
                                $.ajax({
                                    method: 'POST',
                                    headers: headers,  
                                    contentType: 'application/json; charset=utf-8',
                                    url: relation1InsertRecord,
                                    data: JSON.stringify(data),
                                    success:function(response){
                                        $.ajax({
                                            method: 'GET',
                                            url: Hypr.getThemeSetting('linkRelation1GetToken'),
                                            data: dataForRelation1,
                                            success:function(response){
                                                window.location.href = relation1Link + response;
                                            },
                                            error:function(error){
                                                console.log("error: " + JSON.stringify(error));
                                            }
                                        });
                                    },
                                    error: function (error) {
                                        console.log(error);
                                    }
                                });
                            }
                            $('.signup-container').hide();
                            $('.signup-success-message').removeClass('hidden');
                            $('.recently-viewed-on-signup').removeClass('hidden');
                            $('.signup-header').addClass('hidden');
                            $('.singin-link').addClass('hidden');
                            $('body').animate({scrollTop: 100});
                        }
                    }, self.displayApiMessage);
                }
            }else{
                $('#goToStoreLocator').modal().show();
            }    
        } 
    });
    $(document).on("pageload",function() {
        $('.mz-user-firstname').html(' ');
    });
    
    $(document).ready(function() {
    	var locale = require.mozuData('apicontext').headers['x-vol-locale'], apiContext = require.mozuData('apicontext');
    	locale = locale.split('-')[0];
    	if(locale === 'en' && siteId !== Hypr.getThemeSetting('homeFurnitureSiteId')){ //execute this code only for English site
    		// Set searchField to the search input field.
    		// Set timeout to the time you want to wait after the last character in milliseconds.
    		// Set minLength to the minimum number of characters that constitutes a valid search.
    			var searchField = $('#search-input'),
    			autocompleteCategory = $('.search-category'),
    			autocompleteProduct = $('.search-product'),
    			timeout = 2000,
    			minLength = 2;
    			
    			var textEntered = false;
    			
    			var timer, searchText;
    			
    			$('.tt-dropdown-menu').on('click', '.search-category', function(event){
    				window.clearTimeout(timer);
    				var categoryId = $(event.currentTarget).data('category-id');
    				searchText = searchField.val();
    				handleInput("Category Autocompelete Click", searchText+"#"+categoryId);
    			});
    			
    			$('.tt-dropdown-menu').on('click', '.search-product', function(event){
    				window.clearTimeout(timer);
    				var productCode = $(event.currentTarget).data('product-code');
    				searchText = searchField.val();
    				handleInput("Product Autocompelete Click",searchText+"#"+productCode);
    			});
    			
    		var handleInput = function(eventType, eventLabel) {
    			searchText = searchField ? searchField.val() : '';
    			if (searchText!== null && searchText.length >= minLength) {
    				return false;
    			}
    			/*window.dataLayer.push({
    				event: 'typeahead',
    				eventCategory: 'Search Autocomplete',
    				eventAction: eventType,
    				eventLabel: eventLabel,
    				eventValue : 1,
    				nonInteraction : false
    			});*/
    			textEntered = false;
    		};
    		if(!user.isAnonymous && !user.isAuthenticated){
                $.ajax({
                    method: 'GET',
                    url: '/logout',
                    complete: function() { location.reload();}
                });
            }
    		var startTimer = function(e) {
    			textEntered = true;
    			window.clearTimeout(timer);
    			searchText = searchField.val();
    			if (e.keyCode === 13) {
    		        handleInput("Autocomplete Typeahead", searchText);
    		        return;
    			}
    			timer = setTimeout(handleInput("Autocomplete Typeahead", "Search Term"), timeout);
    		};
    		
    		if (searchField) {
    			searchField.keyup(startTimer);
    		}
    	}
		
    	setTimeout(function() {
	    	if($('#mobile-search #searchbox').hasClass('active')){
	    		$('#page-content').addClass('addTopOnMobile');
				if(require.mozuData('pagecontext').cmsContext.page.path === 'flyer'){
					$('#page-content').addClass('removeBottom');
				}
	    	}
    	}, 1000);

    	//Popover for longin keep me sign in
    	$("[data-toggle=popover]").popover({
            html : true,
            content: function() {
              var content = $(this).attr("data-popover-content");
              return $(content).children(".popover-body").html();
            },
            title: function() {
              var title = $(this).attr("data-popover-content");
              return $(title).children(".popover-heading").html();
            }
        });
    	
        // Removing is-loading class on load forgot password page
        $('.forgot-password-cont > .btn-block').removeClass('is-loading');
    	
        $(document).on("changeUserName", function() {
            var cust = api.get('customer', {
                id: user.accountId
            }).then(function(response) {
            $('.mz-user-firstname').html(response.data.firstName);
            });
        });
        
        var fName = document.getElementById('firstname');
        if (fName) {
            fName.addEventListener('keydown', function (event) {
                if (event.which === 32) {
                    fName.value = fName.value.replace(/^\s+/, '');
                    if (fName.value.length === 0) {
                        event.preventDefault();
                    }
                }
            });
        }

        var lName = document.getElementById('lastname');
        if (lName) {
            lName.addEventListener('keydown', function (event) {
                if (event.which === 32) {
                    lName.value = lName.value.replace(/^\s+/, '');
                    if (lName.value.length === 0) {
                        event.preventDefault();
                    }
                }
            });
        }
        var lAeroplanName = document.getElementById('signupAeroplanLastNameInput');
        if (lAeroplanName) {
        	lAeroplanName.addEventListener('keydown', function (event) {
                if (event.which === 32) {
                	lAeroplanName.value = lAeroplanName.value.replace(/^\s+/, '');
                    if (lAeroplanName.value.length === 0) {
                        event.preventDefault();
                    }
                }
            });
        }

        
        //preferred store view
        var PreferredStoreView = Backbone.MozuView.extend({
            templateName: 'modules/location/preferred-store',
            additionalEvents: {
            "click .changePreferredStore": "changePreferredStore"
            },
            changePreferredStore: function(e){
                e.preventDefault();
                var locale = require.mozuData('apicontext').headers['x-vol-locale'];
                var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
                locale = locale.split('-')[0];
                var currentLocale = '';
                if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
                currentLocale = locale === 'fr' ? '/fr' : '/en';
                }
                window.location.href=currentLocale + "/store-locator?returnUrl="+window.location.pathname;
            }
        });
        console.log("before store check");
        var preferredStore;    
        if(localStorage.getItem('preferredStore')){    
            preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
            var screenWidth = window.matchMedia("(max-width: 767px)");
            if (screenWidth.matches) {
                $("#example-navbar-collapse .mz-sitenav-item .primary-menu-nav").find(".flyer-enabled-mobile").removeClass("hidden");
            }else{
                $("#example-navbar-collapse .featuredCategories .mz-sitenav-item").find(".flyer-enabled-dekstop").removeClass("hidden-lg hidden-md hidden-sm");
            }
            var clearanceLink = $("#example-navbar-collapse .feturedCategoriesOfMobile .mz-sitenav-item").find(".mobile-clearance-enabled").attr("href");
            if(!clearanceLink) {
				clearanceLink = $("#example-navbar-collapse .featuredCategories .mz-sitenav-item").find(".clearance-link").attr("href");
			}
           
            clearanceLink += preferredStore.code;
            $("#example-navbar-collapse .featuredCategories .mz-sitenav-item .deals-dropdown").find(".clearance-link").attr("href",clearanceLink);
            $("#example-navbar-collapse .featuredCategories .mz-sitenav-item").find(".mobile-clearance-enabled").attr("href",clearanceLink);
            $(".mz-navright-list .mz-navright-item").find(".mobile-clearance-link").attr("href",clearanceLink);

            var onSaleLink = $("#example-navbar-collapse .feturedCategoriesOfMobile .mz-sitenav-item .child-tabs").find(".mobile-onsale-enabled").attr("href");
            if(!onSaleLink) {
				onSaleLink = $("#example-navbar-collapse .featuredCategories .mz-sitenav-item .deals-dropdown").find(".onSale").attr("href");
			}
            onSaleLink += preferredStore.code;
            $("#example-navbar-collapse .featuredCategories .mz-sitenav-item .deals-dropdown").find(".onSale").attr("href",onSaleLink);
            $("#example-navbar-collapse .featuredCategories .mz-sitenav-item").find(".mobile-onsale-enabled").attr("href",onSaleLink);
            $(".mz-navright-list .mz-navright-item").find(".mobile-onsale-link").attr("href",onSaleLink);
        }else{
            preferredStore = {
                noPreferredStore:true
            };
        }
        if(preferredStore){
            var preferredStoreView = new PreferredStoreView({
                                        el: $("#preferred-store"),
                                        model: new Backbone.Model(preferredStore)
                                    });
            preferredStoreView.render();
        }
        $("#selectPreferStoreBtn").click(function(e){
            e.preventDefault();
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            locale = locale.split('-')[0];
            var currentLocale = locale === 'fr' ? '/fr' : '/en';
            window.location.href=currentLocale + "/store-locator?returnUrl="+window.location.pathname;
        });
        var SessionSharingView = Backbone.MozuView.extend({
        	templateName: 'modules/session-sharing-links/session-sharing-links',
        	additionalEvents: {
        		"click [data-mz-lang]": "changeSite"
        	},
        	initialize: function() {
        		var urlData = this.makeUrlToLand();
        		var apiContext = require.mozuData('apicontext'), tenantId = apiContext.headers["x-vol-tenant"], siteId = apiContext.headers["x-vol-site"];
        		this.model.set('tenantId', urlData.tenantId);
        		this.model.set('siteId', urlData.siteId);
        		this.model.set('locationPath', urlData.locationPath);
                this.model.set('search', urlData.search);
        	},
        	changeSite: function(e) {
                e.preventDefault();
                sessionStorage.removeItem('allCategories');
        		var urlData = this.makeUrlToLand();
        		
        		if(!require.mozuData('pagecontext').isEditMode && Hypr.getThemeSetting('setPreferredLanguage')) {
        			var siteId = apiContext.headers["x-vol-site"];
	        		var expiryDate = new Date();
	            	expiryDate.setYear(expiryDate.getFullYear() + 1);
	            	
	        		if(siteId === Hypr.getThemeSetting('enSiteId')) { //current site is english

	        			if (ua.indexOf('chrome') > -1) {
                            if (/edg|edge/i.test(ua)){
                                $.cookie("hhLanguage", 'fr', {path:'/', expires: expiryDate});
                            }else{
                                document.cookie = "hhLanguage=fr;expires="+expiryDate+";path=/;Secure;SameSite=None";
                            }
                        } else {
                            $.cookie("hhLanguage", 'fr', {path:'/', expires: expiryDate});
                        }
	        		}else if(siteId === Hypr.getThemeSetting('frSiteId')) {
	        			if (ua.indexOf('chrome') > -1) {
                            if (/edg|edge/i.test(ua)){
                                $.cookie("hhLanguage", 'en', {path:'/', expires: expiryDate});
                            }else{
                                document.cookie = "hhLanguage=en;expires="+expiryDate+";path=/;Secure;SameSite=None";
                            }
                        } else {
                            $.cookie("hhLanguage", 'en', {path:'/', expires: expiryDate});
                        }
	        		}
        		}
        		
        		if(Hypr.getThemeSetting('selectedEnvironment') === 'DEV') { 
        			window.location.href = Hypr.getThemeSetting('hhWebsiteLink') + urlData.locationPath + urlData.search;
        		}else if(Hypr.getThemeSetting('selectedEnvironment') === 'PROD') {
        			window.location.href = Hypr.getThemeSetting('hhWebsiteLink') + urlData.locationPath + urlData.search;
        		}
        	},
        	makeUrlToLand: function() {
        		var apiContext = require.mozuData('apicontext'), tenantId = apiContext.headers["x-vol-tenant"],
        		siteId = apiContext.headers["x-vol-site"], locationPath = window.location.pathname, landingSiteId = '', search = '', pageType = require.mozuData('pagecontext').pageType,
        		tempLoc = '', newTempLoc = '';
        		if(window.location.search) {
        			var paramsArray = window.location.search.split('&');
        			_.each(paramsArray, function(param, index) {
        				if(param && param.indexOf('categoryId') !== -1) {
        					paramsArray.splice(index, 1);
        				}
        			});
        			var newSearch = paramsArray.join('&');
        			search = newSearch.indexOf('returnurl') == -1 ? newSearch : '';
        		}
        		if(siteId === Hypr.getThemeSetting('enSiteId')) { //current site is english
        			landingSiteId = Hypr.getThemeSetting('frSiteId');
        			if(locationPath.indexOf('/en') != -1){
        				locationPath = locationPath.replace('/en', '/fr');
        				if(pageType === 'category' && (locationPath.indexOf('/c/')>0) && require.mozuData('pagecontext').categoryId != Hypr.getThemeSetting('clearanceCategoryId')) {
        					tempLoc = locationPath.split('/fr/');
        					newTempLoc = tempLoc[1].split('/c/');
        					locationPath = '/fr/c/' + newTempLoc[1];
        				}
        			}else{
        				locationPath = '/fr' + locationPath;
        			}
        		}else if(siteId === Hypr.getThemeSetting('frSiteId')){
        			landingSiteId = Hypr.getThemeSetting('enSiteId');
        			if(locationPath.indexOf('/fr') != -1){
        				locationPath = locationPath.replace('/fr', '/en');
        				if(pageType === 'category' && (locationPath.indexOf('/c/')>0) && require.mozuData('pagecontext').categoryId != Hypr.getThemeSetting('clearanceCategoryId')) {
        					tempLoc = locationPath.split('/en/');
        					newTempLoc = tempLoc[1].split('/c/');
        					locationPath = '/en/c/' + newTempLoc[1];
        				}
        			}else{
        				locationPath = '/en' + locationPath;
        			}
        		}
        		var urlData = {
        				'tenantId': tenantId,
        				'siteId': siteId,
        				'landingSiteId': landingSiteId,
        				'locationPath': locationPath,
        				'search': search
        		};
        		return urlData;
            }
        });
        var sessionSharingView = new SessionSharingView({
            el: $(".frLink"),
            model: new Backbone.Model()
        });
        sessionSharingView.render();

        $docBody = $(document.body);

        $('[data-mz-action="login"]').each(function() {
            var popover = new LoginPopover();
            popover.init(this);
            $(this).data('mz.popover', popover);
        });
        $('[data-mz-action="signup"]').each(function() {
            var popover = new SignupPopover();
            popover.init(this);
            $(this).data('mz.popover', popover);
        });
        $('[data-mz-action="continueAsGuest"]').on('click', function(e) {
            e.preventDefault();
            var returnURL = returnUrl();
            if(returnURL .indexOf('checkout') === -1) {
                returnURL = '';
            }

            //saveUserId=true Will logut the current user while persisting the state of the current shopping cart
            $.ajax({
                    method: 'GET',
                    url: '/logout?saveUserId=true&returnUrl=' + returnURL,
                    complete: function(data) {
                        location.href = require.mozuData('pagecontext').secureHost + '/' + returnURL;
                        if($.cookie('inStorePickUpStep')){
                        	$.removeCookie('inStorePickUpStep', { path: '/' });
                        }
                    }
            });

        });
        $('[data-mz-action="launchforgotpassword"]').each(function() {
            var popover = new LoginPopover();
            popover.init(this);
            $(this).data('mz.popover', popover);
        });
        $('[data-mz-action="signuppage-submit"]').each(function(){
            var signupPage = new SignupPopover();
            signupPage.formSelector = 'form[name="mz-signupform"]';
            signupPage.pageType = 'signup';
            //signupPage.redirectTemplate = 'myaccount';
            signupPage.init(this);
        });
        $('[data-mz-action="loginpage-submit"]').each(function(){
            var loginPage = new SignupPopover();
            loginPage.formSelector = 'form[name="mz-loginform"]';
            loginPage.pageType = 'login';
            loginPage.init(this);
        });
        $('[data-mz-action="anonymousorder-submit"]').each(function () {
            var loginPage = new SignupPopover();
            loginPage.formSelector = 'form[name="mz-anonymousorder"]';
            loginPage.pageType = 'anonymousorder';
            loginPage.init(this);
        });
        $('[data-mz-action="forgotpasswordpage-submit"]').each(function(){
            var loginPage = new SignupPopover();
            loginPage.formSelector = 'form[name="mz-forgotpasswordform"]';
            loginPage.pageType = 'retrievePassword';
            loginPage.init(this);
        });
        $('[data-mz-action="resetpassword-submit"]').each(function() {
            $('[data-mz-action="resetpassword-submit"]').attr('disabled',true);
            $('[data-mz-new-password]').keyup(function(){
                if($(this).val().length !== 0){
                    $('[data-mz-action="resetpassword-submit"]').attr('disabled', false);
                }
                else
                {
                    $('[data-mz-action="resetpassword-submit"]').attr('disabled', true);        
                }
            });
        });
       
        $(document).on('click', '#storecollapse', function() {
            if ($(this).hasClass('collapsed')) {
                $(document).find('.mz-sitenav').toggleClass('top-border');
                $(document).find("#mobile-search #searchbox").removeClass("drawer-open-search").addClass("add-top active");
            } else {
                $(window).on('scroll',function(){
                if($(this).scrollTop() > 0){
                    if($(window).width()< 768){
                            $(".mystore-details").removeClass('in'); 
                        }
                	}
                });
                $(document).find('.mz-sitenav').toggleClass('top-border');
                $(document).find("#mobile-search #searchbox").addClass("drawer-open-search").removeClass("add-top");
            }
        });
        var id = '#dialog';
        var isIpBasedStore = $.cookie("preferredStore_userConfirmationRequired") ? $.parseJSON($.cookie("preferredStore_userConfirmationRequired")) : undefined;
        //transition effect
        if($.cookie("showDialog")){
            if (localStorage.getItem('preferredStore')){
                $("#no-store-dialog").hide();
                $(id).show();
                setTimeout(function(){
                    if($.cookie("showDialog")){
                        $.removeCookie('showDialog', { path: '/' });
                    }
                    $(id).fadeOut();
                },8000);

            }else{
                if(!isIpBasedStore) {
                    $(id).hide();
                    var showDialog = $.parseJSON($.cookie("showDialog"));
                    if(require.mozuData('pagecontext').cmsContext.page.path !== "store-locator"){
                        if(showDialog && !user.isAuthenticated && user.isAnonymous){
                            $("#no-store-dialog").show();
                            setTimeout(function(){
                                // $("#no-store-dialog").fadeOut();
                            },8000);
                        }
                    } 
                }        
            }         
        }else{
            if (localStorage.getItem('preferredStore')){
                $(id).hide();
            }else{  
                if(require.mozuData('pagecontext').cmsContext.page.path !== "store-locator"){
                	if(!user.isAuthenticated && user.isAnonymous){
	                    $("#no-store-dialog").show();
	                    setTimeout(function(){
	                        // $("#no-store-dialog").fadeOut();
	                    },8000);
                	}
                }    
            }
        }
        //if close button is clicked
        $('.window .close').click(function (e) {
            //Cancel the link behavior
            e.preventDefault();
            $('.window').hide();
            $.removeCookie('showDialog', { path: '/' });
        });  
        // Close no-store popover
        $('.window .no-store-close').click(function (e) {
            //Cancel the link behavior
            e.preventDefault();
            $('.window').hide();
        });

        $(".popbadge-link").click(function(){
            $.removeCookie('showDialog', { path: '/' });
        });
        
        $('#aeroplan-member').on('click',function(){
        	var self = $('#signupAeroplanSection');
	        if(self.hasClass('hidden')){
		    	$(self).removeClass('hidden').addClass('aeroplanAvailable');
	        }else{
	        	$(self).addClass('hidden').removeClass('aeroplanAvailable');
	        }
	        if(self.hasClass('aeroplanAvailable')){
	        	$('#lastname').change(function() {
	        	    $('#signupAeroplanLastName').val($(this).val());
	        	});
	        }
	        
	      //starts aeroplan 3 fields value
	        var inputQuantity = [];
	        $(function() {
	          $(".digit-length").each(function(i) {
	            inputQuantity[i]=this.defaultValue;
	             $(this).data("idx",i);
	          });
	          $(".digit-length").on("keyup", function (e) {
	            var $field = $(this),
	                val=this.value,
	                $thisIndex=parseInt($field.data("idx"),10); 
	            if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
	                this.value = inputQuantity[$thisIndex];
	                return;
	            } 
	            if (val.length > Number($field.attr("maxlength"))) {
	              val=val.slice(0, 5);
	              $field.val(val);
	            }
	            inputQuantity[$thisIndex]=val;
	            if($(this).val().length==$(this).attr("maxlength")){
	                $(this).next().focus();
	            }
	            var num1 = $('#number1').val();
	            var num2 = $('#number2').val();
	            var num3 = $('#number3').val();
	            var number = num1 + num2 + num3;
	            $('#signupAeroplanNumberInput').val(number);
	            
	          });      
	        });
        
        //end aeroplan 3 fields value
        });
    
        $(function(){
            $('#search-input').each(function (e) {
                $(this).keyup(function (evt) {
                    var code = evt.charCode || evt.keyCode;
                    if (code == 27) {
                        $(this).val('');
                    }
                });
            });

              function closeSearch() {
            	  if($('.tt-input').val()){
            		  $('.mz-searchbox-input').val('');
            	  } else {
            		  $('.mz-searchbox-input').val('');
                      $('.mz-searchbox-input').hide();
                      $('#searchbox button[type="reset"]').hide();
                    }
                }

              function openSearch() {
                  $('#searchbox').addClass('active');
                  $('.mz-searchbox-input').show();
                  $('#searchbox button[type="reset"]').show();
                  $('.mz-sitenav').removeClass('fixed-on-mobile-search'); 
                  $('.tt-input').focus().val('');
              }
              openSearch();
             
              // Show Search if form is not active or input search empty
              $('#searchbox button[type="submit"]').click(function(event) {
                  if(!$( "#searchbox" ).hasClass( "active" )) {
                      event.preventDefault();
                      openSearch();
                  }
                  $(this).parents().eq(6).find('.mz-pageheader').addClass('is-search-open');
                  //$('.mobilemenuclose').removeClass('mobilemenuclose').addClass('mobilemenutoggle');
                  $('#example-navbar-collapse').removeClass('in');
                                    
              });

              //close form
              $('#searchbox button[type="reset"]').click(function(event) {
                  //event.preventDefault();
                  closeSearch();
                  $(this).parents().eq(6).find('.mz-pageheader').removeClass('is-search-open');
              });
        });

        $('[data-mz-action="logout"]').each(function(){
            var el = $(this);

            //if were in edit mode, we override the /logout GET, to preserve the correct referrer/page location | #64822
            if (require.mozuData('pagecontext').isEditMode) {

                 el.on('click', function(e) {
                    e.preventDefault();
                    $.ajax({
                        method: 'GET',
                        url: '/logout',
                        complete: function() { location.reload();}
                    });
                });
            }

        });
        $('[data-mz-action="logout"]').on("click",function(e) {         
        	e.preventDefault();
            var preferredStore = localStorage.getItem('preferredStore');
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            locale = locale.split('-')[0];
            var currentLocal = '';
            if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
                currentLocal = locale === 'fr' ? '/fr' : '/en';
            }
        	$.ajax({
                method: 'GET',
                url: '/logout',
                success: function() {
                	if(preferredStore) {
                		var store = JSON.parse(preferredStore);
                		$.ajax({
                    		url: "/set-purchase-location", 
                    		data :{ "purchaseLocation": store.code}, 
                    		type:"GET",
                    		success:function(response){
                    			if($.cookie('inStorePickUpStep')){
                                	$.removeCookie('inStorePickUpStep', { path: '/' });
                                }
                    			if($.cookie('recentlyViewd')){
                                	$.removeCookie('recentlyViewd', { path: '/' });
                                }
                            	window.location.href = require.mozuData('pagecontext').secureHost + currentLocal;
                            	
                			},
                			error: function (error) {
                				// if arc action failed
                			}
                    	});
                	}else {
                		window.location.href = require.mozuData('pagecontext').secureHost + currentLocal;
                	}
                }
            });
        });
        
        /* password show-hide functionality */
        $(".show-hide").on("click",function(e){
        	if(e){
        		e.preventDefault();
        		e.stopPropagation();
        	}
            var inputElement = $(this).parent().find('input[name="password"]');
            if(inputElement.attr('type') == 'password'){
            	inputElement.attr('type', 'text');
                $(this).text($(this).data('label-hide'));
                $(this).attr("title", Hypr.getLabel('hidePassword'));
            }else{
            	inputElement.attr('type', 'password');
                $(this).text($(this).data('label-show'));
                $(this).attr("title", Hypr.getLabel('show'));
            }
        });
        $(".show-hide-pwd").on("click",function(e){
        	if(e){
        		e.preventDefault();
        		e.stopPropagation();
        	}
            var inputElement = $(this).parent().find('input[name="confirmPassword"]');
            if(inputElement.attr('type') == 'password'){
            	inputElement.attr('type', 'text');
                $(this).text($(this).data('label-hide'));
                $(this).attr("title", Hypr.getLabel('hidePassword'));
            }else{
            	inputElement.attr('type', 'password');
                $(this).text($(this).data('label-show'));
                $(this).attr("title", Hypr.getLabel('show'));
            }
        });
        $(".show-hide-current-pwd").on("click",function(e){
        	if(e){
        		e.preventDefault();
        		e.stopPropagation();
        	}
            var inputElement = $(this).parent().find('input[name="currentPassword"]');
            if(inputElement.attr('type') == 'password'){
            	inputElement.attr('type', 'text');
                $(this).text($(this).data('label-hide')); 
                $(this).attr("title", Hypr.getLabel('hidePassword'));               
            }else{
            	inputElement.attr('type', 'password');
                $(this).text($(this).data('label-show'));
                $(this).attr("title", Hypr.getLabel('show'));
            }
        });
        /* Popover functionality*/
        $('[data-toggle="popover"]').popover();
        
        $('body').on("click",function(e){
        	if (typeof $(e.target).data('original-title') == 'undefined') {
        	   $('[data-original-title]').popover('hide');
        	}
        });
        
        $('a[href="#collapse-"]').on("click",function(e){
        	if($('.accordion-collapse').hasClass('in')){
        		$(this).parents('.accordion-panel').addClass('open');
        	}
        });
        
        /* Checkout as guest */
        $('#cart-checkout').click(function() {
        	$('#cart-form').submit();
        	var locale = require.mozuData('apicontext').headers['x-vol-locale'];
        	locale = locale.split('-')[0];
        	var currentLocal = locale === 'fr' ? '/fr' : '/en';
        	window.location.href = currentLocal + "/cart/checkout";
        });
        
        /*Update Wishlist Count*/
        if (!(user.isAnonymous) && user.isAuthenticated) {
	        api.get('wishlist',{}).then(function(response){
	        	if(response && response.data && response.data.items.length > 0) {
	        		var items = response.data.items[0].items;
		            var totalQuantity = 0;
		            _.each(items, function(item, index) {
		            	totalQuantity += item.quantity;
		            });
		            $('#wishlist-count').html('('+totalQuantity+')');
	        	}
	        });
        }
        
        /*2020 login call*/                
        $('.goto2020').on('click', function(e) {
            var goToLink =$("#kitchenDisignerAnchor").attr('href').trim();
            var target =$("#kitchenDisignerAnchor").attr('target');           
             e.preventDefault();    
                 
             if (!(user.isAnonymous) && user.isAuthenticated ) {
                 var requireUser = require.mozuData('pagecontext').user;            
                 $.ajax({
                     type:"GET",
                     dataType: 'text',                    
                     url: Hypr.getThemeSetting('link2020'),
                     data: {
                         firstName: requireUser.firstName,
                         emailAddress: requireUser.email,
                         userId: requireUser.accountId
                     },
                     success:function(response){                     
                         window.open(goToLink + '?token=' + response , target);          			
                     },
                     error: function (error) {
                         console.log(error);
                     }
                 });
             } else {
                 window.open(goToLink, target);
             } 	      
         });     
        /** Footer Signup link -On keyppress**/
       $('#footer-newsletter-email').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            $('.signup-btn').click();
            event.preventDefault();
        }
    });
    /** Footer Signup link -On keyppress**/     
        /*Relation1 for Newsletter from Footer*/
        $('.signup-btn').on('click', function(){ 
        	var email = $('.footer-newsletter-email').val();
        	var emailReg = Backbone.Validation.patterns.email;
        	var emailFormat = emailReg.test( email );   

            if(localStorage.getItem('preferredStore')){
                if(emailFormat) {
                    $('.emailFormatError').hide();
                    var headers = {};
                    headers.Authorization = b_token;
                    
                    if(locale === "fr"){
                        locale = "French";
                    } else {
                        locale = "English";
                    }

                    var postalCode = $.parseJSON(localStorage.getItem('preferredStore')).address.postalOrZipCode;
                    var now = new Date();
                    var timeStamp = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + (now.getDate()) + "-" + now.getHours() + ':' + ((now.getMinutes() < 10) ? ("0" + now.getMinutes()) : (now.getMinutes())) + ':' + ((now.getSeconds() < 10) ? ("0" + now.getSeconds()) : (now.getSeconds()));
                    var data = {
                                EmailAddress: email,
                                FirstName: " ",
                                PostalCode: postalCode,
                                Preferred_Language: locale,
                                Is_Contractor: "No",
                                Source: "HH Website",
                                ConsentType: 'explicit'

                            };
                    var dataForRelation1 = "emailaddress="+email+"&timeStamp="+timeStamp;
                  if(email) {
                          $.ajax({
                            method: 'POST',
                            headers: headers,  
                            contentType: 'application/json; charset=utf-8',
                            url: relation1InsertRecord,
                            data: JSON.stringify(data),
                            success:function(response){ 
								var isNewCustomer = response.IsNew;
								if(isNewCustomer) {
									$.ajax({
										method: 'GET',
										url: Hypr.getThemeSetting('linkRelation1GetToken'),
										data: dataForRelation1,
										success: function(response) { 
											window.location.href = relation1Link + response;
										},
										error: function (error) {
											$('.emailFormatError').text(Hypr.getLabel('relation1ExistingUser')).show();
											console.log("error: " + JSON.stringify(error));
										}
									});
								}
								else {
									$('.emailFormatError').text(Hypr.getLabel('relation1ExistingUser')).show();
									console.log("Existing user so not redirecting to Preference Page.");
								}
                            },
                            error:function(error){
                                console.log(error);
                            }
                            
                        });
                    }
                } else {
                    $('#footer-loading').hide();
                    $('.emailFormatError').show();
                }
            }else{
                if(locale === "fr"){
                    window.location.href = "https://pc.fr.homehardware.ca/preferences.aspx";
                } else {
                    window.location.href = "https://pc.en.homehardware.ca/preferences.aspx";
                } 
            }
        	
        });   
        
        if($(window).width() > 767) {
        	$('#static-navigation-collapse').addClass('in');
        }
        $("#mobile-search #searchbox").addClass("active add-top");
     
        // destination url click for banner video widget
        $(document).on('click touchstart', '.main-video', function(e) {
        	var url = $(e.currentTarget).data('url');
        	if(url) {
        		window.open(url, '_blank');
        	}    	
        });

        var lgDiff = 0,mdDiff = 0;
        
        var windowWidth = $(window).width();
        $(window).resize(function(){
            if ($(window).width() != windowWidth) {
                $(".mz-searchbox-input").trigger("blur");
                windowWidth = $(window).width();
                showClearance();
            }
        });
        showClearance();
        function showClearance(){
            if($(window).width() > 1380){
                showClearanceTag();
                setTimeout(function(){ 
                    $("#desktop-search").show();
                    var pagecontext = require.mozuData('pagecontext');
                    if(pagecontext.pageType === "search"){
                        var searchQueryString = decodeURIComponent(location.search.split("query=")[1].split("&")[0]);
                        if(!$.cookie('QueryWithoutStopwords')){
                            $(".mz-searchbox-input").val('');
                        }else{
                            if($.cookie('QueryWithoutStopwords') === searchQueryString){
                                if($.cookie('stopwords')){
                                    $(".mz-searchbox-input").val('');
                                }
                            }else{
                                $(".mz-searchbox-input").val('');
                            }
                        }

                    }
                    var searchBoxWidth = "";
                    var isMousedown = false;
                $(".mz-searchbox-input").on("focus",function(){
                    if(!isMousedown) {
                        searchBoxWidth = $(".mz-searchbox-field").width();
                    }       
                });
        
                $(document).on('blur',".mz-searchbox-input", function(){
                    if(!isMousedown) {
                        $(".suggestedSearch").addClass("hidden");
                    }else {
                        $(".mz-searchbox-expands.active").find(".mz-searchbox-input.tt-input").trigger("focus");
                        isMousedown = false;
                    }
                });
        
                $(document).on('mousedown',".suggestedSearch, #search_submit", function(){
                    isMousedown = true;
                });
                $(document).on('touchstart',".suggestedSearch, #search_submit", function(){
                    isMousedown = true;
                });
                    showClearanceTag();
                }, 3000);
            }
            if($(window).width() > 991 && $(window).width() < 1381) {
                showClearanceTag();
                setTimeout(function(){ 
                    $("#desktop-search").show();
                    var pagecontext = require.mozuData('pagecontext');
                    if(pagecontext.pageType === "search"){
                        var searchQueryString = decodeURIComponent(location.search.split("query=")[1].split("&")[0]);
                        if(!$.cookie('QueryWithoutStopwords')){
                            $(".mz-searchbox-input").val('');
                        }else{
                            if($.cookie('QueryWithoutStopwords') === searchQueryString){
                                if($.cookie('stopwords')){
                                    $(".mz-searchbox-input").val('');
                                }
                            }else{
                                $(".mz-searchbox-input").val('');
                            }
                        }

                    }
                    var searchBoxWidth = "";
                    var isMousedown = false;
                $(".mz-searchbox-input").on("focus",function(){
                    if(!isMousedown) {
                        searchBoxWidth = $(".mz-searchbox-field").width();
                    }       
                });
        
                $(document).on('blur',".mz-searchbox-input", function(){
                    if(!isMousedown) {
                        $(".suggestedSearch").addClass("hidden");
                    }else {
                        if($(window).width() > 1024){
                            $(".mz-searchbox-expands.active").find(".mz-searchbox-input.tt-input").trigger("focus");
                        }
                        isMousedown = false;
                    }
                });
        
                $(document).on('mousedown',".suggestedSearch, #search_submit", function(){
                    isMousedown = true;
                });
                $(document).on('touchstart',".suggestedSearch, #search_submit", function(){
                    isMousedown = true;
                });

                $(document).on('touchstart',".mz-l-pagecontent, .mz-sitenav, .my-store-nav, .top-header", function(){
                    if ($(".mz-searchbox-input").is(":focus")) {
                        isMousedown = false;
                        $(".mz-searchbox-input").trigger("blur");
                    }
                });

                $(document).on('click touchstart', '#search_submit-close', function() {
                    if($(window).width() > 767 && $(window).width() < 1025){
                        $(".mz-searchbox-input").trigger("focus");
                        $(".mz-searchbox-input").trigger("blur");
                    }
                });

                    showClearanceTag();
                }, 3000);
            }
            
			if($(window).width() > 767 && $(window).width() < 992){
                showClearanceTag();
                setTimeout(function(){ 
                    $("#desktop-search").show();
                    var pagecontext = require.mozuData('pagecontext');
                    if(pagecontext.pageType === "search"){
                        var searchQueryString = decodeURIComponent(location.search.split("query=")[1].split("&")[0]);
                        if(!$.cookie('QueryWithoutStopwords')){
                            $(".mz-searchbox-input").val('');
                        }else{
                            if($.cookie('QueryWithoutStopwords') === searchQueryString){
                                if($.cookie('stopwords')){
                                    $(".mz-searchbox-input").val('');
                                }
                            }else{
                                $(".mz-searchbox-input").val('');
                            }
                        }

                    }

                    var searchBoxWidth = "";
                    var isMousedown = false;
                $(".mz-searchbox-input").on("focus",function(){
                    if(!isMousedown) {
                        searchBoxWidth = $(".mz-searchbox-field").width();
                    }       
                });
        
                $(document).on('blur',".mz-searchbox-input", function(){
                    if(!isMousedown) {
                        $(".suggestedSearch").addClass("hidden");
                    }else {
                        isMousedown = false;
                    }
                });
        
                $(document).on('mousedown',".suggestedSearch, #search_submit", function(){
                    isMousedown = true;
                });
                $(document).on('touchstart',".suggestedSearch, #search_submit", function(){
                    isMousedown = true;
                });

                $(document).on('touchstart',".mz-l-pagecontent, .mz-sitenav, .my-store-nav, .top-header", function(){
                    if ($(".mz-searchbox-input").is(":focus")) {
                        isMousedown = false;
                        $(".mz-searchbox-input").trigger("blur");
                    }
                });

                $(document).on('click touchstart', '#search_submit-close', function() {
                    $(".mz-searchbox-input").trigger("focus");
                    $(".mz-searchbox-input").trigger("blur");
                });
                    showClearanceTag();
                }, 3000);
            }
            if($(window).width() < 768){
                $(".twitter-typeahead").css({"width":"100%"});
                $(".mz-searchbox-input").css({"width":"100%"});
                $(".mz-searchbox-field").css({"width":"100%"});
                $("#desktop-search").show();
                showClearanceTag();
                setTimeout(function(){
                    var pagecontext = require.mozuData('pagecontext');
                    if(pagecontext.pageType === "search"){
                        var searchQueryString = decodeURIComponent(location.search.split("query=")[1].split("&")[0]);
                        if(!$.cookie('QueryWithoutStopwords')){
                            $(".mz-searchbox-input").val('');
                        }else{
                            if($.cookie('QueryWithoutStopwords') === searchQueryString){
                                if($.cookie('stopwords')){
                                    $(".mz-searchbox-input").val('');
                                }
                            }else{
                                $(".mz-searchbox-input").val('');
                            }
                        }

                    }
                    $(document).on('click touchstart',function(e){
                        if(!$(".suggestedSearch").hasClass("hidden")){
                            if(!($(e.target).hasClass("suggestedSearch") || $(e.target).hasClass("mz-searchbox-input") || $(e.target).parents(".suggestedSearch").length)){
                                $(".suggestedSearch").addClass("hidden");
                                $('body').removeClass('no-scroll-mobile');
                            }
                        }
                    });
                    showClearanceTag();
                }, 3000);
     
            }
       
        }

		var isartical = false;
		$(document).on('click','.search-product-tab',function(){
			isartical = false;
		});
		$(document).on('click','.search-artical-tab',function(){
			isartical = true;
		});
		if(!isartical) {
            $(document).on('click','.mz-facetingform-value, .sortProducts, .mz-pagenumbers-number, .mz-pagenumbers-prev, .mz-pagenumbers-next, .filter-button, .clear-filters, #tenantdynattribute-23_e-commerce-item', function(e){
                setTimeout(function(){
                    showClearanceTag();
                }, 2000);
            });

            $(document).on('change','.mz-pagingcontrols-pagesize-dropdown', function(e){
                setTimeout(function(){
                    showClearanceTag();
                }, 2000);
            });
		} 
          
        var allCategories = window.allCategories = [];
        var parentCategoryId,currentCategoryFlow, productImpressionArray = [], productImpression = {};
		var AllProductsImpressions = [];
		var nonStaticPages = ['home', 'search-results','my-account','signup', 'no-search-results', 'category', 'product', 'store-details', 'store-locator', 'wishlist', 'cart', 'checkout', 'confirmation'];
		function getParentCategory(parentCategoryId, currentCategoryId, allCategories, impression){
			var parentCategoryData = [];
            _.find(allCategories,function(val){
				if(val.categoryId === currentCategoryId){
        	    	var parentCategoryName = val.content.name;
        	    	currentCategoryFlow = parentCategoryName + '/' + currentCategoryFlow;
        	    	if(parentCategoryId != val.parentCategory.categoryId){
        	    		//if current category is not NEW LBM And Hardlines then do the same process for current category
        	    		getParentCategory(parentCategoryId, val.parentCategory.categoryId, allCategories, impression);
        	    	}else{
        	    		//if current category is NEW LBM And Hardlines then do stop the process and set the ccategory flow in model
	                  	var price = $(impression).find('.mz-price.is-saleprice').length>0 ? $(impression).find('.mz-price.is-saleprice').text() : $(impression).find('.mz-price').text();
	                  	if(price && price.indexOf('/') > 0) {
                      	  price = price.split('/')[0];
                        }
	                  	var listAttr = '';
						if($(impression).find('.mz-productlisting-title') && $(impression).find('.mz-productlisting-title').length>0) {
                      	  listAttr = $(impression).find('.mz-productlisting-title').attr('href').split('?page=')[1];
                      	  if(listAttr && listAttr.indexOf('#ccode') > 0){
                      		  listAttr = listAttr.split('#ccode')[0];
                      	  }
                        }else if($(impression).find('.product-title') && $(impression).find('.product-title').length>0) { //for related products
                      	  listAttr = $(impression).find('.product-title').children().attr('href').split('?page=')[1];
                        }else if($(impression).find('.productTile-title-link') && $(impression).find('.productTile-title-link').length>0){ //for image slider widget
                        	listAttr = $(impression).find('.productTile-title-link').attr('href').split('?page=')[1];
                        }
                       var pagecontext = require.mozuData('pagecontext');
                       var nonStaticPageListAttr = "";
                       _.find(nonStaticPages,function(pagename){
                            if(pagecontext.cmsContext.template.path === pagename){
                                nonStaticPageListAttr = listAttr;
                            }
                        });

                        if(!nonStaticPageListAttr){
                            var pageHeading = "";

                            if($('body').find('h1.title-for-print').length !== 0){
                               pageHeading = $('body').find('h1.title-for-print').text();
                           }else if($('body').find("h1.main-page-title").length !== 0){
                               pageHeading = $('body').find("h1.main-page-title").text(); 
                           }else{
                               pageHeading = pagecontext.cmsContext.page.path;
                           }
                            nonStaticPageListAttr = pageHeading+ ' - ' + listAttr.substr(listAttr.indexOf('page-')+5);
                        }
                        
                        if(nonStaticPageListAttr) {
                        	if(nonStaticPageListAttr.indexOf('&rrec=true') !== -1){
                        		nonStaticPageListAttr = nonStaticPageListAttr.split('&rrec=true')[0];
                        	}
                        }

                        var category = _.findWhere(allCategories,{"categoryId":pagecontext.categoryId});
                        var updateListName = '';
                        if(category !== undefined && category !== "" && category.parentCategory !== undefined ){
                            if(category.parentCategory.categoryId == Hypr.getThemeSetting('brandsCategoryId')){
                                updateListName = '_brand_'+ category.content.name;
                            }else if(category.parentCategory.categoryId == Hypr.getThemeSetting('dynamicCategoryId')){
                                updateListName = '_landingPage_'+ category.content.name;
                            }else{                            
                                updateListName = '_site-hierarchy_'+ category.content.name;
                            }
                        }else if(nonStaticPageListAttr === 'search-results page'){
                            updateListName = updateListName +'_'+$.cookie('LastSearchTerm');
                        }
	                  	productImpression = {
		                  	'name': $(impression).attr('data-mz-productname'),
		                  	'id' : $(impression).attr('data-mz-product'),
		                  	'price' : $.trim(price.replace('$', '').replace(',', '.')),
		                  	'brand' : $.trim($(impression).find('.brand-name').text()),
		                  	'category' : currentCategoryFlow,
		                  	'position' : $(impression).attr('data-mz-position'),
		                  	'list' : nonStaticPageListAttr + updateListName
	                    };
	                    productImpressionArray.push(productImpression);
        	    	}
				}
            });
        }

        window.getBreadcrumbflow = function(currentCategoryId, allCategories, breadcrumbStr, e){
            var parentCategoryId = Hypr.getThemeSetting('newLBMAndHardlinesCategoryId');
            _.find(allCategories,function(val){
                if(val.categoryId === currentCategoryId){
                    //var parentCatName = _.findWhere(product.get("categories"), {'categoryId' : parentCategoryId });
                	if(!breadcrumbStr) {
                		breadcrumbStr = val.content.name;
                	}else{
                		breadcrumbStr = val.content.name + "/" + breadcrumbStr;
                	}
                    if(val.parentCategory.categoryId != parentCategoryId){
                        window.getBreadcrumbflow(val.parentCategory.categoryId, allCategories, breadcrumbStr, e);
                    }else{
                        var parentElement = $(e.currentTarget).closest(".datalayer-parent-element");
                        var hasSalePrice = $(parentElement).find(".mz-price").hasClass("is-saleprice");
                        var getPrice = hasSalePrice ? $(parentElement).find(".is-saleprice").text().split('/')[0] : $(parentElement).find(".mz-price").text().split('/')[0];
                        var getBrand = $(parentElement).find(".brand-name").text();
                        var actionFieldList = $(e.currentTarget).attr("href").split('?page=')[1];
                        var pagecontext = require.mozuData('pagecontext');
                        var nonStaticPageListAttr = "";
                        _.find(nonStaticPages,function(pagename){
                             if(pagecontext.cmsContext.template.path === pagename){
                                 nonStaticPageListAttr = actionFieldList;
                             }
                         });
 
                         if(!nonStaticPageListAttr){
                            var pageHeading = "";

                            if($('body').find('h1.title-for-print').length !== 0){
                               pageHeading = $('body').find('h1.title-for-print').text();
                           }else if($('body').find("h1.main-page-title").length !== 0){
                               pageHeading = $('body').find("h1.main-page-title").text(); 
                           }else{
                               pageHeading = pagecontext.cmsContext.page.path;
                           }
                             nonStaticPageListAttr = pageHeading+ ' - ' + actionFieldList.substr(actionFieldList.indexOf('page-')+5);
                         }
                         if(nonStaticPageListAttr) {
                         	if(nonStaticPageListAttr.indexOf('&rrec=true') !== -1){
                         		nonStaticPageListAttr = nonStaticPageListAttr.split('&rrec=true')[0];
                         	}
                         }
                        
                        var updateListName = '';
                        if (nonStaticPageListAttr === 'search-results page') {
                            updateListName = updateListName + '_' + $.cookie('LastSearchTerm');
                        }
                        dataLayer.push({
                            'event': 'productClick',
                            'ecommerce': {
                                'click': {
                                'actionField': {'list': nonStaticPageListAttr + updateListName},
                                'products': [{
                                    'name': $.trim($(parentElement).attr("data-mz-productName")),
                                    'id': $.trim($(parentElement).attr("data-mz-product")),
                                    'price': getPrice ? $.trim(getPrice.replace('$', '').replace(',', '.')) : '',
                                    'brand':getBrand ? $.trim(getBrand) : '',
                                    'category': $.trim(breadcrumbStr),
                                    'position':$(parentElement).attr("data-mz-position")
                                }] 
                              }
                            }
                        });
                    }
                }
            });
        };

        var pageContext = require.mozuData('pagecontext');
        setTimeout(function() {
        	if(pageContext.cmsContext.template.path != "checkout" && pageContext.cmsContext.template.path != "confirmation" && pageContext.cmsContext.template.path != "category" && pageContext.cmsContext.template.path != "search-results" && pageContext.cmsContext.template.path != "cart" && require.mozuData('pagecontext').cmsContext.template.path !== 'hhh'){
    	        $('.data-layer-productClick').click(function(e){
    	        	e.stopPropagation();
    	            var breadcrumbStr = "";
    	            var currentParentId = -1;
                    
                    if(e.currentTarget && $(e.currentTarget).closest("[data-mz-categoryId]").length > 0){
                        var dataCategoryID = $(e.currentTarget).closest("[data-mz-categoryId]").attr('data-mz-categoryId');

                        if(dataCategoryID){
                            currentParentId = parseInt(dataCategoryID.split('|')[1], 10);
                        }
                    }

                    if(currentParentId && currentParentId > 0){
                        window.getBreadcrumbflow(currentParentId, window.allCategories, breadcrumbStr, e);
                    }
    	        });
            }
        }, 4000);
        

		if(require.mozuData('pagecontext').pageType !== 'product' && require.mozuData('pagecontext').cmsContext.template.path !== 'hhh'){
			setTimeout(function(e) {
				parentCategoryId = Hypr.getThemeSetting('newLBMAndHardlinesCategoryId');
				if(sessionStorage.getItem('allCategories')){
					allCategories = window.allCategories = JSON.parse(sessionStorage.getItem('allCategories'));
					var allImpressions = createImpressionArray();
					window.sendGtmDataLayer(allImpressions);
				}else{
                    var responseFields = "?responseFields=items(categoryCode, childrenCategories(categoryCode, childrenCategories(categoryCode, childrenCategories(categoryCode, childrenCategories(categoryCode, content(name)), content(name), parentCategory), content(name), parentCategory), content(name), parentCategory), content(name), parentCategory)";
                    api.request('GET', '/api/commerce/catalog/storefront/categories/tree' + responseFields, {
						cache: false
					}).then(function(result1){
						if(result1.items.length > 0) {
							_.each(result1.items[1].childrenCategories, function(children){
								allCategories.push(children);
								_.each(children.childrenCategories, function(subChildren){
									allCategories.push(subChildren);
									_.each(subChildren.childrenCategories, function(subSubChildren){
										allCategories.push(subSubChildren);
										_.each(subSubChildren.childrenCategories, function(subSubSubChildren){
											allCategories.push(subSubSubChildren);
										});
									});
								});
							});
							var pluckedArray = [];
							_.map(allCategories, function(category){
								pluckedArray.push(_.pick(category, 'categoryId', 'parentCategory', 'content'));
							});
							window.allCategories = pluckedArray;
							sessionStorage.setItem('allCategories', JSON.stringify(pluckedArray));
							var allImpressions = createImpressionArray();
							window.sendGtmDataLayer(allImpressions);
						}
					});
				}
			}, 2000);
		}
		var createImpressionArray = function(){
//			create impression without certona products as the separate impression arrey for certona will be created from certona-content.js file
			var productImpressions = $('.ign-data-product-impression').filter(function(){ 
        		return $(this).closest('.certona-products-container').length === 0; 
        	});
			var imageSliderProductImpression = $('.ign-data-product-impression-image-slider');
			if(imageSliderProductImpression && imageSliderProductImpression.length > 0) {
        	  _.each($('.bannerimg.slick-slide[aria-describedby]'), function(banner) {
        		  _.each($(banner).find('.ign-data-product-impression-image-slider'), function(productOnBanner) {
        			  productImpressions.push(productOnBanner);
        		  });
        	  });
			}
			return productImpressions;
		};
		var sendImpressionView = function(productImpressions) {
			productImpressionArray = [];
			_.each(productImpressions, function(impression){
          	  var productCategory;
          	  if($(impression).attr('data-mz-categoryId')){
          		  productCategory = parseInt($(impression).attr('data-mz-categoryId').split('|')[0], 10);
          	  }
          	  
          	  _.find(allCategories,function(val){
                    if(val.categoryId === productCategory){
                      var currentCategoryName = val.content.name;
                      var currentParentId = parseInt($(impression).attr('data-mz-categoryId').split('|')[1], 10);
                      currentCategoryFlow = currentCategoryName;
                      if(parentCategoryId != currentParentId){
                        //if current category is not NEW LBM And Hardlines then do the same process for current category
                        getParentCategory(parentCategoryId, currentParentId, allCategories, impression);
                      }else {
                        var price = $(impression).find('.mz-price.is-saleprice').length>0 ? $(impression).find('.mz-price.is-saleprice').text() : $(impression).find('.mz-price').text();
                        if(price && price.indexOf('/') > 0) {
                      	  price = price.split('/')[0];
                        }
                        var listAttr = '';
                        if($(impression).find('.mz-productlisting-title') && $(impression).find('.mz-productlisting-title').length>0) {
                      	  listAttr = $(impression).find('.mz-productlisting-title').attr('href').split('?page=')[1];
                      	  if(listAttr.indexOf('#ccode') > 0){
                      		  listAttr = listAttr.split('#ccode')[0];
                      	  }
                        }else if($(impression).find('.product-title') && $(impression).find('.product-title').length>0) { //for related products
                      	  listAttr = $(impression).find('.product-title').children().attr('href').split('?page=')[1];
                        }else if($(impression).find('.productTile-title-link') && $(impression).find('.productTile-title-link').length>0){ //for image slider widget
                        	listAttr = $(impression).find('.productTile-title-link').attr('href').split('?page=')[1];
                        }
                        
                        var pagecontext = require.mozuData('pagecontext');
                        var nonStaticPageListAttr = "";
                        _.find(nonStaticPages,function(pagename){
                             if(pagecontext.cmsContext.template.path === pagename){
                                 nonStaticPageListAttr = listAttr;
                             }
                         });
 
                         if(!nonStaticPageListAttr){
                            var pageHeading = "";

                            if($('body').find('h1.title-for-print').length !== 0){
                               pageHeading = $('body').find('h1.title-for-print').text();
                           }else if($('body').find("h1.main-page-title").length !== 0){
                               pageHeading = $('body').find("h1.main-page-title").text(); 
                           }else{
                               pageHeading = pagecontext.cmsContext.page.path;
                           }
                             nonStaticPageListAttr = pageHeading + ' - ' + listAttr.substr(listAttr.indexOf('page-')+5);
                         }
                        
                        
                        productImpression = {
                      	'name': $(impression).attr('data-mz-productname'),
                      	'id' : $(impression).attr('data-mz-product'),
                      	'price' : $.trim(price.replace('$', '').replace(',', '.')),
                      	'brand' : $.trim($(impression).find('.brand-name').text()),
                      	'category' : currentCategoryFlow,
                      	'position' : $(impression).attr('data-mz-position'),
                      	'list' : nonStaticPageListAttr
                        };
                        productImpressionArray.push(productImpression);
                      }
                    }
                 });
            });
            
			
			//send datalayer
			 if(productImpressions && productImpressions.length > 0) {
				// Product impressions can be sent on the pageview event.
				dataLayer.push({ 
					'event': 'impressionview',
					'ecommerce': {
						'currencyCode': 'CAD',
						'impressions': productImpressionArray
					}
				});
			}
		};
		window.sendGtmDataLayer = function (productImpressions, isCertonaImpressions) {
			if(require.mozuData('pagecontext').pageType === 'product' && isCertonaImpressions){
          		parentCategoryId = Hypr.getThemeSetting('newLBMAndHardlinesCategoryId');
          		if(sessionStorage.getItem('allCategories')){
            		allCategories = window.allCategories = JSON.parse(sessionStorage.getItem('allCategories'));
					sendImpressionView(productImpressions);
          		}else{
	        		var responseFields = "?responseFields=items(categoryCode, childrenCategories(categoryCode, childrenCategories(categoryCode, childrenCategories(categoryCode, childrenCategories(categoryCode, content(name)), content(name), parentCategory), content(name), parentCategory), content(name), parentCategory), content(name), parentCategory)";
                    api.request('GET', '/api/commerce/catalog/storefront/categories/tree' + responseFields, {
							cache: false
						}).then(function(result1){
	                    if(result1.items.length > 0){
	                        _.each(result1.items[1].childrenCategories, function(children){
	                      	  allCategories.push(children);
	                      	 _.each(children.childrenCategories, function(subChildren){
	                            allCategories.push(subChildren);
	                      		 _.each(subChildren.childrenCategories, function(subSubChildren){
	                                allCategories.push(subSubChildren);
	                      			 _.each(subSubChildren.childrenCategories, function(subSubSubChildren){
	                                    allCategories.push(subSubSubChildren);
	                      			 });
	                      		 });
	                      	 });
	                        });
							var pluckedArray = [];
							_.map(allCategories, function(category){
								pluckedArray.push(_.pick(category, 'categoryId', 'parentCategory', 'content'));
							});
							window.allCategories = pluckedArray;
							sessionStorage.setItem('allCategories', JSON.stringify(pluckedArray));
							sendImpressionView(productImpressions);
	                    }
	            	});
	        	}
          	  }else{
				sendImpressionView(productImpressions);
			  }
		};
        // Used to hide shipping address option in header menu for non STH store
		checkIsStoreAvailForSTH();
    });
    
    function checkIsStoreAvailForSTH(){
        if (localStorage.getItem('preferredStore') && !user.isAnonymous && user.isAuthenticated ) {
            //Set store level ship to home flag
            var preferredStoreDetails = $.parseJSON(localStorage.getItem('preferredStore')),
            storeAllowsShipToHome = !!_.find(preferredStoreDetails.attributes,
                function predicate(attribute) {
                    return (attribute.fullyQualifiedName === Hypr.getThemeSetting('shipToHomeStoreFlagAttrFQN') &&
                        attribute.values[0]);
                });
            if(!storeAllowsShipToHome){
                $('#shippingAddressLink').addClass('hidden');
                $('#returnsHistoryLink').addClass('hidden');
            }
        }
    }

    function showClearanceTag(){
        if(localStorage.getItem('preferredStore')){
            var mycurrentStore = JSON.parse(localStorage.getItem('preferredStore'));
             if(mycurrentStore) {
                $(".onSale").removeClass("hidden");
                 var attribute = _.findWhere(mycurrentStore.attributes, {"fullyQualifiedName":Hypr.getThemeSetting("isEcomStore")});
                        
                if(attribute){
                    var isEcomStore = attribute.values[0] === "Y" ? true : false; 
                    if(isEcomStore){
                    	var clearanceAttrVal = Hypr.getThemeSetting('clearanceProduct');
                    	var isClearance = Hypr.getThemeSetting('isclearance');
                    	var filter = isClearance + ' eq "' + clearanceAttrVal + '"';

                        if($.cookie('clearanceCount'))
                        {
                               var clearanceCount = $.parseJSON($.cookie("clearanceCount"));  
                                if(0 < clearanceCount) {                    
                                   $(".clearance-link, .clearanceTagImg").removeClass("hidden");
                                }
                        }
                        else
                        {
                            api.get('products', {filter: filter, pageSize: 0}).then(function(response){   
                            var expiryDate = new Date();
                            expiryDate.setHours(23,59,59,999); 
                            var clearanceCounValueEncoded = encodeURI(JSON.stringify(response.data.totalCount)); 
                            document.cookie = "clearanceCount="+clearanceCounValueEncoded+";expires=" + expiryDate + ";path=/;Secure;SameSite=None";
                            if(0 < response.data.totalCount) {                    
                                $(".clearance-link, .clearanceTagImg").removeClass("hidden");
                            } 
                            });
                        }
                    }else{
                        $(".showSaleTag").removeClass("hidden");
                    }
                }
            }  
        }
    }

    //Change icon of footer lists on collapse
	$('.footer-list').on('shown.bs.collapse', function(e) {
 		$(this).parent().find('.collapseBtn').text('-');
    });
    $('.footer-list').on('hidden.bs.collapse', function(e) {
		$(this).parent().find('.collapseBtn').text('+');
    });
    if( $('#mzWishlist').is(':empty') ) {
    	$(".empty-wishlist-container").show(); 
    }
    
    $(".accordion-panel").on("show.bs.collapse hide.bs.collapse", function(e) {
        if (e.type=='show'){
          $(this).addClass('active');
        }else{
          $(this).removeClass('active');
        }
      });

    $("#signInLink, #signInLinkMobile,.annonymous-signin-link ").click(function(e) {
        e.preventDefault();
        var locale = require.mozuData('apicontext').headers['x-vol-locale'];
        var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
        locale = locale.split('-')[0];
        var currentLocale = '';
        if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
            currentLocale = locale === 'fr' ? '/fr' : '/en';
        }
        if(window.location.pathname === currentLocale+"/login-shopping-list" || window.location.pathname === "/login-shopping-list"){
            window.location.href=currentLocale + "/user/login?returnurl=/shopping-list";
        }else{
            window.location.href=currentLocale + "/user/login?returnurl="+window.location.pathname;
        }
    });
    if($(window).width() < 768) {
       $("#desktop-search").prop('id', 'mobile-search');
    }
    else{
        $("#mobile-search").prop('id', 'desktop-search');
    }
    $(".mz-storebranding-mobile").hide(); 
    $(".mz-furniture-logo-mobile").hide();
    $(window).on('scroll',function(){
    	if($(this).scrollTop() > 0) {
            $("#mobile-search #searchbox").removeClass("active").hide();
            $(".nav-search-icon").show();
            $(".mz-sitenav").addClass("fixed-on-mobile-search active fixed-on-search");
            $(".mz-storebranding-mobile").show();
            $(".mz-furniture-logo-mobile").show();
            $('#storecollapse').addClass('collapsed');
            $('.navbar-toggle').addClass('menu-toggle');
            $('.navbar-toggle').removeClass('menu-padding');
        }
    	else{
            $("#mobile-search #searchbox").addClass("active add-top").removeClass('drawer-open-search').show();
            $(".nav-search-icon").hide(); 
            $(".mz-storebranding-mobile").hide();
            $(".mz-furniture-logo-mobile").hide();
            $(".mz-sitenav").removeClass("fixed-on-mobile-search active fixed-on-search");
            $('.navbar-toggle').removeClass('menu-toggle');
            $('.navbar-toggle').addClass('menu-padding');
        } 
    });

    var searchboxWidth;
    var twitterTypeaheadWidth;
    var searchFieldEidth;
    var isSearchFocused = true;
    $('#search-input').on("focusin",function(e){
    	$('.mz-searchbox-field').addClass('after-focus');
        e.stopPropagation();
        event.cancelBubble = true;
        if($(window).width() > 767 && $(window).width() < 992){
             if(!isSearchFocused){
                searchboxWidth = $(this).width();
                twitterTypeaheadWidth = $(this).closest(".twitter-typeahead").width();
                searchFieldEidth = $(".mz-searchbox-field").width();
                var utilityContainerWidth = $(".utility-nav-width").width() + 20;
                var diff1 = twitterTypeaheadWidth - searchboxWidth;
                var diff2 = searchFieldEidth - twitterTypeaheadWidth;
                $(this).width(utilityContainerWidth - diff1);
                $(this).closest(".mz-searchbox-field").width(utilityContainerWidth + diff2);
                $(".twitter-typeahead").width(utilityContainerWidth);
            }else{
                isSearchFocused = false;
                $('#search-input').trigger("blur");
            }
        }
    });

    $('#search-input').blur(function(){
        if($(window).width() > 767 && $(window).width() < 992){
        searchboxWidth = $(this).width();
        isSearchFocused = false;
        $(this).width(searchboxWidth);  
        $(this).closest(".mz-searchbox-field").width(searchFieldEidth);
        $(this).closest(".twitter-typeahead").width(twitterTypeaheadWidth);
        $(this).closest(".mz-searchbox-input").css({"width":"100%"});
        
        }     
    });
    
    $('.navbar-toggle').removeClass('menu-toggle').addClass('menu-padding');
    $(".nav-search-icon").click(function(){
        //$(".nav-search-icon").hide(); ---IWM-432---
        $("#mobile-search #searchbox").addClass("active").removeClass('hidden add-top').show();
        $(".mz-searchbox-mobileview").toggleClass("hide");
        $('#example-navbar-collapse').removeClass('in');
    });

    $('.search-close').on("click",function() {
       $('#desktop-search #searchbox').removeClass('active').addClass("mz-searchbox-no-expands");
       $('#desktop-search .mz-searchbox-button').show();
       $('.nav-search-icon-text').removeClass('hidden').css({'display':'block'});
       $('.mz-searchbox-input').css('display','none');
       $('.search-close').css('display','none');
    });
    $('.mz-searchbox-button').click(function(){
        $('.nav-search-icon-text').css('display','none');
        $('#desktop-search #searchbox').removeClass('mz-searchbox-no-expands');
    });
    
    $("#mobile-search #searchbox #search-input").on("click", function() {
        $("body").scrollTop(0);
    });

    //select text in searchbox on focus
    $(document).find(".mz-searchbox-input").focus(function() { $(this).select(); } );
    
    $("#selectStore").on("click", function(e){
        e.preventDefault();
        var locality = require.mozuData('apicontext').headers['x-vol-locale'];
        locality = locality.split('-')[0];
        var currentLocality = locality === 'fr' ? '/fr' : '/en';
        if(window.location.pathname.length > 1){
            if(require.mozuData('pagecontext').pageType === 'search'){
                var url = window.location.search.split('&')[0];
                window.location.href= currentLocality + "/store-locator?returnUrl=" + window.location.pathname+url;
            }
            else{
                window.location.href= currentLocality + "/store-locator?returnUrl=" + window.location.pathname;
            }
        }else{
            window.location.href= currentLocality + "/store-locator";
        }
    });
    $("#no-store-hiddenpopbadge").on("click", function(e){
        e.preventDefault();
        var locale = require.mozuData('apicontext').headers['x-vol-locale'];
        locale = locale.split('-')[0];
        var currentLocale = locale === 'fr' ? '/fr' : '/en';
        var pageContext = require.mozuData('pagecontext');
        if(window.location.pathname.length > 1){
         if(pageContext.pageType=='search'){
             var url = window.location.search.split('&')[0];
                window.location.href= currentLocale + "/store-locator?returnUrl=" + window.location.pathname+url;
             }
         else{
            window.location.href= currentLocale + "/store-locator?returnUrl=" + window.location.pathname;
                 }
        }else{
            window.location.href= currentLocale + "/store-locator";
        }
    });
    $("#different-storelink").on("click", function(e){
        e.preventDefault();
        var locale = require.mozuData('apicontext').headers['x-vol-locale'];
        locale = locale.split('-')[0];
        var currentLocale = locale === 'fr' ? '/fr' : '/en';
        var pageContext = require.mozuData('pagecontext');
        if(window.location.pathname.length > 1){
         if(pageContext.pageType=='search'){
             var url = window.location.search.split('&')[0];
                window.location.href= currentLocale + "/store-locator?returnUrl=" + window.location.pathname+url;
             }
         else{
            window.location.href= currentLocale + "/store-locator?returnUrl=" + window.location.pathname;
                 }
        }else{
            window.location.href= currentLocale + "/store-locator";
        }
    });
});   
