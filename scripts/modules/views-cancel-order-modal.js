define([
    'modules/jquery-mozu',
    'underscore',
    'hyprlive',
    'modules/backbone-mozu'
], function ($, _, Hypr, Backbone) {
    var cancelOrderModalView = Backbone.MozuView.extend({
        templateName: 'modules/my-account/my-account-cancel-order-modal',
        additionalEvents: {
            "change [data-mz-reason]": "onReasonChange",
            "input .other-reason": "onReasonEnter"
        },
        autoUpdate: [
            'otherReason'
        ],
        render: function () {
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            this.model.set("currentLocale", locale, {silent: true});
            Backbone.MozuView.prototype.render.apply(this, arguments);
        },
        onReasonChange: function (event) {
            var selectedReasonCode = $(event.currentTarget).find("option:selected").val(),
                selectedReasonDescription = $(event.currentTarget).find("option:selected").text();
            this.model.set('reasonDescription', selectedReasonDescription);
            this.model.set('selectedReasonCode', selectedReasonCode);
            if ("Other" === selectedReasonCode) {
                this.$el.find('.new-btn-primary').prop("disabled", true);
                this.$el.find('.specify-reason-container').removeClass('hidden');
            } else if ("default" === selectedReasonCode) {
                this.$el.find('.new-btn-primary').prop("disabled", true);
            } else {
                this.$el.find('.new-btn-primary').prop("disabled", false);
                this.$el.find('.specify-reason-container').addClass('hidden');
                this.$el.find('.other-reason').val('');
            }
        },
        onReasonEnter: function (event) {
            var enteredReason = $(event.currentTarget).val().trim();
            if (enteredReason === '') {
                this.$el.find('.new-btn-primary').prop("disabled", true);
            } else {
                this.$el.find('.new-btn-primary').prop("disabled", false);
            }
        },
        initiateCancelOrder: function () {
            var orderNumber = this.model.get('orderNumber'),
                reasonCode = this.model.get('selectedReasonCode'),
                reasonDescription = this.model.get('reasonDescription').trim(),
                otherReasonDescription = this.model.get('otherReason'),
                payload = {
                    orderNumber: orderNumber.toString(),
                    reasonCode: reasonCode,
                    reasonDescription: reasonCode === 'Other' ? otherReasonDescription : reasonDescription
                };

            this.cancelOrder(payload, orderNumber);
            this.$el.find('.new-btn-primary').addClass('is-loading');
            this.$el.find('.cancel-order-error').addClass('hidden');
        },
        cancelOrder: function (payload, orderNumber) {
            var self = this;
            $.ajax({
                url: Hypr.getThemeSetting("cancelOrderURL"),
                data: JSON.stringify(payload),
                type: "POST",
                cors: true,
                contentType:'application/json',
                headers: {
                    'x-vol-app-claims': 'MGpOaGtaZao7fYOFL356P33aAz0mEj5f39vlrtiRBCyRgP/XTlu8J661zJ6XPSEZMrjtH+3GtbQ+s/WJBqcoq+cLUVSLT7HYCljuQ7HZ0jcg08DNGC/UPC2mLoNLDaSmgQSRAV/v4ZeUR9WfRQMn/+JFZyOvXRIUcViHIkUTI/ZUXs1XGYaXjHuq1iPpNNOaR4FKN8raOphmgOtftZgPnhJdDPUZ+EAoVx6a3gVB42sE4UqVGSGaF7BSz0TWNheFJNvpskoO0lBDNyf8s1mWhaGZaeRaEWD4OM5L9CkApDauoszSzLIGBAziVosPif0qr057093bmMqyXNT61iQSATuFGtdtz5trzfe5/WQ9p+WA75ys99NMN0QxiAf0mBFmW7m8bb3y38j9MjOT9kR8umdE/5HOgoyHFhjIe//0merGBI7Woqu/RRVC880PXWTl8zVvIdZ5+iRCqO97hXus1CZ5gHunCGqttu+ErjDFwRqURkkmYVGxuzMpmbKlRS9Xzts7HFVeVlaHi8XL8Zt5ykkgCEfhI07hnyNpbLl2GJa54oBs+phNC3BF0ubUBEUZojrt/zvtwoc/drKHQS1rqAqQIoIQQ9YEJ2a+yRvFw5s='
                },
                success:function(response){
                    if(response && response.statusCode && response.statusCode === 200){
                        self.$el.find('.new-btn-primary').removeClass('is-loading');
                        $("#orderCancelModal").modal('hide');
                        setTimeout(function(){
                            $(document).trigger('orderCancelled',{orderNumber: orderNumber});
                        },500);
                    }else{
                        self.$el.find('.new-btn-primary').removeClass('is-loading');
                        self.$el.find('.cancel-order-error').removeClass('hidden');
                        console.log('cancel order',response);
                    }

                },
                error: function (error) {
                    self.$el.find('.new-btn-primary').removeClass('is-loading');
                    self.$el.find('.cancel-order-error').removeClass('hidden');
                    console.log('cancel order',error);
                }
            });
        }
    });

    return cancelOrderModalView;
});