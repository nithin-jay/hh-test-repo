define([
    "modules/jquery-mozu",
    "underscore",
    "modules/backbone-mozu",
    "bignumber"
], function ($, _, Backbone, BigNumber) {
    /*This model is used to merging taxes in to one tax ( Pickup HST + Ship HST = HST) and adding shipping tax in item tax ( Ship HST + Shipping HST = Ship HST) for mix order and ship order*/
    var TaxCalculator = Backbone.MozuModel.extend({
        getCustomTaxes: function (taxData, orderType, priceListCode) {
            if (orderType === "shipToHomeOrder") {
                return this.getTaxesForShipOrder(taxData);
            } else if (orderType === "mixOrder") {
                return this.getTaxesForPickupOrder(taxData, priceListCode);
            }
        },
        getTaxesForPickupOrder: function (taxData, priceListCode) {
            var preferredStore = $.parseJSON(localStorage.getItem('preferredStore')),
                pickUpTaxes = priceListCode ? taxData['Pickup_' + priceListCode].taxRates : taxData['Pickup_' + preferredStore.code].taxRates,
                shipTaxes = this.getTaxesForShipOrder(taxData),
                taxesArray = pickUpTaxes.concat(shipTaxes),
                customTaxData = [];
            if (shipTaxes.length > 0 && pickUpTaxes.length > 0) {
                var mergedTaxData = _.groupBy(taxesArray, function (value) {
                    return value.taxType;
                });
                _.each(mergedTaxData, function (value, key) {
                    var taxArray = mergedTaxData[key],
                        taxAmtTotal = 0,
                        taxType = '';
                    _.each(taxArray, function (tax) {
                        taxAmtTotal = parseFloat(BigNumber.sum(taxAmtTotal.toFixed(2), tax.taxAmt.toFixed(2)).toFixed(2, 0));
                        taxType = tax.taxType;
                    });
                    customTaxData.push({
                        taxType: taxType,
                        taxAmt: taxAmtTotal
                    });
                });
            } else {
                customTaxData = pickUpTaxes;
            }
            console.log('Modified mixorder or pickup Taxes', customTaxData);
            return customTaxData;
        },
        //function is used to add item tax + shipping Tax
        getTaxesForShipOrder: function (taxData) {
            var shipTaxes = taxData.Ship_1 ? taxData.Ship_1.taxRates : [],
                modifiedShipTaxes = [];
            if (shipTaxes.length > 1) {
                var shipTaxesPresentCounter = 0;
                _.each(shipTaxes, function (shipTax) {
                    var shippingTax = _.findWhere(shipTaxes, {'taxType': "shipping-" + shipTax.taxType});
                    if (shippingTax) {
                        var taxAmtTotal = parseFloat(BigNumber.sum(shipTax.taxAmt.toFixed(2), shippingTax.taxAmt.toFixed(2)).toFixed(2, 0));
                        modifiedShipTaxes.push({
                            'taxType': shipTax.taxType,
                            'taxAmt': taxAmtTotal
                        });
                        shipTaxesPresentCounter++;
                    }
                });
                if(shipTaxesPresentCounter === 0) modifiedShipTaxes = shipTaxes;
            } else {
                modifiedShipTaxes = shipTaxes;
            }

            console.log("Modified Shipping taxes", modifiedShipTaxes);
            return modifiedShipTaxes;
        }
    });
    return TaxCalculator;
});