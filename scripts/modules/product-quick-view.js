define(["modules/jquery-mozu", 
	"underscore", 
	"hyprlive", 
	"modules/backbone-mozu", 
	"modules/cart-monitor", 
	"modules/models-product",
	"modules/views-productimages",  
	"hyprlivecontext",
	'modules/api',
	'modules/check-nearby-store',
    'vendor/jquery/moment',
    'vendor/timezonesupport/timezonesupport',
    'modules/cookie-utils',
    'modules/models-sth-postal-code-cookie',
    'modules/models-shipping-time-lines',
    'modules/views-estimate-shipping-modal',
    'modules/models-cart',
	'cloud',
    'slick'], function ($, _, Hypr, Backbone, CartMonitor, ProductModels, ProductImageViews, HyprLiveContext, api, 
        CheckNearbyStoreModal, moment, timeZoneSupport, CookieUtils, ShippingPostalCodeModel, ShippingTimelineModel, EstimateShippingModalView,CartModels) {
	var apiContext = require.mozuData('apicontext');
	var user = require.mozuData('user');
	var filtersString = "",
        isHomeFurnitureSite = false,
        isMapboxEnabled = false,
		today = null,
        maxQuantityAllowed,
        checkQuantity = false,
        warehouseInventoryCount,
        locationInventoryCount,
        field,
        quantity,
        $qField,
        estimateShippingModalView = null,
        days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        ua = navigator.userAgent.toLowerCase();
        var usePDPArcFunction = Hypr.getThemeSetting("usePDPArcFunction");
    var activeWarehousesForSTH = Hypr.getThemeSetting("warehousesForSTH");
	if (Hypr.getThemeSetting("homeFurnitureSiteId") === require.mozuData('apicontext').headers["x-vol-site"]) {
		filtersString = "properties.storeType in['Home Furniture']";
		isHomeFurnitureSite = true;
	} else {
		if (window.location.search.indexOf('storeFilter') < 0)
			filtersString = "properties.storeType in['Home Hardware','Home Building Centre', 'Home Hardware Building Centre'] ";
	}
	
	/* Adding display online filter */
	if (!filtersString) {
		filtersString = "properties.displayOnline eq true ";
	} else {
		filtersString += "and properties.displayOnline eq true ";
	}

	var QuickProductModalView = Backbone.MozuView.extend({
        templateName: 'modules/product/product-quick-view', 
        additionalEvents: {
            "change [data-mz-product-option]": "onOptionChange",
            "blur [data-mz-product-option]": "onOptionChange",
            "click [data-mz-product-option]": "onOptionChange",
            "change [data-mz-value='quantity']": "onQuantityChange",
            "click [data-mz-value='qty-increment']": "onQuantityChangeClick",
            "click [data-mz-value='qty-decrement']": "onQuantityChangeClick",
			"keyup input[data-mz-value='quantity']": "onQuantityChange",
            "blur input[data-mz-value='quantity']": "onQuantityChange",
			"click #checkNearByStore": "checkNearbyStores",
			"click .changePreferredStore": "changePreferredStore"
        },
        render: function () { 
        	$('#add-to-cart').off();
            $('#add-to-wishlist').off();
            $("#checkNearByStore").off();
            var me = this,activeIframe,inActiveIframe,
        	locale = require.mozuData('apicontext').headers['x-vol-locale'];
            me.model.set("currentLocale", locale, {silent: true});
            me.model.set("isVideoAvailable", window.vzaarUrl, {silent: true});
            //set EHF value in
            var ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
            if(!me.model.get('isEhfValueSet')){
                var ehfOption = me.model.get('options').findWhere({'attributeFQN': ehfAttributeFQN});
                if (ehfOption && me.model.get('ehfValue')) {
                    ehfOption.set('shopperEnteredValue', me.model.get('ehfValue'));
                    ehfOption.set('value', me.model.get('ehfValue'));
                    me.model.set('isEhfValueSet', true);
                }
            }
            if (navigator.userAgent.match(/(iPhone)|(iPod)|(android)|(webOS)|(ipad)/i)) {
                me.model.set("isDesktop", false,{silent: true});
            } else {
                me.model.set("isDesktop", true,{silent: true});
            }
        	Backbone.MozuView.prototype.render.apply(this, arguments);            
        	$("#sirvComponents").removeClass("hidden");
            var variationProdCode = this.model.get('variationProductCode');
            if(variationProdCode){
            	var prodImg = this.model.get('content').get('productImages');
            	me.swatchProductImageView(prodImg, variationProdCode);
            }
            $('#add-to-wishlist').on('click', function(e) {
            	e.stopImmediatePropagation(); //To off previous modal's click events
            	me.addToWishlist();
            });

            $("#checkNearByStore").on('click', function(e) {
            	e.stopPropagation(); //To off previous modal's click events
            	me.checkNearbyStores();
            });
			me.sliderFunction();
			setTimeout(function(){
				$(".business-messages").removeClass('hidden');
				$(".marketing-proposition").hide(); 
				$(".mz-productdetail-buttons").addClass("productdetail-btn");
			},700);
			
			/* Info Popover */
            $('[data-toggle="popover"]').popover();

            showClearanceTag();
            function showClearanceTag(){
                if(localStorage.getItem('preferredStore')){
                    var mycurrentStore = JSON.parse(localStorage.getItem('preferredStore'));
                     if(mycurrentStore) {
                         var attribute = _.findWhere(mycurrentStore.attributes, {"fullyQualifiedName":Hypr.getThemeSetting("isEcomStore")});
                                
                        if(attribute){
                            var isEcomStore = attribute.values[0] === "Y" ? true : false; 
                            if(isEcomStore){
                                $(".clearanceTagImg").removeClass("hidden");
                            }
                        }
                    }  
                }
            }
            setTimeout(function(){
                $(".product-badge").removeClass("hidden");
            },2000);
        },
        onOptionChange: function (e) {
            return this.configure($(e.currentTarget));
        },
        onQuantityChange: _.debounce(function (e) {
        	if (isNaN($(e.currentTarget).val()) || $(e.currentTarget).val() < 0) {
                e.preventDefault();
                $(e.currentTarget).val(1);
                $(".decrement").addClass("is-disabled");
                this.checkQuantity(1, $(e.currentTarget));
            } else {
                if ($('#add-to-wishlist').prop('disabled')) {
                    $('#add-to-wishlist').text(Hypr.getLabel('saveToList'));
                    $('#add-to-wishlist').prop('disabled', false);
                }
                field = $(e.currentTarget).attr('data-mz-value'); //this field is used to increment/decrement the quantity
                quantity = $(e.currentTarget).siblings('.mz-productdetail-qty').val();

                if (field === 'qty-decrement') {
                    $(e.currentTarget).siblings('.mz-productdetail-qty').val(parseInt(quantity, 10) - 1);
                }
                if (field === 'qty-increment') {
                    $(e.currentTarget).siblings('.mz-productdetail-qty').val(parseInt(quantity, 10) + 1);
                }
                $qField = "";
                if (field != 'quantity') {
                    $qField = $(e.currentTarget).siblings('.mz-productdetail-qty');
                } else {
                    $qField = $(e.currentTarget);
                }

                var newQuantity = parseInt($qField.val(), 10);

                if (newQuantity <= 1 || isNaN(newQuantity)) {
                    $qField.val(1);
                    newQuantity = 1;
                }

                if (!isNaN(newQuantity) && newQuantity !== this.model.get('quantity')) {
                    if (!this.model.get('maxQuantityAvailable')) this.setMaxQuantityAllowed();
                    this.checkQuantity(newQuantity, $qField);
                }
            }
        },500),
        onQuantityChangeClick: _.debounce(function (e){
            field = $(e.target).parent().attr('data-mz-value'); //this field is used to increment/decrement the quantity                
        	quantity = $(e.target).parent().siblings('.mz-productdetail-qty').val();    
        	
        	if(field === 'qty-decrement'){    
        		$(e.target).parent().siblings('.mz-productdetail-qty').val(parseInt(quantity, 10) - 1); 
        	}
        	if(field === 'qty-increment'){
        		$(e.target).parent().siblings('.mz-productdetail-qty').val(parseInt(quantity, 10) + 1);
        	}    
        	$qField = "";    
        	if(field != 'quantity'){    
        		$qField = $(e.target).parent().siblings('.mz-productdetail-qty');    
        	}else {    
        		$qField = $(e.target).parent();    
        	}
        	
            var newQuantity = parseInt($qField.val(), 10);
            
            if (!isNaN(newQuantity) && newQuantity !== this.model.get('quantity')) {
                if (!this.model.get('maxQuantityAvailable')) this.setMaxQuantityAllowed();
                this.checkQuantity(newQuantity, $qField);
            }
        },500),
        setMaxQuantityAllowed: function(){
            var warehouseInventoryCount = this.model.get('warehouseInventoryCount'),
                locationInventoryCount = this.model.get('locationInventoryCount') && this.model.get('locationInventoryCount') > 0 ? this.model.get('locationInventoryCount') : 0,
                isBohStore = this.model.get('bohStore') ? this.model.get('bohStore') : false,
                bopisAvailableInventory = isBohStore ? warehouseInventoryCount + locationInventoryCount : warehouseInventoryCount,
                pluckedWarehouseInv = this.model.get('wareHouseInventory') ? _.pluck(this.model.get('wareHouseInventory'), 'stockAvailable') :[],
                shipMaxAllowed = pluckedWarehouseInv.length ? _.max(pluckedWarehouseInv) : 0;
            this.model.set('shipMaxAllowed', shipMaxAllowed);
            pluckedWarehouseInv.push(bopisAvailableInventory);
            var maxQuantityAvailable = _.max(pluckedWarehouseInv);
            this.model.set('bopisMaxAllowed', bopisAvailableInventory);
            this.model.set('maxQuantityAvailable',maxQuantityAvailable);

            console.log('bopisMaxAllowed', bopisAvailableInventory);
            console.log('shipMaxAllowed', this.model.get('shipMaxAllowed'));
            console.log('maxQuantityAvailable', maxQuantityAvailable);
        },
        checkQuantity: function (newQuantity, $qField) {
            var maxQuantityAvailable = this.model.get('maxQuantityAvailable');
            if (newQuantity >= maxQuantityAvailable) {
                $qField.val(maxQuantityAvailable);
                this.enableDisableButtons(maxQuantityAvailable);
                this.showMaxQuantityError();
                this.model.updateQuantity(maxQuantityAvailable);
            } else {
                $qField.val(newQuantity);
                this.model.updateQuantity(newQuantity);
                this.enableDisableButtons(newQuantity);
                this.model.set('quantityError', false);
                this.$el.find('#quantityWarning').addClass('hidden');
                this.$el.find("a[data-mz-value='qty-increment']").removeClass('is-disabled');
            }
        },
        showMaxQuantityError: function () {
            this.$el.find('#quantityWarning').removeClass('hidden');
            this.model.set('quantityError', true);
            this.$el.find("a[data-mz-value='qty-increment']").addClass('is-disabled');
        },
        enableDisableButtons: function(newQuantity){
            if (newQuantity <= 1 || isNaN(newQuantity)) {
                $(".decrement").addClass("is-disabled");
            } else {
                $(".decrement").removeClass("is-disabled");
            }
        },
        validateQuantity: function (cartItemQuantity) {
            this.setMaxQuantityAllowed();
            var quantity = this.model.get("quantity"),
                maxQuantityAvailable = this.model.get('maxQuantityAvailable'),
                maxAllowedForAddToCart = maxQuantityAvailable - cartItemQuantity,
                isQuantityValid = true;
            maxQuantityAllowed = quantity;
            if (quantity > maxAllowedForAddToCart) {
                $('#limited-quantity-container').find('#maximumQuantityCount').text(maxQuantityAvailable > 0 ? maxQuantityAvailable : 0);
                maxQuantityAllowed = maxAllowedForAddToCart;
                isQuantityValid = false;
            } else if (this.model.get('quantityError')) {
                isQuantityValid = true;
            }
            return {
                isQuantityValid: isQuantityValid,
                maxQuantityAllowed: maxQuantityAllowed
            };
        },
        configure: function ($optionEl) {
        	var newValue = $optionEl.val(),
	            oldValue,
	            id = $optionEl.data('mz-product-option'),
	            optionEl = $optionEl[0],
	            isPicked = (optionEl.type !== "checkbox" && optionEl.type !== "radio") || optionEl.checked,
	            option = this.model.get('options').findWhere({'attributeFQN':id});
	        if (option) {
	            if (option.get('attributeDetail').inputType === "YesNo") {
	                option.set("value", isPicked);
	            } else if (isPicked) {
	                oldValue = option.get('value');
	                if (oldValue !== newValue && !(oldValue === undefined && newValue === '')) {
	                    option.set('value', newValue);
	                }
	            }
	        }
	        
        },
        swatchProductImageView: function(prodContent, variationProductCode) {
            var prodImg = _.findWhere(prodContent, {
                altText: variationProductCode
            });
            
            if (prodImg) {
                var prodImage = prodImg.imageUrl;
                var ImgAltText = prodImg.altText;
               $('.mz-productimages-main').find('img[data-mz-productimage-main]').attr({
                   "src": prodImage + "?max=768",
                   "alt": ImgAltText
               });
               
               $(".mz-productimages-thumbimage").removeClass("is-selected");
               $('.mz-productimages-thumbs').find('img[alt="' + variationProductCode + '"]:first').addClass("is-selected");
            } else {
            	var defaultProdImage = prodContent[0].imageUrl;
                var defaultImgAltText = prodContent[0].altText;
               $('.mz-productimages-main').find('img[data-mz-productimage-main]').attr({
                   "src": defaultProdImage + "?max=768",
                   "alt": defaultImgAltText
               });
               
               $(".mz-productimages-thumbimage").removeClass("is-selected");
               $('.mz-productimages-thumbs').find('img:first').addClass("is-selected");
            }
        },
        addToCart: function (quantity) {
            this.model.addToCart(quantity);
        },
        //to be changed when we integrate Ship to store functionality
        initiateAddToCart: function () {
            var self = this;
            this.checkQuantityOnBlur();
            this.fetchCart().then(function (response) {
                var cartItems = response.data.items,
                    cartItemData = cartItems && cartItems.length ? self.getCartItemData(cartItems) : null,
                    cartItem = cartItemData && cartItemData.cartItem ? cartItemData.cartItem : null,
                    cartPresentQuantity = cartItem ? cartItem.quantity : 0,
                    isQuantityError = self.validateQuantity(cartPresentQuantity),
                    defaultFulfillmentType = cartItemData && cartItemData.isAllCartsItemShip ? "Ship" : self.getDefaultFulfillmentType(),
                    cartPresentFulfillmentType = cartItem ? cartItem.fulfillmentMethod : defaultFulfillmentType,
                    totalQuantity = isQuantityError.maxQuantityAllowed + cartPresentQuantity;
                checkQuantity = isQuantityError.isQuantityValid;
                maxQuantityAllowed = isQuantityError.maxQuantityAllowed;
                if (maxQuantityAllowed <= 0 ) {
                   // $('#limited-quantity-container').modal().show();
                   $('#quantityContainer').find('#quantityWarning').removeClass('hidden');
                } else {
                    self.model.updateQuantity(maxQuantityAllowed);
                    if(cartPresentFulfillmentType === "Pickup"){
                        self.checkPickupAddToCart(totalQuantity, cartPresentQuantity, cartItem, cartPresentFulfillmentType);
                    } else {
                        self.CheckShipAddToCart(totalQuantity, cartPresentQuantity, cartItem, cartPresentFulfillmentType);
                    }
                }
            }, function (error) {
                console.log("Error While fetching cart", error);
            });
        },
        fetchCart: function () {
            return api.get('cart', {});
        },
        getCartItemData: function (cartItems) {
            var shipItemsCount = 0,
                productCode = this.model.get('productCode'),
                cartItem = _.find(cartItems,function(item){
                    if(item.fulfillmentMethod === "Ship")shipItemsCount++;
                    return productCode === item.product.productCode;
                });
            return {
                cartItem: cartItem,
                isAllCartsItemShip: shipItemsCount == cartItems.length ? true : false
            };
        },
        getDefaultFulfillmentType: function(){
            var bopisAvailabelInventory = this.model.get('bopisMaxAllowed'),
                sthWarehouseInventory = this.model.get('shipWarehouseInventory') ? this.model.get('shipWarehouseInventory') : 0,
                defaultFulfillmentType = (bopisAvailabelInventory > 0 && sthWarehouseInventory > 0) || bopisAvailabelInventory > 0 ? "Pickup" : "Ship";
            return defaultFulfillmentType;
        },
        checkPickupAddToCart: function (totalQuantity, cartPresentQuantity, cartItemData, cartPresentFulfillmentType) {
            if (this.model.get('isInventoryAvail') && totalQuantity <= this.model.get('bopisMaxAllowed')) {
                if (cartPresentFulfillmentType === "Ship" || cartItemData) {
                    this.removeProductFromCart(cartItemData, 'Pickup', totalQuantity);
                } else {
                    this.addToCartForPickup();
                }
            } else {
                if(cartItemData){
                    this.removeProductFromCart(cartItemData, 'Ship', totalQuantity);
                } else {
                    this.addToCart(totalQuantity);
                }
            }
        },
        CheckShipAddToCart: function(totalQuantity, cartPresentQuantity, cartItemData, cartPresentFulfillmentType){
            var shipWarehouseInventory = this.model.get('shipWarehouseInventory');
            if(shipWarehouseInventory &&  totalQuantity <= shipWarehouseInventory){
                if(cartPresentFulfillmentType === "Pickup" || cartItemData){
                    this.removeProductFromCart(cartItemData, 'Ship', totalQuantity);
                } else {
                    this.addToCart();
                }
            }else{
                if(cartItemData){
                    this.removeProductFromCart(cartItemData, 'Pickup', totalQuantity);
                } else {
                    this.addToCartForPickup(totalQuantity);
                }
            }
        },
        removeProductFromCart: function(cartItem, fulfillmentType, totalQuantity){
            var self = this,
                cartItemObj = new CartModels.CartItem(cartItem);
            cartItemObj.apiModel.del().then(function() {
                if (fulfillmentType === "Pickup"){
                    self.addToCartForPickup(totalQuantity);
                } else {
                    self.addToCart(totalQuantity);
                }
            });
        },
        checkQuantityOnBlur: function () {
            var $qtyField = $('#product-qty'),
                qtyFieldVal = $qtyField.val();
            if ($qtyField.blur()) {
                if (qtyFieldVal.trim() !== "" && qtyFieldVal > 0 && !isNaN(qtyFieldVal)) {
                    this.model.updateQuantity(parseInt(qtyFieldVal, 10));
                } else {
                    this.model.updateQuantity(1);
                }
            }
        },
        addToCartForPickup: function (modifiedQuantity) {
            var quantity = modifiedQuantity ? modifiedQuantity : this.model.get("quantity");
            var preferredStore = $.parseJSON(localStorage.getItem('preferredStore')) ? $.parseJSON(localStorage.getItem('preferredStore')) : $.parseJSON($.cookie('preferredStore')) ? $.parseJSON($.cookie('preferredStore')):'';
            this.model.addToCartForPickup(preferredStore.code, preferredStore.name, quantity);
        },
        addToWishlist: function () {
        	var me = this;
        	if (user.isAnonymous) {
        		var locale = require.mozuData('apicontext').headers['x-vol-locale'];
        		var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
                locale = locale.split('-')[0];
                var currentLocale = '';
                if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
                	currentLocale = locale === 'fr' ? '/fr' : '/en';
				}
				window.location.href=currentLocale + "/user/login?productCode="+me.model.get('productCode');
        	} else {
        		this.model.addToWishlist();
        	}
        },
        optionInitialize: function () {
        	// handle preset selects, etc
        	var me = this;
        	setTimeout(function(){
        		me.$('[data-mz-product-option]').each(function (index) {
                	if (index === 0) {
                		var $this = $(this), isChecked, wasChecked;
                        if ($this.val()) {
                            switch ($this.attr('type')) {
                                case "checkbox":
                                case "radio":
                                    isChecked = $this.prop('checked');
                                    wasChecked = !!$this.attr('checked');
                                    if ((isChecked && !wasChecked) || (wasChecked && !isChecked)) {
                                        me.configure($this);
                                    }
                                    break;
                                default:
                                    me.configure($this);
                            }
                        }            		
                	}
                    
                });
        	},500);
        },
        setAeroplanMiles: function(aeroplanMiles) {
        	var me = this;
        	var documentListForAeroplanPromo = Hypr.getThemeSetting('documentListForAeroplanPromo');
        	api.get('documentView', {listName: documentListForAeroplanPromo}).then(function(response) {
            	if(response.data.items.length > 0) {
            		var aeroplanMilesFactor = response.data.items[0].properties.markupFactor;
            		me.model.set('aeroplanMiles', aeroplanMilesFactor);
            		var promoCode = response.data.items[0].properties.promoCode;
                    me.model.set('aeroplanPromoCode', promoCode);
                    var startDate = new Date(response.data.items[0].activeDateRange.startDate);
                    var endDate = new Date(response.data.items[0].activeDateRange.endDate);
                    me.model.set('aeroplanPromoDate', true);
            		me.model.set('aeroplanStartDate', startDate.getDate());
                    me.model.set('aeroplanStartMonth', startDate.getMonth() + 1);
                    me.model.set('aeroplanStartYear', startDate.getFullYear());
                    me.model.set('aeroplanEndDate', endDate.getDate());
                    me.model.set('aeroplanEndMonth', endDate.getMonth() + 1);
                    me.model.set('aeroplanEndYear', endDate.getFullYear());
            		me.render();
            	}else {
            		me.model.set('aeroplanMiles', aeroplanMiles);
            		me.render();
            	}
        	});
        },
        setStoreDetailsFromDocumentList: function(){
            var ecomAttr = _.findWhere(this.model.attributes.properties, {'attributeFQN': Hypr.getThemeSetting('soldItemAttr')}),
                ecomAttrVal = ecomAttr ? ecomAttr.values[0].value : undefined,
                supplierDirectAttr = _.findWhere(this.model.attributes.properties, {'attributeFQN': Hypr.getThemeSetting('supplierDirectItem')}),
                supplierDirectAttrVal = supplierDirectAttr ? supplierDirectAttr.values[0].value : undefined,
                productPrice = this.model.get('price').get('price'),
                isStoreDetailsRequired = ecomAttrVal !== Hypr.getThemeSetting('ecommItemValue') || supplierDirectAttrVal === Hypr.getThemeSetting('supplierDirectItemValue') || productPrice == "999999.00";
            if(isStoreDetailsRequired) this.fetchStoreDetails();
        },
        fetchStoreDetails: function(){
            var self = this,
                preferredStore =$.parseJSON(localStorage.getItem('preferredStore')) ? $.parseJSON(localStorage.getItem('preferredStore')) : $.parseJSON($.cookie('preferredStore')) ? $.parseJSON($.cookie('preferredStore')):'',
                queryString = "properties.documentKey eq " + preferredStore.code,
                locationList = Hypr.getThemeSetting("storeDocumentListFQN");
            api.get('documentView', {
                listName: locationList,
                filter: queryString
            }).then(function (response) {
                if (response && response.data.items.length > 0) {
                    var storeContactInfo = response.data.items[0];
                    var contactInfo = {
                        "phone": storeContactInfo.properties.phone.trim(),
                        "fax": storeContactInfo.properties.fax.trim(),
                        "email": storeContactInfo.properties.email.replace(/\s/g, '')
                    };
                    self.model.set("storeContactInfo",contactInfo);
                    Backbone.MozuView.prototype.render.apply(self, arguments);
                }
            });
        },
        checkProvinceMatch: function(configurableMessageResponse,currentStore){
            var me = this;
            if(currentStore && currentStore.address.stateOrProvince){
                var isProvinceMatched = _.findWhere(configurableMessageResponse.msgContent, {province: currentStore.address.stateOrProvince});
                if(isProvinceMatched){
                    me.model.set("provinceMatched",true);
                    me.model.set("minimumOrderMsgPdp",isProvinceMatched.productPageMessage);
                    me.model.set("pdpOrderVolumeMessage",isProvinceMatched.pdpOrderVolumeMsg);
                    me.model.set("minOrderAmount",isProvinceMatched.minimumOrderAmount);
                }else{
                    me.model.set("provinceMatched",false);
                }
            }
        },
        setShippingPostalCode: function(){
            var self = this,
                shippingPostalCodeModel = new ShippingPostalCodeModel();
            shippingPostalCodeModel.getPostalCode().then(function(postalCodeData){
                self.model.set('shippingPostalCodeData', postalCodeData);
                console.log(postalCodeData);
                self.getWarehouseQuantity(postalCodeData);
                Backbone.MozuView.prototype.render.apply(self, arguments);
            }, function(err){
                console.log('Error While fetching postal code', err);
            });
        },
        getWarehouseQuantity: function(postalCodeData){
            var self = this,
                locationCodes = activeWarehousesForSTH.join(','),
                inventoryApi = '/api/commerce/catalog/storefront/products/' + this.model.get('productCode') + '/locationinventory?locationCodes=' + locationCodes + '&time=' + Date.now();
            api.request("GET", inventoryApi).then(function (response) {
                self.addBufferToWarehouseInventory(response.items);
                if(postalCodeData.status === "Valid") self.getShippingTimeline(postalCodeData.postalCode, response.items);
                self.model.set('wareHouseInventory', response.items);
                self.setSThWarehouseInventory(response.items);
                if(postalCodeData.status !== "Valid")Backbone.MozuView.prototype.render.apply(self, arguments);
            }, function(error){
                Backbone.MozuView.prototype.render.apply(self, arguments);
                console.log('error while fetching warehouse inventories',error);
            });
        },
        setSThWarehouseInventory: function(wareHouseInventory){
            var pluckedWarehouseInv =  wareHouseInventory.length ? _.pluck(wareHouseInventory, 'stockAvailable') : 0,
                maxAvailable = _.max(pluckedWarehouseInv);
            this.model.set('shipWarehouseInventory', maxAvailable > 0 ? maxAvailable : 0);
            console.log('shipWarehouseInventory', maxAvailable);
        },
        addBufferToWarehouseInventory: function(warehouseData){
            var self = this;
            _.each(warehouseData, function(warehouse){
                var stockAvailable = warehouse.stockAvailable > 0 ? self.getBufferedInventory(warehouse.stockAvailable) : 0;
                warehouse.stockAvailable = stockAvailable;
            });
            console.log('warehouseData', warehouseData);
        },
        setWarehouseInventory: function(fetchedWarehouseCount){
            var warehouseInventoryCount = this.getBufferedInventory(fetchedWarehouseCount);
            this.model.set('warehouseInventoryCount', warehouseInventoryCount);
            this.model.set('isInventoryAvail', warehouseInventoryCount > 0 ? true : false);
            this.model.set('wareHouseInventoryAvail',  warehouseInventoryCount > 0 ? true : false);
            this.model.set("isLimitedQuantity",warehouseInventoryCount > 0 && warehouseInventoryCount <= 3 ? true : false);
            console.log("warehouseInventoryCount", warehouseInventoryCount);
        },
        getBufferedInventory: function(fetchedWarehouseCount){
            var isWarehouseBufferEnabled = Hypr.getThemeSetting('enableWarehouseBuffer'),
                warehouseBufferValue = Hypr.getThemeSetting('warehouseBufferValue'),
                bufferedWarehouseInventory = 0,
                warehouseCount = fetchedWarehouseCount && fetchedWarehouseCount > 0 ? fetchedWarehouseCount : 0;
            warehouseInventoryCount = warehouseCount;
            if(isWarehouseBufferEnabled && warehouseBufferValue > 0 && warehouseCount > 1){
                bufferedWarehouseInventory = Math.round((warehouseCount/100)*warehouseBufferValue);
                bufferedWarehouseInventory = bufferedWarehouseInventory < 0.5 ? 1 : bufferedWarehouseInventory;
                warehouseInventoryCount = warehouseCount - bufferedWarehouseInventory;
            }
            return warehouseInventoryCount;
        },
        getShippingTimeline: function(postalCode, warehouseInventory){
            var self = this,
                shippingModel = new ShippingTimelineModel();
            shippingModel.getShippingTimeline(postalCode, warehouseInventory, 1).then(function(timelineData){
                console.log('getShippingTimeline',timelineData);
                self.model.set('sthShippingTimeline', timelineData.shippingTimeline);
                Backbone.MozuView.prototype.render.apply(self, arguments);
            }, function(error){
                Backbone.MozuView.prototype.render.apply(self, arguments);
                console.log('Error while getting shipping Timelines', error);
            });
        },
        showEstimateShippingModal: function(event){
            if(!this.model.get('isShippingModalOpened')){
                var productMeasurements =  this.model.apiModel.data.measurements,
                    productPrice = this.model.apiModel.data.price,
                    isCurrentPostalCodeValid = $(event.currentTarget).hasClass('change-location');

                if(estimateShippingModalView) estimateShippingModalView.undelegateEvents();
                estimateShippingModalView = new EstimateShippingModalView({
                    el: $('#shipping-popup-container'),
                    model: new Backbone.MozuModel({
                        quantity: this.model.get('quantity'),
                        productPrice: productPrice,
                        productMeasurements : productMeasurements,
                        isCurrentPostalCodeValid : isCurrentPostalCodeValid,
                        ehfValue: this.model.get('ehfValue') ? this.model.get('ehfValue') : 0,
                        shippingData: this.model.get('shippingPostalCodeData'),
                        wareHouseInventory: this.model.get('wareHouseInventory'),
                        sthShippingTimeline: this.model.get('sthShippingTimeline')
                    })
                });
                this.model.set('isShippingModalOpened', true);
                estimateShippingModalView.render();
            }
        },
        getTotalInventory: function (locationInventory) {
            var me = this,
                warehouseInventoryCount = me.model.get('warehouseInventoryCount'),
                locationInventoryCount = locationInventory && locationInventory > 0 ? locationInventory : 0,
                totalInventoryCount = warehouseInventoryCount + locationInventoryCount;
            if (totalInventoryCount > 0) {
                me.model.set("showAddToCart", true);
                me.model.set('isInventoryAvail', true);
            } else {
                me.model.set("isInventoryAvail", false);
                me.model.set("showAddToCart", false);
            }
            if (totalInventoryCount == 1 || totalInventoryCount == 2 || totalInventoryCount == 3) {
                me.model.set("isLimitedQuantity", true);
            } else {
                me.model.set("isLimitedQuantity", false);
            }
            Backbone.MozuView.prototype.render.apply(me, arguments);
        },
        refreshPage: function(){
            var self = this;
            $(document).on('refreshPdp', function(event, data){
                self.model.set('shippingPostalCodeData', data.shippingData);
                self.model.set('isShippingModalOpened', false);
                self.model.set('sthShippingTimeline', data.shippingTimeline);
                self.render();
            });
        },
        initialize: function () {
        	this.optionInitialize();
        	var me = this;
            var currentStore;
            today = new Date();
            var contextSiteId = apiContext.headers["x-vol-site"];
            var homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId");
            var frSiteId = Hypr.getThemeSetting("frSiteId");
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
        	locale = locale.split('-')[0];
        	me.model.set("currentLocale", locale);
			me.model.set("currentSite", currentSite);

            if (usePDPArcFunction) {
                console.log("********usePDPArcFunction***********" + usePDPArcFunction);
                var pdpArcStr = me.model.get('mfgPartNumber');
                if (pdpArcStr) {
                    var pdpArcData = JSON.parse(pdpArcStr);
                    me.model.set('aeroplanMiles', pdpArcData.aeroplanMiles);
                    me.model.set('aeroplanPromoCode', pdpArcData.aeroplanPromoCode);
                    me.model.set('aeroplanPromoDate', pdpArcData.aeroplanPromoDate);
                    me.model.set('aeroplanStartDate', pdpArcData.aeroplanStartDate);
                    me.model.set('aeroplanStartMonth', pdpArcData.aeroplanStartMonth);
                    me.model.set('aeroplanStartYear', pdpArcData.aeroplanStartYear);
                    me.model.set('aeroplanEndDate', pdpArcData.aeroplanEndDate);
                    me.model.set('aeroplanEndMonth', pdpArcData.aeroplanEndMonth);
                    me.model.set('aeroplanEndYear', pdpArcData.aeroplanEndYear);
                    me.model.set('ehfValue', pdpArcData.ehfValue);
                    me.model.set('stockAvailable', pdpArcData.stockAvailable);
                    me.model.set('isInventoryAvail', pdpArcData.isInventoryAvail);
                    me.model.set('isLimitedQuantity', pdpArcData.isLimitedQuantity);
                    me.model.set('wareHouseInventoryAvail', pdpArcData.isInventoryAvail);
                    me.model.set('isItemRestricted', pdpArcData.isItemRestricted);
                    me.setWarehouseInventory(pdpArcData.stockAvailable);
                    if (pdpArcData.isWebsiteInd) {
                        me.model.set('isWebsiteInd', pdpArcData.isWebsiteInd);
                    }
                    console.log("********pdpArcData***********" + pdpArcData);
                }
            }

            if(homeFurnitureSiteId === contextSiteId){
                me.model.set("isHomeFurnitureSite", true);
            }
            if(frSiteId === contextSiteId){
                me.model.set("isFrenchSite", true);   
            }
            if(localStorage.getItem('preferredStore')){
                currentStore = JSON.parse(localStorage.getItem('preferredStore'));
                me.model.set({"preferredStore":currentStore});
            }
        	if (localStorage.getItem('preferredStore') || $.cookie('preferredStore')) {
                //Set store level ship to home flag
                var preferredStoreDetails = $.parseJSON(localStorage.getItem('preferredStore')) ? $.parseJSON(localStorage.getItem('preferredStore')) : $.parseJSON($.cookie('preferredStore')) ? $.parseJSON($.cookie('preferredStore')):'';
                this.model.set('storeAllowsShipToHome', !!_.find(preferredStoreDetails.attributes,
                    function predicate(attribute) {
                        return (attribute.fullyQualifiedName === Hypr.getThemeSetting('shipToHomeStoreFlagAttrFQN') &&
                            attribute.values[0]);
                    }));
            }
            this.model.set('isEhfValueSet', false);
            if((localStorage.getItem('preferredStore') || $.cookie('preferredStore')) && this.model.get('properties').length)this.setStoreDetailsFromDocumentList();
            var productProperties = this.model.get('properties') && this.model.get('properties').length ? this.model.get('properties') : [],
                isSThAttrPresent =  productProperties.length ? _.findWhere(productProperties, {'attributeFQN': Hypr.getThemeSetting('shipToHomeEnableAttrName')}) : false,
                isProductEnabledForSTH = isSThAttrPresent ? isSThAttrPresent.values[0].value : false;
            if((localStorage.getItem('preferredStore') || $.cookie('preferredStore')) && this.model.get('storeAllowsShipToHome') && isProductEnabledForSTH){
                this.setShippingPostalCode();
                this.refreshPage();
            }
            if(currentStore) {
                var remoteStoreattribute = _.findWhere(currentStore.attributes, {"fullyQualifiedName": Hypr.getThemeSetting("remoteStoreAttr")});
                if (remoteStoreattribute) {
                    var isRemoteStore = remoteStoreattribute.values[0];
                    if(isRemoteStore===true)
                    {
                        $('.cart-icon').addClass('hidden');
                    }
                    else
                    {
                        $('.cart-icon').removeClass('hidden');
                    }
                    me.model.set("isRemoteStore", isRemoteStore);
                }
                var attribute = _.findWhere(currentStore.attributes, {"fullyQualifiedName": Hypr.getThemeSetting("isEcomStore")});     
	            if(attribute){
	                var isEcomStore = attribute.values[0] === "Y" ? true : false; 
	                me.model.set("isEcomStore", isEcomStore);
	            }
	           
	            var aeroplanMiles;
	            me.model.on("productConfigured", function(response){
	            	if(response.get('price.salePrice')) {
	                	aeroplanMiles = Math.floor(response.get('price.salePrice')/2); 
	                }else {
	                	aeroplanMiles = Math.floor(response.get('price.price')/2); 
	                }
	                me.setAeroplanMiles(aeroplanMiles);
	            });
	            
	            if(!me.model.get('hasPriceRange')) {
	            	if(me.model.get('price.salePrice')) {
	                	aeroplanMiles = Math.floor(me.model.get('price.salePrice')/2); 
	                }else {
	                	aeroplanMiles = Math.floor(me.model.get('price.price')/2); 
	                }
	                me.setAeroplanMiles(aeroplanMiles);
	            }
	            
	        	//check EHF for current product
	            var entityListForEHF = Hypr.getThemeSetting('entityListForEHF');
	        	var productCode;
	        	if(me.model.get('variations') && me.model.get('variations').length > 0) {
	        		//from current variation find the variationProductCode
	        		var productOptionValue = $('.mz-productoptions-option').val();
	        		var variation = _.find(me.model.get('variations'), function(variation) {
	        			return _.findWhere(variation.options, {'value': productOptionValue});
	        		});
	        		productCode = me.model.get('variations')[0].productCode;
	        	}else {
	        		productCode = me.model.get('productCode');
	        	}
	        	var filterQuery = 'productCode eq ' + productCode + ' and province eq ' + currentStore.address.stateOrProvince;
	        	var ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
	        	api.get('entityList', { listName: entityListForEHF, filter: filterQuery }).then(function(response) {
	        		if(response.data.items.length > 0) { //if EHF is applied for current product
	        			var ehfOption = me.model.get('options').findWhere({'attributeFQN': ehfAttributeFQN});
	                    if(ehfOption) {
	                    	ehfOption.set('shopperEnteredValue', response.data.items[0].feeAmt);
	                    }
                        me.model.set('ehfValue', response.data.items[0].feeAmt);
	        			me.render();
	        		}
	        	});
	        	
	        	//entityListForItemRestricted
	        	filterQuery = 'productCode eq ' + productCode + ' and (province eq ' + currentStore.address.stateOrProvince + ' or store eq ' + currentStore.code + ')';
	        	var entityListForItemRestricted = Hypr.getThemeSetting('entityListForItemRestricted');
	        	api.get('entityList', { listName: entityListForItemRestricted, filter: filterQuery}).then(function(response) {
	        		if(response.data.items.length > 0) {
						if(response.data.items[0].ecommerce_ind === "Y"){
                            me.model.set('isItemRestricted', true);
                        }else {
                            me.model.set('isItemRestricted', false);
                        }
	                    var websiteInd = response.data.items[0].website_ind;
	                    if(websiteInd === 'N'){
	                         me.model.set('isWebsiteInd', true);
	                    }
	        			me.render();
	        		}else{
                        me.model.set('isItemRestricted', true);
                        me.render();
                    }
	        	});
	        	
	        	var currentStoresWarehouse = _.find(currentStore.attributes, function(attribute){
	        		return attribute.fullyQualifiedName === Hypr.getThemeSetting('warehouseAttr');
				});

				var isBOHStore = _.find(currentStore.attributes, function(attribute){
            		return attribute.fullyQualifiedName === Hypr.getThemeSetting('hhBOHStoreAttr');
                });
                if (isBOHStore && isBOHStore.values[0]) {
                    me.model.set("isBOHStore", true);
                }
	        	if(currentStoresWarehouse) {
	        		var inventoryApi = '/api/commerce/catalog/storefront/products/' + productCode + '/locationinventory?locationCodes=' + currentStoresWarehouse.values[0];
	            	api.request("GET", inventoryApi).then(function(response){
	            		if (response && response.items.length > 0) {
                            if (response.items[0].stockAvailable > 0) {
                                me.model.set('isInventoryAvail', true);
                                me.model.set('wareHouseInventoryAvail', true);
                                if (response.items[0].stockAvailable == 1 || response.items[0].stockAvailable == 2 || response.items[0].stockAvailable == 3) {
                                    me.model.set("isLimitedQuantity", true);
                                }
                                me.render();
                            } else {
                                $(".mobile-show-sale").removeClass("hidden");
                                me.render();
                            }
                        }
	            	});
				}
				
				//store level inventory fetch
                if(isBOHStore && isBOHStore.values[0]){
                    api.request("GET", '/api/commerce/catalog/storefront/products/' + productCode + '/locationinventory?locationCodes=' + currentStore.code).then(function(response){
                        if(response && response.items.length > 0){
                            me.model.set("bohStore",true); 
                            locationInventoryCount = response.items[0].stockAvailable;
                            me.model.set("storeInventoryData",response.items[0]);
                            me.model.set("showBOH",true);
                            me.model.set('locationInventoryCount', locationInventoryCount);
                            me.render(); 
                            me.getTotalInventory(locationInventoryCount);
                        }else{
                            me.model.set("bohStore",true);
                            me.model.set("showBOH",true);
                            me.render(); 
                        }
                    });
                }else{
                    api.get("location",{code:currentStore.code}).then(function(response){
                        if(response){
                            isBOHStore = _.find(response.data.attributes, function(attribute){
                                return attribute.fullyQualifiedName === Hypr.getThemeSetting('hhBOHStoreAttr');
                            });
                            if(isBOHStore && isBOHStore.values[0]){
                                var selectedStore = response.data;
							    CookieUtils.setPreferredStore(selectedStore);
                                api.request("GET", '/api/commerce/catalog/storefront/products/' + productCode + '/locationinventory?locationCodes=' + currentStore.code).then(function(response){
                                    if(response && response.items.length > 0){
                                        me.model.set("bohStore",true); 
                                        me.model.set("storeInventoryData",response.items[0]);
                                        me.model.set("showBOH",true);
                                        me.render(); 
                                    }else{
                                        me.model.set("bohStore",true);
                                        me.model.set("showBOH",true);
                                        me.render(); 
                                    }
                                });
                            }else{
                                me.model.set("bohStore",false);
                                me.model.set("showBOH",true);
                                me.render(); 
                            }    
                        }
                    }); 
                }
				
				//Set date for product availability (as of) and pickup date at store (current date + 15)

                me.model.set("stockAvailableAsOF",me.formatDate(today)); 
                var todayDate = new Date();
                //me.model.set("pickUpAtStoreBy",me.formatDate(todayDate.setDate(todayDate.getDate() + 15)));
            	//IWM-222 Remove get it by messaging on PDP & NRTI Check Nearby Store Locator
                var torontoTimeZone = timeZoneSupport.findTimeZone('America/Toronto');
                var promoItemZonedTime = timeZoneSupport.getZonedTime(new Date(me.model.get("updateDate")), torontoTimeZone);
                var promoItemDate = new Date(promoItemZonedTime.year, promoItemZonedTime.month-1, promoItemZonedTime.day, promoItemZonedTime.hours, promoItemZonedTime.minutes, promoItemZonedTime.seconds, promoItemZonedTime.milliseconds);
                if(today <= promoItemDate){
                    me.model.set("isPromoEnabled",true);
                    me.model.set("promoTillDate",me.formatDate(promoItemDate));
                }
                var messageConfig = $(".mz-configuarble-message-container").attr('data-mz-message-config');
                var parsedMessageRes = messageConfig ? JSON.parse(messageConfig) : '';
                me.checkProvinceMatch(parsedMessageRes,currentStore);
            }else{
                me.model.set("showBOH",true);
            }
            
            var createDate = me.model.get("createDate");
            var newTagDate = moment(createDate).add(30,'d');
            if(moment(createDate) <= moment(today) && moment(today) <= moment(newTagDate)){
                me.model.set("newTagPromotion",true);
            }
            var onlineOnlyProducts = Hypr.getThemeSetting('productCodes');
            var onlineOnlyProductsList = onlineOnlyProducts.split(",");
            var onlineOnly =  _.find(onlineOnlyProductsList, function(product){ return product === me.model.get("productCode"); });
            var promoStartsAt = new Date(Hypr.getThemeSetting("onlineOnlyMessageStartDate"));
            var promoEndsAt = new Date(Hypr.getThemeSetting("onlineOnlyMessageEndDate"));

            if((onlineOnly) && (promoStartsAt <= today && today <= promoEndsAt)){
                me.model.set("isOnlineOnlyProduct",true);
            } 
            api.get('cart', {}).then(function (response) {
                var cartItems = response.data.items,
                    preferredStore = $.parseJSON(localStorage.getItem('preferredStore')) ? $.parseJSON(localStorage.getItem('preferredStore')) : $.parseJSON($.cookie('preferredStore')) ? $.parseJSON($.cookie('preferredStore')):'';
                for (var i = 0; i < cartItems.length; i++) {
                    if(cartItems[i].product.productCode === me.model.get('productCode')) me.model.set('cartFulfillmentMethod',cartItems[i].fulfillmentMethod);
                    if ((cartItems[i].fulfillmentMethod === 'Pickup' && cartItems[i].fulfillmentLocationCode !== preferredStore.code) || (cartItems[i].fulfillmentMethod === 'Ship' && cartItems[i].purchaseLocation !== preferredStore.code)) {
                        var cartItemData = cartItems && cartItems.length ? me.getCartItemData(cartItems) : 0,
                            cartPresentQuantity = cartItemData && cartItemData.cartItem ? cartItemData.cartItem.cartItemQuantity : 0,
                            isQuantityError = me.validateQuantity(cartPresentQuantity),
                            finalMaxAllowedQuantity = isQuantityError.maxQuantityAllowed + cartPresentQuantity;
                        if (cartPresentQuantity !== 0 && cartPresentQuantity > finalMaxAllowedQuantity) {
                            maxQuantityAllowed = finalMaxAllowedQuantity > cartPresentQuantity ? cartPresentQuantity - finalMaxAllowedQuantity : finalMaxAllowedQuantity - cartPresentQuantity;
                            CartMonitor.addToCount(maxQuantityAllowed);
                        }
                        break;
                    }
                }
            });
            setTimeout(function() {
                me.$el.find(".orderVolumeMsgAddToCart").removeClass("hidden");
                console.log(me.model.get('isEcomStore'),me.model.get('bohStore'));
                if(me.model.get('isEcomStore')===false &&  me.model.get('bohStore')===false)
                {
                    me.$el.find("#product-detail").find(".orderVolumeMsg").addClass("hidden");
                }
                else  if(me.model.get('isEcomStore')===false &&  me.model.get('bohStore')===true)
                {
                    me.$el.find("#product-detail").find(".orderVolumeMsg").addClass("hidden");
                }
                else
                {
                    me.$el.find("#product-detail").find(".orderVolumeMsg").removeClass("hidden");
                }
            },3500);
        }, 
        sliderFunction: function() {
            var divLength = $('.slider-first-item').size();  
            var sildeLoop = null;
            $(document).ready(function(){
        		$("figure.mz-productimages-thumbs").removeClass("hidden");
        	});
            if(divLength > 1) {
                sildeLoop =  true;
            }
            else {
                sildeLoop = false;
            }
            $(".regular").not('.slick-initialized').slick({
                dots: false,
                infinite: false,
                slidesToShow: 2,
                slidesToScroll: 2,
                variableWidth:true,
                responsive: [
                 {
                   breakpoint: 420,
                   settings: {
                     slidesToShow: 3,
                     slidesToScroll: 3
                   }
                 },
                 {
                   breakpoint: 320,
                   settings: {
                     slidesToShow: 2,
                     slidesToScroll: 2
                   }
                 }
               ]
            });
		},
		changePreferredStore: function(e){
			e.preventDefault();
			var locale = require.mozuData('apicontext').headers['x-vol-locale'];
			var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
			locale = locale.split('-')[0];
			var currentLocale = '';
			if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
				currentLocale = locale === 'fr' ? '/fr' : '/en';
			}
			window.location.href=currentLocale + "/store-locator?returnUrl="+window.location.pathname + window.location.search;
		},
		checkNearbyStores: function(e){
			
			$(document).find('#QuickviewModal').modal('hide'); 
			$(document).find('#check-nearby-store-modal').modal().show();
			setTimeout(function() {
				$('body').addClass('modal-open');
			},1000);
			$("#check-nearby-loading").show();
            var locationList = Hypr.getThemeSetting("storeDocumentListFQN");
            var self = this,
            locationInventory = null;
            var contextSiteId = apiContext.headers["x-vol-site"];
            var productCode = self.model.get('productCode');
            var isEcomEnabled = _.findWhere(self.model.attributes.properties, {'attributeFQN':Hypr.getThemeSetting('soldItemAttr')});
            
            if(!isMapboxEnabled){
                var mapboxGlCSS = document.createElement('link');
                mapboxGlCSS.setAttribute( 'href', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.44.2/mapbox-gl.css');
                mapboxGlCSS.setAttribute( 'rel', 'stylesheet');
                mapboxGlCSS.setAttribute( 'type', 'text/css');
                document.head.appendChild( mapboxGlCSS );
                var unwiredGlCSS = document.createElement('link');
                unwiredGlCSS.setAttribute( 'href', 'https://tiles.unwiredmaps.com/v2/js/unwired-gl.css?v=0.1.6');
                unwiredGlCSS.setAttribute( 'rel', 'stylesheet');
                unwiredGlCSS.setAttribute( 'type', 'text/css');
                document.head.appendChild( unwiredGlCSS );
                isMapboxEnabled = true;
            }     
            api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter='+filtersString+'&pageSize=1100', {
                cache: false
            }).then(function (response) {
                var curStore = $.parseJSON(localStorage.getItem('preferredStore')) ? $.parseJSON(localStorage.getItem('preferredStore')) : $.parseJSON($.cookie('preferredStore')) ? $.parseJSON($.cookie('preferredStore')):'';
                var range = Hypr.getThemeSetting("storeDistanceInkm");
                var nearStoresList = [];
                if (response && response.items.length > 0) {
                    nearStoresList = findNearStores(response.items, curStore.geo, range);
                    var prefStore = _.findWhere(nearStoresList, {
                        name: curStore.code
                    });
                    if (prefStore) {
                        nearStoresList = _.without(nearStoresList, _.findWhere(nearStoresList, {
                            name: curStore.code
                        }));
                        nearStoresList.unshift(prefStore);
                    }

                    nearStoresList = nearStoresList.slice(0, 49);
                    _.each(nearStoresList, function (store) {
                        setStoreStatus(store, days[today.getDay()]);
                    });

                    var locationCodes = "";

                    _.each(nearStoresList,function(item,index){
                        if(index != nearStoresList.length-1){
                            locationCodes += item.name+",";
                          }else{
                            locationCodes += item.name;
                        }
                    });
                    
                    api.request('GET','/api/commerce/catalog/storefront/products/'+productCode+'/locationinventory?pageSize=1100&locationCodes='+locationCodes).then(function(response){
                        locationInventory = response.items;
                        var currentDate = new Date();
                        _.each(nearStoresList,function(item){
                            var storeInventory = _.findWhere(locationInventory, {
                                locationCode: item.name
                            });
                            if(storeInventory){
                                item.locationInventory = storeInventory;
                                //item.getItBy = me.formatDate(new Date().setDate(currentDate.getDate() + 15));
                            	//IWM-222 Remove get it by messaging on PDP & NRTI Check Nearby Store Locator
                                if(isEcomEnabled){
                                    item.itemEcomEnabled = true; 
                                }
                            }else{
                            	//item.getItBy = me.formatDate(new Date().setDate(currentDate.getDate() + 15));
                            	//IWM-222 Remove get it by messaging on PDP & NRTI Check Nearby Store Locator
                                if(isEcomEnabled){
                                    item.itemEcomEnabled = true;
                                }
                            }
                        });

                        if (ua.indexOf('chrome') > -1) {
                            if (/edg|edge/i.test(ua)){
                                $.cookie("quickViewProduct",JSON.stringify({'productCode':productCode, 'isEcomEnabled': isEcomEnabled ? true : false}),{path:'/'});
                            }else{
                                document.cookie = "quickViewProduct="+JSON.stringify({'productCode':productCode, 'isEcomEnabled': isEcomEnabled ? true : false})+";path=/;Secure;SameSite=None";
                            }
                        } else {
                            $.cookie("quickViewProduct",JSON.stringify({'productCode':productCode, 'isEcomEnabled': isEcomEnabled ? true : false}),{path:'/'});
                        }
                        if($.cookie('fullAssortmentProduct')){
                            document.cookie = "fullAssortmentProduct="+ false +";Secure;SameSite=None";
                        }else{
							document.cookie = "fullAssortmentProduct="+ false +";Secure;SameSite=None";
						}
                        var storeData = {
                            "stores": nearStoresList
                        };
                        LoadResources.load("/scripts/unwired-gl.js", function(){
                            var checkNearbyStoreView = new CheckNearbyStoreModal.CheckNearbyStore({
                                el: $('#check-nearby-store-modal'),
                                model: new Backbone.Model(storeData)
                            });
                            checkNearbyStoreView.render();
                        });
                    });

                }
            }, function (error) {
                console.error("API Error: ", error);
            });
        },
        formatDate: function(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }
    });
    
    function getParentCategory(currentCategory,categoryString,productCode) {
        var lbmCategoryCode = Hypr.getThemeSetting('newLBMAndHardlinesCategoryCode');
        if(currentCategory.parentCategory){
            if(currentCategory.parentCategory.categoryCode !== lbmCategoryCode){
                categoryString = currentCategory.parentCategory.content.name + "/" + categoryString;
                getParentCategory(currentCategory.parentCategory, categoryString,productCode);
            }else{
                var crumbFlowArray; 
                var data={
                   "productCode":productCode,
                   "flow":categoryString
                };
                var expiryDate = new Date();
                expiryDate.setYear(expiryDate.getFullYear() + 1);
                if($.cookie('BreadCrumbFlow')){
                    crumbFlowArray = $.parseJSON($.cookie("BreadCrumbFlow"));
                    var isavailable = _.findWhere(crumbFlowArray, {'productCode' : data.productCode});
					if(!isavailable){
						if(data.flow){
							crumbFlowArray.push(data);
						}
					}else{
						_.find(crumbFlowArray, function(prod, index) {
							if(prod.productCode == data.productCode) {
								crumbFlowArray[index] = data;
							}
						});
					}

					if (ua.indexOf('chrome') > -1) {
                        if (/edg|edge/i.test(ua)){
                            $.cookie('BreadCrumbFlow', JSON.stringify(crumbFlowArray),{path: '/',expires: expiryDate}); 
                        }else{
                            document.cookie = "BreadCrumbFlow="+JSON.stringify(crumbFlowArray)+";expires="+expiryDate+";path=/;Secure;SameSite=None";
                        }
                    } else {
                        $.cookie('BreadCrumbFlow', JSON.stringify(crumbFlowArray),{path: '/',expires: expiryDate});
                    }
                }else{
                    if(data.flow){
                        crumbFlowArray = [];
                        crumbFlowArray.push(data);

                        if (ua.indexOf('chrome') > -1) {
                            if (/edg|edge/i.test(ua)){
                                $.cookie('BreadCrumbFlow', JSON.stringify(crumbFlowArray),{path: '/',expires: expiryDate}); 
                            }else{
                                document.cookie = "BreadCrumbFlow="+JSON.stringify(crumbFlowArray)+";expires="+expiryDate+";path=/;Secure;SameSite=None";
                            }
                        } else {
                            $.cookie('BreadCrumbFlow', JSON.stringify(crumbFlowArray),{path: '/',expires: expiryDate});
                        }
                    }
                }
            }
        }
    }

	var quickViewBind = function(){		
		
		var globalProductImagesView;
		$(document).on('click', 'a[data-mz-product-code]', function(e){
			e.preventDefault();
			var productCode = $(this).data('mz-product-code');
			var productToAdd = new ProductModels.Product({
	            productCode: productCode
	        });
			
			/* Info Popover */
			$('[data-toggle="popover"]').popover();
			
			productToAdd.fetch().then(function() {
				var productImagesView = new ProductImageViews.ProductPageImagesView({
		            el: $('[data-mz-productimages]'),
		            model: productToAdd
		        });
				globalProductImagesView = productImagesView;
				
				var quickProductModalView = new QuickProductModalView({
		            el: $('#QuickviewModal'),
		            model: productToAdd
		        });
				quickProductModalView.render();				
				$('#QuickviewModal').modal();
			});
            
			productToAdd.on('addedtocart', function (cartitem) {                
	            if (cartitem && cartitem.prop('id')) {
	            	productToAdd.isLoading(true);
                    CartMonitor.addToCount(productToAdd.get('quantity'));
                    var pageContext = require.mozuData('pagecontext');
	                var locale = require.mozuData('apicontext').headers['x-vol-locale'];
	                var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
	                locale = locale.split('-')[0];
	                var currentLocale = '';
	                if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
	                	currentLocale = locale === 'fr' ? '/fr' : '/en';
	                }
	                var currAttribute = _.findWhere(productToAdd.attributes.properties, {'attributeFQN':Hypr.getThemeSetting('brandDesc')});
                    
                    var breadcrumbs = $(".mz-breadcrumb-link:not('.is-first')");
                    var breadcrumbstring = '';
                    for(var i=0; i<breadcrumbs.length; i++) {
                        if(i !== breadcrumbs.length-1) {
                            breadcrumbstring += $.trim(breadcrumbs[i].text) + '/'; 
                        }else {
                            breadcrumbstring += $.trim(breadcrumbs[i].text);
                        }
                    }


                    var currentCategoryId = pageContext.categoryId;
                    var thisCategory = _.findWhere(productToAdd.get('categories'), {'categoryId' : currentCategoryId});
                    if(pageContext.pageType=='category' && thisCategory && thisCategory.parentCategory.categoryId != Hypr.getThemeSetting('dynamicCategoryId')){
                    	breadcrumbstring += '/' + $.trim($(".mz-breadcrumb-current").text());
                    }
                    else if(pageContext.pageType=='category' && thisCategory && thisCategory.parentCategory.categoryId == Hypr.getThemeSetting('dynamicCategoryId')){
                        breadcrumbstring = thisCategory.content.name;
                    }
                    var crumbFlowArray; 
                    var data={
                       "productCode":productToAdd.get('productCode'),
                       "flow":breadcrumbstring
                    };
                    var expiryDate = new Date();
                    expiryDate.setYear(expiryDate.getFullYear() + 1);
                    
                    if($.cookie('BreadCrumbFlow')){
                        crumbFlowArray = $.parseJSON($.cookie("BreadCrumbFlow"));
                        var isavailable = _.findWhere(crumbFlowArray, {'productCode' : data.productCode});
                                if(!isavailable){
                                	if(data.flow){
                                		crumbFlowArray.push(data);
                                	}
                                }else{
                                    _.find(crumbFlowArray, function(prod, index) {
                                        if(prod.productCode == data.productCode) {
                                            crumbFlowArray[index] = data;
                                        }
                                    });
                                }

                                if (ua.indexOf('chrome') > -1) {
                                    if (/edg|edge/i.test(ua)){
                                        $.cookie('BreadCrumbFlow', JSON.stringify(crumbFlowArray),{path: '/',expires: expiryDate}); 
                                    }else{
                                        document.cookie = "BreadCrumbFlow="+JSON.stringify(crumbFlowArray)+";expires="+expiryDate+";path=/;Secure;SameSite=None";
                                    }
                                } else {
                                    $.cookie('BreadCrumbFlow', JSON.stringify(crumbFlowArray),{path: '/',expires: expiryDate});
                                }
                    }else{
                    	if(data.flow){
	                        crumbFlowArray = [];
	                        crumbFlowArray.push(data);

                            if (ua.indexOf('safari') != -1) { 
                                if (ua.indexOf('chrome') > -1) {
                                    document.cookie = "BreadCrumbFlow="+JSON.stringify(crumbFlowArray)+";expires="+expiryDate+";path=/;Secure;SameSite=None";
                                } else {
                                    document.cookie = "BreadCrumbFlow="+escape(JSON.stringify(crumbFlowArray))+";expires="+escape(expiryDate)+";path=/;Secure;SameSite=None";
                                }
                            }else{
                                document.cookie = "BreadCrumbFlow="+JSON.stringify(crumbFlowArray)+";expires="+expiryDate+";path=/;Secure;SameSite=None";    
                            }
                    	}
                    }
    
                    var isCategorypage = pageContext.pageType=='category';
                    var currentCategory = productToAdd.get('categories')[0];
                    if(!isCategorypage){
                        var categoryString = currentCategory.content.name;
                        getParentCategory(currentCategory,categoryString,productToAdd.get('productCode'));
                    }
                    
                    var productPrice = productToAdd.get('price').get('salePrice') ? productToAdd.get('price').get('salePrice') : productToAdd.get('price').get('price');
                    var cookieCategoryName;
                     if($.cookie('BreadCrumbFlow')){
                        var FlowArray = $.parseJSON($.cookie("BreadCrumbFlow"));
                        var getproduct = _.findWhere(FlowArray, {'productCode' : productToAdd.get('productCode')});
                        if(getproduct){
                            cookieCategoryName = getproduct.flow;
                        }else{
                            cookieCategoryName = breadcrumbstring;
                        }
                     }
                     api.get('cart',{}).then(function(cartdata){
                        var saleforceData = [];
                        _.each(cartdata.data.items,function(item){
                          saleforceData.push({
                            'id': item.product.productCode, // String. Any unique identifier for a product/SKU
                            'price': item.product.price.salePrice > 0 ? parseFloat(item.product.price.salePrice).toFixed(2).replace('$', '').replace(',', '.').trim() : parseFloat(item.product.price.price).toFixed(2).replace('$', '').replace(',', '.').trim(),
                            'quantity': item.quantity
                          });
                        });
                        dataLayer.push({
                            'event': 'quickAddToCart',
                            'metric4':cartdata.data.total, // String/Currency. This metric tracks total dollar value  that is in the cart (for all products) after this product(s) addition 
                            'ecommerce': {
                              'currencyCode': 'CAD',
                              'add': {
                                'products': [{                            
                                  'name': productToAdd.get('content').get("productName"),
                                  'id': productToAdd.get('productCode'),
                                  'price': productPrice.toFixed(2).replace('$', '').replace(',', '.').trim(),
                                  'brand': currAttribute ? currAttribute.values[0].stringValue : '',
                                  'category': cookieCategoryName, 
                                  'quantity': parseInt(productToAdd.get("quantity"),10) 
                                 }
                          ]}
                          },
                          'salesForce': {
                            'products': saleforceData,
                            'cartRecoveryId': cartdata.data.id
                            }
                          });
                     });

	                window.location.href = currentLocale + "/cart";
	            } else {
	            	productToAdd.trigger("error", { message: Hypr.getLabel('unexpectedError') });
	            }
	        });
			
			productToAdd.on('addedtowishlist', function (cartitem) {
	            $('#add-to-wishlist').prop('disabled', 'disabled').text(Hypr.getLabel('savedToList'));
	            
	            /*Update Wishlist Count*/
	            api.get('wishlist',{}).then(function(response){        	
	            	var items = response.data.items[0].items;
	                var totalQuantity = 0;
	                _.each(items, function(item, index) {
	                	totalQuantity += item.quantity;
	                });
	                $('#wishlist-count').html('('+totalQuantity+')');
	            });
	            
	        });
		});
		
		//code to change product image on click of thumbnail		
		$(document).on('click', 'span[data-mz-productimage-thumb]', function (e) {
			var $thumb = $(e.currentTarget);
	        
            if($("#videoContainer").css("display") == "block"){
            	$(".product-main-video").attr("id", "");
                $(".product-main-video").attr("name", "");
                $(".product-main-video").attr("src", "");
                $("#videoContainer").toggleClass("show-video");
                $(".ign-product-videothumb").removeClass("is-selected");
                $("#imageContainer").show();
            }

            $(".mz-productimages-thumbimage").removeClass("is-selected");
	        $thumb.find(".mz-productimages-thumbimage").addClass("is-selected");
	        globalProductImagesView.selectedImageIx = $thumb.data('mz-productimage-thumb');
	        if (globalProductImagesView.imageCache[globalProductImagesView.selectedImageIx]) {
	        	$('[data-mz-productimage-main]')
	                .prop('src', globalProductImagesView.imageCache[globalProductImagesView.selectedImageIx].src)
	                .prop('title', globalProductImagesView.imageCache[globalProductImagesView.selectedImageIx].title)
	                .prop('alt', globalProductImagesView.imageCache[globalProductImagesView.selectedImageIx].alt);
	        }
	        
		});
        /*click listener on video thumbnail*/
        $(document).on("click","#main-video-thumb",function(e){
            if($("#videoContainer").css("display") == "none"){
                $("#videoContainer").toggleClass("show-video");
                $("#imageContainer").hide();
                $(".mz-productimages-thumbimage").removeClass("is-selected");
                $(".ign-product-videothumb").addClass("is-selected");
            }
        });
        /*Play/Pause main video */
        var videoPlaying=false;
        $(document).on("click",'#product-main-video',function() {
            if (videoPlaying) {
                this.pause();videoPlaying = false;
                $(".main-play-video").removeClass("video-pause");
            }
            else {
                this.play();videoPlaying = true;
                $(".main-play-video").addClass("video-pause");
            }
        });
	};

    var LoadResources = {
        load: function(src, callback) {
          var script = document.createElement('script'),
              loaded;
          script.setAttribute('src', src);
          if (callback) {
            script.onreadystatechange = script.onload = function() {
              if (!loaded) {
                callback();
              }
              loaded = true;
            };
          }
          document.getElementsByTagName('head')[0].appendChild(script);
        }
    };
	/*This function finds stores near preferred store in 50km range by default*/
    function findNearStores(storeList, location, range) {
        var nearStores = [];
        _.each(storeList, function (store) {
            var distance = getDistanceFromLatLongInKm(store.properties.latitude, store.properties.longitude, location.lat, location.lng);
            if (distance <= range) {
                var storeData = {
                    distance: parseFloat(distance.toFixed(3)),
                    modelData: store
                };
                nearStores.push(storeData);
            }
        });
        nearStores = _.sortBy(nearStores, "distance");
        var resultArray = [];
        _.each(nearStores, function (store) {
            resultArray.push(store.modelData);
        });
        return resultArray;
    }
    /*This function calculates and return the distance between preferred store and input store*/
    function getDistanceFromLatLongInKm(lat1, lng1, lat2, lng2) {
        var earthRadius = 6371; // Radius of the earth in km
        var dLat = deg2rad(lat2 - lat1); // deg2rad below
        var dLng = deg2rad(lng2 - lng1);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var distance = earthRadius * c * 0.62137119; // Distance in Mi
        return distance;
    }
    /*this funtion converts deg to rad*/
    function deg2rad(deg) {
        return deg * (Math.PI / 180);
    }

    /*this function set working hours status */
    function setStoreStatus(store, day) {
        store.storeStatus = getStoreStatus(store, day);
    }

    /*this function calculates and returns status object with working hours*/
    function getStoreStatus(store, day) {
        var workingHours = "",
            statusData;
        var start = new Date(),
            end = new Date();
        switch (day) {

            case "Sunday":
                workingHours = store.properties.sundayHours;
                break;
            case "Monday":
                workingHours = store.properties.mondayHours;
                break;
            case "Tuesday":
                workingHours = store.properties.tuesdayHours;
                break;
            case "Wednesday":
                workingHours = store.properties.wednesdayHours;
                break;
            case "Thursday":
                workingHours = store.properties.thursdayHours;
                break;
            case "Friday":
                workingHours = store.properties.fridayHours;
                break;
            case "Saturday":
                workingHours = store.properties.saturdayHours;
                break;
        }
        var currentTime = moment(),
            startTime, endTime;
        if (workingHours !== "Closed") {
            var workHours = workingHours.split("-");
            startTime = moment(workHours[0], "HH:mm a");
            endTime = moment(workHours[1], "HH:mm a");
        }
        if (currentTime.isBetween(startTime, endTime)) {
            statusData = {
                "status": "open",
                "hours": workingHours
            };
        } else {
            statusData = {
                "status": "closed",
                "hours": workingHours
            };
        }
        return statusData;
	}
	
	$(document).ready(function() {
		$('.mz-productlisting .mz-productlisting-info .mz-productlisting-title').addClass('add-ellipsis');
        quickViewBind();
	});
	
	window.quickViewBind = quickViewBind;

});
