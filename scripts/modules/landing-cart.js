require(["modules/jquery-mozu", "underscore", "hyprlive", "modules/backbone-mozu", "hyprlivecontext", "modules/api", 'modules/models-cart'], function ($, _, Hypr, Backbone, HyprLiveContext,api,CartModels) {
    console.log("landing cart");
     
     var apiContext = require.mozuData('apicontext'),
     user = require.mozuData('user'),
     ua = navigator.userAgent.toLowerCase(),
     returnUrl = '/cart';
     console.log("apiContext",apiContext);
        var LandingPageView = Backbone.MozuView.extend({
            templateName: 'modules/common/landing-cart',
            render: function () {
                Backbone.MozuView.prototype.render.apply(this);
            },
            initialize: function(){
                
            }
        });
       
    $(document).ready(function () {
        console.log("In ready");
        var getUrl = window.location.search;
        var splitURL = getUrl.split('=');
        var refreshToken = splitURL.pop() || splitURL.pop();
        console.log("refreshToken",refreshToken);



            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            locale = locale.split('-')[0];
            var currentLocale = '';
            if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
                currentLocale = locale === 'fr' ? '/fr' : '/en';
            }






        api.request('PUT', '/api/commerce/customer/authtickets/refresh?refreshToken='+refreshToken, {
            
        }).then(function (response) {
            console.log("user",user);
           
            console.log("response",response);
           // window.location.href= returnUrl;
            var emailAddr = response.customerAccount.emailAddress;
            var userId = response.userId;
            console.log("emailAddr",emailAddr);
            console.log("userId",userId);
            var isHomeFurnitureSite = false;
            var accountId = response.customerAccount.id;
            var productCode  = $.urlParam('productCode');
            var userStore = isHomeFurnitureSite ? Hypr.getThemeSetting('hfPreferredStore') : Hypr.getThemeSetting('preferredStore');   
            console.log("userStore",userStore);
            
           /*  api.request('GET', '/api/commerce/customer/accounts/'+accountId+'/attributes/'+userStore).then(function(data){
                console.log("data",data);
            }); */

            api.request('GET', '/api/commerce/customer/accounts/'+accountId+'/attributes/').then(function(data){
                var customerStore = data.values[0]; 
                console.log("step1");
                if($.cookie("preferredStore")){
                    console.log("step2");
                    var currentStore = $.parseJSON($.cookie("preferredStore"));
                    if(customerStore != 'N/A' && customerStore != currentStore.code){
                        console.log("step4");
                        var cartModelData = new CartModels.Cart();
                        var cartCount = null;
                        cartModelData.on('sync', function () {
                            cartCount = cartModelData.count();
                        });
                        var itemsInCart = [];
                        cartModelData.apiGet().then(function (response) {
                            if (cartCount > 0) {
                                itemsInCart = response.data.items;
                            }
                            $.ajax({
                                url: "/set-purchase-location", 
                                data :{ "purchaseLocation": customerStore}, 
                                type:"GET",
                                success:function(response){
                                    api.get("location",{code:customerStore}).then(function(response){
                                        var selectedStore = response.data;  
                                        var expiryDate = new Date();
                                        expiryDate.setYear(expiryDate.getFullYear() + 1);

                                        if (ua.indexOf('chrome') > -1) {
                                            if (/edg|edge/i.test(ua)){
                                                $.cookie("preferredStore",JSON.stringify(selectedStore),{path:'/',expires: expiryDate});
                                            }else{
                                                document.cookie = "preferredStore="+JSON.stringify(selectedStore)+";expires="+expiryDate+";path=/;Secure;SameSite=None";
                                            }
                                        } else {
                                            $.cookie("preferredStore",JSON.stringify(selectedStore),{path:'/',expires: expiryDate});
                                        }
                                        //$.cookie("sessionStore",JSON.stringify(selectedStore.code),{path:'/'});
                                        if(itemsInCart.length > 0){
                                            var previousItemQuantity = [];
                                            _.each(itemsInCart, function(item){
                                                var prodObj = {
                                                    'quantity' : item.quantity,
                                                    'productCode' : item.product.productCode,
                                                    'price': item.product.price.price,
                                                    'salePrice': item.product.price.salePrice
                                                };
                                                previousItemQuantity.push(prodObj);
                                            });
                                            if (ua.indexOf('chrome') > -1) {
                                                if (/edg|edge/i.test(ua)){
                                                    $.cookie("previousItems",JSON.stringify(previousItemQuantity),{path:'/'});
                                                }else{
                                                    document.cookie = "previousItems="+JSON.stringify(previousItemQuantity)+";path=/;Secure;SameSite=None";
                                                }
                                            } else {
                                                $.cookie("previousItems",JSON.stringify(previousItemQuantity),{path:'/'});
                                            }
                                        }
                                        if ( returnUrl ){
                                            window.location.href= returnUrl;
                                        }else if (productCode){                                        	
                                            window.location.href = currentLocale+"/p/" + productCode + "#addToWishlist";
                                        }else {
                                            window.location.href = "/myaccount";
                                        }
                                    });
                                },
                                error: function (error) {
                                    console.log(error);
                                }
                            });
                        });
                    }else{
                        console.log("step5");
                        if ( returnUrl ){
                            window.location.href= returnUrl;
                        }else if (productCode){                            	
                            window.location.href = currentLocale+"/p/" + productCode + "#addToWishlist";                            	
                        }else{
                            window.location.href = "/myaccount";
                        }
                    }
                }else{
                    console.log("step3");
                    if(customerStore != 'N/A'){
                        console.log("step6");
                        $.ajax({
                            url: "/set-purchase-location", 
                            data :{ "purchaseLocation": customerStore}, 
                            type:"GET",
                            success:function(response){
                                api.get("location",{code:customerStore}).then(function(response){
                                    var selectedStore = response.data;  
                                    var expiryDate = new Date();
                                    expiryDate.setYear(expiryDate.getFullYear() + 1);

                                    if (ua.indexOf('chrome') > -1) {
                                        if (/edg|edge/i.test(ua)){
                                            $.cookie("preferredStore", JSON.stringify(selectedStore), {
                                                path: '/',
                                                expires: expiryDate
                                            });
                                        }else{
                                            document.cookie = "preferredStore="+JSON.stringify(selectedStore)+";expires="+expiryDate+";path=/;Secure;SameSite=None";
                                        }
                                    } else {
                                        $.cookie("preferredStore", JSON.stringify(selectedStore), {
                                            path: '/',
                                            expires: expiryDate
                                        });
                                    }
                                    //$.cookie("sessionStore",JSON.stringify(selectedStore.code),{path:'/'});
                                    if ( returnUrl ){
                                        window.location.href= returnUrl;
                                    }else if (productCode){                                        	
                                        window.location.href = currentLocale+"/p/" + productCode + "#addToWishlist";
                                    }else {
                                        window.location.href = "/myaccount";
                                    }
                                });
                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });
                    }else{
                        console.log("step7");
                        if ( returnUrl ){
                            window.location.href= returnUrl;
                        }else if (productCode){                            	
                            window.location.href = currentLocale+"/p/" + productCode + "#addToWishlist";                            	
                        }else{
                            window.location.href = "/myaccount";
                        }
                    }
                }
            });


        }); 
       
    });  
});