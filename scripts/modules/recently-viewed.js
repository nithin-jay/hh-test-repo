require([
	"modules/jquery-mozu", 
	"underscore", 
	"hyprlive", 
	"modules/backbone-mozu", 
	"hyprlivecontext",
	'modules/api',
	'modules/models-customer'], 
	function ($, _, Hypr, Backbone, HyprLiveContext, api, CustomerModels) {
	var apiContext = require.mozuData('apicontext'),
	ua = navigator.userAgent.toLowerCase(); 
	var RecentlyViewedView = Backbone.MozuView.extend({
		templateName: 'modules/recently_viewed/recently_viewed',
        initialize: function () {
        	var me = this;
        	var user = require.mozuData('user');
            var alreadyViewedProducts;
            var recentlyViewedItems;
            if(!(user.isAnonymous) && user.isAuthenticated){
            	var customer = new CustomerModels.EditableCustomer(); 
         		customer.set("id",user.accountId);
         		var recentlyViewedItemsFromCookie = $.cookie('recentlyViewd');
        		customer.fetch().then(function(response){
        			alreadyViewedProducts = _.findWhere(response.get('attributes').toJSON(), {fullyQualifiedName:Hypr.getThemeSetting('recentlyViewed')});
                    if(alreadyViewedProducts && alreadyViewedProducts.values[0] != 'N/A') {
                    	recentlyViewedItems = alreadyViewedProducts.values[0];
                    	if (recentlyViewedItemsFromCookie){ 
                    		recentlyViewedItems = recentlyViewedItems + ',' + recentlyViewedItemsFromCookie;
                    		recentlyViewedItems = recentlyViewedItems.split(',');
                    		if(recentlyViewedItems.length > 6){
                    			recentlyViewedItems = recentlyViewedItems.slice(Math.max(recentlyViewedItems.length - 6, 1)); 
                        	} 
                        	recentlyViewedItems = _.unique(recentlyViewedItems);
                        	recentlyViewedItems = recentlyViewedItems.join(',');
                        	var currentlyViewed = $.cookie('recentlyViewd');

                        	if (ua.indexOf('chrome') > -1) {
								if (/edg|edge/i.test(ua)){
									$.cookie('recentlyViewd', currentlyViewed, {path: '/' });
								}else{
									document.cookie = "recentlyViewd="+currentlyViewed+";path=/;Secure;SameSite=None";
								}
							} else {
								$.cookie('recentlyViewd', currentlyViewed, {path: '/' });
							}
                    	}
                    }else {
                    	recentlyViewedItems = recentlyViewedItemsFromCookie;
                    }
                	me.addProductInRecentlyViewed(recentlyViewedItems);
                	if(recentlyViewedItems && alreadyViewedProducts) {
                		response.updateAttribute(alreadyViewedProducts.fullyQualifiedName,alreadyViewedProducts.attributeDefinitionId,[recentlyViewedItems]);
                	}
        		});
            }else {
            	recentlyViewedItems = $.cookie('recentlyViewd');
            	me.addProductInRecentlyViewed(recentlyViewedItems);
            }
            var contextSiteId = apiContext.headers["x-vol-site"];
            var homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId");
            var frSiteId = Hypr.getThemeSetting("frSiteId");
            if(homeFurnitureSiteId === contextSiteId){
                me.model.set("isHomeFurnitureSite", true);
            }
            if(frSiteId === contextSiteId){
                me.model.set("isFrenchSite", true);   
            }
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
			var currentPurchaseLocation = require.mozuData('pagecontext').purchaseLocation;
        	locale = locale.split('-')[0];
        	me.model.set("currentLocale", locale);
			me.model.set("currentSite", currentSite);
			if(currentPurchaseLocation){
				me.model.set("currentPurchaseLocation", currentPurchaseLocation.code);
			}	
		},
        addProductInRecentlyViewed: function (recentlyViewedItems) {
        	if(recentlyViewedItems) {
            	recentlyViewedItems = recentlyViewedItems.split(',');
            	var filter = "";
            	_.each(recentlyViewedItems, function(item) {
                    if (filter !== "") filter += " or ";
                    filter += "productCode eq "+ item;    
                });
            	var response = api.get('products',filter);
            	var me = this;
            	response.then(function(products) {
            		me.model.set('recentlyViewedItems', products.data.items);
            		me.render();
            	});
        	}
        },
        clearItems: function() {
        	var me = this;
        	$.removeCookie('recentlyViewd', { path: '/' });
        	var user = require.mozuData('user');
        	if(!(user.isAnonymous) && user.isAuthenticated) {
        		var customer = new CustomerModels.EditableCustomer(); 
         		customer.set("id",user.accountId);
         		var recentlyViewedItemsFromCookie = $.cookie('recentlyViewd');
        		customer.fetch().then(function(response){
        			var alreadyViewedProducts = _.findWhere(response.get('attributes').toJSON(), {fullyQualifiedName:Hypr.getThemeSetting('recentlyViewed')});
        			response.updateAttribute(alreadyViewedProducts.fullyQualifiedName,alreadyViewedProducts.attributeDefinitionId,['N/A']);
        		});
        	}
        	me.model.clear('recentlyViewedItems');
        	me.model.set('recentlyViewedCleared', true);
        	me.render();
        }
    });
	var recentlyViewed = function() {
		var recentlyViewedView = new RecentlyViewedView({
			el: $('#recently-viewed-section'),
			model: new Backbone.Model()
		});
		recentlyViewedView.render();
	};
	$(document).ready(function () {
		recentlyViewed();
	});
	window.recentlyViewed = recentlyViewed;
});