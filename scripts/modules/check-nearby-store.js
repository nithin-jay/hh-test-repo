define([
    "modules/jquery-mozu",
    "underscore",
    "hyprlive",
    "modules/backbone-mozu",
    'modules/models-cart',
    'modules/models-customer',
    'modules/api',
    'vendor/jquery/moment',
    "https://api.tiles.mapbox.com/mapbox-gl-js/v0.44.2/mapbox-gl.js",
    'modules/cookie-utils'
],
function ($, _, Hypr, Backbone, CartModels, CustomerModels, api,moment, mapboxgl, CookieUtils) {

    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var today = new Date();
    var map, markers = [],filtersString,isHomeFurnitureSite=false,
    cityChanges = false,
    prevCity = "",
    storeCitiesFQN = Hypr.getThemeSetting("storeCitiesListFQN"),
    checkedFilters = [],
    search = "",
    searchBasedLatLong,
    locationList = Hypr.getThemeSetting("storeDocumentListFQN"),
    statusFilter = false,
    distanceFilters = Hypr.getThemeSetting("distanceFilters"),
    returnParam = "",
    storeWithStock = false,
    storeToChange = null,
    user = require.mozuData('user'),
    itemsInCart = [],
    ua = navigator.userAgent.toLowerCase();

    if (Hypr.getThemeSetting("homeFurnitureSiteId") === require.mozuData('apicontext').headers["x-vol-site"]) {
        filtersString = "properties.storeType in['Home Furniture']";
        isHomeFurnitureSite = true;
    } else {
        if (window.location.search.indexOf('storeFilter') < 0)
            filtersString = "properties.storeType in['Home Hardware','Home Building Centre', 'Home Hardware Building Centre'] ";
    }
    
     /* Adding display online filter */
     if (!filtersString) {
        filtersString = "properties.displayOnline eq true ";
    } else {
        filtersString += "and properties.displayOnline eq true ";
    }
    
    var CheckNearbyStoreModal = Backbone.MozuView.extend({
        templateName: 'modules/product/check-nearby-store-modal',
        additionalEvents: {
        "keyup input[id='city']": "populateCities",
        "keypress input[id='postalCode']": "findStores",
        "keypress input[id='city']": "findStores",
        "focus input[id='postalCode']": "onSearchFocusBlur",
        "blur input[id='postalCode']": "onSearchFocusBlur",
        "focus input[id='city']": "onSearchFocusBlur",
        "click [id='applyFilters']": "applyFilters",
        "click input[id='storeWithAvailableStock']":"storeWithAvailableStock"
        },
        render: function(){
            var self = this;
            var stores = self.model.get("stores");
            if (localStorage.getItem('preferredStore')) {
                var preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
                var currentStore = _.findWhere(stores, {
                    name: preferredStore.code
                });
                if (currentStore) {
                    currentStore.isPreferresStore = true;
                    self.model.set("isPreferresStore", true);
                }
            }
            if (search) {
                var searchObject = {};
                if (search.indexOf(",") > -1) {
                    var searchString = search.split(",");
                    searchObject = {
                        "city": searchString[0].trim(),
                        "province": searchString[1].trim()
                    };
                    self.model.set("search", searchObject);
                } else {
                    searchObject = {
                        "postalCode": search.trim()
                    };
                    self.model.set("search", searchObject);
                }
            }

            self.setFilters();
            if(stores && stores.length > 0 && !self.model.get("search")){
                if(stores.length == 1 && self.model.get("isPreferresStore")){
                    self.model.set("nearByStores",stores.length - 1 + Hypr.getLabel("nearByStore"));
                }else{
                    if(self.model.get("isPreferresStore")){
                        if(stores.length == 2){
                            self.model.set("nearByStores",stores.length - 1 +" "+ Hypr.getLabel("nearByStore"));
                        }else{
                            self.model.set("nearByStores",stores.length - 1 +" "+ Hypr.getLabel("nearByStores"));
                        }
                    }else{
                        if(stores.length == 1){
                            self.model.set("nearByStores",stores.length +" "+ Hypr.getLabel("nearByStore"));
                        }else{
                            self.model.set("nearByStores",stores.length +" "+ Hypr.getLabel("nearByStores"));
                        }
                    }
                }
            }else{
                if(self.model.get("search")){
                    if(stores && stores.length == 1){
                        self.model.set("searchNearByStores",stores.length +" "+ Hypr.getLabel("storeNear"));
                    }else{
                        if(stores)
                            self.model.set("searchNearByStores",stores.length +" "+ Hypr.getLabel("storesNear"));
                    }
                }else{
                    self.model.set("noNearByStoresFound",Hypr.getLabel("noStoreFoundOnFilters"));
                }
            }    

            Backbone.MozuView.prototype.render.apply(this, arguments);
            if(storeWithStock){
                $("#storeWithAvailableStock").prop("checked", true);
            }
            else{
                $("#storeWithAvailableStock").prop("checked", false);
            }
            self.loadMap();
        },
        initialize:function(){
            this.trigger('remove-view');
            var _this = this;
            Backbone.View.prototype.on('remove-view',function(){
                //Backbone.View.prototype.remove;
                Backbone.View.prototype.off();
                _this.undelegateEvents();
            });
            var productCode = $.parseJSON($.cookie("quickViewProduct")).productCode,
            isItemEcomEnabled = $.parseJSON($.cookie("quickViewProduct")).isEcomEnabled,
            locationCodes="",locationInventory,nearStoresList;
            _.each(_this.model.get("stores"),function(item,index){
                if(index != _this.model.get("stores").length-1){
                    locationCodes += item.name+",";
                    }else{
                    locationCodes += item.name;
                }
            });
            api.request('GET','/api/commerce/catalog/storefront/products/'+ productCode +'/locationinventory?pageSize=1100&locationCodes='+'1,2,4').then(function(warehouseResponse){
                api.request('GET','/api/commerce/catalog/storefront/products/'+ productCode +'/locationinventory?pageSize=1100&locationCodes='+locationCodes).then(function(storesResponse){
                    locationInventory = storesResponse.items;
                    if(warehouseResponse && warehouseResponse.items.length > 0 && locationInventory && locationInventory.length > 0){
                        nearStoresList = _this.setStoreInventoryInfo(_this.model.get("stores"), warehouseResponse, locationInventory, isItemEcomEnabled);
                        _this.renderStoresView(nearStoresList, warehouseResponse, locationInventory);
                    }
                });
            });
        },
        /*This function will create map and plot markers for respective stores */
        loadMap: function(){
            var self = this;
            var storeList = self.model.get("stores");
            var index;
            
            unwired.key = mapboxgl.accessToken = Hypr.getThemeSetting('mapApiKey');
            //Define the map and configure the map's theme
            map = new mapboxgl.Map({
                container: document.getElementById('store-map'),
                attributionControl: false, //need this to show a compact attribution icon (i) instead of the whole text
                style: unwired.getLayer("streets"),//get Unwired's style template.
                center:[-113.323975,53.631611],
                zoom: 3
            });

            //Add Navigation controls to the map to the top-right corner of the map
            var nav = new mapboxgl.NavigationControl();
            map.addControl(nav, 'top-right');

            //Add a Scale to the map
            map.addControl(new mapboxgl.ScaleControl({
                maxWidth: 80,
                unit: 'metric' //imperial for miles
            }));

            //plot markers on map
            if (storeList && storeList.length > 0) {
                _.each(storeList, function (store, index) {
                    //var location, marker, icon;
                    if (store.properties.displayOnline) {
                        
                        var popup = new mapboxgl.Popup().setHTML(self.infoWindowContent(store, (index === 0 && store.isPreferresStore)));
                        var currentIcon = (index === 0 && store.isPreferresStore) ? 'url(/../resources/images/marker-selection.png)' : 'url(' + '/../resources/images/marker-' + (isHomeFurnitureSite ? 'hf' : 'hh') + '.png'+')';
                        var el = document.createElement('div');
                            el.style.backgroundImage = currentIcon;
                            el.style.width = isHomeFurnitureSite ? '27px' : '33px'; 
                            el.style.height = isHomeFurnitureSite ? '28px' : '40px';
                            el.style.backgroundSize = 'cover';
                            
                            // create the marker
                            var marker = new mapboxgl.Marker(el).setLngLat([parseFloat(store.properties.longitude), parseFloat(store.properties.latitude)]).setPopup(popup).addTo(map);
                            self.addMarkerEvent(el, store);
                            markers.push(marker);
                    }
                });
                self.zoomExtends();
            }else{
                map.setCenter([-113.323975, 53.631611]);
            }
            //plot current position on map
            if ($.cookie("currentPosition")) {
                self.currentPosition();
            }
           
        },
        /*This function will reset map to default view i.e will remove markers*/
        resetMap: function () {
            this.clearMarkers();
        },
        clearMarkers: function () {
            _.each(markers, function (marker) {
                marker.remove();
            });
        },
        addMarkerEvent: function(marker, data){
            marker.addEventListener("click", function () {
                var screenWidth = window.matchMedia("(max-width: 359px)");
                if(!screenWidth.matches){
                    var screenWidth1 = window.matchMedia("(max-width: 767px)");
                    if(screenWidth1.matches) {
                        map.setZoom(Hypr.getThemeSetting('mapZoomLevel'));
                        map.panTo([parseFloat(data.properties.longitude), parseFloat(data.properties.latitude) + (0.6 * 0.33 * (map.getBounds().getNorth() - map.getBounds().getSouth()))]);
                    }
                }else{
                    map.setZoom(Hypr.getThemeSetting('mapZoomLevel'));
                    map.panTo([parseFloat(data.properties.longitude), parseFloat(data.properties.latitude) + (0.5 * 0.5 * (map.getBounds().getNorth() - map.getBounds().getSouth()))]);
                }
                $('.nearby-store-results').animate({
                    scrollTop: $("#" + data.name).parent().scrollTop() + $("#" + data.name).offset().top - $("#" + data.name).parent().offset().top
                }, 1000);
                $(".nearby-store-locator-result").removeClass("is-highlighted");
                $("#" + data.name).addClass("is-highlighted");
            });
        },
        currentPosition: function () {
            var self = this;
            var currentMarker, shopperPos;
            // check current position set in cookie or not if -> yes plot on map
            shopperPos = $.parseJSON($.cookie("currentPosition"));
            var el = document.createElement('div');
                el.style.backgroundImage = 'url(/../resources/images/marker.png)';
                el.style.width = '35px'; 
                el.style.height = '44px';
                el.style.backgroundSize = 'cover';
            currentMarker = new mapboxgl.Marker(el).setLngLat([parseFloat(shopperPos.lng), parseFloat(shopperPos.lat)]).addTo(map);
            markers.push(currentMarker);
        },
        /*this function will extends to make map visible in given window */
        zoomExtends: function () {
            var bounds = new mapboxgl.LngLatBounds();
            var screenWidth = window.matchMedia("(max-width: 767px)");
            if (markers.length > 0) {
                _.each(markers, function (marker) {
                    bounds.extend(marker.getLngLat());
                });
                if (screenWidth.matches) {
                    map.fitBounds(bounds,{padding: {top: 25, bottom:25, left: 50, right: 50}});
                }else{
                    map.fitBounds(bounds,{padding: {top: 25, bottom:25, left: 100, right: 100}});
                }
            }
            // fit bounds as screen size changes
            map.on('resize', function () {
                map.fitBounds(bounds);  
            });
        },
        /*This function will create store info window on map */
        infoWindowContent: function (data, selection) {
            var storeLink = "/store/" + data.name;
            var status, address1 = "",
                address2 = "",
                phone = "";
            if (data.storeStatus.status === "open") {
                status = true;
            }
            if (data.properties.address1 !== "NA" && data.properties.address1 !== "N/A") {
                address1 = data.properties.address1;
            }
            if (data.properties.address2 !== "NA" && data.properties.address2 !== "N/A") {
                address2 = data.properties.address2;
            }
            if(data.properties.phone){
                var phoneNumList = data.properties.phone.split("#");
                phone = phoneNumList[0];
            }
            var content = '<div class="store-info-window">' +
                '<div class="info-container">' +
                '<a href=' + storeLink + ' ' + 'class="link"><span class="material-icons">keyboard_arrow_right</span></a>' +
                '<div class="info-content">' +
                '<h4 class="store-title">' + data.properties.storeName + '</h4>' +
                '<p class="store-address-details">' +
                (
                    address1 ? '<span class="addr-street">' + address1 + '</span>' :
                    ""
                ) +
                (
                    address2 ? '<span class="addr-street">' + address2 + '</span>' :
                    ""
                ) +
                '<span class="addr-loc">' + data.properties.city + '</span>' +
                '<span class="addr-tele">' + phone + '</span>' +
                '</p>' +
                '<p class="store-status">' +
                (
                    status ? '<span><i class="material-icons">done</i>' + Hypr.getLabel("openNow") + '</span>' :
                    '<span>' + Hypr.getLabel("closedNow") + '</span>'
                ) +
                (
                    status ? data.storeStatus.hours : ""
                ) +
                '</p>' +
                '</div>' +
                '</div>' +
                (
                    selection ? '<div class="home-store"><span class="material-icons">place</span>' + Hypr.getLabel('myStore') + '</div>' :
                    '<button class="make-store" data-mz-store=' + data.name + ' data-mz-action="makeMyStore">' + Hypr.getLabel('makeMyStore') + '</button>'
                ) +
                '</div>';
            return content;
        },
        getCityList: function(filterString, city_list, cityInput) {
            if(!cityInput) {
                api.get('entityList', { listName: storeCitiesFQN , pageSize:5 , filter:filterString, sortBy: 'city asc' }).then(function(response) {
                    if(response.data.items.length > 0) {
                        $("#city_list").show();
                        $("#city_list").empty();
                        _.each(response.data.items, function (item) {
                            var li = document.createElement('li');
                            var a = document.createElement('a');
                            var text = document.createTextNode(item.city +", " + item.province);
                            a.href = '#';a.appendChild(text);
                            li.appendChild(a);
                            city_list.appendChild(li);
                            a.onclick = onCitySelect;    
                            if (response.data.items.length == 1) {
                                $("#city_list li").addClass("selected");
                            } else {
                                $("#city_list li:first").addClass("selected");
                            }
                        });
                    }else{
                        $("#city_list").empty();
                        $("#city_list").hide();
                    }
                });
            }
            else {
                api.get('entityList', { listName: storeCitiesFQN , pageSize:5 , filter:filterString }).then(function(response) {
                    prevCity = cityInput.value;
                    if(response.data.items.length > 0) {
                        $("#city_list").show();
                        $("#city_list").empty();
                        _.each(response.data.items, function (item) {
                            var li = document.createElement('li');
                            var a = document.createElement('a');
                            var text = document.createTextNode(item.city +", " + item.province);
                            a.href = '#';a.appendChild(text);
                            li.appendChild(a);
                            city_list.appendChild(li);
                            a.onclick = onCitySelect;    
                            if (response.data.items.length == 1) {
                                $("#city_list li").addClass("selected");
                            } else {
                                $("#city_list li:first").addClass("selected");
                            }
                        });
                    }else{
                        $("#city_list").empty();
                        $("#city_list").hide();
                    }
                });
            }
        },
        /*This function will fetch entity list based on search value and populate resulted cities */
        populateCities: function(event){
            
            var selected, filterString="", cityValue="";
            var cityInput = event.target, min_characters = 2;
            var city_list = document.getElementById('city_list');
            
            if (event.keyCode == 13) { // enter
                if ($("#city_list").is(":visible")) {
                    onCitySelect(event);
                }
            }
            if (cityInput.value.length <= min_characters ) {
                $("#city_list").empty();
                $("#city_list").hide();
                prevCity = cityInput.value;
                return;
            } else { 
                if(cityInput.value.indexOf(",") == -1 && (event.keyCode !== 38 && event.keyCode !== 40)){  
                    cityValue = cityInput.value.replace("'","^'");
                    if(prevCity != cityInput.value){
                        filterString = "city sw '"+cityValue.toLowerCase() +"' or"+ " city eq '"+ cityValue.toLowerCase()+"'";
                        prevCity = cityInput.value; 
                        _.debounce(this.getCityList(filterString, city_list), 400);
                    }
                }else{
                    if(cityChanges && cityInput.value.indexOf(",") != -1 && (event.keyCode !== 38 && event.keyCode !== 40)){
                        var splitCity = cityInput.value.split(",");
                        cityValue = splitCity[0].replace("'","^'");
                        filterString = "city sw '"+cityValue.toLowerCase() +"' or"+ " city eq '"+ cityValue.toLowerCase()+"'";
                        _.debounce(this.getCityList(filterString, city_list, cityInput), 400);
                    }
                }
            }
            if (event.keyCode == 38) { // up
                selected = $(".selected");
                $("#city_list li").removeClass("selected");
                if (selected.prev().length === 0) {
                    selected.siblings().last().addClass("selected");
                    cityInput.value = $(".selected a").text();
                    cityChanges = true;
                } else {
                    selected.prev().addClass("selected");
                    cityInput.value = $(".selected a").text();
                    cityChanges = true;
                }
            }
            if (event.keyCode == 40) { // down
                if($("#city_list li").hasClass("selected")){
                    selected = $(".selected");
                    $("#city_list li").removeClass("selected");
                    if (selected.next().length === 0) {
                        selected.siblings().first().addClass("selected");
                        cityInput.value = $(".selected a").text();
                        cityChanges = true;
                    } else {
                        selected.next().addClass("selected");
                        cityInput.value = $(".selected a").text();
                        cityChanges = true;
                    }
                } else {
                    $("#city_list li:first").addClass("selected").focus();
                    cityInput.value = $(".selected a").text();
                    cityChanges = true;
                }
            }
        },
        /*This function will check cart data first if cart conatins item then show's modal for confirmation of store change if 
        store is ecom enabled. If store is not ecom enabled then non ecom store confirmation modal is shown. If not items in 
        cart then will continie with selected store */
        makeMyStore: function(e){
            e.stopPropagation();
            var me = this;
            var currentTarget = e.currentTarget;
            storeToChange = $(currentTarget).data("mzStore");
            var cartModelData = new CartModels.Cart();
            var cartCount = null;
             
            cartModelData.on('sync', function () {
                cartCount = cartModelData.count();
            });
            cartModelData.apiGet().then(function (response) {
                if (cartCount > 0) {
                    itemsInCart = response.data.items;
                    api.get("location", {
                        code: storeToChange
                    }).then(function (response) {
                        var selectedStore = response.data;
                        var isEcomStore = _.findWhere(selectedStore.attributes, {
                            "fullyQualifiedName": Hypr.getThemeSetting("isEcomStore")
                        });
                        if (isEcomStore && isEcomStore.values[0] === 'Y') {
                            $('#store-change-confirmation-modal').modal().show();
                        } else {
                            $('#non-ecom-store-confirmation-modal').modal().show();
                        }
                    });
                } else {
                    continueWithNewStore();
                }
            });
        },
        /*This function will make call to locationIQ geoCoder to get lat long value of searched value and then 
          pass that lat long values to  getSearchStore function to fetch document list  
        */
        findStores: function(e){
            var me = this;
            var postalCode = "",
                city = "",
                searchValue = null,
                searchConfigure = null;
            // validate form based on user input for postal code or city and province 
            
            if ($('#postalCode').val()) {
                postalCode = $('#postalCode').val().trim();
            }

            if ($("#city_list").is(":visible")){
                if ($("#city_list li").hasClass("selected")){
                    cityChanges = true;
                }
            }
            
            if ($("#city").val()) {
					var CityHiddenValue=$("#hdnFlagClick").val();
                    if($("#hdnFlagClick").val() === ""){
                        CityHiddenValue = $(".selected").text().trim();
                    }
					if(CityHiddenValue !== ""){
						city = CityHiddenValue;
					}else{
					   city = $(".selected").text().trim();
                       if(CityHiddenValue === ""){
                           city = $("#city").val().trim();
                       }
					}
				}
          
            if (e.which === 13 || e.which === 1) {
                $("#check-nearby-loading").show();
                if (postalCode) {
                    if (me.validatePostalCode(postalCode.trim())) {
                        if (city === "") {
                            searchValue = postalCode.trim();
                            checkedFilters.splice(0, checkedFilters.length);
                            me.model.unset("searchErrorMsg");
                            
                            var whiteSpaceSearch = "", whiteSpaceFlag = false;
                            if (!(/\s/.test(searchValue))) {
                                whiteSpaceSearch = searchValue.replace(/^(.{3})(.*)$/, "$1 $2");
                                whiteSpaceFlag = true;
                            }
                            whiteSpaceSearch = whiteSpaceFlag ? whiteSpaceSearch : searchValue;
                            searchConfigure = {
                                   "async": true,
                                   "crossDomain": true,
                                   "url": "https://us1.locationiq.com/v1/search.php?key="+Hypr.getThemeSetting('mapApiKey')+"&postalcode="+whiteSpaceSearch+"&format=json&countrycodes=ca&addressdetails=1",
                                   "method": "GET"
                           };
                            $.ajax(searchConfigure).done(function (response) {
                            if(response && response.length > 0){
                                   searchBasedLatLong = {
                                           "lat": response[0].lat,
                                           "lng": response[0].lon
                                   };
                                    me.getSearchStores(searchValue, searchBasedLatLong);
                                    me.blurSearchField();
                                }else {
                                    search = searchValue;
                                    me.reRenderView();
                                }
                            }).error(function(error){
                                search = postalCode.trim();
                                me.reRenderView(null);
                            });
                        }
                    } else {
                        me.model.set("searchErrorMsg", Hypr.getLabel("notValidPostalCode"));
                        setTimeout(function () {
                            $("#check-nearby-loading").hide();
                            search = postalCode.trim();
                            me.model.unset("stores", []);
                            me.render();
                            me.blurSearchField();
                        }, 500);
                    }
                } else if (city !== "") {
                    if(cityChanges && city.indexOf(",") != -1){
                        var searchByCity = city.split(",");
                        if(searchByCity[1].trim().length == 2){
                            if (postalCode === "") {
                                searchValue = city.trim();
                                me.model.unset("searchErrorMsg");
                                checkedFilters.splice(0, checkedFilters.length);
                                
                                searchConfigure = {
                                    "async": true,
                                    "crossDomain": true,
                                    "url": "https://us1.locationiq.com/v1/search.php?key="+Hypr.getThemeSetting('mapApiKey')+"&addressdetails=1&q="+searchValue+"&countrycodes=ca&source=v3&layers=coarse&format=json",
                                    "method": "GET"
                                };

                                $.ajax(searchConfigure).done(function (response) {
                                    if(response && response.length > 0){
                                        searchBasedLatLong = {
                                            "lat": response[0].lat,
                                            "lng": response[0].lon
                                        };
                                        me.getSearchStores(searchValue, searchBasedLatLong);
                                        me.blurSearchField();
                                    }else {
                                        search = searchValue;
                                        me.reRenderView();
                                    }
                                }).error(function(error){
                                    search = searchValue;
                                    me.reRenderView(null);
                                });
                            }
                        }else{
                            me.model.set("searchErrorMsg", Hypr.getLabel("enterProperCityName"));
                            cityChanges = false;
                            setTimeout(function () {
                                $("#check-nearby-loading").hide();
                                search = city.trim() + ",";
                                me.model.unset("stores", []);
                                me.render();
                                me.blurSearchField();
                            }, 500); 
                        }
                    } else if (cityChanges && $("#city_list").is(":visible") && city.indexOf(",") == -1){
                            city = $("#city_list li.selected").text();
                            if (postalCode === "") {
                                searchValue = city.trim();
                                me.model.unset("searchErrorMsg");
                                checkedFilters.splice(0, checkedFilters.length);
                                searchConfigure = {
                                    "async": true,
                                    "crossDomain": true,
                                    /* "url": "https://us1.locationiq.com/v1/search.php?key="+Hypr.getThemeSetting('mapApiKey')+"&addressdetails=1&q="+searchValue+"&countrycodes=ca&source=v3&layers=coarse&format=json", */
                                    "url":"https://staging.locationiq.com/v1/search.php?key=268b72520dbf98&format=json&q="+searchValue+"&addressdetails=1&countrycodes=ca&matchquality=1&layers=coarse",
                                    "method": "GET"
                                };
                                $.ajax(searchConfigure).done(function (response) {
                                    if(response && response.length > 0){
                                        var matchedLevel = false;
                                        var boundingBoxValues = [];
                                        _.each(response,function(res){
                                            if((res.matchquality.matchlevel === 'neighbourhood' || res.matchquality.matchlevel === 'city' || res.matchquality.matchlevel === 'island') && !matchedLevel){
                                                searchBasedLatLong = {
                                                    "lat": res.lat,
                                                    "lng": res.lon
                                                };
                                                matchedLevel = true;
                                                boundingBoxValues = res.boundingbox;
                                            }
                                        });
                                        if(matchedLevel){
                                            searchBasedLatLong = searchBasedLatLong;
                                        }else{
                                            searchBasedLatLong = {
                                                "lat": response[0].lat,
                                                "lng": response[0].lon
                                            };
                                            boundingBoxValues = response[0].boundingbox;
                                        }
                                        me.getSearchStores(searchValue, searchBasedLatLong,boundingBoxValues);
                                        me.blurSearchField();
                                    }else {
                                        search = searchValue;
                                        me.reRenderView();
                                    }
                                }).error(function(error){
                                    search = searchValue;
                                    me.reRenderView(null);
                                });
                            }
                        } else{
                        me.model.set("searchErrorMsg", Hypr.getLabel("enterProperCityName"));
                        setTimeout(function () {
                            $("#check-nearby-loading").hide();
                            search = city.trim() + ",";
                            me.model.unset("stores", []);
                            me.render();
                            me.blurSearchField();
                        }, 500);
                    }
                } else {
                    me.model.set("searchErrorMsg", Hypr.getLabel("enterValidSearchValue"));
                    search = "";
                    me.model.set("search", "empty");
                    setTimeout(function () {
                        $("#check-nearby-loading").hide();
                        me.model.unset("stores", []);
                        me.render();
                    }, 500);
                }
            }
        },
        /*This function will make request to fetch document list and pass that result to findNearStores 
        with lat/long value and range
        */
        getSearchStores: function (searchValue, searchBasedLatLong) {
            var self = this,
                queryString = "";
            if (isHomeFurnitureSite) {
                queryString = "properties.displayOnline eq true and properties.storeType in['Home Furniture']";
            } else {
               queryString = "properties.displayOnline eq true and properties.storeType in['Home Hardware','Home Building Centre','Home Hardware Building Centre']"; 
            }
            api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + queryString + '&pageSize=1100', {
                cache: false
            }).then(function (response) {
                search = searchValue; 
                var nearStoresList = findNearStores(response.items, searchBasedLatLong, Hypr.getThemeSetting("storeDistanceInkm"));
                self.reRenderView(nearStoresList);
            });
        },
        /*This function accept store list and search value and render's the view */
        reRenderView: function (storesList, searchValue) {
            var me = this;
            var nearStoresList = [],
                preferredStore, currentStore,
                storeData = { "stores" : null },
                locationCodes = "",
                locationInventory = null;
            if (storesList && storesList.length > 0) {
                me.resetMap();
                if (localStorage.getItem('preferredStore')) {
                    currentStore = $.parseJSON(localStorage.getItem('preferredStore'));
                    preferredStore = _.findWhere(storesList, {
                        name: currentStore.code
                    });
                    nearStoresList = _.without(storesList, _.findWhere(storesList, {
                        name: currentStore.code
                    }));

                    if (preferredStore)
                        nearStoresList.unshift(preferredStore);

                    nearStoresList = nearStoresList.slice(0, 49);
                } else {
                    nearStoresList = storesList.slice(0, 49);
                }

                _.each(nearStoresList, function (store) {
                    setStoreStatus(store, days[today.getDay()]);
                });

                if (statusFilter) {
                    statusFilter = false;
                    nearStoresList = _.filter(nearStoresList, function (store) {
                        if (store.storeStatus.status === "open") {
                            return store;
                        }
                    });
                }
                _.each(nearStoresList,function(item,index){
                    if(index != nearStoresList.length-1){
                        locationCodes += item.name+",";
                        }else{
                        locationCodes += item.name;
                    }
                });

                
                var productCode = $.parseJSON($.cookie("quickViewProduct")).productCode;
                var isItemEcomEnabled = $.parseJSON($.cookie("quickViewProduct")).isEcomEnabled;
                api.request('GET','/api/commerce/catalog/storefront/products/'+ productCode +'/locationinventory?pageSize=1100&locationCodes='+locationCodes).then(function(storesResponse){
                    locationInventory = storesResponse.items;
                    api.request('GET','/api/commerce/catalog/storefront/products/'+ productCode +'/locationinventory?pageSize=1100&locationCodes='+'1,2,4').then(function(warehouseResponse){
                        nearStoresList = me.setStoreInventoryInfo(nearStoresList,warehouseResponse, locationInventory, isItemEcomEnabled);
                        me.renderStoresView(nearStoresList, warehouseResponse, locationInventory);
                    });
                });
                
            } else {
                storeData = {
                    "stores": nearStoresList
                };
                me.model = new Backbone.Model(storeData);
                setTimeout(function () {
                    if (checkedFilters && checkedFilters.length > 0) {
                        me.resetFilters();
                        me.model.set("searchErrorMsg", Hypr.getLabel("noStoreFoundOnFilters"));
                    }else{
                        me.model.set("searchErrorMsg", Hypr.getLabel("noStoreFoundOnSearch"));
                    }
                    me.render();
                    $("#check-nearby-loading").hide();
                }, 500);
            }
        },
        /*This event will make field empty i.e If focus is on Postal code then city will get empty 
        and vice versa.
        */
        onSearchFocusBlur: function (e) {
            var inputElement = e.currentTarget;
            if (inputElement.name === "postalCode") {
                $("#city").val("");
            } else {
                $("#postalCode").val("");
                if(cityChanges){
                    this.populateCities(e);
                }
            }
        },
        /*This function will make search field blur once searched has been done*/
        blurSearchField: function () {
            if ($("#postalCode").is(":focus")) {
                $("#postalCode").blur();
            }
            if ($("#city").is(":focus")) {
                $("#city").blur();
            }
        },
        /*this function validates the postal according to canadian country */
        validatePostalCode: function (postal) {
            var regex = /^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ]( )?\d[ABCEGHJKLMNPRSTVWXYZ]\d$/i;
            return regex.test(postal) ? true : false;

        },
        /*This function will open filter window */
        openFilter: function (e) {
            if (e) {
                e.preventDefault();
                e.stopPropagation();
            }
            $(".store-locator-filters").addClass('store-locator-filters-open');
            var screenWidth = window.matchMedia("(max-width: 767px)");
            if (screenWidth.matches) {
                $("body,html").addClass("no-scroll");
            }
        },
        /*This function will close the filter window */
        closeFilter: function (e) {
            if (e) {
                e.preventDefault();
                e.stopPropagation();
            }
            $(".store-locator-filters").removeClass('store-locator-filters-open');
            var screenWidth = window.matchMedia("(max-width: 767px)");
            if (screenWidth.matches) {
                $("body,html").removeClass("no-scroll");
            }
        },
        /*This function will highlight the store in list, open store info on map */
        highlightStore: function (e) {
            var self = this;
            var currentTarget = e.currentTarget;
            if(!$(e.target).hasClass("store-details-link")){
                $(".nearby-store-locator-result").removeClass("is-highlighted");
                $(currentTarget).addClass("is-highlighted");
                var storeIndex = parseInt(currentTarget.getAttribute("data-mz-index"), 10) - 1;
                var data = self.model.attributes.stores[storeIndex];
                var geo = {
                    "lng": parseFloat(data.properties.longitude),
                    "lat": parseFloat(data.properties.latitude)
                };

                var popUps = document.getElementsByClassName('mapboxgl-popup');
                // Check if there is already a popup on the map and if so, remove it
                if (popUps[0]) $(popUps[0]).remove();
                    
                //setZoom and panTo to current selected store
                map.setZoom(Hypr.getThemeSetting('mapZoomLevel'));
                map.panTo(geo);

                new mapboxgl.Popup().setLngLat([parseFloat(data.properties.longitude),parseFloat(data.properties.latitude)]).setHTML(self.infoWindowContent(data, (storeIndex === 0 && data.isPreferresStore))).addTo(map);
            }
        },
        /*This function will create store filters accordingly stores in results */
        setFilters: function () {
            var me = this;
            var stores = me.model.get("stores");
            var storeServices = [],
                storeTypes = [],
                hfServices = [],
                storeFilters,
                contextSiteId = require.mozuData('apicontext').headers["x-vol-site"],
                homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId");
            /*creating service filters array*/

            if (Hypr.getThemeSetting('frSiteId') === contextSiteId) {
                _.each(stores, function (store) {
                    _.each(store.properties.storeServices, function (service) {
                        if (service) {
                            var frenchValue = me.getFrenchTranslation(service);
                            var exist = _.findWhere(storeServices, {
                                "serviceName": service,
                                "serviceValue": frenchValue
                            });
                            if (!exist) {
                                storeServices.push({
                                    "serviceName": service,
                                    "serviceValue": frenchValue
                                });
                            }
                        }
                    });
                });
            } else {
                _.each(stores, function (store) {
                    _.each(store.properties.storeServices, function (service) {
                        if (service) {
                            var exist = _.findWhere(storeServices, {
                                "serviceName": service,
                                "serviceValue": service
                            });
                            if (!exist) {
                                storeServices.push({
                                    "serviceName": service,
                                    "serviceValue": service
                                });
                            }
                        }
                    });
                });
            }

            storeServices = _.sortBy(storeServices, "serviceName");

            if (homeFurnitureSiteId === contextSiteId) {
                /*creating hf services filters array*/
                _.each(stores, function (store) {
                    _.each(store.properties.homeFurnitureServices, function (service) {
                        if (service) {
                            var exist = _.contains(hfServices, service);
                            if (!exist) {
                                hfServices.push(service);
                            }
                        }
                    });
                });
                hfServices = _.sortBy(hfServices);
                storeFilters = {
                    distanceFilters: distanceFilters,
                    hfServices: hfServices,
                    storeServices: storeServices
                };
                me.model.set("isHomeFurnitureSite", true);
            } else {

                /*creating type filters array*/

                if (Hypr.getThemeSetting('frSiteId') === contextSiteId) {
                    _.each(stores, function (store) {
                        _.each(store.properties.storeType, function (type) {
                            if (type) {
                                var frenchType = me.getFrenchTypeTranslation(type);
                                var exist = _.findWhere(storeTypes, {
                                    "storeType": type,
                                    "storeTypeValue": frenchType
                                });
                                if (!exist) {
                                    storeTypes.push({
                                        "storeType": type,
                                        "storeTypeValue": frenchType
                                    });
                                }
                            }
                        });
                    });
                } else {
                    _.each(stores, function (store) {
                        _.each(store.properties.storeType, function (type) {
                            if (type) {
                                var exist = _.findWhere(storeTypes, {
                                    "storeType": type,
                                    "storeTypeValue": type
                                });
                                if (!exist) {
                                    storeTypes.push({
                                        "storeType": type,
                                        "storeTypeValue": type
                                    });
                                }
                            }
                        });
                    });
                }
                storeTypes = _.sortBy(storeTypes, "storeType");

                storeFilters = {
                    storeServices: storeServices,
                    storeTypes: storeTypes,
                    distanceFilters: distanceFilters
                };
                me.model.set("isHomeFurnitureSite", false);
            }

            me.model.set("storeFilters", storeFilters);
            if (checkedFilters && checkedFilters.length > 0) {
                var length = '(' + checkedFilters.length + ')';
                me.model.set("numOFFilters", length);
            } else {
                me.model.set("numOFFilters", "");
            }
        },
        /*This function will return french translation of services */
        getFrenchTranslation: function (service) {
            var servicesList = Hypr.getThemeSetting("storeServices");
            var storeService = _.find(servicesList, function (item) {
                return item.enName === service;
            });
            if (storeService)
                return storeService.frName;
        },
        /*This function will return french translation of store type */
        getFrenchTypeTranslation: function (type) {
            var frenchValue = "";
            switch (type) {
                case "Home Building Centre":
                    frenchValue = "Centre de Rénovation";
                    break;
                case "Home Hardware Building Centre":
                    frenchValue = "Centre de Rénovation Home Hardware";
                    break;
                case "Home Hardware":
                    frenchValue = "Quincaillerie Home Hardware";
                    break;
            }
            return frenchValue; 
        },
        /*This function will apply selected filters */
        applyFilters: function (e) {
            var me = this;
            e.stopImmediatePropagation();
            var screenWidth = window.matchMedia("(max-width: 767px)");
            var filtersToApply = $(".store-locator-filters").find("input:checked");
            $(".store-locator-filters").removeClass('store-locator-filters-open');
            if (screenWidth.matches) {
                $("body,html").removeClass("no-scroll");
            }
            if (filtersToApply.length > 0) {
                $("#check-nearby-loading").show();
                checkedFilters = filtersToApply;
                me.getFilterValues(checkedFilters);
            } else {
                if (checkedFilters.length > 0) {
                    $("#check-nearby-loading").show();
                    checkedFilters = filtersToApply;
                    me.getFilterValues(checkedFilters);
                }
            }
        },
        /*This function will accept array of filters and fetches document list with applied filters */
        getFilterValues: function (filters) {
            var me = this;
            var storeTypeFilters = [],
                storeServiceFilters = [],
                storesProductsFilters = [],
                queryString = "",
                distanceFilter = "",
                preferredStore = null;
            if (localStorage.getItem('preferredStore')) {
                preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
            }
            _.each(filters, function (filter) {
                var type = $(filter).data("mz-type");

                switch (type) {
                    case "distance":
                        distanceFilter = $(filter).data("mz-value");
                        break;
                    case "storeType":
                        storeTypeFilters.push("'" + $(filter).data("mz-value") + "'");
                        break;
                    case "storeService":
                        storeServiceFilters.push("'" + $(filter).data("mz-value") + "'");
                        break;
                    case "status":
                        statusFilter = filter.checked;
                        break;
                    case "productsFilters":
                        storesProductsFilters.push("'" + $(filter).data("mz-value") + "'");
                        break;
                }
            });

            if (!isHomeFurnitureSite) {
                if (storeTypeFilters.length > 0 && storeServiceFilters.length > 0) {
                    queryString = 'properties.storeType in[' + storeTypeFilters.join() + '] and properties.storeServices in[' + storeServiceFilters.join() + ']';
                } else if (storeServiceFilters.length > 0) {
                    queryString = 'properties.storeServices in[' + storeServiceFilters.join() + ']';
                } else if (storeTypeFilters.length > 0) {
                    queryString = 'properties.storeType in[' + storeTypeFilters.join() + ']';
                }
                if (!queryString) {
                    queryString = "properties.storeType in['Home Hardware','Home Building Centre', 'Home Hardware Building Centre'] and properties.displayOnline eq true";
                } else {
                    queryString += " and properties.displayOnline eq true";
                }
            } else {
                if (storesProductsFilters.length > 0 && storeServiceFilters.length > 0) {
                    queryString = 'properties.homeFurnitureServices in[' + storesProductsFilters.join() + '] and properties.storeServices in[' + storeServiceFilters.join() + ']';
                } else if (storeServiceFilters.length > 0) {
                    queryString = 'properties.storeServices in[' + storeServiceFilters.join() + ']';
                } else if (storesProductsFilters.length > 0) {
                    queryString = 'properties.homeFurnitureServices in[' + storesProductsFilters.join() + ']';
                }
                if (!queryString) {
                    queryString += "properties.storeType in['Home Furniture'] and properties.displayOnline eq true";
                } else {
                    queryString += "and properties.storeType in['Home Furniture'] and properties.displayOnline eq true";
                }
            }

            api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + queryString + '&pageSize=1100', {
                cache: false
            }).then(function (response) {
                var nearStoresList;
                if (distanceFilter) {
                    // filter response with distance
                    nearStoresList = searchBasedLatLong ? findNearStores(response.items, searchBasedLatLong, distanceFilter) : findNearStores(response.items, preferredStore.geo, distanceFilter);
                    me.reRenderView(nearStoresList);
                } else {
                    nearStoresList = searchBasedLatLong ? findNearStores(response.items, searchBasedLatLong, Hypr.getThemeSetting("storeDistanceInkm")) : findNearStores(response.items, preferredStore.geo, Hypr.getThemeSetting("storeDistanceInkm"));
                    me.reRenderView(nearStoresList);
                }
            });
        },
        resetFilters: function () {

            $(".store-locator-filters").removeClass('store-locator-filters-open');
            var screenWidth = window.matchMedia("(max-width: 767px)");
            if (screenWidth.matches) {
                $("body,html").removeClass("no-scroll");
            }
            _.each(checkedFilters, function (filter) {
                var id = $(".store-locator-filters").find("#" + filter.id);
                $(id).prop("checked", true);
            });

        },
        clearFilters: function (e) {
            var me = this;
            e.preventDefault();
            if (checkedFilters && checkedFilters.length > 0) {
                var screenWidth = window.matchMedia("(max-width: 767px)");
                if (screenWidth.matches) {
                    $("body,html").removeClass("no-scroll");
                }
                _.each(checkedFilters, function (filter) {
                    var id = $(".store-locator-filters").find("#" + filter.id);
                    $(id).prop("checked", false);
                });
                checkedFilters.splice(0, checkedFilters.length);

                api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + filtersString + '&pageSize=1100', {
                    cache: false
                }).then(function (response) {
                    var preferredStore, latLong;
                    if (localStorage.getItem('preferredStore')) {
                        preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
                        latLong = search ? searchBasedLatLong : preferredStore.geo;
                    } else {
                        latLong = searchBasedLatLong;
                    }
                    var nearStoresList = findNearStores(response.items, latLong, Hypr.getThemeSetting("storeDistanceInkm"));
                    me.reRenderView(nearStoresList);
                });
            } else {
                checkedFilters = $(".store-locator-filters").find("input:checked");
                _.each(checkedFilters, function (filter) {
                    var id = $(".store-locator-filters").find("#" + filter.id);
                    $(id).prop("checked", false);
                });
                checkedFilters.splice(0, checkedFilters.length);
            }
        },

        toggleMapListView: function (e) {
            if (e) {
                e.preventDefault();
                e.stopPropagation();
            }
            $(".nearby-store-dialog").toggleClass('store-loc-map-toggle');
            $(".store-locator-search-mobile").toggleClass('store-loc-mobile-toggled');
            //resize map here
            map.resize();
        },

        storeWithAvailableStock:function(){
            $("#check-nearby-loading").show();
            var self = this;
            var preferredStore =  null;
            if (localStorage.getItem('preferredStore')) {
                preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
            }
            if((!cityChanges && self.model.get("search"))){
                if($('#postalCode').val() === ""){
                    self.model.set("search",null);
                    search = "";
                    searchBasedLatLong = null;
                }
            }
            if(($('#postalCode').val() && !self.validatePostalCode($('#postalCode').val().trim()) && self.model.get("search")) || (!searchBasedLatLong) ){
                self.model.set("search",null);
                search = "";
                searchBasedLatLong = null;
            }
            if($("#storeWithAvailableStock").is(":checked")){
                api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter='+filtersString+'&pageSize=1100', {
                    cache: false
                }).then(function (response) {
                    storeWithStock = true;
                    var nearStoresList = [];
                    nearStoresList = searchBasedLatLong ? findNearStores(response.items, searchBasedLatLong, Hypr.getThemeSetting("storeDistanceInkm")) : findNearStores(response.items, preferredStore.geo, Hypr.getThemeSetting("storeDistanceInkm"));
                    self.reRenderView(nearStoresList);
                });
            }
            else{
                api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter='+filtersString+'&pageSize=1100', {
                    cache: false
                }).then(function (response) {
                    storeWithStock = false;
                    var nearStoresList = [];
                    nearStoresList = searchBasedLatLong ? findNearStores(response.items, searchBasedLatLong, Hypr.getThemeSetting("storeDistanceInkm")) : findNearStores(response.items, preferredStore.geo, Hypr.getThemeSetting("storeDistanceInkm"));
                    self.reRenderView(nearStoresList);
                });
            }
        },
        closeModal: function(){
            checkedFilters = [];
            search = "";
            storeWithStock = false;
            searchBasedLatLong = null;
            cityChanges = false;
            setTimeout(function(){
                if($('body').css("padding-right") == '15px') {
                    $('body').css("padding-right",'0');
                }
            },1000);
        },
        formatDate: function(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        },
        setWarehouseInventory: function(response,locationInventory, nearStoreList){
            var me = this, currentStore;
            nearStoreList = nearStoreList;
            var isDcAvailableForFirst = false , isDcAvailableForSecond = false , isDcAvailableForFourth = false;
                if('1' === response.items[0].locationCode && response.items[0].stockAvailable > 0){
                    isDcAvailableForFirst = true;
                }else if('2' === response.items[1].locationCode && response.items[1].stockAvailable > 0){
                    isDcAvailableForSecond = true;
                }else if('4' === response.items[2].locationCode && response.items[2].stockAvailable > 0){
                    isDcAvailableForFourth = true;
                }

            me.model.set('isDcAvailableForFirst', isDcAvailableForFirst ); 
            me.model.set('isDcAvailableForSecond', isDcAvailableForSecond );
            me.model.set('isDcAvailableForFourth' , isDcAvailableForFourth);
        },
        setStoreInventoryInfo: function(nearStoresList,warehouseResponse, locationInventory, isItemEcomEnabled){
            var currentDate = new Date(),
                me = this;
            _.each(nearStoresList,function(item){
                var storeInventory = _.findWhere(locationInventory, {
                    locationCode: item.name
                });
                var wareHouseInventory = _.findWhere(warehouseResponse.items, {
                    "locationCode": item.properties.warehouse
                });
            /** Scenario's- 
             * 1) For NRTI Store, if WareHouse Inventory + Store inventory is greater than 0 then Show Get it by message and 
             *    if false then do not show any message.
             * 2) If Non NRTI Store, if Warehouse inventory is greater than 0 then show get it by message 
             *    else do not display any message
             * Ticket number - IWM-92
             */
                var DCPlusLocationInventoryTotal;
                if(wareHouseInventory && storeInventory && item.properties.hhBohInd === true){
                    DCPlusLocationInventoryTotal = wareHouseInventory.stockAvailable + storeInventory.stockAvailable;
                    if(DCPlusLocationInventoryTotal > 0)
                        item.getItBy = me.formatDate(new Date().setDate(currentDate.getDate() + 15));
                
                } else if(wareHouseInventory && (false === item.properties.hhBohInd || undefined === item.properties.hhBohInd || null === item.properties.hhBohInd)){
                        if(wareHouseInventory.stockAvailable > 0)
                            item.getItBy = me.formatDate(new Date().setDate(currentDate.getDate() + 15));
                }
                if(storeInventory){
                    item.locationInventory = storeInventory;
                    if(isItemEcomEnabled){
                        item.itemEcomEnabled = true;
                    }
                } else {
                    if(isItemEcomEnabled){
                        item.itemEcomEnabled = true;
                    }
                }
            });
            // filter stores based on stock available i.e stock > 0
            if(storeWithStock){
                nearStoresList = _.filter(nearStoresList, function (store) {
                    if(store.locationInventory && store.locationInventory.stockAvailable > 0 && store.properties.hhBohInd){
                        return store;
                    }
                });
            }
            return nearStoresList;
        },
        renderStoresView: function(nearStoresList, warehouseResponse, locationInventory){
            var me = this,
                storeData = { "stores" : null };
            if(nearStoresList.length > 0){
                storeData = {
                    "stores": nearStoresList
                };
                me.model = new Backbone.Model(storeData);
                setTimeout(function () {
                    me.setWarehouseInventory(warehouseResponse, locationInventory,nearStoresList);
                    me.render();
                    $("#check-nearby-loading").hide();
                    if (checkedFilters && checkedFilters.length > 0) {
                        me.resetFilters();
                    }
                }, 500);
            }else{
                storeData = {
                    "stores": nearStoresList
                };
                me.model = new Backbone.Model(storeData);
                setTimeout(function () {
                    me.model.set("searchErrorMsg", Hypr.getLabel("noStoreFoundOnFilters"));
                    if (checkedFilters && checkedFilters.length > 0) {
                        me.resetFilters();  
                    }
                    me.setWarehouseInventory(warehouseResponse, locationInventory);
                    me.render();
                    $("#check-nearby-loading").hide();
                }, 500);
            }
        }
    });

    /*This function finds stores near preferred store in 50km range by default*/
    function findNearStores(storeList, location, range) {
        var nearStores = [];
        _.each(storeList, function (store) {
            var distance = getDistanceFromLatLongInKm(store.properties.latitude, store.properties.longitude, location.lat, location.lng);
            if (distance <= range) {
                var storeData = {
                    distance: parseFloat(distance.toFixed(3)),
                    modelData: store
                };
                nearStores.push(storeData);
            }
        });
        nearStores = _.sortBy(nearStores, "distance");
        var resultArray = [];
        _.each(nearStores, function (store) {
            resultArray.push(store.modelData);
        });
        return resultArray;
    }
    /*This function calculates and return the distance between preferred store and input store*/
    function getDistanceFromLatLongInKm(lat1, lng1, lat2, lng2) {
        var earthRadius = 6371; // Radius of the earth in km
        var dLat = deg2rad(lat2 - lat1); // deg2rad below
        var dLng = deg2rad(lng2 - lng1);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var distance = earthRadius * c * 0.62137119; // Distance in Mi
        return distance;
    }
    /*this funtion converts deg to rad*/
    function deg2rad(deg) {
        return deg * (Math.PI / 180);
    }

    /*this function set working hours status */
    function setStoreStatus(store, day) {
        store.storeStatus = getStoreStatus(store, day);
    }

    /*this function calculates and returns status object with working hours*/
    function getStoreStatus(store, day) {
        var workingHours = "",
            statusData;
        var start = new Date(),
            end = new Date();
        switch (day) {

            case "Sunday":
                workingHours = store.properties.sundayHours;
                break;
            case "Monday":
                workingHours = store.properties.mondayHours;
                break;
            case "Tuesday":
                workingHours = store.properties.tuesdayHours;
                break;
            case "Wednesday":
                workingHours = store.properties.wednesdayHours;
                break;
            case "Thursday":
                workingHours = store.properties.thursdayHours;
                break;
            case "Friday":
                workingHours = store.properties.fridayHours;
                break;
            case "Saturday":
                workingHours = store.properties.saturdayHours;
                break;
        }
        var currentTime = moment(),
            startTime, endTime;
        if (workingHours !== "Closed") {
            var workHours = workingHours.split("-");
            startTime = moment(workHours[0], "HH:mm a");
            endTime = moment(workHours[1], "HH:mm a");
        }
        if (currentTime.isBetween(startTime, endTime)) {
            statusData = {
                "status": "open",
                "hours": workingHours
            };
        } else {
            statusData = {
                "status": "closed",
                "hours": workingHours
            };
        }
        return statusData;
    }
    /*This event highlights in populated cities on mouseover */
    $("#city_list li").mouseover(function() {
        $("#city_list li").removeClass("selected");
        $(this).addClass("selected");
    });
    /*This event closes populated city window on click of window */
    $(document).click(function() {
        $('#city_list:visible').hide();
    });
    /*This event stops the event propagation on city */
    $("#city_list").click(function(e) {
        e.stopPropagation(); 
        return false;
    });
    /*This function reads the searched city value and hides the populated city window */
    function onCitySelect(event) {
        var target = $( event.target ); 
        var cityInput = document.getElementById("city");
        $("#hdnFlagClick").val(cityInput.value = target.text());
        if(target.is('a')){
            event.preventDefault();
            cityInput.value = target.text();
            cityChanges = true;
        }
        $("#city_list").hide();
    }

    /*This function will make call to set purchase location and then in sucess it updates the cookie and make a call to 
        update customere attribute is user is logged in */
    function continueWithNewStore() {
        $.ajax({
            url: "/set-purchase-location",
            data: {
                "purchaseLocation": storeToChange
            },
            type: "GET",
            success: function (response) {
                api.get("location", {
                    code: storeToChange
                }).then(function (response) {
                    var selectedStore = response.data;
                    CookieUtils.setPreferredStore(selectedStore);
                    //$.cookie("sessionStore",JSON.stringify(selectedStore.code),{path:'/'});
                    /**
                     `* Below code removes a flag stored in a cookie. This cookie is set through Arc (IGN_HH_IP_BASED_STORE_LOCATOR.1.0.0)
                      * When a user selects a store manually, we need to remove this flag, so that at cart checkout store confirmation pop-up disappears.
                      * Ticket - Main Ticket - 1202, Sub ticket - IWM-1471
                      * @Date `- 19/04/2021
					*/
					if($.cookie('preferredStore_userConfirmationRequired')) {
                        $.removeCookie('preferredStore_userConfirmationRequired', { path: '/' });
                    }
                    if(itemsInCart.length > 0){
                        var previousItemQuantity = [];
                        _.each(itemsInCart, function(item){
                            var prodObj = {
                                'quantity' : item.quantity,
                                'productCode' : item.product.productCode,
                                'price': item.product.price.price,
                                'salePrice': item.product.price.salePrice
                            };
                            previousItemQuantity.push(prodObj);
                        });

                        if (ua.indexOf('chrome') > -1) {
                            if (/edg|edge/i.test(ua)){
                                $.cookie("previousItems",JSON.stringify(previousItemQuantity),{path:'/'});
                            }else{
                                document.cookie = "previousItems="+JSON.stringify(previousItemQuantity)+";path=/;Secure;SameSite=None";
                            } 
                        } else {
                            $.cookie("previousItems",JSON.stringify(previousItemQuantity),{path:'/'});
                        }
                    }
                    if (user.isAuthenticated && !user.isAnonymous) {
                        updateCustomerAttribute();
                    } else {
                        window.location.reload();
                    }
                });
            },
            error: function (error) {

            }
        });
    }
    /*This function will update the customer attributes with selected store */
    function updateCustomerAttribute() {
        var preferredStore = null;
        if (localStorage.getItem('preferredStore')) {
            preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
        }
        var customer = new CustomerModels.EditableCustomer();
        customer.set("id", user.accountId);
        customer.fetch().then(function (response) {
            var attribute;
            if (isHomeFurnitureSite) {
                attribute = _.findWhere(response.apiModel.data.attributes, {
                    fullyQualifiedName: Hypr.getThemeSetting('hfPreferredStore')
                });
            } else {
                attribute = _.findWhere(response.apiModel.data.attributes, {
                    fullyQualifiedName: Hypr.getThemeSetting('preferredStore')
                });
            }
            var attributeData = {
                attributeFQN: attribute.fullyQualifiedName,
                attributeDefinitionId: attribute.attributeDefinitionId,
                values: [preferredStore.code]
            };
            response.apiUpdateAttribute(attributeData).then(function () {
                window.location.reload();
            });
        });
    }

    /*Handled modal close */
    $(document).on("click" , ".close-confirmation",function() {
        $('body').addClass('modal-open-fix');
        if($('body').css("padding-right") == '15px') {
            $('body').addClass('modal-open-fix-pad');
        }
        setTimeout(function() {
            if($('body').hasClass('modal-open-fix-pad')) {
                $('body').css('padding-right', '15px');
                $('body').removeClass('modal-open-fix-pad');
            }
            $('body').addClass('modal-open').removeClass('modal-open-fix');
        }, 1000);
    });

    $(document).on("click", "#continueWithNewStoreBtn",function(){
        continueWithNewStore();
    }); 
    return {
        CheckNearbyStore : CheckNearbyStoreModal
    };
});
