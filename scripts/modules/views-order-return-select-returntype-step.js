define([
    'modules/jquery-mozu',
    'underscore',
    'modules/backbone-mozu',
    'modules/views-order-return-select-return-method-step'
], function ($, _, Backbone, OrderReturnSelectReturnMethodView) {
    var SelectReturnTypeStepView = Backbone.MozuView.extend({
        templateName: 'modules/my-account/my-account-order-return-select-returntype-step',
        initialize: function () {
            this.setDataRmaItem();
        },
        setDataRmaItem: function () {
            var orderItems = this.model.get('items'),
                rmaItems = this.model.get('rma').get('items').models,
                shipReturnItemsCount = 0;
            _.each(rmaItems, function (rmaItem) {
                var orderItem = _.findWhere(orderItems.models, {'id': rmaItem.get('orderItemId')});
                rmaItem.set('fulfillmentLocationCode', orderItem.get('fulfillmentLocationCode'));
                rmaItem.set('fulfillmentMethod', orderItem.get('fulfillmentMethod'));
                if (orderItem.get('fulfillmentMethod') === 'Ship') shipReturnItemsCount++;
            });
            this.model.set('returnOrderType', shipReturnItemsCount == rmaItems.length ? "ship" : "mixOrBopis");
        },
        render: function () {
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            this.model.set("currentLocale", locale, {silent: true});
            if (this.model.get('billingInfo').paymentType === "CreditCard") this.setCardLastDigits();
            this.setReturnTypes();
            Backbone.MozuView.prototype.render.apply(this, arguments);
        },
        setCardLastDigits: function () {
            var cartNumber = this.model.get('billingInfo').card.cardNumberPartOrMask,
                lastFourDigits = cartNumber.substring(cartNumber.length - 4, cartNumber.length);
            this.model.set('cartLastFourDigits', lastFourDigits);
        },
        setReturnTypes: function () {
            if(this.model.get('returnOrreplaceselected')!=='1') 
            { 
            var rma = this.model.get('rma'); 
            _.each(rma.get('items').models, function (rma) {
                rma.set('rmaReturnType', "Refund");
                rma.set('replaceType', 'refund');
                rma.set('selectedReturnType', 'Refund'); 
            }); 
            } 
        },
        continueToSelectReturnMethodStep: function () {
            this.model.set('returnOrreplaceselected', '1');
            var orderReturnSelectReturnMethodView = new OrderReturnSelectReturnMethodView({
                el: this.$el,
                model: this.model
            }); 
            orderReturnSelectReturnMethodView.render();
        },
        setReturnType: function (e) {
            var currentTarget = $(e.currentTarget),
                orderItemId = currentTarget.data('order-item-id'),
                replaceType = currentTarget.data('replace-type'),
                returnType = currentTarget.val(),
                rma = this.model.get('rma');
            _.each(rma.get('items').models, function (rma) {
                if (rma.get('orderItemId') === orderItemId) {
                    rma.set('rmaReturnType', returnType);
                    rma.set('replaceType', replaceType);
                    rma.set('selectedReturnType', returnType); 
                }
            }); 
        }
    });

    return SelectReturnTypeStepView;
});