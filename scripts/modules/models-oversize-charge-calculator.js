define(["modules/api", "modules/backbone-mozu"], function (api, Backbone) {
    var OversizeCalculator = Backbone.MozuModel.extend({
        isOversizeDimension: function (dimension, oversizeLimit) {
            var isOversizeDimension = false;

            if (dimension.unit === "in" && oversizeLimit && (dimension.value || 0) >= oversizeLimit) {
                isOversizeDimension = true;
            } else if (dimension.unit === "cm" && oversizeLimit) {
                var inchVal = (dimension.value || 0) / 2.54;
                isOversizeDimension = inchVal >= oversizeLimit;
            }

            return isOversizeDimension;
        },
        getOversizeConfigurations: function () {
            return api.request("GET", "/hh/api/getOversizeConfiguration");
        },
        getOversizeCharges: function (globalConfig, cartItems) {
            var oversizeCharge = 0;
            var self = this;
            cartItems.forEach(function (cartItem) {
                if (cartItem.fulfillmentMethod === "Ship") {
                    var height = cartItem.product.measurements.height,
                        width = cartItem.product.measurements.width,
                        length = cartItem.product.measurements.length;
                    globalConfig.oversizeLimit = globalConfig.oversizeLimit || {};

                    var isOversizeDimension = self.isOversizeDimension(height, globalConfig.oversizeLimit.height);
                    isOversizeDimension =
                        isOversizeDimension || self.isOversizeDimension(width, globalConfig.oversizeLimit.width);
                    isOversizeDimension =
                        isOversizeDimension || self.isOversizeDimension(length, globalConfig.oversizeLimit.length);

                    if (isOversizeDimension) {
                        oversizeCharge += (globalConfig.oversizeLimit.surcharge * cartItem.quantity) || 0;
                    }
                }
            });
            return oversizeCharge;
        }
    });
    return OversizeCalculator;
});
