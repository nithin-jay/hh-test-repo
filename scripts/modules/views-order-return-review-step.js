define(["modules/api",
    'modules/jquery-mozu',
    'underscore',
    'modules/backbone-mozu',
    'hyprlive',
    'modules/views-order-return-confirmation',
    'bignumber'
], function (api,$, _, Backbone, Hypr, OrderReturnConfirmationView,BigNumber) {
    var ReturnReviewStepView = Backbone.MozuView.extend({
        templateName: 'modules/my-account/my-account-order-return-review-step',
        additionalEvents: {
            "change [data-mz-edit-quantity]": "onQuantityChangeReview",
            "click [data-mz-edit-quantity]": "onQuantityChangeReview",
            "keyup input[data-mz-edit-quantity]": "onQuantityChangeReview",
            "blur input[data-mz-edit-quantity]": "onQuantityChangeReview",
            "click .editLink": "displayQuantityInput"
        },
        initialize: function(){
            this.getAndSetShipTimelines();
        },
        render: function () {
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            this.model.set("currentLocale", locale, {silent: true});
            Backbone.MozuView.prototype.render.apply(this, arguments);
        },
        continueToReturnConfirmation: function(data){
            var orderReturnConfirmationView = new OrderReturnConfirmationView({
                el: this.$el,
                model: new Backbone.MozuModel(data)
            });
            orderReturnConfirmationView.render();
        },
        getAndSetShipTimelines: function () {
            var self = this,
                getShippingTimelineTasks = [],
                replaceViaCourierItems = [];
                _.each(this.model.get('rma').get('items').models, function (rmaItem) {
                    if(rmaItem.get('rmaReturnType') === "Replace" && rmaItem.get('replaceType') === "replaceViaCourier"){
                        getShippingTimelineTasks.push(self.fetchShippingTimelines(rmaItem));
                        replaceViaCourierItems.push(rmaItem.get('productCode'));
                    }
                });
                this.model.set('replaceViaCourierItems', replaceViaCourierItems);
            if(getShippingTimelineTasks.length){
                this.$el.parent().siblings(".content-loading").css('display', 'block');
                Promise.all(getShippingTimelineTasks).then(function (response) {
                    self.successHandler(response);
                }, function (error) {
                    this.$el.parent().siblings(".content-loading").css('display', 'none');
                    console.log('Error while getting shipping rates', error);
                });
            }
        },
        successHandler: function(shippingTimelines){
            var isFrenchSite = this.model.get('currentLocale') === "en-US" ? false : true;
            _.each(this.model.get('rma').get('items').models, function (rmaItem) {
                if(rmaItem.get('rmaReturnType') === "Replace" && rmaItem.get('replaceType') === "replaceViaCourier"){
                    var shipmentTimelineData = _.find(shippingTimelines, function(shippingTimeline){
                        return shippingTimeline.serviceAndPrice.statusCode === "200" && shippingTimeline.serviceAndPrice.shipmentNumber == rmaItem.get('shipmentNumber');
                    });
                    if (shipmentTimelineData) {
                        var shippingTimeline = shipmentTimelineData.serviceAndPrice.expectedTransitTime,
                            bufferedTimeline = shippingTimeline + (isFrenchSite ? ' à ' : '-') + (shippingTimeline + Hypr.getThemeSetting('shippingTimelineBuffer'));
                        rmaItem.set('shippingTimeline', bufferedTimeline);
                    }
                }
            });
            this.render();
            this.$el.parent().siblings(".content-loading").css('display', 'none');
        },
        fetchShippingTimelines: function (rmaItem) {
            var payload = this.getPayload(rmaItem);
            return $.ajax({
                url: Hypr.getThemeSetting("getShippingRates"),
                data: JSON.stringify(payload),
                type: "POST",
                cors: true,
                contentType: 'application/json'
            });
        },
        getPayload: function (rmaItem) {
            return {
                'shipmentNumber': rmaItem.get('shipmentNumber'),
                'products': [
                    {
                        "productCode": rmaItem.get('productCode'),
                        "quantity": 1
                    }
                ]
            };
        },
        onQuantityChangeReview: _.debounce(function (e) {
            var $ele = $(e.currentTarget),
            changedQty = parseInt($ele.val(),10),
            quantityReturnable = parseInt($ele.attr("data-mz-returnable-quantity"),10),
            orderItemId = $ele.data('order-item-id'), 
            maxReturnableQuantity = quantityReturnable,
            qtyField = $(e.currentTarget).closest("div").siblings(".product-quantity").find(".qty");
            if(isNaN(changedQty) || changedQty <= 0) {
                e.preventDefault();
                $(e.currentTarget).val(1);
                this.updateQuantityToRma(orderItemId,1);
                qtyField.text(1);
            }else if(changedQty > maxReturnableQuantity){
                $(e.currentTarget).val(maxReturnableQuantity);
                this.updateQuantityToRma(orderItemId,maxReturnableQuantity);
                qtyField.text(maxReturnableQuantity);
            }else{
                this.updateQuantityToRma(orderItemId,changedQty);
                qtyField.text(changedQty);
            }
            this.calculateRefundAmount();
        },600),
        calculateRefundAmount: function(){
            var self = this,
                rmaItems = this.model.get("rma").get("items").models,
                orderItems = this.model.get('items'),
                shippingCost = this.model.get("shippingCost"),
                refundSubtotal = 0,
                totalEstimatedShipping = 0;

            _.each(rmaItems,function(item){
                if(item.get('replaceType') != "replaceViaCourier"){
                    var rmaItemSubtotal =  parseFloat(new BigNumber(item.get('itemPriceWithEHF')).multipliedBy(item.get('rmaQuantity')).toFixed(2)),
                        orderItem = _.findWhere(orderItems.models, {'id': item.get('orderItemId')}),
                        itemTaxTotal = self.getItemTax(orderItem, rmaItemSubtotal),
                        itemPriceWithTax = parseFloat(BigNumber.sum(rmaItemSubtotal, itemTaxTotal).toFixed(2)),
                        itemTotalShipping = parseFloat(new BigNumber(item.get('itemShippingCost')).multipliedBy(item.get('rmaQuantity')).toFixed(2));
                    refundSubtotal = parseFloat(BigNumber.sum(refundSubtotal.toFixed(2), itemPriceWithTax, itemTotalShipping).toFixed(2));
                }
            });
            totalEstimatedShipping = parseFloat(new BigNumber(refundSubtotal).minus(shippingCost).toFixed(2));
            this.model.set('refundSubtotal', refundSubtotal);
            this.model.set('totalEstimatedRefund', totalEstimatedShipping);
            self.setDataToSummary(refundSubtotal,totalEstimatedShipping);
        },
        getItemTax: function (orderItem, rmaItemSubtotal) {
            var itemTaxData = orderItem.get('taxData').taxRates,
                itemTaxTotal = 0;
            _.each(itemTaxData, function (tax) {
                var taxRate = parseFloat(tax.taxRate),
                    itemTax = parseFloat(new BigNumber(rmaItemSubtotal).multipliedBy(taxRate).toFixed(2));
                itemTaxTotal = parseFloat(BigNumber.sum(itemTaxTotal, itemTax).toFixed(2));
                console.log(tax, itemTax);
            });
            return itemTaxTotal;
        },
        setDataToSummary: function(refundSubtotal,totalEstimatedShipping){
            var currentLocale = this.model.get("currentLocale");
            if(currentLocale === "en-US"){
                refundSubtotal = refundSubtotal.toLocaleString('en-US',{style: 'currency', currency: 'USD'});
                totalEstimatedShipping = totalEstimatedShipping.toLocaleString('en-US',{style: 'currency', currency: 'USD'}); 
            }else if(currentLocale === "fr-CA"){
                refundSubtotal = refundSubtotal.toLocaleString('fr-CA',{style: 'currency', currency: 'USD'}).replace("US", "");
                totalEstimatedShipping = totalEstimatedShipping.toLocaleString('fr-CA',{style: 'currency', currency: 'USD'}).replace("US", "");
            }
            this.$el.find(".summary-table").find("#refund-subTotal").text(refundSubtotal);
            this.$el.find(".summary-table").find(".total-refund").text(totalEstimatedShipping);
        },
        updateRMA: function(){
            _.each(this.model.get('rma').get("items").models,function(item){
                var returnType,rmaReason,rmaQuantity,rmaComment;
                returnType = item.get("rmaReturnType");
                rmaReason = item.get("rmaReason");
                rmaQuantity =  item.get("reasons") && item.get("reasons")[0] ? item.get("reasons")[0].quantity : item.get("rmaQuantity");
                rmaComment = item.get("rmaComments");

                item.set("returnType",returnType);
                item.set("reasons",[
                    {
                        reason:rmaReason,
                        quantity:parseInt(rmaQuantity,10)
                    }
                ]);
                item.set("notes",[{
                    text:rmaComment
                }]);
                item.unset("rmaReturnType");
                item.unset("rmaReason");
                item.unset("rmaQuantity");
                item.unset("rmaComments");
            });
        },  
        setDataToRmaForEmail: function(){
            var rmas = this.model.get('rma'),
            courierType = this.model.get("selectedCourierType") == "store-drop" ? "In-Store Drop Off*" : this.model.get("selectedCourierType"),
            courierRate = this.model.get("selectedCourierType") == "store-drop" ? 0 : this.model.get("courierRate"),
            returnMethod = "Return Method:" + courierType,
            shippingAmount = "Shipping Amount:" + (courierRate ? courierRate : 0),
            notes = [{id:"returnMethod",text:returnMethod},{id:"shippingAmount",text:shippingAmount}];
            if(this.model.get('replaceViaCourierItems').length && courierType === "canadaPost"){
                notes.push({
                    id:"ReplaceViaCourierItems",text: "ReplaceViaCourierItems:" + this.model.get('replaceViaCourierItems').join(',')
                });
            }
            rmas.set({
                notes: notes,
                customerInteractionType: "Website",
                webSessionId: this.model.get('shipmentNumberForShippingLabel')//Used in return history to get shipping labels
            });
        },
        finishOrderReturn: function(){
            var self = this,
            rma = this.model.get('rma'),
            refundSubtotal = self.model.get("refundSubtotal"),
            totalEstimatedRefund = self.model.get("totalEstimatedRefund"),
            selectedCourierType = self.model.get("selectedCourierType"),
            courierRate = self.model.get("courierRate"),
            currentLocale = self.model.get("currentLocale");
            var getReturnableStatus = self.getReturnableStatus();
            this.model.set('preservedRma', this.model.get('rma').toJSON());
            self.updateRMA();
            self.setDataToRmaForEmail();
            rma.syncApiModel();
            self.$el.find(".mz-orderlisting-footer-review").find(".new-btn-primary").addClass("isLoading");
             rma.apiCreate().then(function (data) {
                data.refundSubtotal = refundSubtotal;
                data.totalEstimatedRefund = totalEstimatedRefund;
                data.selectedCourierType = selectedCourierType;
                data.courierRate = courierRate;
                if (data && data.selectedCourierType === 'canadaPost') {
                    data.shippingCost = self.model.get('courierRate');
                } else {
                    data.shippingCost = 0;
                }
                if(getReturnableStatus.showReturn){
                    $(document).find('[data-mz-start-return-id="'+ getReturnableStatus.orderId +'"]').addClass("hidden");
                }
               
                $(document).trigger('refreshReturnHistory');
                self.replaceTypeToRmaItem(data);
                self.continueToReturnConfirmation(data);
                $("#returnOrderItems").parent().siblings(".modal-header").ScrollTo({ duration: 200 });
                $("#returnOrderItems").parent().siblings(".modal-header").find(".cancel-return").text('X').attr('title',currentLocale === 'en-US' ? 'Close' : currentLocale === 'fr-CA' ? 'Fermé' : 'Close');
            }, function (error) {
                 self.$el.find(".mz-orderlisting-footer-review").find(".new-btn-primary").removeClass("isLoading");
                console.log("Error while creating return",error);
            });
        },
        replaceTypeToRmaItem: function(data){
            var self = this;
            _.each(data.data.items,function(item){
                var rmaItem = _.findWhere(self.model.get('preservedRma').items, {'productCode': item.product.productCode });
                    item.replaceType = rmaItem.replaceType;
                    item.shippingTimeline = rmaItem.shippingTimeline;
            });
            data.shippingAddress = self.model.get('orderType') != "shipToStoreOrder" ? self.model.get('fulfillmentInfo').fulfillmentContact :'';
        },
        getReturnableStatus: function(e) {
            var returnableItems = this.model.get("returnableItems").models;
            var rmaItems = this.model.get('rma').get("items").models,
            counter1 = 0,
            counter2 = 0;
            _.each(returnableItems,function(returnableItem){
                var item = _.find(rmaItems, function(item){
                    return item.get('productCode') === returnableItem.get('productCode');
                });
                if(!item){ 
                    counter1++;
                }else if(item && parseInt(item.get('rmaQuantity'),10) === returnableItem.get("quantityReturnable")){
                    counter2++;
                }
            });
            return{
                orderId : this.model.get('id'),
                showReturn : counter1 === 0 && counter2 === returnableItems.length ? true : false
            };
        },
        displayQuantityInput: function (e) {
            var $target = $(e.currentTarget);
            var productCode = $target.data('mzProductCode');
            $("#qty"+productCode).css("display","block");
        },
        updateQuantityToRma: function(orderItemId,newQuantity){
            var rma = this.model.get('rma');
            _.each(rma.get('items').models, function (rma) {
                if (rma.get('orderItemId') === orderItemId) {
                    rma.set('rmaQuantity', newQuantity);
                }
            });
        }
    });

    return ReturnReviewStepView;
});