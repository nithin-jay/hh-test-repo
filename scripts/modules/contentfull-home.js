require(["modules/jquery-mozu", "underscore", "hyprlive", "modules/backbone-mozu", "hyprlivecontext", "modules/api", "contentful"], function ($, _, Hypr, Backbone, HyprLiveContext,api,Contentful) {
    console.log("ContentFull",Contentful);
    var DemoPageView = Backbone.MozuView.extend({
        templateName: 'modules/common/contentfull-home-new',
        render: function () {
            Backbone.MozuView.prototype.render.apply(this);
        },
        initialize: function(){

        }
    });

    var SmallBannerView = Backbone.MozuView.extend({
        templateName: 'modules/common/contentfull-smallBanner',
        render: function () {
            Backbone.MozuView.prototype.render.apply(this);
        },
        initialize: function(){

        }
    });
    
    $(document).ready(function () {
        var client = Contentful.createClient({
            space: 'nsson6zwcoj8',
            accessToken: 'N1277InXu1aT8z_uoIj3sN4CuCerXJl_5vdwkIgmQPA'
        });
        client.getEntry($('.contentful-widget').data('entryid')).then(function (entry) {
            
            
            if(entry.sys.contentType.sys.id == 'bigBanner'){
                console.log("entries",entry);
                DemoPageView = new DemoPageView({
                    el: $('#demo_page'),
                    model: new Backbone.Model(entry)
                });
                DemoPageView.render();
            }
        });  
        client.getEntries({content_type: 'smallBanner'}).then(function (entries) {
            var getEntry = [];
           
           // console.log(JSON.stringify(entries));
            entries.items.forEach(function (entry) {
                console.log(JSON.stringify(entry));
                getEntry.push(entry.fields);
               
              });
              SmallBannerView = new SmallBannerView({
                el: $('#small_banner'),
                model: new Backbone.Model(getEntry)
            });
            SmallBannerView.render();
        });
    });
});