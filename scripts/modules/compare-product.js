define([
	"modules/jquery-mozu", 
	"underscore", 
	"hyprlive", 
	"modules/backbone-mozu",
	'modules/api'
	],
	function($, _, Hypr, Backbone, api){
		
	var ComapreProductModal = Backbone.MozuView.extend({
		templateName: 'modules/product/compare-product-modal',
		initialize:function(){
			
		}
	});
	return {
		CompareProduct : ComapreProductModal
	};
}); 