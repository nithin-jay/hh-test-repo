define([
    "underscore",
    "modules/backbone-mozu",
    "modules/api",
    "modules/jquery-mozu",
    "modules/models-postal-code-checker"
], function (_, Backbone, api, $, PostalCodeChecker) {
    var user = require.mozuData("user");
    var STHPostalCodeCookie = Backbone.MozuModel.extend({
        getPostalCode: function () {
            var self = this;
            return new Promise(function (resolve, reject) {
                if ($.cookie("sthPostalCode")) {
                    var postalCodeData = $.parseJSON($.cookie('sthPostalCode'));
                    resolve({
                        postalCode: postalCodeData.postalCode,
                        status: postalCodeData.status
                    });
                } else if (user && user.accountId && !user.isAnonymous) {
                    self.getCustomerDetails(user).then(
                        function (response) {
                            resolve(response);
                        },
                        function () {
                            self.getStorePostalCode().then(function (response) {
                                resolve(response);
                            }, function (error) {
                                reject("error:", error);
                            });
                        }
                    );
                } else {
                    self.getStorePostalCode().then(function (response) {
                        resolve(response);
                    }, function (error) {
                        reject("error:", error);
                    });
                }
            });
        },
        getCustomerDetails: function (user) {
            var self = this;
            return new Promise(function (resolve, reject) {
                api.get("customer", {
                    id: user.accountId
                }).then(
                    function (response) {
                        var contacts = response.data.contacts,
                            postal,
                            isPrimaryFound = false;
                        _.each(contacts, function (contact) {
                            if (contact && contact.types) {
                                _.each(contact.types, function (type) {
                                    if (type.isPrimary && !isPrimaryFound) {
                                        postal = contact.address.postalOrZipCode;
                                        isPrimaryFound = true;
                                    }
                                });
                            }
                        });
                        if (postal) {
                            self.checkPostalCode(postal, resolve, reject);
                        } else {
                            reject();
                        }
                    },
                    function (error) {
                        reject("error:", error);
                    }
                );
            });
        },
        setPostalCodeCookie: function (postalCode, status, setViaPreferredStore) {
            var expiryDate = new Date(),
                isSetViaPreferredStore = setViaPreferredStore ? setViaPreferredStore : false;
            expiryDate.setYear(expiryDate.getFullYear() + 1);
            $.cookie("sthPostalCode", JSON.stringify({
                postalCode: postalCode,
                status: status,
                setViaPreferredStore: isSetViaPreferredStore
            }), { path: "/", expires: expiryDate });
        },
        checkPostalCode: function (postal, resolve, reject, setViaPreferredStore) {
            var postalCodeChecker = new PostalCodeChecker();
            var self = this,
                postalcode = postal.trim().split(" ").join("");

            postalCodeChecker.getPostalCode(postalcode).then(function (response) {
                var status = response.status,
                    cookieStatus = status === "SUCCESS" ? 'Valid' : "Invalid";
                self.setPostalCodeCookie(postalcode, cookieStatus, setViaPreferredStore);
                if (status === "SUCCESS") {
                    resolve({
                        postalCode: postal,
                        status: "Valid"
                    });
                } else {
                    resolve({
                        postalCode: postal,
                        status: "Invalid"
                    });
                }
            }, function (err) {
                reject("error:", err);
            });
        },
        getStorePostalCode: function () {
            var self = this;
            return new Promise(function (resolve, reject) {
                var preferredStore,
                    setViaPreferredStore;
                if (localStorage.getItem('preferredStore')) {
                    preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
                    setViaPreferredStore = true;
                    self.checkPostalCode(preferredStore.address.postalOrZipCode, resolve, reject, setViaPreferredStore);
                }
            });
        }
    });

    return STHPostalCodeCookie;
});
