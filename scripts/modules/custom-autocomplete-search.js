define([
    'modules/jquery-mozu', 
    'underscore',
    'hyprlive', 
    'modules/api',
    'modules/backbone-mozu',
    'hyprlivecontext'
],
function ($, _, Hypr, api, Backbone, HyprLiveContext) {
    var searchQuery = HyprLiveContext.locals.pageContext.search.query;
    var locale = require.mozuData('apicontext').headers['x-vol-locale'];
    var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
    var ua = navigator.userAgent.toLowerCase();
    var isProductCode = false;
    var hardwareSuggestions = [];
    locale = locale.split('-')[0];
     var AutoComplete = Backbone.MozuView.extend({
        templateName: 'modules/search/search-autocomplete',
        additionalEvents: {
            "keyup .mz-searchbox-input": "handleSearchKeyUp",
            "click .suggestedSearch [data-mz-action='addTerm']": "addTerm"
        },
        render: function () {
            var me = this,
                isActive = false,
                currentSearch = "";

            if(document.activeElement === me.$el.find("input")[0]){
                isActive = true;
                currentSearch = me.$el.find("input").val();
            }

            Backbone.MozuView.prototype.render.apply(this);

            if(isActive){
                me.$el.find("input").val(currentSearch);
                me.$el.find("input").focus();
                me.$el.find("input").trigger("keyup");
            }
        },
        initialize: function () {
            // handle preset selects, etc
            var me = this;
        },


        hasSuggestions: function(suggestionGroup){
            var suggestions = _.find(suggestionGroup, function(suggestion){
                if(suggestion.suggestions)
                return suggestion.suggestions.length > 0;
            });
            if (suggestions){
                return true;
            }
            return false;
        },

        handleSearchKeyUp: _.debounce(function(e){
        	if (e.currentTarget.value.length > 2) {
                var term = e.currentTarget.value.toLowerCase();
                var contextSiteId = require.mozuData('apicontext').headers['x-vol-site'];
                var isFurnitureSite = false;
                var homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId");
                if(homeFurnitureSiteId === contextSiteId){
                    isFurnitureSite = true;
                }
                var locale = require.mozuData('apicontext').headers['x-vol-locale'];
                this.getSuggestion(term);
                this.$el.find(".mz-searchbox-field").addClass('increase-width');
                var environment = Hypr.getThemeSetting('selectedEnvironment');                
                if (e.keyCode === 13) {
                    this.handleRedirect(term, locale, isFurnitureSite, environment);
                }
            } else {
                this.$el.find(".suggestedSearch").addClass('hidden');
                this.$el.find(".mz-searchbox-field").removeClass('increase-width');
            }
        }, Hypr.getThemeSetting('searchSuggestionDelay') || 100),

        handleRedirect : function(term, locale, isFurnitureSite, environment) {
            // console.log('handleRedirect: term -->', term);
            isProductCode = term && (term.length < 9) && (/\d{7}$/.test(term) || /\d{4}-\d{3}/.test(term));
            // console.log('handleRedirect: isProductCode -->', isProductCode);
            if(isProductCode) {
                var redirectPath = '/p/' + term.replace(/-/g, '');
                if (isFurnitureSite) {
                    window.location.href = redirectPath;
                } else {
                    window.location.href = ((locale == "fr-CA") ? '/fr' : '/en') + redirectPath;
                }
            }else{
                if (environment === "PROD") {
                    if(locale == "en-US" && !isFurnitureSite){        		
                        window.location.href = '/en/search?query=' + term;
                    }else if(locale == "fr-CA" && !isFurnitureSite){
                        window.location.href = '/fr/search?query=' + term;
                    }else{
                        window.location.href = '/search?query=' + term;
                    }
                } else {
                    window.location.href = '/search?query=' + term;
                }
            }
        },
        getSuggestion: function(term){
            var _parent = this;
            if(term){
                // console.log('getSuggestion: term -->', term);
                if (/^\d{4}-\d{3}/.test(term)) {
                    var queryParts = term.replace(/ /g,'').replace(/,/g,'/').replace(/-/g,'/').split('/');
                    term = queryParts[0];
                    // console.log('getSuggestion: term (changed to) -->', term);
                }
                this.model.set("searchQuery", term);
                api.request("GET", "/api/commerce/catalog/storefront/productsearch/suggest?groups=pages,categories&pageSize=9&query="+ term).then(function(e){
                    if (_parent.hasSuggestions(e.suggestionGroups)){
                        var pageSuggestions = e.suggestionGroups[0].suggestions;
                        var articleTypeId = Hypr.getThemeSetting('articleProductTypeId');
                        hardwareSuggestions =pageSuggestions.filter(function(el) { 
                            return el.suggestion.productTypeId != articleTypeId; 
                        });
                         e.suggestionGroups[0].suggestions = hardwareSuggestions;
                         _.each(pageSuggestions, function(prod) {
                            var productCode =  prod.suggestion.productCode,
                            finalprodcode = productCode.slice(0, 4) + '-' + productCode.slice(4, 8);
                            prod.suggestion.modifiedProductCode = finalprodcode;
                        });
                        _parent.model.set("suggestions", e.suggestionGroups);
                        _parent.showSuggestions();
                    } else {
                        _parent.spellingSuggestion(term);
                    }
                });
            }
        },

        showSuggestions: function(){
            var _parent = this,
                suggestionTemplate = Hypr.getTemplate("modules/search/custom-autocomplete-search"),
                suggestions = this.model.get("suggestions");
            if(suggestions[0].suggestions.length || suggestions[1].suggestions.length){
                _.each(suggestions[1].suggestions, function(cat) {
                    var category = _.find(window.allCategories, function(item) {
                        return (cat.suggestion.categoryId === item.categoryId) && (item.parentCategory.categoryId === parseInt(Hypr.getThemeSetting('dynamicCategoryId'), 10));
                    });
                    if(category) {
                        cat.suggestion.isDynamicCategory = true;
                        console.log("Dyn category found. " + cat.suggestion.categoryId);
                    }

                    var isBrand = _.find(window.allCategories,function(item){
                        return (cat.suggestion.categoryId === item.categoryId) && (item.parentCategory.categoryId === parseInt(Hypr.getThemeSetting('brandsCategoryId'), 10));
                    });
                    if(isBrand) {
                        cat.suggestion.isBrandCategory = true;
                        console.log("Brand found. " + cat.suggestion.categoryId);
                    }

                });
               
                this.$el.find(".suggestedSearch").html(suggestionTemplate.render({model: {
                    suggestions: this.model.get("suggestions"),
                    searchQuery: this.model.get("searchQuery"),
                    currentLocale: locale,
                    currentSite: currentSite
                }}));
                if(hardwareSuggestions.length > 0){
                    _parent.$el.find(".suggestedSearch").removeClass('hidden');
                }
                if($(window).width() > 991){
                	var sugprodheight = $('.suggested-product-image').height();
                	$('.suggested-product-image').css({'height':sugprodheight});
                	var sugprodimgheight = $('.suggested-search-product-img').height();
                	$('.suggested-search-product-img').css({'height':sugprodimgheight});
                }
                setTimeout(function(){
                    if(hardwareSuggestions.length > 0){
                        _parent.$el.find(".suggestedSearch").removeClass('hidden');
                    }
                    $('body').addClass('no-scroll-mobile');
                }, 500);
            }
        },
        addTerm: function(event){
            var value = $(event.currentTarget).data("value");
            this.$el.find("input.mz-searchbox-input").val(value);
            this.$el.submit();
        }
    });

    $(document).ready(function (e) { 
        var Model = Backbone.MozuModel.extend();

        var autoComplete = new AutoComplete({
            el: $("form#searchbox"),
            model: new Model()
        });
        
        autoComplete.render();
        

        $(document).on("click", function(){
            $('body').removeClass("no-scroll-mobile");
        });
        $(".mz-searchbox").on("click", function(event){
            event.stopPropagation();
        });
        $("body").on("submit", "#searchbox", function(event){
            if(!$(event.currentTarget).find("input").val()){
                event.preventDefault();
            }
            else{
                $(".mz-searchbox-input").val($.trim($(".mz-searchbox-input").val()));
            }
        });

        function searchboxAfterNameDisplay(){
            var lgDiff = 0;
            var mdDiff = 0;
            if($(window).width() > 1380){
                var utilityContainerWidth1 = $(".utility-nav-width").width();
                var utilityWidth1 = $(".headerUtility").width();
                var searchWidth1 = utilityContainerWidth1 - utilityWidth1;
                if($(".headerUtility").hasClass('home-furniture-signin-width')){
                    searchWidth1 = searchWidth1;
                    lgDiff = 90;
                    }
                $(".mz-searchbox-field").width(searchWidth1);
            }

            if($(window).width() > 991 && $(window).width() < 1381) {
                var utilityContainerWidth2 = $(".utility-nav-width").width();
                    var utilityWidth2 = $(".headerUtility").width();
                    var searchWidth2 = utilityContainerWidth2 - utilityWidth2;
                    var furnitureDiff2 = 0;
                    if($(".headerUtility").hasClass('home-furniture-signin-width')){
                        searchWidth2 = searchWidth2;
                        lgDiff = 90;
                        mdDiff = 12;
                        furnitureDiff2 = 60;
                        }
                    $(".mz-searchbox-field").width(searchWidth2);
                    if($(window).width() > 991 && $(window).width() < 1025){
                        $(".mz-searchbox-field").width(searchWidth2 + furnitureDiff2);
                    }
            }
            if($(window).width() > 767 && $(window).width() < 992){
                var utilityContainerWidth3 = $(".utility-nav-width").width();
                    var utilityWidth3 = $(".headerUtility").width();
                    var searchWidth3 = utilityContainerWidth3 - utilityWidth3;
                    if($(".headerUtility").hasClass('home-furniture-signin-width')){
                        searchWidth3 = searchWidth3;
                        }
                    $(".mz-searchbox-field").width(searchWidth3);
            }
        }
        
        $('.search-close').on("click",function() {
    		$('#page-content').removeClass('addTopOnMobile addTopOnMobileAfterSearch removeBottom');
    		$('#desktop-search #searchbox').addClass("mz-searchbox-no-expands");
    		$('#desktop-search #searchbox').removeClass('active');
    	    $('#desktop-search .mz-searchbox-button').show();
    	    $('.nav-search-icon-text').css({'display':'block'});
    	    $('.nav-search-icon-text').removeClass('hidden');
    	    $('.mz-searchbox-input').css('display','none');
    	    $('.search-close').css('display','none');
    	    $('#searchbox .nav-search-icon-text').css('display','none');
    	    $('.suggestedSearch').addClass('hidden');
    	    $('#search_submit-close').removeClass('hidden');
            $('#search_submit').addClass('hidden');
    	});
    	setTimeout(function() {
    		$('linearGradient').find('stop[offset="1%"]').css('stop-color', '#c7c7c7');
    	}, 5000);
    	
    	$('#search_submit-close').on("click",function(e) {
    		$('#desktop-search #searchbox').removeClass('mz-searchbox-no-expands');
    		$('#desktop-search #searchbox').addClass("active");
    		$('.search-close').css('display','block');
    		$('.mz-searchbox-input').css('display','block');
            $('#searchbox .nav-search-icon-text').css('display','none');
            $('#search_submit-close').addClass('hidden');
            $(".nav-search-icon-text").addClass('hidden');

            searchboxAfterNameDisplay();

    	});
    	$('.mz-searchbox-input').keypress(function(){
    		$('#search_submit').removeClass('hidden');
    		$('#search_submit-close').addClass('hidden');
    	});

        $(document).on('click', '.tt-dataset-cat ul>li>a', function() { 
            var categoryId = $(this).parent().data('categoryId');           
            if(locale == "en-US"){              
                window.location.href = '/en/search?query=' + window.autocomplete + '&categoryId=' + categoryId;
            } else{
                window.location.href = '/fr/search?query=' + window.autocomplete + '&categoryId=' + categoryId;
            }
        });
        
        $(document).on('click', '.tt-dataset-cat>a', function() { 
            var categoryId = $(this).parent().data('categoryId');           
            if(locale == "en-US"){              
                window.location.href = '/en/search?query=' + window.autocomplete;
            } else{
                window.location.href = '/fr/search?query=' + window.autocomplete;
            }
        });

        var stopwords = [];
        if(locale == "fr-CA"){
            stopwords = ["un","une","an","&","et","sont","êtes","etes","comme","à","a","au","aux","e","chez","es","est", "mais","sauf","par","de","pour","car","si","dans","selon","is","le","la","il","elle","lui","ça","ca","aucun","sans","pas","ne","of","sur","ou","tel","telle","qui","ce","cette","ces","que","lequel","laquelle","afin","dont","les","des","leur","leurs","ensuite","alors","puis","après","apres","ainsi","donc","là","la","voici","voilà","voila","quel","quelle","quels","quelles","lesquels","lesquelles","celle","ce","celles","ceux","celui","ces","ils","elles","on","ceci","avec" ];
        }else {
            stopwords = ["a","an","and","&","are","as","at","be","but","by","for","if","in","into","is","it","no","not","of","on","or","such","that","the","their","then","there","these","they","this","to","was","will","with"];
        }
        $('.mz-searchbox').on('submit', function(e) {
            e.preventDefault();
            var $ele = $(e.currentTarget).find('#search-input');
            // var term = $ele.val().trim();
            // isProductCode = term && (term.length < 9) && (/\d{7}$/.test(term) || /\d{4}-\d{3}/.test(term));
            // // console.log('mz-searchbox submit: isProductCode -->', isProductCode);
            // if(isProductCode) {
            //     var isFurnitureSite = false;
            //     var contextSiteId = require.mozuData('apicontext').headers['x-vol-site'];
            //     if(Hypr.getThemeSetting("homeFurnitureSiteId") === contextSiteId){
            //         isFurnitureSite = true;
            //     }
            //     var redirectPath = '/p/' + term.replace(/-/g, '');
            //     if (isFurnitureSite) {
            //         window.location.href = redirectPath;
            //     } else {
            //         window.location.href = ((locale == "fr-CA") ? '/fr' : '/en') + redirectPath;
            //     }
            //     return;
            // }
            var searchKeyword = $ele.val().trim();
            if (ua.indexOf('chrome') > -1) {
                if (/edg|edge/i.test(ua)){
                    $.cookie('stopwords', encodeURIComponent(searchKeyword), {path:'/'});
                }else{
                    document.cookie = "stopwords="+encodeURIComponent(searchKeyword)+";path=/;Secure;SameSite=None";
                }
            } else {
                $.cookie('stopwords', encodeURIComponent(searchKeyword), {path:'/'});
            }
            /** Allowed pattern (Flyer Item#): 1730-103 to 185 */
            var isFlyerPattern = false;
            var queryParts = searchKeyword.split('to');
            if (queryParts.length == 2) {
                var leftPart = queryParts[0].trim();
                var rightPart = queryParts[1].trim();
                if (/\d{4}-\d{3}/.test(leftPart) && /\d{3}/.test(rightPart)) {
                    isFlyerPattern = true;
                }
            }
            removeStopwords(searchKeyword.toLowerCase(), isFlyerPattern);
            $(".mz-searchbox-input").val(searchKeyword);
            $("#search-input").val(searchKeyword);
        });

        function removeStopwords(searchKeyword, isFlyerPattern) {
            document.cookie = "QueryWithStopwords="+searchKeyword+";path=/;Secure;SameSite=None";
            var result = [];
            var keywords = searchKeyword.split(' ');
            _.each(keywords, function(keyword) {
                if((isFlyerPattern && keyword === 'to') || (stopwords.indexOf(keyword) === -1)) {
                    result.push(keyword);
                }
            });
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            locale = locale.split('-')[0];
            var currentLocale = '';
            if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
                currentLocale = locale === 'fr' ? '/fr' : '/en';
            }
            var removedchar = result.join('+');
            if(removedchar && removedchar.indexOf('%') > -1){
                removedchar = removedchar.split('%')[0].concat('%25')+removedchar.split('%')[1].trim();
            }

            if (ua.indexOf('chrome') > -1) {
                if (/edg|edge/i.test(ua)){
                    $.cookie('QueryWithoutStopwords', removedchar, {path:'/'});
                }else{
                    document.cookie = "QueryWithoutStopwords="+removedchar+";path=/;Secure;SameSite=None";
                }
            } else {
                $.cookie('QueryWithoutStopwords', removedchar, {path:'/'});
            }
            setTimeout(function(){
                if(!isProductCode){
                    document.location.href = currentLocale+"/search?query="+removedchar;
                }
            },500);
            
        }
    });
});