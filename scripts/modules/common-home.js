require([
    'modules/jquery-mozu',
    'underscore',
    'modules/api',
    'modules/backbone-mozu',
    'widgets/image-slider'
], function ($, _, api, Backbone, homeSliderView) {
	
	$('#mz-drop-zone-promotional-carousel .mz-cms-show-zone').bind('DOMNodeInserted', function(e) {
		window.homeSliderView.render();
	});
	 
});