define([
  'modules/jquery-mozu',
  'underscore',
  'modules/api',
  'modules/backbone-mozu',
  'slick'
], function ($, _, api, Backbone) {

var HomeSliderView = Backbone.View.extend({
      sliderFunction: function() {
          var $status = $('.widget-pro-programs .slider-paging');
          var $slickElement = $('.widget-pro-programs .widget-pro-programs-slider');

          $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
              //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
              var i = (currentSlide ? currentSlide : 0) + 1;
              $status.text(i + '/' + slick.slideCount);
          });

          $slickElement.slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              mobileFirst: true,
              centerMode: true,
              dots: false,
              arrows: true,
              prevArrow: $('.widget-pro-programs .slider-navigation--controls .fa-arrow-left'),
              nextArrow: $('.widget-pro-programs .slider-navigation--controls .fa-arrow-right'),
              responsive: [
                  {
                      breakpoint: 991,
                      settings: {
                          slidesToShow: 4,
                          centerMode: false,
                          dots: false,
                          arrows: false
                      }
                  }
              ]
          });
      },
      render: function() {
          this.sliderFunction();
      }
  });
  var homeSliderView = new HomeSliderView();
  homeSliderView.render();
  //window.homeSliderView = homeSliderView;
});