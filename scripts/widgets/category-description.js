define([
    'modules/jquery-mozu',
    'hyprlive'
], function ($,Hypr){
    var ua = navigator.userAgent.toLowerCase();
    $(document).on('click', '.toggle-btn', function(e){	
        $(e.currentTarget).closest(".description-container").toggleClass("is-collapsed");
        $(e.currentTarget).closest(".toggle-container").toggleClass("is-collapsed");
        var iscollapsed = $(".description-container").hasClass("is-collapsed");
        if(iscollapsed){
            $(e.currentTarget).attr("title" , Hypr.getLabel('readLess'));
            $(e.currentTarget).text(Hypr.getLabel('readLess'));
            $('.category-description p').first().addClass("line-clamp");
        }
        else{
            $(e.currentTarget).attr("title" , Hypr.getLabel('readMore'));
            $(e.currentTarget).text(Hypr.getLabel('readMore'));
            $('.category-description p').first().removeClass("line-clamp");
        }
    });
    
    //calculates description content height & hides less/more button
    var collapseSectionHeight = $(".description-content").height();
    var descriptionHeight = function(){
        $(".description-content").css('min-height','auto');
        $(".toggle-container").hide();
    };
    if($(window).width() < 768){
        if(collapseSectionHeight < 200){
            descriptionHeight();
        }
    }
    if($(window).width() > 767){
        if(collapseSectionHeight < 130){
            descriptionHeight();
        }
    }
    //end
    var numberOfParagarph = $('.category-description p').length;
    var characterCount = $(".category-description p").eq(0).text().length;
    var minCount;
    var isMobile = navigator.userAgent.match(/(iPhone)|(iPod)|(android)|(webOS)/i),
    isiPad = ua.indexOf('ipad') > -1 || ua.indexOf('macintosh') > -1 && 'ontouchend' in document;
    if(isiPad) {
        minCount = 385;
    }else if(isMobile){
        minCount = 200;
    }else{
        minCount = 448;
    }
    if(numberOfParagarph > 1 || characterCount > minCount){
        $(".description-container .toggle-container").css('display','block');
        $('.category-description p').first().removeClass("line-clamp");
    }else{
        $(".description-container .toggle-container").css('display','none');
        $('.category-description p').first().addClass("line-clamp");
    }

});