define([
    'modules/jquery-mozu',
    'underscore',
    'modules/api',
    'modules/backbone-mozu'
], function ($, _, api, Backbone) {

    var promoPosition;
    var pageContext = require.mozuData('pagecontext');
    var allPromotions = [];
    _.each($('.ign-banner-datalayer-promotion'), function (promotion) {
        console.log('promotion : ' + JSON.stringify(promotion));
        if (promotion) {
            if ($(promotion).attr('data-promo-position')) {
                promoPosition = $(promotion).attr('data-promo-name') + '-' + $(promotion).attr('data-promo-position');
            } else {
                promoPosition = $(promotion).attr('data-promo-name') + '-' + '1';
            }

            var nonStaticPages = ['home', 'search-results', 'my-account', 'signup', 'no-search-results', 'category', 'product', 'store-details', 'store-locator', 'wishlist', 'cart', 'checkout', 'confirmation'];

            var pagecontext = require.mozuData('pagecontext');
            var nonStaticPagePromoName = "";
            var nonStaticPagePromoId = "";
            _.find(nonStaticPages, function (pagename) {
                if (pagecontext.cmsContext.template.path === pagename) {
                    nonStaticPagePromoName = pageContext.cmsContext.template.path + " page - " + $(promotion).attr('data-promo-name');
                    nonStaticPagePromoId = pageContext.cmsContext.template.path + " page - " + $(promotion).attr('data-promo-id');
                }
            });

            if (!nonStaticPagePromoName) {
                var pageHeading = "";

                if ($('body').find('h1.title-for-print').length !== 0) {
                    pageHeading = $('body').find('h1.title-for-print').text();
                } else if ($('body').find("h1.main-page-title").length !== 0) {
                    pageHeading = $('body').find("h1.main-page-title").text();
                } else {
                    pageHeading = pagecontext.cmsContext.page.path;
                }
                nonStaticPagePromoName = pageHeading + ' - ' + $(promotion).attr('data-promo-name');
                nonStaticPagePromoId = pageHeading + ' - ' + $(promotion).attr('data-promo-id');
            }

            var promotionData = {
                'id': nonStaticPagePromoId,
                'name': nonStaticPagePromoName,
                'creative': $(promotion).attr('data-promo-datalayer-creative'),
                'position': promoPosition
            };
            allPromotions.push(promotionData);
        }
    });

    if (allPromotions && allPromotions.length > 0) {
        dataLayer.push({
            'event': 'promoview',
            'ecommerce': {
                'promoView': {
                    'promotions': allPromotions
                }
            }
        });
    }

    $(document).ready(function () {
        /**
         * Ticket - IWM-2023
         * @Yogeet
         * Below Code is used to replace extra questions marks from a url, after skipping 
         * first occurrence of question mark, replacing remaining question marks with & character. 
         */
        var formattedUrl, urls=$(".images-with-responsiveness a");
        $.each(urls,function(index,element){
            var urlArray = $(element).attr("href").split("?");
            urlArray.forEach(function(item,index){
                if(index === 0) {
                    formattedUrl = urlArray[0];
                
                } else if(index === 1){
                    formattedUrl+="?"+item;
                } else {
                    formattedUrl+="&"+item;
                    $(element).attr("href",formattedUrl);
                }
            });
        });
        $('.ign-banner-datalayer-promotion').click(function (e) {
            if ($(e.currentTarget).attr('data-promo-position')) {
                promoPosition = $(e.currentTarget).attr('data-promo-name') + '-' + $(e.currentTarget).attr('data-promo-position');
            } else {
                promoPosition = $(e.currentTarget).attr('data-promo-name') + '-' + '1';
            }

            var nonStaticPages = ['home', 'search-results', 'my-account', 'signup', 'no-search-results', 'category', 'product', 'store-details', 'store-locator', 'wishlist', 'cart', 'checkout', 'confirmation'];

            var pagecontext = require.mozuData('pagecontext');
            var nonStaticPagePromoName = "";
            var nonStaticPagePromoId = "";
            _.find(nonStaticPages, function (pagename) {
                if (pagecontext.cmsContext.template.path === pagename) {
                    nonStaticPagePromoName = pageContext.cmsContext.template.path + " page - " + $(e.currentTarget).attr('data-promo-name');
                    nonStaticPagePromoId = pageContext.cmsContext.template.path + " page - " + $(e.currentTarget).attr('data-promo-id');
                }
            });

            if (!nonStaticPagePromoName) {
                var pageHeading = "";

                if ($('body').find('h1.title-for-print').length !== 0) {
                    pageHeading = $('body').find('h1.title-for-print').text();
                } else if ($('body').find("h1.main-page-title").length !== 0) {
                    pageHeading = $('body').find("h1.main-page-title").text();
                } else {
                    pageHeading = pagecontext.cmsContext.page.path;
                }
                nonStaticPagePromoName = pageHeading + ' - ' + $(e.currentTarget).attr('data-promo-name');
                nonStaticPagePromoId = pageHeading + ' - ' + $(e.currentTarget).attr('data-promo-id');
            }

            var promotionData = {
                'id': nonStaticPagePromoId,
                'name': nonStaticPagePromoName,
                'creative': $(e.currentTarget).attr('data-promo-datalayer-creative'),
                'position': promoPosition
            };

            dataLayer.push({
                'event': 'promotionClick',
                'ecommerce': {
                    'promoClick': {
                        'promotions': [promotionData]
                    }
                }
            });
        });
    });

});
