require([
    'modules/jquery-mozu',
    'hyprlive'
], function ($,Hypr){
    var ua = navigator.userAgent.toLowerCase();
    $(document).on('click', '.categoryToggle', function(e){	
        $(e.currentTarget).closest(".description-container-outer").toggleClass("is-collapsed");
        $(e.currentTarget).closest(".category-toggle-container").toggleClass("is-collapsed");
        var iscollapsed = $(".description-container-outer").hasClass("is-collapsed");
        if(iscollapsed){
            $(e.currentTarget).attr("title" , Hypr.getLabel('readLess'));
            $(e.currentTarget).parent().addClass('readLessAlignment');
            $(e.currentTarget).text(Hypr.getLabel('readLess'));
            $(e.currentTarget).parent().siblings('div.description-container-inner').children().find('p').first().addClass("line-clamp");
        }
        else{
            $(e.currentTarget).parent().removeClass('readLessAlignment');
            $(e.currentTarget).attr("title" , Hypr.getLabel('readMore'));
            $(e.currentTarget).text(Hypr.getLabel('readMore'));
            $(e.currentTarget).parent().siblings('div.description-container-inner').children().find('p').first().removeClass("line-clamp");
        }
    });
    
    //calculates description content height & hides less/more button
    var collapseDescriptionHeight = $(".description").height();
    var categoryDescriptionHeight = function(){
        $(".description").css('min-height','auto');
        $(".category-toggle-container").hide();
    };
    if($(window).width() < 768){
        if(collapseDescriptionHeight < 200){
            categoryDescriptionHeight();
        }
    }
    if($(window).width() > 767){
        if(collapseDescriptionHeight < 130){
            categoryDescriptionHeight();
        }
    }
    //end
    var categoryDescriptionParagraphCount = $('.description-container-inner p').length;
    var categoryDescriptionCharacterCount = $(".description-container-inner p").eq(0).text().length;
    var categoryDescriptionMinCount;
    var isMobile = navigator.userAgent.match(/(iPhone)|(iPod)|(android)|(webOS)/i),
    isiPad = ua.indexOf('ipad') > -1 || ua.indexOf('macintosh') > -1 && 'ontouchend' in document;
    if(isiPad) {
        categoryDescriptionMinCount = 385;
    }else if(isMobile){
        categoryDescriptionMinCount = 200;
    }else{
        categoryDescriptionMinCount = 448;
    }
    if(categoryDescriptionParagraphCount > 1 || categoryDescriptionCharacterCount > categoryDescriptionMinCount){
        $(".description-container-outer .category-toggle-container").css('display','block');
        $('.description-container-inner p').first().removeClass("line-clamp");
    }else{
        $(".description-container-outer .category-toggle-container").css('display','none');
        $('.description-container-inner p').first().addClass("line-clamp");
    }

});