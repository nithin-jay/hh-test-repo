define([
    'modules/jquery-mozu'
], function ($){
    var pagecontext = require.mozuData('pagecontext');
    var mozuGridColumns = $('div').hasClass('mozuGridColumns');
    var imageWidgetArray, mozuGrid;
    if(pagecontext.pageType == "web_page" || pagecontext.cmsContext.template.path == "parent-category" || pagecontext.cmsContext.template.path == "lavelOneCategory" || pagecontext.pageType == "category" && mozuGridColumns) {
        imageWidgetArray = $('.mozuGridColumns');
        imageWidgetArray.each(function(index,item){
            mozuGrid = $(this).parents("div[class^='mz-cms-col-']");
            if(mozuGrid[0]) {
                $(this).parents("div[class^='mz-cms-col-']").parent().addClass('manageMobileDropZone manageDropZone');
                $(this).parents("div[class^='mz-cms-col-']").parent().addClass('flex flex-wrap');
                $(this).parents("div[class^='mz-cms-col-']").addClass('manageMobileDropZoneWidth');
                $(this).removeClass('hide-on-site');
            } else {
                $(this).parents('.mz-cms-content').parent().addClass('manageMobileDropZone manageDropZone');
                $(this).removeClass('hide-on-site');
            }
        });  
    }
});