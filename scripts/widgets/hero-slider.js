define([
    'modules/jquery-mozu',
    'underscore',
    'modules/api',
    'modules/backbone-mozu',
    'slick'
], function ($, _, api, Backbone) {

	var HomeSliderView = Backbone.View.extend({
        sliderFunction: function() {
            var $status = $('.widget-hero .slider-paging');
            var $slickElement = $('.widget-hero .hero-slider');
            var $slickTrigger = $('.slider-autoplay');

            $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
                //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
                var i = (currentSlide ? currentSlide : 0) + 1;
                $status.text(i + '/' + slick.slideCount);
            });

            $slickElement.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                mobileFirst: true,
                dots: false,
                arrows: true,
                autoplay: true,
                autoplaySpeed: 7000,
                fade: true,
                prevArrow: $('.widget-hero .slider-navigation--controls .fa-arrow-left'),
                nextArrow: $('.widget-hero .slider-navigation--controls .fa-arrow-right')
            });

            $slickTrigger.click(function() {
                if ($slickTrigger.hasClass('fa-pause')) {
                    $slickElement.slick('slickPause');
                    $slickTrigger.toggleClass('fa-pause');
                    $slickTrigger.toggleClass('fa-play');
                } else {
                    $slickElement.slick('slickPlay');
                    $slickTrigger.toggleClass('fa-pause');
                    $slickTrigger.toggleClass('fa-play');
                }

            });
        },
        render: function() {
            this.sliderFunction();
        }
    });
    var homeSliderView = new HomeSliderView();
    homeSliderView.render();
    //window.homeSliderView = homeSliderView;
});