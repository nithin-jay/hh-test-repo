define([
    'modules/jquery-mozu',
    'hyprlive',
    'underscore',
    'modules/api',
    'modules/backbone-mozu',
    'vendor/timezonesupport/timezonesupport',
    'slick'       
], function ($, Hypr, _, api, Backbone, timeZoneSupport) {

	var pageContext = require.mozuData('pagecontext');
	var apiContext = require.mozuData('apicontext');
	var contextSiteId = apiContext.headers['x-vol-site'];
	var HomeSliderView = Backbone.View.extend({
        initialize: function() {
            this.render();  
        },
        sliderFunction: function() {
            var divLength = $('.slider-first-item').size();  
            var sildeLoop = null;
            var slideSpeed = $(".mz-image-slider-container").attr('data-slide-speed');
            if(slideSpeed === ""){
                slideSpeed = 5000;
            }
            if(divLength > 1) {
                sildeLoop =  true;
            }
            else {
                sildeLoop = false;
            }
			 
			$(".mz-image-slider-container").find(".regular").removeClass("hidden");
			 
			var imgUrl, singleImgUrl;

			// _.each($(".mz-image-slider-container").find(".sliderSingleimg"),function(val,index){
			// 	singleImgUrl = $(val).attr("data-imgUrl");
			// 	$(val).attr("src", singleImgUrl);
			// 	if (singleImgUrl.split(".mozu")[0].split("-")[1] == "sb" || singleImgUrl.split(".mozu")[0].split("-")[1] == "tp1" || singleImgUrl.split(".mozu")[0].split("-")[1] == "tp2" ) {
			// 		singleImgUrl = Hypr.getThemeSetting("sirvImageUrl") + singleImgUrl.split("files/")[1];
			// 		$(val).css("background-image","url("+singleImgUrl+")");
			// 		$(val).attr("data-imgUrl", singleImgUrl);
			// 		$(val).attr("src", singleImgUrl);
			// 	} else {
			// 		$(val).css("background-image","url("+singleImgUrl+")");
			// 	}
			// });
			 
			
			// $(".mz-image-slider-container").find(".sliderimg").each(function(index,val){
			// 	imgUrl = $(val).attr("data-imgUrl");
			// 	 if (imgUrl.split(".mozu")[0].split("-")[1] == "sb" || imgUrl.split(".mozu")[0].split("-")[1] == "tp1" || imgUrl.split(".mozu")[0].split("-")[1] == "tp2" ) {
			// 		imgUrl = Hypr.getThemeSetting("sirvImageUrl")+imgUrl.split("files/")[1];
			// 		$(val).css("background-image","url("+imgUrl+")");
			// 		$(val).attr("data-imgUrl", imgUrl);
			// 	} else {
			// 		$(val).css("background-image","url("+imgUrl+")");
			// 	}	
			// });



            $(".regular").slick({
                dots: true,
                fade: false,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: slideSpeed,
                useTransform: true,
                cssEase: 'ease-in'
            });
            $(".productTile-title-link").addClass('add-ellipsis');
            $("#promotional-carousel").removeClass("initial-load-image");
            $("#promotional-carousel").fadeIn("slow");
        },
        render: function() {
			//commented on 30-07-2020 by payal, as per ticket IWM 202
        	// if(pageContext.cmsContext.page.path == 'home' ){
        		this.sliderFunction();
        	// }
        } 
    });

	var ImageSlideProductsView = Backbone.MozuView.extend({
		templateName: 'modules/product/image-slider-widget',
		initialize: function(){
			var self = this;
			var productsCodes = $(self.el).closest(".related-contents").attr("data-image-slider-product"),
			promoName = $(self.el).closest(".related-contents").attr("image-slide-promoname"),
			queryString = "",
			torontoTimeZone = timeZoneSupport.findTimeZone('America/Toronto'),
			today = new Date();

			if(productsCodes){
				productsCodes = productsCodes.split(",");
				productsCodes.forEach(function(item,index){
					if(index != productsCodes.length-1){
						queryString += "productCode eq "+item+" or ";
					}else{
						queryString += "productCode eq "+item;
					}
				});

				api.get("search",{filter: queryString}).then(function(productResponse){
					var products = productResponse.data.items;
					_.each(products,function(item){
						var promoItemZonedTime = timeZoneSupport.getZonedTime(new Date(item.updateDate), torontoTimeZone);
						var promoItemDate = new Date(promoItemZonedTime.year, promoItemZonedTime.month-1, promoItemZonedTime.day, promoItemZonedTime.hours, promoItemZonedTime.minutes, promoItemZonedTime.seconds, promoItemZonedTime.milliseconds);
						if(today <= promoItemDate){
							item.isPromoEnabled = true;
							item.promoTillDate = self.formatDate(promoItemDate);
						}
					});
					self.model.set('items', products);
					self.model.set("promoName", promoName);
					console.log(products);
					  self.render();
				});
			}
			
		    var homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId");
			if(homeFurnitureSiteId === contextSiteId){
				self.model.set('isFurnitureSite', true);
			}
			
		},
		formatDate: function(date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();
	
			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;
	
			return [year, month, day].join('-');
		}
	});

    var homeSliderView = new HomeSliderView();
    window.homeSliderView = homeSliderView;
    $(document).ready(function(){
    	var promoId, promoName, promoCreative, promoPosition;
    	var allPromotions = [];
		_.each($('.ign-datalayer-promotion.slick-slide[aria-describedby]'), function(promotion) {
		  if(promotion){
			 if($(promotion).attr('data-promo-position')) {
				 promoPosition = $(promotion).attr('data-promo-name') + '-' +  $(promotion).attr('data-promo-position');
			 }else {
				 promoPosition = $(promotion).attr('data-promo-name') + '-' + '1';
			 }
			 var promotionData = {
				'id': $(promotion).attr('data-promo-id'),
        	    'name': $(promotion).attr('data-promo-name'),
        	    'creative': $(promotion).attr('data-promo-datalayer-creative'),
        	    'position': promoPosition
  			  };
  			  allPromotions.push(promotionData);
		  }
  	  	});
		
  		if(allPromotions && allPromotions.length > 0) {
  			dataLayer.push({
  			  'event': 'promoview',
  			  'ecommerce': {
  			    'promoView': {
  			      'promotions': allPromotions
  			    }
  			  }
  			});
  		}
  		$('.ign-datalayer-promotion.slick-slide[aria-describedby]').click(function(e) {
  			if($(e.currentTarget).attr('data-promo-position')) {
				 promoPosition = $(e.currentTarget).attr('data-promo-name') + '-' +  $(e.currentTarget).attr('data-promo-position');
			 }else {
				 promoPosition = $(e.currentTarget).attr('data-promo-name') + '-' + '1';
			 }
  			var promotionData = {
				'id': $(e.currentTarget).attr('data-promo-id'),
	    	    'name': $(e.currentTarget).attr('data-promo-name'),
	    	    'creative': $(e.currentTarget).attr('data-promo-datalayer-creative'),
	    	    'position': promoPosition
  			};
  			
  			dataLayer.push({
  			  'event': 'promotionClick',
  			  'ecommerce': {
  			    'promoClick': {
  			      'promotions': [promotionData]
  			    }
  			  }
  			});
  		});
	}); 
	
	//commented on 05-08-2020 by payal, as per ticket IWM 202
	/*
	if(pageContext.cmsContext.page.path == 'home' ){
	*/	
		if(!localStorage.getItem('preferredStore')){
			renderImageSlider();
		}else{
			if(pageContext.purchaseLocation){
				renderImageSlider();
			}else{
				return {
					ImageSlideProductsView : ImageSlideProductsView
				};
			}	
		}
		/*
	}
	*/

	function renderImageSlider(){
		var numberOfBanner = $(".mz-image-slider-container .bannerimg ").not(".slick-cloned");

		_.each(numberOfBanner, function (element, index) {
			var renderEle = $(element).find(".image-slide-products-view");
			var imageSlideProductsView = new ImageSlideProductsView({
				el: $(renderEle),
				model: new Backbone.MozuModel()
			});	
			imageSlideProductsView.render();		
		});
	}
});
