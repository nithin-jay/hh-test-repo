define([
    'modules/jquery-mozu'
], function ($){
	$(document).ready(function(){
		//send data to GA,when grouping widget is clicked.
		$('.themed-content-grouping').click(function(e){
			e.stopPropagation();
		});
	});
});