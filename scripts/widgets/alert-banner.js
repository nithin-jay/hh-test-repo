define([
  'modules/jquery-mozu'
], function ($){

  function alertBanner(){
    // Check cookie 
    if ($.cookie('alertBanner') != "active") $('#alert-banner').slideDown(); 

    //Assign cookie on click
    $('#alert-banner--close').on('click',function(){
        $.cookie('alertBanner', 'active'); // cookie will expire after session
        $('#alert-banner').slideUp();
    });
  }

  $(document).ready(function(){
    alertBanner();
  });
});