define([
    'modules/jquery-mozu',
    'underscore',
    'modules/api',
    'modules/backbone-mozu',
    'slick'
], function ($, _, api, Backbone) {

    var HomeExclusiveSliderView = Backbone.View.extend({
        /**
         * Home Exculsive Service Up Card 2
         */
        sliderFunction2: function() {
            var $status = $('.HES-up-card-2-container .HES-slider-paging');
            var $slickElement = $('.HES-up-card-2-container .HES-mobile-outer');

            $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
                //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
                var i = (currentSlide ? currentSlide : 0) + 1;
                $status.text(i + '/' + slick.slideCount);
            });

            $slickElement.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                mobileFirst: true,
                centerMode: true,
                dots: false,
                arrows: true,
                prevArrow: $('.HES-up-card-2-container .slider-navigation--controls .fa-arrow-left'),
                nextArrow: $('.HES-up-card-2-container .slider-navigation--controls .fa-arrow-right'),
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 2,
                            centerMode: false,
                            dots: false,
                            arrows: false
                        }
                    }
                ]
            });
        },
        /**
         * Home Exculsive Service Up Card 3
         */
        sliderFunction3: function() {
            var $status = $('.HES-up-card-3-container .HES-slider-paging');
            var $slickElement = $('.HES-up-card-3-container .HES-mobile-outer');

            $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
                //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
                var i = (currentSlide ? currentSlide : 0) + 1;
                $status.text(i + '/' + slick.slideCount);
            });

            $slickElement.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                mobileFirst: true,
                centerMode: true,
                dots: false,
                arrows: true,
                prevArrow: $('.HES-up-card-3-container .slider-navigation--controls .fa-arrow-left'),
                nextArrow: $('.HES-up-card-3-container .slider-navigation--controls .fa-arrow-right'),
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 3,
                            centerMode: false,
                            dots: false,
                            arrows: false
                        }
                    }
                ]
            });
        },
        render: function() {
            this.sliderFunction2();
            this.sliderFunction3();
        }
    });
    var homeExclusiveSliderView = new HomeExclusiveSliderView();
    homeExclusiveSliderView.render();
});