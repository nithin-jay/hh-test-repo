define([
    'modules/jquery-mozu',
    'underscore',
    'modules/api',
    'modules/backbone-mozu',
    'slick'       
], function ($, _, api, Backbone) {

	var HomeSliderView = Backbone.View.extend({
        sliderFunction: function() {
            $(".brandregular").removeClass('hidden');
            $(".brandregular").slick({
                dots: true,
                infinite: true,
                slidesToShow: 6,
                slidesToScroll: 6,
                responsive: [
                    {
                      breakpoint: 767,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                      }
                    },
                    {
                        breakpoint: 1023,
                        settings: {
                          slidesToShow: 5,
                          slidesToScroll: 5
                        }
                    }
                ]
            });
        },
        render: function() {
            this.sliderFunction();
        } 
    });
    var homeSliderView = new HomeSliderView();
    homeSliderView.render();
    //window.homeSliderView = homeSliderView;
});