require(['modules/jquery-mozu', 'hyprlive', 'twbspagination'],function ($, Hypr, twbsPagination) {
	$(document).ready(function() {
		
		var videosPagesLength = $('#hhHistory').attr('data-mz-videoLength');
		console.log(videosPagesLength);
		$('#pagination-demo').twbsPagination({
			totalPages: videosPagesLength,
			// the current page that show on start
			startPage: 1,

			// maximum visible pages
			visiblePages: videosPagesLength > 6 ? 6 : videosPagesLength,

			initiateStartPageClick: true,

			// template for pagination links
			href: false,

			// variable name in href template for page number
			hrefVariable: '{{number}}',

			// Text labels
			first: '',
			prev: Hypr.getLabel('previous'),
			next: Hypr.getLabel('next'),
			last: '',

			// carousel-style pagination
			loop: false,

			// callback function
			onPageClick: function (event, page) {
				$('.page-active').removeClass('page-active');
			  $('#page'+page).addClass('page-active');
			},

			// pagination Classes
			paginationClass: 'pagination',
			nextClass: 'next',
			prevClass: 'prev',
			lastClass: '',
			firstClass: '',
			pageClass: 'page',
			activeClass: 'active',
			disabledClass: 'disabled'
		});
	});
});