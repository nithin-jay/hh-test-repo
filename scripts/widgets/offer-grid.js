define([
    'modules/jquery-mozu',
    'underscore',
    'modules/api',
    'modules/backbone-mozu',
    'slick'
], function ($, _, api, Backbone) {

	var HomeSliderView = Backbone.View.extend({
        sliderFunction: function() {
            var $status = $('.offer-grid-4 .slider-paging');
            var $slickElement = $('.offer-grid-4 .offer-grid-slider');

            $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
                //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
                var i = (currentSlide ? currentSlide : 0) + 1;
                $status.text(i + '/' + slick.slideCount);
            });

            $slickElement.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                mobileFirst: true,
                centerMode: true,
                dots: false,
                arrows: true,
                prevArrow: $('.offer-grid-4 .slider-navigation--controls .fa-arrow-left'),
                nextArrow: $('.offer-grid-4 .slider-navigation--controls .fa-arrow-right'),
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 4,
                            centerMode: false,
                            dots: false,
                            arrows: false
                        }
                    }
                ]
            });
        },
        sliderFunction3: function() {
            var $status = $('.offer-grid-3 .slider-paging');
            var $slickElement = $('.offer-grid-3 .offer-grid-slider');

            $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
                //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
                var i = (currentSlide ? currentSlide : 0) + 1;
                $status.text(i + '/' + slick.slideCount);
            });

            $slickElement.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                mobileFirst: true,
                centerMode: true,
                dots: false,
                arrows: true,
                prevArrow: $('.offer-grid-3 .slider-navigation--controls .fa-arrow-left'),
                nextArrow: $('.offer-grid-3 .slider-navigation--controls .fa-arrow-right'),
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 3,
                            centerMode: false,
                            dots: false,
                            arrows: false
                        }
                    }
                ]
            });
        },
        render: function() {
            this.sliderFunction();
            this.sliderFunction3();
        }
    });
    var homeSliderView = new HomeSliderView();
    homeSliderView.render();
    //window.homeSliderView = homeSliderView;
});