define([
    'modules/jquery-mozu'
], function ($){
	$(document).ready(function(){
		//send data to GA,when featured brand widget is clicked.
		$('.featured-brand-link').click(function(e){
			e.stopPropagation();
		});
	});
});