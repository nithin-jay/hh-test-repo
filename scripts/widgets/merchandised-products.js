define([
	'modules/jquery-mozu',
	'underscore',
	'vendor/timezonesupport/timezonesupport',
	"modules/backbone-mozu",
	'modules/api'

], function ($, _, timeZoneSupport, Backbone,api){

	var pageContext = require.mozuData('pagecontext');
	var apiContext = require.mozuData('apicontext');
	var MerchandisedProductView = Backbone.MozuView.extend({
		templateName: 'modules/product/merchandised-productlive',
        initialize: function(){
			var self = this;
			var productsCodes =  $(self.el).attr("data-merchandised-product"),
			queryString = "",
			promoName = $(self.el).attr("data-merchandised-promo"),
			torontoTimeZone = timeZoneSupport.findTimeZone('America/Toronto'),
			today = new Date();
		    productsCodes = productsCodes.split(",");
		
		    productsCodes.forEach(function(item,index){
			  if(index != productsCodes.length-1){
				queryString += "productCode eq "+item+" or ";
			   }else{
				queryString += "productCode eq "+item;
			  }
			});
			if(queryString){
				api.get("search",{filter: queryString}).then(function(productResponse){
					var products = productResponse.data.items;
					_.each(products,function(item){
						var promoItemZonedTime = timeZoneSupport.getZonedTime(new Date(item.updateDate), torontoTimeZone);
						var promoItemDate = new Date(promoItemZonedTime.year, promoItemZonedTime.month-1, promoItemZonedTime.day, promoItemZonedTime.hours, promoItemZonedTime.minutes, promoItemZonedTime.seconds, promoItemZonedTime.milliseconds);
						if(today <= promoItemDate){
							item.isPromoEnabled = true;
							item.promoTillDate = self.formatDate(promoItemDate);
						}
					});
					self.model.set('items', products);
					console.log(products);
					self.model.set("promoName", promoName);
					self.model.set("isMerchandisedProduct", true);
				self.render();
				});
			}
		},
		formatDate:function(date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();
	
			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;
	
			return [year, month, day].join('-');
		}
	});
	

	$(document).ready(function(){
		$("#merchandised-product-slider").slick({ 
			dots: false,
			infinite: true,
			slidesToShow: 2,
			slidesToScroll: 1,
			variableWidth:true,
			mobileFirst: true,
			responsive: [{
				breakpoint: 769,
                settings: 'unslick'
			}]
		});
		if($(".poppular-categories").find(".merchandised-product-widet-view")){
			$(".poppular-categories").find(".col-sm-12").removeClass("col-sm-12");
		}
		$(window).on('resize', function() {
			if(screen.width<768) {
				if($('#merchandised-product-slider').length > 0){
					$('#merchandised-product-slider').slick('resize');
				}
			}
		});
		//send data to GA,when merchandised products widget is clicked.
		$('.merchandises-products .image-wrap, .merchandises-products .mz-productlisting-title').click(function(e){
			e.stopPropagation();
		});
	});

	
	if(!localStorage.getItem('preferredStore')){
		renderMerchandisedWidget();
	}else{
		if(pageContext.purchaseLocation){
			renderMerchandisedWidget();
		}else{
			return {
				MerchandisedProductViews : MerchandisedProductView
			};
		}	
	}
	
	function renderMerchandisedWidget(){
		var numMerWidgets = $(".merchandises-products");

		_.each(numMerWidgets, function (element, index) {
			var renderEle = $(element).find(".merchandised-product-widet-view");
			var merchandisedProductView = new MerchandisedProductView({
				el: $(renderEle),
				model: new Backbone.MozuModel()
			});
			var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
			merchandisedProductView.model.set("currentSite",currentSite);
			merchandisedProductView.render();	
		});	
	}
});