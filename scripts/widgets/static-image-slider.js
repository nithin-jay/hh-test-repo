define([
    'modules/jquery-mozu',
    'underscore',
    'modules/api',
    'modules/backbone-mozu',
    'slick'       
], function ($, _, api, Backbone) {

	var HomeSliderView = Backbone.View.extend({
        initialize: function() {
            this.render();  
        },
        sliderFunction: function() {
            var divLength = $('.slider-first-item').size();  
            var sildeLoop = null;
            if(divLength > 1) {
                sildeLoop =  true;
            }
            else {
                sildeLoop = false;
            }
            $(".static-images").removeClass('hidden');
            $(".static-images").slick({
                dots: true,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1
            });
        },  
        render: function() {
            this.sliderFunction();
        } 
    });
        
        var homeSliderView = new HomeSliderView();
        window.homeSliderView = homeSliderView;

});
