require(['modules/jquery-mozu', 'hyprlive', 'twbspagination'],function ($, Hypr, twbsPagination) {
	$(document).ready(function() {
		$('.video-tile-image, .video-tile-link').click(function(e) {
			e.preventDefault();
			var videoLink = $(e.currentTarget).attr('href');
			var videoDesc = $(e.currentTarget).attr('data-mz-desc');
			$('#main-video').attr('src', videoLink); 
			$('#videoDesc').text(videoDesc);
		});
		var videosPagesLength = $('#relatedVideos').attr('data-mz-videoLength');
		$('#pagination-demo').twbsPagination({
			totalPages: videosPagesLength,
			// the current page that show on start
			startPage: 1,

			// maximum visible pages
			visiblePages: videosPagesLength > 6 ? 6 : videosPagesLength,

			initiateStartPageClick: true,

			// template for pagination links
			href: false,

			// variable name in href template for page number
			hrefVariable: '{{number}}',

			// Text labels
			first: '',
			prev: Hypr.getLabel('previous'),
			next: Hypr.getLabel('next'),
			last: '',

			// carousel-style pagination
			loop: false,

			// callback function
			onPageClick: function (event, page) {
				$('.page-active').removeClass('page-active');
			  $('#page'+page).addClass('page-active');
			},

			// pagination Classes
			paginationClass: 'pagination',
			nextClass: 'next',
			prevClass: 'prev',
			lastClass: '',
			firstClass: '',
			pageClass: 'page',
			activeClass: 'active',
			disabledClass: 'disabled'
		});
	});
});