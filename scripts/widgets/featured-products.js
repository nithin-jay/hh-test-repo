define([
	'modules/jquery-mozu',
	'underscore',
	'vendor/timezonesupport/timezonesupport',
	"modules/backbone-mozu",
	'modules/api'
], function ($, _,timeZoneSupport,Backbone,api){
	
	var pageContext = require.mozuData('pagecontext'),
	ua = navigator.userAgent.toLowerCase(),
	apiContext = require.mozuData('apicontext');
	var FeartureProductsView = Backbone.MozuView.extend({
		templateName: 'modules/product/product-list-tiled',
        initialize: function(){
			var self = this;
			var productsCodes = $(self.el).attr("data-feature-products"),
			queryString = "",
			promoName = $(self.el).attr("data-feature-widget-promo"),
			templateName = $(self.el).attr("data-feature-products-template"),
			torontoTimeZone = timeZoneSupport.findTimeZone('America/Toronto'),
			today = new Date();
		    productsCodes = productsCodes.split(",");
			
			if(templateName){
				self.templateName = templateName;
			}
				
		    productsCodes.forEach(function(item,index){
			  if(index != productsCodes.length-1){
				queryString += "productCode eq "+item+" or ";
			   }else{
				queryString += "productCode eq "+item;
			  }
			});
			if(queryString){
				api.get("search",{filter: queryString}).then(function(productResponse){
					var products = productResponse.data.items;
					_.each(products,function(item){
						var promoItemZonedTime = timeZoneSupport.getZonedTime(new Date(item.updateDate), torontoTimeZone);
						var promoItemDate = new Date(promoItemZonedTime.year, promoItemZonedTime.month-1, promoItemZonedTime.day, promoItemZonedTime.hours, promoItemZonedTime.minutes, promoItemZonedTime.seconds, promoItemZonedTime.milliseconds);
						if(today <= promoItemDate){
							item.isPromoEnabled = true;
							item.promoTillDate = self.formatDate(promoItemDate);
						}
					});
					self.model.set("items", products);
					console.log(products);
					self.model.set("promoName", promoName);
					self.model.set("isWidget",true);
					var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
					self.model.set("currentSite",currentSite);
					self.render();
				});
			}
		},
		formatDate:function(date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();
	
			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;
	
			return [year, month, day].join('-');
		}
	});
	
	$(document).ready(function(){
		//send data to GA,when featured brand widget is clicked.
		$('.featured-products-widget .image-wrap, .featured-products-widget .mz-productlisting-title').click(function(e){
			e.stopPropagation();
		});

	});
	
	if(!localStorage.getItem('preferredStore')){
		renderFeatureProductWidget();
	}else{
		if(pageContext.purchaseLocation){
			renderFeatureProductWidget();
		}else{
			return {
				FeartureProductsViews : FeartureProductsView
			};
		}	
	}
	
	function renderFeatureProductWidget(){
		var numFeatureWidgets = $(".featured-products-widget");
		_.each(numFeatureWidgets, function (element, index) {
			var renderEle = $(element).find(".feature-product-widget-view");
			var feartureProductsView = new FeartureProductsView({
				el: $(renderEle),
				model: new Backbone.MozuModel()
			});
			feartureProductsView.render();	
		});	
	}
});