require([
		'modules/jquery-mozu',
		'hyprlive',
		"underscore",
		'modules/backbone-mozu',
		'hyprlivecontext',
		'modules/api',
		'modules/models-cart',
		'modules/models-customer',
		'moment',
		'modules/cookie-utils'
	],
	function ($, Hypr, _, Backbone, HyprLiveContext, api, CartModels, CustomerModels, moment, CookieUtils) {

		/*Page Context*/
		var pageContext = require.mozuData('pagecontext'),
			apiContext = require.mozuData('apicontext'),
			user = require.mozuData('user'),
			contextSiteId = apiContext.headers["x-vol-site"],
			homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId");

		var map, storeToChange, markers = [],
			search = null,
			today = new Date(),
			statusFilter = false,
			checkedFilters = [],
			locationList = Hypr.getThemeSetting("storeDocumentListFQN"),
			distanceFilters = Hypr.getThemeSetting("distanceFilters"),
			days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
			isHomeFurnitureSite = false,
			searchStoreFlag = false,
			searchBasedLatLong,
			cityChanges = false,
			prevCity = "",
			storeCitiesFQN = Hypr.getThemeSetting("storeCitiesListFQN"),
			itemsInCart = [],
			ua = navigator.userAgent.toLowerCase(),
			filtersToCheck,
			isFilterChecked = true;

		var filtersString = "",
			returnParam = "",
			storeView;

		/* Filter for home furniture site */
		if (homeFurnitureSiteId === contextSiteId) {
			filtersString = "properties.storeType in['Home Furniture']";
			isHomeFurnitureSite = true;
		} else {
			if (window.location.search.indexOf('storeFilter') < 0)
				filtersString = "properties.storeType in['Home Hardware','Home Building Centre', 'Home Hardware Building Centre'] ";
		}

		/* store filter from static page i.e home install and commercial maintenance */
		if (window.location.search.indexOf('storeService') > 0) {
			returnParam = (window.location.search.split('storeService=')[1] || '');
			var serviceFilterString = decodeURIComponent(returnParam) === "Home Installs" ? "properties.storeServices in['Home Installs']" : "properties.commercialMaintenance eq true ";
			if(decodeURIComponent(returnParam) === "Top Notch Rewards"){
				serviceFilterString = "properties.storeServices in['Top Notch Rewards']";
			}
			if(decodeURIComponent(returnParam) === "Home Rentals"){
				serviceFilterString = "properties.homeRentals eq true";
			}
			if (!filtersString) {
				filtersString = serviceFilterString;
			} else {
				filtersString += " and " + serviceFilterString;
			}
		}

		//for nosearch result link
		if (window.location.search.indexOf('storeType') > 0) {
			returnParam = (window.location.search.split('storeType=')[1] || '');
			var typeFilterString = decodeURIComponent(returnParam);
			if(typeFilterString == "HBC and HHBC"){
				if (!filtersString) {
					filtersString = "properties.storeType in['Home Building Centre']";
				} else {
					filtersString += " and " + "properties.storeType in['Home Building Centre']"+" or " + "properties.storeType in['Home Hardware Building Centre']";
				}
			}
		}

		/* store filters for HBC and HF from PDP */
		if (window.location.search.indexOf('storeFilter') > 0) {
			returnParam = (window.location.search.split('storeFilter=')[1] || '');
			if (returnParam == "HBC") {
				if (!filtersString) {
					filtersString = "properties.storeType in['Home Building Centre']";
				} else {
					filtersString += " and " + "properties.storeType in['Home Building Centre']";
				}
			}
		}

		/* Adding display online filter */
		if (!filtersString) {
			filtersString = "properties.displayOnline eq true";
		} else {
			filtersString += " and properties.displayOnline eq true";
		}
		
		/*store view starts here*/
		var StoreView = Backbone.MozuView.extend({
			templateName: "modules/location/store-locations",
			additionalEvents: {
				"focus input[id='postalCode']": "onSearchFocusBlur",
				"blur input[id='postalCode']": "onSearchFocusBlur",
				"mousedown #useMyLocation": "searchOnCurrentLocation",
				"click [id='applyFilters']": "applyFilters",
				"keypress input[id='postalCode']": "findStores",
				"keypress input[id='city']": "findStores",
				"focus select[id='province']": "onSearchFocusBlur",
				// "focus input[id='city']": "onSearchFocusBlur",
				"keyup input[id='city']": "populateCities"
			},
			render: function () {
				var me = this;
				var stores = me.model.get("stores");
				if (localStorage.getItem('preferredStore')) {
					var preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
					var currentStore = _.findWhere(stores, {
						name: preferredStore.code
					});
					if (currentStore) {
						currentStore.isPreferresStore = true;
						me.model.set("isPreferresStore", true);
					}
				}
				if (search) {
					var searchObject = {};
					search = search.value ? search.value : search;
					if (search.indexOf(",") > -1) {
						var searchString = search.split(",");
						searchObject = {
							"city": searchString[0].trim(),
							"province": searchString[1].trim()
						};
						me.model.set("search", searchObject);
					} else {
						searchObject = {
							"postalCode": search
						};
						me.model.set("search", searchObject);
					}
				}
				if (window.location.search.indexOf('storeService') > 0) {
					returnParam = (window.location.search.split('storeService=')[1] || '');
					var pageUrl = decodeURIComponent(returnParam) === "Home Installs" ? "Home Installs" : "commercial maintenance";
					if(decodeURIComponent(returnParam) === "Top Notch Rewards"){
						pageUrl = "Top Notch Rewards";
					}
					if(decodeURIComponent(returnParam) === "Home Rentals"){
						pageUrl = "Home Rentals";
					}
					me.model.set("pageUrl", pageUrl);
					me.model.set("isHomeInstComMain", true);
				}
				me.setStoreFilters();
				Backbone.MozuView.prototype.render.apply(this, arguments);
				me.loadMap();
			},
			setStoreFilters: function () {
				var me = this;
				var stores = me.model.get("stores");
				var storeServices = [],
					storeTypes = [],
					hfServices = [],
					storeFilters,
					contextSiteId = apiContext.headers["x-vol-site"],
					homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId");
				/*creating service filters array*/

				if (Hypr.getThemeSetting('frSiteId') === contextSiteId) {
					_.each(stores, function (store) {
						_.each(store.properties.storeServices, function (service) {
							if (service) {
								var frenchValue = me.getFrenchTranslation(service);
								var exist = _.findWhere(storeServices, {
									"serviceName": service,
									"serviceValue": frenchValue
								});
								if (!exist) {
									storeServices.push({
										"serviceName": service,
										"serviceValue": frenchValue
									});
								}
							}
						});
					});
				} else {
					_.each(stores, function (store) {
						_.each(store.properties.storeServices, function (service) {
							if (service) {
								var exist = _.findWhere(storeServices, {
									"serviceName": service,
									"serviceValue": service
								});
								if (!exist) {
									storeServices.push({
										"serviceName": service,
										"serviceValue": service
									});
								}
							}
						});
					});
				}

				storeServices = _.sortBy(storeServices, "serviceName");

				if (homeFurnitureSiteId === contextSiteId) {
					/*creating hf services filters array*/
					_.each(stores, function (store) {
						_.each(store.properties.homeFurnitureServices, function (service) {
							if (service) {
								var exist = _.contains(hfServices, service);
								if (!exist) {
									hfServices.push(service);
								}
							}
						});
					});
					hfServices = _.sortBy(hfServices);
					storeFilters = {
						distanceFilters: distanceFilters,
						hfServices: hfServices,
						storeServices: storeServices
					};
					me.model.set("isHomeFurnitureSite", true);
				} else {

					/*creating type filters array*/

					if (Hypr.getThemeSetting('frSiteId') === contextSiteId) {
						_.each(stores, function (store) {
							_.each(store.properties.storeType, function (type) {
								if (type) {
									var frenchType = me.getFrenchTypeTranslation(type);
									var exist = _.findWhere(storeTypes, {
										"storeType": type,
										"storeTypeValue": frenchType
									});
									if (!exist) {
										storeTypes.push({
											"storeType": type,
											"storeTypeValue": frenchType
										});
									}
								}
							});
						});
					} else {
						_.each(stores, function (store) {
							_.each(store.properties.storeType, function (type) {
								if (type) {
									var exist = _.findWhere(storeTypes, {
										"storeType": type,
										"storeTypeValue": type
									});
									if (!exist) {
										storeTypes.push({
											"storeType": type,
											"storeTypeValue": type
										});
									}
								}
							});
						});
					}
					storeTypes = _.sortBy(storeTypes, "storeType");

					storeFilters = {
						storeServices: storeServices,
						storeTypes: storeTypes,
						distanceFilters: distanceFilters
					};
					if (window.location.search.indexOf('storeService') > 0) {
						returnParam = (window.location.search.split('storeService=')[1] || '');
						_.each(storeFilters.storeServices,function(service){
							if(decodeURIComponent(returnParam) === service.serviceName ){
								filtersToCheck = service.serviceName ? service.serviceName.toLowerCase().replace(/ /g,"-"):"";
							}
						});
						if(decodeURIComponent(returnParam) === "Home Rentals"){
							filtersToCheck = 'rentals';
						}
					} 
					me.model.set("isHomeFurnitureSite", false);
				}

				me.model.set("storeFilters", storeFilters);
				if (checkedFilters && checkedFilters.length > 0) {
					var length = '(' + checkedFilters.length + ')';
					me.model.set("numOFFilters", length);
				} else {
					me.model.set("numOFFilters", "");
				}
			},
			makeMyStore: function (e) {
				e.stopPropagation();
				var me = this;
				var currentTarget = e.currentTarget;
				storeToChange = $(currentTarget).data("mzStore");
				var cartModelData = new CartModels.Cart();
				var cartCount = null;
				if (window.location.search.indexOf('storeService') > 0) {
					returnParam = (window.location.search.split('storeService=')[1] || '');
					var queryString = "";
					if (decodeURIComponent(returnParam) === "Home Installs") {
						queryString = "properties.documentKey eq " + storeToChange;
						$("#content-loading").show();
						api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + queryString + '&pageSize=1100', {
							cache: false
						}).then(function (response) {
							if (response && response.items.length > 0) {
								var storeDetails = response.items[0];
								var homeInstallStoreInfo = {
									"code": storeDetails.name,
									"name": storeDetails.properties.storeName,
									"homeInstallEmail": storeDetails.properties.homeInstallsEmail,
									"address1": storeDetails.properties.address1,
									"address2": storeDetails.properties.address2,
									"city": storeDetails.properties.city,
									"province": storeDetails.properties.province[0],
									"postalCode": storeDetails.properties.postalCode,
									"phone": storeDetails.properties.phone
								};

								if (ua.indexOf('chrome') > -1) {
									if (/edg|edge/i.test(ua)){
										$.cookie("homeInstallStore", JSON.stringify(homeInstallStoreInfo), {
											path: '/'
										});
									}else{
										document.cookie = "homeInstallStore="+ JSON.stringify(homeInstallStoreInfo)+";path=/;Secure;SameSite=None";
									}
								} else {
									$.cookie("homeInstallStore", JSON.stringify(homeInstallStoreInfo), {
										path: '/'
									});
								}

								$("#content-loading").show();
								window.location.href = "/home-install";
							}
						});
					} else if (decodeURIComponent(returnParam) === "Home Rentals") {
						queryString = "properties.documentKey eq " + storeToChange;
						$("#content-loading").show();
						api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + queryString + '&pageSize=1100', {
							cache: false
						}).then(function (response) {
							if (response && response.items.length > 0) {
								var storeDetails = response.items[0];
								var homeRentalsStoreInfo = {
									"code": storeDetails.name,
									"name": storeDetails.properties.storeName,
									"homeRentalsEmail": storeDetails.properties.homeRentalsEmail,
									"address1": storeDetails.properties.address1,
									"address2": storeDetails.properties.address2,
									"city": storeDetails.properties.city,
									"province": storeDetails.properties.province[0],
									"postalCode": storeDetails.properties.postalCode,
									"phone": storeDetails.properties.phone
								};

								if (ua.indexOf('chrome') > -1) {
									if (/edg|edge/i.test(ua)){
										$.cookie("homeRentalsStore", JSON.stringify(homeRentalsStoreInfo), {
											path: '/'
										});
									}else{
										document.cookie = "homeRentalsStore="+ JSON.stringify(homeRentalsStoreInfo)+";path=/;Secure;SameSite=None";
									}
								} else {
									$.cookie("homeRentalsStore", JSON.stringify(homeRentalsStoreInfo), {
										path: '/'
									});
								}
								

								$("#content-loading").show();
								window.location.href = "/home-rental";
							}
						});
					} else if(decodeURIComponent(returnParam) === "commercial maintenance"){
						queryString = "properties.documentKey eq " + storeToChange;
						$("#content-loading").show();
						api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + queryString + '&pageSize=1100', {
							cache: false
						}).then(function (response) {
							if (response && response.items.length > 0) {
								var storeDetails = response.items[0];
								var commercialMaintenanceStore = {
									"code": storeDetails.name,
									"commercialMaintenanceEmail": storeDetails.properties.commercialMaintenanceEmail,
									"name": storeDetails.properties.storeName,
									"address1": storeDetails.properties.address1,
									"address2": storeDetails.properties.address2,
									"city": storeDetails.properties.city,
									"province": storeDetails.properties.province[0],
									"postalCode": storeDetails.properties.postalCode,
									"phone": storeDetails.properties.phone
								};

								if (ua.indexOf('chrome') > -1) {
									if (/edg|edge/i.test(ua)){
										$.cookie("commercialMaintenanceStore", JSON.stringify(commercialMaintenanceStore), {
											path: '/'
										});
									}else{
										document.cookie = "commercialMaintenanceStore="+ JSON.stringify(commercialMaintenanceStore)+";path=/;Secure;SameSite=None";
									}
								} else {
									$.cookie("commercialMaintenanceStore", JSON.stringify(commercialMaintenanceStore), {
										path: '/'
									});
								}
								$("#content-loading").show();
								window.location.href = "/commercial-maintenance";
							}
						});
					}else{
						var locale = apiContext.headers['x-vol-locale'];
						locale = locale.split('-')[0];
						var currentLocale = '';
						if (Hypr.getThemeSetting('homeFurnitureSiteId') != contextSiteId){
							currentLocale = locale === 'fr' ? '/fr' : '/en';
						}
						if(decodeURIComponent(returnParam) === "Top Notch Rewards"){
							window.location.href = currentLocale+'/store/'+storeToChange;
						}
					}
				} else {
					me.handleMakeThisMyStore(cartModelData,cartCount,storeToChange);
				}
			},
			handleMakeThisMyStore: function(cartModelData,cartCount,storeToChange){
				var me = this;
				cartModelData.on('sync', function () {
					cartCount = cartModelData.count();
				});
				cartModelData.apiGet().then(function (response) {
					if (cartCount > 0) {
                        itemsInCart = response.data.items;
						api.get("location", {
							code: storeToChange
						}).then(function (response) {
							var selectedStore = response.data;
							var isEcomStore = _.findWhere(selectedStore.attributes, {
								"fullyQualifiedName": Hypr.getThemeSetting("isEcomStore")
							});
							if (isEcomStore && isEcomStore.values[0] === 'Y') {
								$('#store-change-confirmation-modal').modal().show();
							} else {
								$('#non-ecom-store-confirmation-modal').modal().show();
							}
						});
					} else {
						me.continueWithNewStore();
					}
				});
			},
			updateCustomerAttribute: function (returnUrl) {
				var preferredStore = null;
				if (localStorage.getItem('preferredStore')) {
					preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
				}
				var customer = new CustomerModels.EditableCustomer();
				customer.set("id", user.accountId);
				customer.fetch().then(function (response) {
					var attribute;
					if (isHomeFurnitureSite) {
						attribute = _.findWhere(response.apiModel.data.attributes, {
							fullyQualifiedName: Hypr.getThemeSetting('hfPreferredStore')
						});
					} else {
						attribute = _.findWhere(response.apiModel.data.attributes, {
							fullyQualifiedName: Hypr.getThemeSetting('preferredStore')
						});
					}
					var attributeData = {
						attributeFQN: attribute.fullyQualifiedName,
						attributeDefinitionId: attribute.attributeDefinitionId,
						values: [preferredStore.code]
					};
					response.apiUpdateAttribute(attributeData).then(function () {
						if (returnUrl.indexOf("/store/") != -1) {
							window.location.href = "/";
						} else {
							window.location.href = returnUrl;
						}
					});
				});
			},
			onSearchFocusBlur: function (e) {
				var inputElement = e.currentTarget;
				if (inputElement.name === "postalCode") {
					if (e.type === "focusin") {
						$('.store-locator-search-loc').show();
					} else {
						$('.store-locator-search-loc').hide();
					}
					$("#city").val("");
					$("#province").val("");
				} else {
					$("#postalCode").val("");
					if(cityChanges){
						this.populateCities(e);
					}
				}
			},
			blurSearchField: function () {
				if ($("#postalCode").is(":focus")) {
					$("#postalCode").blur();
				}
				if ($("#city").is(":focus")) {
					$("#city").blur();
				}
			},
			findStores: function (e) {
				var me = this;
				var postalCode = "",
					city = "",
					searchValue = null,
					searchConfigure = null;
				// validate form based on user input for postal code or city and province 
				if ($('#postalCode').val()) {
					postalCode = $('#postalCode').val().trim();
				}
				if ($("#city").val()) {
					var CityHiddenValue=$("#hdnFlagClick").val();
                    if($("#hdnFlagClick").val() === ""){
                        CityHiddenValue = $(".selected").text().trim();
                    }
					if(CityHiddenValue !== ""){
						city = CityHiddenValue;
					}else{
					   city = $(".selected").text().trim();
                       if(CityHiddenValue === ""){
                           city = $("#city").val().trim();
                       }
					}
				}
				if ($("#city_list").is(":visible")){
					if ($("#city_list li").hasClass("selected")){
						cityChanges = true;
					}
				}
				if (e.which === 13 || e.which === 1) {
					$("#content-loading").show();
					if (postalCode) {
						if (me.validatePostalCode(postalCode.trim())) {
							if (city === "") {
								searchValue = postalCode.trim();
								checkedFilters.splice(0, checkedFilters.length);
								me.model.unset("searchErrorMsg");
								var whiteSpaceSearch = "", whiteSpaceFlag = false;
								if (!(/\s/.test(searchValue))) {
									whiteSpaceSearch = searchValue.replace(/^(.{3})(.*)$/, "$1 $2");
									whiteSpaceFlag = true;
								}
								whiteSpaceSearch = whiteSpaceFlag ? whiteSpaceSearch : searchValue;
								searchConfigure = {
									"async": true,
									"crossDomain": true,
									"url": "https://us1.locationiq.com/v1/search.php?key="+Hypr.getThemeSetting('mapApiKey')+"&postalcode="+whiteSpaceSearch+"&format=json&countrycodes=ca",
									"method": "GET"
								};
								  
								$.ajax(searchConfigure).done(function (response) {
									if(response && response.length > 0){
										searchBasedLatLong = {
											"lat": response[0].lat,
											"lng": response[0].lon
										};
										me.getSearchStores(searchValue, searchBasedLatLong);
										me.blurSearchField();
									}else {
										search = searchValue;
										me.reRenderView();
									}
								}).error(function(error){
									search = postalCode.trim();
									me.reRenderView(null);
								});
							}
						} else {
							me.model.set("searchErrorMsg", Hypr.getLabel("notValidPostalCode"));
							setTimeout(function () {
								$("#content-loading").hide();
								search = postalCode.trim();
								me.model.unset("stores", []);
								me.render();
								me.blurSearchField();
							}, 500);
						}
					} else if (city !== "") {
						if(cityChanges && city.indexOf(",") != -1){
							if (postalCode === "") {
								searchValue = city.trim();
								me.model.unset("searchErrorMsg");
								checkedFilters.splice(0, checkedFilters.length);
								searchConfigure = {
									"async": true,
									"crossDomain": true,
									"url": "https://us1.locationiq.com/v1/search.php?key="+Hypr.getThemeSetting('mapApiKey')+"&addressdetails=1&q="+searchValue+"&countrycodes=ca&layers=coarse&format=json&matchquality=1",
									"method": "GET"
								};
								$.ajax(searchConfigure).done(function (response) {
									if(response && response.length > 0){
										var matchedLevel = false;
										var boundingBoxValues = [];
										_.each(response,function(res){
											if((res.matchquality.matchlevel === 'neighbourhood' || res.matchquality.matchlevel === 'city' || res.matchquality.matchlevel === 'island') && !matchedLevel){
												searchBasedLatLong = {
													"lat": res.lat,
													"lng": res.lon
												};
												matchedLevel = true;
												boundingBoxValues = res.boundingbox;
											}
										});
										if(matchedLevel){
											searchBasedLatLong = searchBasedLatLong;
										}else{
											searchBasedLatLong = {
												"lat": response[0].lat,
												"lng": response[0].lon
											};
											boundingBoxValues = response[0].boundingbox;
										}
										me.getSearchStores(searchValue, searchBasedLatLong,boundingBoxValues);
										me.blurSearchField();
									}else {
										search = searchValue;
										me.reRenderView();
									}
								}).error(function(error){
									search = searchValue;
									me.reRenderView(null);
								});
							}
						} else if (cityChanges && $("#city_list").is(":visible") && city.indexOf(",") == -1){
							city = $("#city_list li.selected").text();
							if (postalCode === "") {
								searchValue = city.trim();
								me.model.unset("searchErrorMsg");
								checkedFilters.splice(0, checkedFilters.length);
								searchConfigure = {
									"async": true,
									"crossDomain": true,
									/* "url": "https://us1.locationiq.com/v1/search.php?key="+Hypr.getThemeSetting('mapApiKey')+"&addressdetails=1&q="+searchValue+"&countrycodes=ca&source=v3&layers=coarse&format=json", */
									"url":"https://staging.locationiq.com/v1/search.php?key=268b72520dbf98&format=json&q="+searchValue+"&addressdetails=1&countrycodes=ca&matchquality=1&layers=coarse",
									"method": "GET"
								};
								$.ajax(searchConfigure).done(function (response) {
									if(response && response.length > 0){
										var matchedLevel = false;
										var boundingBoxValues = [];
										_.each(response,function(res){
											if((res.matchquality.matchlevel === 'neighbourhood' || res.matchquality.matchlevel === 'city' || res.matchquality.matchlevel === 'island') && !matchedLevel){
												searchBasedLatLong = {
													"lat": res.lat,
													"lng": res.lon
												};
												matchedLevel = true;
												boundingBoxValues = res.boundingbox;
											}
										});
										if(matchedLevel){
											searchBasedLatLong = searchBasedLatLong;
										}else{
											searchBasedLatLong = {
												"lat": response[0].lat,
												"lng": response[0].lon
											};
											boundingBoxValues = response[0].boundingbox;
										}
										me.getSearchStores({"key":"city", "value":searchValue}, searchBasedLatLong,boundingBoxValues);
										me.blurSearchField();
									}else {
										search = searchValue;
										me.reRenderView();
									}
								}).error(function(error){
									search = searchValue;
									me.reRenderView(null);
								});
							}
						}else{
							me.model.set("searchErrorMsg", Hypr.getLabel("enterProperCityName"));
							setTimeout(function () {
								$("#content-loading").hide();
								search = city.trim() + ",";
								me.model.unset("stores", []);
								me.render();
								me.blurSearchField();
							}, 500);
						}
					} else {
						me.model.set("searchErrorMsg", Hypr.getLabel("enterValidSearchValue"));
						search = "";
						me.model.set("search", "empty");
						setTimeout(function () {
							$("#content-loading").hide();
							me.model.unset("stores", []);
							me.render();
						}, 500);
					}
				}
			},
			validatePostalCode: function (postal) {
				var regex = /^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ]( )?\d[ABCEGHJKLMNPRSTVWXYZ]\d$/i;
				return regex.test(postal) ? true : false;

			},
			getSearchStores: function (searchValue, searchBasedLatLong,boundingBoxValues) {
				var self = this,
					queryString = "";
				if (isHomeFurnitureSite) {
					queryString = "properties.displayOnline eq true and properties.storeType in['Home Furniture']";
				} else {
					if (window.location.search.indexOf('storeService') > 0) {
						returnParam = (window.location.search.split('storeService=')[1] || '');
						var serviceFilterString = decodeURIComponent(returnParam) === "Home Installs" ? "properties.storeServices in['Home Installs']" : "properties.commercialMaintenance eq true ";
						if(decodeURIComponent(returnParam) === "Top Notch Rewards"){
							serviceFilterString = "properties.storeServices in['Top Notch Rewards']";
						}
						if(decodeURIComponent(returnParam) === "Home Rentals"){
							serviceFilterString = "properties.homeRentals eq true";
						}
						queryString = "properties.displayOnline eq true and properties.storeType in['Home Hardware','Home Building Centre','Home Hardware Building Centre'] and " + serviceFilterString;
					} else {
						queryString = "properties.displayOnline eq true and properties.storeType in['Home Hardware','Home Building Centre','Home Hardware Building Centre']";
					}


					//for nosearch result link
					if (window.location.search.indexOf('storeType') > 0) {
						returnParam = (window.location.search.split('storeType=')[1] || '');
						var typeFilterString = decodeURIComponent(returnParam);
						if(typeFilterString == "HBC and HHBC"){
							if (!filtersString) {
								filtersString = "properties.storeType in['Home Building Centre']";
							} else {
								filtersString += " and " + "properties.storeType in['Home Building Centre']"+" or " + "properties.storeType in['Home Hardware Building Centre']";
							}
							queryString += "and" + filtersString;
						}
					}


				}
				api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + queryString + '&pageSize=1100', {
					cache: false
				}).then(function (response) {
					search = searchValue;
					var nearStoresList = findNearStores(response.items, searchBasedLatLong, Hypr.getThemeSetting("storeDistanceInkm"));
					self.reRenderView(nearStoresList, searchValue,boundingBoxValues);
				});
			},
			openFilter: function (e) {
				if (e) {
					e.preventDefault();
					e.stopPropagation();
				}
				$(".store-locator-filters").addClass('store-locator-filters-open');
				var screenWidth = window.matchMedia("(max-width: 767px)");
				if (screenWidth.matches) {
					$("body,html").addClass("no-scroll");
				}
				if(isFilterChecked){
					var id = $(".store-locator-filters").find("#" + filtersToCheck);
					$(id).prop("checked", true);
					isFilterChecked = false; 
				}
			},
			closeFilter: function (e) {
				if (e) {
					e.preventDefault();
					e.stopPropagation();
				}
				$(".store-locator-filters").removeClass('store-locator-filters-open');
				var screenWidth = window.matchMedia("(max-width: 767px)");
				if (screenWidth.matches) {
					$("body,html").removeClass("no-scroll");
				}
			},
			clearFilters: function (e) {
				var me = this;
				e.preventDefault();
				isFilterChecked = false;
				if (checkedFilters && checkedFilters.length > 0) {
					var screenWidth = window.matchMedia("(max-width: 767px)");
					if (screenWidth.matches) {
						$("body,html").removeClass("no-scroll");
					}
					_.each(checkedFilters, function (filter) {
						var id = $(".store-locator-filters").find("#" + filter.id);
						$(id).prop("checked", false);
					});
					checkedFilters.splice(0, checkedFilters.length);

					api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + filtersString + '&pageSize=1100', {
						cache: false
					}).then(function (response) {
						var preferredStore, latLong;
						if (localStorage.getItem('preferredStore')) {
							preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
							latLong = search ? searchBasedLatLong : preferredStore.geo;
						} else {
							latLong = searchBasedLatLong;
						}
						var nearStoresList = findNearStores(response.items, latLong, Hypr.getThemeSetting("storeDistanceInkm"));
						me.reRenderView(nearStoresList);
					});
				} else {
					checkedFilters = $(".store-locator-filters").find("input:checked");
					_.each(checkedFilters, function (filter) {
						var id = $(".store-locator-filters").find("#" + filter.id);
						$(id).prop("checked", false);
					});
					checkedFilters.splice(0, checkedFilters.length);
				}
			},
			toggleMapListView: function (e) {
				if (e) {
					e.preventDefault();
					e.stopPropagation();
				}
				$(".store-locator").toggleClass('store-loc-map-toggle');
				$(".store-locator-search-mobile").toggleClass('store-loc-mobile-toggled');
				
			},
			selectStore: function (e) {
				var self = this;
				if(!$(e.target).hasClass("store-details-link")){
					var currentTarget = e.currentTarget;
					$(".store-locator-result").removeClass("is-highlighted");
					$(currentTarget).addClass("is-highlighted");
					var storeIndex = parseInt(currentTarget.getAttribute("data-mz-index"), 10) - 1;
					var data = self.model.attributes.stores[storeIndex];
					var geo = {
						"lng": parseFloat(data.properties.longitude),
						"lat": parseFloat(data.properties.latitude)
					};
	
					var popUps = document.getElementsByClassName('mapboxgl-popup');
					// Check if there is already a popup on the map and if so, remove it
					if (popUps[0]) $(popUps[0]).remove();

					//setZoom and panTo to current selected store
					map.setZoom(Hypr.getThemeSetting('mapZoomLevel'));
					map.panTo(geo);
	
					new mapboxgl.Popup().setLngLat([parseFloat(data.properties.longitude),parseFloat(data.properties.latitude)]).setHTML(self.infoWindowContent(data, (storeIndex === 0 && data.isPreferresStore))).addTo(map);
				}
			},
			loadMap: function () {
				var self = this;
				var storeList = self.model.get("stores");
				unwired.key = mapboxgl.accessToken = Hypr.getThemeSetting('mapApiKey');
				//Define the map and configure the map's theme
				map = new mapboxgl.Map({
					container: document.getElementById('store-map'),
					attributionControl: false, //need this to show a compact attribution icon (i) instead of the whole text
					style: unwired.getLayer("streets"),//get Unwired's style template
					center:[-113.323975,53.631611],
					zoom: 3
				});

	            //Add Navigation controls to the map to the top-right corner of the map
	            var nav = new mapboxgl.NavigationControl();
	            map.addControl(nav, 'top-right');

	            //Add a Scale to the map
	            map.addControl(new mapboxgl.ScaleControl({
	                maxWidth: 80,
	                unit: 'metric' //imperial for miles
	            }));

				//plot markers on map
				if (storeList && storeList.length > 0) {
					_.each(storeList, function (store, index) {
						//var location, marker, icon;
						if (store.properties.displayOnline) {
							
							var popup = new mapboxgl.Popup().setHTML(self.infoWindowContent(store, (index === 0 && store.isPreferresStore)));
							var currentIcon = (index === 0 && store.isPreferresStore) ? 'url('+Hypr.getThemeSetting('sirvResourceImageUrl')+'/marker-selection.png)' : 'url('+Hypr.getThemeSetting('sirvResourceImageUrl')+'/marker-' + (isHomeFurnitureSite ? 'hf' : 'hh') + '.png'+')';
							var el = document.createElement('div');
								el.style.backgroundImage = currentIcon;
								el.style.width = isHomeFurnitureSite ? '27px' : '33px'; 
					            el.style.height = isHomeFurnitureSite ? '28px' : '40px';
					            el.style.backgroundSize = 'cover';
								
								// create the marker
								var marker = new mapboxgl.Marker(el).setLngLat([parseFloat(store.properties.longitude), parseFloat(store.properties.latitude)]).setPopup(popup).addTo(map);
								self.addMarkerEvent(el, store);
								markers.push(marker);
						}
					});
					// fit map bounds
					self.zoomExtends();
				}
				//plot current position on map
				if ($.cookie("currentPosition")) {
					self.currentPosition(map);
				}
			},
			resetMap: function () {
				this.clearMarkers();				
			},
			clearMarkers: function () {
				_.each(markers, function (marker) {
					marker.remove();
				});
			},
			zoomExtends: function () {
				var bounds = new mapboxgl.LngLatBounds();
				var screenWidth = window.matchMedia("(max-width: 767px)");
				if (markers.length > 0) {
					_.each(markers, function (marker) {
						bounds.extend(marker.getLngLat());
					});
					if (screenWidth.matches) {
						map.fitBounds(bounds,{padding: {top: 25, bottom:25, left: 50, right: 50}});
					}else{
						map.fitBounds(bounds,{padding: {top: 25, bottom:25, left: 100, right: 100}});
					}
					
				}
				// fit bounds as screen size changes
				map.on('resize', function () {
					map.fitBounds(bounds);  
				});
			},
			addMarkerEvent: function(marker, data){
				marker.addEventListener("click", function () {
					var screenWidth = window.matchMedia("(max-width: 359px)");
					if(!screenWidth.matches){
						var screenWidth1 = window.matchMedia("(max-width: 767px)");
						if(screenWidth1.matches) {
							map.setZoom(Hypr.getThemeSetting('mapZoomLevel'));
							map.panTo([parseFloat(data.properties.longitude), parseFloat(data.properties.latitude) + (0.6 * 0.33 * (map.getBounds().getNorth() - map.getBounds().getSouth()))]);
						}	
					}else{
						map.setZoom(Hypr.getThemeSetting('mapZoomLevel'));
						map.panTo([parseFloat(data.properties.longitude), parseFloat(data.properties.latitude) + (0.5 * 0.5 * (map.getBounds().getNorth() - map.getBounds().getSouth()))]);
					}
					
					$('.store-locator-results').animate({
						scrollTop: $("#" + data.name).parent().scrollTop() + $("#" + data.name).offset().top - $("#" + data.name).parent().offset().top
					}, 1000);
					$(".store-locator-result").removeClass("is-highlighted");
					$("#" + data.name).addClass("is-highlighted");
				});
			},
			currentPosition: function (map) {
				var self = this;
				var currentMarker, shopperPos;
				// check current position set in cookie or not if -> yes plot on map
				shopperPos = $.parseJSON($.cookie("currentPosition"));
				var el = document.createElement('div');
					el.style.backgroundImage = 'url(../resources/images/marker.png)';
					el.style.width = '35px'; 
					el.style.height = '44px';
					el.style.backgroundSize = 'cover';
				currentMarker = new mapboxgl.Marker(el).setLngLat([parseFloat(shopperPos.lng), parseFloat(shopperPos.lat)]).addTo(map);
				markers.push(currentMarker);
			},
			infoWindowContent: function (data, selection) {
				var storeLink = "store/" + data.name;
				var status, address1 = "",
					address2 = "",
					phone = "";
				if (data.storeStatus.status === "open") {
					status = true;
				}
				if (data.properties.address1 !== "NA" && data.properties.address1 !== "N/A") {
					address1 = data.properties.address1;
				}
				if (data.properties.address2 !== "NA" && data.properties.address2 !== "N/A") {
					address2 = data.properties.address2;
				}
				if(data.properties.phone){
					var phoneNumList = data.properties.phone.split("#");
					phone = phoneNumList[0];
				}
				var content = '<div class="store-info-window">' +
					'<div class="info-container">' +
					'<a href=' + storeLink + ' ' + 'class="link"><span class="material-icons">keyboard_arrow_right</span></a>' +
					'<div class="info-content">' +
					'<h4 class="store-title">' + data.properties.storeName + '</h4>' +
					'<p class="store-address-details">' +
					(
						address1 ? '<span class="addr-street">' + address1 + '</span>' :
						""
					) +
					(
						address2 ? '<span class="addr-street">' + address2 + '</span>' :
						""
					) +
					'<span class="addr-loc">' + data.properties.city + '</span>' +
					'<span class="addr-tele">' + phone + '</span>' +
					'</p>' +
					'<p class="store-status">' +
					(
						status ? '<span><i class="material-icons">done</i>' + Hypr.getLabel("openNow") + '</span>' :
						'<span>' + Hypr.getLabel("closedNow") + '</span>'
					) +
					(
						status ? data.storeStatus.hours : ""
					) +
					'</p>' +
					'</div>' +
					'</div>' +
					(
						selection ? '<div class="home-store"><span class="material-icons">place</span>' + Hypr.getLabel('myStore') + '</div>' :
						'<button class="make-store" data-mz-store=' + data.name + ' data-mz-action="makeMyStore">' + Hypr.getLabel('makeMyStore') + '</button>'
					) +
					'</div>';
				return content;
			},
			searchOnCurrentLocation: function (e) {
				var self = this,
					queryString = "";
				e.preventDefault();
				$("#content-loading").show();
				if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(function (location) {
						var currentLocation = {
							"lat": location.coords.latitude,
							"lng": location.coords.longitude
						};

						if (ua.indexOf('chrome') > -1) {
							if (/edg|edge/i.test(ua)){
								$.cookie("currentPosition", JSON.stringify(currentLocation), {
									path: '/'
								});
							}else{
								document.cookie = "currentPosition="+JSON.stringify(currentLocation)+";path=/;Secure;SameSite=None";
							}
						} else {
							$.cookie("currentPosition", JSON.stringify(currentLocation), {
								path: '/'
							});
						}
						if (self.model.get("search")) {
							search = "";
							self.model.unset("search");
						}
						if (isHomeFurnitureSite) {
							queryString = "properties.displayOnline eq true and properties.storeType in['Home Furniture']";
						} else {
							if (window.location.search.indexOf('storeService') > 0) {
								returnParam = (window.location.search.split('storeService=')[1] || '');
								var serviceFilterString = decodeURIComponent(returnParam) === "Home Installs" ? "properties.storeServices in['Home Installs']" : "properties.commercialMaintenance eq true ";
								if(decodeURIComponent(returnParam) === "Top Notch Rewards"){
									serviceFilterString = "properties.storeServices in['Top Notch Rewards']";
								}
								if(decodeURIComponent(returnParam) === "Home Rentals"){
									serviceFilterString = "properties.homeRentals eq true";
								}
								queryString = "properties.displayOnline eq true and properties.storeType in['Home Hardware','Home Building Centre','Home Hardware Building Centre'] and " + serviceFilterString;
							} else {
								queryString = "properties.displayOnline eq true and properties.storeType in['Home Hardware','Home Building Centre','Home Hardware Building Centre']";
							}
						}
						api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + queryString + '&pageSize=1100', {
							cache: false
						}).then(function (response) {
							var nearStoresList = findNearStores(response.items, currentLocation, Hypr.getThemeSetting("storeDistanceInkm"));
							self.reRenderView(nearStoresList);
						});

					}, function (error) {
						search = "";
						self.model.set("search", "empty");
						setTimeout(function () {
							$("#content-loading").hide();
							self.model.unset("stores", []);
							self.render();
							$("#useMyLocation").hide();
							$("#locationError").show();
						}, 500);
					}, {
						enableHighAccuracy: true,
						maximumAge: 0,
						timeout: 5000
					});
				} else {
					// browser does not support geolocation.
					setTimeout(function () {
						$("#content-loading").hide();
					}, 500);
					$("#useMyLocation").hide();
					$("#locationError").show();
				}
			},
			continueWithNewStore: function () {
				var me = this;
				var returnUrl = "/";
				if (window.location.search.indexOf('returnUrl') > 0) {
					returnUrl = (window.location.search.split('returnUrl=')[1] || '');
				}else{
					var locale = apiContext.headers['x-vol-locale'];
					locale = locale.split('-')[0];
					var currentLocale = '';
					if (Hypr.getThemeSetting('homeFurnitureSiteId') != contextSiteId){
						currentLocale = locale === 'fr' ? '/fr' : '/en';
					}
					else{
						currentLocale = "/";
					}
					returnUrl = currentLocale;
				}
				
				$.ajax({
					url: "/set-purchase-location",
					data: {
						"purchaseLocation": storeToChange
					},
					type: "GET",
					success: function (response) {
						api.get("location", {
							code: storeToChange
						}).then(function (response) {
							var selectedStore = response.data;
							CookieUtils.setPreferredStore(selectedStore);
							/**
							 * Below code removes a flag stored in a cookie. This cookie is set through Arc (IGN_HH_IP_BASED_STORE_LOCATOR.1.0.0)
							 * When a user selects a store manually, we need to remove this flag, so that at cart checkout store confirmation pop-up disappears.
							 * Ticket - Main Ticket - 1202, Sub ticket - IWM-1471
							 * @Date - 19/04/2021
							*/
							console.log("storeToChange",storeToChange);
							console.log("response",response);
							if($.cookie('preferredStore_userConfirmationRequired')) {
								$.removeCookie('preferredStore_userConfirmationRequired', { path: '/' });
							}
							//$.cookie("sessionStore",JSON.stringify(selectedStore.code),{path:'/'});
							if(itemsInCart.length > 0){
								var previousItemQuantity = [];
								_.each(itemsInCart, function(item){
									var prodObj = {
										'quantity' : item.quantity,
										'productCode' : item.product.productCode,
										'price': item.product.price.price,
										'salePrice': item.product.price.salePrice
									};
									previousItemQuantity.push(prodObj);
								});

								if (ua.indexOf('chrome') > -1) {
									if (/edg|edge/i.test(ua)){
										$.cookie("previousItems",JSON.stringify(previousItemQuantity),{path:'/'});		
									}else{
										document.cookie = "previousItems="+JSON.stringify(previousItemQuantity)+";path=/;Secure;SameSite=None";
									}
								} else {
									$.cookie("previousItems",JSON.stringify(previousItemQuantity),{path:'/'});
								}
							}
							if (user.isAuthenticated && !user.isAnonymous) {
								me.updateCustomerAttribute(returnUrl);
							} else {
								if (returnUrl.indexOf("/store/") != -1) {
									window.location.href = locale === 'fr' ? '/fr' : '/en';
								} else {
									window.location.href = returnUrl;
								}
							}
						});
					},
					error: function (error) {

					}
				});
			},
			applyFilters: function (e) {
				var me = this;
				e.stopImmediatePropagation();
				var screenWidth = window.matchMedia("(max-width: 767px)");
				var filtersToApply = $(".store-locator-filters").find("input:checked");
				$(".store-locator-filters").removeClass('store-locator-filters-open');
				if (screenWidth.matches) {
					$("body,html").removeClass("no-scroll");
				}
				if (filtersToApply.length > 0) {
					$("#content-loading").show();
					checkedFilters = filtersToApply;
					me.getFilterValues(checkedFilters);
				} else {
					if (checkedFilters.length > 0) {
						$("#content-loading").show();
						checkedFilters = filtersToApply;
						me.getFilterValues(checkedFilters);
					}
				}
			},
			getFilterValues: function (filters) {
				var me = this;
				var storeTypeFilters = [],
					storeServiceFilters = [],
					storesProductsFilters = [],
					queryString = "",
					distanceFilter = "",
					preferredStore = null;
				if (localStorage.getItem('preferredStore')) {
					preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
				}
				_.each(filters, function (filter) {
					var type = $(filter).data("mz-type");

					switch (type) {
						case "distance":
							distanceFilter = $(filter).data("mz-value");
							break;
						case "storeType":
							storeTypeFilters.push("'" + $(filter).data("mz-value") + "'");
							break;
						case "storeService":
							storeServiceFilters.push("'" + $(filter).data("mz-value") + "'");
							break;
						case "status":
							statusFilter = filter.checked;
							break;
						case "productsFilters":
							storesProductsFilters.push("'" + $(filter).data("mz-value") + "'");
							break;
					}
				});

				if (!isHomeFurnitureSite) {
					if (storeTypeFilters.length > 0 && storeServiceFilters.length > 0) {
						queryString = 'properties.storeType in[' + storeTypeFilters.join() + '] and properties.storeServices in[' + storeServiceFilters.join() + ']';
					} else if (storeServiceFilters.length > 0) {
						queryString = 'properties.storeServices in[' + storeServiceFilters.join() + ']';
					} else if (storeTypeFilters.length > 0) {
						queryString = 'properties.storeType in[' + storeTypeFilters.join() + ']';
					}
					if (!queryString) {
						/* Check Store Service parameter in url*/
						if (window.location.search.indexOf('storeService') > 0) {
							returnParam = (window.location.search.split('storeService=')[1] || '');
							var serviceFilter = decodeURIComponent(returnParam) === "Home Installs" ? "properties.storeServices in['Home Installs']" : "properties.commercialMaintenance eq true ";
							if(decodeURIComponent(returnParam) === "Top Notch Rewards"){
								serviceFilter = "properties.storeServices in['Top Notch Rewards']";
							}
							if(decodeURIComponent(returnParam) === "Home Rentals"){
								serviceFilter = "properties.homeRentals eq true";
							}
							queryString = "properties.storeType in['Home Hardware','Home Building Centre', 'Home Hardware Building Centre'] and properties.displayOnline eq true and " + serviceFilter;
						} else {
							queryString = "properties.storeType in['Home Hardware','Home Building Centre', 'Home Hardware Building Centre'] and properties.displayOnline eq true";
						}
					} else {
						/* Check Store Service parameter in url*/
						if (window.location.search.indexOf('storeService') > 0) {
							returnParam = (window.location.search.split('storeService=')[1] || '');
							var serviceFilterString = decodeURIComponent(returnParam) === "Home Installs" ? "properties.storeServices in['Home Installs']" : "properties.commercialMaintenance eq true ";
							if(decodeURIComponent(returnParam) === "Top Notch Rewards"){
								serviceFilterString = "properties.storeServices in['Top Notch Rewards']";
							}
							if(decodeURIComponent(returnParam) === "Home Rentals"){
								serviceFilterString = "properties.homeRentals eq true";
							}
							queryString += " and properties.displayOnline eq true and " + serviceFilterString;
						} else {
							queryString += " and properties.displayOnline eq true";
						}
					}
				} else {
					if (storesProductsFilters.length > 0 && storeServiceFilters.length > 0) {
						queryString = 'properties.homeFurnitureServices in[' + storesProductsFilters.join() + '] and properties.storeServices in[' + storeServiceFilters.join() + ']';
					} else if (storeServiceFilters.length > 0) {
						queryString = 'properties.storeServices in[' + storeServiceFilters.join() + ']';
					} else if (storesProductsFilters.length > 0) {
						queryString = 'properties.homeFurnitureServices in[' + storesProductsFilters.join() + ']';
					}
					if (!queryString) {
						queryString += "properties.storeType in['Home Furniture'] and properties.displayOnline eq true";
					} else {
						queryString += "and properties.storeType in['Home Furniture'] and properties.displayOnline eq true";
					}
				}

				api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + queryString + '&pageSize=1100', {
					cache: false
				}).then(function (response) {
					var nearStoresList;
					if (distanceFilter) {
						// filter response with distance
						nearStoresList = searchBasedLatLong ? findNearStores(response.items, searchBasedLatLong, distanceFilter) : findNearStores(response.items, preferredStore.geo, distanceFilter);
						me.reRenderView(nearStoresList);
					} else {
						nearStoresList = searchBasedLatLong ? findNearStores(response.items, searchBasedLatLong, Hypr.getThemeSetting("storeDistanceInkm")) : findNearStores(response.items, preferredStore.geo, Hypr.getThemeSetting("storeDistanceInkm"));
						me.reRenderView(nearStoresList);
					}
				});
			},
			resetFilters: function () {

				$(".store-locator-filters").removeClass('store-locator-filters-open');
				var screenWidth = window.matchMedia("(max-width: 767px)");
				if (screenWidth.matches) {
					$("body,html").removeClass("no-scroll");
				}
				_.each(checkedFilters, function (filter) {
					var id = $(".store-locator-filters").find("#" + filter.id);
					$(id).prop("checked", true);
				});

			},
			reRenderView: function (storesList, searchValue,boundingBoxValues) {
				var me = this;
				var nearStoresList = [],
					preferredStore, currentStore;
				if (storesList && storesList.length > 0) {
					me.resetMap();
					if (localStorage.getItem('preferredStore')) {
						currentStore = $.parseJSON(localStorage.getItem('preferredStore'));
						preferredStore = _.findWhere(storesList, {
							name: currentStore.code
						});
						nearStoresList = _.without(storesList, _.findWhere(storesList, {
							name: currentStore.code
						}));

						if (preferredStore)
							nearStoresList.unshift(preferredStore);
						if(searchValue){
							nearStoresList =  me.sortByCityName(nearStoresList,boundingBoxValues,searchValue).concat(nearStoresList);
							nearStoresList = nearStoresList.slice(0, 49);
						}else{
							nearStoresList = nearStoresList.slice(0, 49);
						}
					} else {
						if(searchValue){
							nearStoresList = me.sortByCityName(storesList,boundingBoxValues,searchValue).concat(storesList);
							nearStoresList = nearStoresList.slice(0, 49);
						}else{
							nearStoresList = storesList.slice(0, 49);
						}
					}

					_.each(nearStoresList, function (store) {
						setStoreStatus(store, days[today.getDay()]);
					});
					nearStoresList = _.unique(nearStoresList);
					if (statusFilter) {
						statusFilter = false;
						nearStoresList = _.filter(nearStoresList, function (store) {
							if (store.storeStatus.status === "open") {
								return store;
							}
						});
					}
				} else {
					setTimeout(function () {
						window.location.reload();
					}, 2000);
				}
				var storeData = {
					"stores": nearStoresList
				};
				me.model = new Backbone.Model(storeData);
				setTimeout(function () {
					me.render();
					$("#content-loading").hide();
					if (checkedFilters && checkedFilters.length > 0) {
						me.resetFilters();
					}
				}, 500);
			},
			getFrenchTranslation: function (service) {
				var servicesList = Hypr.getThemeSetting("storeServices");
				var storeService = _.find(servicesList, function (item) {
					return item.enName === service;
				});
				if (storeService)
					return storeService.frName;
			},
			getFrenchTypeTranslation: function (type) {
				var frenchValue = "";
				switch (type) {
					case "Home Building Centre":
						frenchValue = "Centre de Rénovation";
						break;
					case "Home Hardware Building Centre":
						frenchValue = "Centre de Rénovation Home Hardware";
						break;
					case "Home Hardware":
						frenchValue = "Quincaillerie Home Hardware";
						break;
				}
				return frenchValue;
			},
			addEmptySearch: function () {
				var me = this;
				me.model.set("noStores", "empty");
			},
			getCityList: function(filterString, city_list, cityInput) {
				if(!cityInput) {
					api.get('entityList', { listName: storeCitiesFQN , pageSize:5 , filter:filterString, sortBy: 'city asc' }).then(function(response) {
						if(response.data.items.length > 0) {
							$("#city_list").show();
							$("#city_list").empty();
							_.each(response.data.items, function (item) {
								var li = document.createElement('li');
								var a = document.createElement('a');
								var text = document.createTextNode(item.city +", " + item.province);
								a.href = '#';a.appendChild(text);
								li.appendChild(a);
								city_list.appendChild(li);
								a.onclick = onCitySelect; 
								if (response.data.items.length == 1) {
									$("#city_list li").addClass("selected");
								} else {
									$("#city_list li:first").addClass("selected");
								}
							});
						}else{
							$("#city_list").empty();
							$("#city_list").hide();
						}
					});
				}
				else {
					api.get('entityList', { listName: storeCitiesFQN , pageSize:5 , filter:filterString }).then(function(response) {
						prevCity = cityInput.value; 
						if(response.data.items.length > 0) {
							$("#city_list").show();
							$("#city_list").empty();
							_.each(response.data.items, function (item) {
								var li = document.createElement('li');
								var a = document.createElement('a');
								var text = document.createTextNode(item.city +", " + item.province);
								a.href = '#';a.appendChild(text);
								li.appendChild(a);
								city_list.appendChild(li);
								a.onclick = onCitySelect;   
								if (response.data.items.length == 1) {
									$("#city_list li").addClass("selected");
								} else {
									$("#city_list li:first").addClass("selected");
								}
							});
						}else{
							$("#city_list").empty();
							$("#city_list").hide();
						}
					});
				}
			},
			populateCities: function(event){
				//var input = document.getElementById('city');
				var selected, filterString="", cityValue="";
				var cityInput = event.target, min_characters = 2;
				var city_list = document.getElementById('city_list');
				if (event.keyCode == 13) { // enter
					if ($("#city_list").is(":visible")) {
						onCitySelect(event);
					}
				}
				if (cityInput.value.length <= min_characters ) {
					$("#city_list").empty();
					$("#city_list").hide();
					prevCity = cityInput.value; 
					return;
				} else { 
					if(cityInput.value.indexOf(",") == -1 && (event.keyCode !== 38 && event.keyCode !== 40)){  
						cityValue = cityInput.value.replace("'","^'");
						if(prevCity != cityInput.value){
							filterString = "city sw '"+cityValue.toLowerCase() +"' or"+ " city eq '"+ cityValue.toLowerCase()+"'";
							prevCity = cityInput.value;
							_.debounce(this.getCityList(filterString, city_list), 400);
						}
					}else{
						if(cityChanges && cityInput.value.indexOf(",") != -1 && (event.keyCode !== 38 && event.keyCode !== 40)){
							var splitCity = cityInput.value.split(",");
							cityValue = splitCity[0].replace("'","^'");
							filterString = "city sw '"+cityValue.toLowerCase() +"' or"+ " city eq '"+ cityValue.toLowerCase()+"'";
							_.debounce(this.getCityList(filterString, city_list, cityInput), 400);
						}
					}
				}
				if (event.keyCode == 38) { // up
					selected = $(".selected");
					$("#city_list li").removeClass("selected");
					if (selected.prev().length === 0) {
						selected.siblings().last().addClass("selected");
						cityInput.value = $(".selected a").text();
						cityChanges = true;
					} else {
						selected.prev().addClass("selected");
						cityInput.value = $(".selected a").text();
						cityChanges = true;
					}
				}
				if (event.keyCode == 40) { // down
					if($("#city_list li").hasClass("selected")){
						selected = $(".selected");
						$("#city_list li").removeClass("selected");
						if (selected.next().length === 0) {
							selected.siblings().first().addClass("selected");
							cityInput.value = $(".selected a").text();
							cityChanges = true;
						} else {
							selected.next().addClass("selected");
							cityInput.value = $(".selected a").text();
							cityChanges = true;
						}
					} else {
						$("#city_list li:first").addClass("selected").focus();
						cityInput.value = $(".selected a").text();
						cityChanges = true;
					}
				}
			},
			sortByCityName:function(nearStores,boundingBoxValues,searchValue){
				var storeList = [];
				var matchedCities = [];
				var unmatchedCities = [];
				var citySearch = searchValue.value ? searchValue.value : searchValue;
				if(citySearch.indexOf(",") != -1){
					_.each(nearStores,function(store){
						if(boundingBoxValues){
							if( boundingBoxValues[0] <= store.properties.latitude && store.properties.latitude <= boundingBoxValues[1] && boundingBoxValues[2]  >= store.properties.longitude && store.properties.longitude >= boundingBoxValues[3] ) {
								storeList.push(store);
							}
						}
					});
					_.filter(storeList, function(value){ 
						if(value.properties.city.toLowerCase() === citySearch.split(",")[0].toLowerCase().trim()){
							matchedCities.push(value);
						}else{
							unmatchedCities.push(value); 
						}
					});
				}else{
					_.filter(nearStores, function(value){ 
						if(value.properties.postalCode.toLowerCase() === citySearch.toLowerCase().replace(/ /g, "")){
							matchedCities.push(value);
						}else{
							unmatchedCities.push(value);  
						}
					});
				}
			    var finalCities = _.unique(matchedCities).concat(unmatchedCities);
				return finalCities;
			}
		});

		$("#city_list li").mouseover(function() {
			$("#city_list li").removeClass("selected");
			$(this).addClass("selected");
		});

		$(document).click(function() {
			$('#city_list:visible').hide();
		});
		$("#city_list").click(function(e) {
			e.stopPropagation(); 
			return false;
		});

		var LoadResources = {
			load: function(src, callback) {
			  var script = document.createElement('script'),
				  loaded;
			  script.setAttribute('src', src);
			  if (callback) {
				script.onreadystatechange = script.onload = function() {
				  if (!loaded) {
					callback();
				  }
				  loaded = true;
				};
			  }
			  document.getElementsByTagName('head')[0].appendChild(script);
			}
		};

		function onCitySelect(event) {
			var target = $( event.target ); 
			var cityInput = document.getElementById("city");
			$("#hdnFlagClick").val(cityInput.value = target.text());
			if(target.is('a')){
				event.preventDefault();
				cityInput.value = target.text();
				cityChanges = true;
			}
			$("#city_list").hide();
		}
		/*This function finds stores near preferred store in 50km range by default*/
		function findNearStores(storeList, location, range) {
			var nearStores = [];
			_.each(storeList, function (store) {
				var distance = getDistanceFromLatLongInKm(store.properties.latitude, store.properties.longitude, location.lat, location.lng);
				if (distance <= range) {
					var storeData = {
						distance: parseFloat(distance.toFixed(3)),
						modelData: store
					};
					nearStores.push(storeData);
				}
			});
			nearStores = _.sortBy(nearStores, "distance");
			var resultArray = [];
			_.each(nearStores, function (store) {
				resultArray.push(store.modelData);
			});
			return resultArray;
		}
		/*This function calculates and return the distance between preferred store and input store*/
		function getDistanceFromLatLongInKm(lat1, lng1, lat2, lng2) {
			var earthRadius = 6371; // Radius of the earth in km
			var dLat = deg2rad(lat2 - lat1); // deg2rad below
			var dLng = deg2rad(lng2 - lng1);
			var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			var distance = earthRadius * c * 0.62137119; // Distance in Mi
			return distance;
		}
		/*this funtion converts deg to rad*/
		function deg2rad(deg) {
			return deg * (Math.PI / 180);
		}

		/*this funtion set the store working hours status i.e open/closed*/
		function setStoreStatus(store, day) {
			store.storeStatus = getStoreStatus(store, day);
		}

		/*this function calculates and returns status object with working hours*/
		function getStoreStatus(store, day) {
			var workingHours = "",
				statusData;
			var start = new Date(),
				end = new Date();
			switch (day) {

				case "Sunday":
					workingHours = store.properties.sundayHours;
					break;
				case "Monday":
					workingHours = store.properties.mondayHours;
					break;
				case "Tuesday":
					workingHours = store.properties.tuesdayHours;
					break;
				case "Wednesday":
					workingHours = store.properties.wednesdayHours;
					break;
				case "Thursday":
					workingHours = store.properties.thursdayHours;
					break;
				case "Friday":
					workingHours = store.properties.fridayHours;
					break;
				case "Saturday":
					workingHours = store.properties.saturdayHours;
					break;
			}
			var currentTime = moment(),
				startTime, endTime;
			if (workingHours !== "Closed") {
				var workHours = workingHours.split("-");
				startTime = moment(workHours[0], "HH:mm a");
				endTime = moment(workHours[1], "HH:mm a");
			}
			if (currentTime.isBetween(startTime, endTime)) {
				statusData = {
					"status": "open",
					"hours": workingHours
				};
			} else {
				statusData = {
					"status": "closed",
					"hours": workingHours
				};
			}
			return statusData;
		}

		try {
			$("#content-loading").show();
			LoadResources.load("/scripts/unwired-gl.js", function(){
				if(window.location.search.indexOf('postalCode=') > 0){
					var postalCode = window.location.search.split('postalCode=')[1];
					postalCode = postalCode.split('&')[0];
					var pcStoreData = {
						"stores": []
					};
					storeView = new StoreView({
						el: $("#store-locator"),
						model: new Backbone.Model(pcStoreData)
					});
					storeView.addEmptySearch();
					storeView.render();
//					$("#content-loading").hide();
					
					$('#postalCode').val(postalCode);
					var e = $.Event( "keypress", { which: 13 });
					$('#postalCode').trigger(e);
				} else if (localStorage.getItem('preferredStore')) {
					if (window.location.search.indexOf('filter=storeType') > 0){
						filtersString = "properties.storeType in['Home Building Centre', 'Home Hardware Building Centre'] ";
					}
					api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + filtersString + '&pageSize=1100', {
						cache: false
					}).then(function (response) {
						var currentStore = $.parseJSON(localStorage.getItem('preferredStore'));
						var range = Hypr.getThemeSetting("storeDistanceInkm");
						var nearStoresList = [];
						if (response && response.items.length > 0) {
							nearStoresList = findNearStores(response.items, currentStore.geo, range);
							var preferredStore = _.findWhere(nearStoresList, {
								name: currentStore.code
							});
							if (preferredStore) {
								nearStoresList = _.without(nearStoresList, _.findWhere(nearStoresList, {
									name: currentStore.code
								}));
								nearStoresList.unshift(preferredStore);
							}

							nearStoresList = nearStoresList.slice(0, 49);
							_.each(nearStoresList, function (store) {
								setStoreStatus(store, days[today.getDay()]);
							});
						}

						var storeData = {
							"stores": nearStoresList
						};
						storeView = new StoreView({
							el: $("#store-locator"),
							model: new Backbone.Model(storeData)
						});
						storeView.render();
						$("#content-loading").hide();
					}, function (error) {
						console.error("API Error: ", error);
					});
				} else {
					var storeData = {
						"stores": []
					};
					storeView = new StoreView({
						el: $("#store-locator"),
						model: new Backbone.Model(storeData)
					});
					storeView.addEmptySearch();
					storeView.render();
					$("#content-loading").hide();
				}
			});
		} catch (error) {
			console.log("Error...while calling api..." + error);
		}

	});