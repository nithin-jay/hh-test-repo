define([
    'modules/jquery-mozu', 
    "underscore",
    "modules/backbone-mozu"], function($, _, Backbone) {
        
    var apiContext = require.mozuData('apicontext');
	var parentBrandCat = require.mozuData('category');
	var routeData = require.mozuData('routeData');

    var BrandsView = Backbone.MozuView.extend({
		templateName: 'modules/common/brands-detail',
		initialize:function(){
			var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
        	locale = locale.split('-')[0];
        	this.model.set("currentLocale", locale);
			this.model.set("currentSite", currentSite);	
		},
        render: function() {
			Backbone.MozuView.prototype.render.apply(this);
			var selectChar = this.model.get('character');
			$('.brand-character').removeClass('selected');
			var temp = '';
			$(".brand-character").each(function(ind,val){
				temp = val.text.toLowerCase().trim();
				if(selectChar == temp){
					$(val).addClass("selected");
				}
			});
        },
		selectCharacter: function(e){
			e.preventDefault();
			var $currentTarget = $(e.currentTarget);
            var sortChar = $currentTarget.data('sorting-character');
			this.model.setActiveBrands(sortChar);
			this.render();
        }
    });

    var BrandsModel = Backbone.MozuModel.extend({
		filterBrands: function(character){
			character = character.toLowerCase();
			var filteredBrands = this.get('allBrands').filter(function(brand){
				return brand.content.name.toLowerCase().charAt(0) == character;
			});
			return filteredBrands;
		},
		setFilteredBrands: function(character){
			this.set('brands', this.sortActiveBrands(this.filterBrands(character)));
			this.set('character', character);
		},
		sortActiveBrands: function(brandList){
			if(!brandList)
				brandList = this.get('brands');

			brandList.sort(function(a, b){
				if(a.content.name > b.content.name) return 1;
				if(b.content.name > a.content.name) return -1;
				return 0;
			});
		},
		setActiveBrands:function(character){
			character = character.toLowerCase();

			this.set('selectedCharacter', character);
			var alphabetReg = new RegExp('^[A-Za-z]');
			var filterAction =
				character != '#' ?
					function(brand){ return brand.content.name.toLowerCase().charAt(0) == character;} :
						function (brand){return (!alphabetReg.test(brand.content.name.toLowerCase().charAt(0))); };

			var viewableBrands =
				_.chain(this.get('allBrands'))
					.filter(filterAction)
					.sortBy(function(brand){
						return brand.content.name;
					})
					.value();
			this.set('brands', viewableBrands);
			this.set('character', character);
		}
	});
   	
	$(document).ready(function() {
		
		$('.brand-character').on('click',function(e){
			e.preventDefault();
			$('.brand-character').removeClass('selected');
			var currentTarget = $(e.target);
			currentTarget.addClass('selected');
			$('.brand-character[data-sorting-character="' + currentTarget.data('sorting-character') +'"]').addClass('selected');
			brandView.model.setActiveBrands(currentTarget.data('sorting-character'));
			brandView.render();
		});
		
		var brandView = new BrandsView({
			el: $('#brandList'),
			model: new BrandsModel({brands: [], allBrands: parentBrandCat.childrenCategories})
		});
		
		var sortByChar = 'a';

		if(routeData && routeData.sortBy) {
			sortByChar = routeData.sortBy;
		}

		brandView.model.setActiveBrands(sortByChar);
		brandView.render();
	});
});
