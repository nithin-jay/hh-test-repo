define([
         'modules/jquery-mozu',
         'hyprlive',
         "underscore",
         'modules/backbone-mozu',
         'hyprlivecontext',
         'modules/api'],
         function($, Hypr, _, Backbone,HyprLiveContext, api){

         var locationList = Hypr.getThemeSetting("storeDocumentListFQN"),
		 storeHours = Backbone.MozuModel.extend({
			timeConvert: function(time) {
				// Check correct time format and split into components
				time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
		
				if (time.length > 1) { // If time format correct
					time = time.slice(1); // Remove full string match value
					time[5] = +time[0] < 12 ? ' am' : ' pm'; // Set AM/PM
					time[0] = +time[0] % 12 || 12; // Adjust hours
				}
				return time.join(''); // return adjusted time or original string
				}
		 });
	var MyStore = Backbone.MozuView.extend({
		templateName: "modules/location/mystore",
		additionalEvents: {
			"click #changeStoreMobile": "changeStore",
			"click #storecollapse": "titleChange",
			"click #changeStoreDesktop": "changeStoreDesktop"
		},
		initialize: function(){
			var self = this;
			var weekDays = ["sunday", "monday", "tuesday",  "wednesday", "thursday", "friday", "saturday"],
			today = new Date(),
			day = weekDays[today.getDay()],
			hours=self.model.attributes.regularHours[day],
			description = self.model.attributes.description? self.model.attributes.description: "", 
			storehours = new storeHours(),
			homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId"),
			apiContext = require.mozuData('apicontext'),
			contextSiteId = apiContext.headers["x-vol-site"],
			pageContext = require.mozuData('pagecontext'),
			ua = navigator.userAgent.toLowerCase(),
			iscloseStoreDescription = _.findWhere(self.model.attributes.attributes, {
				"fullyQualifiedName": Hypr.getThemeSetting("closeStoreDescription")
			});
			var locale = require.mozuData('apicontext').headers['x-vol-locale'];
			locale = locale.split('-')[0];
			var currentLocale = locale === 'fr' ? '/fr' : '/en';
				if(hours.isClosed !== true){
					if(hours && hours.openTime && hours.closeTime){
						hours.openTime = storehours.timeConvert(hours.openTime);
						hours.closeTime = storehours.timeConvert(hours.closeTime);
					}
					self.model.set("isStoreOpen",true);
				}else{
					self.model.set("isStoreOpen",false);	
				}
			self.model.set("todayHours",hours);
			var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            self.model.set("currentLocale",currentLocale);
			self.model.set("currentSite",currentSite);

			var locationCode = self.model.get('code'),
			queryString = "properties.documentKey eq " + locationCode;
            if(description !== null && description !== undefined){
				self.setDescription(description,locale);
			}
			if(iscloseStoreDescription !== null && iscloseStoreDescription !== undefined){
				self.setCloseStoreDescription(iscloseStoreDescription,locale);
			}
            //STARTS: HH-1540 Change Store Hours
            //var selectedStore = JSON.parse(sessionStorage.getItem('selectedStore'));
            //var note = selectedStore ? selectedStore.note : '';
            //if(!note){

            	api.get('documentView', {
					listName: locationList,
					filter: queryString
				}).then(function (response) {
					if (response && response.data.items.length > 0) {
						var storeContactInfo = response.data.items[0];
						var contactInfo = {
							"phone": storeContactInfo.properties.phone,
							"fax": storeContactInfo.properties.fax,
							"email": storeContactInfo.properties.email
						};
						self.model.set("storeContactInfo",contactInfo);
						self.model.set("regularHours",self.model.get("regularHours"));
	            		window.storeDetails.model.set("storeContactInfo",contactInfo);
	            		if($('#store-name').length > 0){
	            			window.storeDetails.render();
	            			self.render();
						}
						var sEmail = self.model.get("storeContactInfo").email;
						if (ua.indexOf('chrome') > -1) {
							if (/edg|edge/i.test(ua)) {
							  $.cookie('storeEmail', sEmail, { path: '/' });
							} else {
							  document.cookie = "storeEmail=" + sEmail + ";path=/;Secure;SameSite=None";
							}
						  } else {
							$.cookie('storeEmail', sEmail, { path: '/' });
						  }
					}
				});
            //}
            //self.setNotes(note, locale);
            //ENDS: HH-1540 Change Store Hours
		},
		setCloseStoreDescription :function(closeStoreDescription, locale){
			var model = this.model;
			if(closeStoreDescription && closeStoreDescription.values.length > 0){
				if('en' === locale) {
					model.set('closeStoreDescription', closeStoreDescription.values[0].split('|')[0] ? closeStoreDescription.values[0].split('|')[0].trim() : "" );
				}
				else {
					model.set('closeStoreDescription', closeStoreDescription.values[0].split('|')[1] ? closeStoreDescription.values[0].split('|')[1].trim() : "" );
				}
			}
		},
		setDescription : function(description, locale){
			var model = this.model;
			if('en' === locale) {
				model.set('description', description.split('|')[0] ? description.split('|')[0].trim() : "");
			}
			else {
				model.set('description', description.split('|')[1] ? description.split('|')[1].trim() : "" );
			}
		},
		setNotes: function(note, locale) {
			var model = this.model;
			if(note !== null && note !== undefined){
				if('en' === locale) {
					model.set('notes', note.split('|')[0]);
				}
				else {
					model.set('notes', note.split('|')[1]);
				}
			}
		},
		render:function(){
			Backbone.MozuView.prototype.render.apply(this, arguments);
			if (window.matchMedia("(min-width:768px)").matches) {
				var storewidth = $("#store-name").width();

				var storeName = $('#store-name').html();
				console.log(storeName);
				$('.store-selected-popup .popbadge-text #nearest-store-notification').html(storeName);

				$(".home-furniture-content .store-selected-popup").css("left", 168 + storewidth);
			}
			var locale = require.mozuData('apicontext').headers['x-vol-locale'];
			if ($(".store-hour-Mobile").text().indexOf('Close') > -1) {
				if(locale === 'en-US') {
					$('.store-hour-Mobile').text($('.store-hour-Mobile').text().replace('Open', ''));
				}
				else {
					$('.store-hour-Mobile').text($('.store-hour-Mobile').text().replace('Ouvert', ''));
				}
			}
			if($('.nav-todays-hours p.todaysHour').text().indexOf('Close') > -1) {
				if(locale === 'en-US') {
					$('.nav-todays-hours p.todaysHour').text($('.nav-todays-hours p.todaysHour').text().replace('Open', ''));
				}
				else {
					$('.nav-todays-hours p.todaysHour').text($('.nav-todays-hours p.todaysHour').text().replace('Ouvert', ''));
				}
			}

			var storeHourLength = $(".store-hour-Mobile").text().length;
			if (storeHourLength > 30) {
				$(".store-hour-Mobile").css({"margin-right": "-20px","margin-left": "-20px"});
			}

			if ($('a#storecollapse').hasClass('collapsed')) {
            	$("a#storecollapse").attr({"title" : Hypr.getLabel("expand")});
            }
		},
		changeStore: function(e){
			e.preventDefault();
		 	var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            locale = locale.split('-')[0];
            var currentLocale = locale === 'fr' ? '/fr' : '/en';
		 	var pageContext = require.mozuData('pagecontext');
		 	if(window.location.pathname.length > 1){
		 	 if(pageContext.pageType=='search'){
		 		 var url = window.location.search.split('&')[0];
			 		window.location.href= currentLocale + "/store-locator?returnUrl=" + window.location.pathname+url;
		 		 }
		 	 else{
		 		window.location.href= currentLocale + "/store-locator?returnUrl=" + window.location.pathname;
		 			 }
		 	}else{
		 		window.location.href= currentLocale + "/store-locator";
		 	}
		},
		titleChange: function(e){
			e.preventDefault();
			var locale = require.mozuData('apicontext').headers['x-vol-locale'];
			if('en-US' === locale) {
				if($('a#storecollapse[title="Expand"]').length > 0) {
					$("a#storecollapse").attr({"title" : Hypr.getLabel("hide")});
				} else {
					$("a#storecollapse").attr({"title" : Hypr.getLabel("expand")});
				}
			} else {
				if($('a#storecollapse[title="Voir plus"]').length > 0) {
					$("a#storecollapse").attr({"title" : Hypr.getLabel("hide")});
				} else {
					$("a#storecollapse").attr({"title" : Hypr.getLabel("expand")});
				}
			}
		},
		changeStoreDesktop: function(e){
			e.preventDefault();
		 	var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            locale = locale.split('-')[0];
            var currentLocale = locale === 'fr' ? '/fr' : '/en';
		 	var pageContext = require.mozuData('pagecontext');
		 	if(window.location.pathname.length > 1){
		 	 if(pageContext.pageType=='search'){
		 		 var url = window.location.search.split('&')[0];
			 		window.location.href= currentLocale + "/store-locator?returnUrl=" + window.location.pathname+url;
		 		 }
		 	 else{
		 		window.location.href= currentLocale + "/store-locator?returnUrl=" + window.location.pathname;
		 			 }
		 	}else{
		 		window.location.href= currentLocale + "/store-locator";
		 	}
		}
	});
	
	var MyStoreDetails = Backbone.MozuView.extend({
		templateName: "modules/location/mystore-detail",
		 additionalEvents: {
		 	"click #chooseAnotherStore": "changeStore",
		 	"click #storeLocator": "changeStore",
		 	"click #hide-btn": "closeExpand",
		 	"click #close-btn": "hideExpand"
		 },
		 initialize: function(){
		 		var self = this;
		 		var locale = require.mozuData('apicontext').headers['x-vol-locale'];
		 		var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
                locale = locale.split('-')[0];
                var currentLocale = locale === 'fr' ? '/fr' : '/en';
                this.model.set("currentLocale",currentLocale);
				this.model.set("currentSite",currentSite);
				var storehours = new storeHours(),
				description = self.model.attributes.description? self.model.attributes.description: "",
				iscloseStoreDescription = _.findWhere(self.model.attributes.attributes, {
					"fullyQualifiedName": Hypr.getThemeSetting("closeStoreDescription")
				});
				
				_.each(self.model.get("regularHours"), function(day) {
					if(day.isClosed !== true){
						if(typeof day === 'object' && day) {                                                                       
							day.openTime = storehours.timeConvert(day.openTime);
							day.closeTime = storehours.timeConvert(day.closeTime);
						}
					}
				 });
				 if(description !== null && description !== undefined){
					self.setDescription(description,locale);
				}
				if(iscloseStoreDescription !== null && iscloseStoreDescription !== undefined){
					self.setCloseStoreDescription(iscloseStoreDescription,locale);
				}
				/*var selectedStore = JSON.parse(sessionStorage.getItem('selectedStore'));
            	var note = selectedStore ? selectedStore.note : '';
            	self.setNotes(selectedStore.note, locale);*/
		 },
		 setCloseStoreDescription :function(closeStoreDescription, locale){
			var model = this.model;
			if(closeStoreDescription && closeStoreDescription.values.length > 0){
				if('en' === locale) {
					model.set('closeStoreDescription', closeStoreDescription.values[0].split('|')[0] ? closeStoreDescription.values[0].split('|')[0].trim() : "" );
				}
				else {
					model.set('closeStoreDescription', closeStoreDescription.values[0].split('|')[1] ? closeStoreDescription.values[0].split('|')[1].trim() : "" );
				}
			}
		},
		 setDescription : function(description, locale){
			var model = this.model;
			if('en' === locale) {
				model.set('description', description.split('|')[0] ? description.split('|')[0].trim() : "");
			}
			else {
				model.set('description', description.split('|')[0] ? description.split('|')[0].trim() : "");
			}
		},
		 setNotes: function(note, locale) {
			var model = this.model;
			if(note !== null && note !== undefined){
				if('en' === locale) {
					model.set('notes', note.split('|')[0]);
				}
				else {
					model.set('notes', note.split('|')[1]);
				}
			}
		 },
		 changeStore: function(e){
		 	e.preventDefault();
		 	var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            locale = locale.split('-')[0];
            var currentLocale = locale === 'fr' ? '/fr' : '/en';
		 	var pageContext = require.mozuData('pagecontext');
		 	if(window.location.pathname.length > 1){
		 	 if(pageContext.pageType=='search'){
		 		 var url = window.location.search.split('&')[0];
			 		window.location.href= currentLocale + "/store-locator?returnUrl=" + window.location.pathname+url;
		 		 }
		 	 else{
		 		window.location.href= currentLocale + "/store-locator?returnUrl=" + window.location.pathname;
		 			 }
		 	}else{
		 		window.location.href= currentLocale + "/store-locator";
		 	}
		 },
		 closeExpand: function(e) {
		 	e.preventDefault();
		 	$("#myCollapsible").collapse('hide');
	        $("#show-btn").removeClass("hidden");
	        $("#hide-btn").addClass("hidden");
		 },
		 hideExpand: function(e) {
		 	e.preventDefault();
		 	$(".mystore-details#collapseOne").collapse('hide');
		 	$(".mystore-details").css({"border-top": "none"});
		 	$(".sitenav-container.add-border").css({"border-top": "none"});
		 	$(".add-bottom-border").css({"border-bottom-color": "#c5c5c5", "border-bottom-width":"1px", "border-bottom-style":"solid"});
		 	$("a#storecollapse").attr({"title" : Hypr.getLabel("expand")});
		 },
		 headerCallStore: function(e){
		 	var screenWidth = window.matchMedia("(max-width: 767px)");
            if(!screenWidth.matches){
                e.preventDefault();
            }
		 }
	});
	var TodayHours = Backbone.MozuView.extend({
		templateName: "modules/location/todays-hours",
		additionalEvents: {
			"click #changeStoreDesktop": "changeStore",
			"click #show-btn": "openExpand"
		},
		initialize: function(){
			var weekDays = ["sunday", "monday", "tuesday",  "wednesday", "thursday", "friday", "saturday"],
				today = new Date(),
				day = weekDays[today.getDay()],
				hours=this.model.attributes.regularHours[day];
				this.model.set("todayHours",hours);
		},
		changeStore: function(e){
			e.preventDefault();
		 	var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            locale = locale.split('-')[0];
            var currentLocale = locale === 'fr' ? '/fr' : '/en';
		 	var pageContext = require.mozuData('pagecontext');
		 	if(window.location.pathname.length > 1){
		 	 if(pageContext.pageType=='search'){
		 		 var url = window.location.search.split('&')[0];
			 		window.location.href= currentLocale + "/store-locator?returnUrl=" + window.location.pathname+url;
		 		 }
		 	 else{
		 		window.location.href= currentLocale + "/store-locator?returnUrl=" + window.location.pathname;
		 			 }
		 	}else{
		 		window.location.href= currentLocale + "/store-locator";
		 	}
		},
		openExpand: function(e){
			e.preventDefault();
			$("#myCollapsible").collapse('show');
	        $("#hide-btn").removeClass("hidden");
	        $("#show-btn").addClass("hidden");
		}
	 });
	return {
		MyStore : MyStore,
		MyStoreDetails : MyStoreDetails,
		StoreTodayHours: TodayHours
	};

});