define([
        'modules/backbone-mozu',
        "modules/api",
        'hyprlive',
        'hyprlivecontext',
        'modules/jquery-mozu',
        'underscore',
        'modules/models-customer',
        'modules/views-paging',
        'modules/editable-view',
        'modules/models-product',
        'creditcardvalidator',
        "modules/cart-monitor",
        'modules/views-cancel-order-modal',
        'modules/models-tax-calculator',
        'modules/views-order-return-modal',
        'modules/views-order-return-select-returntype-step',
        'modules/models-postal-code-checker'
    ],
    function (Backbone, api, Hypr, HyprLiveContext, $, _, CustomerModels, PagingViews, EditableView, ProductModels, creditcardvalidator, CartMonitor, CancelOrderModalView, TaxCalculatorModel, OrderReturnModalView, OrderReturnReturnTypeStep, PostalCodeChecker) {
        var apiContext = require.mozuData('apicontext');
        var contextSiteId = apiContext.headers["x-vol-site"];
        var user = require.mozuData('user'),
            cancelOrderModalView = null,
            returnItemModalView = null,
            orderReturnModalView = null,
            ua = navigator.userAgent.toLowerCase(),
            isMobile = navigator.userAgent.match(/(iPhone)|(iPod)|(android)|(webOS)/i),
            taxCalculatorModel = new TaxCalculatorModel();

        // EDIT - Moneris Checkout
        var monerisCheckoutEnvironment = 'QA';
        var siteContext = HyprLiveContext.locals.siteContext;
        var monerisPaymentSettings = siteContext.checkoutSettings.externalPaymentWorkflowSettings.find(function (paymentSetting) {
            return paymentSetting.name === Hypr.getThemeSetting('monerisPaymetWorkflowId');
        });
        if (monerisPaymentSettings) {
            monerisCheckoutEnvironment = monerisPaymentSettings.credentials.find(function (credential) {
                return credential.apiName === 'monerisCheckoutEnvironment';
            });
            monerisCheckoutEnvironment = monerisCheckoutEnvironment ? monerisCheckoutEnvironment.value : 'QA';
        }
        var monerisCheckoutInstance = new window.monerisCheckout();
        monerisCheckoutInstance.setMode(monerisCheckoutEnvironment === 'QA' ? 'qa': 'prod');
        monerisCheckoutInstance.setCheckoutDiv("monerisCheckout");

        var AccountSettingsView = EditableView.extend({
            templateName: 'modules/my-account/my-account-settings',
            render: function () {
                var self = this;
                Backbone.MozuView.prototype.render.apply(this, arguments);
                var fName = document.getElementById('myaccntFname');
                if (fName) {
                    fName.addEventListener('keyup', function (event) {
                        if (event.which === 32) {
                            fName.value = fName.value.replace(/^\s+/, '');
                            if (fName.value.length === 0) {
                                event.preventDefault();
                            }
                        }
                });
            }

            var lName = document.getElementById('myaccntLname');
            if (lName) {
                lName.addEventListener('keyup', function (event) {
                    if (event.which === 32) {
                        lName.value = lName.value.replace(/^\s+/, '');
                        if (lName.value.length === 0) {
                            event.preventDefault();
                        }
                    } else if (fName.value == " ") {
                        event.preventDefault();
                    }
                });
            }

            var user = require.mozuData('user');
            if (!(user.isAnonymous)) {
                var cust = api.get('customer', {
                    id: user.accountId
                }).then(function (response) {
                    $('.display-FirstName').text(response.data.firstName);
                    $('.display-lastName').text(response.data.lastName);
                    $('.display-email').text(response.data.emailAddress);
                    $('.mz-accountsettings-firstname').val(response.data.firstName);
                    $('.mz-accountsettings-lastname').val(response.data.lastName);
                    $('.mz-accountsettings-email').val(response.data.emailAddress);
                });
            }
            //aeroplan member number

        },
        autoUpdate: [
            'firstName',
            'lastName',
            'emailAddress',
            'acceptsMarketing',
            'contractor'
        ],
        constructor: function () {
            EditableView.apply(this, arguments);
            this.editing = false;
            this.invalidFields = {};
        },
        varructor: function () {
            EditableView.apply(this, arguments);
            this.editing = false;
            this.invalidFields = {};
        },
        initialize: function () {
            return this.model.getAttributes().then(function (customer) {
                customer.get('attributes').each(function (attribute) {
                    attribute.set('attributeDefinitionId', attribute.get('id'));
                });
                return customer;
            });
        },
        updateAttribute: function (e) {
            var self = this;
            var attributeFQN = e.currentTarget.getAttribute('data-mz-attribute');
            var attribute = this.model.get('attributes').findWhere({
                attributeFQN: attributeFQN
            });
            var nextValue = attribute.get('inputType') === 'YesNo' ? $(e.currentTarget).prop('checked') : $(e.currentTarget).val();

            attribute.set('values', [nextValue]);
            attribute.validate('values', {
                valid: function (view, attr, error) {
                    self.$('[data-mz-attribute="' + attributeFQN + '"]').removeClass('is-invalid')
                        .next('[data-mz-validationmessage-for="' + attr + '"]').text('');
                },
                invalid: function (view, attr, error) {
                    self.$('[data-mz-attribute="' + attributeFQN + '"]').addClass('is-invalid')
                        .next('[data-mz-validationmessage-for="' + attr + '"]').text(error);
                }
            });
        },
        startEdit: function (event) {
            event.preventDefault();
            this.editing = true;
            this.render();
        },
        cancelEdit: function () {
            this.editing = false;
            this.afterEdit();
        },
        finishEdit: function () {
            var self = this;
            if ($('[data-mz-value="acceptsMarketing"]:checked').val() === 'on') {
                this.model.set('acceptsMarketing', true);
            }
            self.changeValidationFlag(true);
            var emailReg = Backbone.Validation.patterns.email;
            var fnameVal = !self.model.validate("firstName") ? true : false,
                lnameVal = !self.model.validate("lastName") ? true : false,
                emailVal = !self.model.validate("emailAddress") ? true : false;
            var emailExp = self.model.get("emailAddress");
            if (emailVal && fnameVal && lnameVal) {
                if (emailExp.match(emailReg)) {
                    self.model.set('userName', emailExp);
                    self.doModelAction('apiUpdate').then(function () {
                        self.editing = false;
                        self.changeValidationFlag(false);
                        $('#settings-success').html('<div class="success-msg success-msg-timeout agenda"><div class="alert alert-success">' + Hypr.getLabel('successProfile') + '</div></div>');
                        setTimeout(function () {
                            $('.success-msg-timeout').fadeOut();
                        }, 3000);
                        $(document).trigger("changeUserName");

                        function showClearance() {
                            if ($(window).width() > 1380) {
                                setTimeout(function () {
                                    if (user.isAnonymous) {
                                        var utilityContainerWidth = $(".utility-nav-width").width();
                                        var utilityWidth = $(".headerUtility").width();
                                        var searchWidth = utilityContainerWidth - utilityWidth;
                                        if ($(".headerUtility").hasClass('home-furniture-signin-width')) {
                                            searchWidth = searchWidth;
                                        }
                                        $(".mz-searchbox-field").width(searchWidth);
                                    }
                                    $("#desktop-search").show();
                                }, 2000);
                            }
                            if ($(window).width() > 991 && $(window).width() < 1381) {
                                setTimeout(function () {
                                    if (user.isAnonymous) {
                                        var utilityContainerWidth = $(".utility-nav-width").width();
                                        var utilityWidth = $(".headerUtility").width();
                                        var searchWidth = utilityContainerWidth - utilityWidth;
                                        if ($(".headerUtility").hasClass('home-furniture-signin-width')) {
                                            searchWidth = searchWidth;
                                        }
                                        $(".mz-searchbox-field").width(searchWidth);
                                    }
                                    $("#desktop-search").show();
                                }, 2000);
                            }

                            if ($(window).width() > 767 && $(window).width() < 992) {
                                setTimeout(function () {
                                    if (user.isAnonymous) {
                                        var utilityContainerWidth = $(".utility-nav-width").width();
                                        var utilityWidth = $(".headerUtility").width();
                                        var searchWidth = utilityContainerWidth - utilityWidth;
                                        if ($(".headerUtility").hasClass('home-furniture-signin-width')) {
                                            searchWidth = searchWidth;
                                        }
                                        $(".mz-searchbox-field").width(searchWidth);
                                    }
                                    $("#desktop-search").show();
                                }, 2000);
                            }
                            if ($(window).width() < 768) {
                                $(".twitter-typeahead").css({"width": "100%"});
                                $(".mz-searchbox-input").css({"width": "100%"});
                                $(".mz-searchbox-field").css({"width": "100%"});
                            }
                        }

                        showClearance();

                    }).otherwise(function () {
                        self.changeValidationFlag(false);
                        $('#settings-success').html('');
                        self.editing = true;
                    }).ensure(function () {
                        self.changeValidationFlag(false);
                        setTimeout(function () {
                            $('.msg-timeout').fadeOut();
                        }, 5000);
                        if (self.messageView.model.models.length === 0) {
                            self.afterEdit();
                        }
                    });
                } else {
                    $('#settings-success').html('<div class="success-msg success-msg-timeout agenda"><div class="alert alert-danger">' + Hypr.getLabel('emailMissing') + '</div></div>');
                    setTimeout(function () {
                        $('.success-msg-timeout').fadeOut();
                    }, 5000);
                }
            }
        },
        afterEdit: function () {
            var self = this;

            self.initialize().ensure(function () {
                self.render();
            });
        },
        changeValidationFlag: function (value) {
            this.model.validateFirstName = value;
            this.model.validateLastName = value;
            this.model.validateEmailAddress = value;
        }
    });

    var PasswordView = EditableView.extend({
        templateName: 'modules/my-account/my-account-password',
        autoUpdate: [
            'oldPassword',
            'password',
            'confirmPassword'
        ],
        additionalEvents: {
            "click .show-hide-current": 'showHideCurrent',
            "click .show-hide-new": 'showHideNewPwd',
            "click .show-hide-confirm": 'showHideConfirmPwd'
        },
        render: function () {
            var self = this;
            Backbone.MozuView.prototype.render.apply(this, arguments);
        },
        showHideCurrent: function (e) {
            e.preventDefault();
            e.stopPropagation();
            var inputElement = $("#account-oldpassword");
            if (inputElement.attr('type') == 'password') {
                inputElement.attr('type', 'text');
                $('.show-hide-current').text(Hypr.getLabel('hidePassword'));
                $('.show-hide-current').attr("title", Hypr.getLabel('hidePassword'));
            } else {
                inputElement.attr('type', 'password');
                $('.show-hide-current').text(Hypr.getLabel('show'));
                $('.show-hide-current').attr("title", Hypr.getLabel('show'));
            }
        },
        showHideNewPwd: function (e) {
            e.preventDefault();
            e.stopPropagation();
            var inputElement = $("#account-password");
            if (inputElement.attr('type') == 'password') {
                inputElement.attr('type', 'text');
                $('.show-hide-new').text(Hypr.getLabel('hidePassword'));
                $('.show-hide-new').attr("title", Hypr.getLabel('hidePassword'));
            } else {
                inputElement.attr('type', 'password');
                $('.show-hide-new').text(Hypr.getLabel('show'));
                $('.show-hide-new').attr("title", Hypr.getLabel('show'));
            }
        },
        showHideConfirmPwd: function (e) {
            e.preventDefault();
            e.stopPropagation();
            var inputElement = $("#account-confirmpassword");
            if (inputElement.attr('type') == 'password') {
                inputElement.attr('type', 'text');
                $('.show-hide-confirm').text(Hypr.getLabel('hidePassword'));
                $('.show-hide-confirm').attr("title", Hypr.getLabel('hidePassword'));
            } else {
                inputElement.attr('type', 'password');
                $('.show-hide-confirm').text(Hypr.getLabel('show'));
                $('.show-hide-confirm').attr("title", Hypr.getLabel('show'));
            }
        },
        startEditPassword: function () {
            this.editing.password = true;
            this.render();
        },
        finishEditPassword: function () {
            var self = this;

            var isSuccess = false;
            var oldPassword = $('[data-mz-value="oldPassword"]').val();
            if (!oldPassword) {
                $('[data-mz-validationmessage-for="oldPassword"]').html(Hypr.getLabel('oldPasswordMissing'));  //Validation message
                $('[data-mz-value="oldPassword"]').addClass('is-invalid');
                return false;
            } else {
                $('[data-mz-validationmessage-for="oldPassword"]').html('');  //Validation message
                $('[data-mz-value="oldPassword"]').removeClass('is-invalid');
            }
            this.doModelAction('changePassword').then(function () {
                _.delay(function () {
                    self.$('[data-mz-validationmessage-for="passwordChanged"]').show().text(Hypr.getLabel('passwordChanged')).fadeOut(3000);
                }, 250);
                $('#settings-success').html(Hypr.getLabel('successPassword'));
                isSuccess = true;
            }, function () {
                $('#settings-success').html('');
                self.editing.password = true;
            });
            this.editing.password = false;
        },
        cancelEditPassword: function () {
            this.editing.password = false;
            this.render();
        }
    });

    var WishListView = EditableView.extend({
        templateName: 'modules/my-account/my-account-wishlist',
        addItemToCart: function (e) {
            var self = this,
                $target = $(e.currentTarget),
                id = $target.data('mzItemId');
            if (id) {
                this.editing.added = id;
                return this.doModelAction('addItemToCart', id);
            }
        },
        doNotRemove: function () {
            this.editing.added = false;
            this.editing.remove = false;
            this.render();
        },
        beginRemoveItem: function (e) {
            var self = this;
            var id = $(e.currentTarget).data('mzItemId');
            if (id) {
                this.editing.remove = id;
                this.render();
            }
        },
        finishRemoveItem: function (e) {
            var self = this;
            var id = $(e.currentTarget).data('mzItemId');
            if (id) {
                var removeWishId = id;
                return this.model.apiDeleteItem(id).then(function () {
                    self.editing.remove = false;
                    var itemToRemove = self.model.get('items').where({
                        id: removeWishId
                    });
                    if (itemToRemove) {
                        self.model.get('items').remove(itemToRemove);
                        self.render();
                    }
                });
            }
        }
    });

    var OrderHistoryView = Backbone.MozuView.extend({
        templateName: "modules/my-account/order-history-list",
        getRenderContext: function () {
            var context = Backbone.MozuView.prototype.getRenderContext.apply(this, arguments);
            context.returning = this.returning;
            if (!this.returning) {
                context.returning = [];
            }
            context.returningPackage = this.returningPackage;
            return context;
        },
        render: function () {
            var self = this;
            Backbone.MozuView.prototype.render.apply(this, arguments);

            $.each(this.$el.find('[data-mz-order-history-listing]'), function (index, val) {

                var orderId = $(this).data('mzOrderId');
                var myOrder = self.model.get('items').get(orderId);
                var orderHistoryListingView = new OrderHistoryListingView({
                    el: $(this).find('.listing'),
                    model: myOrder,
                    messagesEl: $(this).find('[data-order-message-bar]')
                });
                orderHistoryListingView.render();
            });
        },
        selectReturnItems: function () {
            if (typeof this.returning == 'object') {
                $.each(this.returning, function (index, value) {
                    $('[data-mz-start-return="' + value + '"]').prop('checked', 'checked');
                });
            }
        },
        addReturnItem: function (itemId) {
            if (typeof this.returning == 'object') {
                this.returning.push(itemId);
                return;
            }
            this.returning = [itemId];
        },
        removeReturnItem: function (itemId) {
            if (typeof this.returning == 'object') {
                if (this.returning.length === 0) {
                    delete this.returning;
                } else {
                    var itemIdx = this.returning.indexOf(itemId);
                    if (itemIdx != -1) {
                        this.returning.splice(itemIdx, 1);
                    }
                }
            }
        },
        getLocationInfo: function () {
            var self = this,
                locationFilterString = this.getLocationQuery();
            api.request("GET", "api/commerce/storefront/locationUsageTypes/SP/locations/?filter=" + locationFilterString).then(function (response) {
                self.setLocationInfoToOrder(response.items);
                self.render();
            }, function (error) {
                console.log(error);
            });
        },
        setLocationInfoToOrder: function (locationList) {
            var self = this;
            if (locationList && locationList.length > 0) {
                locationList.forEach(function (location) {
                    self.model.get('items').models.forEach(function (order) {
                        if (order.get('items').models[0].get('purchaseLocation') === location.code) {
                            order.set('locationData', location);
                        }
                    });
                });
            }
        },
        getLocationQuery: function () {
            return this.model.get('items').map(function (order) {
                return 'code eq ' + order.get('items').models[0].get('purchaseLocation');
            }).join(' or ');
        }
    });

    var OrderHistoryListingView = Backbone.MozuView.extend({
        templateName: "modules/my-account/order-history-listing",
        additionalEvents: {
            "click .cancel-order": "initiateCancelOrder"
        },
        initialize: function () {
            this._views = {
                standardView: this,
                returnView: null
            };
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            this.model.set("isFrenchSite", locale === "en-US" ? false : true, {silent: true});
            this.setOrderType();
            if (this.model.get("orderType") !== "shipToHomeOrder")this.getLocationData();
            this.setTotalItemQty();
            this.setCustomTaxData();
            this.setTotalEhfToOrder();
            this.checkShipmentsAvailable();
            this.calculateReturnLinkDays();
            this.displayReturnLink();
            if (this.model.get('shipmentsAvailable')) {
                this.setShipmentsToOrder();
                this.checkPaymentStatusForCancelOrder();
            }
        },
        views: function () {
            return this._views;
        },
        displayReturnLink: function(){
            var orderStatus = this.model.get('status'),
            isValidDays = this.model.get('validDate'),
            orderId = this.model.get('id');
            var self = this;

            if(orderStatus == "Completed" && isValidDays){
                this.fetchOrderReturnableItems(orderId).then(function(res){
                    if(res.items && res.items.length > 0){
                        var returnableItem = _.find(res.items, function(item){
                            return item.quantityReturnable > 0;
                        });
                        if(returnableItem && returnableItem.quantityReturnable > 0){
                            self.model.set("isReturnItemsPresent",true);
                        }else{
                            self.model.set("isReturnItemsPresent",false);
                        }
                        self.render();
                    }
                });
            }
        },
        getRenderContext: function () {
            var context = Backbone.MozuView.prototype.getRenderContext.apply(this, arguments);
            context.returning = this.returning;
            if (!this.returning) {
                context.returning = [];
            }
            context.returningPackage = this.returningPackage;
            return context;
        },
        render: function () {
            var self = this;
            Backbone.MozuView.prototype.render.apply(this, arguments);

            if (!this._views.returnView) {
                this._views.returnView = new ReturnOrderListingView({
                    el: self.el,
                    model: self.model
                });
                this.views().returnView.on('renderMessage', this.renderMessage, this);
                this.views().returnView.on('returnCancel', this.returnCancel, this);
                this.views().returnView.on('returnSuccess', this.returnSuccess, this);
                this.views().returnView.on('returnFailure', this.returnFailure, this);
            }
        },
        setOrderType: function () {
            var isPickUp = 0,
                isShip = 0,
                orderType = '';
            _.each(this.model.get('items').models, function (item) {
                if (item.get('fulfillmentMethod') === "Pickup") {
                    isPickUp++;
                } else if (item.get('fulfillmentMethod') === "Ship") {
                    isShip++;
                }
            });

            if (this.model.get('status') === 'Cancelled') {
                this.model.set('orderCanceled', true);
            } else {
                this.model.set('orderCanceled', false);
            }

            if (isPickUp > 0 && isShip > 0) {
                orderType = 'mixOrder';
            } else if (isPickUp > 0) {
                orderType = 'shipToStoreOrder';
            } else if (isShip > 0) {
                orderType = 'shipToHomeOrder';
            }
            this.model.set('orderType', orderType);
        },
        setTotalItemQty: function () {
            var totalQty = 0,
                orderItems = this.model.get('items');
            _.each(orderItems.models, function (item) {
                totalQty += item.get('quantity');
            });
            this.model.set('totalItemsQuantity', totalQty);
        },
        setCustomTaxData: function () {
            var orderType = this.model.get('orderType');
            if ((orderType === "mixOrder" || orderType === "shipToHomeOrder") && this.model.get('taxData')) {
                var taxData = this.model.get('taxData'),
                    priceListCode = this.model.get('priceListCode'),
                    customTaxes = taxCalculatorModel.getCustomTaxes(taxData, orderType, priceListCode);
                this.model.set('customTaxData', customTaxes);
            }
        },
        setTotalEhfToOrder: function () {
            var self = this,
                ehfTotal = 0;
            _.each(this.model.get('items').models, function (item) {
                var options = item.get('product').apiModel.data.options,
                    ehfOption = options.length ? _.findWhere(options, {attributeFQN: Hypr.getThemeSetting("ehfFees")}) : null,
                    itemQty = item.get('quantity');
                if (ehfOption) {
                    if(item.get('fulfillmentMethod') === "Pickup") {
                        ehfTotal += ehfOption.shopperEnteredValue * itemQty;
                        }
                    }
                    if(item.get('fulfillmentMethod') === "Ship") {
                        var prices = item.get('unitPrice');
                        var itemPrice = prices.saleAmount ? prices.saleAmount : prices.listAmount;
                        if(prices.overrideAmount && prices.overrideAmount !== itemPrice){
                            var ehfFees = prices.overrideAmount - itemPrice;
                            var ehfOptionObj = {
                                name: "EHF Fees",
                                value: ehfFees,
                                shopperEnteredValue: ehfFees,
                                attributeFQN: "tenant~ehf-fees"
                            };
                            item.get('product').get('options').reset([ehfOptionObj]);
                            ehfTotal = ehfFees * item.attributes.quantity;
                        } else {
                            item.get('product').get('options').reset([]);
                        }
                    }
            });
            if (ehfTotal > 0) this.model.set('ehfTotal', ehfTotal);
        },
        checkShipmentsAvailable: function () {
            var shipments = this.model.get('shipments') ? this.model.get('shipments') : null,
                orderShipments = shipments ? shipments._embedded ? shipments._embedded.shipments : [] : [];
            if (shipments === null || !orderShipments.length) {
                this.model.set('shipmentsAvailable', false);
            } else {
                this.model.set('shipmentsAvailable', true);
            }
        },
        calculateReturnLinkDays:function(){
            var configuredDays = HyprLiveContext.locals.themeSettings.returnLinkDisplayDuration;
            if(configuredDays.indexOf(' ') >= 0){
                configuredDays = configuredDays.split(' ')[0];
            }
            var closeDate = this.model.get('closedDate');
            var newDate = this.addDays(closeDate,configuredDays);
            var currentDate = new Date();
            if(newDate >= currentDate ){
               this.model.set('validDate',true);
            }else{
                this.model.set('validDate',false);
            }
        },
        addDays:function(date, days) {
            var result = new Date(date);
            result.setDate(result.getDate() + parseInt(days,10));
            return result;
        },
        setShipmentsToOrder: function () {
            var shipToStoreShipments = this.filterShipments("BOPIS"),
                shipToHomeShipments = this.filterShipments("STH");
            this.model.set("shipToStoreShipments", shipToStoreShipments);
            this.model.set("shipToHomeShipments", shipToHomeShipments);
            if(shipToHomeShipments.length)this.getAndSetShipTimelines(shipToHomeShipments);
        },
        getAndSetShipTimelines: function (shipToHomeShipments) {
            var self = this,
                tasks = [];
            _.each(shipToHomeShipments, function (shipment) {
                if(shipment.workflowState.shipmentState !== "CUSTOMER_CARE" && shipment.workflowState.shipmentState !== "CANCELED") tasks.push(self.fetchShippingTimelines(shipment));
            });
            if(tasks.length){
                Promise.all(tasks).then(function (shipmentTimelines) {
                    self.setTimelineToShipment(shipmentTimelines, shipToHomeShipments);
                },function (error) {
                    console.log('Error while getting shipping timelines', error);
                });
            }
        },
        setTimelineToShipment: function (shipmentTimelines, shipToHomeShipments) {
            var isFrenchSite = this.model.get('isFrenchSite');
            this.setLocationCode(shipmentTimelines);
            _.each(shipToHomeShipments, function (shipment) {
                if(shipment.workflowState.shipmentState !== "CUSTOMER_CARE" && shipment.workflowState.shipmentState !== "CANCELED"){
                    var shipmentTimelineData = _.findWhere(shipmentTimelines, {fulfillmentLocationCode: shipment.fulfillmentLocationCode});
                    if (shipmentTimelineData) {
                        var shippingTimeline = shipmentTimelineData.shippingDateAndTime.expectedTransitTime,
                            bufferedTimeline = shippingTimeline + (isFrenchSite ? ' à ' : '-') + (shippingTimeline + Hypr.getThemeSetting('shippingTimelineBuffer'));
                        shipment.shippingTimeline = bufferedTimeline;
                    }
                }
            });
            this.model.set("shipToHomeShipments", shipToHomeShipments);
            this.render();
        },
        setLocationCode: function(shipmentTimelines){
            _.each(shipmentTimelines, function (timeline) {
                var warehouseZipCode = timeline.shippingDateAndTime.sourcePostalCode,
                    warehouseLocationCode = _.findWhere(Hypr.getThemeSetting('warehouseLatLong'), {zipCode:warehouseZipCode}).warehouseCode;
                timeline.fulfillmentLocationCode  = warehouseLocationCode;
            });
        },
        fetchShippingTimelines: function (shipment) {
            var self = this;
            var payload = self.getTimelineApiPayload(shipment),
                requestBody = {
                    url: Hypr.getThemeSetting("getShippingTimelines"),
                    data: JSON.stringify(payload),
                    type: "POST",
                    cors: true,
                    contentType: 'application/json'
                };
            return $.ajax(requestBody);
        },
        getTimelineApiPayload: function (shipment) {
            var fulfillmentLocationCode = shipment.fulfillmentLocationCode,
                warehouseLatLongs = Hypr.getThemeSetting('warehouseLatLong'),
                warhouse = _.findWhere(warehouseLatLongs, {warehouseCode: fulfillmentLocationCode}),
                customerZipcode = this.model.get('fulfillmentInfo').fulfillmentContact.address.postalOrZipCode,
                payLoad = {
                    "sourcePostalCode": warhouse.zipCode,
                    "destinationPostalCode": customerZipcode.trim().replace(/ /g, "").toUpperCase(),
                    "weight": 1,
                    "length": 1,
                    "width": 1,
                    "height": 1
                };
            return payLoad;
        },
        getLocationData : function() {
            var self = this,
                locationCode = this.model.get('priceListCode');
            api.request("GET", "api/commerce/storefront/locationUsageTypes/SP/locations/?filter=code eq " + locationCode + "&responseFields=items").then(function (response) {
                self.model.set('locationData', response.items[0]);
                self.render();
            }, function (error) {
                console.log("Error while getting location data", error);
            });
        },
        filterShipments: function (shipmentType) {
            var shipments = this.model.get('shipments') ? this.model.get('shipments') : null,
                orderShipments = shipments ? shipments._embedded ? shipments._embedded.shipments : [] : [],
                filteredShipments = _.filter(orderShipments, function (shipment) {
                    return shipment.shipmentType === shipmentType;
                });
            return filteredShipments ? filteredShipments : [];
        },
        setExpectedArrivalDateForBOPIS: function () {
            var expectedArrival = this.getDate(1),
                expectedArrivalTransfer = this.getDate(10);
            this.model.set("expectedArrivalDate", expectedArrival);
            this.model.set("expectedArrivalTransferDate", expectedArrivalTransfer);
        },
        getDate: function (days) {
            var submittedDate = new Date(this.model.get('submittedDate'));
            submittedDate.setDate(submittedDate.getDate() + days);
            return submittedDate;
        },
        checkPaymentStatusForCancelOrder: function () {
            var payments = this.model.get('payments'),
                isPaymentCollected = _.findWhere(payments, {paymentStatus: 'Collected'}),
                isTransferShipmentPresent = this.checkTransferShipments();
            if (isPaymentCollected || isTransferShipmentPresent) {
                this.model.set('allowCancelOrder', false);
            } else {
                this.model.set('allowCancelOrder', true);
            }
        },
        checkTransferShipments: function () {
            var shipments = this.model.get('shipments') ? this.model.get('shipments') : null,
                shipmentsArray = shipments ? shipments._embedded ? shipments._embedded.shipments : [] : [],
                shipment = _.find(shipmentsArray, function (shipment) {
                    return shipment.workflowState.shipmentState === 'WAITING_FOR_TRANSFER' || (shipment.transferShipmentNumbers && shipment.transferShipmentNumbers.length > 0) || shipment.workflowState.shipmentState === "COMPLETED";
                });
            return shipment ? true : false;
        },
        setTotalQuantity: function () {
            var self = this,
                shipmentsAvailable = this.model.get('shipmentsAvailable'),
                totalQty = 0;
            if (!shipmentsAvailable) {
                _.each(self.model.get('items').models, function (item) {
                    totalQty += item.get("quantity");
                });
                self.model.set('totalItemsQuantity', totalQty);
            }
        },
        initiateCancelOrder: function () {
            var orderNumber = this.model.get('orderNumber');
            console.log(orderNumber);
            if (cancelOrderModalView) cancelOrderModalView.undelegateEvents();
            cancelOrderModalView = new CancelOrderModalView({
                el: $('#cancelOrderModalContainer'),
                model: new Backbone.Model({
                    orderNumber: orderNumber
                })
            });
            cancelOrderModalView.render();
            $('#orderCancelModal').modal('show');
        },
        showReturnModal: function(event){
            var self = this,
                $ele = $(event.currentTarget),
                shipmentType = $ele.data('mz-shipment'),
                orderId = $ele.data('mz-order-id'),
                itemId = $ele.data('mz-item-id'),
                shipmentNumber = $ele.data('mz-shipment-number'),
                op = this.fetchOrderReturnableItems(orderId);
            op.then(function(response){
                var returnableItem = self.getReturnableItem(response.items, itemId, shipmentNumber);
                if(returnableItem && returnableItem.quantityOrdered !== returnableItem.quantityReturned){
                    self.renderReturnModalView(orderId, itemId, shipmentType, returnableItem);
                    if(shipmentType === "STH")  self.startReturn(orderId, itemId, returnableItem);
                }else{
                    console.log(response);
                }
            },function(error){
                console.log(error);
            });

        },
        getReturnableItem: function(items, itemId, shipmentNumber){
            var returnableItem = _.find(items,function(item){
                return item.orderItemId === itemId && item.shipmentNumber === shipmentNumber;
            });
            return returnableItem;
        },
        fetchOrderReturnableItems: function(orderId){
            return api.request("GET","/api/commerce/orders/" + orderId +"/returnableitems");
        },
        startReturn: function (orderId, itemId, returnableItem) {
            var rma = this.model.get('rma'),
                item = this.setDataToRma(this.model.get('items').get(itemId), returnableItem);
            if (item) {
                item = item.toJSON();
                item.orderItemId = item.id;
                rma.get('items').reset([item]);
                rma.set({
                    originalOrderId: orderId,
                    returnType: 'Refund'
                });
            }
        },
        setDataToRma: function(item, returnableItem){
            item.set('fulfillmentStatus',returnableItem.fulfillmentStatus);
            item.set('orderLineId',returnableItem.orderLineId);
            item.set('quantityFulfilled',returnableItem.quantityFulfilled);
            item.set('quantityOrdered',returnableItem.quantityOrdered);
            item.set('quantityReturnable',returnableItem.quantityReturnable);
            item.set('quantityReturned',returnableItem.quantityReturned);
            item.set('shipmentItemId',returnableItem.shipmentItemId);
            item.set('shipmentNumber',returnableItem.shipmentNumber);
            item.set('unitQuantity',returnableItem.unitQuantity);
            return item;
        },
        clearReturn: function() {
            var rma = this.model.get('rma');
            rma.clear();
        },
        finishReturn: function (id) {
            var self = this, op;
            var rma = this.model.get('rma');
            op = rma.apiCreate();
            if (op) return op.then(function (rma) {
                self.trigger('returncreated', rma.prop('id'));
                self.clearReturn();
                return rma;
            }, function (error) {
                console.log(error);
            });
        },
        renderMessage: function(message) {
            var self = this;
            if (message) {
                if (message.messageType) {
                    message.autoFade = true;
                    this.model.messages.reset([message]);
                    this.messageView.render();
                }
            }
        },
        returnSuccess: function() {
            this.renderMessage({
                messageType: 'returnSuccess'
            });
            this.render();
        },
        returnFailure: function() {
            this.renderMessage({
                messageType: 'returnFailure'
            });
            this.render();
        },
        startOrderReturn: function(e) {
            this.clearModalData();
            this.initiateOrderReturn();
            this.model.unset("selectedreplaceType");
            this.model.unset("returnOrreplaceselected");
            var returnOrderListingView = new ReturnOrderListingView({
                el: this.$el.find('#returnOrderItems'),
                model: this.model
            });
            returnOrderListingView.render();
        },
        clearModalData: function(){
            this.model.get('rma').clear();
            if(this.model.get('rma').get('items') && this.model.get('rma').get('items').length > 0 )this.model.get('rma').get('items').reset([]);
            this.model.unset('availableCourierMethods');
            this.model.unset('courierRate');
            this.model.unset('refundSubtotal');
            this.model.unset('shippingCost');
            this.model.unset('selectedCourierType');
            this.model.unset('isCourierReplace');
            this.model.unset('isCustomerPayShipping');
            this.model.unset('preservedRma');
            this.model.unset('returnOrderType');
            this.model.unset('cartLastFourDigits');
            this.model.unset('replaceViaCourierCount');
            this.model.unset('replaceViaCourierItems');
        },
        initiateOrderReturn: function(){
            if(orderReturnModalView)orderReturnModalView.undelegateEvents();
            var el = this.$el.find('#returnOrderModalContainer');
                 orderReturnModalView = new OrderReturnModalView({
                    el: el,
                    model: this.model,
                    parent: this
                });
            orderReturnModalView.render();
            this.$el.find('#orderReturnModal').modal('show');
        }
    });

    var ReturnOrderListingView = Backbone.MozuView.extend({
        templateName: "modules/my-account/order-history-listing-return",
        getRenderContext: function () {
            var context = Backbone.MozuView.prototype.getRenderContext.apply(this, arguments);
            var order = this.model;
            if (order) {
                this.order = order;
                context.order = order.toJSON();
            }
            return context;
        },
        render: function () {
            var self = this;
            var returnItemViews = [];
            var locationCode = this.getReturnLocationCode();
            if(this.model.get('rma').get('items') && this.model.get('rma').get('items').length > 0 )
                this.model.get('rma').get('items').reset([]);
            self.model.fetchReturnableItems().then(function (data) {
                var returnableItems = self.model.returnableItems(data.items);
                if (self.model.getReturnableItems().length < 1) {
                    self.$el.parent().siblings(".content-loading").css('display','none');
                    Backbone.MozuView.prototype.render.apply(self, arguments);
                    return false;
                }
                Backbone.MozuView.prototype.render.apply(self, arguments);

                $.each(self.$el.find('[data-mz-order-history-listing-return-item]'), function (index, val) {
                    var packageItem = returnableItems.find(function (model) {
                        if ($(val).data('mzOrderLineId') == model.get('orderLineId')) {
                            if ($(val).data('mzOptionAttributeFqn')) {
                                return (model.get('orderItemOptionAttributeFQN') == $(val).data('mzOptionAttributeFqn') && model.uniqueProductCode() == $(val).data('mzProductCode'));
                            }
                            return (model.uniqueProductCode() == $(val).data('mzProductCode'));
                        }
                        return false;
                    });

                    returnItemViews.push(new ReturnOrderItemView({
                        el: this,
                        model: packageItem,
                        returnLocationCode: locationCode,
                        parent: self
                    }));
                });

                _.invoke(returnItemViews, 'render');
                self.$el.parent().siblings(".content-loading").css('display','none');
            });
            },
            validateInputFields: function(){
            var returnReasonListData = Hypr.getThemeSetting('returnReasonList'),
            counter = 0,
            continueBtn = this.$el.find(".continue-btn"),
            items = this.model.get('rma').get('items').models;

            _.each(items,function(item){
                var commentVal = item.get("rmaComments") ? item.get("rmaComments"):"";
                var rmaQuantity = item.get("rmaQuantity");
                var rmaReason = item.get("rmaReason") ? item.get("rmaReason"):"";
                var reasonData = rmaReason !== "" ? _.findWhere(returnReasonListData,{'reasonCode':rmaReason}): null;
                var commentsRequired = reasonData !== null ? reasonData.additionalComments === "required" && commentVal.trim() === "" :true;

                if(commentsRequired || rmaQuantity <= 0 || rmaReason === "" ){
                    counter++;
                }
            });

            if(counter === 0 && items.length > 0){
                if(continueBtn.hasClass("disabled")){
                    continueBtn.removeClass("disabled");
                    continueBtn.prop("disabled", false);
                }
            }else{
                if(!continueBtn.hasClass("disabled")){
                    continueBtn.addClass("disabled");
                    continueBtn.attr('disabled', 'disabled');
                }
                if(items.length > 0){
                    continueBtn.removeClass("hidden");
                }else{
                    continueBtn.addClass("hidden");
                }
            }
        },
        getReturnLocationCode: function(){
            var orderType = this.model.get('orderType'),
                locationCode = orderType === 'mixOrder' || orderType === 'shipToStoreOrder' ? this.model.get('priceListCode') : this.model.get('items').models[0].get('purchaseLocation');
            return locationCode;
        },
        clearOrderReturn: function () {
            this.$el.find('[data-mz-value="isSelectedForReturn"]:checked').click();
        },
        cancelOrderReturn: function () {
            this.clearOrderReturn();
            this.trigger('returnCancel');
        },
        finishOrderReturn: function () {
            var self = this,
                op = this.model.finishReturn();
            if (op) {
                return op.then(function (data) {
                    self.model.isLoading(false);
                    self.clearOrderReturn();
                    self.trigger('returnSuccess');
                }, function () {
                    self.model.isLoading(false);
                    self.clearOrderReturn();
                    this.trigger('returnFailure');
                });
            }
        },
        continueToSelectReturnTypeStep: function(){
            var orderReturnReturnTypeStep = new OrderReturnReturnTypeStep({
                el: this.$el,
                model: this.model
            });
            orderReturnReturnTypeStep.render();
            $(orderReturnReturnTypeStep.el).parent().siblings(".modal-header").ScrollTo({ duration: 200 });
        }
    });

    var ReturnOrderItemView = Backbone.MozuView.extend({
        templateName: "modules/my-account/order-history-listing-return-item",
        autoUpdate: [
            'isSelectedForReturn',
            'rmaReturnType'
        ],
        additionalEvents: {
            "change [data-mz-reason]": "onOptionChange",
            "keyup [data-mz-mandatoryComments]": "onCommentsChanged",
            "change [data-mz-quantity]": "onQuantityChange",
            "click [data-mz-quantity]": "onQuantityChange",
            "keyup input[data-mz-quantity]": "onQuantityChange",
            "blur input[data-mz-quantity]": "onQuantityChange",
            "change [data-mz-primary-check]": "onCheckboxCheck",
            "click [data-mz-primary-check]": "onCheckboxCheck"
        },
        onOptionChange: function (e) {
            var selectedReasonCode = $(e.currentTarget).find("option:selected").val();
            this.model.set("rmaReason", selectedReasonCode);
            this.render();
            this.parent.validateInputFields();
        },
        onQuantityChange: _.debounce(function (e) {
            var $ele = $(e.currentTarget),
            changedQty = $ele.val(),
            maxReturnableQuantity = this.model.get('quantityReturnable');
            if(isNaN(changedQty) || changedQty <= 0) {
                e.preventDefault();
                $(e.currentTarget).val(1);
                this.model.set("rmaQuantity", 1);
            }else if(changedQty > maxReturnableQuantity){
                $(e.currentTarget).val(maxReturnableQuantity);
                this.model.set("rmaQuantity", maxReturnableQuantity);
            }else{
                this.model.set("rmaQuantity", changedQty);
            }
            this.parent.validateInputFields();
        },600),
        onCommentsChanged: function(e){
            var enteredComment = $(e.currentTarget).val();
            this.model.set("rmaComments", enteredComment);
            this.parent.validateInputFields();
        },
        dataTypes: {
            'isSelectedForReturn': Backbone.MozuModel.DataTypes.Boolean
        },
        initialize: function(options){
            this.returnLocationCode = options.returnLocationCode;
            this.parent = options.parent;
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            this.model.set("isFrenchSite", locale === "en-US" ? false : true, {silent: true});
            this.model.set('rmaQuantity',this.model.get("quantityReturnable"));
        },
        startReturnItem: function (e) {
            var $target = $(e.currentTarget);

            if (this.model.uniqueProductCode()) {
                if (!e.currentTarget.checked) {
                    this.model.unset("rmaReason");
                    this.model.unset("rmaComments");
                    this.model.set('isSelectedForReturn', false);
                    this.model.cancelReturn();
                    this.parent.validateInputFields();
                    this.render();
                    return;
                }

                this.model.set('isSelectedForReturn', true);
                this.model.startReturn(this.returnLocationCode);
                this.parent.validateInputFields();
                this.render();
            }
        },
        render: function () {
            Backbone.MozuView.prototype.render.apply(this, arguments);
        }
    });

    var ReturnHistoryView = Backbone.MozuView.extend({
        templateName: "modules/my-account/return-history-list",
        initialize: function () {
            var self = this;
            $(document).on("refreshReturnHistory",function(){
                self.getReturns();
            });
            self.model.on("getOrders",function(){
                self.getOrders();
            });
            self.model.set("returns",true);
            self.getReturns();
            this.listenTo(this.model, "change:pageSize", _.bind(this.model.changePageSize, this.model));
            this.listenTo(this.model, 'returndisplayed', function (id) {
                var $retView = self.$('[data-mz-id="' + id + '"]');
                if ($retView.length === 0) $retView = self.$el;
                $retView.ScrollTo({
                    axis: 'y'
                });
            });
        },
        getReturns: function(){
            var self = this;
            this.model.apiGet({
                pageSize: 5,
                sortBy : "createDate desc"
            }).then(function (res) {
                console.log(res);
                if(res && res.data && res.data.items && res.data.items.length > 0){
                    self.getOrders();
                }
            });
        },
        getOrdersQuery: function () {
            var query = this.model.get('items').map(function (rma) {
                return 'orderNumber eq ' + rma.get('originalOrderNumber');
            }).join(' or ');
            return query;
        },
        getOrders: function(){
            var rmaItems = this.model.get('items').models,
            self = this,
            ordersQuery = self.getOrdersQuery(),
            matchedOrder;

            api.get('orders', {
                filter: ordersQuery,
                responseFields:"items(closedDate,orderNumber, id)"
            }).then(function (response) {
                _.each(rmaItems,function(rmaItem){
                    matchedOrder = _.findWhere(response.data.items, {orderNumber : rmaItem.get("originalOrderNumber")});
                    if(matchedOrder){
                        rmaItem.set('closedDate',matchedOrder.closedDate);
                        self.calculateReturnLinkDays(rmaItem);
                    }
                });
                self.render();
            },function(error){
                console.log("Error Fetching Orders:",error);
            });
        },
        render: function () {
            this.setDataToRMA();
            this.model.set("returns",true);
            Backbone.MozuView.prototype.render.apply(this, arguments);
        },
        setDataToRMA: function(){
            var self = this;
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            this.model.set("currentLocale", locale, {silent: true});
            _.each(this.model.get('items').models, function(rma){
                rma.set('currentLocale', locale);
                self.setPaymentInfo(rma);
                self.checkReplaceItems(rma);
                self.setRefundAmount(rma);
            });
        },
        calculateReturnLinkDays:function(rma){
            var configuredDays = Hypr.getThemeSetting('returnLinkDisplayDuration');
            var expiryDate = this.addDays(rma.get("closedDate"),configuredDays);
            var isValid = expiryDate >= new Date() ? true : false;
            rma.set('validDate',isValid);
        },
        addDays:function(date, days) {
            var result = new Date(date);
            result.setDate(result.getDate() + parseInt(days,10));
            return result;
        },
        setPaymentInfo: function(rma){
            var payments = rma.get('payments'),
                payment =_.find(payments, function(payment){
                    return payment.status === "Credited" || payment.status === "Collected" || payment.status === "Authorized";
                }),
                billingInfo = payment.billingInfo;
            rma.set('paymentType',billingInfo.paymentType);
            if(billingInfo.paymentType === "CreditCard"){
                var cardNumber = billingInfo.card.cardNumberPartOrMask,
                    lastFourDigits = cardNumber.substring(cardNumber.length-4, cardNumber.length);
                rma.set('cardType',billingInfo.card.paymentOrCardType);
                rma.set('cartLastFourDigits',lastFourDigits);
            }
        },
        checkReplaceItems: function(rma){
            var self = this,
                rmaItems = rma.get('items').models,
                replaceItemCount = 0,
            refundItemCount = 0,
            internalNotes = rma.get('notes'),
            replaceViaCourierItemsData = internalNotes.length == 3 ?  internalNotes[2].text : null,
            replaceViaCourierItems = replaceViaCourierItemsData ? replaceViaCourierItemsData.split(':') : [];
            replaceViaCourierItems = replaceViaCourierItems.length ? replaceViaCourierItems[1].split(',') :[];
            _.each(rmaItems, function(rma){
            	if(rma.get('returnType') === 'Replace'){
                    var isReplaceViaCourierItem = replaceViaCourierItems.includes(rma.get('product').productCode);
                    rma.set('replaceViaCourier', isReplaceViaCourierItem);
                    replaceItemCount++;
                }
                if(rma.get('returnType') === 'Refund')refundItemCount++;
                var modifiedProductCode = self.getModifiedProductCode(rma);
                rma.set('modifiedProductCode', modifiedProductCode);
                var reasonAndQuantity = rma.get('reasons')[0];
                rma.set('reasons', [
                    {
                        reason : reasonAndQuantity.reason ? reasonAndQuantity.reason.trim().replace(/ /g, "") : '',
                        quantity: reasonAndQuantity.quantity
                    }
                ]);
            });
            rma.set('replaceItemCount',replaceItemCount);
            rma.set('refundItemCount',refundItemCount);
        },
        getModifiedProductCode: function(rma){
            var productCode = rma.get('product').productCode,
                modifiedProductCode = productCode.slice(0, 4) + '-' + productCode.slice(4, 8);
            return modifiedProductCode;
        },
        setRefundAmount: function (rma) {
            var rmaItems = rma.get('items').models,
                estimatedRefundTotalAmount = 0;
            _.each(rmaItems, function (rmaItem) {
                var returnType = rmaItem.get('returnType'),
                replaceViaCourier = rmaItem.get('replaceViaCourier');
                if(returnType === "Refund" || (returnType === "Replace" && !replaceViaCourier )){
                    var returnReason = rmaItem.get('reasons')[0].reason,
                    itemAmount = returnReason === "NoLongerWanted" ? rmaItem.get('totalWithoutWeightedShippingAndHandling') : rmaItem.get('totalWithWeightedShippingAndHandling');
                    estimatedRefundTotalAmount += itemAmount;
                }
            });
            rma.set('estimatedRefundTotalAmount', estimatedRefundTotalAmount);
        },
        initiatePrintLabel: function(e){
            var self = this,
                currentTarget = $(e.currentTarget),
                returnNumber = currentTarget.data('mz-return-number');
            currentTarget.addClass('is-loading');
            self.printReturnLabel(currentTarget, returnNumber);
        },
        printReturnLabel: function(currentTarget, returnNumber){
            var self = this;
            self.getShippingLabel(returnNumber).then(function(res){
                self.downloadAndPrintLabel(currentTarget, res.byteArray);
            },function(err){
                currentTarget.removeClass('is-loading');
                console.log("Failed to get api response",err);
            });
        },
        downloadAndPrintLabel: function(currentTarget, byteArray){
            var bin = atob(byteArray);
            var obj = document.createElement('object');
            var link = document.createElement('a');
            link.innerHTML = 'Download PDF file';
            link.download = 'ShippingLabel.pdf';
            link.href = 'data:image/pdf;base64,' + bin;
            link.click();
            currentTarget.removeClass('is-loading');
            obj.style.width = '100%';
            obj.style.height = '842pt';
            obj.type = 'application/pdf';
            obj.data = 'data:application/pdf;base64,' + bin;
            var iframe = "<iframe width='100%' height='100%' src='" + obj.data + "'></iframe>";
            var x = window.open();
            x.document.open();
            x.document.write('<title>Shipping Lable</title>');
            x.document.write(iframe);
            x.document.close();
        },
        getShippingLabel: function(returnNumber){
            var currentReturn = _.find(this.model.get('items').models,function(returnModels){
                return returnModels.get('returnNumber') === returnNumber;
            }),
            shipmentNumber = currentReturn.get('webSessionId'),
            payLoad = {
                "shipmentNumber" : shipmentNumber,
                "returnNumber" : returnNumber
            },
            requestBody = {
                url: Hypr.getThemeSetting("getShippingLabelURL"),
                data: JSON.stringify(payLoad),
                type: "POST",
                cors: true,
                contentType: 'application/json'
            };
            return $.ajax(requestBody);
        }
    });

    var PrintView = Backbone.MozuView.extend({
        templateName: "modules/my-account/my-account-print-window",
        el: $('#mz-printReturnLabelView'),
        initialize: function () {
        },
        loadPrintWindow: function () {
            var host = HyprLiveContext.locals.siteContext.cdnPrefix,
                printScript = host + "/scripts/modules/print-window.js",
                printStyles = host + "/stylesheets/modules/my-account/print-window.css";

            var my_window,
                self = this,
                width = window.screen.width - (window.screen.width / 2),
                height = window.screen.height - (window.screen.height / 2),
                offsetTop = 200,
                offset = window.screen.width * 0.25;


            my_window = window.open("", 'mywindow' + Math.random() + ' ', 'width=' + width + ',height=' + height + ',top=' + offsetTop + ',left=' + offset + ',status=1');
            my_window.document.write('<html><head>');
            my_window.document.write('<link rel="stylesheet" href="' + printStyles + '" type="text/css">');
            my_window.document.write('</head>');

            my_window.document.write('<body>');
            my_window.document.write($('#mz-printReturnLabelView').html());

            my_window.document.write('<script src="' + printScript + '"></script>');

            my_window.document.write('</body></html>');
        }
    });
    var scrollBackUpReturns = _.debounce(function () {
        $('#returnhistory').ScrollTo({axis: 'y', offsetTop: Hypr.getThemeSetting('gutterWidth')});
    }, 100);
    var ReturnHistoryPageNumbers = PagingViews.PageNumbers.extend({
        previous: function () {
            var op = PagingViews.PageNumbers.prototype.previous.apply(this, arguments);
            if (op) op.then(scrollBackUpReturns);
        },
        next: function () {
            var op = PagingViews.PageNumbers.prototype.next.apply(this, arguments);
            if (op) op.then(scrollBackUpReturns);
        },
        page: function () {
            var op = PagingViews.PageNumbers.prototype.page.apply(this, arguments);
            if (op) op.then(scrollBackUpReturns);
        }
    });

        var PaymentMethodsView = EditableView.extend({
            templateName: "modules/my-account/my-account-paymentmethods",
            additionalEvents: {
                "change [data-mz-value='editingContact.address.countryCode']": "changeProvince",
                "keyup [data-mz-value='editingCard.cardNumberPartOrMask']": "detectCardType"
            },
            autoUpdate: [
                'editingCard.isDefaultPayMethod',
                'editingCard.paymentOrCardType',
                'editingCard.nameOnCard',
                'editingCard.cardNumberPartOrMask',
                'editingCard.expireMonth',
                'editingCard.expireYear',
                'editingCard.cvv',
                'editingCard.isCvvOptional',
                'editingCard.contactId',
                'editingContact.firstName',
                'editingContact.lastNameOrSurname',
                'editingContact.address.address1',
                'editingContact.address.address2',
                'editingContact.address.address3',
                'editingContact.address.cityOrTown',
                'editingContact.address.countryCode',
                'editingContact.address.stateOrProvince',
                'editingContact.address.postalOrZipCode',
                'editingContact.address.addressType',
                'editingContact.phoneNumbers.home',
                'editingContact.isBillingContact',
                'editingContact.isPrimaryBillingContact',
                'editingContact.isShippingContact',
                'editingContact.isPrimaryShippingContact'
            ],
            initialize: function () {
                var self = this;
                console.log('initialized....');
                this.getMonerisCards(); //get all the cards registered in moneries..
            },
            addNewAddress: function (e) {
                var id = e.currentTarget.getAttribute('value');
                this.model.set('contactId', id);
                if (id === 'new') {
                    $(document).find('.address-form').removeClass('hidden');
                } else {
                    $(document).find('.address-form').addClass('hidden');
                }
            },
            beginEditCard: function (e) {
                var id = this.editing.card = e.currentTarget.getAttribute('data-mz-card');
                window.id = id;
               // this.model.beginEditCard(id);
                this.render(id);
                $(".mz-myaccount").css({"padding-right": "0px"});
            },
            finishEditCard: function () {
                var self = this;
                var id = window.id;
                var month = $('#mz-payment-expiration-month-'+id).val();
                var year = $('#mz-payment-expiration-year-'+id).val();
                this.updateMonerisCard(id,month,year);
                /*
                if (self.model.get('editingContact').get('address').get('countryCode') === 'CA') {
                    var currentProvince = self.model.get('editingContact').get('address').get('stateOrProvince');
                    var currentProvinceText = $('#stateOrProvinceCA-  option:selected').text();
                    var canadaProvinceKeys = [];
                    var canadaProvinceNames = [];
                    _.each(Hypr.getThemeSetting('canadaStates'), function (province) {
                        canadaProvinceKeys.push(province.key);
                        canadaProvinceNames.push(province.name);
                    });
                    var isCanadaProvince = _.contains(canadaProvinceKeys, currentProvince);
                    var isCanadaProvinceText = _.contains(canadaProvinceNames, currentProvinceText);
                    if (!isCanadaProvince) {
                        if (isCanadaProvinceText) {
                            var valofText = $("#stateOrProvinceCA-" + " option").filter(function () {
                                return this.text == currentProvinceText;
                            }).val();
                            $('#stateOrProvinceCA-').val(valofText);
                            currentProvince = $('#stateOrProvinceCA-  option:selected').val();
                            self.model.get('editingContact').get('address').set('stateOrProvince', currentProvince);
                        } else {
                            _.each($('.mz-addressform-state').children(), function (ele) {
                                if ($(ele).hasClass('mz-validationmessage')) {
                                    self.model.get('editingContact').get('address').set('stateOrProvince', currentProvince);
                                    $(ele).siblings('.select-icon-cont').find('select').val(Hypr.getLabel('selectProvince'));
                                }
                            });
                        }
                    }
                }
                var operation = this.doModelAction('saveCard');
                $(window).scrollTop(0);
                if (operation) {
                    $('#account-messages').hide();
                    operation.otherwise(function () {
                        self.editing.card = true;
                    });
  
                    this.editing.card = false;
                    $('[id*="edit-card-modal-"]').modal('hide');
                }
                */
            },
            changeProvince: function (e) {
                $(e.currentTarget).closest('.address-form').find('div[class*="province-"]').addClass('hidden');
                if ($(e.currentTarget).val() === 'US') {
                    $(document).find('.province-us').removeClass('hidden');
                    $(document).find('.province-other').addClass('hidden');
                    $(document).find('.province-ca').addClass('hidden');
                } else if ($(e.currentTarget).val() === 'CA') {
                    $(document).find('.province-ca').removeClass('hidden');
                    $(document).find('.province-us').addClass('hidden');
                    $(document).find('.province-other').addClass('hidden');
                } else {
                    $(document).find('.province-other').removeClass('hidden').find('input').val('');
                    $(document).find('.province-ca').addClass('hidden');
                    $(document).find('.province-us').addClass('hidden');
                }

                /* this.editing.card = false;
                $('[id*="edit-card-modal-"]').modal('hide');
                if (window.id === 'new') {
                    $(window).scrollTop(0);
                    this.render();
                } */
            },
            beginDeleteCard: function (e) {
                var id = e.currentTarget.getAttribute('data-mz-card'),
                    card = this.model.get('cards').get(id);
                $('#delete-card-modal-' + id).modal();
                $(".mz-myaccount").css({"padding-right": "0px"});
            },
            removeDeleteCard: function (e) {
                var id = e.currentTarget.getAttribute('data-mz-card');
                // card = this.model.get('cards').get(id);
                /*if (card) {
                 window.cardDeleted = true;
                 this.doModelAction('deleteCard', id);
                 }*/
                this.deleteMonerisCard(id);
            },
            showBillingAddresses: function (e) {
                $(e.currentTarget).closest('.billing-address-summary').addClass('hidden');
                $(document).find('.mz-creditcard-billingaddresses').removeClass('hidden');
            },
            deleteAllCardClasses: function (e) {
                var classNames = $(e.currentTarget).siblings('.defaultCardImg').attr("class").split(" ");
                var newclasses = [];
                var classToRemove = '';
                for (var i = 0; i < classNames.length; i++) {
                    classToRemove = classNames[i].search(/show+/);
                    if (classToRemove) newclasses[newclasses.length] = classNames[i];
                }
                return newclasses;
            },
            detectCardType: function (e) {
                var me = this;
                if ($.trim($(e.currentTarget).val()) === '') {
                    var newcl = me.deleteAllCardClasses(e);
                    $(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showDefaultCard');
                } else {
                    $(e.currentTarget).validateCreditCard(function (result) {
                        if (result.card_type) {
                            var newcl = [];
                            switch (result.card_type.name) {
                                case 'visa' :
                                    newcl = me.deleteAllCardClasses(e);
                                    $(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showVisaImg');
                                    me.model.set('editingCard.paymentOrCardType', 'VISA');
                                    break;
                                case 'mastercard' :
                                    newcl = me.deleteAllCardClasses(e);
                                    $(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showMastercardImg');
                                    me.model.set('editingCard.paymentOrCardType', 'MC');
                                    break;
                                case 'amex' :
                                    newcl = me.deleteAllCardClasses(e);
                                    $(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showAmexImg');
                                    me.model.set('editingCard.paymentOrCardType', 'AMEX');
                                    break;
                                case 'discover' :
                                    newcl = me.deleteAllCardClasses(e);
                                    $(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showDiscoverImg');
                                    me.model.set('editingCard.paymentOrCardType', 'DISCOVER');
                                    break;
                                default :
                                    newcl = me.deleteAllCardClasses(e);
                                    $(e.currentTarget).siblings('.defaultCardImg').removeClass().addClass(newcl.join(" ")).addClass('showDefaultCard');
                                    break;
                            }
                        }
                    });
                }
            },

            //adding cards for moneries
            /*************************************************************************************/
            attachMonerisEvents: function () {
                var me = this;

                monerisCheckoutInstance.setCallback("error_event", function (errorResponse) {
                    var response = JSON.parse(errorResponse);
                    console.log('error_event');
                    console.log(response);
                    monerisCheckoutInstance.closeCheckout(response.ticket || me.model.get('monerisCheckoutToken'));
                });

                monerisCheckoutInstance.setCallback("payment_complete", function (paymentSuccessResponse) {
                    var response = JSON.parse(paymentSuccessResponse);
                    console.log('payment_complete');
                    console.log(response);
                    // Receipt API - Check if payment was accepted in moneris or not.
                    // If receipt api fails then reinitialize the iframe.
                    api.request('POST', '/hh/api/preloadOrReceiptApi', {
                        amountToCharge: '1.00',
                        isReceiptRequest: true,
                        useCardVerificationCheckoutId : true,
                        monerisToken: response.ticket
                    }).then(function successCallback(apiResponse) {
                        console.log(apiResponse);
                        if (apiResponse && apiResponse.response && apiResponse.response.receipt &&
                            apiResponse.response.receipt.result === 'a') {
                            var receipt = apiResponse.response.receipt;
                            console.log(receipt);
                            monerisCheckoutInstance.closeCheckout(response.ticket || me.model.get('monerisCheckoutToken'));
                            me.assignMonerisDataKey(receipt);
                        } else {
                            // show error that card was declined by moneris.
                            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
                            if(locale == "fr-CA"){
                                me.model.trigger('error', {
                                    message: 'Échec de la vérification. Veuillez réessayer plus tard.'
                                });
                            }else{
                                me.model.trigger('error', {
                                    message: 'Verification failed. Please try again later!.'
                                });
                            }
                            // Payment not accepted - Close and re-open moneris iframe
                            monerisCheckoutInstance.closeCheckout(response.ticket || me.model.get('monerisCheckoutToken'));
                            me.initializeMoneris();
                        }
                    }, function errorCallback(error) {
                        //Payment not accepted - Close and re-open moneris iframe
                        monerisCheckoutInstance.closeCheckout(response.ticket || me.model.get('monerisCheckoutToken'));
                        me.initializeMoneris();
                    });
                });

                monerisCheckoutInstance.setCallback("cancel_transaction", function (cancelTransactionResponse) {
                    var response = JSON.parse(cancelTransactionResponse);
                    console.log('cancel_transaction');
                    console.log(response);
                    monerisCheckoutInstance.closeCheckout(response.ticket);
                });

                monerisCheckoutInstance.setCallback("page_loaded", function (pageLoadResponse) {
                    var response = JSON.parse(pageLoadResponse);
                    me.model.set({ paymentType: Hypr.getThemeSetting('monerisPaymetWorkflowId') });
                    me.model.set({ paymentWorkflow: Hypr.getThemeSetting('monerisPaymetWorkflowId') });

                    if (response.response_code === '001') {
                        var iframeStyle = $('#monerisCheckout iframe').attr('style');
                        var iframeStyleWithoutHeight = iframeStyle.replace('height: 100%;', '');
                        $('#monerisCheckout iframe').attr('style', iframeStyleWithoutHeight + 'height: 1000px;');
                        $('#moneris-payment-not-avail').addClass('hidden');
                        console.log($('#monerisCheckout iframe').html());
                        $('input[type="submit"] #process').val('Verify Your Card');
                    } else {
                        $('#moneris-payment-not-avail').removeClass('hidden');
                    }
                });
            },
            initializeMoneris: function () {
                var me = this;
                api.request('POST', '/hh/api/preloadOrReceiptApi', {
                    useCardVerificationCheckoutId : true,
                    amountToCharge: 1
                }).then(function successCallback(apiResponse) {
                    console.log(apiResponse);
                    try {
                        var initMonerisCheckout = apiResponse.response && apiResponse.response.ticket;
                        if (initMonerisCheckout) {
                            console.log('Initializing the moneris checkout flow.');
                            monerisCheckoutInstance.startCheckout(apiResponse.response.ticket);
                            me.model.set('monerisCheckoutToken', apiResponse.response.ticket);
                        } else {
                            console.log('initialize error');
                        }
                    } catch (err) {
                        console.error('Moneris checkout failure');
                        console.error(err.stack);
                    }
                }, function monerisGetTokenError(error) {
                    console.error(error);
                });
            },
            assignMonerisDataKey: function (receipt) {
                var me = this;
                api.request('POST', 'hh/api/assignDataKeyToCustomer', { receipt: receipt })
                    .then(function successCallback(apiResponse) {
                        try {
                            console.log(apiResponse);
                            if(apiResponse.status === 'SUCCESS'){
                                me.getMonerisCards();
                            }else{
                                console.log('errror while saving the card');
                            }
                        } catch (err) {
                            console.error(err.stack);
                        }
                    }, function Errror(error) {
                        console.error(error);
                        alert('errror while saving the card');
                    });
            },
            //fetch moneries cards
            getMonerisCards: function () {
                var me = this;
                api.request('POST', 'hh/api/fetchMonerisCards', {})
                    .then(function successCallback(cardResponse) {
                        console.log(cardResponse);
                        try {
                            var cards = [];
                            if(cardResponse.status === 'SUCCESS'){
                                cardResponse.response.forEach(function (resp) {
                                    if(resp.ResolveData != 'null'){
                                        var carddata = resp.ResolveData;
                                        var res = resp;
                                        var card = {
                                            'cardNumberPart': carddata.masked_pan.replace(/^.{4}/g, '****'),
                                            'paymentOrCardType': res.CardType,
                                            'contactId': 1102,
                                            'expireMonth': carddata.expdate.substring(2, 4),
                                            'expireYear':carddata.expdate.substring(0, 2),
                                            'id': res.DataKey,
                                            'isCvvOptional': false,
                                            'isDefaultPayMethod': true,
                                            'isSavedCard': false,
                                            'isVisaCheckout': false
                                        };
                                        cards.push(card);
                                    }
                                });
                                if(cards.length>0){
                                    me.model.set('hasSavedCards',true);
                                }else{
                                    me.model.set('hasSavedCards',false);
                                }
                                me.model.set({ 'monerisCards': cards });
                                me.render();
                            }
                        } catch (err) {
                            console.error('Failed while getting cards...');
                            console.error(err.stack);
                        }
                    }, function getCradsErrror(error) {
                        console.error(error);
                    });
            },
            updateMonerisCard:function(dataKey,month,year){
                var me = this;
                var expdate = year.substring(2,4)+month;
                api.request('POST', 'hh/api/updateMonerisCard', {dataKey:dataKey,expDate:expdate.toString()})
                    .then(function successCallback(apiResponse) {
                        try {
                            if (apiResponse.status === 'SUCCESS' && apiResponse.response.receipt.ResponseCode === '001') {
                                var cards = me.model.get('monerisCards');
                                cards.forEach(function (card) {
                                    if (card.id === dataKey) {
                                        var updatedDate = apiResponse.response.receipt.ResolveData.expdate;
                                        card.expireMonth = updatedDate.substring(2, 4);
                                        card.expireYear = updatedDate.substring(0, 2);
                                    }
                                });
                                me.model.set('monerisCard', cards);
                                $('#account-messages').hide();
                                $('[id*="edit-card-modal-"]').modal('hide');
                                me.editing.card = false;
                                me.render();
                            } else {
                                console.log('failed to update card');
                            }
                        } catch (err) {
                            console.error(err.stack);
                        }
                    }, function getCradsErrror(error) {
                        console.error(error);
                    });
                this.render();
            },
            deleteMonerisCard:function(dataKey){
                var me = this;
                api.request('POST', 'hh/api/deleteMonerisCard', { dataKey:dataKey })
                    .then(function successCallback(apiResponse) {
                        try {
                            if(apiResponse.status === 'SUCCESS' && apiResponse.response.receipt.ResponseCode === '001'){
                                var cards = me.model.get('monerisCards');
                                if(cards){
                                    /*cards.forEach(function(card,i){
                                        if(card.id === dataKey){
                                            cards.splice(i,1);
                                        }
                                    });*/
                                    var FilteredCards = cards.filter(function(card){
                                        return card.id !== dataKey;
                                    });
                                    me.model.set({ 'monerisCards': FilteredCards });
                                    me.render();
                                }
                            }else{
                                console.log('failed to delete');
                            }
                        } catch (err) {
                            console.error(err.stack);
                        }
                    }, function getCradsErrror(error) {
                        console.error(error);
                    });
            },

            /***********************************************************************************/

            render: function (id) {
                var me = this;
                this.attachMonerisEvents();
                Backbone.MozuView.prototype.render.apply(this, arguments);

                if (window.cardDeleted) {
                    $(document).find('.card-deleted').removeClass('hidden');
                    window.cardDeleted = false;
                } else {
                    $(document).find('.card-deleted').addClass('hidden');
                    $(".modal-backdrop").remove();
                    $(".mz-myaccount").removeClass("modal-open");
                    $(".mz-myaccount").css({"padding-right": "0px"});
                }

                if (typeof id === 'string') {
                    if (id != "new") {
                        $('#edit-card-modal-' + id).modal();
                    }
                }
        },
        
        cancelEditCard: function () {
            this.editing.card = false;
            this.model.endEditCard();
            $('[id*="edit-card-modal-"]').modal('hide');
            if (window.id === 'new') {
                $(window).scrollTop(0);
                this.render();
            }
        }
    });

    var AddressBookView = EditableView.extend({
        templateName: "modules/my-account/my-account-addressbook",
        autoUpdate: [
            'editingContact.firstName',
            'editingContact.lastNameOrSurname',
            'editingContact.address.address1',
            'editingContact.address.address2',
            'editingContact.address.address3',
            'editingContact.address.cityOrTown',
            'editingContact.address.countryCode',
            'editingContact.address.stateOrProvince',
            'editingContact.address.postalOrZipCode',
            'editingContact.address.addressType',
            'editingContact.phoneNumbers.home',
            'editingContact.isBillingContact',
            'editingContact.isPrimaryBillingContact',
            'editingContact.isShippingContact',
            'editingContact.isPrimaryShippingContact'
        ],
        renderOnChange: [
            'editingContact.address.countryCode',
            'editingContact.isBillingContact',
            'editingContact.isShippingContact'
        ],
        beginAddContact: function () {
            this.editing.contact = "new";
            this.render();
        },
        beginEditContact: function (e) {
            var id = this.editing.contact = e.currentTarget.getAttribute('data-mz-contact');
            this.model.beginEditContact(id);
            this.render();
        },
        finishEditContact: function () {
            var self = this,
                isAddressValidationEnabled = HyprLiveContext.locals.siteContext.generalSettings.isAddressValidationEnabled;
                var email = self.$el.find("#email-").val();
            var operation = this.doModelAction('saveContact', {
                forceIsValid: isAddressValidationEnabled,
                email: email
            }); // hack in advance of doing real validation in the myaccount page, tells the model to add isValidated: true
            if (operation) {
                operation.otherwise(function () {
                    self.editing.contact = true;
                });
                this.editing.contact = false;
            }
                var postalCode = self.$el.find("#postal-code-").val(),
                postalCodeChecker = new PostalCodeChecker();
                var primaryCheck = self.$el.find("#primaryCheckbox").is(':checked');
                if(primaryCheck){
                    postalCodeChecker.checkPostalCode(postalCode).then(function(){
                    },function(error){
                    self.errorHandler(error, postalCode);
                    });
                }
        },
        cancelEditContact: function () {
            this.editing.contact = false;
            this.model.endEditContact();
            this.render();
        },
        beginDeleteContact: function (e) {
            var self = this,
                contact = this.model.get('contacts').get(e.currentTarget.getAttribute('data-mz-contact')),
                associatedCards = this.model.get('cards').where({
                    contactId: contact.id
                }),
                windowMessage = Hypr.getLabel('confirmDeleteContact', contact.get('address').get('address1')),
                doDeleteContact = function () {
                    return self.doModelAction('deleteContact', contact.id);
                },
                go = doDeleteContact;


            if (associatedCards.length > 0) {
                windowMessage += ' ' + Hypr.getLabel('confirmDeleteContact2');
                go = function () {
                    return self.doModelAction('deleteMultipleCards', _.pluck(associatedCards, 'id')).then(doDeleteContact);
                };
            }

            if (window.confirm(windowMessage)) {
                return go();
            }
        }
    });
        $(document).ready(function () {
            CartMonitor.update();
            var accountModel = window.accountModel = CustomerModels.EditableCustomer.fromCurrent();

    var StoreCreditView = Backbone.MozuView.extend({
        templateName: 'modules/my-account/my-account-storecredit',
        addStoreCredit: function (e) {
            var self = this;
            var id = this.$('[data-mz-entering-credit]').val();
            if (id) return this.model.addStoreCredit(id).then(function () {
                return self.model.getStoreCredits();
            });
        }
    });

    // Preferred Store view
    var PreferredStoreView = Backbone.MozuView.extend({
        templateName: 'modules/location/preferred-store',
        additionalEvents: {
            "click .changePreferredStore": "changePreferredStore"
        },
        changePreferredStore: function (e) {
            e.preventDefault();
            window.location.href = "/store-locator?returnUrl=" + window.location.pathname;
        },
        callStore: function (e) {
            var screenWidth = window.matchMedia("(max-width: 767px)");
            if (!screenWidth.matches) {
                e.preventDefault();
            }
        }
    });

    $(document).ready(function () {
        CartMonitor.update();
        var accountModel = window.accountModel = CustomerModels.EditableCustomer.fromCurrent();

        $('#aeroplan-member-edit').on('click', function () {
            var self = $('#signupAeroplanSection');
            if (self.hasClass('hidden')) {
                $(self).removeClass('hidden').addClass('aeroplanAvailable');
                $('#aeroplan-number-available').addClass('hidden');
            } else {
                $(self).addClass('hidden').removeClass('aeroplanAvailable');
                $('#aeroplan-number-available').removeClass('hidden');
            }
        });

        //Aeroplan Section Start
        $("#signupAeroplanLastName").attr("value", accountModel.get('lastName'));
        var aeroplanAvailable = $('#signupAeroplanSection').hasClass('aeroplanAvailable');
        var aeroplanNumber = _.find(accountModel.get('attributes').toJSON(), function (attribute) {
            return attribute.fullyQualifiedName === Hypr.getThemeSetting('aeroplanMemberNumber');
        });
        if (aeroplanNumber && aeroplanNumber.values[0] != "N/A") {
            $('#no-aeroplan').addClass('hidden');
            $('#aeroplan-number-available').removeClass('hidden');
            var aeroplanValue = aeroplanNumber.values[0];
            $("#signupAeroplanNumberInput").attr("value", aeroplanNumber.values[0]);
            var displayNumber = aeroplanValue.replace(/(\d{1})(.*)(\d{3})/, '*** *** $3');
            $('#aeroplan-beforedisplay').text(aeroplanValue);
            $('#aeroplan-display').text(displayNumber);
            $('#aeroplan-beforeLastName').text(accountModel.get('lastName'));
            $('#aeroplan-LastName').text(accountModel.get('lastName'));
            var aeroplanNum1 = aeroplanValue.substring(0, 3);
            var aeroplanNum2 = aeroplanValue.substring(3, 6);
            var aeroplanNum3 = aeroplanValue.substring(6, 9);
            $('#number1').attr("value", aeroplanNum1);
            $('#number2').attr("value", aeroplanNum2);
            $('#number3').attr("value", aeroplanNum3);
        } else {
            $("#signupAeroplanNumberInput").attr("value", " ");
            $('#aeroplan-number-available').addClass('hidden');
            $('#no-aeroplan').removeClass('hidden');
        }
        //starts aeroplan 3 fields value
        var inputQuantity = [];
        $(function () {
            $(".digit-length").each(function (i) {
                inputQuantity[i] = this.defaultValue;
                $(this).data("idx", i);
            });
            $(".digit-length").on("keyup", function (e) {
                var $field = $(this),
                    val = this.value,
                    $thisIndex = parseInt($field.data("idx"), 10);
                if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid")) {
                    this.value = inputQuantity[$thisIndex];
                    return;
                }
                if (val.length > Number($field.attr("maxlength"))) {
                    val = val.slice(0, 5);
                    $field.val(val);
                }
                inputQuantity[$thisIndex] = val;
                if ($(this).val().length == $(this).attr("maxlength")) {
                    $(this).next().focus();
                }
                var num1 = $('#number1').val();
                var num2 = $('#number2').val();
                var num3 = $('#number3').val();
                var number = num1 + num2 + num3;
                $('#signupAeroplanNumberInput').val(number);

            });

            $("#mz-payment-credit-card-number").each(function (i) {
                inputQuantity[i] = this.defaultValue;
                $(this).data("idx", i);
            });
            $("#mz-payment-credit-card-number").on("keyup", function (e) {
                var $field = $(this),
                    val = this.value,
                    $thisIndex = parseInt($field.data("idx"), 10);
                if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid")) {
                    this.value = inputQuantity[$thisIndex];
                    return;
                }
                if (val.length > Number($field.attr("maxlength"))) {
                    val = val.slice(0, 5);
                    $field.val(val);
                }
                inputQuantity[$thisIndex] = val;
                if ($(this).val().length == $(this).attr("maxlength")) {
                    $(this).next().focus();
                }
            });


        });
        //end aeroplan 3 fields value

        $('#aeroplan-member').on("click", function (e) {
            $('#aeroplan-number-available').addClass('hidden');
        });
        $('[data-mz-action="cancelAeroplanEdit"]').on("click", function (e) {
            $('#signupAeroplanSection').removeClass('aeroplanAvailable').addClass('hidden');
            $('#aeroplan-member').attr('checked', false);
            if ($('#signupAeroplanNumberInput') != " ") {
                var aeroplanLastName = $('#aeroplan-beforeLastName').text();
                var aeroplanSetValue = $('#aeroplan-beforedisplay').text();
                var aeroplanNum1 = aeroplanSetValue.substring(0, 3);
                var aeroplanNum2 = aeroplanSetValue.substring(3, 6);
                var aeroplanNum3 = aeroplanSetValue.substring(6, 9);
                $('#number1').val(aeroplanNum1);
                $('#number2').val(aeroplanNum2);
                $('#number3').val(aeroplanNum3);
                $('#signupAeroplanLastName').val(aeroplanLastName);
                $('#aeroplan-number-available').removeClass('hidden');
            } else {
                $('#aeroplan-number-available').addClass('hidden');
            }
        });
        $('[data-mz-action="finishAeroplanEdit"]').on("click", function (e) {
            var aeroplanValidNumber = $('[data-mz-validate-aeroplan-number]').val();
            var aeroplanLastName = $('[data-mz-aeroplan-lastname]').val();
            var finalValue = aeroplanValidNumber.replace(/(\d{1})(.*)(\d{3})/, '*** *** $3');
            $('#aeroplan-beforedisplay').text(aeroplanValidNumber);
            $('#aeroplan-display').text(finalValue);
            $('#aeroplan-beforeLastName').text(aeroplanLastName);
            $('#aeroplan-LastName').text(aeroplanLastName);
            if (aeroplanValidNumber === '' || aeroplanValidNumber.length !== 9) {
                $('#signupAeroplanNumberInput').addClass('is-invalid');
                $('.digit-length').addClass('is-invalid');
                $('[data-mz-validationmessage-for="aeroplanNumber"]').text(Hypr.getLabel('aeroplanNumberMissingInvalid'));
            } else if (aeroplanLastName === '') {
                $('#signupAeroplanLastName').addClass('is-invalid');
                $('[data-mz-validationmessage-for="aeroplanLastName"]').text(Hypr.getLabel('aeroplanNumberMissingInvalid'));
            } else {
                $('#signupAeroplanNumberInput').removeClass('is-invalid');
                $('.digit-length').removeClass('is-invalid');
                $('[data-mz-validationmessage-for="aeroplanNumber"]').text('');
                $('#signupAeroplanLastName').removeClass('is-invalid');
                $('[data-mz-validationmessage-for="aeroplanLastName"]').text('');
                $('#no-aeroplan').addClass('hidden');
                var self = $('.myaccount-shopping-information');
                self.addClass('is-loading');
                window.setTimeout(function () {
                    self.removeClass('is-loading');
                    $('#signupAeroplanSection').removeClass('aeroplanAvailable').addClass('hidden');
                    $('#aeroplan-number-available').removeClass('hidden');
                    $('#aeroplan-member').attr('checked', false);
                }, 5 * 1000);

                $('.myaccount-shopping-information').addClass('is-loading');
                var aeroplanNumSet = $('#signupAeroplanNumberInput').val();
                var aeroplanNumberSet = _.find(accountModel.get('attributes').toJSON(), function (attribute) {
                    return attribute.attributeFQN === Hypr.getThemeSetting('aeroplanMemberNumber');
                });
                accountModel.set('aeroplanMemberNumber', aeroplanNumSet);
                accountModel.getAttributes().then(function (customer) {
                    customer.get('attributes').each(function (attribute) {
                        attribute.set('attributeDefinitionId', attribute.get('id'));
                    });
                    customer.updateAttribute(aeroplanNumberSet.attributeFQN, aeroplanNumberSet.attributeDefinitionId, [aeroplanNumSet]);
                });

                var lAeroplanName = document.getElementById('signupAeroplanLastNameInput');
                if (lAeroplanName) {
                    lAeroplanName.addEventListener('keydown', function (event) {
                        if (event.which === 32) {
                            lAeroplanName.value = lAeroplanName.value.replace(/^\s+/, '');
                            if (lAeroplanName.value.length === 0) {
                                event.preventDefault();
                            }
                        }
                    });
                }


            }

        });

        //Aeroplan Section End

        //Contraction Section start
        $('#contractor').on("change", function (e) {
            var isContractor;
            if (this.checked) {
                isContractor = true;
            } else {
                isContractor = false;
            }
            $('.myaccount-shopping-information').addClass('is-loading');
            var isContractorSet = _.find(accountModel.get('attributes').toJSON(), function (attribute) {
                return attribute.attributeFQN === Hypr.getThemeSetting('isContractor');
            });
            accountModel.set('isContractor', isContractor);
            accountModel.getAttributes().then(function (customer) {
                customer.get('attributes').each(function (attribute) {
                    attribute.set('attributeDefinitionId', attribute.get('id'));
                });
                customer.updateAttribute(isContractorSet.attributeFQN, isContractorSet.attributeDefinitionId, [isContractor]);
            });
        });
        //Contraction Section End

        var $accountSettingsEl = $('#account-settings'),
            $passwordEl = $('#password-section'),
            $orderHistoryEl = $('#account-orderhistory'),
            $paymentMethodsEl = $('#account-paymentmethods'),
            $returnHistoryEl = $('#account-returnhistory'),
            $addressBookEl = $('#account-addressbook'),
            $wishListEl = $('#account-wishlist'),
            $messagesEl = $('#account-messages'),
            $storeCreditEl = $('#account-storecredit'),
            $editModalEl = $('#edit-card-modal'),
            orderHistory = accountModel.get('orderHistory'),
            returnHistory = accountModel.get('returnHistory'),
            $myStore = $("#user-preferred-store");

        var myStore;
        if (localStorage.getItem('preferredStore')) {
            myStore = $.parseJSON(localStorage.getItem('preferredStore'));
        }

        var accountViews = window.accountViews = {
            settings: new AccountSettingsView({
                el: $accountSettingsEl,
                model: accountModel,
                messagesEl: $messagesEl
            }),
            password: new PasswordView({
                el: $passwordEl,
                model: accountModel,
                messagesEl: $messagesEl
            }),
            orderHistory: new OrderHistoryView({
                el: $orderHistoryEl.find('[data-mz-orderlist]'),
                model: orderHistory
            }),
            orderHistoryPagingControls: new PagingViews.PagingControls({
                templateName: 'modules/my-account/order-history-paging-controls',
                el: $orderHistoryEl.find('[data-mz-pagingcontrols]'),
                model: orderHistory
            }),
            orderHistoryPageNumbers: new PagingViews.PageNumbers({
                el: $orderHistoryEl.find('[data-mz-pagenumbers]'),
                model: orderHistory
            }),
            returnHistory: new ReturnHistoryView({
                el: $returnHistoryEl.find('[data-mz-orderlist]'),
                model: returnHistory
            }),
            returnHistoryPagingControls: new PagingViews.PagingControls({
                templateName: 'modules/my-account/order-history-paging-controls',
                el: $returnHistoryEl.find('[data-mz-pagingcontrols]'),
                model: returnHistory
            }),
            returnHistoryPageNumbers: new ReturnHistoryPageNumbers({
                el: $returnHistoryEl.find('[data-mz-pagenumbers]'),
                model: returnHistory
            }),
            paymentMethods: new PaymentMethodsView({
                el: $paymentMethodsEl,
                model: accountModel,
                messagesEl: $messagesEl
            }),
            addressBook: new AddressBookView({
                el: $addressBookEl,
                model: accountModel,
                messagesEl: $messagesEl
            }),
            storeCredit: new StoreCreditView({
                el: $storeCreditEl,
                model: accountModel,
                messagesEl: $messagesEl
            }),
            userStore: new PreferredStoreView({
                el: $myStore,
                model: new Backbone.Model(myStore)
            })

        };

        window.accountViews = accountViews;
        if (HyprLiveContext.locals.siteContext.generalSettings.isWishlistCreationEnabled) accountViews.wishList = new WishListView({
            el: $wishListEl,
            model: accountModel.get('wishlist'),
            messagesEl: $messagesEl
        });

        // TODO: upgrade server-side models enough that there's no delta between server output and this render,
        // thus making an up-front render unnecessary.
        _.invoke(window.accountViews, 'render');

        $("#settings-tab").on('click', function () {
            $(".render-text").empty().append(Hypr.getLabel('myAccount'));
            $('.mz-pagetitle').empty().append(Hypr.getLabel('myAccount'));
        });
        $("#paymentmethods-tab").on('click', function () {
            $(".render-text").empty().append(Hypr.getLabel('paymentInformationText'));
            $(".mz-pagetitle").empty().append(Hypr.getLabel('paymentInformationText'));
        });
        $("#orderhistory-tab").on('click', function () {
            $(".render-text").empty().append(Hypr.getLabel('orderHistory'));
            $(".mz-pagetitle").empty().append(Hypr.getLabel('orderHistory'));
        });
        $("#address-tab").on('click', function() {
            $(".render-text").empty().append(Hypr.getLabel('shippingAddress'));
            $(".mz-pagetitle").empty().append(Hypr.getLabel('shippingAddress'));
        });
        $("#returnHistory-tab").on('click', function() {
            $(".render-text").empty().append(Hypr.getLabel('returnHistory'));
            $(".mz-pagetitle").empty().append(Hypr.getLabel('returnHistory'));
        });

        // Check for Hash in URL and open appropriate tab
        if (window.location.hash) {
            // hash found
            var AccountHash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
            console.log(AccountHash);
            $("#" + AccountHash + "-tab").trigger('click');
        }

        $(document).on('click', '.myaccount-view-detail-link', function (e) {
            e.preventDefault();
            var currentTarget = e.currentTarget;
            var currentParent = $(currentTarget).closest(".listing");
            $(currentParent).find(".myaccount-vieworderdetail-container").removeClass('hidden');
            var viewOrderValue = $(this).attr('href');
            $(viewOrderValue).removeClass('hidden');
            $('.mz-orderlisting').addClass('hidden');
            $('.mz-l-stack-sectiontitle').addClass('hidden');
            $('.mz-pagenumbers').hide();
        });

        $(document).on('click', '.view-all-orders', function (e) {
            e.preventDefault();
            $('.myaccount-vieworderdetail-container').addClass('hidden');
            var orderListing = $(this).attr('href');
            $(orderListing).addClass('hidden');
            $('.mz-orderlisting').removeClass('hidden');
            $('.mz-l-stack-sectiontitle').removeClass('hidden');
            $('.mz-pagenumbers').show();
        });


        $(document).on('orderCancelled', function (event, data) {
            var orderNumber = data.orderNumber,
                orderId = '';
            window.accountViews.orderHistory.model.get('items').models.forEach(function (item) {
                if (item.get('orderNumber') === orderNumber) {
                    item.set('status', 'Cancelled');
                    orderId = item.get('id');
                }
            });
            var ele = '[data-mz-order-id=' + orderId + ']';
            window.accountViews.orderHistory.render();
            $(window).scrollTop($(ele).offset().top);
        });

        $('#account-nav').on('click', function (event) {
            if (isMobile) {
                event.stopPropagation();
                var $ele = $(this);
                var currentElementId = event.target.id;
                if ($ele.hasClass('open')) {
                    $ele.removeClass('open');
                    $ele.find(".mz-scrollnav-item").removeClass("active");
                    $("#account-panels").find(".tab-pane").removeClass("active");
                } else {
                    $ele.addClass('open');
                }
                if (currentElementId === 'settings-tab') {
                    $(".render-text").empty().append(Hypr.getLabel('myAccount'));
                    $('.mz-pagetitle').empty().append(Hypr.getLabel('myAccount'));
                    $ele.find("#settings-tab-link").addClass("active");
                    $("#account-panels").find("#settings").addClass("active");
                } else if (currentElementId === 'paymentmethods-tab') {
                    $(".render-text").empty().append(Hypr.getLabel('paymentInformationText'));
                    $(".mz-pagetitle").empty().append(Hypr.getLabel('paymentInformationText'));
                    $ele.find("#payment-tab-link").addClass("active");
                    $("#account-panels").find("#paymentmethods").addClass("active");
                } else if (currentElementId === 'orderhistory-tab') {
                    $(".render-text").empty().append(Hypr.getLabel('orderHistory'));
                    $(".mz-pagetitle").empty().append(Hypr.getLabel('orderHistory'));
                    $ele.find("#order-history-tab-link").addClass("active");
                    $("#account-panels").find("#orderhistory").addClass("active");
                } else if (currentElementId === 'address-tab') {
                    $(".render-text").empty().append(Hypr.getLabel('contactInformation'));
                    $(".mz-pagetitle").empty().append(Hypr.getLabel('contactInformation'));
                    $ele.find("#address-tab-link").addClass("active");
                    $("#account-panels").find("#addressbookTab").addClass("active");
                }
                else if (currentElementId === 'returnHistory-tab') {
                    $(".render-text").empty().append(Hypr.getLabel('returnHistory'));
                    $(".mz-pagetitle").empty().append(Hypr.getLabel('returnHistory'));
                    $ele.find("#return-history-tab-link").addClass("active");
                    $("#account-panels").find("#returnHistory").addClass("active");
                }
            }

        });
        var preferredStoreDetails = localStorage.getItem('preferredStore') ? $.parseJSON(localStorage.getItem('preferredStore')) : '',
        storeAllowsShipToHome = !!_.find(preferredStoreDetails.attributes,
        function predicate(attribute) {
            return (attribute.fullyQualifiedName === Hypr.getThemeSetting('shipToHomeStoreFlagAttrFQN') &&
                attribute.values[0]);
        });
        if(storeAllowsShipToHome){
            $("#account-nav").find($("#address-tab-link")).removeClass("hidden");
            $("#account-nav").find($("#return-history-tab-link")).removeClass("hidden");
        }else{
            $("#account-nav").find($("#address-tab-link")).addClass("hidden");
        }

    });
});
});