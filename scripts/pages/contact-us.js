require(["modules/jquery-mozu", 
	"underscore", 
	"hyprlive", 
	"modules/backbone-mozu", 
	'modules/api',
	'hyprlivecontext'], 
	function ($, _, Hypr, Backbone, api, HyprLiveContext) {

	$(document).ready(function() {	
		var clearFields = function(){
			$("#radio1, #radio2, #radio3, #topic, #storeLocation, #orderNumber, #firstName, #lastName, #email, #number1, #number2, #number3, #message").val('');
		};
		
		var emailReg = Backbone.Validation.patterns.email;
		var numbersReg = /^[0-9]+$/;
		$(".errors").remove();
		
		$('#contact-button').on('click',function(){
			var errors = false;
			var scrollid1 = false, scrollid2 = false;
			if($('input[type=radio][name=optradio]:checked').length === 0){
				$('[data-mz-validationmessage-for="option"]').text(Hypr.getLabel('categoryMissing'));
				scrollid1 = true;
				errors = true;
			}else{
				$("#topic").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="option"]').text('');
			}

			if (!$("#topic").val() || $("#topic").val() === ""){
				$("#topic").addClass('is-invalid');
				$('[data-mz-validationmessage-for="topic"]').text(Hypr.getLabel('topicMissing'));
				scrollid1 = true;
				errors = true;
			}else{
				$("#topic").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="topic"]').text('');
			}

			if (!$("#firstName").val() || $("#firstName").val() === ""){
				$("#firstName").addClass('is-invalid');
				$('[data-mz-validationmessage-for="firstName"]').text(Hypr.getLabel('fnameMissing'));
				scrollid1 = true;
				errors = true;
			}else if(numbersReg.test($("#firstName").val())){
				$("#firstName").addClass('is-invalid');
				$('[data-mz-validationmessage-for="firstName"]').text(Hypr.getLabel('invalidName'));
				scrollid1 = true;
				errors = true;
			}else{
				$("#firstName").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="firstName"]').text('');
			}

			if (!$("#lastName").val() || $("#lastName").val() === ""){
				$("#lastName").addClass('is-invalid');
				$('[data-mz-validationmessage-for="lastName"]').text(Hypr.getLabel('lnameMissing'));
				scrollid1 = true;
				errors = true;
			}else if(numbersReg.test($("#lastName").val())){
				$("#lastName").addClass('is-invalid');
				$('[data-mz-validationmessage-for="lastName"]').text(Hypr.getLabel('invalidName'));
				scrollid1 = true;
				errors = true;
			}else{
				$("#lastName").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="lastName"]').text('');
			}

			digitLength();
			if (!$("#phoneNumber").val() || $("#phoneNumber").val() === ""){
				$(".phone-numbercontainer input").addClass('is-invalid');
				$('[data-mz-validationmessage-for="phoneNumber"]').text(Hypr.getLabel('phoneNumberMissing'));
				scrollid2 = true;
				errors = true;
			}else{
				$(".phone-numbercontainer input").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="phoneNumber"]').text('');
			}
			
			if($("#email").val() === ""){
				$("#email").addClass('is-invalid');
				$('[data-mz-validationmessage-for="email"]').text(Hypr.getLabel('emailMissing'));
				scrollid2 = true;
				errors = true;                            
			}else if(!emailReg.test($("#email").val())){
				$("#email").addClass('is-invalid');
				$('[data-mz-validationmessage-for="email"]').text(Hypr.getLabel('emailMissing'));
				scrollid2 = true;
				errors = true;
			}else{
				 $("#email").removeClass('is-invalid');
				 $('[data-mz-validationmessage-for="email"]').text('');
			}

			if(!$('#g-recaptcha-response').val()){
				$('[data-mz-validationmessage-for="captcha"]').text(Hypr.getLabel('missingCaptcha'));
				scrollid2 = true;
				errors = true;
			}else{
				$('[data-mz-validationmessage-for="captcha"]').text('');
			}
			 digitLength();
			 
			 if(!errors) {
				 $('.contact-us-form').addClass('is-loading');
				 var topicRadioVal;
				 if ($("input[name='optradio']:checked").val() == "option1") {
				 	topicRadioVal = "Store Experience";
				 } else if ($("input[name='optradio']:checked").val() == "option2") {
				 	topicRadioVal = "Online Ordering";
				 } else if ($("input[name='optradio']:checked").val() == "option3") {
				 	topicRadioVal = "Corporate";
				 }
				 var data = {
				 	 topicHeader:topicRadioVal,
				 	 topic:$("#topic").val(),
				 	 storeLocation:$("#storeLocation").val(),
				 	 orderNumber:$("#orderNumber").val(),
				 	 firstName:$("#firstName").val(),
					 lastName:$("#lastName").val(),
					 email:$("#email").val(),
					 phoneNumber: $("#phoneNumber").val(),
					 customerMessage: $("#message").val()
				 };
				 $.ajax({
					method: 'POST',
            		contentType: 'application/json; charset=utf-8',
            		url: Hypr.getThemeSetting('formPostingUrl') + 'customerService',
            		data: JSON.stringify(data),     
            		success:function(response){
            			$('.sucess-email').show();
     	        	   	$('.contact-us-form').removeClass('is-loading');
     	        	   	setTimeout(location.reload.bind(location), 2000);
     	        	   	clearFields();
        			},
        			error: function (error) {
        				$('.error-email').show();
        			}
				 });
			 }else{
				  if (scrollid1) {
					$('body').animate({
	                  	scrollTop: $("#form-question").offset().top
	                },800);
	            } else if (!scrollid1 && scrollid2) {
	            	$('body').animate({
	                  	scrollTop: $("#firstName").offset().top
	                },800);
	            }
			}			
		});
		
		  $(".digit-length").keypress(function(){
			  digitLength();
		  });
		  
		  
		  var digitLength = function(){ 
			//starts phonenumber 3 and 4 fields value
		        var inputQuantity = [];
		        $(function() {
		          $(".digit-length").each(function(i) {
		            inputQuantity[i]=this.defaultValue;
		             $(this).data("idx",i);
		          });
		          $(".digit-length").on("keyup", function (e) {
		            var $field = $(this),
		                val=this.value,
		                $thisIndex=parseInt($field.data("idx"),10); 
		            if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
		                this.value = inputQuantity[$thisIndex];
		                return;
		            } 
		            if (val.length > Number($field.attr("maxlength"))) {
		              val=val.slice(0, 5);
		              $field.val(val);
		            }
		            inputQuantity[$thisIndex]=val;
		            if($(this).val().length==$(this).attr("maxlength")){
		                $(this).next().focus();
		            }
		            var num1 = $('#number1').val();
		            var num2 = $('#number2').val();
		            var num3 = $('#number3').val();
		            var number = num1 + num2 + num3;
		            $('#phoneNumber').val(number);
		            
		          });      
		        });
			
		    };

		$(function() {
	    // Group the select's options by the radio they correspond to
		    var options = {},
		        radios = $("#Group1 :radio");
		    options[radios.eq(0).val()] = [];
		    options[radios.eq(1).val()] = [];
		    options[radios.eq(2).val()] = [];
		    $("#Group2 select option").each(function(i, el) {
		        var e = $(el);
		        if (i < 8)
		            options[radios.eq(0).val()].push(e);
		        else if (i < 17) //38
		            options[radios.eq(1).val()].push(e);
		        else
		            options[radios.eq(2).val()].push(e);
		        e.remove();
		    });

		    $("#online-shopping").on("click", function() {
		    	$('input[name="optradio"][value="option2"]').trigger('click');
		    	//$('input[name="optradio"][value="option2"]').attr('checked','checked');
		    	$('input[name="optradio"][value="option1"]').attr('checked',false);
		    	$('input[name="optradio"][value="option3"]').attr('checked',false);
		    	$("#Group2 select").empty();
		        var arr = options[$('input#radio2.form-check-input').val()];
		        for (var i = 0; i < arr.length; i++)
		        $("#Group2 select").append(arr[i]);
		    	$("#Group2 #topic option:first").attr('selected','selected');

		    	$('#topic').find('option[selected="selected"]').each(function(){
				    $(this).prop('selected', true);
				});
		    	$('body').animate({
                  	scrollTop: $("#contact-us-form").offset().top
                },800);
		    });
		    
		    // On radio's change, empty the select and append the options that correspond to the selected radio
		    $("#Group1 :radio").on("change", function() {
		        $("#Group2 select").empty();
		        var arr = options[$(this).val()];
		        for (var i = 0; i < arr.length; i++)
		        $("#Group2 select").append(arr[i]);
		    	$("#Group2 #topic option:first").attr('selected','selected');

		    	$('#topic').find('option[selected="selected"]').each(function(){
				    $(this).prop('selected', true);
				});
		    });

		    $( window ).on("load", function(e) {
		    	e.preventDefault();
		        var arr = options[$('input#radio1.form-check-input').val()];
		        for (var i = 0; i < arr.length; i++) {
		        	$("#Group2 select").append(arr[i]);
		        }
		        $("#Group2 #topic option:first").attr('selected','selected');

		    	$('#topic').find('option[selected="selected"]').each(function(){
				    $(this).prop('selected', true);
				});
		        return false;
		    });
		});

		$("input[name$='optradio']").click(function() {
	        var test = $(this).val();
	        $("div.store-detail").hide();
	        if (test === "option2") {
	        	$("#order-number").show();
	        } else {
	        	$("#store-location").show();
	        }
	    });
	});

	var selectedStore = localStorage.getItem('preferredStore') && JSON.parse(localStorage.getItem('preferredStore'));
	var enabledStates = HyprLiveContext.locals.themeSettings.selectedState;
	var selectedState = selectedStore && selectedStore.address && selectedStore.address.stateOrProvince;
	var apiContext = require.mozuData('apicontext');
	var chatUrl = apiContext.headers['x-vol-locale'] == "fr-CA" ? HyprLiveContext.locals.themeSettings.chatFrenchUrl : HyprLiveContext.locals.themeSettings.chatEnglishUrl;
	var chatTitle = apiContext.headers['x-vol-locale'] == "fr-CA" ? 'Savoir. Faire.' : 'Here’s How';
	
	if(!selectedState || (!_.contains(enabledStates, selectedState))) {
		$('.model_agent').show();
	} else {
		$('.model_agent').hide();
	}

	$('.model_agent').click(function () {
		if(HyprLiveContext.locals.pageContext.isMobile || HyprLiveContext.locals.pageContext.isTablet) {
			window.open(chatUrl, '_blank');
		} else {
			window.open(chatUrl, chatTitle, 'location=yes,height=600,width=350');
		}
	});
});