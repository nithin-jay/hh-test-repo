require(["modules/jquery-mozu", 
	"underscore", 
	"hyprlive", 
	"modules/backbone-mozu", 
	'modules/api',
	'hyprlivecontext'], 
	function ($, _, Hypr, Backbone, api, HyprLiveContext) {
	$(document).ready(function() {	
		$("h1.main-page-title").hide();
		if($('body').find('h1.title-for-print').length !== 0){
        	$("h1.main-page-title").remove(); 
    	}else{
    		$("h1.main-page-title").show(); 
    	}

    	$("#beautitone").change(function(){
		    console.log($("#beautitone option:selected")[0].value);
        	window.location.href = $("#beautitone option:selected")[0].value;
		});

		if($(window).width() > 767 && $(window).width() < 1025){
			var $lis = $(".beautitone-list li.dropdown-submenu");
	    	$lis.hide().filter(':lt(5)').show();
	    	$(".beautitone-list li.dropdown-submenu:nth-last-child(3)").css( 
              "border-right", "none");
	    }
    	$(document).on('touchstart click',".tab-dropdown-submenu-open", function(e){
    		if($(window).width() > 767 && $(window).width() < 1025){
		        e.preventDefault();
                e.stopImmediatePropagation();
                $(this).find(".sub-menu").toggle(); 
            }
		});

		$(".dropdown-submenu").mouseenter(function(){
			$(this).parents('div.mz-cms-content').addClass('nav-contain');
			$(this).parents('div.mz-cms-content').parents('div.beauti-tone-navigation').find('div#mz-drop-zone-static-richtext').css({'margin-top': "17px"});
		});
		$(".dropdown-submenu").mouseleave(function(){
			$(this).parents('div.mz-cms-content').removeClass('nav-contain');
			$(this).parents('div.mz-cms-content').parents('div.beauti-tone-navigation').find('div#mz-drop-zone-static-richtext').css({'margin-top': ""});
		});
    	
    	$('.excerpt-tile-image, .excerpt-tile-title-link').click(function(e) {
    		e.preventDefault();
    		var locale = require.mozuData('apicontext').headers['x-vol-locale'];
	         var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
	         locale = locale.split('-')[0];
	         var currentLocale = '';
	         if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
	             currentLocale = locale === 'fr' ? '/fr' : '/en';
	         }
	         window.location.href = currentLocale + '/c/' + $(e.currentTarget).attr('data-category-code');
    	});

		if ((this.pathname && location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'')) || (this.hostname && location.hostname === this.hostname)) {   
	      	var target = $(this.hash);
	      	var headerHeight = 120;
	      	target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      	if (target.length) {
		        $('html,body').stop().animate({
		          scrollTop: target.offset().top - 125
		        }, 'linear');
	        
	      	}
	    }

	});
});