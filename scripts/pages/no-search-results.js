require(['modules/jquery-mozu',
     	"modules/backbone-mozu",
    	'underscore',
    	"modules/views-collections",
    	"hyprlivecontext",
    	"modules/api",
    	'hyprlive'], function($, Backbone, _, CollectionViewFactory, HyprLiveContext, api, Hypr) {
	
	//No search view created
	var NoSearchAddressView = Backbone.MozuView.extend({
    	templateName: "modules/common/nosearch-address-view", 
    	additionalEvents: {
			"click #changeStorelink": "changeStore"
		},
    	initialize: function(){ 
    		if(localStorage.getItem('preferredStore')){
	    		var preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
	            this.model.set("preferredStore", preferredStore); 
	            $('#store-map-container').addClass("store-selected");
				var homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId"),
					isHomeFurnitureSite = false,
					contextSiteId = require.mozuData('apicontext').headers["x-vol-site"];
				
				if (homeFurnitureSiteId === contextSiteId) {
					isHomeFurnitureSite = true;
				}
				//start map view 
				var map, marker, currentIcon, storeType;
				
				if(localStorage.getItem('preferredStore')){
					LoadResources.load("/scripts/unwired-gl.js", function(){
						preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
						var attributes = preferredStore.attributes;
						storeType =_.findWhere(attributes, {"fullyQualifiedName": Hypr.getThemeSetting("isStoreType")});
						if(storeType && storeType.values[0] === "Home Hardware"){
							$("#store-type").removeClass("hidden");
							currentIcon = 'url(' + '../../resources/images/marker-' + (isHomeFurnitureSite ? 'hf' : 'hh') + '.png'+')';
						}else{
							currentIcon = 'url(../../resources/images/marker-selection.png)';
						}
			
						unwired.key = mapboxgl.accessToken = Hypr.getThemeSetting('mapApiKey');
						//Define the map and configure the map's theme
						
						map = new mapboxgl.Map({
							container: document.getElementById("nosearch-store-map"), // container id
							style: unwired.getLayer("streets"), // stylesheet location
							center: [parseFloat(preferredStore.geo.lng), parseFloat(preferredStore.geo.lat)], // starting position [lng, lat]
							zoom: Hypr.getThemeSetting('storeDetailsMapZoomLevel') // starting zoom
						});
	
						// create the marker and add to map
						var el = document.createElement('div');
						
							el.style.backgroundImage = currentIcon;
							el.style.width = isHomeFurnitureSite ? '27px' : '33px'; 
							el.style.height = isHomeFurnitureSite ? '28px' : '40px';
							el.style.backgroundSize = 'cover';
						
						marker = new mapboxgl.Marker(el).setLngLat([parseFloat(preferredStore.geo.lng), parseFloat(preferredStore.geo.lat)]).addTo(map);
					});
				}
				//end map view
    		}
		},
		changeStore: function(e){
			e.preventDefault();
		 	var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            locale = locale.split('-')[0];
            var currentLocale = locale === 'fr' ? '/fr' : '/en';
		 	var pageContext = require.mozuData('pagecontext');
		 	if(window.location.pathname.length > 1){
		 	 if(pageContext.pageType=='search'){
		 		 var url = window.location.search.split('&')[0];
			 		window.location.href= currentLocale + "/store-locator?returnUrl=" + window.location.pathname+url;
		 		 }
		 	 else{
		 		window.location.href= currentLocale + "/store-locator?returnUrl=" + window.location.pathname;
		 			 }
		 	}else{
		 		window.location.href= currentLocale + "/store-locator";
		 	}
		},
		render:function(){
			Backbone.MozuView.prototype.render.apply(this); 
		}
    });	
	// No search result view created - Spellcheck
	var didYouMeanView = Backbone.MozuView.extend({
		templateName: null, 
		additionalEvents: {
			"click .selectCookie" :"setCookie"
		},
		initialize: function(){ 
			if($.cookie("spellChecker")){
	    		var spellChecker = $.parseJSON($.cookie("spellChecker"));
	            this.model.set("spellChecker", spellChecker); 
				
				switch(spellChecker.action){
					case "spelling":
							this.templateName = "modules/search/didyoumean/spelling";	
					break;
					case "redirect":
							window.location.href = spellChecker.data;
					break; 
					case "suggest":
							this.templateName = "modules/search/didyoumean/suggest";
							var spellArr = spellChecker.data.split(",");
							spellArr = spellArr.map(function(el) { return el.trim(); });
							spellChecker.data = spellArr.join();	
					break;
					case "message":
							this.templateName = "modules/search/didyoumean/no-search-message";
					break;
					default:
							this.templateName = "modules/search/didyoumean/no-search-results";
					break;
				}
				return spellChecker;
			}
		},
		saveSearchKeyword : function(e){
			var searchKeyword = $(e.currentTarget).attr("data-search");
			document.cookie = "stopwords="+encodeURIComponent(searchKeyword)+";path=/;Secure;SameSite=None";	
		}, 

		render:function(){
			Backbone.MozuView.prototype.render.apply(this); 
		}
	});
	
	var LoadResources = {
		load: function(src, callback) {
		  var script = document.createElement('script'),
			  loaded;
		  script.setAttribute('src', src);
		  if (callback) {
			script.onreadystatechange = script.onload = function() {
			  if (!loaded) {
				callback();
			  }
			  loaded = true;
			};
		  }
		  document.getElementsByTagName('head')[0].appendChild(script);
		}
	};
	//Call function for search view
	var noSearchAddressView = new NoSearchAddressView({
		el:$("#store-address"),
		model:new Backbone.MozuModel()
	});
	if(localStorage.getItem('preferredStore')){
		noSearchAddressView.render();
	}
	didYouMeanView = new didYouMeanView({
		el:$("#spellChecker"),
		model:new Backbone.MozuModel()
	});
	if($.cookie("spellChecker")){
		setTimeout(function(){
			didYouMeanView.render();
		},2000);
	}
	$(document).ready(function(){
		var searchQueryString = decodeURIComponent(location.search.split("query=")[1].split("&")[0]);
			if(!$.cookie('QueryWithoutStopwords')){
				$("#nosearch-stopword-result").text('"'+searchQueryString.replace(/\+/g, " ")+'"');
			}else{
				if($.cookie('QueryWithoutStopwords') === searchQueryString){
					if($.cookie('stopwords')){
						$("#nosearch-stopword-result").text('"'+decodeURIComponent($.cookie('stopwords')).replace(/\+/g, " ")+'"');
					}
				}else{
					$("#nosearch-stopword-result").text('"'+searchQueryString.replace(/\+/g, " ")+'"');
				}
			}
			$('.mz-searchbox-input').click(function(){
				$(".mz-searchbox-input").val('');
			});
	});
});