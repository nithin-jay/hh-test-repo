require(["modules/jquery-mozu", 
	"underscore", 
	"hyprlive", 
	"modules/backbone-mozu", 
	'modules/api',
	'hyprlivecontext'], 
	function ($, _, Hypr, Backbone, api, HyprLiveContext) {
	var clearFields = function(){
		$("#firstName, #lastName, #emailAddress, #phoneNumber1, #phoneNumber2, #phoneNumber3, #projectDetails, #projectTools, #equipmentRequire, #rentalDuration").val('');
	};
	$(document).ready(function() {	

		var emailReg = Backbone.Validation.patterns.email;
		
		var homeRentalsStore = $.cookie('homeRentalsStore');
		
		clearFields();
		
		var locale = require.mozuData('apicontext').headers['x-vol-locale'];
		
		var homeRentalsView = Backbone.MozuView.extend({
			templateName: 'modules/common/home-rentals-view'	
		});
		
		if (homeRentalsStore) {
			homeRentalsStore = JSON.parse(homeRentalsStore);
			homeRentalsStore.locale = locale;
			var HomeRentalsView = new homeRentalsView({
			       el: $('#home-rentals-store'),
			       model: new Backbone.Model(homeRentalsStore)
				});
			HomeRentalsView.render();
		} else if (HyprLiveContext.locals.pageContext.purchaseLocation && HyprLiveContext.locals.pageContext.purchaseLocation.code){
			var storeCode = HyprLiveContext.locals.pageContext.purchaseLocation.code;
			var filtersString = "properties.storeServices in['Home Rentals'] and properties.documentKey eq " + storeCode;
			var locationList = Hypr.getThemeSetting("storeDocumentListFQN");
			
			api.get('documentView', {
				listName: locationList,
				filter:filtersString
			}).then(function(response) {		
				if(response.data.items.length > 0) {
					
					var item = response.data.items[0];
					homeRentalsStore = {
							code: item.name,
							homeRentalsEmail: item.properties.homeRentalsEmail,
							name: item.properties.storeName,
							address2: item.properties.address2,
							city: item.properties.city,
							province: item.properties.province[0],
							postalCode:item.properties.postalCode,
							phone:item.properties.phone,
							locale: locale
					};
					
					var HomeRentalsView = new homeRentalsView({
					       el: $('#home-rentals-store'),
					       model: new Backbone.Model(homeRentalsStore)
						});
					HomeRentalsView.render();
				}
			});

		}
		
				
		$('#home-rentals-button').on('click',function(){
			var errors = false;
			var scrollid1 = false, scrollid2 = false;
			if (!$("#firstName").val() || $("#firstName").val().trim() === ""){
				$("#firstName").addClass('is-invalid');
				$('[data-mz-validationmessage-for="firstName"]').text(Hypr.getLabel('fnameMissing'));
				errors = true;
				scrollid1 = true;
			}else{
				$("#firstName").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="firstName"]').text('');
			}
			
			if (!$("#lastName").val() || $("#lastName").val().trim() === ""){
				$("#lastName").addClass('is-invalid');
				$('[data-mz-validationmessage-for="lastName"]').text(Hypr.getLabel('lnameMissing'));
				errors = true;
				scrollid1 = true;
			}else{
				$("#lastName").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="lastName"]').text('');
			}
		
			if($('input[name="prefererd-method"]:checked').val() == 'emailAddress') {
				$('[data-mz-validationmessage-for="phoneNumber"]').text('');
				$(".phone-numbercontainer input").removeClass('is-invalid');
				if($("#emailAddress").val().trim() === ""){
					$("#emailAddress").addClass('is-invalid');
					$('[data-mz-validationmessage-for="emailAddress"]').text(Hypr.getLabel('emailMissing'));
					errors = true;  
					scrollid1 = true;                     
				}else if(!emailReg.test($("#emailAddress").val())){
					$("#emailAddress").addClass('is-invalid');
					$('[data-mz-validationmessage-for="emailAddress"]').text(Hypr.getLabel('emailMissing'));
					errors = true;
					scrollid1 = true; 
				}else{
					 $("#emailAddress").removeClass('is-invalid');
					 $('[data-mz-validationmessage-for="emailAddress"]').text('');
				}
			}
			
			 digitLength();
			 if($('input[name="prefererd-method"]:checked').val() == 'phoneNumber') {
				 $('[data-mz-validationmessage-for="emailAddress"]').text('');
				 $("#emailAddress").removeClass('is-invalid');
				 if (!$("#phoneNumber").val() || $("#phoneNumber").val() === ""){
					$(".phone-numbercontainer input").addClass('is-invalid');
					$('[data-mz-validationmessage-for="phoneNumber"]').text(Hypr.getLabel('phoneNumberMissing'));
					errors = true;
					scrollid1 = true;
				}else{
					$(".phone-numbercontainer input").removeClass('is-invalid');
					$('[data-mz-validationmessage-for="phoneNumber"]').text('');
				}
			 }
						
			
			if(!$("#projectDetails").val() || $("#projectDetails").val().trim() === ""){
				$("#projectDetails").addClass('is-invalid');
				$('[data-mz-validationmessage-for="projectDetails"]').text(Hypr.getLabel('messageMissing'));
				errors = true;
				scrollid2 = true;
			}else{
				$("#projectDetails").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="projectDetails"]').text('');
			}
			
			if(!$("#projectTools").val() || $("#projectTools").val().trim() === ""){
				$("#projectTools").addClass('is-invalid');
				$('[data-mz-validationmessage-for="projectTools"]').text(Hypr.getLabel('messageMissing'));
				errors = true;
				scrollid2 = true;
			}else{
				$("#projectTools").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="projectTools"]').text('');
			}

			if(!$("#equipmentRequire").val() || $("#equipmentRequire").val().trim() === ""){
				$("#equipmentRequire").addClass('is-invalid');
				$('[data-mz-validationmessage-for="equipmentRequire"]').text(Hypr.getLabel('messageMissing'));
				errors = true;
				scrollid2 = true;
			}else{
				$("#equipmentRequire").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="equipmentRequire"]').text('');
			}

			if(!$("#rentalDuration").val() || $("#rentalDuration").val().trim() === ""){
				$("#rentalDuration").addClass('is-invalid');
				$('[data-mz-validationmessage-for="rentalDuration"]').text(Hypr.getLabel('messageMissing'));
				errors = true;
				scrollid2 = true;
			}else{
				$("#rentalDuration").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="rentalDuration"]').text('');
			}

			if(!$('#g-recaptcha-response').val()){
				$('[data-mz-validationmessage-for="captcha"]').text(Hypr.getLabel('missingCaptcha'));
				errors = true;
			}else{
				$('[data-mz-validationmessage-for="captcha"]').text('');
			}
			
			if(!homeRentalsStore) {
				$('[data-mz-validationmessage-for="homeRentalsStore"]').text(Hypr.getLabel('homeRentalsStoreErorMessage'));
				errors = true;
				scrollid1 = true;
			} else {
				$('[data-mz-validationmessage-for="homeRentalsStore"]').text('');
			}
			
			 digitLength();
			 
			if(!errors) {
				 $('#home-rentals-form').addClass('is-loading');
				 var data = {
						 firstName:$("#firstName").val().trim(),
						 lastName: $("#lastName").val().trim(),
						 emailAddress: $("#emailAddress").val().trim(),
						 phoneNumber: $('#phoneNumber').val(),
						 projectDetails: $("#projectDetails").val(),
						 projectTools: $("#projectTools").val(),
						 equipmentRequire: $("#equipmentRequire").val(),
						 rentalDuration: $("#rentalDuration").val(),
						 preferredContactMethod: $('input[name="prefererd-method"]:checked').val(),
						 homeRentalsStoreCode: homeRentalsStore.code,
						 homeRentalsStoreName: homeRentalsStore.name,
						 homeRentalsStoreEmail: homeRentalsStore.homeRentalsEmail,
						 locale: homeRentalsStore.locale
						};
				 $.ajax({
					method: 'POST',
            		contentType: 'application/json; charset=utf-8',
            		url: Hypr.getThemeSetting('formPostingUrl') + 'home-rental',
            		data: JSON.stringify(data),     
            		success:function(response){            			
            			$('#home-rentals-store').hide();
            			$('.home-rentals-form').hide();
            			$('#home-rentals-form').removeClass('is-loading');
            			$('.sucess-email-template').show();
            			$('html, body').scrollTop($('#goto-thankyou').offset().top);
            			var storeDetailStr = "<p class='agenda-bold store-name'>" + homeRentalsStore.name +"</p>"+
							"<p class=''address'>" + homeRentalsStore.address2 +"</p>"+
							"<p class='address'>" + homeRentalsStore.city +", " + homeRentalsStore.province +"</p>"+
							"<p class='address'>" + homeRentalsStore.postalCode +"</p>"+
							"<p class='address'>" + homeRentalsStore.phone +"</p>";
            				$(".store-info-details").append(storeDetailStr);
        			},
        			error: function (error) {
        				$('.error-email').show();
        			}
				 });
			} else {
				if (scrollid1) {
					$('body').animate({
	                  	scrollTop: $("#home-rentals-store").offset().top
	                },800);
	            } else if (!scrollid1 && scrollid2) {
	            	$('body').animate({
	                  	scrollTop: $("#message-details").offset().top
	                },800);
	            }
			}
		});
		
		$(".digit-length").keypress(function(){
			  digitLength();
		  });
		  
		  
		  var digitLength = function(){ 
			//starts phonenumber 3 and 4 fields value
		        var inputQuantity = [];
		        $(function() {
		          $(".digit-length").each(function(i) {
		            inputQuantity[i]=this.defaultValue;
		             $(this).data("idx",i);
		          });
		          $(".digit-length").on("keyup", function (e) {
		            var $field = $(this),
		                val=this.value,
		                $thisIndex=parseInt($field.data("idx"),10); 
		            if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
		            	$('[data-mz-validationmessage-for="phoneNumber"]').text(Hypr.getLabel('invalidPhoneNumber'));
		                this.value = inputQuantity[$thisIndex];
		                return;
		            } 
		            if (val.length > Number($field.attr("maxlength"))) {
		              val=val.slice(0, 5);
		              $field.val(val);
		            }
		            inputQuantity[$thisIndex]=val;
		            if($(this).val().length==$(this).attr("maxlength")){
		                $(this).next().next().focus();
		            }
		            
		          });    
		          	var num1 = $('#phoneNumber1').val();
		            var num2 = $('#phoneNumber2').val();
		            var num3 = $('#phoneNumber3').val();
		            var number = num1 + num2 + num3;
		            if(num1 && num2 && num3) {
		            	$('#phoneNumber').val(number);
		            } else {
		            	$('#phoneNumber').val('');
		            }
		        });
			
		    };
		
		
		  	  
	});
});