require([
    "modules/jquery-mozu",
    "underscore",
    "hyprlive",
    "modules/backbone-mozu",
    "modules/cart-monitor",
    "modules/models-product",
    "modules/views-productimages",
    "hyprlivecontext",
    'modules/api',
    'modules/check-nearby-store',
    'vendor/jquery/moment',
    'vendor/timezonesupport/timezonesupport',
    'modules/cookie-utils',
    'modules/views-estimate-shipping-modal',
    'modules/models-sth-postal-code-cookie',
    'modules/models-cart',
    'modules/models-shipping-time-lines',
    'modules/models-oversize-charge-calculator',
    'slick',
    'cloud',
    'shim!vendor/jquery/rrssb.min[jQuery=jquery]'
],
function ($, _, Hypr, Backbone, CartMonitor, ProductModels, ProductImageViews, HyprLiveContext, api,
              CheckNearbyStoreModal, moment, timeZoneSupport, CookieUtils, EstimateShippingModalView, ShippingPostalCodeModel, CartModels, ShippingTimelineModel, OversizeChargeCalculatorModel) {
    var user = require.mozuData('user');
    var apiContext = require.mozuData('apicontext');
    var breadCrumbCategory = [];
    var filtersString = "",
        isCompareCategoryIdAvailable = false,
        productMappedToCompareCategoryIdSet = [],
        today = new Date(),
        isHomeFurnitureSite = false,
        isMapboxEnabled = false,
        checkQuantity = false,
        maxQuantityAllowed,
        estimateShippingModalView = null;
    //delete this variable once pdp arc is in action
    var usePDPArcFunction = Hypr.getThemeSetting("usePDPArcFunction");
    var activeWarehousesForSTH = Hypr.getThemeSetting("warehousesForSTH");
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        ua = navigator.userAgent.toLowerCase(),
        wareHouseInventory,
        warehouseInventoryCount,
        locationInventoryCount,
        isMobile = navigator.userAgent.match(/(iPhone)|(iPod)|(android)|(webOS)/i);

    if (Hypr.getThemeSetting("homeFurnitureSiteId") === require.mozuData('apicontext').headers["x-vol-site"]) {
        filtersString = "properties.storeType in['Home Furniture']";
        isHomeFurnitureSite = true;
    } else {
        if (window.location.search.indexOf('storeFilter') < 0)
            filtersString = "properties.storeType in['Home Hardware','Home Building Centre', 'Home Hardware Building Centre'] ";
    }

    /* Adding display online filter */
    if (!filtersString) {
        filtersString = "properties.displayOnline eq true ";
    } else {
        filtersString += "and properties.displayOnline eq true ";
    }

    var contextSiteId = apiContext.headers["x-vol-site"];
    var homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId");

    var ProductView = Backbone.MozuView.extend({
        templateName: 'modules/product/product-detail',
        additionalEvents: {
            "change [data-mz-product-option]": "onOptionChange",
            "blur [data-mz-product-option]": "onOptionChange",
            "click [data-mz-product-option]": "onOptionChange",
            "change [data-mz-value='quantity']": "onQuantityChange",
            "click [data-mz-value='qty-increment']": "onQuantityChange",
            "click [data-mz-value='qty-decrement']": "onQuantityChange",
            "keyup input[data-mz-value='quantity']": "onQuantityChange",
            "blur input[data-mz-value='quantity']": "onQuantityChange",
            "click .changePreferredStore": "changePreferredStore",
            "click #checkNearByStore": "checkNearbyStores",
            "click .show-oos": "showOosContainer",
            "click .close": "closeOosContainer"
        },
        render: function () {
            var me = this;
            $("#main-content-loader").hide();
            //set EHF value in
            var ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
            if(!me.model.get('isEhfValueSet')){
                var ehfOption = me.model.get('options').findWhere({'attributeFQN': ehfAttributeFQN});
                if (ehfOption && me.model.get('ehfValue')) {
                    ehfOption.set('shopperEnteredValue', me.model.get('ehfValue'));
                    ehfOption.set('value', me.model.get('ehfValue'));
                    me.model.set('isEhfValueSet', true);
                }
            }
            if (navigator.userAgent.match(/(iPhone)|(iPod)|(android)|(webOS)|(ipad)/i)) {
                me.model.set("isDesktop", false);
            } else {
                me.model.set("isDesktop", true);
            }
            Backbone.MozuView.prototype.render.apply(this);
            this.$('[data-mz-is-datepicker]').each(function (ix, dp) {
                $(dp).dateinput().css('color', Hypr.getThemeSetting('textColor')).on('change  blur', _.bind(me.onOptionChange, me));
            });
            var prodDesc = $(".collapse-section").find("ul");
            _.each(prodDesc, function (e) {
                var listElm = $(e).find("li");
                if (listElm.length == 1) {
                    $(e).addClass("no-column-count");
                }
            });
            setTimeout(function () {
                $(".business-messages").removeClass('hidden');
                /* Info Popover */
                $('[data-toggle="popover"]').popover();
            }, 700);
            var dataAttribute = $('.mz-productdetail-btns-container').find('button').attr('data-mz-action');
            if (dataAttribute != 'addToCartForPickup') {
                $('.mz-productdetail-btns-container').addClass('full-width-btn');
            }
            var productInfo = _.findWhere(me.model.attributes.properties, {'attributeFQN': Hypr.getThemeSetting('productInformation')});
            var instructionManuals = _.findWhere(me.model.attributes.properties, {'attributeFQN': Hypr.getThemeSetting('instructionManuals')});
            var warranty = _.findWhere(me.model.attributes.properties, {'attributeFQN': Hypr.getThemeSetting('warranty')});
            var specifications =  _.findWhere(me.model.attributes.properties, {'attributeFQN': Hypr.getThemeSetting('specifications')});
            if(productInfo && productInfo.values[0].value || instructionManuals && instructionManuals.values[0].value || warranty && warranty.values[0].value || specifications && specifications.values[0].value) {
                $(".infonguides").show();
                $("#documentsGuides").show();
            } else {
                $(".infonguides").hide();
                $("#documentsGuides").hide();
            }
        },
        showOosContainer: function(e) {
            e.preventDefault();
                if (!$(".certona-regular-oos_rr").hasClass("hidden")) {
                    $("#oosProducts").removeClass("hide");
                    $(".temprory-outofstock").removeClass("hide-oos-products");
                    $(".temprory-outofstock").addClass("hide");
                    $(".oos-products").css("border-bottom", "1px solid #c7c7c7");
                    $(".oos-close-button").removeClass("hide");
                }
                if (navigator.userAgent.match(/(iPhone)|(iPod)|(android)|(webOS)/i)) {
                    $(".oos-products").css("display", "block !important");
                    $(".temprory-outofstock").removeClass("hide");
                    $(".temprory-outofstock").css("float", "none");
                }
        },
        closeOosContainer: function(e){
            $(".temprory-outofstock").removeClass("hide");
            $(".temprory-outofstock").addClass("hide-oos-products");
            $(".oos-products").addClass("hide");
        },
        onOptionChange: function (e) {
            return this.configure($(e.currentTarget));
        },
        onQuantityChange: _.debounce(function (e) {
            if (isNaN($(e.currentTarget).val()) || $(e.currentTarget).val() < 0) {
                e.preventDefault();
                $(e.currentTarget).val(1);
                $(".decrement").addClass("is-disabled");
                this.checkQuantity(1, $(e.currentTarget));
            } else {
                if ($('#add-to-wishlist').prop('disabled')) {
                    $('#add-to-wishlist').text(Hypr.getLabel('saveToList'));
                    $('#add-to-wishlist').prop('disabled', false);
                }
                var field = $(e.currentTarget).attr('data-mz-value'); //this field is used to increment/decrement the quantity
                var quantity = $(e.currentTarget).siblings('.mz-productdetail-qty').val();

                if (field === 'qty-decrement') {
                    $(e.currentTarget).siblings('.mz-productdetail-qty').val(parseInt(quantity, 10) - 1);
                }
                if (field === 'qty-increment') {
                    $(e.currentTarget).siblings('.mz-productdetail-qty').val(parseInt(quantity, 10) + 1);
                }
                var $qField = "";
                if (field != 'quantity') {
                    $qField = $(e.currentTarget).siblings('.mz-productdetail-qty');
                } else {
                    $qField = $(e.currentTarget);
                }

                var newQuantity = parseInt($qField.val(), 10);

                if (newQuantity <= 1 || isNaN(newQuantity)) {
                    $qField.val(1);
                    newQuantity = 1;
                }

                if (!isNaN(newQuantity) && newQuantity !== this.model.get('quantity')) {
                    if (!this.model.get('maxQuantityAvailable')) this.setMaxQuantityAllowed();
                    this.checkQuantity(newQuantity, $qField);
                }
            }
        }, 500),
            setMaxQuantityAllowed: function(){
                var warehouseInventoryCount = this.model.get('warehouseInventoryCount'),
                    locationInventoryCount = this.model.get('locationInventoryCount') && this.model.get('locationInventoryCount') > 0 ? this.model.get('locationInventoryCount') : 0,
                    isBohStore = this.model.get('bohStore') ? this.model.get('bohStore') : false,
                    bopisAvailableInventory = isBohStore ? warehouseInventoryCount + locationInventoryCount : warehouseInventoryCount,
                    pluckedWarehouseInv = this.model.get('wareHouseInventory') ? _.pluck(this.model.get('wareHouseInventory'), 'stockAvailable') :[],
                    shipMaxAllowed = pluckedWarehouseInv.length ? _.max(pluckedWarehouseInv) : 0;
                this.model.set('shipMaxAllowed', shipMaxAllowed);
                pluckedWarehouseInv.push(bopisAvailableInventory);
                var maxQuantityAvailable = _.max(pluckedWarehouseInv);
                this.model.set('bopisMaxAllowed', bopisAvailableInventory);
                this.model.set('maxQuantityAvailable',maxQuantityAvailable);

                console.log('bopisMaxAllowed', bopisAvailableInventory);
                console.log('shipMaxAllowed', this.model.get('shipMaxAllowed'));
                console.log('maxQuantityAvailable', maxQuantityAvailable);
            },
            checkQuantity: function (newQuantity, $qField) {
                var maxQuantityAvailable = this.model.get('maxQuantityAvailable');
                if (newQuantity >= maxQuantityAvailable) {
                    $qField.val(maxQuantityAvailable);
                    this.enableDisableButtons(maxQuantityAvailable);
                    this.showMaxQuantityError();
                    this.model.updateQuantity(maxQuantityAvailable);
                } else {
                    $qField.val(newQuantity);
                    this.model.updateQuantity(newQuantity);
                    this.enableDisableButtons(newQuantity);
                    this.model.set('quantityError', false);
                    this.$el.find('#quantityWarning').addClass('hidden');
                    this.$el.find("a[data-mz-value='qty-increment']").removeClass('is-disabled');
                }
            },
            enableDisableButtons: function(newQuantity){
                if (newQuantity <= 1 || isNaN(newQuantity)) {
                    $(".decrement").addClass("is-disabled");
                } else {
                    $(".decrement").removeClass("is-disabled");
                }
            },
            configure: function ($optionEl) {
                var newValue = $optionEl.val(),
                    oldValue,
                    id = $optionEl.data('mz-product-option'),
                    optionEl = $optionEl[0],
                    isPicked = (optionEl.type !== "checkbox" && optionEl.type !== "radio") || optionEl.checked,
                    option = this.model.get('options').findWhere({'attributeFQN': id});
                if (option) {
                    if (option.get('attributeDetail').inputType === "YesNo") {
                        option.set("value", isPicked);
                    } else if (isPicked) {
                        oldValue = option.get('value');
                        if (oldValue !== newValue && !(oldValue === undefined && newValue === '')) {
                            option.set('value', newValue);
                        }
                    }
                }
                // var productDetailView = new ProductDetailView({
                //     el: $('#product-header-detail'),
                //     model: this.model
                // });
                // productDetailView.render();
            },
            addToCart: function (quantity) {
                this.model.addToCart(quantity);
            },
            checkQuantityOnBlur: function () {
                var $qtyField = $('#product-qty'),
                    qtyFieldVal = $qtyField.val();
                if ($qtyField.blur()) {
                    if (qtyFieldVal.trim() !== "" && qtyFieldVal > 0 && !isNaN(qtyFieldVal)) {
                        this.model.updateQuantity(parseInt(qtyFieldVal, 10));
                    } else {
                        this.model.updateQuantity(1);
                    }
                }
            },
            //to be changed when we integrate Ship to store functionality
            initiateAddToCart: function () {
                var self = this;
                this.checkQuantityOnBlur();
                this.fetchCart().then(function (response) {
                    var cartItems = response.data.items,
                        cartItemData = cartItems && cartItems.length ? self.getCartItemData(cartItems) : null,
                        cartItem = cartItemData && cartItemData.cartItem ? cartItemData.cartItem : null,
                        cartPresentQuantity = cartItem ? cartItem.quantity : 0,
                        isQuantityError = self.validateQuantity(cartPresentQuantity),
                        defaultFulfillmentType = cartItemData && cartItemData.isAllCartsItemShip ? "Ship" : self.getDefaultFulfillmentType(),
                        cartPresentFulfillmentType = cartItem ? cartItem.fulfillmentMethod : defaultFulfillmentType,
                        totalQuantity = isQuantityError.maxQuantityAllowed + cartPresentQuantity;
                    checkQuantity = isQuantityError.isQuantityValid;
                    maxQuantityAllowed = isQuantityError.maxQuantityAllowed;
                    if (maxQuantityAllowed <= 0 ) {
                        $('#limited-quantity-container').modal().show();
                    } else {
                        self.model.updateQuantity(maxQuantityAllowed);
                        if(cartPresentFulfillmentType === "Pickup"){
                            self.checkPickupAddToCart(totalQuantity, cartPresentQuantity, cartItem, cartPresentFulfillmentType);
                        } else {
                            self.CheckShipAddToCart(totalQuantity, cartPresentQuantity, cartItem, cartPresentFulfillmentType);
                        }
                    }
                }, function (error) {
                    console.log("Error While fetching cart", error);
                });
            },
            getDefaultFulfillmentType: function(){
                var bopisAvailabelInventory = this.model.get('bopisMaxAllowed'),
                    sthWarehouseInventory = this.model.get('shipWarehouseInventory') ? this.model.get('shipWarehouseInventory') : 0,
                    defaultFulfillmentType = (bopisAvailabelInventory > 0 && sthWarehouseInventory > 0) || bopisAvailabelInventory > 0 ? "Pickup" : "Ship";
                return defaultFulfillmentType;
            },
            checkPickupAddToCart: function (totalQuantity, cartPresentQuantity, cartItemData, cartPresentFulfillmentType) {
                if (this.model.get('isInventoryAvail') && totalQuantity <= this.model.get('bopisMaxAllowed')) {
                    if (cartPresentFulfillmentType === "Ship" || cartItemData) {
                        this.removeProductFromCart(cartItemData, 'Pickup', totalQuantity);
                    } else {
                        this.addToCartForPickup();
                    }
                } else {
                    if(cartItemData){
                        this.removeProductFromCart(cartItemData, 'Ship', totalQuantity);
                    } else {
                        this.addToCart(totalQuantity);
                    }
                }
            },
            CheckShipAddToCart: function(totalQuantity, cartPresentQuantity, cartItemData, cartPresentFulfillmentType){
                var shipWarehouseInventory = this.model.get('shipWarehouseInventory');
                if(shipWarehouseInventory &&  totalQuantity <= shipWarehouseInventory){
                    if(cartPresentFulfillmentType === "Pickup" || cartItemData){
                        this.removeProductFromCart(cartItemData, 'Ship', totalQuantity);
                    } else {
                        this.addToCart();
                    }
                }else{
                    if(cartItemData){
                        this.removeProductFromCart(cartItemData, 'Pickup', totalQuantity);
                    } else {
                        this.addToCartForPickup(totalQuantity);
                    }
                }
            },
            getCartItemData: function (cartItems) {
                var shipItemsCount = 0,
                    productCode = this.model.get('productCode'),
                    cartItem = _.find(cartItems,function(item){
                        if(item.fulfillmentMethod === "Ship")shipItemsCount++;
                        return productCode === item.product.productCode;
                    });
                return {
                    cartItem: cartItem,
                    isAllCartsItemShip: shipItemsCount == cartItems.length ? true : false
                };
            },
            removeProductFromCart: function(cartItem, fulfillmentType, totalQuantity){
                var self = this,
                    cartItemObj = new CartModels.CartItem(cartItem);
                cartItemObj.apiModel.del().then(function() {
                    if (fulfillmentType === "Pickup"){
                        self.addToCartForPickup(totalQuantity);
                    } else {
                        self.addToCart(totalQuantity);
                    }
                });
            },
            fetchCart: function () {
                return api.get('cart', {});
            },
            addToCartForPickup: function (modifiedQuantity) {
                var quantity = modifiedQuantity ? modifiedQuantity : this.model.get("quantity");
                var preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
                this.model.addToCartForPickup(preferredStore.code, preferredStore.name, quantity);
            },
            validateQuantity: function (cartItemQuantity) {
                this.setMaxQuantityAllowed();
                var quantity = this.model.get("quantity"),
                    maxQuantityAvailable = this.model.get('maxQuantityAvailable'),
                    maxAllowedForAddToCart = maxQuantityAvailable - cartItemQuantity,
                    isQuantityValid = true;
                maxQuantityAllowed = quantity;
                if (quantity > maxAllowedForAddToCart) {
                    $('#limited-quantity-container').find('#maximumQuantityCount').text(maxQuantityAvailable > 0 ? maxQuantityAvailable : 0);
                    maxQuantityAllowed = maxAllowedForAddToCart;
                    isQuantityValid = false;
                } else if (this.model.get('quantityError')) {
                    isQuantityValid = true;
                }
                return {
                    isQuantityValid: isQuantityValid,
                    maxQuantityAllowed: maxQuantityAllowed
                };
            },
            setWarehouseInventory: function(fetchedWarehouseCount){
                var warehouseInventoryCount = this.getBufferedInventory(fetchedWarehouseCount);
                this.model.set('warehouseInventoryCount', warehouseInventoryCount);
                this.model.set('isInventoryAvail', warehouseInventoryCount > 0 ? true : false);
                this.model.set('wareHouseInventoryAvail',  warehouseInventoryCount > 0 ? true : false);
                this.model.set("isLimitedQuantity",warehouseInventoryCount > 0 && warehouseInventoryCount <= 3 ? true : false);
                console.log("warehouseInventoryCount", warehouseInventoryCount);
            },
            getBufferedInventory: function(fetchedWarehouseCount){
                var isWarehouseBufferEnabled = Hypr.getThemeSetting('enableWarehouseBuffer'),
                    warehouseBufferValue = Hypr.getThemeSetting('warehouseBufferValue'),
                    bufferedWarehouseInventory = 0,
                    warehouseCount = fetchedWarehouseCount && fetchedWarehouseCount > 0 ? fetchedWarehouseCount : 0;
                warehouseInventoryCount = warehouseCount;
                if(isWarehouseBufferEnabled && warehouseBufferValue > 0 && warehouseCount > 1){
                    bufferedWarehouseInventory = Math.round((warehouseCount/100)*warehouseBufferValue);
                    bufferedWarehouseInventory = bufferedWarehouseInventory < 0.5 ? 1 : bufferedWarehouseInventory;
                    warehouseInventoryCount = warehouseCount - bufferedWarehouseInventory;
                }
                return warehouseInventoryCount;
            },
            showMaxQuantityError: function () {
                this.$el.find('#quantityWarning').removeClass('hidden');
                this.model.set('quantityError', true);
                this.$el.find("a[data-mz-value='qty-increment']").addClass('is-disabled');
            },
            addToWishlist: function () {
                var me = this;
                if (user.isAnonymous) {
                    var locale = require.mozuData('apicontext').headers['x-vol-locale'];
                    var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
                    locale = locale.split('-')[0];
                    var currentLocale = '';
                    if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite) {
                        currentLocale = locale === 'fr' ? '/fr' : '/en';
                    }
                    window.location.href = currentLocale + "/user/login?productCode=" + me.model.get('productCode');
                } else {
                    this.model.addToWishlist();
                }

            },
            checkLocalStores: function (e) {
                var me = this;
                e.preventDefault();
                this.model.whenReady(function () {
                    var $localStoresForm = $(e.currentTarget).parents('[data-mz-localstoresform]'),
                        $input = $localStoresForm.find('[data-mz-localstoresform-input]');
                    if ($input.length > 0) {
                        $input.val(JSON.stringify(me.model.toJSON()));
                        $localStoresForm[0].submit();
                    }
                });
            },
            changePreferredStore: function (e) {
                e.preventDefault();
                var locale = require.mozuData('apicontext').headers['x-vol-locale'];
                var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
                locale = locale.split('-')[0];
                var currentLocale = '';
                if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite) {
                    currentLocale = locale === 'fr' ? '/fr' : '/en';
                }
                if ($('.changePreferredStore').hasClass('fullAssortmentSelectStoreLink')) {
                    window.location.href = currentLocale + "/store-locator?returnUrl=" + window.location.pathname + "&filter=storeType ne hh";
                } else {
                    window.location.href = currentLocale + "/store-locator?returnUrl=" + window.location.pathname;
                }
            },
            setAeroplanMiles: function (aeroplanMiles) {
                var me = this;
                var documentListForAeroplanPromo = Hypr.getThemeSetting('documentListForAeroplanPromo');
                api.get('documentView', {listName: documentListForAeroplanPromo}).then(function (response) {
                    if (response.data.items.length > 0) {
                        var aeroplanMilesFactor = response.data.items[0].properties.markupFactor;
                        me.model.set('aeroplanMiles', aeroplanMilesFactor);
                        var promoCode = response.data.items[0].properties.promoCode;
                        me.model.set('aeroplanPromoCode', promoCode);
                        var startDate = new Date(response.data.items[0].activeDateRange.startDate);
                        var endDate = new Date(response.data.items[0].activeDateRange.endDate);
                        me.model.set('aeroplanPromoDate', true);
                        me.model.set('aeroplanStartDate', startDate.getDate());
                        me.model.set('aeroplanStartMonth', startDate.getMonth() + 1);
                        me.model.set('aeroplanStartYear', startDate.getFullYear());
                        me.model.set('aeroplanEndDate', endDate.getDate());
                        me.model.set('aeroplanEndMonth', endDate.getMonth() + 1);
                        me.model.set('aeroplanEndYear', endDate.getFullYear());
                        me.render();
                    } else {
                        me.model.set('aeroplanMiles', aeroplanMiles);
                        me.render();
                    }
                });
            },
            initialize: function () {
                // handle preset selects, etc
                var me = this;
                var locale = require.mozuData('apicontext').headers['x-vol-locale'];
                var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
                /**
                 * Below code is for Google Analytics Sending Compare CategoryId in stock status
                 * Ticket number - IWM-1192
                 * @Date - 09/02/2021
                 * */
                var breadcrumbs = $(".mz-breadcrumb-link:not('.is-first')");
                if(null !== Hypr.getThemeSetting("compareCategoryIds") || "" !== Hypr.getThemeSetting("compareCategoryIds")){
                    var categoryIdArray = Hypr.getThemeSetting("compareCategoryIds").split(",").map(Number);
                    _.forEach(breadcrumbs, function(category,index){
                        var categoryId = $(category).attr("data-originalId");
                        categoryIdArray.find(function(currentValue,index,array){
                            if(currentValue == categoryId) {
                                isCompareCategoryIdAvailable = true;
                                productMappedToCompareCategoryIdSet.push(currentValue);
                            }
                        });
                    });
                }

            locale = locale.split('-')[0];
            me.model.set("currentLocale", locale);
            me.model.set("currentSite", currentSite);
            if (usePDPArcFunction) {
                console.log("********usePDPArcFunction***********" + usePDPArcFunction);
                var pdpArcStr = me.model.get('mfgPartNumber');
                if (pdpArcStr) {
                    var pdpArcData = JSON.parse(pdpArcStr);
                    me.model.set('aeroplanMiles', pdpArcData.aeroplanMiles);
                    me.model.set('aeroplanPromoCode', pdpArcData.aeroplanPromoCode);
                    me.model.set('aeroplanPromoDate', pdpArcData.aeroplanPromoDate);
                    me.model.set('aeroplanStartDate', pdpArcData.aeroplanStartDate);
                    me.model.set('aeroplanStartMonth', pdpArcData.aeroplanStartMonth);
                    me.model.set('aeroplanStartYear', pdpArcData.aeroplanStartYear);
                    me.model.set('aeroplanEndDate', pdpArcData.aeroplanEndDate);
                    me.model.set('aeroplanEndMonth', pdpArcData.aeroplanEndMonth);
                    me.model.set('aeroplanEndYear', pdpArcData.aeroplanEndYear);
                    me.model.set('ehfValue', pdpArcData.ehfValue);
                    me.model.set('stockAvailable', pdpArcData.stockAvailable);
                    me.model.set('isInventoryAvail', pdpArcData.isInventoryAvail);
                    me.model.set('isLimitedQuantity', pdpArcData.isLimitedQuantity);
                    me.model.set('wareHouseInventoryAvail', pdpArcData.isInventoryAvail);
                    me.model.set('isItemRestricted', pdpArcData.isItemRestricted);
                    me.setWarehouseInventory(pdpArcData.stockAvailable);
                    if (pdpArcData.isWebsiteInd) {
                        me.model.set('isWebsiteInd', pdpArcData.isWebsiteInd);
                    }
                    console.log("********pdpArcData***********" + pdpArcData);
                }
            }
            this.$('[data-mz-product-option]').each(function (index) {
                if (index === 0) {
                    var $this = $(this), isChecked, wasChecked;
                    if ($this.val()) {
                        switch ($this.attr('type')) {
                            case "checkbox":
                            case "radio":
                                isChecked = $this.prop('checked');
                                wasChecked = !!$this.attr('checked');
                                if ((isChecked && !wasChecked) || (wasChecked && !isChecked)) {
                                    me.configure($this);
                                }
                                break;
                            default:
                                me.configure($this);
                        }
                    }
                }
            });

            /* Hiding More/Less Toggle button based on container height*/
            if ($(".collapse-section > .collapse-content").height() < 100) {
                $(".toggle-container").hide();
            } else {
                $(".toggle-container").show();
            }
            var $products = [];
            var currentStoreForFullAssortment;
            $products = $('[data-mz-supplier-direct-items-related-products]');
            if ($.cookie('preferredStore')) {
                window.setPreferredStoreFromCookie();
                me.model.set({"preferredStore": $.parseJSON(localStorage.getItem('preferredStore'))});
                currentStoreForFullAssortment = JSON.parse(localStorage.getItem('preferredStore'));
                if (currentStoreForFullAssortment) {
                    var storeTypeObj = _.find(currentStoreForFullAssortment.attributes, function (attribute) {
                        return attribute.fullyQualifiedName === 'tenant~storetype' || attribute.fullyQualifiedName === 'tenant~store-type';
                    });
                    if (storeTypeObj) {
                        var storeType = storeTypeObj.values[0];
                        _.each($products, function (element) {
                            if ('Home Hardware' === storeType) {
                                $(element).text(Hypr.getLabel('onlyAvailableAtBuildingCenter')).removeClass('hidden');
                            } else {
                                $(element).text(Hypr.getLabel('contactStoreForPriceAndAvailability')).removeClass('hidden');
                            }
                        });
                    }
                }
            } else {
                if ($products) {
                    _.each($products, function (element) {
                        $(element).text(Hypr.getLabel('onlyAvailableAtBuildingCenter')).removeClass('hidden');
                    });
                }
            }
            var aeroplanMiles;
            me.model.on("productConfigured", function (response) {
                if (response.get('price.salePrice')) {
                    aeroplanMiles = Math.floor(response.get('price.salePrice') / 2);
                } else {
                    aeroplanMiles = Math.floor(response.get('price.price') / 2);
                }
                me.setAeroplanMiles(aeroplanMiles);
            });

            if (!me.model.get('hasPriceRange')) {
                if (me.model.get('price.salePrice')) {
                    aeroplanMiles = Math.floor(me.model.get('price.salePrice') / 2);
                } else {
                    aeroplanMiles = Math.floor(me.model.get('price.price') / 2);
                }
                me.setAeroplanMiles(aeroplanMiles);
            }

            var recentlyViewedItems = $.cookie('recentlyViewd');
            if (recentlyViewedItems) {
                if (me.model.get('productType') != 'Articles') {
                    recentlyViewedItems = recentlyViewedItems + ',' + me.model.get('productCode');
                }
                recentlyViewedItems = recentlyViewedItems.split(',');
                if (recentlyViewedItems.length > 6) {
                    recentlyViewedItems = recentlyViewedItems.slice(Math.max(recentlyViewedItems.length - 6, 1));
                }
                recentlyViewedItems = _.unique(recentlyViewedItems);
                recentlyViewedItems = recentlyViewedItems.join(',');
            } else {
                if (me.model.get('productType') != 'Articles') {
                    recentlyViewedItems = me.model.get('productCode');
                }
            }
            if (ua.indexOf('chrome') > -1) {
                if (/edg|edge/i.test(ua)) {
                    $.cookie('recentlyViewd', recentlyViewedItems, {path: '/'});
                } else {
                    document.cookie = "recentlyViewd=" + recentlyViewedItems + ";path=/;Secure;SameSite=None";
                }
            } else {
                $.cookie('recentlyViewd', recentlyViewedItems, {path: '/'});
            }

            var frSiteId = Hypr.getThemeSetting("frSiteId");
            if (homeFurnitureSiteId === contextSiteId) {
                me.model.set("isHomeFurnitureSite", true);
            }
            if (frSiteId === contextSiteId) {
                me.model.set("isFrenchSite", true);
            }

            if (localStorage.getItem('preferredStore')) {
                var currentStore = $.parseJSON(localStorage.getItem('preferredStore'));
                var remoteStoreattribute = _.findWhere(currentStore.attributes, {"fullyQualifiedName": Hypr.getThemeSetting("remoteStoreAttr")});
               // console.log(remoteStoreattribute);
                if (remoteStoreattribute) {
                    var isRemoteStore = remoteStoreattribute.values[0];
                    //console.log(isRemoteStore);
                    if(isRemoteStore===true)
                    {
                      $('.cart-icon').addClass('hidden');
                    }
                    else
                    {
                       $('.cart-icon').removeClass('hidden');
                    }
                    me.model.set("isRemoteStore", isRemoteStore);
                }
                var attribute = _.findWhere(currentStore.attributes, {"fullyQualifiedName": Hypr.getThemeSetting("isEcomStore")});

                if (attribute) {
                    var isEcomStore = attribute.values[0] === "Y" ? true : false;
                    me.model.set("isEcomStore", isEcomStore);
                }
                if (currentStore) {
                    _.each(currentStore.attributes, function (obj) {
                        if (obj.fullyQualifiedName === 'tenant~storetype') {
                            var storeType = obj.values[0];
                            me.model.set('storeType', storeType);
                        }
                    });
                }

                //check EHF for current product
                var entityListForEHF = Hypr.getThemeSetting('entityListForEHF');
                var productCode;
                if (me.model.get('variations') && me.model.get('variations').length > 0) {
                    //from current variation find the variationProductCode
                    var productOptionValue = $('.mz-productoptions-option').val();
                    var variation = _.find(me.model.get('variations'), function (variation) {
                        return _.findWhere(variation.options, {'value': productOptionValue});
                    });
                    productCode = variation.productCode;
                } else {
                    productCode = me.model.get('productCode');
                }
                var filterQuery = 'productCode eq ' + productCode + ' and province eq ' + currentStore.address.stateOrProvince;
                if (!usePDPArcFunction) {
                    var ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
                    api.get('entityList', {
                        listName: entityListForEHF,
                        filter: filterQuery
                    }).then(function (response) {
                        if (response.data.items.length > 0) { //if EHF is applied for current product
                            me.model.set('ehfValue', response.data.items[0].feeAmt);
                            me.render();
                        }
                    });
                }

                //entityListForItemRestricted
                filterQuery = 'productCode eq ' + productCode + ' and (province eq ' + currentStore.address.stateOrProvince + ' or store eq ' + currentStore.code + ')';
                if (!usePDPArcFunction) {
                    var entityListForItemRestricted = Hypr.getThemeSetting('entityListForItemRestricted');
                    api.get('entityList', {
                        listName: entityListForItemRestricted,
                        filter: filterQuery
                    }).then(function (response) {
                        if (response.data.items.length > 0) {
                            if (response.data.items[0].ecommerce_ind === "Y") {
                                me.model.set('isItemRestricted', true);
                            } else {
                                me.model.set('isItemRestricted', false);
                            }
                            var websiteInd = response.data.items[0].website_ind;
                            if (websiteInd === 'N') {
                                me.model.set('isWebsiteInd', true);
                            }
                            me.render();
                        } else {
                            me.model.set('isItemRestricted', true);
                            me.render();
                        }
                    });
                }

                var isBOHStore = _.find(currentStore.attributes, function (attribute) {
                    return attribute.fullyQualifiedName === Hypr.getThemeSetting('hhBOHStoreAttr');
                });
                if (!usePDPArcFunction) {
                    var currentStoresWarehouse = _.find(currentStore.attributes, function (attribute) {
                        return attribute.fullyQualifiedName === Hypr.getThemeSetting('warehouseAttr');
                    });
                    if (currentStoresWarehouse) {
                        var inventoryApi = '/api/commerce/catalog/storefront/products/' + productCode + '/locationinventory?locationCodes=' + currentStoresWarehouse.values[0] + '&time=' + Date.now();
                        api.request("GET", inventoryApi).then(function (response) {
                            if (response && response.items.length > 0) {
                                var stockAvailable = response.items[0].stockAvailable;
                                me.setWarehouseInventory(stockAvailable);
                                   stockAvailable = me.model.get('warehouseInventoryCount');
                                if (stockAvailable > 0) {
                                    wareHouseInventory = stockAvailable;
                                    if (isBOHStore && !isBOHStore.values[0] || isBOHStore === undefined) {
                                        dataLayer.push({
                                            'event': 'stockStatus',
                                            'stock': {
                                                'warehouse': wareHouseInventory, //display warehouse inventory for selected store
                                                'store': 0 //display store inventory.  If non-NRTI store display 0
                                            }
                                        });
                                        if(isCompareCategoryIdAvailable){
                                            dataLayer.push({
                                                'event': 'certonaContainers',
                                                'compareProduct':{
                                                    'status': isCompareCategoryIdAvailable, // If product is in a category that is configured in admin for compare then return true, otherwise return false
                                                    'category': productMappedToCompareCategoryIdSet.toString() // return the categoryId of the compare product category
                                                  },
                                                'stock': {
                                                    'warehouse':wareHouseInventory, //display warehouse inventory for selected store
                                                    'store': 0 //display store inventory.  If non-NRTI store display 0
                                                }
                                            });
                                        } else {
                                            dataLayer.push({
                                                'event': 'certonaContainers',
                                                'compareProduct':{
                                                    'status': isCompareCategoryIdAvailable, // If product is in a category that is configured in admin for compare then return true, otherwise return false
                                                    'category': '' // return the categoryId of the compare product category
                                                  },
                                                'stock': {
                                                    'warehouse':wareHouseInventory, //display warehouse inventory for selected store
                                                    'store': 0 //display store inventory.  If non-NRTI store display 0
                                                }
                                            });
                                        }
                                    }
                                    me.render();
                                }
                            } else {
                                wareHouseInventory = 0;
                                if (isBOHStore && !isBOHStore.values[0] || isBOHStore === undefined) {
                                    dataLayer.push({
                                        'event': 'stockStatus',
                                        'stock': {
                                            'warehouse': wareHouseInventory, //display warehouse inventory for selected store
                                            'store': 0 //display store inventory.  If non-NRTI store display 0
                                        }
                                    });
                                    if(isCompareCategoryIdAvailable){
                                        dataLayer.push({
                                            'event': 'certonaContainers',
                                            'compareProduct':{
                                                'status': isCompareCategoryIdAvailable, // If product is in a category that is configured in admin for compare then return true, otherwise return false
                                                'category': productMappedToCompareCategoryIdSet.toString() // return the categoryId of the compare product category
                                              },
                                            'stock': {
                                                'warehouse':wareHouseInventory, //display warehouse inventory for selected store
                                                'store': 0 //display store inventory.  If non-NRTI store display 0
                                            }
                                        });
                                    } else {
                                        dataLayer.push({
                                            'event': 'certonaContainers',
                                            'compareProduct':{
                                                'status': isCompareCategoryIdAvailable, // If product is in a category that is configured in admin for compare then return true, otherwise return false
                                                'category': '' // return the categoryId of the compare product category
                                              },
                                            'stock': {
                                                'warehouse':wareHouseInventory, //display warehouse inventory for selected store
                                                'store': 0 //display store inventory.  If non-NRTI store display 0
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                } else if (isBOHStore && !isBOHStore.values[0] || isBOHStore === undefined) {
                    dataLayer.push({
                        'event': 'stockStatus',
                        'stock': {
                            'warehouse': me.model.get('warehouseInventoryCount') ? me.model.get('warehouseInventoryCount') : 0, //display warehouse inventory for selected store
                            'store': 0 //display store inventory.  If non-NRTI store display 0
                        }
                    });
                    if(isCompareCategoryIdAvailable){
                        dataLayer.push({
                            'event': 'certonaContainers',
                            'compareProduct':{
                                'status': isCompareCategoryIdAvailable, // If product is in a category that is configured in admin for compare then return true, otherwise return false
                                'category': productMappedToCompareCategoryIdSet.toString() // return the categoryId of the compare product category
                              },
                            'stock': {
                                'warehouse':me.model.get('stockAvailable') ? me.model.get('stockAvailable') : 0, //display warehouse inventory for selected store
                                'store': 0 //display store inventory.  If non-NRTI store display 0
                            }
                        });
                    } else {
                        dataLayer.push({
                            'event': 'certonaContainers',
                            'compareProduct':{
                                'status': isCompareCategoryIdAvailable, // If product is in a category that is configured in admin for compare then return true, otherwise return false
                                'category': '' // return the categoryId of the compare product category
                              },
                            'stock': {
                                'warehouse':me.model.get('stockAvailable') ? me.model.get('stockAvailable') : 0, //display warehouse inventory for selected store
                                'store': 0 //display store inventory.  If non-NRTI store display 0
                            }
                        });
                    }
                }

                //store level inventory fetch
                if (isBOHStore && isBOHStore.values[0]) {
                    api.request("GET", '/api/commerce/catalog/storefront/products/' + productCode + '/locationinventory?locationCodes=' + currentStore.code + '&time=' + Date.now()).then(function (response) {
                        wareHouseInventory = me.model.get('warehouseInventoryCount') ? me.model.get('warehouseInventoryCount') : 0;
                        if (response && response.items.length > 0) {
                            me.model.set("bohStore", true);
                            locationInventoryCount = response.items[0].stockAvailable;
                            me.model.set("storeInventoryData", response.items[0]);
                            me.model.set("showBOH", true);
                            me.model.set('locationInventoryCount', locationInventoryCount);
                            me.render();
                            _.defer(function () {
                                dataLayer.push({
                                    'event': 'stockStatus',
                                    'stock': {
                                        'warehouse': wareHouseInventory, //display warehouse inventory for selected store
                                        'store': response.items[0].stockAvailable //display store inventory.  If non-NRTI store display 0
                                    }
                                });
                                if(isCompareCategoryIdAvailable){
                                    dataLayer.push({
                                        'event': 'certonaContainers',
                                        'compareProduct':{
                                            'status': isCompareCategoryIdAvailable, // If product is in a category that is configured in admin for compare then return true, otherwise return false
                                            'category': productMappedToCompareCategoryIdSet.toString() // return the categoryId of the compare product category
                                          },
                                        'stock': {
                                            'warehouse':wareHouseInventory, //display warehouse inventory for selected store
                                            'store': response.items[0].stockAvailable //display store inventory.  If non-NRTI store display 0
                                        }
                                    });
                                } else {
                                    dataLayer.push({
                                        'event': 'certonaContainers',
                                        'compareProduct':{
                                            'status': isCompareCategoryIdAvailable, // If product is in a category that is configured in admin for compare then return true, otherwise return false
                                            'category': '' // return the categoryId of the compare product category
                                          },
                                        'stock': {
                                            'warehouse':wareHouseInventory, //display warehouse inventory for selected store
                                            'store': response.items[0].stockAvailable //display store inventory.  If non-NRTI store display 0
                                        }
                                    });
                                }
                                me.getTotalInventory(locationInventoryCount);
                            }, 100);
                        } else {
                            me.model.set("bohStore", true);
                            me.model.set("showBOH", true);
                            if (response && response.items.length <= 0) {
                                _.defer(function () {
                                    dataLayer.push({
                                        'event': 'stockStatus',
                                        'stock': {
                                            'warehouse': wareHouseInventory,
                                            'store': 0
                                        }
                                    });
                                    if(isCompareCategoryIdAvailable){
                                        dataLayer.push({
                                            'event': 'certonaContainers',
                                            'compareProduct':{
                                                'status': isCompareCategoryIdAvailable, // If product is in a category that is configured in admin for compare then return true, otherwise return false
                                                'category': productMappedToCompareCategoryIdSet.toString() // return the categoryId of the compare product category
                                              },
                                            'stock': {
                                                'warehouse':wareHouseInventory, //display warehouse inventory for selected store
                                                'store': 0 //display store inventory.  If non-NRTI store display 0
                                            }
                                        });
                                    } else {
                                        dataLayer.push({
                                            'event': 'certonaContainers',
                                            'compareProduct':{
                                                'status': isCompareCategoryIdAvailable, // If product is in a category that is configured in admin for compare then return true, otherwise return false
                                                'category': '' // return the categoryId of the compare product category
                                              },
                                            'stock': {
                                                'warehouse':wareHouseInventory, //display warehouse inventory for selected store
                                                'store': 0 //display store inventory.  If non-NRTI store display 0
                                            }
                                        });
                                    }
                                });
                            }
                            me.render();
                        }
                    });
                } else {
                    api.get("location", {code: currentStore.code}).then(function (response) {
                        if (response) {
                            isBOHStore = _.find(response.data.attributes, function (attribute) {
                                return attribute.fullyQualifiedName === Hypr.getThemeSetting('hhBOHStoreAttr');
                            });
                            if (isBOHStore && isBOHStore.values[0]) {
                                var selectedStore = response.data;
                                CookieUtils.setPreferredStore(selectedStore);
                                api.request("GET", '/api/commerce/catalog/storefront/products/' + productCode + '/locationinventory?locationCodes=' + currentStore.code).then(function (response) {
                                    if (response && response.items.length > 0) {
                                        me.model.set("bohStore", true);
                                        me.model.set("storeInventoryData", response.items[0]);
                                        me.model.set("showBOH", true);
                                        me.render();
                                    } else {
                                        me.model.set("bohStore", true);
                                        me.model.set("showBOH", true);
                                        me.render();
                                    }
                                });
                            } else {
                                me.model.set("bohStore", false);
                                me.model.set("showBOH", true);
                                me.render();
                            }
                        }
                    });
                }

                //Set date for product availability (as of) and pickup date at store (current date + 10)
                me.model.set("stockAvailableAsOF", me.formatDate(today));
                var todayDate = new Date();
                //me.model.set("pickUpAtStoreBy",me.formatDate(todayDate.setDate(todayDate.getDate() + 15)));
                //IWM-222 Remove get it by messaging on PDP & NRTI Check Nearby Store Locator
                var torontoTimeZone = timeZoneSupport.findTimeZone('America/Toronto');
                var promoItemZonedTime = timeZoneSupport.getZonedTime(new Date(me.model.get("updateDate")), torontoTimeZone);
                var promoItemDate = new Date(promoItemZonedTime.year, promoItemZonedTime.month - 1, promoItemZonedTime.day, promoItemZonedTime.hours, promoItemZonedTime.minutes, promoItemZonedTime.seconds, promoItemZonedTime.milliseconds);
                if (today <= promoItemDate) {
                    me.model.set("isPromoEnabled", true);
                    me.model.set("promoTillDate", me.formatDate(promoItemDate));
                }

                var messageConfig = $(".mz-configuarble-message-container").attr('data-mz-message-config');
                var parsedMessageRes = messageConfig ? JSON.parse(messageConfig) : '';
                me.checkProvinceMatch(parsedMessageRes,currentStore);

            } else {
                me.model.set("showBOH", true);
            }
            var screenWidth = window.matchMedia("(max-width: 767px)");
            var createDate = me.model.get("createDate");
            var newTagDate = moment(createDate).add(30, 'd');
            if (moment(createDate) <= moment(today) && moment(today) <= moment(newTagDate)) {
                me.model.set("newTagPromotion", true);
            }
            var onlineOnlyProducts = Hypr.getThemeSetting('productCodes');
            var onlineOnlyProductsList = onlineOnlyProducts.split(",");
            var onlineOnly = _.find(onlineOnlyProductsList, function (product) {
                return product === me.model.get("productCode");
            });
            var promoStartsAt = new Date(Hypr.getThemeSetting("onlineOnlyMessageStartDate"));
            var promoEndsAt = new Date(Hypr.getThemeSetting("onlineOnlyMessageEndDate"));

            if ((onlineOnly) && (promoStartsAt <= today && today <= promoEndsAt)) {
                me.model.set("isOnlineOnlyProduct", true);
            }
            if (screenWidth.matches) {
                me.model.set("isMobileView", true);
            }
            api.get('cart', {}).then(function (response) {
                var cartItems = response.data.items,
                preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
                for (var i = 0; i < cartItems.length; i++) {
                        if(cartItems[i].product.productCode === me.model.get('productCode')) me.model.set('cartFulfillmentMethod',cartItems[i].fulfillmentMethod);
                        if ((cartItems[i].fulfillmentMethod === 'Pickup' && cartItems[i].fulfillmentLocationCode !== preferredStore.code) || (cartItems[i].fulfillmentMethod === 'Ship' && cartItems[i].purchaseLocation !== preferredStore.code)) {
                            var cartItemData = cartItems && cartItems.length ? me.getCartItemData(cartItems) : 0,
                                cartPresentQuantity = cartItemData && cartItemData.cartItem ? cartItemData.cartItem.cartItemQuantity : 0,
                            isQuantityError = me.validateQuantity(cartPresentQuantity),
                            finalMaxAllowedQuantity = isQuantityError.maxQuantityAllowed + cartPresentQuantity;
                        if (cartPresentQuantity !== 0 && cartPresentQuantity > finalMaxAllowedQuantity) {
                            maxQuantityAllowed = finalMaxAllowedQuantity > cartPresentQuantity ? cartPresentQuantity - finalMaxAllowedQuantity : finalMaxAllowedQuantity - cartPresentQuantity;
                            CartMonitor.addToCount(maxQuantityAllowed);
                        }
                        break;
                    }
                }
            });

            if (localStorage.getItem('preferredStore')) {
                //Set store level ship to home flag
                var preferredStoreDetails = $.parseJSON(localStorage.getItem('preferredStore'));
                this.model.set('storeAllowsShipToHome', !!_.find(preferredStoreDetails.attributes,
                    function predicate(attribute) {
                        return (attribute.fullyQualifiedName === Hypr.getThemeSetting('shipToHomeStoreFlagAttrFQN') &&
                            attribute.values[0]);
                    }));
            }

                this.model.set('isEhfValueSet', false);
                //STHF-99 set cookie postal code
                if(localStorage.getItem('preferredStore') && this.model.get('properties').length)this.setStoreDetailsFromDocumentList();
             var productProperties = this.model.get('properties') && this.model.get('properties').length ? this.model.get('properties') : [],
                 isSThAttrPresent =  productProperties.length ? _.findWhere(productProperties, {'attributeFQN': Hypr.getThemeSetting('shipToHomeEnableAttrName')}) : false,
                 isProductEnabledForSTH = isSThAttrPresent ? isSThAttrPresent.values[0].value : false;
                 if(localStorage.getItem('preferredStore') && this.model.get('storeAllowsShipToHome') && isProductEnabledForSTH){
                this.getOverSizeConfiguration();
                this.setShippingPostalCode();
                this.refreshPage();
            }

            },
            getOverSizeConfiguration: function () {
                var self = this,
                oversizeChargeCalculatorModel = new OversizeChargeCalculatorModel();
                oversizeChargeCalculatorModel.getOversizeConfigurations().then(function (res) {
                    if(res.shippingThreshold === true){
                        $('.header-notifications').find('.pdp_shipping_text').removeClass('hidden');
                        $('.shippingThreshold').text(res.shippingThresholdAmount);
                    }
                }, function (err) {
                    console.log('Error while getting oversize config.', err);
                });
            },
            getWarehouseQuantity: function(postalCodeData){
                var self = this,
                    locationCodes = activeWarehousesForSTH.join(','),
                    inventoryApi = '/api/commerce/catalog/storefront/products/' + this.model.get('productCode') + '/locationinventory?locationCodes=' + locationCodes + '&time=' + Date.now();
                api.request("GET", inventoryApi).then(function (response) {
                    self.addBufferToWarehouseInventory(response.items);
                    if(postalCodeData.status === "Valid") self.getShippingTimeline(postalCodeData.postalCode, response.items);
                    self.model.set('wareHouseInventory', response.items);
                    self.setSThWarehouseInventory(response.items);
                    if(postalCodeData.status !== "Valid")Backbone.MozuView.prototype.render.apply(self, arguments);
                }, function(error){
                    Backbone.MozuView.prototype.render.apply(self, arguments);
                    console.log('error while fetching warehouse inventories',error);
                });
            },
            setSThWarehouseInventory: function(wareHouseInventory){
                var pluckedWarehouseInv =  wareHouseInventory.length ? _.pluck(wareHouseInventory, 'stockAvailable') : 0,
                    maxAvailable = _.max(pluckedWarehouseInv);
                this.model.set('shipWarehouseInventory', maxAvailable > 0 ? maxAvailable : 0);
                console.log('shipWarehouseInventory', maxAvailable);
            },
            addBufferToWarehouseInventory: function(warehouseData){
                var self = this;
                _.each(warehouseData, function(warehouse){
                    var stockAvailable = warehouse.stockAvailable > 0 ? self.getBufferedInventory(warehouse.stockAvailable) : 0;
                    warehouse.stockAvailable = stockAvailable;
                });
                console.log('warehouseData', warehouseData);
        },
        checkProvinceMatch: function(configurableMessageResponse,currentStore){
            var me = this;
            if(currentStore && currentStore.address.stateOrProvince){
                var isProvinceMatched = _.findWhere(configurableMessageResponse.msgContent, {province: currentStore.address.stateOrProvince});
                if(isProvinceMatched){
                    me.model.set("provinceMatched",true);
                    me.model.set("minimumOrderMsgPdp",isProvinceMatched.productPageMessage);
                    me.model.set("pdpOrderVolumeMessage",isProvinceMatched.pdpOrderVolumeMsg);
                    me.model.set("minOrderAmount",isProvinceMatched.minimumOrderAmount);
                }else{
                    me.model.set("provinceMatched",false);
                }
            }
        },
        setShippingPostalCode: function(){
            var self = this,
                shippingPostalCodeModel = new ShippingPostalCodeModel();
                shippingPostalCodeModel.getPostalCode().then(function(postalCodeData){
                    self.model.set('shippingPostalCodeData', postalCodeData);
                    console.log(postalCodeData);
                    self.getWarehouseQuantity(postalCodeData);
                Backbone.MozuView.prototype.render.apply(self, arguments);
            }, function(err){
                console.log('Error While fetching postal code', err);
            });
            },
            getShippingTimeline: function(postalCode, warehouseInventory){
                var self = this,
                    shippingModel = new ShippingTimelineModel();
                shippingModel.getShippingTimeline(postalCode, warehouseInventory, 1).then(function(timelineData){
                    console.log('getShippingTimeline',timelineData);
                    self.model.set('sthShippingTimeline', timelineData.shippingTimeline);
                    Backbone.MozuView.prototype.render.apply(self, arguments);
                }, function(error){
                    Backbone.MozuView.prototype.render.apply(self, arguments);
                    console.log('Error while getting shipping Timelines', error);
                });
        },
        setStoreDetailsFromDocumentList: function(){
            var ecomAttr = _.findWhere(this.model.attributes.properties, {'attributeFQN': Hypr.getThemeSetting('soldItemAttr')}),
                ecomAttrVal = ecomAttr ? ecomAttr.values[0].value : undefined,
                supplierDirectAttr = _.findWhere(this.model.attributes.properties, {'attributeFQN': Hypr.getThemeSetting('supplierDirectItem')}),
                supplierDirectAttrVal = supplierDirectAttr ? supplierDirectAttr.values[0].value : undefined,
                productPrice = this.model.get('price').get('price'),
                isStoreDetailsRequired = ecomAttrVal !== Hypr.getThemeSetting('ecommItemValue') || supplierDirectAttrVal === Hypr.getThemeSetting('supplierDirectItemValue') || productPrice == "999999.00";
            if(isStoreDetailsRequired) this.fetchStoreDetails();
        },
        fetchStoreDetails: function(){
            var self = this,
                preferredStore = $.parseJSON(localStorage.getItem('preferredStore')),
                queryString = "properties.documentKey eq " + preferredStore.code,
                locationList = Hypr.getThemeSetting("storeDocumentListFQN");
            api.get('documentView', {
                listName: locationList,
                filter: queryString
            }).then(function (response) {
                if (response && response.data.items.length > 0) {
                    var storeContactInfo = response.data.items[0];
                    var contactInfo = {
                        "phone": storeContactInfo.properties.phone.trim(),
                        "fax": storeContactInfo.properties.fax.trim(),
                        "email": storeContactInfo.properties.email.replace(/\s/g, '')
                    };
                    self.model.set("storeContactInfo",contactInfo);
                    Backbone.MozuView.prototype.render.apply(self, arguments);
                }
            });
        },
        callStore: function (e) {
            var screenWidth = window.matchMedia("(max-width: 767px)");
            if (!screenWidth.matches) {
                e.preventDefault();
            }
        },
        checkNearbyStores: function (e) {
            e.preventDefault();
            $('#check-nearby-store-modal').modal().show();
            $("#check-nearby-loading").show();
            var locationList = Hypr.getThemeSetting("storeDocumentListFQN");
            var self = this,
                locationInventory = null;
            var productCode = self.model.get('productCode');
            var isEcomEnabled = _.findWhere(self.model.attributes.properties, {'attributeFQN': Hypr.getThemeSetting('soldItemAttr')});
            if (!isMapboxEnabled) {
                var mapboxGlCSS = document.createElement('link');
                mapboxGlCSS.setAttribute('href', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.44.2/mapbox-gl.css');
                mapboxGlCSS.setAttribute('rel', 'stylesheet');
                mapboxGlCSS.setAttribute('type', 'text/css');
                document.head.appendChild(mapboxGlCSS);

                var unwiredGlCSS = document.createElement('link');
                unwiredGlCSS.setAttribute('href', 'https://tiles.unwiredmaps.com/v2/js/unwired-gl.css?v=0.1.6');
                unwiredGlCSS.setAttribute('rel', 'stylesheet');
                unwiredGlCSS.setAttribute('type', 'text/css');
                document.head.appendChild(unwiredGlCSS);
                isMapboxEnabled = true;
            }
            if ($(e.currentTarget).hasClass('check-nearby-fullAssortment')) {
                filtersString = "properties.storeType in['Home Building Centre', 'Home Hardware Building Centre'] and properties.displayOnline eq true";
                document.cookie = "fullAssortmentpdp=" + true + ";Secure;SameSite=None";
                if ($.cookie('fullAssortmentProduct')) {
                    document.cookie = "fullAssortmentProduct=" + true + ";Secure;SameSite=None";
                } else {
                    document.cookie = "fullAssortmentProduct=" + true + ";Secure;SameSite=None";
                }
            } else {
                filtersString = "properties.storeType in['Home Hardware','Home Building Centre', 'Home Hardware Building Centre'] and properties.displayOnline eq true";
                document.cookie = "fullAssortmentProduct=" + false + ";Secure;SameSite=None";
            }
            var curStore;
            if (localStorage.getItem('preferredStore')) {
                curStore = $.parseJSON(localStorage.getItem('preferredStore'));
            }
            if (curStore) {
                api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + filtersString + '&pageSize=1100', {
                    cache: false
                }).then(function (response) {
                    var range = Hypr.getThemeSetting("storeDistanceInkm");
                    var nearStoresList = [];
                    if (response && response.items.length > 0) {
                        nearStoresList = findNearStores(response.items, curStore.geo, range);
                        var prefStore = _.findWhere(nearStoresList, {
                            name: curStore.code
                        });
                        if (prefStore) {
                            nearStoresList = _.without(nearStoresList, _.findWhere(nearStoresList, {
                                name: curStore.code
                            }));
                            nearStoresList.unshift(prefStore);
                        }

                        nearStoresList = nearStoresList.slice(0, 49);

                        _.each(nearStoresList, function (store) {
                            setStoreStatus(store, days[today.getDay()]);
                        });

                        var locationCodes = "";

                        _.each(nearStoresList, function (item, index) {
                            if (index != nearStoresList.length - 1) {
                                locationCodes += item.name + ",";
                            } else {
                                locationCodes += item.name;
                            }
                        });

                        api.request('GET', '/api/commerce/catalog/storefront/products/' + productCode + '/locationinventory?pageSize=1100&locationCodes=' + locationCodes).then(function (response) {
                            locationInventory = response.items;
                            var currentDate = new Date();
                            _.each(nearStoresList, function (item) {
                                var storeInventory = _.findWhere(locationInventory, {
                                    locationCode: item.name
                                });
                                if (storeInventory) {
                                    item.locationInventory = storeInventory;
                                    //item.getItBy = self.formatDate(new Date().setDate(currentDate.getDate() + 15));
                                    if (isEcomEnabled) {
                                        item.itemEcomEnabled = true;
                                    }
                                } else {
                                    //item.getItBy = self.formatDate(new Date().setDate(currentDate.getDate() + 15));
                                    if (isEcomEnabled) {
                                        item.itemEcomEnabled = true;
                                    }
                                }
                            });
                            if (ua.indexOf('chrome') > -1) {
                                if (/edg|edge/i.test(ua)) {
                                    $.cookie("quickViewProduct", JSON.stringify({
                                        'productCode': productCode,
                                        'isEcomEnabled': isEcomEnabled ? true : false
                                    }), {path: '/'});
                                } else {
                                    document.cookie = "quickViewProduct=" + JSON.stringify({
                                        'productCode': productCode,
                                        'isEcomEnabled': isEcomEnabled ? true : false
                                    }) + ";path=/;Secure;SameSite=None";
                                }
                            } else {
                                $.cookie("quickViewProduct", JSON.stringify({
                                    'productCode': productCode,
                                    'isEcomEnabled': isEcomEnabled ? true : false
                                }), {path: '/'});
                            }
                            var storeData = {
                                "stores": nearStoresList
                            };
                            LoadResources.load("/scripts/unwired-gl.js", function () {
                                var checkNearbyStoreView = new CheckNearbyStoreModal.CheckNearbyStore({
                                    el: $('#check-nearby-store-modal'),
                                    model: new Backbone.Model(storeData)
                                });
                                if ($.cookie('fullAssortmentProduct') != 'false') {
                                    checkNearbyStoreView.model.set("fullAssortmentSelectStore", true);
                                }
                                checkNearbyStoreView.render();
                            });
                        });

                    }
                }, function (error) {
                    console.error("API Error: ", error);
                });
            } else {
                var nearStoresList = [];
                api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter=' + filtersString + '&pageSize=1100', {
                    cache: false
                }).then(function (response) {
                    if (response && response.items.length > 0) {
                        nearStoresList = response.items;
                        nearStoresList = nearStoresList.slice(0, 49);
                        _.each(nearStoresList, function (store) {
                            setStoreStatus(store, days[today.getDay()]);
                        });
                    }
                    if (ua.indexOf('chrome') > -1) {
                        if (/edg|edge/i.test(ua)) {
                            $.cookie("quickViewProduct", JSON.stringify({
                                'productCode': productCode,
                                'isEcomEnabled': isEcomEnabled ? true : false
                            }), {path: '/'});
                        } else {
                            document.cookie = "quickViewProduct=" + JSON.stringify({
                                'productCode': productCode,
                                'isEcomEnabled': isEcomEnabled ? true : false
                            }) + ";path=/;Secure;SameSite=None";
                        }
                    } else {
                        $.cookie("quickViewProduct", JSON.stringify({
                            'productCode': productCode,
                            'isEcomEnabled': isEcomEnabled ? true : false
                        }), {path: '/'});
                    }
                    var storeData = {
                        "stores": nearStoresList
                    };
                    LoadResources.load("/scripts/unwired-gl.js", function () {
                        var checkNearbyStoreView = new CheckNearbyStoreModal.CheckNearbyStore({
                            el: $('#check-nearby-store-modal'),
                            model: new Backbone.Model(storeData)
                        });
                        if ($.cookie('fullAssortmentProduct') != 'false') {
                            checkNearbyStoreView.model.set("fullAssortmentSelectStore", true);
                        }
                        checkNearbyStoreView.render();
                    });
                });
            }
        },
        getTotalInventory: function (locationInventory) {
            var me = this,
                warehouseInventoryCount = me.model.get('warehouseInventoryCount'),
                locationInventoryCount = locationInventory && locationInventory > 0 ? locationInventory : 0,
                totalInventoryCount = warehouseInventoryCount + locationInventoryCount;
            if (totalInventoryCount > 0) {
                me.model.set("showAddToCart", true);
                me.model.set('isInventoryAvail', true);
            } else {
                me.model.set("isInventoryAvail", false);
                me.model.set("showAddToCart", false);
            }
            if (totalInventoryCount == 1 || totalInventoryCount == 2 || totalInventoryCount == 3) {
                me.model.set("isLimitedQuantity", true);
            } else {
                me.model.set("isLimitedQuantity", false);
            }
            Backbone.MozuView.prototype.render.apply(me, arguments);
        },
        formatDate: function (date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        },
        showEstimateShippingModal: function(event){
            if(!this.model.get('isShippingModalOpened')){
                var productMeasurements =  this.model.apiModel.data.measurements,
                    productPrice = this.model.apiModel.data.price,
                    isCurrentPostalCodeValid = $(event.currentTarget).hasClass('change-location');

                if(estimateShippingModalView) estimateShippingModalView.undelegateEvents();
                estimateShippingModalView = new EstimateShippingModalView({
                    el: $('#shipping-popup-container'),
                    model: new Backbone.MozuModel({
                        quantity: this.model.get('quantity'),
                        productPrice: productPrice,
                        productMeasurements : productMeasurements,
                        isCurrentPostalCodeValid : isCurrentPostalCodeValid,
                        ehfValue: this.model.get('ehfValue') ? this.model.get('ehfValue') : 0,
                        shippingData: this.model.get('shippingPostalCodeData'),
                        wareHouseInventory: this.model.get('wareHouseInventory'),
                        sthShippingTimeline: this.model.get('sthShippingTimeline')
                    })
                });
                this.model.set('isShippingModalOpened', true);
                estimateShippingModalView.render();
            }
        },
        refreshPage: function(){
            var self = this;
            $(document).on('refreshPdp', function(event, data){
                self.model.set('shippingPostalCodeData', data.shippingData);
                self.model.set('isShippingModalOpened', false);
                self.model.set('sthShippingTimeline', data.shippingTimeline);
                self.render();
            });
        }
    });

    var ProductModalView = Backbone.MozuView.extend({
        templateName: 'modules/product/product-modal',
        render: function () {
            var me = this;
            var contextSiteId = apiContext.headers["x-vol-site"];
            var frSiteId = Hypr.getThemeSetting("frSiteId");
            if (frSiteId === contextSiteId) {
                me.model.set("isFrenchSite", true);
            }
            Backbone.MozuView.prototype.render.apply(this);
        },
        initialize: function () {
            var me = this;
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            locale = locale.split('-')[0];
            var currentLocale = '';
            if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite) {
                currentLocale = locale === 'fr' ? '/fr' : '/en';
            }
            me.model.set("currentLocale", locale);
            me.model.set("currentSite", currentSite);
        }
    });

    var parentCategoryId;
    var ProductDetailView = Backbone.MozuView.extend({
        templateName: 'modules/product/product-header-detail',
        initialize: function () {
            var me = this;
            var allProductCategories = [];
            parentCategoryId = Hypr.getThemeSetting('newLBMAndHardlinesCategoryId');
            if (window.location.href.indexOf('ccode') > 0) {
                var categoryCode = window.location.href.split('#ccode=')[1];
                if (categoryCode === 'clearance') {
                    me.model.set('isCategoryCodePresent', false);
                    setTimeout(function () {
                        me.render();
                        $('.pdp-breadcrumbs').removeClass('hidden');
                        me.pushDatalayer(me);
                    }, 3000);
                } else {
                    var locale = require.mozuData('apicontext').headers['x-vol-locale'];
                    me.model.set('isCategoryCodePresent', true);
                    me.model.set('locale', locale);
                    me.model.set('siteId', contextSiteId);
                    var categoryFromCurrentProduct = _.findWhere(me.model.get('categories'), {'categoryCode': categoryCode});
                    if (categoryFromCurrentProduct && (categoryFromCurrentProduct.parentCategoryId == Hypr.getThemeSetting('dynamicCategoryId') || categoryFromCurrentProduct.parentCategoryId == Hypr.getThemeSetting('brandsCategoryId'))) {
                        me.model.set('isCategoryCodePresent', false);
                        setTimeout(function () {
                            me.render();
                            $('.pdp-breadcrumbs').removeClass('hidden');
                            me.pushDatalayer(me);
                        }, 3000);
                    } else {
                        if (!categoryFromCurrentProduct) {
                            var filterQuery = 'categoryCode eq ' + categoryCode;
                            api.get('categories', {filter: filterQuery}).then(function (category) {
                                breadCrumbCategory.push({
                                    'categoryCode': category.data.items[0].categoryCode,
                                    'categoryName': category.data.items[0].content.name
                                });
                                me.getCategoryForBreadcrumb(category.data.items[0].parentCategory.categoryId);
                            });
                        } else {
                            breadCrumbCategory.push({
                                'categoryCode': categoryFromCurrentProduct.categoryCode,
                                'categoryName': categoryFromCurrentProduct.content.name
                            });
                            if (parentCategoryId !== categoryFromCurrentProduct.parentCategoryId) {
                                me.getCategoryForBreadcrumb(categoryFromCurrentProduct.parentCategoryId);
                            } else {
                                me.model.set('breadcrumbs', breadCrumbCategory);
                                me.render();
                                $('.pdp-breadcrumbs').removeClass('hidden');
                                me.pushDatalayer(me);
                            }
                        }
                    }
                }
            } else {
                $('.pdp-breadcrumbs').removeClass('hidden');
                me.pushDatalayer(me);
            }
            setTimeout(function () {
                if (me.model.attributes.price.attributes.onSale) {
                    $(".product-badge").removeClass("hidden");
                }
            }, 3000);
        },
        pushDatalayer: function (me) {
            var breadcrumbs = $(".mz-breadcrumb-link:not('.is-first')");
            var breadcrumbstring = '';
            var GTMListParam = '';
            if (window.location.search.indexOf('page') > 0) {
                GTMListParam = decodeURIComponent(window.location.search.split('page=')[1]);
            }
            var currAttribute = _.findWhere(me.model.attributes.properties, {'attributeFQN': Hypr.getThemeSetting('brandDesc')});
            var j;
            if (window.location.href.indexOf('ccode') > 0) {
                if (window.location.href.split('#ccode=')[1] == "clearance") {
                    var getClearanceCategory = _.findWhere(me.model.get("categories"), {'categoryCode': Hypr.getThemeSetting('clearanceCategoryCode')});
                    breadcrumbstring = getClearanceCategory.content.name;
                } else {
                    var categoryCode = window.location.href.split('#ccode=')[1];
                    var getcategory = _.findWhere(me.model.get("categories"), {'categoryCode': categoryCode});
                    if (getcategory && getcategory.parentCategoryId == Hypr.getThemeSetting("dynamicCategoryId")) {
                        breadcrumbstring = getcategory.content.name;
                    } else {
                        for (j = 0; j < breadcrumbs.length; j++) {
                            if (j !== breadcrumbs.length - 1) {
                                breadcrumbstring += $.trim(breadcrumbs[j].text) + '/';
                            } else {
                                breadcrumbstring += $.trim(breadcrumbs[j].text);
                            }
                        }
                    }
                }
            } else {
                for (j = 0; j < breadcrumbs.length; j++) {
                    if (j !== breadcrumbs.length - 1) {
                        breadcrumbstring += $.trim(breadcrumbs[j].text) + '/';
                    } else {
                        breadcrumbstring += $.trim(breadcrumbs[j].text);
                    }
                }
            }
            if (GTMListParam) {
                if (GTMListParam.indexOf('&rrec=true') !== -1) {
                    GTMListParam = GTMListParam.split('&rrec=true')[0];
                }
            }

             if (me.model.get('productType') != 'Articles') {
				var warehouseInventoryCount = me.model.get('warehouseInventoryCount');
                var productCode = me.model.id;
                var storeCode = me.model.attributes.purchaseLocation;
                var locationInventory, locationInventoryCount, totalInventoryCount;
                api.request("GET", '/api/commerce/catalog/storefront/products/' + productCode + '/locationinventory?locationCodes=' + storeCode + '&time=' + Date.now()).then(function (response) {
                    if (response && response.items.length > 0) {
                        locationInventory = response.items[0].stockAvailable ;
                        locationInventoryCount = locationInventory && locationInventory > 0 ? locationInventory : 0 ;
                        totalInventoryCount = locationInventoryCount + warehouseInventoryCount;
                    } else {
                        locationInventoryCount = 0 ;
                        totalInventoryCount = warehouseInventoryCount;
                    }
                    dataLayer.push({
                        'event': 'detailview',
                        'ecommerce': {
                            'detail': {
                                'actionField': {'list': GTMListParam},
                                'products': [{
                                    'name': me.model.get('content').get('productName'),
                                    'id': me.model.get('productCode'),
                                    'price': me.model.get('price').get('salePrice') > 0 ? parseFloat(me.model.get('price').get('salePrice')).toFixed(2).replace('$', '').replace(',', '.').trim() : parseFloat(me.model.get('price').get('price')).toFixed(2).replace('$', '').replace(',', '.').trim(),
                                    'brand': currAttribute ? currAttribute.values[0].stringValue : '',
                                    'category': breadcrumbstring,
                                    'dimension16' : totalInventoryCount >  0 ? 'In Stock' : 'Out of Stock',
                                    'dimension17' : locationInventoryCount >  0 ? 'Available' : 'Not Available',
                                    'metric3' : totalInventoryCount > 0 ? 0 : 1
                                }]
                            }
                        }
                    });
                });
            }
        },
        getCategoryForBreadcrumb: function (categoryId) {
            var me = this;

            api.request('GET', '/api/commerce/catalog/storefront/categories/' + categoryId).then(function (parentCategory) {
                breadCrumbCategory.push({
                    'categoryCode': parentCategory.categoryCode,
                    'categoryName': parentCategory.content.name
                });
                if (parentCategoryId.toString() !== parentCategory.parentCategory.categoryId.toString()) {
                    me.getCategoryForBreadcrumb(parentCategory.parentCategory.categoryId);
                }
                var newBreadCrumbCategory = [];
                for (var i = breadCrumbCategory.length - 1; i >= 0; i--) {
                    newBreadCrumbCategory.push(breadCrumbCategory[i]);
                }
                me.model.set('breadcrumbs', newBreadCrumbCategory);
                me.render();
                if (parentCategoryId.toString() == parentCategory.parentCategory.categoryId.toString()) {
                    $('.pdp-breadcrumbs').removeClass('hidden');
                    me.pushDatalayer(me);
                }
            });
        }
    });

    var configureCart = function (product, cartitem) {
        api.get('cart', {}).then(function (response) {
            var cartTotalAmount = response.data.total;
            var cartTotalCount = 0;
            _.each(response.data.items, function (product) {
                if (product.product.productCode != 'EHF101') {
                    cartTotalCount = cartTotalCount + product.quantity;
                }
            });
            cartitem = new Backbone.Model(cartitem);
            cartitem.set('cartTotalAmount', cartTotalAmount);
            cartitem.set('cartTotalCount', cartTotalCount);
            cartitem.set('preferredStore', product.get('preferredStore'));
            cartitem.set('quantity', product.get('quantity'));
            var productModalView = new ProductModalView({
                el: $('#product-modal-container'),
                model: cartitem
            });
            productModalView.render();
            $('.view-cart-link').parent().removeClass('is-loading');
        });
    };

    var ProductDetailMobileView = Backbone.MozuView.extend({
        templateName: 'modules/product/product-header-detail-mobile',
        initialize: function () {
            var me = this;
            if (localStorage.getItem('preferredStore')) {
                var currentStore = $.parseJSON(localStorage.getItem('preferredStore'));
                var attribute = _.findWhere(currentStore.attributes, {"fullyQualifiedName": Hypr.getThemeSetting("isEcomStore")});
                var productCode;
                if (me.model.get('variations') && me.model.get('variations').length > 0) {
                    //from current variation find the variationProductCode
                    var productOptionValue = $('.mz-productoptions-option').val();
                    var variation = _.find(me.model.get('variations'), function (variation) {
                        return _.findWhere(variation.options, {'value': productOptionValue});
                    });
                    productCode = variation.productCode;
                } else {
                    productCode = me.model.get('productCode');
                }
                var currentStoresWarehouse = _.find(currentStore.attributes, function (attribute) {
                    return attribute.fullyQualifiedName === Hypr.getThemeSetting('warehouseAttr');
                });

                if (currentStoresWarehouse) {
                    var inventoryApi = '/api/commerce/catalog/storefront/products/' + productCode + '/locationinventory?locationCodes=' + currentStoresWarehouse.values[0];
                    api.request("GET", inventoryApi).then(function (response) {
                        if (response && response.items.length > 0) {
                            if (response.items[0].stockAvailable > 0) {
                                me.model.set('isInventoryAvail', true);
                                me.model.set('wareHouseInventoryAvail', true);
                                if (response.items[0].stockAvailable == 1 || response.items[0].stockAvailable == 2 || response.items[0].stockAvailable == 3) {
                                    me.model.set("isLimitedQuantity", true);
                                }
                                me.render();
                            } else {
                                $(".mobile-show-sale").removeClass("hidden");
                                me.render();
                            }
                        }
                    });
                }

            }
            var createDate = me.model.get("createDate");
            var newTagDate = moment(createDate).add(30, 'd');
            if (moment(createDate) <= moment(today) && moment(today) <= moment(newTagDate)) {
                me.model.set("newTagPromotion", true);
            }
        }
    });

    var certonaTitle = '';
    var CertonaRecommendationsViewAddToCart = Backbone.MozuView.extend({
        templateName: 'Widgets/cms/certona-recommended-products',
        initialize: function () {
            var me = this;
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            me.model.set("currentLocale", locale);
            me.model.set("currentSite", currentSite);
            if (localStorage.getItem('preferredStore')) {
                var currentStore = $.parseJSON(localStorage.getItem('preferredStore'));
                var isBOHStore = _.find(currentStore.attributes, function (attribute) {
                    return attribute.fullyQualifiedName === Hypr.getThemeSetting('hhBOHStoreAttr');
                });
                if (isBOHStore && isBOHStore.values[0]) {
                    me.model.set("isBOHStore", true);
                }
                _.each(currentStore.attributes, function (obj) {
                    if (obj.fullyQualifiedName === 'tenant~storetype' || obj.fullyQualifiedName === 'tenant~store-type') {
                        var storeType = obj.values[0];
                        me.model.set('storeType', storeType);
                    }
                });
            }
            var currentSceme;
            console.log("********Certona Response***********");
            console.log(window.certonaHome);
            var queryStringCertona = "", sorting = [];
            var currentItems = _.find(window.certonaHome.resonance.schemes, function (object) {
                return object.scheme === 'addtocart1_rr';
            });
            if (currentItems) {
                currentItems.items.forEach(function (item, index) {
                    if (index != currentItems.items.length - 1) {
                        queryStringCertona += "productCode eq " + item.ID + " or ";
                    } else {
                        queryStringCertona += "productCode eq " + item.ID;
                    }
                    sorting.push(item.ID);
                });
            }
            if (queryStringCertona) {
                api.get("search", {filter: queryStringCertona}).then(function (certonaResponse) {
                    var newData = [];
                    if (certonaResponse && certonaResponse.data) {
                        var newSortingArray = sorting.filter(function (item) {
                            return certonaResponse.data.items.some(function (certonaItem) {
                                return item === certonaItem.productCode;
                            });
                        });
                        _.each(certonaResponse.data.items, function (item) {
                            newData[newSortingArray.indexOf(item.productCode)] = item;
                        });
                        me.model.set('items', newData);
                        me.model.set('currentSceme', 'addtocart1_rr');
                        me.model.set("isWidget", true);
                        me.model.set("isCertonaProduct", true);
                        me.setElement($(".certona-regular-addtocart1_rr"));
                        $('.content-loading').addClass('hidden');
                        me.render();
                        if (certonaResponse.data.items.length > 0) {
                            var explaination = "";
                            var selectedScheme = _.find(window.certonaHome.resonance.schemes, function (scheme) {
                                return scheme.scheme === 'addtocart1_rr';
                            });
                            if (selectedScheme) {
                                explaination = selectedScheme.explanation;
                            }
                            $(".certona-title-addtocart1_rr").text(explaination);

                            var certonaInnerHtml = document.getElementById("product-certona-container");
                            var innerHtml = certonaInnerHtml.innerHTML;
                            certonaTitle = $("#product-certona-container").find('.certona-title').text();
                            $("#product-certona-container").find(".certona-regular-addtocart1_rr").addClass('oldCertonaAddToCartData').removeClass("certona-regular-addtocart1_rr").removeClass('slick-initialized').removeClass('slick-slider');
                            $("#product-certona-container").find('.certona-title').addClass('hidden');
                            $('.certona-innerHTML-container').append(innerHtml);
                            $('.certona-innerHTML-container').find('div[id^=mz-drop-zone-certona]').removeClass('hidden');
                            $('.certona-innerHTML-container').find('.certona-title').text(certonaTitle);
                            if ($('.certona-innerHTML-container').find('.certona-products-container').hasClass('hidden')) {
                                $('.certona-innerHTML-container').find('.certona-products-container').removeClass('hidden');
                            }
                            $(".certona-regular-addtocart1_rr").removeClass('hidden').removeClass('slick-initialized').removeClass('slick-slider');
                            $('.certona-innerHTML-container').find('.certona-title').removeClass('hidden');
                            $(".certona-regular-addtocart1_rr").not('.slick-initialized').slick({
                                dots: false,
                                infinite: false,
                                slidesToShow: 4,
                                slidesToScroll: 4,
                                variableWidth: true,
                                responsive: [
                                    {
                                        breakpoint: 1024,
                                        settings: {
                                            slidesToShow: 4,
                                            slidesToScroll: 4
                                        }
                                    },
                                    {
                                        breakpoint: 600,
                                        settings: {
                                            infinite: true,
                                            arrows: false,
                                            slidesToShow: 2,
                                            slidesToScroll: 1
                                        }
                                    }
                                ]
                            });//Slider End
                            $(".certona-container").find('.mz-productlisting-title').addClass('add-ellipsis');
                            $("#product-certona-container").find('.oldCertonaAddToCartData').addClass("certona-regular-addtocart1_rr").addClass('slick-initialized').addClass('slick-slider');
                        }
                    }
                });
            }
        }
    });
    $(window).resize(function () {
        var hasclassEllipses = $(".certona-container").find(".mz-productlisting-title").hasClass("add-ellipsis");
        if (hasclassEllipses) {
            $(".certona-container").find(".mz-productlisting-title").removeClass('add-ellipsis');
        }
        setTimeout(function () {
            $(".certona-container").find(".mz-productlisting-title").addClass('add-ellipsis');
            if($('#shipToHomeContainer').length){
                var containerWidth = $('#shipToHomeContainer').width();
                $('.update-shipping-postal-code').width(containerWidth);
            }
        }, 5);
    });
    var LoadResources = {
        load: function (src, callback) {
            var script = document.createElement('script'),
                loaded;
            script.setAttribute('src', src);
            if (callback) {
                script.onreadystatechange = script.onload = function () {
                    if (!loaded) {
                        callback();
                    }
                    loaded = true;
                };
            }
            document.getElementsByTagName('head')[0].appendChild(script);
        }
    };

    /*This function finds stores near preferred store in 50km range by default*/
    function findNearStores(storeList, location, range) {
        var nearStores = [];
        _.each(storeList, function (store) {
            var distance = getDistanceFromLatLongInKm(store.properties.latitude, store.properties.longitude, location.lat, location.lng);
            if (distance <= range) {
                var storeData = {
                    distance: parseFloat(distance.toFixed(3)),
                    modelData: store
                };
                nearStores.push(storeData);
            }
        });
        nearStores = _.sortBy(nearStores, "distance");
        var resultArray = [];
        _.each(nearStores, function (store) {
            resultArray.push(store.modelData);
        });
        return resultArray;
    }

    /*This function calculates and return the distance between preferred store and input store*/
    function getDistanceFromLatLongInKm(lat1, lng1, lat2, lng2) {
        var earthRadius = 6371; // Radius of the earth in km
        var dLat = deg2rad(lat2 - lat1); // deg2rad below
        var dLng = deg2rad(lng2 - lng1);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var distance = earthRadius * c * 0.62137119; // Distance in Mi
        return distance;
    }

    /*this funtion converts deg to rad*/
    function deg2rad(deg) {
        return deg * (Math.PI / 180);
    }

    /*this function set working hours status */
    function setStoreStatus(store, day) {
        store.storeStatus = getStoreStatus(store, day);
    }

    /*this function calculates and returns status object with working hours*/
    function getStoreStatus(store, day) {
        var workingHours = "",
            statusData;
        var start = new Date(),
            end = new Date();
        switch (day) {

            case "Sunday":
                workingHours = store.properties.sundayHours;
                break;
            case "Monday":
                workingHours = store.properties.mondayHours;
                break;
            case "Tuesday":
                workingHours = store.properties.tuesdayHours;
                break;
            case "Wednesday":
                workingHours = store.properties.wednesdayHours;
                break;
            case "Thursday":
                workingHours = store.properties.thursdayHours;
                break;
            case "Friday":
                workingHours = store.properties.fridayHours;
                break;
            case "Saturday":
                workingHours = store.properties.saturdayHours;
                break;
        }
        var currentTime = moment(),
            startTime, endTime;
        if (workingHours !== "Closed") {
            var workHours = workingHours.split("-");
            startTime = moment(workHours[0], "HH:mm a");
            endTime = moment(workHours[1], "HH:mm a");
        }
        if (currentTime.isBetween(startTime, endTime)) {
            statusData = {
                "status": "open",
                "hours": workingHours
            };
        } else {
            statusData = {
                "status": "closed",
                "hours": workingHours
            };
        }
        return statusData;
    }

    function loadNewPdp(me) {
        var productPricePdp = me.$el.find(".mz-price")[0].innerText,
            $prodInfo = $('.product-information'),
            $prodPlaInfo = $('.pla-container'),
            $prodPlaMobInfo = $('.pla-mobile-container'),
            $prodWrapper = $('.mz-productdetail-wrap'),
            outOfStockVisible = me.$el.find(".temprory-outofstock").length,
            addToCartVisible = me.$el.find("#add-to-cart").length,
            isDisabled = me.$el.find("#add-to-cart").hasClass("is-disabled"),
            isDisabledWishList = me.$el.find("#add-to-wishlist").hasClass("is-disabled"),
            isBrandAvailable = me.$el.find(".product-brand").length,
            brandHref = me.$el.find(".product-brand").attr('href'),
            preferredStore = localStorage.getItem('preferredStore'),
                ipad = /ipad/i.test(navigator.userAgent),
                isStoreAllowShipToHome = $('#shipToHomeContainer').length;
            if(isStoreAllowShipToHome){
                $prodInfo.find(".orderOnlineSection").addClass('storeAllowsSTH');
            }

        if (ipad || !preferredStore) {
            $prodInfo.find(".orderOnlineSection").addClass("hidden");
        } else {
            $prodInfo.find(".orderOnlineSection").removeClass("hidden");
        }
        if (addToCartVisible) {
            $prodInfo.find("#add-to-cart-new-pdp").removeClass('hidden');
            $prodInfo.find(".mz-price-new-pdp").text(productPricePdp);
            if (isDisabled) {
                $prodInfo.find("#add-to-cart-new-pdp").addClass('is-disabled');
                $prodInfo.find("#add-to-cart-new-pdp").attr('disabled', 'disabled');
            }
        } else {
            $prodInfo.find("#add-to-wishList-new-pdp").removeClass('hidden');
            if (isDisabledWishList) {
                $prodInfo.find("#add-to-wishList-new-pdp").addClass('is-disabled');
                $prodInfo.find("#add-to-wishList-new-pdp").attr('disabled', 'disabled');
            }
        }
        if (require.mozuData('pagecontext').editMode != "template") {
            if (!outOfStockVisible) {
                $prodWrapper.find('.oos-products').addClass('hide');
            }
            // $prodPlaInfo.find("#product-pla-container").removeClass('hidden');
            var placontainer = $("#product-pla-container").html();
            $("#product-pla-mobile-container").append(placontainer);
            if ($(window).width() < 768) {
                $prodPlaInfo.find("#product-pla-container").remove();
            }
            if ($(window).width() === 808) {
                $prodPlaMobInfo.find("#product-pla-mobile-container").remove();
            }
            if (addToCartVisible) {
                $prodWrapper.find(".oos-products").addClass('hide');
            }
        }
        var specificationLength = $prodInfo.find("#specification").find(".attribute-value").length;
        if (specificationLength === 0) {
            $prodInfo.find("#specification").addClass("hidden");
            $prodInfo.find("#specificationTab").addClass("hidden");
        } else {
            $prodInfo.find("#specification").removeClass("hidden");
            $prodInfo.find("#specificationTab").removeClass("hidden");
        }
        if (isBrandAvailable) {
            $prodInfo.find(".productdetails-title").css("margin-top", "0px");
            $prodInfo.find(".product-header-details").find(".product-brand-new-pdp").attr("href", brandHref);
        } else {
            $prodInfo.find(".productdetails-title").css("margin-top", "23px");
            $prodInfo.find(".inline-ratings-pdp-page").css("top", "53px");
        }
        if (isMobile) {
            $(".mz-searchbox-input").trigger("blur");
        }
        if (!require.mozuData('pagecontext').isEditMode) {
            $(".oos-products").addClass("hide");
            if ($(".temprory-outofstock").hasClass("hide-oos-products")) {
                $(".oos-products").addClass("hide");
            }
        }
        me.$el.find(".orderVolumeMsgAddToCart").removeClass("hidden");
        console.log(me.model.get('isEcomStore'),me.model.get('bohStore'));
        if(me.model.get('isEcomStore')===false &&  me.model.get('bohStore')===false)
        {
            me.$el.find(".orderVolumeMsg").addClass("hidden");
        }
        else  if(me.model.get('isEcomStore')===false &&  me.model.get('bohStore')===true)
        {
            me.$el.find(".orderVolumeMsg").addClass("hidden");
        }
        else
        {
           me.$el.find(".orderVolumeMsg").removeClass("hidden");
        }
    }

    $(document).ready(function () {

        var product = ProductModels.Product.fromCurrent();
        var currAttribute = _.findWhere(product.attributes.properties, {'attributeFQN': Hypr.getThemeSetting('brandDesc')});
        var breadcrumbstring = '';
        product.on('addedtocart', function (cartitem) {
            if (cartitem && cartitem.prop('id')) {
                var sorting = [];
                product.isLoading(false);
                if (!certonaRecommendations) {
                    var certonaRecommendations = window.certonaRecommendations = function (res) {
                        window.certonaHome = res;
                        var certonaRecommendationsViewAddToCart = new CertonaRecommendationsViewAddToCart({
                            model: new Backbone.Model()
                        });
                    };
                }
                CartMonitor.addToCount(maxQuantityAllowed ? maxQuantityAllowed : product.get('quantity'));
                if (checkQuantity) {
                    $('#product-modal-container').modal().show();
                } else {
                    $('#limited-quantity-container').modal().show();
                }
                configureCart(product, cartitem);
                var breadcrumbs = $(".mz-breadcrumb-link:not('.is-first')");
                var i;
                if (window.location.href.indexOf('ccode') > 0) {
                    if (window.location.href.split('#ccode=')[1] == "clearance") {
                        var getClearanceCategory = _.findWhere(product.get("categories"), {'categoryCode': Hypr.getThemeSetting('clearanceCategoryCode')});
                        breadcrumbstring = getClearanceCategory.content.name;
                    } else {
                        var categoryCode = window.location.href.split('#ccode=')[1];
                        var getcategory = _.findWhere(product.get("categories"), {'categoryCode': categoryCode});
                        if (getcategory && getcategory.parentCategoryId == Hypr.getThemeSetting("dynamicCategoryId")) {
                            breadcrumbstring = getcategory.content.name;
                        } else {
                            for (i = 0; i < breadcrumbs.length; i++) {
                                if (i !== breadcrumbs.length - 1) {
                                    breadcrumbstring += $.trim(breadcrumbs[i].text) + '/';
                                } else {
                                    breadcrumbstring += $.trim(breadcrumbs[i].text);
                                }
                            }
                        }
                    }
                } else {
                    for (i = 0; i < breadcrumbs.length; i++) {
                        if (i !== breadcrumbs.length - 1) {
                            breadcrumbstring += $.trim(breadcrumbs[i].text) + '/';
                        } else {
                            breadcrumbstring += $.trim(breadcrumbs[i].text);
                        }
                    }
                }

                var crumbFlowArray;
                var data = {
                    "productCode": product.get('productCode'),
                    "flow": breadcrumbstring
                };
                var expiryDate = new Date();
                expiryDate.setYear(expiryDate.getFullYear() + 1);

                if ($.cookie('BreadCrumbFlow')) {
                    crumbFlowArray = $.parseJSON($.cookie("BreadCrumbFlow"));
                    var isavailable = _.findWhere(crumbFlowArray, {'productCode': data.productCode});
                    if (!isavailable) {
                        if (data.flow) {
                            crumbFlowArray.push(data);
                        }
                    } else {
                        _.find(crumbFlowArray, function (prod, index) {
                            if (prod.productCode == data.productCode) {
                                crumbFlowArray[index] = data;
                            }
                        });
                    }
                    if (ua.indexOf('chrome') > -1) {
                        if (/edg|edge/i.test(ua)) {
                            $.cookie('BreadCrumbFlow', JSON.stringify(crumbFlowArray), {
                                path: '/',
                                expires: expiryDate
                            });
                        } else {
                            document.cookie = "BreadCrumbFlow=" + JSON.stringify(crumbFlowArray) + ";expires=" + expiryDate + ";path=/;Secure;SameSite=None";
                        }
                    } else {
                        $.cookie('BreadCrumbFlow', JSON.stringify(crumbFlowArray), {
                            path: '/',
                            expires: expiryDate
                        });
                    }
                } else {
                    if (data.flow) {
                        crumbFlowArray = [];
                        crumbFlowArray.push(data);
                        if (ua.indexOf('chrome') > -1) {
                            if (/edg|edge/i.test(ua)) {
                                $.cookie('BreadCrumbFlow', JSON.stringify(crumbFlowArray), {
                                    path: '/',
                                    expires: expiryDate
                                });
                            } else {
                                document.cookie = "BreadCrumbFlow=" + JSON.stringify(crumbFlowArray) + ";expires=" + expiryDate + ";path=/;Secure;SameSite=None";
                            }
                        } else {
                            $.cookie('BreadCrumbFlow', JSON.stringify(crumbFlowArray), {
                                path: '/',
                                expires: expiryDate
                            });
                        }
                    }

                }

                api.get('cart', {}).then(function (cartdata) {
                    console.log(cartdata.data.subtotal);
                    var productPrice = product.get('price').get('salePrice') ? product.get('price').get('salePrice') : product.get('price').get('price');
                    var saleforceData = [];
                    _.each(cartdata.data.items, function (item) {
                        saleforceData.push({
                            'id': item.product.productCode, // String. Any unique identifier for a product/SKU
                            'price': item.product.price.salePrice > 0 ? parseFloat(item.product.price.salePrice).toFixed(2).replace('$', '').replace(',', '.').trim() : parseFloat(item.product.price.price).toFixed(2).replace('$', '').replace(',', '.').trim(),
                            'quantity': item.quantity
                        });
                    });
                    dataLayer.push({
                        'event': 'addToCart',
                        'metric4': cartdata.data.total, // String/Currency. This metric tracks total dollar value  that is in the cart (for all products) after this product(s) addition
                        'ecommerce': {
                            'currencyCode': 'CAD',
                            'add': {
                                'products': [{
                                    'name': product.get('content').get("productName"),
                                    'id': product.get('productCode'),
                                    'price': productPrice.toFixed(2).replace('$', '').replace(',', '.').trim(),
                                    'brand': currAttribute ? currAttribute.values[0].stringValue : '',
                                    'category': breadcrumbstring,
                                    'quantity': parseInt(product.get("quantity"), 10)
                                }
                                ]
                            }
                        },
                        'salesForce': {
                                'products': saleforceData,
                                'cartRecoveryId': cartdata.data.id
                        }
                    });
                });
            } else {
                product.trigger("error", {message: Hypr.getLabel('unexpectedError')});
            }

        });
        product.on('addedtowishlist', function (cartitem) {
            $('#add-to-wishlist').prop('disabled', 'disabled').text(Hypr.getLabel('savedToList'));
            $('.product-information').find(".header-right-section").removeClass("is-loading");
            $('#add-to-wishList-new-pdp').prop('disabled', 'disabled').text(Hypr.getLabel('savedToList'));
            /*Update Wishlist Count*/
            api.get('wishlist', {}).then(function (response) {
                var items = response.data.items[0].items;
                var totalQuantity = 0;
                _.each(items, function (item, index) {
                    totalQuantity += item.quantity;
                });
                $('#wishlist-count').html('(' + totalQuantity + ')');
            });
        });
        var scrollToTabPosition = function (e, hash) {
            var headerHeight;
            if ($('#pdpHeader').hasClass("goToTop")) {
                headerHeight = $("#pdpHeader").height() + $(".navbar-new-pdp").height() - 5;
            } else {
                if (!/ipad/i.test(navigator.userAgent)) {
                    headerHeight = $("#pdpHeader").height() + $(".navbar-new-pdp").height() + 70;
                } else {
                    headerHeight = $("#pdpHeader").height() + $(".navbar-new-pdp").height() + 90;
                }
            }
            var scrollToPosition = $(hash).offset().top - headerHeight;
            $('html').animate({'scrollTop': scrollToPosition}, 600, function () {
            });
            e.preventDefault();
        };
        $(window).bind('scroll', function () {
            var navHeight;
            navHeight = $(".container.product-details").children(":first").height() + $(".productdetails-container").height() - 80;
            if (/ipad/i.test(navigator.userAgent)) {
                navHeight = $(".container.product-details").children(":first").height() + $(".productdetails-container").height() - 30;
            }
            if (!isMobile) {
                if ($(window).scrollTop() > navHeight) {
                    $(".new-pdp-main-header-block").addClass("fixedHeader");
                    $('#pdpHeader').addClass('goToTop');
                    $('.product-information').find(".navbar-new-pdp").removeClass('hidden');
                    $("#product-detail").find(".shipping-info").find('.shipping-store-details').addClass('hidden');
                    $("#product-detail").find(".shipping-info").find('.changePreferredStore').addClass('hidden');
                    $("#rating-tab").find('.bv_avgRating_component_container').addClass("hideBVR");
                    $(".productdetails-detail").find(".inline-ratings-pdp-page").find(".bv_averageRating_component_container").addClass("hideBVR");
                } else {
                    $('#pdpHeader').removeClass('goToTop');
                    $(".new-pdp-main-header-block").removeClass("fixedHeader");
                    $('.product-information').find(".navbar-new-pdp").addClass('hidden');
                    $("#product-detail").find(".shipping-info").find('.shipping-store-details').removeClass('hidden');
                    $("#product-detail").find(".shipping-info").find('.changePreferredStore').removeClass('hidden');
                    $("#rating-tab").find('.bv_avgRating_component_container').removeClass("hideBVR");
                }
            }
            var scrollPos = $(document).scrollTop();
            $('#newPdpMainNavbar a').each(function () {
                var currLink = $(this);
                var refElement = $(currLink.attr("href"));
                var refPosition = refElement.position().top - 30;
                if (/ipad/i.test(navigator.userAgent)) {
                    navHeight = $(".container.product-details").children(":first").height() + $(".productdetails-container").height() - 30;
                }
                if (!isMobile) {
                    if ($(window).scrollTop() > navHeight) {
                        $(".new-pdp-main-header-block").addClass("fixedHeader");
                        $('#pdpHeader').addClass('goToTop');
                        $('.product-information').find(".navbar-new-pdp").removeClass('hidden');
                        $("#product-detail").find(".shipping-info").find('.shipping-store-details').addClass('hidden');
                        $("#product-detail").find(".shipping-info").find('.changePreferredStore').addClass('hidden');
                        //$("#rating-tab").find('.bv_avgRating_component_container').addClass("hideBVR");
                        //$(".productdetails-detail").find(".inline-ratings-pdp-page").find(".bv_averageRating_component_container").addClass("hideBVR");
                    } else {
                        $('#pdpHeader').removeClass('goToTop');
                        $(".new-pdp-main-header-block").removeClass("fixedHeader");
                        $('.product-information').find(".navbar-new-pdp").addClass('hidden');
                        $("#product-detail").find(".shipping-info").find('.shipping-store-details').removeClass('hidden');
                        $("#product-detail").find(".shipping-info").find('.changePreferredStore').removeClass('hidden');
                        //$("#rating-tab").find('.bv_avgRating_component_container').removeClass("hideBVR");
                    }
                }
                var combiendPosition = refPosition + refElement.height();
                if (refPosition <= scrollPos && combiendPosition > scrollPos) {
                    $('#newPdpMainNavbar ul li a').removeClass("active");
                    currLink.addClass("active");
                } else {
                    currLink.removeClass("active");
                }
            });
        });
        $("#newPdpMainNavbar a").on('click', function (e) {
            var hash = this.hash;
            scrollToTabPosition(e, hash);
        });
        $(".inline-ratings-pdp-page").on('click', function (e) {
            var hash = "#reviewsTab";
            scrollToTabPosition(e, hash);
        });
        $("#scrollToTopBtn").on("click", function () {
            $("html").animate({scrollTop: 0}, "slow");
        });
        /*Specification toggle functionality */
        $(".toggle-btn").on("click", function (e) {
            e.preventDefault();
            $(this).closest(".toggle-container").toggleClass("is-collapsed");
            $(this).closest(".collapse-section").toggleClass("is-collapsed");
            if ($(this).closest(".toggle-container").hasClass("is-collapsed")) {
                $(this).text(Hypr.getLabel('less'));
                $(this).attr({"title": Hypr.getLabel('less')});
            } else {
                $(this).text(Hypr.getLabel('more'));
                $(this).attr({"title": Hypr.getLabel('more')});
            }
        });
        $("#add-to-cart-new-pdp").on("click", function () {
                window.productView.initiateAddToCart();
        });
        $("#add-to-wishList-new-pdp").on("click", function () {
            $('.product-information').find(".header-right-section").addClass("is-loading");
            window.productView.addToWishlist();
        });
        setTimeout(function () {
            loadNewPdp(window.productView);
        }, 3500);
        /*View all related products functionality*/
        $('.related-list-toggle').on('click', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();

            var $toggle = $(e.target).closest('.related-list-toggle');
            var $parent = $toggle.closest('.related-products-link');
            var expanded = $parent.attr('aria-expanded') === 'true';
            expanded = !expanded;

            $toggle.attr('aria-label', $toggle.attr(expanded ? 'data-label-less' : 'data-label-more'));
            $parent.attr('aria-expanded', expanded);
        });

        var productImagesView = new ProductImageViews.ProductPageImagesView({
            el: $('[data-mz-productimages]'),
            model: product
        });
        window.productImagesView = productImagesView;

        var productView = new ProductView({
            el: $('#product-detail'),
            model: product,
            messagesEl: $('[data-mz-message-bar]')
        });
        window.productView = productView;

        var productDetailView = new ProductDetailView({
            el: $('#product-header-detail'),
            model: product
        });

        var mediaWidth = window.matchMedia("(max-width: 767px)");
        if (mediaWidth.matches) {
            var productDetailMobileView = new ProductDetailMobileView({
                el: $('#product-mobile-header-detail'),
                model: product
            });
            productDetailMobileView.render();
        }

        $.browser = {};
        (function () {
            $.browser.msie = false;
            $.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                $.browser.msie = true;
                $.browser.version = RegExp.$1;
            }
        })();
        productView.render();

        var addToWishlist = window.location.hash;
        if (addToWishlist == "#addToWishlist") {
            productView.addToWishlist();
        }

        var allCategoriesPDP = window.allCategories = [], parentCategoryId, currentCategoryFlow,
            productImpressionArray = [], productImpression = {};
        var AllProductsImpressions = [];

        function getParentCategory(parentCategoryId, currentCategoryId, allCategoriesPDP, impression) {
            var parentCategoryData = [];
            _.find(allCategoriesPDP, function (val) {
                if (val.categoryId === currentCategoryId) {
                    var parentCategoryName = val.content.name;
                    currentCategoryFlow = parentCategoryName + '/' + currentCategoryFlow;
                    if (parentCategoryId != val.parentCategory.categoryId) {
                        //if current category is not NEW LBM And Hardlines then do the same process for current category
                        getParentCategory(parentCategoryId, val.parentCategory.categoryId, allCategoriesPDP, impression);
                    } else {
                        //if current category is NEW LBM And Hardlines then do stop the process and set the ccategory flow in model
                        var price = $(impression).find('.mz-price.is-saleprice').length > 0 ? $(impression).find('.mz-price.is-saleprice').text() : $(impression).find('.mz-price').text();
                        if (price && price.indexOf('/') > 0) {
                            price = price.split('/')[0];
                        }
                        var listAttr = '';
                        if ($(impression).find('.mz-productlisting-title') && $(impression).find('.mz-productlisting-title').length > 0) {
                            listAttr = $(impression).find('.mz-productlisting-title').attr('href').split('?page=')[1];
                        } else if ($(impression).find('.product-title') && $(impression).find('.product-title').length > 0) { //for related products
                            listAttr = $(impression).find('.product-title').children().attr('href').split('?page=')[1];
                        } else if ($(impression).find('.productTile-title-link') && $(impression).find('.productTile-title-link').length > 0) { //for image slider widget
                            listAttr = $(impression).find('.productTile-title-link').attr('href').split('?page=')[1];
                        }
                        if (listAttr) {
                            if (listAttr.indexOf('&rrec=true') !== -1) {
                                listAttr = listAttr.split('&rrec=true')[0];
                            }
                        }
                        productImpression = {
                            'name': $(impression).attr('data-mz-productname'),
                            'id': $(impression).attr('data-mz-product'),
                            'price': $.trim(price.replace('$', '').replace(',', '.')),
                            'brand': $.trim($(impression).find('.brand-name').text()),
                            'category': currentCategoryFlow,
                            'position': $(impression).attr('data-mz-position'),
                            'list': listAttr
                        };
                        productImpressionArray.push(productImpression);
                    }
                }
            });
        }


        setTimeout(function () {
            var productImpressions = [];
            productImpressions = createImpressionArray();
            if (productImpressions.length > 0) {
                if (sessionStorage.getItem('allCategories')) {
                    allCategoriesPDP = window.allCategories = JSON.parse(sessionStorage.getItem('allCategories'));
                    sendGTMDatalayer(productImpressions);
                } else {
                        var responseFields = "?responseFields=items(categoryCode, childrenCategories(categoryCode, childrenCategories(categoryCode, childrenCategories(categoryCode, childrenCategories(categoryCode, content(name)), content(name), parentCategory), content(name), parentCategory), content(name), parentCategory), content(name), parentCategory)";
                        api.request('GET', '/api/commerce/catalog/storefront/categories/tree' + responseFields, {
                        cache: false
                    }).then(function (result1) {
                        if (result1.items.length > 0) {
                            _.each(result1.items[1].childrenCategories, function (children) {
                                allCategoriesPDP.push(children);
                                _.each(children.childrenCategories, function (subChildren) {
                                    allCategoriesPDP.push(subChildren);
                                    _.each(subChildren.childrenCategories, function (subSubChildren) {
                                        allCategoriesPDP.push(subSubChildren);
                                        _.each(subSubChildren.childrenCategories, function (subSubSubChildren) {
                                            allCategoriesPDP.push(subSubSubChildren);
                                        });
                                    });
                                });
                            });

                            var pluckedArray = [];
                            _.map(allCategoriesPDP, function (category) {
                                pluckedArray.push(_.pick(category, 'categoryId', 'parentCategory', 'content'));
                            });
                            window.allCategories = pluckedArray;
                            sessionStorage.setItem('allCategories', JSON.stringify(pluckedArray));
                            sendGTMDatalayer(productImpressions);
                        }
                    });
                }
            }
        }, 4000);

        function sendGTMDatalayer(productImpressions) {
            parentCategoryId = Hypr.getThemeSetting('newLBMAndHardlinesCategoryId');
            _.each(productImpressions, function (impression) {
                var productCategory;
                if ($(impression).attr('data-mz-categoryId')) {
                    productCategory = parseInt($(impression).attr('data-mz-categoryId').split('|')[0], 10);
                }
                _.find(allCategoriesPDP, function (val) {
                    if (val.categoryId === productCategory) {
                        var currentCategoryName = val.content.name;
                        var currentParentId = parseInt($(productImpressions[0]).attr('data-mz-categoryId').split('|')[1], 10);
                        currentCategoryFlow = currentCategoryName;
                        if (parentCategoryId != currentParentId) {
                            //if current category is not NEW LBM And Hardlines then do the same process for current category
                            getParentCategory(parentCategoryId, currentParentId, allCategoriesPDP, impression);
                        } else {
                            var price = $(impression).find('.mz-price.is-saleprice').length > 0 ? $(impression).find('.mz-price.is-saleprice').text() : $(impression).find('.mz-price').text();
                            if (price && price.indexOf('/') > 0) {
                                price = price.split('/')[0];
                            }
                            var listAttr = '';
                            if ($(impression).find('.mz-productlisting-title') && $(impression).find('.mz-productlisting-title').length > 0) {
                                listAttr = $(impression).find('.mz-productlisting-title').attr('href').split('?page=')[1];
                            } else if ($(impression).find('.product-title') && $(impression).find('.product-title').length > 0) { //for related products
                                listAttr = $(impression).find('.product-title').children().attr('href').split('?page=')[1];
                            } else if ($(impression).find('.productTile-title-link') && $(impression).find('.productTile-title-link').length > 0) { //for image slider widget
                                listAttr = $(impression).find('.productTile-title-link').attr('href').split('?page=')[1];
                            }
                            productImpression = {
                                'name': $(impression).attr('data-mz-productname'),
                                'id': $(impression).attr('data-mz-product'),
                                'price': $.trim(price.replace('$', '').replace(',', '.')),
                                'brand': $.trim($(impression).find('.brand-name').text()),
                                'category': currentCategoryFlow,
                                'position': $(impression).attr('data-mz-position'),
                                'list': listAttr
                            };
                            productImpressionArray.push(productImpression);
                        }
                    }
                });
            });

            //send datalayer

            // Product impressions can be sent on the pageview event.
            if (productImpressions && productImpressions.length > 0) {
                dataLayer.push({
                    'event': 'impressionview',
                    'ecommerce': {
                        'currencyCode': 'CAD',
                        'impressions': productImpressionArray
                    }
                });
            }

        }

        function createImpressionArray() {
            var productImpressions = $('.ign-data-product-impression').filter(function () {
                return $(this).closest('.certona-products-container').length === 0;
            });
//			create impression without certona products as the separate impression arrey for certona will be created from certona-content.js file
            var imageSliderProductImpression = $('.ign-data-product-impression-image-slider');
            if (imageSliderProductImpression && imageSliderProductImpression.length > 0) {
                _.each($('.bannerimg.slick-slide[aria-describedby]'), function (banner) {
                    _.each($(banner).find('.ign-data-product-impression-image-slider'), function (productOnBanner) {
                        productImpressions.push(productOnBanner);
                    });
                });
            }
            return productImpressions;
        }


        if (/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            $('.product-information').find('.certona-products-container').addClass("hidden");
        }

        function tabControl() {
            var tabs = $('.product-information').find('.prod-info-tab');
            var attrTitleLength = $('.productdetails-attributes').find('.attribute-title').length;
            var attrValueLength = $('.productdetails-attributes').find('.attribute-value').length;
            var detailsSectionLength = $('#details').find('#prodDetails ul').length;
            var prodIngredientsLength = $('#details').find('.product-ingredients').length;
            var prodDocumentLength = $('#details').find('.product-document').length;
            if (detailsSectionLength === 0 && prodIngredientsLength === 0 && prodDocumentLength === 0) {
                $('#detailsTab').addClass('hidden').removeClass('tablist');
                if ($(window).width() < 768) {
                    $('#details').addClass('hidden');
                }
            }
            if($("#overviewTab").hasClass('hidden') && $("#specificationTab").hasClass("hidden") && $("#detailsTab").hasClass('hidden')){
                $("#MoreLikeThisTab").addClass('active');
                if($(window).width() > 767){
                    $("#specification").removeClass('active');
                    $('#morelikethis').addClass('active');
                    $('#compareproducts').addClass('active');
                }
            }
            if ($('#overviewTab').hasClass('hidden') && $('#detailsTab').hasClass('hidden')) {
                $("#specification").addClass('active');
                $("#details").removeClass('active');
            }
            if (attrTitleLength === 0 && attrValueLength === 0) {
                $('#specificationTab').removeClass('tablist active').addClass('hidden');
                if ($(window).width() < 768) {
                    $('#specification').addClass('hidden');
                }
            } else {
                $('#specificationTab').removeClass('hidden').addClass('tablist');
                if ($(window).width() < 768) {
                    $('#specification').removeClass('hidden');
                }
            }
            if ($("#overviewTab").hasClass('hidden') && $("#detailsTab").hasClass('hidden')) {
                $("#specificationTab").addClass('active');
            }
            if ($("#overviewTab").hasClass('hidden') && $("#specificationTab").hasClass("hidden") && $("#detailsTab").hasClass('hidden')) {
                $("#MoreLikeThisTab").addClass('active');
                if ($(window).width() > 767) {
                    $("#specification").removeClass('active');
                    $('#morelikethis').addClass('active');
                }
            }
            if (tabs.is(':visible')) {
                tabs.find('.tablist').on('click', function (event) {
                    event.preventDefault();
                    var me = this;
                    var target = $(me).attr('href'),
                        tabs = $(me).parents('.prod-info-tab'),
                        buttons = tabs.find('a'),
                        item = tabs.parents('.product-information').find('.item');
                    buttons.removeClass('active');
                    item.removeClass('active');
                    var getClass = $(event.currentTarget).hasClass('hidden');
                    if (!getClass) {
                        $(me).addClass('active');
                        $(target).addClass('active');
                    }
                });
            }
        }

        tabControl();
        if (Hypr.getThemeSetting("homeFurnitureSiteId") === apiContext.headers["x-vol-site"]) {
            if ($("#main-content-container .mainTitle").text().length <= 0) {
                $('#main-content-container .mainTitle').parent().parent().addClass("hidden");
                $('#main-content-container .mainTitle').parent().parent().parent().addClass("no-border");
            }
        }
            /* Hide header message */
            $(".pdpBannerClose").on("click",function(){
                $('.pdp_shipping_text').addClass("hidden");
            });
    });

    $(".mobileVisible").on("click", function () {
        var currentElement = $(this).parent().attr('id');
        var grandParentId = $(this).parent().parent().attr('id');
        if (grandParentId === "specification") {
            if ($('.product-information').find('#mobileSpecification').hasClass("hidden")) {
                $('.product-information').find('#mobileSpecification').removeClass("hidden");
            } else {
                $('.product-information').find('#mobileSpecification').addClass("hidden");
            }
        }else if(grandParentId === "documentsGuides"){
        if($('.product-information').find('#mobileDocumentsGuides').hasClass("hidden")){
            $('.product-information').find('#mobileDocumentsGuides').removeClass("hidden");
        }else{
            $('.product-information').find('#mobileDocumentsGuides').addClass("hidden");
        }
        } else if (grandParentId === "reviewsTab") {
            if ($('.product-information').find('#mobileReviews').hasClass("hidden")) {
                $('.product-information').find('#mobileReviews').removeClass("hidden");
            } else {
                $('.product-information').find('#mobileReviews').addClass("hidden");
            }
        } else if (grandParentId === "QATab") {
            if ($('.product-information').find('#mobileQA').hasClass("hidden")) {
                $('.product-information').find('#mobileQA').removeClass("hidden");
            } else {
                $('.product-information').find('#mobileQA').addClass("hidden");
            }
        } else {
            if (currentElement === 'morelikethis') {
                $('#mz-drop-zone-rti-recommended-product').toggleClass("rti-toggle ");
                $(".morelikethis-title").toggleClass("morelikethis-collapse");
                if ($('.product-information').find('#morelikethis').find('.certona-products-container').hasClass("hidden")) {
                    $('#morelikethis').find('.certona-products-container').removeClass("hidden");
                } else {
                    $('#morelikethis').find('.certona-products-container').addClass("hidden");
                }
            } else if (currentElement === 'frequentlyBought') {
                $('#mz-drop-zone-certona-frequentlyboughttogether-product').toggleClass("rti-toggle ");
                $(".frequentlyBought-title").toggleClass("morelikethis-collapse");
                if ($('#frequentlyBought').find('.certona-products-container').hasClass("hidden")) {
                    $('#frequentlyBought').find('.certona-products-container').removeClass("hidden");
                } else {
                    $('#frequentlyBought').find('.certona-products-container').addClass("hidden");
                }
            } else {
                if (currentElement === 'recentlyViewed') {
                    $('#mz-drop-zone-certona-recently-viewed-product').toggleClass("rti-toggle ");
                    $(".recentlyViewed-title").toggleClass("morelikethis-collapse");
                    if ($('#recentlyViewed').find('.certona-products-container').hasClass("hidden")) {
                        $('#recentlyViewed').find('.certona-products-container').removeClass("hidden");
                    } else {
                        $('#recentlyViewed').find('.certona-products-container').addClass("hidden");
                    }
                }
            }
        }
    });
    $("#MoreLikeThisTab").on("click", function () {
        var hasclassEllipses = $(".certona-container").find(".mz-productlisting-title").hasClass("add-ellipsis");
        if (hasclassEllipses) {
            $(".certona-container").find(".mz-productlisting-title").removeClass('add-ellipsis');
        }
        setTimeout(function () {
            $(".certona-container").find(".mz-productlisting-title").addClass('add-ellipsis');
        }, 5);
    });

    $('.popupover').popover();
    $("body").on("click touchstart mouseover", '.popupover', function () {
            $(this).popover("show");
            $('.popupover').not(this).popover("hide"); // hide other popovers
            return false;
    });
    $("body").on("click touchstart mouseover", function () {
            $('.popupover').popover("hide"); // hide all popovers when clicked on body
    });
    var productData =[];
    var removeProductionFromPreview = function(productCode){
        var indexOfProductToRemove;
        productData.forEach(function(product, index){
            if(product.productCode === productCode) {
                indexOfProductToRemove = index;
                $('.compare-checkbox[data-mz-productcode='+product.productCode+']').attr('checked', false);
            }
        });
        productData.splice(indexOfProductToRemove, 1);
        CompareProductView.model.set('data', productData);
        var productModelLength = CompareProductView.model.get('data').length;
        if (productModelLength < 4) {
            _.each($('.compare-checkbox'), function(checkbox) {
                if(!$(checkbox).prop('checked')) {
                    $(checkbox).attr('disabled',false);
                    $(checkbox).closest('.compare-section').next().removeClass('disabled');
                }
            });
        }
        CompareProductView.render();
    };
    var ComapreProductModal = Backbone.MozuView.extend({
        templateName: 'modules/product/compare-product-modal',
        initialize:function(){
        },
        clearAllCheckbox: function() {
            this.model.set('data',[]);
            productData = [];
            var checkedBoxes = $('.compare-checkbox:checked');
            _.each(checkedBoxes, function(checkedBox){
                $(checkedBox).attr('checked', false);
            });
            var firstCheckedBox = $('#compareproducts').find('.mz-productlisting:first .compare-checkbox');
            if (firstCheckedBox) {
                firstCheckedBox.prop("checked", true);
            }
            $('.compare-all-button').addClass('hide');
            _.each($('.compare-checkbox'), function(checkbox) {
                $(checkbox).attr('disabled',false);
                $(checkbox).closest('.compare-section').next().removeClass('disabled');
            });
            this.render();
        },
        removeProduct: function(e) {
            var indexOfProductToRemove;
            this.model.get('data').forEach(function(product, index){
                if(product.productCode === $(e.currentTarget).attr('data-mz-productcode')) {
                    indexOfProductToRemove = index;
                    $('.compare-checkbox[data-mz-productcode='+product.productCode+']').attr('checked', false);
                }
            });
            this.model.get('data').splice(indexOfProductToRemove, 1);
            var modelLength = this.model.get('data').length;
            if (modelLength < 4) {
                _.each($('.compare-checkbox'), function(checkbox) {
                    if(!$(checkbox).prop('checked')) {
                        $(checkbox).attr('disabled',false);
                        $(checkbox).closest('.compare-section').next().removeClass('disabled');
                    }
                });
            }
            removeProductionFromPreview($(e.currentTarget).attr('data-mz-productcode'));
            if (indexOfProductToRemove === 0 && modelLength === 0) {
                this.model.set('data',[]);
                productData = [];
                $("#compare-product-modal").modal('hide');
            }
            this.render();
        }
    });


    var compareProductView = Backbone.MozuView.extend({
        templateName: 'modules/product/compare-products-feature',
        render: function() {
            Backbone.MozuView.prototype.render.apply(this, arguments);
        },
        removeProduct: function(e) {
            removeProductionFromPreview($(e.currentTarget).attr('data-mz-productcode'));
            this.render();
        },
        viewCompareDetails: function() {
            if(this.model.get('data').length === 1){
                this.model.set('error', Hypr.getLabel('selectTwoProductsError'));
                this.render();
            }else {
                this.model.set('error', '');
                this.render();
                $('#compare-product-modal').modal().show();
                var filterProductCode = _.map(this.model.get('data'), function(product) { return "productCode eq " + product.productCode; }).join(' or ');

                var productModel = Backbone.MozuModel.extend({
                    mozuType: 'products'
                });

                var ProductModel = new productModel();
                ProductModel.set('filter',  filterProductCode);
                ProductModel.set("responseFields", "items(productCode, properties, content, options, price, measurements)");
                ProductModel.fetch().then(function(responseObject) {
                    var productResponse = responseObject.apiModel.data.items;
                    var productResponseInSequence = [];
                    _.each(productResponse, function(product, index) {
                        productResponseInSequence[productData.indexOf(_.find(productData, function(p){return p.productCode === product.productCode;}))] = product;
                    });
                    CompareProductModalView.model.set('data', productResponseInSequence);
                    var locale = require.mozuData('apicontext').headers['x-vol-locale'];
                    locale = locale.split('-')[0];
                    var currentLocale = locale === 'fr' ? '/fr' : '/en';
                    var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
                    CompareProductModalView.model.set('currentLocale', currentLocale);
                    CompareProductModalView.model.set('currentSite', currentSite);

                    var propertyData = [];
                    _.each(productResponse, function(product) {
                        if (product.properties && Hypr.getThemeSetting("showProductDetailProperties")){
                            _.each(product.properties, function(property){
                                if(
                                property.values && !property.isHidden && property.attributeDetail.inputType != "YesNo" &&
                                property.attributeFQN != Hypr.getThemeSetting("modelAttr") && property.attributeFQN != Hypr.getThemeSetting("brandCode") &&
                                property.attributeFQN != Hypr.getThemeSetting("itemDescription") &&
                                property.attributeFQN != Hypr.getThemeSetting("hhSellingUOM") &&
                                property.attributeFQN != Hypr.getThemeSetting("hhLumberInd") &&
                                property.attributeFQN != Hypr.getThemeSetting("hhHardwareInd") &&
                                property.attributeFQN != Hypr.getThemeSetting("hhFurnitureInd") &&
                                property.attributeFQN != Hypr.getThemeSetting("supplierDirectItem") &&
                                property.attributeFQN != Hypr.getThemeSetting("hhWarrantyExchangeInd") &&
                                property.attributeFQN != Hypr.getThemeSetting("limitedQtyInd") &&
                                property.attributeFQN != Hypr.getThemeSetting("displayItem") &&
                                property.attributeFQN != Hypr.getThemeSetting("hhBulkyCourierInd") &&
                                property.attributeFQN != Hypr.getThemeSetting("hhFragileCourierInd") &&
                                property.attributeFQN != Hypr.getThemeSetting("comments") &&
                                property.attributeFQN != Hypr.getThemeSetting("productVideo") &&
                                property.attributeFQN != Hypr.getThemeSetting("relatedProducts") &&
                                property.attributeFQN != Hypr.getThemeSetting("productRating") &&
                                property.attributeFQN != Hypr.getThemeSetting("productUpsell") &&
                                property.attributeFQN != Hypr.getThemeSetting("fineClass") &&
                                property.attributeFQN != Hypr.getThemeSetting("ehfFees") &&
                                property.attributeFQN != Hypr.getThemeSetting("actualProductCode") &&
                                property.attributeFQN != Hypr.getThemeSetting("productWeight") &&
                                property.attributeFQN != Hypr.getThemeSetting("pilarType") &&
                                property.attributeFQN != Hypr.getThemeSetting("marketingDescription") &&
                                property.attributeFQN != Hypr.getThemeSetting("ingredients") &&
                                property.attributeFQN != Hypr.getThemeSetting("promoItemType") &&
                                property.attributeFQN != Hypr.getThemeSetting("imageId") &&
                                property.attributeFQN != Hypr.getThemeSetting("mktKeyword") && property.attributeFQN != Hypr.getThemeSetting("homeExclusiveInd") &&
                                property.attributeFQN != Hypr.getThemeSetting("onlyAtHH") && property.attributeFQN != Hypr.getThemeSetting("availability") &&
                                property.attributeFQN != Hypr.getThemeSetting("priceListEntityType") &&
                                property.attributeFQN != Hypr.getThemeSetting("gender") &&
                                property.attributeFQN != Hypr.getThemeSetting("disclaimerAttr") &&
                                property.attributeFQN != Hypr.getThemeSetting("onsaleAttr") &&
                                property.attributeFQN != Hypr.getThemeSetting("soldItemAttr")
                                ){
                                    var propertyName = property.attributeDetail.name.split('_');
                                    if(propertyName[0] != "DynAttribute" && propertyName[0] != "DynAttribut") {
                                        var data = {};
                                        if(propertyData.length > 0){
                                            var isPropertyPresent = false;
                                            _.each(propertyData, function(currentProperty) {
                                                if(currentProperty.attributeFQN === property.attributeFQN) {
                                                    isPropertyPresent = true;
                                                }
                                            });
                                            if(!isPropertyPresent) {
                                                data = {'attributeFQN': property.attributeFQN, 'value' : property.attributeDetail.name};
                                                propertyData.push(data);
                                            }

                                        }else {
                                            data = {'attributeFQN': property.attributeFQN, 'value' : property.attributeDetail.name};
                                            propertyData.push(data);
                                        }
                                    }
                                }
                            });
                        }
                    });
                    CompareProductModalView.model.set('propertyData', propertyData);
                    if (localStorage.getItem('preferredStore')){
                        CompareProductModalView.model.set({"preferredStore":$.parseJSON(localStorage.getItem('preferredStore'))});
                    }
                    CompareProductModalView.render();
                });
            }
        }
    });
    $('#compare-product-modal').on('hidden.bs.modal', function () {
        CompareProductModalView.model.set('data', []);
        CompareProductModalView.model.set('propertyData', []);
        CompareProductModalView.render();
    });
    $('#compare-product-modal').bind('mousewheel', function(e){
        var modalHeight = $("#compare-product-modal").find('.modal-body').height(),
            imgHeight = $('#product-img-row').height(),
            nameHeight = $('#product-name-row').height(),
            scrollPosition = $("#compare-product-modal").find('.modal-body').scrollTop();
            var totalHeight = modalHeight-(imgHeight+nameHeight);
        if (e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) {
            if (scrollPosition < imgHeight){
                $('.top-menu-element.img-section').addClass('hide');
            }
        }
        else{
            if (scrollPosition > totalHeight){
                $('.top-menu-element.img-section').removeClass('hide');
            }

        }
    });
    if(navigator.userAgent.toLowerCase().indexOf("firefox") > -1){
        $('#compare-product-modal').bind('wheel', function(e){
            if(e.originalEvent.deltaY < 0){
                $('.top-menu-element.img-section').addClass('hide');
            }
            else{
                $('.top-menu-element.img-section').removeClass('hide');
            }
        });
    } else {
        var lastY;
        $('#compare-product-modal').bind('touchstart', function(e) {
            lastY = e.originalEvent.touches ? e.originalEvent.touches[0].pageY : e.pageY;
            console.log(lastY);
        });
        $('#compare-product-modal').bind('touchmove', function (e) {
            var currentY = e.originalEvent.touches ?  e.originalEvent.touches[0].pageY : e.pageY;
            if (Math.abs(currentY-lastY) < 15) { return; }
            if (currentY > lastY) {
                $('.top-menu-element.img-section').addClass('hide');
            } else {
                $('.top-menu-element.img-section').removeClass('hide');
            }
        });
    }
    $(document).on('change',".compare-checkbox", function(e){
        var checkedBoxes = $('.compare-checkbox:checked');
        if (checkedBoxes.length > 1) {
            $('.compare-all-button').removeClass('hide');
        } else {
            $('.compare-all-button').addClass('hide');
        }
        if (checkedBoxes.length == 4) {
            _.each($('.compare-checkbox'), function(checkbox) {
                if(!$(checkbox).prop('checked')) {
                    $(checkbox).attr('disabled',true);
                    $(checkbox).closest('.compare-section').next().addClass('disabled');
                }
            });
        } else {
            _.each($('.compare-checkbox'), function(checkbox) {
                if(!$(checkbox).prop('checked')) {
                    $(checkbox).attr('disabled',false);
                    $(checkbox).closest('.compare-section').next().removeClass('disabled');
                }
            });
        }
        if(checkedBoxes.length > 0){
            if(checkedBoxes.length <= 4) {
                if(productData.length < 2) {
                    var firstProduct = {
                        productCode: checkedBoxes.attr('data-mz-productcode'),
                        imgUrl: checkedBoxes.attr('data-mz-img')
                    };
                    productData.push(firstProduct);
                }
                if($(e.currentTarget).prop('checked')){
                    var product = {
                        productCode: $(e.currentTarget).attr('data-mz-productcode'),
                        imgUrl: $(e.currentTarget).attr('data-mz-img')
                    };
                    productData.push(product);
                    productData = _.uniq(productData, function(product){ return product.productCode; });
                }else{
                    var indexOfProductToRemove;
                    productData.forEach(function(product, index){
                        if(product.productCode === $(e.currentTarget).attr('data-mz-productcode')) {
                            indexOfProductToRemove = index;
                        }
                    });
                    productData.splice(indexOfProductToRemove, 1);
                }
            }
        }else {
            productData = [];
        }
        CompareProductView.model.set('data', productData);
        if(CompareProductView.model.get('data').length === 2){
            CompareProductView.model.set('error', '');
        }
        CompareProductView.render();
    });
    var CompareProductView = new compareProductView({
        el:$('#compare-product-pdp-container'),
        model: new Backbone.Model()
    });
    var CompareProductModalView = new ComapreProductModal({
        el: $('#compare-product-modal'),
        model: new Backbone.Model()
    });
});
