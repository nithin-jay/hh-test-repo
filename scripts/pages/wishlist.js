require(["modules/jquery-mozu", 
  "underscore", 
  "hyprlive", 
  "modules/backbone-mozu", 
  'modules/api',
  'hyprlivecontext',
  'modules/models-customer',
  'modules/editable-view',
  "modules/cart-monitor",
  'modules/models-product'
  ], 
  function ($, _, Hypr, Backbone, api, HyprLiveContext, CustomerModels, EditableView, CartMonitor, ProductModels) {
  var user = require.mozuData('user');
  var apiContext = require.mozuData('apicontext');
  var contextSiteId = apiContext.headers["x-vol-site"];
  var homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId");
  var updatedWishlist,
      configPromises=[], entityListItems,
      storeEmail;
  var WishlistView = EditableView.extend({
        templateName: 'modules/common/wish-list',
        initialize: function() {
          var me = this, itemsToRemove = [], preferredStore;
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            locale = locale.split('-')[0];
            var currentLocale = '';
	            if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
				        currentLocale = locale === 'fr' ? '/fr' : '/en';
			        }
            me.model.set("currentLocale", locale);
            me.model.set("currentSite", currentSite);
               if(localStorage.getItem('preferredStore')){
                  preferredStore = $.parseJSON(localStorage.getItem('preferredStore')); 
                  var attribute = _.findWhere(preferredStore.attributes, {"fullyQualifiedName":Hypr.getThemeSetting("isEcomStore")});   
                  if(attribute){
                      var isEcomStore = attribute.values[0] === "Y" ? true : false; 
                      me.model.set("isEcomStore", isEcomStore);
                  } 
               }
               if($.cookie("storeEmail")){
                 storeEmail = $.cookie("storeEmail");
                 if (storeEmail !== "" && storeEmail !== undefined ) {
                     me.model.set("isStoreEmail", true);
                 } else {
                     me.model.set("isStoreEmail", false);
                 }
               }
               var wishListItems = [];
               if(me.model.get('items')) {
            	   wishListItems = me.model.get('items').models.length > 0 ? me.model.get('items').models : [];
               }
              
              if(wishListItems.length > 0){
                var firstItemQuantity = wishListItems[0].get('quantity');
                 _.each(wishListItems, function(item, index) {
                    if(index > 0) {
                      firstItemQuantity += item.get('quantity');
                    }
                 }); 
                me.model.set('totalItemsQuantity', firstItemQuantity); 

                wishListItems.forEach(function(item){
                  if(item.get('purchaseLocation') !== preferredStore.code){
                    itemsToRemove.push(item);//line item
                  }
                });
                if(itemsToRemove.length > 0){
                  var queryString = "";
                  wishListItems.forEach(function(item,index){
                    if(index != wishListItems.length-1){
                      queryString += "productCode eq "+item.get('product').get('productCode')+" or ";
                    }else{
                      queryString += "productCode eq "+item.get('product').get('productCode');
                    }
                  });
                  
                //Fetch entitylist for the products from cart
                  var entityListForEHF = Hypr.getThemeSetting('entityListForEHF');
                  if(queryString){
                  	var ehfQueryString = queryString + " and province eq " + preferredStore.address.stateOrProvince;
                  	api.get('entityList', { listName: entityListForEHF, filter: ehfQueryString }).then(function(response) {
          	    		if(response.data.items.length > 0) { //if EHF is applied for current product
          	    			entityListItems = response.data;
          	    		}
                      });
                  }
                  
                  
                   var removeSteps = me.getRemoveSteps(itemsToRemove);
                    api.steps(removeSteps).then(function(response){
                      api.get("products",{filter: queryString}).then(function(reponse){
                        $("button.printorder-email").attr( "disabled", "disabled" );
                        $("button.printorder-email").addClass("disabled");
                          me.performAddSteps(itemsToRemove ,reponse.data.items);
                      });
                  });
                  
                } 
            }  
              var documentListForAeroplanPromo = Hypr.getThemeSetting('documentListForAeroplanPromo');
              api.get('documentView', {listName: documentListForAeroplanPromo}).then(function(response) {
  		    	if(response.data.items.length > 0) {
  		    		var aeroplanMilesFactor = response.data.items[0].properties.markupFactor;
  		    		me.model.set('aeroplanMiles', aeroplanMilesFactor);
  		    		me.model.set('isPromoApplied', true);
  		    		me.render();
  		    	}
  			});
        },
        getRemoveSteps: function(items){
          var self = this;
          var tasks = items.map(function(item){
            return function(){
              return self.removeWishlistItem(item.id);
            };  
          });
          return tasks;
        },
        removeWishlistItem: function(itemId){
            return this.model.apiDeleteItem(itemId);
        },
        performAddSteps: function(wishlistItems, products){
            var self = this;
            self.getFinalProducts(wishlistItems, products);
            $.when(self.resolvePromises(configPromises)).then(function(finalProducts) {
                var addSteps = finalProducts.map(function(finalProduct) {
                var oldItem = _.find(wishlistItems, function(item){
                      return item.attributes.product.attributes.productCode === finalProduct.attributes.productCode;
                  });
                  finalProduct.set("quantity", oldItem.attributes.quantity);  
                  return function(){
                      return finalProduct.apiAddToWishlist({
                          customerAccountId: require.mozuData('user').accountId,
                          quantity: finalProduct.get("quantity")
                      });
                  };
                });

                api.steps(addSteps).then(function(response){     
                   api.request('GET', '/api/commerce/wishlists/customers/'+user.accountId+'/my_wishlist', {} ).then(function(response) {
                      var items = response.items;
                      var soldOnlineArray=[];
                      var onSaleArray=[];
               
                     _.each(items, function(item){
                       var isEcommItem =  _.find(item.product.properties, function (obj) { return obj.attributeFQN == Hypr.getThemeSetting('soldItemAttr'); });
                           if(isEcommItem && isEcommItem.values[0].value) {
                             soldOnlineArray.push(item);
                           }
                     });
                             
                    _.each(items, function(item){
                       if(item.product.price.salePrice) {
                         var price = item.product.price.price;
                         var salePrice = item.product.price.salePrice;
                         if(price > salePrice) {
                           onSaleArray.push(item);
                         }
                       }                     
                    });
              
                    var wishlistModel = new Wishlist(response);
                      if (storeEmail !== "" && storeEmail !== undefined ) {
                        wishlistModel.set("isStoreEmail", true);
                      } else {
                        wishlistModel.set("isStoreEmail", false);
                      }
                      $("button.printorder-email").removeAttr("disabled");
                      $("button.printorder-email").removeClass("disabled");
                    if(homeFurnitureSiteId === contextSiteId){
                        wishlistModel.set("isHomeFurnitureSite", true);
                    }
                     wishlistModel.set('soldOnlineCount', soldOnlineArray.length);
                     wishlistModel.set('onSaleCount', onSaleArray.length);
                     wishlistModel.set('isOnSale', false);
                     wishlistModel.set('isSoldOnline', false);
                     wishlistModel.set('sortByValue', "most recent");
                      var firstItemQuantity = items[0].quantity;
                       _.each(items, function(item, index) {
                          if(index > 0) {
                            firstItemQuantity += item.quantity;
                          }
                       }); 
                      wishlistModel.set('totalItemsQuantity', firstItemQuantity);
                     self.model = wishlistModel; 
                     $('#wishlist-count').html('('+firstItemQuantity+')');
                     self.render();
                     $(".empty-wishlist-container").addClass('hidden');
                     $('.content-loading').hide();
                   });   
                }, function(errorResponse) {
                    //handle error response
                });
                
            });
        },
        getFinalProducts: function(wishlistData, productsToAdd){
          var finalProducts=[],
              self = this,
              price;
          _.each(wishlistData,function(wishlistItem) {
            _.each(productsToAdd, function(product){
              if(wishlistItem.attributes.product.attributes.productCode == product.productCode){
                configPromises.push(function() {
                  return $.Deferred(function(dfd) {
                    var productModel = new ProductModels.Product(product);
                    if (wishlistItem.product && wishlistItem.product.variationProductCode) {
                      _.each(wishlistItem.product.options, function(currentOption) {
                            self.configureProduct(currentOption, productModel);
                        });
                        productModel.on("productConfigured", function(){
                          finalProducts.push(productModel);
                          self.saveNewEHFValueToProduct(productModel, dfd, finalProducts);
                        });
                    }else{
                      finalProducts.push(productModel);
                      self.saveNewEHFValueToProduct(productModel, dfd, finalProducts);
                    }
                  }).promise();
                });
              }
             });
          });
        },
        configureProduct: function($optionEl, currentModel){
          var newValue = $optionEl.value,
                oldValue,
                id = $optionEl.attributeFQN,
                isPicked = true,
                option = currentModel.get('options').findWhere({
                    'attributeFQN': id
                });
            if (option) {
                if (option.get('attributeDetail').inputType === "YesNo") {
                    option.set("value", isPicked);
                } else if (isPicked) {
                    oldValue = option.get('values');
                    if (oldValue !== newValue && !(oldValue === undefined && newValue === '')) {
                        option.set('value', newValue);
                    }
                }
            }
        },
        saveNewEHFValueToProduct: function(productModel, dfd, finalProducts){
          var productCode, preferredStore = $.parseJSON(localStorage.getItem('preferredStore')),
        	    relatedEHF;
        	if(productModel.get('variationProductCode')) {
        		productCode = productModel.get('variationProductCode');
        	}else {
        		productCode = productModel.get('productCode');
          }
          if(entityListItems && entityListItems.items.length > 0) {
        	  relatedEHF = _.findWhere(entityListItems.items, {'productCode': productCode, 'province': preferredStore.address.stateOrProvince});
          }
        	var ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
            var ehfOption = productModel.get('options').findWhere({
                'attributeFQN': ehfAttributeFQN
            });
            
            if(ehfOption && ehfOption.get('shopperEnteredValue')) {
            	ehfOption.set('shopperEnteredValue', ''); //remove previous EHF
            	ehfOption.set('value', '');
            }
            if(relatedEHF && ehfOption) {
	      		  ehfOption.set('shopperEnteredValue', relatedEHF.feeAmt);
              ehfOption.set('value', relatedEHF.feeAmt);
              dfd.resolve(finalProducts);
            }else{
              dfd.resolve(finalProducts);
            }
        },
        resolvePromises: function(promises){
            var deferred = $.Deferred();
            var fulfilled = 0, length = promises.length;
            var results = [];

            if (length === 0) {
                deferred.resolve(results);
            } else {
                promises.forEach(function(promise, i){
                    $.when(promise()).then(function(value) {
                        results = value;
                        fulfilled++;
                        if(fulfilled === length){
                            deferred.resolve(results);
                        }
                    });
                });
            }

            return deferred.promise();
        },
        addToCartForPickup: function (locationCode, locationName, quantity, id, e) {
          var me = this;
          	var productObject = this.model.get('items').get(id).get('product');
            productObject.addToCartForPickup(locationCode, locationName, quantity);
                productObject.on('addedtocart', function (cartitem) {
                  CartMonitor.update();
                  //add related EHF product to cart
                  var productCode, preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
	              if(cartitem.data.product.variationProductCode) {
	              	productCode = cartitem.data.product.variationProductCode;
	              }else {
	              	productCode = cartitem.data.product.productCode;
	              }
	              var relatedEHF = _.findWhere(entityListItems.items, {'productCode': productCode, 'province': preferredStore.address.stateOrProvince});
                  var ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
                  var ehfOption = _.findWhere(cartitem.data.product.options, {'attributeFQN': ehfAttributeFQN});
                  if(relatedEHF && ehfOption) {
                  	var ehfProductModel = new ProductModels.Product({
                          productCode: 'EHF101'
                      });
                      ehfProductModel.fetch().then(function() {
                      	var ehfProductQuantity = ((ehfOption.shopperEnteredValue * quantity).toFixed(2))  / ehfProductModel.get('price.price'); //toFixed(2) to limit number to 2 after decimal point
                      	var preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
                      	var actualProductCodeFQN = Hypr.getThemeSetting('actualProductCode');
                      	var actualProductCodeOption = ehfProductModel.get('options').findWhere({'attributeFQN': actualProductCodeFQN});
                      	if(actualProductCodeOption) {
                      		actualProductCodeOption.set('shopperEnteredValue', productObject.get('productCode'));
                      	}
                      	ehfProductModel.apiAddToCartForPickup({
                      			fulfillmentLocationCode: preferredStore.code,
                                  fulfillmentMethod: ProductModels.Product.Constants.FulfillmentMethods.PICKUP,
                                  fulfillmentLocationName: preferredStore.name,
                                  quantity: ehfProductQuantity
                      	}).then(function(response) { 
                      		me.editing.added = id;
                            me.render();
                      	});
                      });
                  }else {
                	  me.editing.added = id;
                      me.render();
                  }
                });
        },
        doNotRemove: function() {
            this.editing.added = false;
            this.editing.remove = false;
            this.render();
        },
        beginRemoveItem: function(e) {
            var self = this;
            var id = $(e.currentTarget).data('mzItemId');
            if (id) {
                this.editing.remove = id;
                this.render();
            }
        },
        finishRemoveItem: function(e) {
            var self = this;
            var id = $(e.currentTarget).data('mzItemId');
            if (id) {
                var removeWishId = id;
                var itemToRemove = _.findWhere(self.model.get('items').models, {id: removeWishId});
                return this.model.apiDeleteItem(id).then(function() {
                    self.editing.remove = false;
                    
                    if (itemToRemove) {
                        self.model.get('items').remove(itemToRemove);
                        var soldOnlineProducts = self.getSoldOnlineProducts(self.model.get('items'));
                var onSaleProducts = self.getOnSaleProducts( self.model.get('items'));
                self.model.set('soldOnlineCount', soldOnlineProducts.length);
                self.model.set('onSaleCount', onSaleProducts.length);
                var removedWishlistProduct = {'productName': itemToRemove.get('product').get('name'), 'productUrl': itemToRemove.get('product').get('url')};
                self.model.set('removedWishlistProduct', removedWishlistProduct);

                var items = self.model.get('items').models;
                      var totalQuantity = 0;
                      _.each(items, function(item, index) {
                        totalQuantity += item.get('quantity');
                      });                       
                      $('#wishlist-count').html('('+totalQuantity+')');
                        self.render();
                        if(self.model.get('items').length === 0)
                        {
                          $(".empty-wishlist-container").removeClass('hidden');
                        }
                        else{
                          $(".empty-wishlist-container").addClass('hidden');
                          $('.content-loading').hide();
                        } 
                    }
                });
            }
        },
        removeWishlistAllItems: function(e) {
            var self = this;
            var removeItem = self.model.get('id');
            api.request('DELETE', '/api/commerce/wishlists/'+ removeItem +'/items', {} ).then(function() {
            	self.model.set('items', []);
            	self.model.set('soldOnlineCount', 0);
            	self.model.set('onSaleCount', 0);
            	self.model.set('isOnSale', false);
            	self.model.set('isSoldOnline', false);
            	self.model.set('sortByValue', 'most recent');
            	$('#wishlist-count').html('(0)');
            	$(".empty-wishlist-container").removeClass('hidden'); 
            	self.render();
                
		    });
        },
        updateQuantity: function(e){
          var me = this;
          if(isNaN($(e.currentTarget).val()) || $(e.currentTarget).val() < 0) {
      		e.preventDefault();
      		$(e.currentTarget).val(1);
      		$(".qty-decrement").addClass("is-disabled");
          }
          else {
        	  var field = $(e.currentTarget).attr('data-mz-value'); //this field is used to increment/decrement the quantity
              
              var quantity = $(e.currentTarget).parent().siblings('.quantity-input-field').val();
    	      if(field === 'qty-decrement'){
    	        $(e.currentTarget).parent().siblings('.quantity-input-field').val(parseInt(quantity, 10) - 1);
    	      }
    	          
    	      if(field === 'qty-increment'){
    	        $(e.currentTarget).parent().siblings('.quantity-input-field').val(parseInt(quantity, 10) + 1);
    	      } 
    	      var $qField = "";
	          if(field != 'quantity'){
	            $qField = $(e.currentTarget).parent().siblings('.quantity-input-field');
	          }else {
	            $qField = $(e.currentTarget);
	          }
	          var newQuantity = parseInt($qField.val(), 10),
	             id = $qField.data('mz-cart-item'),
	              item = this.model.get("items").get(id);
	          if(newQuantity <= 1 || isNaN(newQuantity)){
	          	$qField.val(1);
	          	newQuantity = 1;
	          	$(".qty-decrement").addClass("is-disabled");
	          }else {
	          	$(".qty-decrement").removeClass("is-disabled");
	          }
	          if (item && !isNaN(newQuantity)) {
	              item.set('quantity', newQuantity);
	              me.render();               
	          }
          }
        },
        sortAndFilter: function(e){
          e.preventDefault();
          $(document).find('.filters').addClass('open');
        },
        applyFacets: function(e){
          e.preventDefault();
          $(document).find('.filters').removeClass('open');
        },
        sortBy: function(e) {
          var me = this;
          var sortbyValue = $(e.currentTarget).data('sortvalue');
          var sortedData = me.sortWishlist(sortbyValue, me.model.get('items').models);
                 
          me.model.set('items', sortedData);
          me.model.set('sortByValue', sortbyValue);
          me.render();

        },
        sortWishlist: function(sortbyValue, items) {
          var sortedDataWishlist;
          if (sortbyValue == "productName asc") {
            sortedDataWishlist = _.sortBy(items, function(item){
              return item.get('product').get('name');
        });           
          } else if (sortbyValue == "productName desc") {
            sortedDataWishlist = _.sortBy(items, function(item){
          return item.get('product').get('name');
        }).reverse();
          } else if (sortbyValue == "price asc") {
            sortedDataWishlist = _.sortBy(items, function(item){
              return item.get('product').get('price').get('price');
        });  
          } else if (sortbyValue == "price desc") {
            sortedDataWishlist = _.sortBy(items, function(item){
              return item.get('product').get('price').get('price');
        }).reverse();
          } else if (sortbyValue == "most recent") {
            sortedDataWishlist = _.sortBy(items, function(item){
          return item.get('auditInfo').updateDate;
        });
          }
          return sortedDataWishlist;
        },
        wFilter: function(e) {
          var me = this;
          var items = me.model.get('items');
          if($(e.currentTarget).data('action-name') == "soldOnline") {            
          if($(e.currentTarget).is(':checked')) {                    
             var soldOnlineProducts = me.getSoldOnlineProducts(items);
             var onSaleProducts = me.getOnSaleProducts(soldOnlineProducts);
             
             me.model.set('items', soldOnlineProducts);
             me.model.set('soldOnlineCount', soldOnlineProducts.length);
             me.model.set('onSaleCount', onSaleProducts.length);
             me.model.set('isSoldOnline', true);
             //me.model.set('sortByValue', me.model.attributes.sortByValue);
              if(me.model.attributes.isOnSale) {
                me.model.set('isOnSale', true); 
                     } else {
                      me.model.set('isOnSale', false);
                     }  
             
              me.render();
                     
              } else {
                if(me.model.attributes.isOnSale) {
                      api.request('GET', '/api/commerce/wishlists/customers/'+user.accountId+'/my_wishlist', {} ).then(function(response) {
                        var wishlistModel = new Wishlist(response);
                        var onSaleProducts = me.getOnSaleProducts(wishlistModel.get('items'));
                        var soldOnlineProducts = me.getSoldOnlineProducts(onSaleProducts);
                        
                        wishlistModel.set('items', onSaleProducts);
                        if(homeFurnitureSiteId === contextSiteId){
                            wishlistModel.set("isHomeFurnitureSite", true);
                        }
                          wishlistModel.set('soldOnlineCount', soldOnlineProducts.length);
                          wishlistModel.set('onSaleCount', onSaleProducts.length);
                          wishlistModel.set('isOnSale', true);
                          wishlistModel.set('isSoldOnline', false);
                          wishlistModel.set('sortByValue', me.model.get('sortByValue'));
                          var setWishlistView = window.wishlistView = new WishlistView({
                            el: $('#mzWishlist'),
                            model:  wishlistModel
                        });
                          setWishlistView.render();
                      });
                  
                      
                     } else {
                      me.getMainWishlist();
                     }
                
              }
            
          } else if ($(e.currentTarget).data('action-name') == "onSale") {
            if($(e.currentTarget).is(':checked')) { 
               var onSaleProducts1 = me.getOnSaleProducts(items);
               var soldOnlineProducts1 = me.getSoldOnlineProducts(onSaleProducts1);
             
             if(homeFurnitureSiteId === contextSiteId){
                 me.model.set("isHomeFurnitureSite", true);
              }  
             me.model.set('items', onSaleProducts1);
             me.model.set('soldOnlineCount', soldOnlineProducts1.length);
             me.model.set('onSaleCount', onSaleProducts1.length);
             me.model.set('isOnSale', true);
             me.model.set('sortByValue', me.model.get('sortByValue'));
              if(me.model.attributes.isSoldOnline) {
                me.model.set('isSoldOnline', true); 
                     } else {
                      me.model.set('isSoldOnline', false);                   }  
             
              me.render();
            } else {
              if(me.model.attributes.isSoldOnline) {
                api.request('GET', '/api/commerce/wishlists/customers/'+user.accountId+'/my_wishlist', {} ).then(function(response) {
                        var wishlistModel = new Wishlist(response);
                        var soldOnlineProducts = me.getSoldOnlineProducts(wishlistModel.get('items'));
                        var onSaleProducts = me.getOnSaleProducts(soldOnlineProducts);
                        
                        
                        wishlistModel.set('items', soldOnlineProducts);
                        if(homeFurnitureSiteId === contextSiteId){
                            wishlistModel.set("isHomeFurnitureSite", true);
                        }
                          wishlistModel.set('soldOnlineCount', soldOnlineProducts.length);
                          wishlistModel.set('onSaleCount', onSaleProducts.length);
                          wishlistModel.set('isOnSale', false);
                          wishlistModel.set('isSoldOnline', true);
                          wishlistModel.set('sortByValue', me.model.get('sortByValue'));
                          var setWishlistView = window.wishlistView = new WishlistView({
                            el: $('#mzWishlist'),
                            model:  wishlistModel
                        });
                          setWishlistView.render();
                      });
                
                  
                     } else {
                      me.getMainWishlist();
                     }
            }
          }
          
        },
        getSoldOnlineProducts: function(items) {
	    	if (items.models) {
	          return items.models.filter(function(item) { 
	            var ecommItem = _.findWhere(item.get('product').get('properties'), { 'attributeFQN': Hypr.getThemeSetting('soldItemAttr') });
	            if(ecommItem){
	            	return ecommItem.values[0].stringValue === Hypr.getThemeSetting('ecommItemValue');
	            }
	          });
	    	} else {
	    		return items.filter(function(item) { 
		            var ecommItem = _.findWhere(item.get('product').get('properties'), { 'attributeFQN': Hypr.getThemeSetting('soldItemAttr') });
		            if(ecommItem){
		            	return ecommItem.values[0].stringValue === Hypr.getThemeSetting('ecommItemValue');
		            }
	    		});
	    	}          
        },
        getOnSaleProducts: function(items) {
            if (items.models) {
              return items.models.filter(function(item) { 
                return item.get('product').get('price').get('price') > item.get('product').get('price').get('salePrice');
              });
            } else {
               return items.filter(function(item) { 
                return item.get('product').get('price').get('price') > item.get('product').get('price').get('salePrice');
              });
            }
        },
        getMainWishlist: function() {
          var me = this;
          api.request('GET', '/api/commerce/wishlists/customers/'+user.accountId+'/my_wishlist', {} ).then(function(response) {
            var wishlistModel = new Wishlist(response);
            var soldOnlineProducts = me.getSoldOnlineProducts(wishlistModel.get('items'));
        var onSaleProducts = me.getOnSaleProducts(wishlistModel.get('items'));
              
              if(homeFurnitureSiteId === contextSiteId){
                  wishlistModel.set("isHomeFurnitureSite", true);
              }
              if (storeEmail !== "" && storeEmail !== undefined ) {
                wishlistModel.set("isStoreEmail", true);
              } else {
                wishlistModel.set("isStoreEmail", false);
              }
              wishlistModel.set('soldOnlineCount', soldOnlineProducts.length);
              wishlistModel.set('onSaleCount', onSaleProducts.length);
              wishlistModel.set('isOnSale', false);
              wishlistModel.set('isSoldOnline', false);
              wishlistModel.set('sortByValue', me.model.get('sortByValue'));
              var setWishlistView = window.wishlistView = new WishlistView({
                el: $('#mzWishlist'),
                model:  wishlistModel
            });
              setWishlistView.render();
          });
          
                  
        },
        renderWishlistView: function(updatedWishlist) {
          window.setWishlistView.model.clear();
          window.setWishlistView.model = updatedWishlist;
          window.setWishlistView.render();
        },
        render: function() {
          var me = this;
          var wishListItems = [];
          if(me.model.get('items')) {
       	   wishListItems = me.model.get('items').models.length > 0 ? me.model.get('items').models : [];
          }
          if(wishListItems.length > 0){
              var wishlistTotal = 0, ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
               _.each(wishListItems, function(item, index) {
                  if(item.get('product').get('price').get('salePrice')) {
                	  wishlistTotal += item.get('product').get('price').get('salePrice') * item.get('quantity');
                  }else {
                	  wishlistTotal += item.get('product').get('price').get('price') * item.get('quantity');
                  }
                  if(item.get('product').get('options').length > 0) {
                	  var ehfOption = item.attributes.product.attributes.options.findWhere({'attributeFQN': ehfAttributeFQN});
                	  if(ehfOption && ehfOption.get('shopperEnteredValue')) {
                		  wishlistTotal += ehfOption.get('shopperEnteredValue')  * item.get('quantity');
                	  }
                  } 
               });
               if(!me.model.get('isPromoApplied')) {
               		var aeroplanMiles;
               		aeroplanMiles = Math.floor(wishlistTotal/2);
               		me.model.set('aeroplanMiles', aeroplanMiles);
               }
          }
          $('input[data-mz-click="wFilter"]').off();
	        var frSiteId = Hypr.getThemeSetting("frSiteId");
	        if(frSiteId === contextSiteId){
	            me.model.set("isFrenchSite", true);
	        }
            Backbone.MozuView.prototype.render.apply(this, arguments);
            
            var sortByText = $(document).find('[data-sortvalue="'+ me.model.get('sortByValue') +'"]').find('a').text();
            $('#selected-sort').text(sortByText);
            
            $('input[data-mz-click="wFilter"]').on('click', function(e) {
              me.wFilter(e);
            });
            $("#emailto-store-modal").on("click", function () {
              $('#wishlisttostore-modal-container').modal().show();
              $('#wishlisttostore-modal-container').addClass("in");
              $('#wishlisttostore-modal-container').css('display','block');
            });
            $("#wishlisttostore-modal-container").on("hidden.bs.modal",function(){
                $('#storeSenderName').val("");
                $("#storeSenderName").removeClass('is-invalid');
                $('[data-mz-validationmessage-for="storeSenderName"]').text('');
                $('#senderEmail').val("");
                $("#senderEmail").removeClass('is-invalid');
                $('[data-mz-validationmessage-for="senderEmail"]').text('');
                $('#storeMessage').val("");
                $('#storeSubject').val("");
                $('.sucess-email').hide();
                $('.error-email').hide();
                $('.wishlist-store-checkbox').prop('checked', true);
            });
            $("#send-wishlisttostore-email").on("click", function (e) {
                var errors = false,
                scrollid1 = false,
                data = [],
                emailReg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                numbersReg = /^[0-9]+$/,
                wrongEmailFlag = false,
                Items= [],
                saleprice,
                language,
                locale = require.mozuData('apicontext').headers['x-vol-locale'],
                currentSite = require.mozuData('apicontext').headers['x-vol-site'],
                currentLocale = '',
                productCode = [],
                storeSubject = Hypr.getLabel('defaultWishlistSubjectLine'),
                storeMessage;
                locale = locale.split('-')[0];
                var senderEmails = $("#senderEmail").val().replace(/\s/g,'').split(",");
                var recepientEmails = storeEmail.replace(";",',');
                if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
                    currentLocale = locale === 'fr' ? '/fr' : '/en';
                }
                if (currentLocale === "/fr") {
                    language = "french";
                } else if (currentLocale === "/en") {
                    language = "english";
                } else {
                    language = "furnture";
                }
                if (!$("#storeSenderName").val() || $("#storeSenderName").val() === ""){
                    $("#storeSenderName").addClass('is-invalid');
                    $('[data-mz-validationmessage-for="storeSenderName"]').text(Hypr.getLabel('fnameMissing'));
                    scrollid1 = true;
                    errors = true;
                }else if(numbersReg.test($("#storeSenderName").val())){
                    $("#storeSenderName").addClass('is-invalid');
                    $('[data-mz-validationmessage-for="storeSenderName"]').text(Hypr.getLabel('invalidName'));
                    scrollid1 = true;
                    errors = true;
                }else{
                    $("#storeSenderName").removeClass('is-invalid');
                    $('[data-mz-validationmessage-for="storeSenderName"]').text('');
                }
                if(!$("#senderEmail").val() || $("#senderEmail").val() === ""){
                  $("#senderEmail").addClass('is-invalid');
                  $('[data-mz-validationmessage-for="senderEmail"]').text(Hypr.getLabel('emailMissing'));
                  scrollid1 = true;
                  errors = true;                            
                }else if($("#senderEmail").val() !== ""){
                  for (var i = 0; i < senderEmails.length; i++) {
                    if( senderEmails[i] === "" || ! emailReg.test(senderEmails[i])){
                      $("#senderEmail").addClass('is-invalid');
                      $('[data-mz-validationmessage-for="senderEmail"]').text(Hypr.getLabel('emailMissing'));
                      scrollid1 = true;
                      errors = true;
                      wrongEmailFlag = true;
                    } else if(!wrongEmailFlag) {
                      $("#senderEmail").removeClass('is-invalid');
                      $('[data-mz-validationmessage-for="senderEmail"]').text('');
                    }
                  }
                }else{
                   $("#senderEmail").removeClass('is-invalid');
                   $('[data-mz-validationmessage-for="senderEmail"]').text('');
                }
                if (!$("#storeMessage").val() || $("#storeMessage").val() === ""){
                  storeMessage = "";
                }else{
                  storeMessage = $("#storeMessage").val();
                }
                var checkedBoxes = $('.wishlist-store-checkbox:checked');
                if(checkedBoxes.length <= 0){
                    $(".wishlist-chk-error").show().delay(3000).fadeOut();
                    errors = true;
                } else {
                    _.each($('.wishlist-store-checkbox:checked'), function(checkbox) {
                        productCode.push(checkbox.dataset.mzProductcode);
                    });
                    $('.wishlist-chk-error').addClass("hide");
                } 
                if(!errors) {
                    if(me.model.get('items')) {
                        if(wishListItems.length > 0){
                            _.each(wishListItems, function(item, index) {
                                var brandNameAttr = _.findWhere(item.attributes.product.attributes.properties, {'attributeFQN': Hypr.getThemeSetting('brandDesc')});
                                    var brandName;
                                    var displayItem = _.findWhere(item.attributes.product.attributes.properties, {'attributeFQN': Hypr.getThemeSetting('displayItem')});
                                    if (item.get('product').get('price').get('salePrice') === undefined) {
                                    saleprice = 0.0;
                                } else {
                                    saleprice = item.get('product').get('price').get('salePrice');
                                }
                                _.each(productCode, function(productcode){
                                    if (productcode == item.get('product').get('productCode')) {
                                        if (brandNameAttr) {
                                            brandName = brandNameAttr.values[0].value;
                                        } else {
                                            brandName = "";
                                        }
                                        Items.push({productName: item.get('product').get('name'), 
                                        productImageUrl: "https:"+item.get('product').get('imageUrl'),
                                        brandName: brandName,
                                        productCode: item.get('product').get('productCode'),
                                        itemDisplay: displayItem.values[0].value,
                                        price: item.get('product').get('price').get('price'),
                                        salePrice: saleprice});
                                    }
                                });
                                data = {
                                    senderName:$("#storeSenderName").val(),
                                    senderEmail:$("#senderEmail").val(),
                                    recipientEmail:recepientEmails,
                                    subject: storeSubject,
                                    message: storeMessage,
                                    language: language,
                                    items: Items
                                };
                            });
                        $.ajax({
                            method: 'POST',
                                contentType: 'application/json; charset=utf-8',
                                url: Hypr.getThemeSetting('formPostingUrl') + 'customWishList',
                                data: JSON.stringify(data),    
                                beforeSend: function(){
                                    // Show image container
                                    $("#wishlist-store-loading").show();
                                },
                                success:function(response){
                                    $('.sucess-email').show();
                                    $("#wishlist-store-loading").hide();
                                    $('#storeSenderName').val("");
                                    $("#storeSenderName").removeClass('is-invalid');
                                    $('[data-mz-validationmessage-for="storeSenderName"]').text('');
                                    $('#senderEmail').val("");
                                    $("#senderEmail").removeClass('is-invalid');
                                    $('[data-mz-validationmessage-for="senderEmail"]').text('');
                                    $('#storeMessage').val("");
                                    $('#storeSubject').val("");
                                    $('.wishlist-store-checkbox').prop('checked', true);
                                },
                                error: function (error) {
                                    $('.error-email').show();
                                    $("#wishlist-store-loading").hide();
                                }
                            });
                        }
                    }
                } else {
                    if (scrollid1) {
                        $('#wishlisttostore-modal-container').animate({ scrollTop: 0 }, 'slow');
                    }
                }
            });
          $("#print-wishlist-modal").on("click", function () {
              $('#wishlist-email-modal-container').modal().show();
          });
          $(".close1").on("click", function () {
            $('#error').toggle();
  
        });

        $(".closemodal").on("click", function () {
          $('#errormodal').toggle();

      });
          $("#wishlist-email-modal-container, #wishlisttostore-modal-container").modal({
            show:false,
            backdrop:'static'
          });
          $("#wishlist-email-modal-container").on("hidden.bs.modal",function(){
            $('#senderName').val("");
            $("#senderName").removeClass('is-invalid');
            $('[data-mz-validationmessage-for="senderName"]').text('');
            $('#recepientEmail').val("");
            $("#recepientEmail").removeClass('is-invalid');
            $('[data-mz-validationmessage-for="recepientEmail"]').text('');
            $('#message').val("");
            $('#subject').val("");
            $('.sucess-email').hide();
            $('.error-email').hide();
            $('.wishlist-checkbox').prop('checked', true);
          });
          $("#send-wishlist-email").on("click", function (e) {
            var errors = false,
            scrollid1 = false,
            data = [],
            emailReg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            numbersReg = /^[0-9]+$/,
            wrongEmailFlag = false,
            Items= [],
            saleprice,
            language,
            locale = require.mozuData('apicontext').headers['x-vol-locale'],
            currentSite = require.mozuData('apicontext').headers['x-vol-site'],
            currentLocale = '',
            productCode = [],
            subject = Hypr.getLabel('defaultWishlistSubjectLine'),
            message;
            locale = locale.split('-')[0];
            var emails = $("#recepientEmail").val().replace(/\s/g,'').split(",");
            if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
              currentLocale = locale === 'fr' ? '/fr' : '/en';
            }
            if (currentLocale === "/fr") {
              language = "french";
            } else if (currentLocale === "/en") {
              language = "english";
            } else {
              language = "furnture";
            }
            if (!$("#senderName").val() || $("#senderName").val() === ""){
              $("#senderName").addClass('is-invalid');
              $('[data-mz-validationmessage-for="senderName"]').text(Hypr.getLabel('fnameMissing'));
              scrollid1 = true;
              errors = true;
            }else if(numbersReg.test($("#senderName").val())){
              $("#senderName").addClass('is-invalid');
              $('[data-mz-validationmessage-for="senderName"]').text(Hypr.getLabel('invalidName'));
              scrollid1 = true;
              errors = true;
            }else{
              $("#senderName").removeClass('is-invalid');
              $('[data-mz-validationmessage-for="senderName"]').text('');
            }
            if(!$("#recepientEmail").val() || $("#recepientEmail").val() === ""){
              $("#recepientEmail").addClass('is-invalid');
              $('[data-mz-validationmessage-for="recepientEmail"]').text(Hypr.getLabel('emailMissing'));
              scrollid1 = true;
              errors = true;                            
            }else if($("#recepientEmail").val() !== ""){
              for (var i = 0; i < emails.length; i++) {
                if( emails[i] === "" || ! emailReg.test(emails[i])){
                  $("#recepientEmail").addClass('is-invalid');
                  $('[data-mz-validationmessage-for="recepientEmail"]').text(Hypr.getLabel('emailMissing'));
                  scrollid1 = true;
                  errors = true;
                  wrongEmailFlag = true;
                } else if(!wrongEmailFlag) {
                  $("#recepientEmail").removeClass('is-invalid');
                  $('[data-mz-validationmessage-for="recepientEmail"]').text('');
                }
              }
            }else{
               $("#recepientEmail").removeClass('is-invalid');
               $('[data-mz-validationmessage-for="recepientEmail"]').text('');
            }
            if (!$("#message").val() || $("#message").val() === ""){
              message = "";
            }else{
              message = $("#message").val();
            }
            var checkedBoxes = $('.wishlist-checkbox:checked');
            if(checkedBoxes.length <= 0){
              $(".wishlist-chk-error").show().delay(3000).fadeOut();
              errors = true;
            } else {
              _.each($('.wishlist-checkbox:checked'), function(checkbox) {
                productCode.push(checkbox.dataset.mzProductcode);
              });
              $('.wishlist-chk-error').addClass("hide");
            }
            if(!errors) {
              if(me.model.get('items')) {
                if(wishListItems.length > 0){
                  _.each(wishListItems, function(item, index) {
                    var brandNameAttr = _.findWhere(item.attributes.product.attributes.properties, {'attributeFQN': Hypr.getThemeSetting('brandDesc')});
                    var brandName;
                    var displayItem = _.findWhere(item.attributes.product.attributes.properties, {'attributeFQN': Hypr.getThemeSetting('displayItem')});
                    if (item.get('product').get('price').get('salePrice') === undefined) {
                      saleprice = 0.0;
                    } else {
                      saleprice = item.get('product').get('price').get('salePrice');
                    }
                     _.each(productCode, function(productcode){
                      if (productcode == item.get('product').get('productCode')) {
                        if (brandNameAttr) {
                          brandName = brandNameAttr.values[0].value;
                        } else {
                          brandName = "";
                        }
                        Items.push({productName: item.get('product').get('name'), 
                          productImageUrl: "https:"+item.get('product').get('imageUrl'),
                          brandName: brandName,
                          productCode: item.get('product').get('productCode'),
                          itemDisplay: displayItem.values[0].value,
                          price: item.get('product').get('price').get('price'),
                          salePrice: saleprice});
                      }
                    });
                    data = {
                       senderName:$("#senderName").val(),
                       recipientEmail:$("#recepientEmail").val(),
                       subject: subject,
                       message:message,
                       language: language,
                       items: Items
                    };
                  });
                  $.ajax({
                      method: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        url: Hypr.getThemeSetting('formPostingUrl') + 'customWishList',
                        data: JSON.stringify(data),    
                        beforeSend: function(){
                          // Show image container
                          $("#wishlist-loading").show();
                        },
                        success:function(response){
                          $('.sucess-email').show();
                          $("#wishlist-loading").hide();
                          $('#senderName').val("");
                          $("#senderName").removeClass('is-invalid');
                          $('[data-mz-validationmessage-for="senderName"]').text('');
                          $('#recepientEmail').val("");
                          $("#recepientEmail").removeClass('is-invalid');
                          $('[data-mz-validationmessage-for="recepientEmail"]').text('');
                          $('#message').val("");
                          $('#subject').val("");
                          $('.wishlist-checkbox').prop('checked', true);
                        },
                        error: function (error) {
                          $('.error-email').show();
                          $("#wishlist-loading").hide();
                        }
                  });
                }
              }
            } else {
              if (scrollid1) {
                $('#wishlist-email-modal-container').animate({ scrollTop: 0 }, 'slow');
              }
            } 
          });
        }
  });
  var WishlistItem = Backbone.MozuModel.extend({
        relations: {
            product: ProductModels.Product
        }
    });
  
  var Wishlist = Backbone.MozuModel.extend({
        mozuType: 'wishlist',
        helpers: ['hasItems'],
        hasItems: function() {
            return this.get('items').length > 0;
        },
        relations: {
            items: Backbone.Collection.extend({
                model: WishlistItem
            })
        }        
    });

  if (!(user.isAnonymous) && user.isAuthenticated) {
	  //Email client check//
      $('a[href^=mailto]').each(function() {
  	    var href = $(this).attr('href');
  	    $(this).click(function() {
  	      var text;
  	      var self = $(this);
  	    $("#emailMessage").removeClass('hidden');
  	      $(window).blur(function() {
  	        clearTimeout(text);
  	      });

  	      text = setTimeout(function() {
  	    	  $("#emailMessage").addClass('hidden');
  	      }, 5000);
  	      
  	    });
  	  });
//
    api.request('GET', '/api/commerce/wishlists/', {} ).then(function(response) {
        if(response.items.length > 0 && response.items[0].items.length > 0){
          var items = response.items[0].items;
              var soldOnlineArray=[];
              var onSaleArray=[];
                   
           _.each(items, function(item){
             var isEcommItem =  _.find(item.product.properties, function (obj) { return obj.attributeFQN == Hypr.getThemeSetting('soldItemAttr'); });
                 if(isEcommItem && isEcommItem.values[0].stringValue === Hypr.getThemeSetting('ecommItemValue')) {
                   soldOnlineArray.push(item);
                 }
           });
                   
          _.each(items, function(item){
             if(item.product.price.salePrice) {
               var price = item.product.price.price;
               var salePrice = item.product.price.salePrice;
               if(price > salePrice) {
                 onSaleArray.push(item);
               }
             }                     
          });
              
          var wishlistModel = new Wishlist(response.items[0]);
          if(homeFurnitureSiteId === contextSiteId){
              wishlistModel.set("isHomeFurnitureSite", true);
          }
          wishlistModel.set('soldOnlineCount', soldOnlineArray.length);
          wishlistModel.set('onSaleCount', onSaleArray.length);
          wishlistModel.set('isOnSale', false);
          wishlistModel.set('isSoldOnline', false);
          wishlistModel.set('sortByValue', "most recent");
          var setWishlistView = window.wishlistView = new WishlistView({
            el: $('#mzWishlist'),
            model:  wishlistModel
          });
          setWishlistView.render();
         
          $(".empty-wishlist-container").addClass('hidden'); 
          $('.content-loading').hide();
        }else{
          var emptyWishlist = new Wishlist();
          var emptyWishlistView = new WishlistView({
            el: $('#mzWishlist'),
            model:  emptyWishlist
          });       
          emptyWishlistView.render();   
          $(".empty-wishlist-container").removeClass('hidden');
        }
    },
    function(){
        $(".empty-wishlist-container").removeClass('hidden'); 
    });
  } else {
	window.location = Hypr.getLabel('loginShoppingListURL');
  }
 });
