define(['modules/jquery-mozu',
	"modules/backbone-mozu",
	'underscore',
	"modules/views-collections",
	"hyprlivecontext",
	"modules/api",
	'hyprlive',
	'vendor/timezonesupport/timezonesupport',
	'modules/product-quick-view'], function($, Backbone, _, CollectionViewFactory, HyprLiveContext, api, Hypr, timeZoneSupport, quickViewBind) {
	var locale = require.mozuData('apicontext').headers['x-vol-locale'];
	var searchQuery = HyprLiveContext.locals.pageContext.search.query;
	var articleProductTypeId = Hypr.getThemeSetting('articleProductTypeId');
	var productTypeId = Hypr.getThemeSetting('productTypeId');
    var apiContext = require.mozuData('apicontext');
    var pageContext = require.mozuData('pagecontext');
    var currentSite = apiContext.headers['x-vol-site'];
    var homeFurnitureSiteId = Hypr.getThemeSetting('homeFurnitureSiteId');
	var searchProductQueryString = 'productTypeId eq ' + productTypeId;
	var searchArticleQueryString = 'productTypeId eq ' + articleProductTypeId;
	var makeurl = "search?query=";
	var defaultPageSize = Hypr.getThemeSetting('defaultPageSize');
	var brandsCategoryId = Hypr.getThemeSetting('brandsCategoryId');
	var globalSearchProductView, globalExpertAdviceView,
	ua = navigator.userAgent.toLowerCase();
	if(locale == "en-US"){
		makeurl = '/en/search?query=';
	} else{
		makeurl = '/fr/search?query=';
	}
	
	if(searchQuery){
		makeurl = makeurl + searchQuery;
	}
	var isPurchesLocation = false;
	if(pageContext.purchaseLocation && pageContext.purchaseLocation.code){
		isPurchesLocation  = true;
	} else {
		isPurchesLocation = false;
	}
	window.makeProductUrl = window.makeArticleUrl = '';
	var defaultSort = Hypr.getThemeSetting('defaultSort'),
	sortByOptions = [
		{
			value: defaultSort,
			text: Hypr.getLabel('default')
		},{
			value: "price asc",
			text: Hypr.getLabel('sortByPriceAsc')
		},{
			value: "price desc",
			text: Hypr.getLabel('sortByPriceDesc')
		},{
			value: "productName asc",
			text: Hypr.getLabel('sortByNameAsc')
		},{
			value: "productName desc",
			text: Hypr.getLabel('sortByNameDesc')
		},{
			value: "createDate desc",
			text: Hypr.getLabel('sortByDateAsc')
		},{
			value: "createDate asc",
			text: Hypr.getLabel('sortByDateDesc')
		},{
			value: "tenant~rating desc",
			text: Hypr.getLabel('sortByRatingDesc')
		},{
			value: "tenant~rating asc",
			text: Hypr.getLabel('sortByRatingAsc')
		}
	];
	var ExpertAdviceView = Backbone.MozuView.extend({
        templateName: 'modules/common/expert-advice-view',
        additionalEvents: {
			'change select[data-mz-value="pageSize"]': 'pageSize'
		},
		pageSize: function(e){
			var me = this;
			var selectedPageSize = $(e.currentTarget).val();
			window.articlePageSize = selectedPageSize;
			if (ua.indexOf('chrome') > -1) {
				if (/edg|edge/i.test(ua)){
					$.cookie('articlePageSize',window.articlePageSize);
				}else{
					document.cookie = "articlePageSize="+window.articlePageSize+";Secure;SameSite=None";
				}
			} else {
				$.cookie('articlePageSize',window.articlePageSize);
			}
			var searchArticleQueryStringTemp = me.makeArticleQueryString();
			me.articlesApiCall(searchArticleQueryStringTemp, 0, window.sortbyValueArticles, window.sortValueArticles, false, true, 1, window.categoryArticlesFilterId, window.articleFacetValueFilter);
		},
        initialize: function() {
        	this.model.set('currentLocale', locale);
			this.model.set('currentSite', currentSite);
        },
        sortArticle: function(e) {        	
			e.preventDefault();
			var me = this;
			window.sortbyValueArticles = $(e.currentTarget).data('value');
			window.sortValueArticles = $(e.currentTarget).text();
        	me.getproductSortByValue();
    		
        },
        applyFacets: function(e){
        	var me = this;
			var selected = $(e.currentTarget).closest('.filters').find('input[name="sortBy"]:checked');
			window.sortbyValueArticles = $(selected).data('value');
			window.sortValueArticles = $(selected).data('sort-value');
			me.getproductSortByValue();
        },
        getproductSortByValue: function() {
        	var me = this;
        	var searchArticleQueryStringTemp = me.makeArticleQueryString();
    		me.articlesApiCall(searchArticleQueryStringTemp, 0, window.sortbyValueArticles, window.sortValueArticles, false, true, 1, window.categoryArticlesFilterId, window.articleFacetValueFilter);
        },
        sortAndFilter: function(e){
        	e.preventDefault();
        	$(e.currentTarget).closest('.tab-pane').find('.filters').addClass('open');
        	moreLessConfig();
        },
        clearFacets:function(){
        	var me = this;
        	$(document).find('.filters').removeClass('open');
        	var searchArticleQueryStringTemp = me.makeArticleQueryString();
        	me.articlesApiCall(searchArticleQueryStringTemp, 0, '', Hypr.getLabel('default'), false, true, 1, window.categoryArticlesFilterId, window.articleFacetValueFilter);
        },
        drillDownSort: function(e) {
        	e.preventDefault();
        	var me = this;
        	window.categoryArticlesFilterId = $(e.currentTarget).data('mz-hierarchy-id');
        	var searchArticleQueryStringTemp = me.makeArticleQueryString();
        	me.articlesApiCall(searchArticleQueryStringTemp, 0, window.sortbyValueArticles, window.sortValueArticles, false, true, 1, window.categoryArticlesFilterId, window.articleFacetValueFilter);
        },
        articleFilterbyFacet: function(e) {
        	var me = this;
        	var facetValueFilter = "";
        	var selected = $(document).find('input[data-mz-action="articleFilterbyFacet"]:checked');
        	selected.each(function(index) {   
            	var facetName = $(selected[index]).data('mz-facet');
            	var facetValue = $(selected[index]).data('mz-facet-value');
            	facetValueFilter = facetValueFilter + facetName + ':' + facetValue + ',';
            });
        	window.articleFacetValueFilter = facetValueFilter;
        	var searchArticleQueryStringTemp = me.makeArticleQueryString();     
        	if($(window).width() > 767) {
        		me.articlesApiCall(searchArticleQueryStringTemp, 0, window.sortbyValueArticles, window.sortValueArticles, false, true, 1, window.categoryArticlesFilterId, window.articleFacetValueFilter);
        	}
        }, 
        allCategories: function() {
        	var me = this;
        	window.categoryArticlesFilterId = Hypr.getThemeSetting('searchableProductsId');
        	window.articleFacetValueFilter = '';
        	var searchArticleQueryStringTemp = me.makeArticleQueryString(); 
        	me.articlesApiCall(searchArticleQueryStringTemp, 0, window.sortbyValueArticles, window.sortValueArticles, false, true, 1, window.categoryArticlesFilterId, window.articleFacetValueFilter);
        },
        next: function(e) {
        	e.preventDefault();
        	var me = this;
        	var startIndex = me.model.get('startIndex') + me.model.get('pageSize');
        	var hasPreviousPage = true;
        	var currentPage = me.model.get('currentPage') + 1;
        	var hasNextPage;
        	if (me.model.get('totalCount')/me.model.get('pageSize') > currentPage) {
        		hasNextPage = true;
        	} else {
        		hasNextPage = false;
        	}
        	
        	var searchArticleQueryStringTemp = me.makeArticleQueryString();
        	me.articlesApiCall(searchArticleQueryStringTemp, startIndex, window.sortbyValueArticles, window.sortValueArticles, hasPreviousPage, hasNextPage, currentPage, window.categoryArticlesFilterId, window.articleFacetValueFilter);

        },
        previous: function(e){
        	e.preventDefault();
        	var me = this;
        	var startIndex = me.model.get('startIndex') - me.model.get('pageSize');
        	var currentPage = me.model.get('currentPage') - 1;
        	var hasPreviousPage;
        	if(currentPage == 1) {
        		hasPreviousPage = false;
        	} else {
        		hasPreviousPage = true;
        	}
        	var hasNextPage = true;  
        	var searchArticleQueryStringTemp = me.makeArticleQueryString();
        	me.articlesApiCall(searchArticleQueryStringTemp, startIndex, window.sortbyValueArticles, window.sortValueArticles, hasPreviousPage, hasNextPage, currentPage, window.categoryArticlesFilterId, window.articleFacetValueFilter);
     	
        },
        page: function(e) {
        	e.preventDefault();
        	var me = this;
        	var selectedPageNumber = $(e.currentTarget).data('mz-page-num');
        	var startIndex = (selectedPageNumber * me.model.get('pageSize')) - me.model.get('pageSize');
        	var currentPage = selectedPageNumber;
        	var hasPreviousPage;
        	if(currentPage == 1) {
        		hasPreviousPage = false;
        	} else {
        		hasPreviousPage = true;
        	}
        	
        	var hasNextPage;
        	if (me.model.get('totalCount')/me.model.get('pageSize') > currentPage) {
        		hasNextPage = true;
        	} else {
        		hasNextPage = false;
        	}
        	var searchArticleQueryStringTemp = me.makeArticleQueryString();
        	me.articlesApiCall(searchArticleQueryStringTemp, startIndex, window.sortbyValueArticles, window.sortValueArticles, hasPreviousPage, hasNextPage, currentPage, window.categoryArticlesFilterId, window.articleFacetValueFilter);

        },
        makeArticleQueryString: function(){
        	if(window.categoryArticlesFilterId) {
        		return searchArticleQueryString + ' and CategoryId req ' + window.categoryArticlesFilterId + '';
        	} else {
        		return searchArticleQueryString;
        	}
        },
        articlesApiCall: function(searchArticlesQueryString, startIndex, sortbyValueArticles, sortValueArticles, hasPreviousPage, hasNextPage, currentPage, categoryID, articleFacetValueFilter){
        	var me = this;
        	var articlePageSize = $.cookie('articlePageSize');
        	$('html, body').scrollTop(145);
        	$('.content-loading').show();
        	if(articlePageSize){
        		    window.articlePageSize = articlePageSize;        		
        	 }else{
        		    window.articlePageSize = defaultPageSize;
        	 }
        	api.get('search', {query:searchQuery, filter:searchArticlesQueryString, facet: 'categoryId', facetTemplate: 'categoryId:' + categoryID + '', facetValueFilter: articleFacetValueFilter, pageSize: window.articlePageSize, startIndex: startIndex, sortBy:sortbyValueArticles} ).then(function(response) {
    			me.model = new Backbone.Model(response.data);
    			
    			if (me.model.get('totalCount')/me.model.get('pageSize') > currentPage) {
            		hasNextPage = true;
            	} else {
            		hasNextPage = false;
            	}
    			
    			me.model.set('sortByValue', sortValueArticles);
    			me.model.set('narrowSearchList', window.globalSearchCategorylist);
    			me.model.set('hasPreviousPage', hasPreviousPage);
    			me.model.set('hasNextPage', hasNextPage);
    			me.model.set('currentPage', currentPage);
    			me.model.set('firstIndex', startIndex + 1);    			
    			me.model.set('lastIndex', startIndex + me.model.get('items').length);
    			
    			var middlePageNumbers = [];
    			
    			if(currentPage <= 3) {
    				middlePageNumbers = [];
    				for(var x=2; x<response.data.pageCount; x++) {
    	    			if(x <= 6) {
    	    				middlePageNumbers.push(x);
    	    			}
    	    		}
    			} else if(currentPage >= response.data.pageCount-2) {
    				middlePageNumbers = [];
    				for(var y=response.data.pageCount-5; y<=response.data.pageCount-1; y++) {
    	    			if(y >= 2) {
    	    				middlePageNumbers.push(y);
    	    			}
    	    		}
    			} else {
    				middlePageNumbers = [];
    				for(var z=2; z>=1; z--) {
        				if(currentPage != 1) {
        					middlePageNumbers.push(currentPage - z);
        				}
            		}
        			middlePageNumbers.push(currentPage);
        			for(var p=1; p<=2; p++) {
            			middlePageNumbers.push(currentPage + p);
            		}
    			}
    			me.model.set('middlePageNumbers', middlePageNumbers);
    			
    			//Making URL as per filters
    			
    			//Making URL as per filters
    			var facetValueFilterUrl = '';
    			_.map(me.model.get('facets'), function(item, index){
    				if(item.field == 'tenant~article-type') {
    					_.map(item.values, function(facet, i){
    						if(facet.isApplied){
    							facetValueFilterUrl = facetValueFilterUrl + facet.filterValue + ',';
    			            }
    					});
    				}
    			});
    			if(!facetValueFilterUrl){
    				facetValueFilterUrl='';
    			} 
    			facetValueFilterUrl = facetValueFilterUrl.replace('&', '%26');
    			
    			var makeArticleUrl = '';
    			if(me.model.get('startIndex')){
    				makeArticleUrl = makeArticleUrl + '&ExpStartIndex=' + me.model.get('startIndex');
    			}
    			if(window.categoryArticlesFilterId){
    				makeArticleUrl = makeArticleUrl + '&ExpCategoryId=' + window.categoryArticlesFilterId;
    			}
    			if(facetValueFilterUrl){
    				makeArticleUrl = makeArticleUrl + '&expFacetValueFilter=' + facetValueFilterUrl;
    			} 
    			if(sortbyValueArticles) {
    				makeArticleUrl = makeArticleUrl + '&ExpSortBy=' + sortbyValueArticles;
    			}    
    			window.makeArticleUrl = makeArticleUrl;
    		    window.history.pushState({},"", makeurl + window.makeProductUrl + window.makeArticleUrl);
    		    
            	me.render();
            	$('.content-loading').hide();
            	$(document).find('a[href="#expert-advice-tab"] > span').text('(' + me.model.get('totalCount') + ')');
            	
    		});
        },
        render: function(){
        	var me = this;
        	me.model.set('currentLocale', locale);
        	me.model.set('currentSite', currentSite);
            Backbone.MozuView.prototype.render.apply(this, arguments);
            moreLessConfig();
        }
	});
	
	var SearchProductView = Backbone.MozuView.extend({
		templateName: "modules/common/search-products-view",
		additionalEvents: {
	        'change select[data-mz-value="pageSize"]': 'pageSize',
	        "click .ga-product-image": "sendProductDataToGA",
			"click .ga-product-name": "sendProductDataToGA"
	    },
	    sendProductDataToGA:function(e){
			var url = $(e.currentTarget).attr("href");
	    	var product = $(e.target).parents('div.ign-ga-ecommerce');
	    	var locale = require.mozuData('apicontext').headers['x-vol-locale'];
	         var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
	         locale = locale.split('-')[0];
	         var currentLocale = '';
	         if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
	             currentLocale = locale === 'fr' ? '/fr' : '/en';
	         }
        },
		pageSize: function(e){
			var me = this;
			var selectedPageSize = $(e.currentTarget).val();
			window.productPageSize = selectedPageSize;
			if (ua.indexOf('chrome') > -1) {
				if (/edg|edge/i.test(ua)){
					$.cookie('productPageSize',window.productPageSize);
				}else{
					document.cookie = "productPageSize="+window.productPageSize+";Secure;SameSite=None";
				}
			} else {
				$.cookie('productPageSize',window.productPageSize);
			}
			var productPageSize = $.cookie('productPageSize');
			var searchProductQueryStringTemp = me.makeProductQueryString();
    		me.productApiCall(searchProductQueryStringTemp, 0, window.sortbyValueProducts, window.sortValueProducts, false, true, 1, window.categoryID, window.facetValueFilter);
        },
		initialize: function() {
			 var locale = require.mozuData('apicontext').headers['x-vol-locale'];
			 var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
	         locale = locale.split('-')[0];
			 var currentLocale = '';
	         if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
				currentLocale = locale === 'fr' ? '/fr' : '/en';
			}
			this.model.set('currentLocale', locale);
			this.model.set('currentSite', currentSite);
		},
		sortProducts: function(e){
			e.preventDefault();
        	var me = this;
        	window.sortbyValueProducts =  $(e.currentTarget).data('value');
        	window.sortValueProducts =  $(e.currentTarget).text();
        	me.getproductSortByValue();
		},
		applyFacets: function(e){
        	var me = this;
			var selected = $(e.currentTarget).closest('.filters').find('input[name="sortBy"]:checked');
			window.sortbyValueProducts = $(selected).data('value');
			window.sortValueProducts =  $(selected).data('sort-value');
			me.getproductSortByValue();
        },
        getproductSortByValue: function() {
        	var me = this;
        	var categoryId = window.categoryID;
        	var searchProductQueryStringTemp = me.makeProductQueryString();
    		me.productApiCall(searchProductQueryStringTemp, 0, window.sortbyValueProducts, window.sortValueProducts, false, true, 1, window.categoryID, window.facetValueFilter);
        },
        sortAndFilter: function(e){
        	e.preventDefault();
        	$(e.currentTarget).closest('.tab-pane').find('.filters').addClass('open');
        	moreLessConfig();
        },
        clearFacets:function(){
        	var me = this;
        	$(document).find('.filters').removeClass('open');
        	var searchProductQueryStringTemp = me.makeProductQueryString();
        	me.productApiCall(searchProductQueryStringTemp, 0, '', Hypr.getLabel('default'), false, true, 1, window.categoryID, '');
        },
        drillDownSort: function(e) {
        	e.preventDefault();
        	var me = this;
        	var categoryID = $(e.currentTarget).data('mz-hierarchy-id');
        	window.categoryID = categoryID;
        	var searchProductQueryStringTemp = me.makeProductQueryString();        	
        	me.productApiCall(searchProductQueryStringTemp, 0, window.sortbyValueProducts, window.sortValueProducts, false, true, 1, window.categoryID, window.facetValueFilter);
        },
        filterbyFacet: function(e) {
        	var me = this;
        	var facetValueFilter = "";
        	var selected = $(document).find('input[data-mz-action="filterbyFacet"]:checked');
        	selected.each(function(index) {   
            	var facetName = $(selected[index]).data('mz-facet');
            	var facetValue = $(selected[index]).data('mz-facet-value');
        		if(facetName == 'Price' || facetName == 'tenant~rating') {
        			facetValueFilter = facetValueFilter + facetValue + ',';
            	}else {
            		facetValueFilter = facetValueFilter + facetName + ':' + facetValue + ',';
            	}            	
            });
        	window.facetValueFilter = facetValueFilter;
        	var searchProductQueryStringTemp = me.makeProductQueryString();        	
        	if($(window).width() > 767 || $(e.currentTarget).data('mz-facet') == HyprLiveContext.locals.themeSettings.soldItemAttr) {
        		me.productApiCall(searchProductQueryStringTemp, 0, window.sortbyValueProducts, window.sortValueProducts, false, true, 1, window.categoryID, window.facetValueFilter);
        	}
        },        
        allCategories: function() {
        	var me = this;
        	window.categoryID = Hypr.getThemeSetting('searchableProductsId');
        	window.facetValueFilter = '';
        	var searchProductQueryStringTemp = me.makeProductQueryString();
        	me.productApiCall(searchProductQueryStringTemp, 0, window.sortbyValueProducts, window.sortValueProducts, false, true, 1, window.categoryID, window.facetValueFilter);
        },
        next: function(e) {
        	e.preventDefault();
        	var me = this;
        	var startIndex = me.model.get('startIndex') + me.model.get('pageSize');
        	var hasPreviousPage = true;
        	var currentPage = me.model.get('currentPage') + 1;
        	var hasNextPage;
        	if (me.model.get('totalCount')/me.model.get('pageSize') > currentPage) {
        		hasNextPage = true;
        	} else {
        		hasNextPage = false;
        	}
        	var searchProductQueryStringTemp = me.makeProductQueryString();
        	me.productApiCall(searchProductQueryStringTemp, startIndex, window.sortbyValueProducts, window.sortValueProducts, hasPreviousPage, hasNextPage, currentPage, window.categoryID, window.facetValueFilter);
        },
        previous: function(e){
        	e.preventDefault();
        	var me = this;
        	var startIndex = me.model.get('startIndex') - me.model.get('pageSize');
        	var currentPage = me.model.get('currentPage') - 1;
        	var hasPreviousPage;
        	if(currentPage == 1) {
        		hasPreviousPage = false;
        	} else {
        		hasPreviousPage = true;
        	}
        	var hasNextPage = true;        
        	var searchProductQueryStringTemp = me.makeProductQueryString();
        	me.productApiCall(searchProductQueryStringTemp, startIndex, window.sortbyValueProducts, window.sortValueProducts, hasPreviousPage, hasNextPage, currentPage, window.categoryID, window.facetValueFilter);
       	},
        page: function(e) {
        	e.preventDefault();
        	var me = this;
        	var selectedPageNumber = $(e.currentTarget).data('mz-page-num');
        	var startIndex = (selectedPageNumber * me.model.get('pageSize')) - me.model.get('pageSize');
        	var currentPage = selectedPageNumber;
        	var hasPreviousPage;
        	if(currentPage == 1) {
        		hasPreviousPage = false;
        	} else {
        		hasPreviousPage = true;
        	}
        	
        	var hasNextPage;
        	if (me.model.get('totalCount')/me.model.get('pageSize') > currentPage) {
        		hasNextPage = true;
        	} else {
        		hasNextPage = false;
        	}
        	var searchProductQueryStringTemp = me.makeProductQueryString();
        	me.productApiCall(searchProductQueryStringTemp, startIndex, window.sortbyValueProducts, window.sortValueProducts, hasPreviousPage, hasNextPage, currentPage, window.categoryID, window.facetValueFilter);
        },
        makeProductQueryString: function(){
        	if(window.categoryID) {
        		return searchProductQueryString + ' and CategoryId req ' + window.categoryID + '';
        	} else {
        		return searchProductQueryString;
        	}
        },
		productApiCall: function(searchProductQueryString, startIndex, sortbyValueProducts, sortValueProducts, hasPreviousPage, hasNextPage, currentPage, categoryID, facetValueFilter){
			var me = this;
			var productPageSize = $.cookie('productPageSize');
			$('html, body').scrollTop(145);
			$('.content-loading').show();
			searchProductQueryString = searchProductQueryString;
			if(productPageSize){
				window.productPageSize = productPageSize;
			}else {
				window.productPageSize = defaultPageSize;
			}
			var settings = '';
			if(localStorage.getItem('preferredStore')){
				var currentStore = $.parseJSON(localStorage.getItem('preferredStore')),
					currentStoresWarehouse = _.find(currentStore.attributes, function(attribute){
						return attribute.fullyQualifiedName === Hypr.getThemeSetting('warehouseAttr');
					}),
					currentStoreForSTH = _.find(currentStore.attributes, function predicate(attribute) {
						return (attribute.fullyQualifiedName === Hypr.getThemeSetting('shipToHomeStoreFlagAttrFQN') &&
							attribute.values[0]);
					});
				if(currentStoreForSTH && currentStoreForSTH.values[0] === true) {
					if(currentStoresWarehouse.values[0] === "1" || currentStoresWarehouse.values[0] === "4" ){
						settings = 'DC1_OR_DC4_S2H';
					}else if(currentStoresWarehouse.values[0] === "2"){
						settings = 'DC2_S2H';
					}
				}else{
					settings = 'DC'+currentStoresWarehouse.values[0]+'_NON_S2H';
				}
			}
			var catId = 'categoryId:' + categoryID + '';
			var torontoTimeZone = timeZoneSupport.findTimeZone('America/Toronto');
			var today = new Date();
			console.log('productApiCall: searchQuery --> ', searchQuery);
			console.log('productApiCall: searchProductQueryString --> ', searchProductQueryString);
			api.request('GET', '/api/commerce/catalog/storefront/productsearch/search?query='+searchQuery+'&filter='+searchProductQueryString+'&facet=categoryId&facetTemplate='+catId+'&facetValueFilter='+facetValueFilter+'&pageSize='+window.productPageSize+'&startIndex='+startIndex+'&sortBy='+sortbyValueProducts+'&searchSettings='+settings).then(function(response) {
				me.model = new Backbone.Model(response);

				var removeValFrom = [];
				_.map(response.facets, function(item, index){
					if(item.values.length === 0) {
						removeValFrom.push(index);
					}
				});

				response.facets = response.facets.filter(function(value, index) {
					return removeValFrom.indexOf(index) == -1;
				});

				me.model.set('facets', response.facets);
				if (me.model.get('totalCount')/me.model.get('pageSize') > currentPage) {
					hasNextPage = true;
				} else {
					hasNextPage = false;
				}
				me.model.set('sortByValue', sortValueProducts);
				me.model.set('narrowSearchList', window.globalSearchCategorylist);
				me.model.set('hasPreviousPage', hasPreviousPage);
				me.model.set('hasNextPage', hasNextPage);
				me.model.set('currentPage', currentPage);
				me.model.set('firstIndex', startIndex + 1);
				me.model.set('isPurchesLocation', isPurchesLocation);
				me.model.set('lastIndex', startIndex + me.model.get('items').length);
				var middlePageNumbers = [];
				if(currentPage <= 3) {
					middlePageNumbers = [];
					for(var x=2; x<response.pageCount; x++) {
						if(x <= 6) {
							middlePageNumbers.push(x);
						}
					}
				} else if(currentPage >= response.pageCount-2) {
					middlePageNumbers = [];
					for(var y=response.pageCount-5; y<=response.pageCount-1; y++) {
						if(y >= 2) {
							middlePageNumbers.push(y);
						}
					}
				} else {
					middlePageNumbers = [];
					for(var z=2; z>=1; z--) {
						if(currentPage != 1) {
							middlePageNumbers.push(currentPage - z);
						}
					}
					middlePageNumbers.push(currentPage);
					for(var p=1; p<=2; p++) {
						middlePageNumbers.push(currentPage + p);
					}
				}
				me.model.set('middlePageNumbers', middlePageNumbers);

				//Making URL as per filters
				var facetValueFilterUrl = '';
				_.map(me.model.get('facets'), function(item, index){
					if(item.field != 'CategoryId' && item.field != 'system~price-list-entry-type') {
						_.map(item.values, function(facet, i){
							if(facet.isApplied){
								facetValueFilterUrl = facetValueFilterUrl + facet.filterValue + ',';
							}
						});
					}
				});
				if(!facetValueFilterUrl){
					facetValueFilterUrl='';
				}
				facetValueFilterUrl = facetValueFilterUrl.replace('&', '%26');

				var makeProductUrl = '';
				if(me.model.get('startIndex')){
					makeProductUrl = makeProductUrl + '&startIndex=' + me.model.get('startIndex');
				}
				if(window.categoryID){
					makeProductUrl = makeProductUrl + '&categoryId=' + window.categoryID;
				}
				if(facetValueFilterUrl){
					makeProductUrl = makeProductUrl + '&facetValueFilter=' + facetValueFilterUrl;
				}
				if(sortbyValueProducts) {
					makeProductUrl = makeProductUrl + '&sortBy=' + sortbyValueProducts;
				}
				window.makeProductUrl = makeProductUrl;
				window.history.pushState({},"", makeurl + window.makeProductUrl + window.makeArticleUrl);
				var searchProds = me.model.get('items');
				_.each(searchProds, function(product){
					var promoItemZonedTime = timeZoneSupport.getZonedTime(new Date(product.updateDate), torontoTimeZone);
					var promoItemDate = new Date(promoItemZonedTime.year, promoItemZonedTime.month-1, promoItemZonedTime.day, promoItemZonedTime.hours, promoItemZonedTime.minutes, promoItemZonedTime.seconds, promoItemZonedTime.milliseconds);
					if(today <= promoItemDate){
						product.isPromoEnabled = true;
						product.promoTillDate = formatDate(promoItemDate);
					}
				});

				me.render();
				$(document).find('a[href="#product-tab"] > span').text(me.model.get('totalCount'));
				$('.content-loading').hide();
				var productImpressions = $('[data-ign-gtm-impression]');
				window.sendGtmDataLayer(productImpressions);
			});
		},
        render: function(){
        	var me = this;
            var contextSiteId = apiContext.headers["x-vol-site"];
            var homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId");
            var frSiteId = Hypr.getThemeSetting("frSiteId");
            if(homeFurnitureSiteId === contextSiteId){
                me.model.set("isHomeFurnitureSite", true);
            }
            if(frSiteId === contextSiteId){
                var productItems = me.model.get('items');
                _.each(productItems, function(product){
                     product.isFrenchSite = true;
                });
            }
            var currentPurchaseLocation = pageContext.purchaseLocation;
			if(currentPurchaseLocation){
				me.model.set("currentPurchaseLocation", currentPurchaseLocation.code);
			}
			if(localStorage.getItem('preferredStore')){
				var currentStore = $.parseJSON(localStorage.getItem('preferredStore'));
				var isBOHStore = _.find(currentStore.attributes, function(attribute){
					return attribute.fullyQualifiedName === Hypr.getThemeSetting('hhBOHStoreAttr');
				});
				if(isBOHStore && isBOHStore.values[0]){	
					me.model.set("isBOHStore", true);
				}
			}
            me.model.set('currentLocale', locale);
			me.model.set('currentSite', currentSite);
            Backbone.MozuView.prototype.render.apply(this, arguments);
            
        	if (window.gridStatus == "list") {
            	$('.result-view-toggle').find('li').removeClass('is-current');
            	$('.result-view-toggle').find('li:first-child').addClass('is-current');
            	$(document).find('.mz-productlist').addClass('list');
            } else {
            	$('.result-view-toggle').find('li').removeClass('is-current');
            	$('.result-view-toggle').find('li:nth-child(2)').addClass('is-current');
            	$(document).find('.mz-productlist').removeClass('list');
            }
        	
        	moreLessConfig();
        }
        
    });
	
    var NoSearchView = Backbone.MozuView.extend({
    	templateName: "modules/common/no-search-view"
    });	
    
    $.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results===null){
           return null;
        }
        else{
           return decodeURI(results[1]) || 0;
        }
    };
    var facetHeightWithPX;
    function moreLessConfig(){
    	var facetHeight = Hypr.getThemeSetting('facetDefaultHeight');
    	facetHeightWithPX = Hypr.getThemeSetting('facetDefaultHeight')+'px';
    	/*Code for moretag on facets - Start*/
        $('.mz-facetingform').find('.mz-l-sidebaritem .facet-details > ul').each(function(){
        	if($(this).height() > facetHeight ) {
        		$(this).parent().find('.moretag').css('display', 'inline-block');
        		if($(this).closest('.mz-l-sidebaritem').hasClass('for-caregory')){
        			$(this).css({'height': '205px', 'overflow-y': 'hidden'});
        		}else {
        			$(this).css({'height': facetHeightWithPX, 'overflow-y': 'hidden'});
        		}
        	}
        });
        $('.mz-facetingform').find('.mz-l-sidebaritem #Price > ul').css({'height': 'auto', 'overflow-y': 'hidden'});
        /*Code for moretag on facets - End*/
    }
    
    function calculateMiddlePageNumbers(dataModel) {
    	var middlePageNumbers = [];    			
		if(dataModel.currentPage <= 3) {
			middlePageNumbers = [];
			for(var x=2; x<dataModel.pageCount; x++) {
    			if(x <= 6) {
    				middlePageNumbers.push(x);
    			}
    		}
		} else if(dataModel.currentPage >= dataModel.pageCount-2) {
			middlePageNumbers = [];
			for(var y=dataModel.pageCount-5; y<=dataModel.pageCount-1; y++) {
    			if(y >= 2) {
    				middlePageNumbers.push(y);
    			}
    		}
		} else {
			middlePageNumbers = [];
			for(var z=2; z>=1; z--) {
				if(dataModel.currentPage != 1) {
					middlePageNumbers.push(dataModel.currentPage - z);
				}
    		}
			middlePageNumbers.push(dataModel.currentPage);
			for(var p=1; p<=2; p++) {
    			middlePageNumbers.push(dataModel.currentPage + p);
    		}
		}
		dataModel.middlePageNumbers = middlePageNumbers;
    }
    
	if (searchQuery) {
		$('.content-loading').show();
	}
	var getProducts = function(isDirectLink){
		var facetValueFilter = decodeURIComponent($.urlParam('facetValueFilter'));
		if(facetValueFilter=='0' || facetValueFilter=='null') {
			facetValueFilter = '';
		}
		window.facetValueFilter = facetValueFilter;
		var categoryId = $.urlParam('categoryId');
		var startIndex;
		if ($.urlParam('startIndex')) {
			startIndex = $.urlParam('startIndex');
		} else {
			startIndex = 0;
		}
		var tempSearchProductQueryString;
		if (!categoryId) {
			categoryId = Hypr.getThemeSetting('searchableProductsId');
		}
		window.categoryID = categoryId;
		tempSearchProductQueryString = searchProductQueryString + ' and CategoryId req ' + categoryId + '';

		var sortbyValueProducts;
		if ($.urlParam('sortBy')) {
			sortbyValueProducts = $.urlParam('sortBy');
		} else {
			sortbyValueProducts = HyprLiveContext.locals.themeSettings.defaultSort;
		}

		if(sortbyValueProducts) {
			var sortValueProducts = _.findWhere(sortByOptions, {value: sortbyValueProducts});
			sortValueProducts = sortValueProducts.text;
		}

		var searchProductResult;
		var productPageSize = $.cookie('productPageSize');
		tempSearchProductQueryString = tempSearchProductQueryString;
		if(productPageSize){
			window.productPageSize = productPageSize;
		}else{
			window.productPageSize = defaultPageSize;
		}
		var settings = '';
		if(localStorage.getItem('preferredStore')){
			var currentStore = $.parseJSON(localStorage.getItem('preferredStore')),
				currentStoresWarehouse = _.find(currentStore.attributes, function(attribute){
					return attribute.fullyQualifiedName === Hypr.getThemeSetting('warehouseAttr');
				}),
				currentStoreForSTH = _.find(currentStore.attributes, function predicate(attribute) {
					return (attribute.fullyQualifiedName === Hypr.getThemeSetting('shipToHomeStoreFlagAttrFQN') &&
						attribute.values[0]);
				});
			if(currentStoreForSTH && currentStoreForSTH.values[0] === true) {
				if(currentStoresWarehouse.values[0] === "1" || currentStoresWarehouse.values[0] === "4" ){
					settings = 'DC1_OR_DC4_S2H';
				}else if(currentStoresWarehouse.values[0] === "2"){
					settings = 'DC2_S2H';
				}
			}else{
				settings = 'DC'+currentStoresWarehouse.values[0]+'_NON_S2H';
			}
		}
		var catId = 'categoryId:' + categoryId + '';
		var searchTerms = HyprLiveContext.locals.pageContext.search.query;
		// console.log('getProducts: searchQuery -->', searchQuery);
		if (/^\d{4}-\d{3}/.test(searchTerms)) {
			var queryParts = searchTerms.replace(/ /g,'/').replace(/,/g,'/').replace(/-/g,'/').split('/');
			// console.log('getProducts: queryParts -->', JSON.stringify(queryParts));
			var newQueryParts = [];
			for (var toIndex = 0;  toIndex < queryParts.length; toIndex++) {
				if (queryParts[toIndex] == 'to') {
					var firstCode = parseInt(queryParts[toIndex-1], 10) + 1;
					var  lastCode = parseInt(queryParts[toIndex+1], 10);
					for (var code = firstCode; code < lastCode; code++) {
						var codeString = code.toString();
						if (codeString.length == 2) codeString = '0' + codeString;
						if (codeString.length == 1) codeString = '00' + codeString;
						newQueryParts.push(codeString);
					}
				} else {
					newQueryParts.push(queryParts[toIndex]);
				}
			}
			// console.info('getProducts: newQueryParts -->', JSON.stringify(newQueryParts));
			var prefix = '';
			var productCodes = '';
			for (var index = 0;  index < newQueryParts.length; index++) {
				var queryPart = newQueryParts[index];
				if (queryPart.length == 4) {
					prefix = queryPart;
				}
				if (queryPart.length == 3) {
					if (productCodes.length > 0) {
						productCodes = productCodes + ',' + prefix + queryPart;
					} else {
						productCodes = prefix + queryPart;
					}
				}
			}
			tempSearchProductQueryString = 'productCode in [' + productCodes +'] and ' + tempSearchProductQueryString;
			searchTerms = '';
		}
		console.log('getProducts: tempSearchProductQueryString --> ', tempSearchProductQueryString);
		api.request('GET', '/api/commerce/catalog/storefront/productsearch/search?query='+searchTerms+'&filter='+tempSearchProductQueryString+'&facet=categoryId&facetTemplate='+catId+'&facetValueFilter='+facetValueFilter+'&pageSize='+window.productPageSize+'&startIndex='+startIndex+'&sortBy='+sortbyValueProducts+'&searchSettings='+settings).then(function(response) {
			searchProductResult = response;
			searchProductResult.facets = response.facets;
			searchProductResult.isPurchesLocation = isPurchesLocation;
			var currentStore;
			var torontoTimeZone = timeZoneSupport.findTimeZone('America/Toronto');
			var today = new Date();
			if(localStorage.getItem('preferredStore')){
				currentStore = $.parseJSON(localStorage.getItem('preferredStore'));
			}
			if(searchProductResult.totalCount > 0){
				var searchProductCount = searchProductResult.totalCount;
				var childrenFacetValues = _.findWhere(response.facets, {facetType: "Hierarchy"});
				var globalSearchCategorylist=[];
				_.map(childrenFacetValues.values, function(category){
					_.map(category.childrenFacetValues, function(childrenitem){
						_.map(childrenitem.childrenFacetValues, function(item){
							if(item.parentFacetValue != brandsCategoryId){
								globalSearchCategorylist.push(item);
							}
						});
					});
				});

				globalSearchCategorylist = _.sortBy(globalSearchCategorylist, function(item){ return -item.count; });

				globalSearchCategorylist = globalSearchCategorylist.slice(0, 6);

				searchProductResult.currentPage = (searchProductResult.startIndex/searchProductResult.pageSize) + 1;
				calculateMiddlePageNumbers(searchProductResult);

				if(sortValueProducts) {
					searchProductResult.sortByValue = sortValueProducts;
					window.sortValueProducts = sortValueProducts;
				} else {
					searchProductResult.sortByValue = Hypr.getLabel('default');
					window.sortValueProducts = Hypr.getLabel('default');
				}
				if(sortbyValueProducts) {
					window.sortbyValueProducts = sortbyValueProducts;
				} else {
					window.sortbyValueProducts = "";
				}

				if(searchProductResult.currentPage == 1) {
					searchProductResult.hasPreviousPage = false;
				} else {
					searchProductResult.hasPreviousPage = true;
				}

				if (searchProductCount/searchProductResult.pageSize > searchProductResult.currentPage) {
					searchProductResult.hasNextPage = true;
				} else {
					searchProductResult.hasNextPage = false;
				}

				searchProductResult.firstIndex = searchProductResult.startIndex + 1;
				searchProductResult.lastIndex = searchProductResult.startIndex + searchProductResult.items.length;
				if(globalSearchCategorylist.length === 6) {
					var filterString = "categoryId eq ",
						lastIndex = 5;
					_.map(globalSearchCategorylist, function(item, i) {
						filterString = filterString + item.value;
						if(i != lastIndex){
							filterString = filterString + ' or categoryId eq ';
						}
					});

					api.get('categories', {filter: filterString} ).then(function(categories) {
						globalSearchCategorylist = categories.data.items;

						if (globalSearchCategorylist.length === 6) {
							window.globalSearchCategorylist = globalSearchCategorylist;
							var narrowSearchList = [];
							_.each(window.globalSearchCategorylist,function(category){
								if(category.content.categoryImages && category.content.categoryImages.length > 0){
									narrowSearchList.push(category);
								}
							});
							searchProductResult.narrowSearchList = narrowSearchList;
						} else {
							searchProductResult.narrowSearchList = window.globalSearchCategorylist = false;
						}
						_.each(searchProductResult.items, function(product){
							var promoItemZonedTime = timeZoneSupport.getZonedTime(new Date(product.updateDate), torontoTimeZone);
							var promoItemDate = new Date(promoItemZonedTime.year, promoItemZonedTime.month-1, promoItemZonedTime.day, promoItemZonedTime.hours, promoItemZonedTime.minutes, promoItemZonedTime.seconds, promoItemZonedTime.milliseconds);
							if(today <= promoItemDate){
								product.isPromoEnabled = true;
								product.promoTillDate = formatDate(promoItemDate);
							}
						});
						if(isDirectLink) {
							var searchProductView = new SearchProductView({
								el: $('#product-tab'),
								model:  new Backbone.Model(searchProductResult)
							});
							globalSearchProductView = searchProductView;
							searchProductView.render();
						} else {
							globalSearchProductView = new Backbone.Model(searchProductResult);
							globalSearchProductView.render();
						}
						$(document).find('a[href="#product-tab"] > span').text(searchProductCount);
						$('.content-loading').hide();
					});
				} else {
					searchProductResult.narrowSearchList = window.globalSearchCategorylist = false;
					_.each(searchProductResult.items, function(product){
						var promoItemZonedTime = timeZoneSupport.getZonedTime(new Date(product.updateDate), torontoTimeZone);
						var promoItemDate = new Date(promoItemZonedTime.year, promoItemZonedTime.month-1, promoItemZonedTime.day, promoItemZonedTime.hours, promoItemZonedTime.minutes, promoItemZonedTime.seconds, promoItemZonedTime.milliseconds);
						if(today <= promoItemDate){
							product.isPromoEnabled = true;
							product.promoTillDate = formatDate(promoItemDate);
						}
					});
					if(isDirectLink) {
						var searchProductView = new SearchProductView({
							el: $('#product-tab'),
							model:  new Backbone.Model(searchProductResult)
						});
						globalSearchProductView = searchProductView;
						searchProductView.render();
					} else {
						globalSearchProductView.model = new Backbone.Model(searchProductResult);
						globalSearchProductView.render();
					}
					$(document).find('a[href="#product-tab"] > span').text(searchProductCount);
					$('.content-loading').hide();
				}
			} else {
				var noResultModel = {tab: 'Products'};
				var noSearchView = new NoSearchView({
					el: $('#product-tab'),
					model: new Backbone.Model(noResultModel)
				});
				noSearchView.render();
				$('.content-loading').hide();
				$(document).find('a[href="#product-tab"] > span').text('0');
				$(document).find('a[href="#expert-advice-tab"]').trigger('click');
			}

		});

	};
		
	var getArticles = function(isDirectLink){
		var facetValueFilter = decodeURIComponent($.urlParam('expFacetValueFilter'));
		if(facetValueFilter=='0' || facetValueFilter=='null') {
			facetValueFilter = '';
		}
		window.articleFacetValueFilter = facetValueFilter;
		var ExpStartIndex;
		if ($.urlParam('ExpStartIndex')) {
			ExpStartIndex = $.urlParam('ExpStartIndex');
		} else {
			ExpStartIndex = 0;
		}
		
		var categoryId = $.urlParam('ExpCategoryId');			
		if (!categoryId) {
			categoryId = Hypr.getThemeSetting('searchableProductsId');
		}
		window.categoryArticlesFilterId = categoryId;
		
		var expCategoryId = $.urlParam('ExpCategoryId');
		var tempSearchArticleQueryString;
		if (categoryId) {
			tempSearchArticleQueryString = searchArticleQueryString + ' and CategoryId req ' + categoryId + '';
		}
		
		var sortbyValueArticles;
		if ($.urlParam('ExpSortBy')) {
			sortbyValueArticles = $.urlParam('ExpSortBy');
		} else {
			sortbyValueArticles = HyprLiveContext.locals.themeSettings.defaultSort;
		}    		
		
		if(sortbyValueArticles) {
			var sortValueArticles = _.findWhere(sortByOptions, {value: sortbyValueArticles});
			sortValueArticles = sortValueArticles.text;
		}
		
		var expertArticles;    
		var articlePageSize = $.cookie('articlePageSize');
		if(articlePageSize){
				window.articlePageSize = articlePageSize;
		}else{
				window.articlePageSize = defaultPageSize;
		}
		$('.content-loading').show();
		api.get('search', {query:searchQuery, filter:tempSearchArticleQueryString, facet: 'categoryId', facetTemplate: 'categoryId:' + categoryId + '', facetValueFilter: facetValueFilter, pageSize: window.articlePageSize, startIndex: ExpStartIndex, sortBy: sortbyValueArticles} ).then(function(response) {
			expertArticles = response.data;   
			if(expertArticles.totalCount > 0){
				var expertAdviceCount = expertArticles.totalCount;
				
				expertArticles.currentPage = (expertArticles.startIndex/expertArticles.pageSize) + 1;
				calculateMiddlePageNumbers(expertArticles);	        		
				
				if(sortValueArticles) {
					expertArticles.sortByValue = sortValueArticles;
					window.sortValueArticles = sortValueArticles;
				} else {
					expertArticles.sortByValue = Hypr.getLabel('default');
					window.sortValueArticles = Hypr.getLabel('default');
				}
				if(sortbyValueArticles) {
					window.sortbyValueArticles = sortbyValueArticles;
				} else {
					window.sortbyValueArticles = "";
				}	        		
				
				if(expertArticles.currentPage == 1) {
					expertArticles.hasPreviousPage = false;
				} else {
					expertArticles.hasPreviousPage = true;
				}
				
				if (expertArticles.totalCount/expertArticles.pageSize > expertArticles.currentPage) {
					expertArticles.hasNextPage = true;
				} else {
					expertArticles.hasNextPage = false;
				}
							
				expertArticles.firstIndex = expertArticles.startIndex + 1;
				expertArticles.lastIndex = expertArticles.startIndex + expertArticles.items.length;
				
				if(isDirectLink) {
					var expertAdviceView = new ExpertAdviceView({
						el: $('#expert-advice-tab'),
						model: new Backbone.Model(expertArticles)
					});	 	    			
					globalExpertAdviceView = expertAdviceView;
					expertAdviceView.render();
				} else {
					globalExpertAdviceView.model = new Backbone.Model(expertArticles);
					globalExpertAdviceView.render(); 
				}
				
				$('.content-loading').hide();
				$(document).find('a[href="#expert-advice-tab"] > span').text('(' + expertAdviceCount + ')');
				if(searchQuery) {
					$(document).find('.tt-input').val(decodeURIComponent($.cookie('stopwords')));
					$(document).find('.mz-searchbox-field > input[type="search"]').val(decodeURIComponent($.cookie('stopwords')));
				}
				
			} else {
				// var noResultModel = {tab: 'Articles'};
				// var noSearchView = new NoSearchView({
				//    el: $('#expert-advice-tab'),
				//    model: new Backbone.Model(noResultModel)
				// });
				// noSearchView.render(); 
				$('.content-loading').hide();
				$(document).find('a[href="#expert-advice-tab"] > span').text('(0)');
				if(searchQuery) {
					$(document).find('.tt-input').val(decodeURIComponent($.cookie('stopwords')));
					$(document).find('.mz-searchbox-field > input[type="search"]').val(decodeURIComponent($.cookie('stopwords')));
				}
			}
			
		});
	};
	
	/*This function resturn date in YYYY-MM-DD format */
	function formatDate(date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('-');
	}
	
	getProducts(true);
	/** In case of Flyer Item#, set articles count to 0, to avoid article search API call */
	if (/^\d{4}-\d{3}/.test(searchQuery)) {
		$(document).find('a[href="#expert-advice-tab"] > span').text('(0)');
	}
	
	$(document).on('click', '.result-view-toggle li a', function (e){
		e.preventDefault();
		$(this).closest('.result-view-toggle').find('li').removeClass('is-current');
		$(this).closest('li').addClass('is-current');
		
		if($(this).data('toggle-type') == 'list') {
			window.gridStatus = "list";
			$(document).find('.mz-productlist').addClass('list');
		} else {
			window.gridStatus = "";
			$(document).find('.mz-productlist').removeClass('list');
		}        	
		$(document).find('#expert-advice-tab .mz-productlist').removeClass('list');
	});
	
	$(document).find('.filter-button').on('click', function() {
		$(document).find('.filters').removeClass('open');
	});
	
	$('.result-type-selector a').on('click', function(event) {
		if ((homeFurnitureSiteId != currentSite) && event && event.target && event.target.href) {
			if (event.target.href.indexOf('#expert-advice-tab') > -1) {
				var articlesFound = $(document).find('a[href="#expert-advice-tab"] > span').text();
				// console.log('articlesFound -->', articlesFound, 'IsEmpty -->', (articlesFound === ''));
				if (articlesFound === '' && $('#expert-advice-tab').text() === '') {
					console.log('#expert-advice-tab: search articles, then show results.');
					getArticles(true);
				} else {
					console.log('#expert-advice-tab: show previous article search results.');
				}
			}
		}
		setTimeout(function(){
			moreLessConfig();
		}, 100);
	});
	
	$(document).on('click', '.moretag', function(event){
		event.preventDefault();
		var viewLess = Hypr.getLabel('viewLess');
		var viewMore = Hypr.getLabel('viewMore');
		if($(this).find('span').text() === viewMore) {
			$(this).parent().find('.mz-facetingform-facet').css('height', 'auto');
			$(this).find('span').text(viewLess);
		} else {
			if($(this).closest('.mz-l-sidebaritem').hasClass('for-caregory')){
				$(this).parent().find('.mz-facetingform-facet').css('height', '205px');
			}else {
				$(this).parent().find('.mz-facetingform-facet').css('height', facetHeightWithPX);
			}
			
			$(this).find('span').text(viewMore);
		}
	});
	
	window.onpopstate = function(event) {
		$('.content-loading').show();
		if (homeFurnitureSiteId == currentSite) {
			getProducts(false);
			$('.content-loading').hide();
		} else {
			getProducts(false);
			getArticles(false);
			$('.content-loading').hide();
		}
	};

	$(document).ready(function() {
		var searchQueryString = decodeURIComponent(location.search.split("query=")[1].split("&")[0]);
		if(!$.cookie('QueryWithoutStopwords')){
			$("#search-stopword-result").text(searchQueryString.replace(/\+/g, " "));
		}else{
			if($.cookie('QueryWithoutStopwords') === searchQueryString){
				if($.cookie('stopwords')){
					$("#search-stopword-result").text(decodeURIComponent($.cookie('stopwords')).replace(/\+/g, " "));
				}
			}else{
				$("#search-stopword-result").text(searchQueryString.replace(/\+/g, " "));
			}
			// $('.mz-searchbox-input').click(function(){
			// 	$(".mz-searchbox-input").val('');
			// });
		}

		$(document).on('click', '.data-layer-productClick',function(e){
			console.log('Clicked... data-layer-productClick');
			var breadcrumbStr = "";
			var currentParentId = parseInt($(e.currentTarget).closest("[data-mz-categoryId]").attr('data-mz-categoryId').split('|')[1], 10);
			window.getBreadcrumbflow(currentParentId, window.allCategories, breadcrumbStr, e);
		});
	});
		
});
