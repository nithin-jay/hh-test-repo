define(['modules/api',
  'modules/backbone-mozu',
  'underscore',
  'modules/jquery-mozu',
  'modules/models-cart',
  'modules/cart-monitor',
  'hyprlivecontext',
  'hyprlive',
  'modules/preserve-element-through-render',
  'modules/modal-dialog',
  'modules/models-product',
  'modules/views-messages',
  'modules/views-cart-quantity-alert-modal',
  'modules/models-postal-code-checker',
  'modules/models-oversize-charge-calculator',
  'modules/models-sth-postal-code-cookie',
  'modules/models-shipping-time-lines',
  "bignumber",
  'modules/cookie-utils'
], function (api, Backbone, _, $, CartModels, CartMonitor, HyprLiveContext, Hypr, preserveElement, modalDialog, ProductModels, messageViewFactory, QuantityAlertModalView, PostalCodeChecker, OversizeCalculator, STHPostalCodeCookie, ShippingTimelineModel, BigNumber, CookieUtils) {  
  var fromcheckout = localStorage.getItem("fromcheckout");

  if(fromcheckout === "true"){
    localStorage.removeItem("fromcheckout");
    window.location.reload();
    return;
  }

  var previousCartMessages = [];
  var appliedCouponCodes = [];
  var configPromises = [];
  var entityListItems;
  var user = require.mozuData('user');
  var apiContext = require.mozuData('apicontext');
  var MessageModel = Backbone.MozuModel.extend({});
  var MessageCollection = new Backbone.Collection();
  var fallBackItemQuantities,
    currentStoresWarehouse,
    isCurrentStoreBoh,
    fetchedItemInventories = [],
    quantityExceededProducts = [],
    fulfillmentChange = false,
    overSizeConfig = {},
    isShipToHomeEnabledStore,
    fulFillmentChangeMethod,
    isFirstTime = true,
    showMessage = false,
    sthItems = [],
    quantityUpdate = false,
    ua = navigator.userAgent.toLowerCase(),
    warehouseList = Hypr.getThemeSetting('warehousesForSTH'),
    activeWarehouse = warehouseList ? warehouseList.slice(0) : [];

  var messageView = messageViewFactory({
    el: $('[data-mz-message-bar]'),
    model: MessageCollection
  });

  var messageModel = new MessageModel();

  var itemQtyList = [], removedProducts = [];
  var oversizeCalculator = new OversizeCalculator();
 
  var isItemRemoveInProgress = 'No';

  var CartView = Backbone.MozuView.extend({
    templateName: 'modules/cart/cart-table',
    additionalEvents: {
      'click .changePreferredStore': 'changePreferredStore'
    },
    initialize: function () {
      sessionStorage.removeItem('s_d');
      $('#content-loading').hide();
      var locale = require.mozuData('apicontext').headers['x-vol-locale'];
      var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
      locale = locale.split('-')[0];
      var currentLocale = '';
      if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite) {
        currentLocale = locale === 'fr' ? '/fr' : '/en';
      }
      this.pickerDialog = this.initializeStorePickerDialog();
      var me = this;
      var messageConfig = $(".mz-configuarble-message-container").attr('data-mz-message-config');
      var parsedMessageRes = messageConfig ? JSON.parse(messageConfig) : '';
      me.model.set('currentLocale', locale);
      me.model.set('currentSite', currentSite);
      previousCartMessages = me.model.get('cartMessage');
      var previousItems = [];
      if (previousCartMessages) {
        removedProducts = previousCartMessages.productsRemoved;
        if (previousCartMessages) {
          removedProducts = previousCartMessages.productsRemoved;
        }
      }
      var cartData = this.model.apiModel.data.items,
      preferredStore;
      if(localStorage.getItem('preferredStore')){
        preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
      }
      var itemsToRemove = [], cartTotalCount = 0;

      /* find products which are having different store for ship to store item and different purchase location for ship to home item and remove those from cart by api call*/
      cartData.forEach(function (item) {
        if ((item.fulfillmentMethod === 'Pickup' && (item.fulfillmentLocationCode !== preferredStore.code || item.purchaseLocation !== preferredStore.code)) || (item.fulfillmentMethod === 'Ship' && item.purchaseLocation !== preferredStore.code)) {
          itemsToRemove.push(item);//line item
        }
      });
      var queryString = '';
      itemsToRemove.forEach(function (item, index) {
        if (index != itemsToRemove.length - 1) {
          queryString += 'productCode eq ' + item.product.productCode + ' or ';
        } else {
          queryString += 'productCode eq ' + item.product.productCode;
        }
      });

      //Fetch entitylist for the products from cart
      var entityListForEHF = Hypr.getThemeSetting('entityListForEHF');
      if (queryString) {
        var ehfQueryString = queryString + ' and province eq ' + preferredStore.address.stateOrProvince;
        api.get('entityList', { listName: entityListForEHF, filter: ehfQueryString }).then(function (response) {
          if (response.data.items.length > 0) { //if EHF is applied for current product
            entityListItems = response.data;
          }
        });
      }
      var currentStore = localStorage.getItem('preferredStore') ? $.parseJSON(localStorage.getItem('preferredStore')):'';
      isShipToHomeEnabledStore = _.findWhere(currentStore.attributes, {fullyQualifiedName : Hypr.getThemeSetting('shipToHomeStoreFlagAttrFQN')});

      me.model.set("shipToHomeEnableStore",isShipToHomeEnabledStore ? isShipToHomeEnabledStore.values[0] : false);

      me.checkProvinceMatch(parsedMessageRes,currentStore);
      // perform steps to remove products from cart and then re add products having price > 0 to cart
      if (itemsToRemove.length > 0) {
        localStorage.setItem("fromcheckout", true);
        
        $('.cart-page-container').css('visibility', 'hidden');
        var removeSteps = this.getRemoveSteps(itemsToRemove);
        var indexOfProductToRemove =[];
        me.model.set('showTotal', false);
        api.steps(removeSteps).then(function (response) {

          var isEcomStore = _.findWhere(preferredStore.attributes, { 'fullyQualifiedName': Hypr.getThemeSetting('isEcomStore') });
          if (isEcomStore && isEcomStore.values[0] === 'Y') {
            api.get('products', { filter: queryString }).then(function (productResponse) {
              if (removedProducts.length > 0) {
                previousItems = $.parseJSON($.cookie('previousItems'));
                me.sendDataLayerInfo(removedProducts, previousItems);
              }
              var productData = productResponse.data.items;              
              productData.forEach(function(productNewArray, index) {
                if (productNewArray.price.price === 999999) {
                  indexOfProductToRemove.push(index);
                }
              });  
              for (var i = indexOfProductToRemove.length -1; i >= 0; i--) {
                productData.splice(indexOfProductToRemove[i],1);
              } 
              me.performAddSteps(itemsToRemove, productData);   
            }, function (errorResponse) {
              //handle product fetch error
            });
          } else {
            me.model.apiGet().then(function (reponse) {
              $('.cart-page-container').css('visibility', 'visible');
              $('#content-loading').hide();
              _.each(itemsToRemove, function (item) {
                removedProducts.push(item.product);
              });
              if (removedProducts.length > 0) {
                previousItems = $.parseJSON($.cookie('previousItems'));
                me.sendDataLayerInfo(removedProducts, previousItems);
              }
              CartMonitor.setCount(0);
            });
          }
        }, function (errorResponse) {
          //handle remove steps error
        });
      } else {
        if(me.model.get("items").length){
          me.getItemInventory(false);
        }else{
          $('#content-loading').hide();
        }
        me.model.set('showTotal', true);
        if (removedProducts.length > 0) {
          previousItems = $.parseJSON($.cookie('previousItems'));
          me.sendDataLayerInfo(removedProducts, previousItems);
        }
      }
      //setup coupon code text box enter.
      this.listenTo(this.model, 'change:couponCode', this.onEnterCouponCode, this);
      this.codeEntered = !!this.model.get('couponCode');
      this.$el.on('keypress', 'input', function (e) {
        if (e.which === 13) {
          if (me.codeEntered) {
            me.handleEnterKey();
          }
          return false;
        }
      });

      this.listenTo(this.model.get('items'), 'quantityupdatefailed', this.onQuantityUpdateFailed, this);

      var visaCheckoutSettings = HyprLiveContext.locals.siteContext.checkoutSettings.visaCheckout;
      var pageContext = require.mozuData('pagecontext');
      if (visaCheckoutSettings.isEnabled) {
        window.onVisaCheckoutReady = initVisaCheckout;
        require([pageContext.visaCheckoutJavaScriptSdkUrl], initVisaCheckout);
      }
      //cache for storing info retrieved through API calls
      this.fulfillmentInfoCache = [];
      this.model.get('items').forEach(function (item) {
        var dataObject = {
          cartItemId: item.id,
          locations: []
        };
        me.fulfillmentInfoCache.push(dataObject);

      });
      this.appliedCouponCodes();

      //update cart count excluding EHF product
      _.each(me.model.get('items').toJSON(), function (product) {
        if (product.product.productCode != 'EHF101') {
          cartTotalCount += product.quantity;
        }
      });
      me.model.set('count', cartTotalCount);
      me.model.set('cartCount', cartTotalCount);
      var documentListForAeroplanPromo = Hypr.getThemeSetting('documentListForAeroplanPromo');
      api.get('documentView', { listName: documentListForAeroplanPromo }).then(function (response) {
        if (response.data.items.length > 0) {
          var aeroplanMilesFactor = response.data.items[0].properties.markupFactor;
          me.model.set('aeroplanMiles', aeroplanMilesFactor);
          me.model.set('isPromoApplied', true);
          me.render();
        }
      });

      if(isShipToHomeEnabledStore && isShipToHomeEnabledStore.values[0]){
        me.getOverSizeConfigurations();
      }
      $(".checkoutBtnSection").removeClass("is-loading");
      this.setOrderType();
      this.calculateEhfTotal(false);
    },
    checkProvinceMatch: function(configurableMessageResponse,currentStore){
        var me = this;
        if(currentStore && currentStore.address.stateOrProvince){
            var isProvinceMatched = _.findWhere(configurableMessageResponse.msgContent, {province: currentStore.address.stateOrProvince});
            if(isProvinceMatched){
                me.model.set("provinceMatched",true);
                me.model.set("minimumOrderMsgCart",isProvinceMatched.cartPageMessage);
                me.model.set("cartOrderArrivalStoreMessage",isProvinceMatched.orderArrivalAtStoreMsg);
                me.model.set("minOrderAmount", parseInt(isProvinceMatched.minimumOrderAmount, 10));
            }else{
                me.model.set("provinceMatched",false);
            }
        }
    },
    closeHeader: function(){
      var me = this;
      me.$el.find("#header-notifications-cart").addClass("hidden");
      showMessage = false;
    },
    setPostalCodeCookie: function(fetchedInventory){
      var sthPostalCodeCookie = new STHPostalCodeCookie();
      var me = this;
      sthPostalCodeCookie.getPostalCode().then(function(response) {
        var sthPostalCode = $.parseJSON($.cookie('sthPostalCode'));
        if(sthPostalCode) me.model.set("setViaPreferredStore",sthPostalCode.setViaPreferredStore);

        if(response){
          me.model.set("sthfResponse",response);
          if(response.status && response.status === "Valid"){
            me.getAndSetShippingTimelines(response.postalCode, fetchedInventory);
            me.getEhfTemplateData(response.postalCode);
          } else {
            me.changeFullFillmentPostal();
            me.render();
          }
        }
      },function(err) {
        console.log(err);
      });
    },
    getState: function(zipCode){
      var me = this;
      var zipCodeProvince = '';
      _.each(Hypr.getThemeSetting('canadaStates'), function (province) {
          var postalCodeFirstLetters= province.postalcodeletters,
              isLetterPresent = postalCodeFirstLetters.includes(zipCode.charAt(0));
          if(isLetterPresent) zipCodeProvince = province.key;
      });
      return zipCodeProvince;
    },
    getEhfTemplateData: function(zipCodeProvince) {
      var self = this;
      var entityListForEHF = Hypr.getThemeSetting('entityListForEHF');
      var province = self.getState(zipCodeProvince);
      var orderItems = this.model.get('items').models;
      var productFilterQuery = orderItems.map(function (orderItem) {
        return 'productCode eq ' + orderItem.get('product').id;
      }).join(' or ');
      var finalQuery = productFilterQuery + ' and province eq ' + province;
      api.get('entityList', {
        listName: entityListForEHF,
        filter: finalQuery
      }).then(function (response) {
          console.log('ehfTemplate', response.data.items);
          self.model.set("ehfTempleteData", response.data.items);
          self.calculateEhfTotal(true);
      }, function(err) {
        self.$el.find('.shipping-next-btn').removeClass('is-loading');
        if(self.model.get('isAddressSubmit'))self.next();
        console.log('Error while fetching ehf templates', err);
      });
    },
    calculateEhfTotal: function(isRenderRequired){
      var ehfTotal=0;
      var ehfData = this.model.get("ehfTempleteData");
      _.each(this.model.get('items').models, function (item) {
        var ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
          var ehfOption = item.attributes.product.attributes.options.findWhere({
            'attributeFQN': ehfAttributeFQN
          });
        if (ehfOption) {
          if (item.get('fulfillmentMethod') === 'Pickup') {
            ehfTotal += ehfOption.get('shopperEnteredValue') * item.attributes.quantity;
          }
        }
        if (item.get('fulfillmentMethod') === 'Ship') {
          var ehfProduct = _.findWhere(ehfData,{ 'productCode': item.get("product").get("productCode") });
          if(ehfProduct) {
            ehfTotal += ehfProduct.feeAmt * item.attributes.quantity;
          }
        }
      });
      this.model.set('EHFTotal', ehfTotal);
      if(isRenderRequired) Backbone.MozuView.prototype.render.apply(this, arguments);
    },
    getAndSetShippingTimelines: function(customerPostalCode, fetchedInventory){
      var self = this,
          shippingModel = new ShippingTimelineModel(),
          shipItems = this.getShipItems(),
          shipInventory = this.getShipItemInventories(shipItems, fetchedInventory);
        shippingModel.getShippingTimeline(customerPostalCode, shipInventory, 'cart', shipItems).then(function(timelineData){
          console.log('shippingTimeline', timelineData);
          self.model.set('sthShippingTimeline', timelineData.shippingTimeline);
          self.render();
        }, function(error){
          self.render();
          console.log('Error while getting shipping timelines', error);
        });
    },
    getShipItems: function() {
      var sthItems = [];
      var shipEnabledItems = _.filter(this.model.get('items').models, function(item){
        var productProperties = item.get('product').get('properties') && item.get('product').get('properties').length ? item.get('product').get('properties') : [],
            isSThAttrPresent =  productProperties.length ? _.findWhere(productProperties, {'attributeFQN': Hypr.getThemeSetting('shipToHomeEnableAttrName')}) : false,
            isProductEnabledForSTH = isSThAttrPresent ? isSThAttrPresent.values[0].value : false;
        if(isProductEnabledForSTH && item.get('fulfillmentMethod') === 'Ship') sthItems.push(item);
         return isProductEnabledForSTH  === true;
      });
      return shipEnabledItems;
    },
    getShipItemInventories: function(shipItems, fetchedInventory){
      var sthInventories = [];
      _.each(shipItems, function(item){
        var productCode = item.get('product').get('productCode'),
            itemInventories = _.where(fetchedInventory, { productCode: productCode });
        sthInventories.push(itemInventories);
      });
      return _.flatten(sthInventories);
    },
    //method to get oversize configurations from arcjs
    getOverSizeConfigurations: function(){
      var self = this;
      oversizeCalculator.getOversizeConfigurations().then(function(response){
        overSizeConfig = response;
        self.render();
      },function(err){
        console.log("Error while fetching oversize configurations",err);
      });
    },
    // Function is used to set order type which will be used to show and hide content on page.
    setOrderType: function () {
      var isPickUp = 0,
        isShip = 0,
        orderType = '';
      _.each(this.model.get('items').models, function (item) {
        if (item.get('fulfillmentMethod') === 'Pickup') {
          isPickUp++;
        } else if (item.get('fulfillmentMethod') === 'Ship') {
          isShip++;
        }
      });

      if (isPickUp > 0 && isShip > 0) {
        orderType = 'mixOrder';
      } else if (isPickUp > 0) {
        orderType = 'shipToStoreOrder';
      } else if (isShip > 0) {
        orderType = 'shipToHomeOrder';
      }
      this.model.set('orderType', orderType);
    },
    storeFallbackQuantity: function () {
      fallBackItemQuantities = [];
      this.model.apiModel.data.items.forEach(function (item) {
        fallBackItemQuantities.push({
          productCode: item.product.productCode,
          quantity: item.quantity
        });
      });
    },
    /* Function used get the current inventory and set item quantity to max [available qty ] if it is greater tha available inventory. */
    getItemInventory: function (isCartRewrite) {
      var self = this;
      if (localStorage.getItem('preferredStore')) {
        var currentStore = $.parseJSON(localStorage.getItem('preferredStore')),
          isBOHStore = _.find(currentStore.attributes, function (attribute) {
            return attribute.fullyQualifiedName === Hypr.getThemeSetting('hhBOHStoreAttr');
          }),
          productCodesQuery = this.getFilterQuery();
        currentStoresWarehouse = _.find(currentStore.attributes, function (attribute) {
          return attribute.fullyQualifiedName === Hypr.getThemeSetting('warehouseAttr');
        });
        isCurrentStoreBoh = isBOHStore ? isBOHStore.values[0] : false;
        warehouseList.push(currentStoresWarehouse.values[0]);
        if (currentStoresWarehouse || isCurrentStoreBoh) {
          var data = {
            'locationCodes': warehouseList,
            'productCodes': productCodesQuery
          };

          if (isCurrentStoreBoh) data.locationCodes.push(currentStore.code);

          self.fetchInventories(data).then(function (response) {
            console.log('fetchedInventories', response);
            fetchedItemInventories = [];
            fetchedItemInventories = response.items;
            if(isShipToHomeEnabledStore && isShipToHomeEnabledStore.values[0]){
              self.setPostalCodeCookie(response.items);
            }
            self.checkQuantity(fetchedItemInventories, isCartRewrite);
          }, function (error) {
            $('#content-loading').hide();
            console.error('failed to get location inventory', error);
          });
        }

      }else{
        $('#content-loading').hide();
      }
    },
    getFilterQuery: function () {
      return this.model.apiModel.data.items.map(function (item) {
        return item.product.productCode;
      });
    },
    fetchInventories: function (data) {
      var inventoryApi = '/api/commerce/catalog/storefront/products/locationinventory?time=' + Date.now();
      return api.request('POST', inventoryApi, data);
    },
    /*Check item quantity if greater than available inventory then update item quantity to available.
     if available inventory is zero then remove product from cart */
    checkQuantity: function (fetchedItemInventories, isCartRewrite) {
      var self = this,
        actionItems = {
          removeItems: [],
          updateItems: []
        };
      _.each(this.model.get('items').models, function (item) {
        var itemInventoryData = self.validateQuantity(fetchedItemInventories, item),
          availableInventory = itemInventoryData.availableInventory;
        item.set('availableInventory', availableInventory);
        if (availableInventory > 0 && item.get('quantity') > availableInventory) {
          item.set('quantity', availableInventory);
          actionItems.updateItems.push(item.toJSON());
        } else if (availableInventory <= 0) {
          actionItems.removeItems.push(item.toJSON());
        }
        self.validateQuantity(fetchedItemInventories, item, true);
      });

      if (actionItems.updateItems.length || actionItems.removeItems.length) {
        //$('#content-loading').show();
          self.performQuantityRemoveSteps(actionItems, isCartRewrite);
          $('.cart-page-container').css('visibility', 'visible');
          $('#content-loading').hide();
          self.renderQuantityAlertModal(actionItems);

      } else {
        self.render();
        $('.cart-page-container').css('visibility', 'visible');
        $('#content-loading').hide();
      }
    },
    renderQuantityAlertModal: function(actionItems){
        var self = this,
           quantityAlertModalView = new QuantityAlertModalView({
              el: $('#quantityAlertModalContainer'),
              model: new Backbone.MozuModel({
                quantityUpdateItems: actionItems.updateItems,
                removeItems: actionItems.removeItems,
                user: user,
                apiContext: apiContext
              })
            });
        quantityAlertModalView.render();
        $('#quantityAlertModal').modal('show');
        $(document).on('refreshCart',function(){
          self.refreshCart(); 
        });
    },
    performQuantityRemoveSteps: function (actionItems, isCartRewrite) {
      var self = this,
        removeSteps = self.getRemoveSteps(actionItems.removeItems);
      api.steps(removeSteps).then(function () {
        if(actionItems.removeItems.length && actionItems.updateItems.length === 0 ){
          self.refreshCart();
        }
      }, function () {
        self.render();
        self.updateCount();
        if (isCartRewrite) self.setPreviousCartMessages();
        $('#content-loading').hide();
        console.log('Failed to update inventory');
      });
    },
    refreshCart: function(){
      var self = this;
      $('#content-loading').show();

      self.model.apiGet().then(function (response) {
        self.updateCount();
        console.log("response", response);
        $("#data-mz-preload-cart").text(JSON.stringify(response.data));
        $('#content-loading').hide();
      });
    },
    setPreviousCartMessages: function () {
      $('.cart-page-container').css('visibility', 'visible');
      try {
        messageModel.set({ message: previousCartMessages.message });
        messageModel.set({ messageType: previousCartMessages.messageType });
        messageModel.set({ productsRemoved: previousCartMessages.productsRemoved });
        MessageCollection.add(messageModel);
        this.trigger('error', previousCartMessages.message);
        messageView.render();
      } catch (error) {
        console.log('Error while setting cart messages', error);
      }

    },
    getQuantityUpdateSteps: function (items) {
      var tasks = items.map(function (item) {
        return function () {
          return item.apiUpdateQuantity(item.get('quantity'));
        };
      });
      return tasks;
    },
    setInventoryData: function () {
      var fetchedProductInventory = fetchedItemInventories,
        errorCount = 0;
      _.each(this.model.get('items').models, function (item) {
        var productCode = item.get('product').get('productCode'),
          matchedData = _.where(fetchedProductInventory, { productCode: productCode }),
          totalInventory = 0;
        if (isCurrentStoreBoh) {
          var inventoryOne = matchedData[0] ? matchedData[0].stockAvailable : 0,
            inventoryTwo = matchedData[1] ? matchedData[1].stockAvailable : 0;
          totalInventory = inventoryOne + inventoryTwo;
        } else {
          totalInventory = matchedData[0] ? matchedData[0].stockAvailable : 0;
        }
        item.set('availableInventory', totalInventory);
      });
    },
    setItemMaxQtyFlag: function () {
      _.each(this.model.get('items').models, function (item) {
        var quantityExceededProduct = _.findWhere(quantityExceededProducts, { productCode: item.get('product').get('productCode'), fulfillmentMethod: item.get('fulfillmentMethod') });
        item.set('isQuantityError', quantityExceededProduct ? quantityExceededProduct.isMaxQuantityError : false);
        item.set('availableInventory', quantityExceededProduct ? quantityExceededProduct.maxQtyCount : 0);
        item.set('storeAndWarehouseInventory', quantityExceededProduct ? quantityExceededProduct.combinedInventory : 0);
        item.set('availableWarehouseInventory', quantityExceededProduct ? quantityExceededProduct.wareHouseInventoryData ? quantityExceededProduct.wareHouseInventoryData : 0 : 0);
        item.set('isPageLoaded',true);
      });
    },
    setAeroplanMiles: function (aeroplanMiles) {
      var me = this;

    },
    resolvePromises: function (promises) {
      var deferred = $.Deferred();
      var fulfilled = 0, length = promises.length;
      var results = [];

      if (length === 0) {
        deferred.resolve(results);
      } else {
        promises.forEach(function (promise, i) {
          $.when(promise()).then(function (value) {
            results = value;
            fulfilled++;
            if (fulfilled === length) {
              deferred.resolve(results);
            }
          });
        });
      }
      return deferred.promise();
    },
    performAddSteps: function (oldItems, newItems) {
      var self = this,
        finalProducts;
      var tempOldItems = $.extend(true, {}, oldItems);
      self.getFinalProducts(oldItems, newItems);
      $.when(self.resolvePromises(configPromises)).then(function (results) {
        finalProducts = results;
        var tasks = finalProducts.map(function (finalProduct) {

          var oldItem = _.find(oldItems, function (item, index) {
            if (item.product.productCode === finalProduct.attributes.productCode) {
              var itemToReturn = item;
              return itemToReturn;
            }
          });
          return function () {
            if(oldItem.fulfillmentMethod === "Pickup" || (oldItem.fulfillmentMethod === "Ship" && !self.model.get('shipToHomeEnableStore'))){
              return finalProduct.apiAddToCartForPickup(self.getProductAddToCartData(oldItem));
            }else{
              return finalProduct.apiAddToCart(self.getProductAddToCartData(oldItem));
            }
          };
        });

        api.steps(tasks).then(function (response) {
          self.model.apiGet().then(function () {
            self.getItemInventory(true);
          });
          self.model.set('showTotal', true);
          self.refreshCart();
        }, function (errorResponse) {
          self.getItemInventory(true);
          console.log('Cart rewrite : Failed to Add products in cart', errorResponse);
        });
      });
    },
    getProductAddToCartData: function (oldItem) {
      var fulfillmentMethod = oldItem.fulfillmentMethod,
      self = this,
      preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
      if (fulfillmentMethod === 'Ship' && self.model.get('shipToHomeEnableStore')) {
        return {
          fulfillmentMethod: ProductModels.Product.Constants.FulfillmentMethods.SHIP,
          quantity: oldItem.quantity
        };
      } else {
        return {
          fulfillmentLocationCode: preferredStore.code,
          fulfillmentMethod: ProductModels.Product.Constants.FulfillmentMethods.PICKUP,
          fulfillmentLocationName: preferredStore.name,
          quantity: oldItem.quantity
        };
      }
    },
    getRemoveSteps: function (items) {
      var self = this;
      var tasks = items.map(function (item) {
        return function () {
          return self.model.removeItem(item.id);
        };
      });
      return tasks;
    },
    getFinalProducts: function (cartData, productsToAdd) {
      var finalProducts = [],
        self = this,
        price;
      _.each(cartData, function (cartItem) {
        _.each(productsToAdd, function (product) {
          if (cartItem.product.productCode == product.productCode) {
            configPromises.push(function () {
              return $.Deferred(function (dfd) {
                var productModel = new ProductModels.Product(product);
                if (cartItem.product.variationProductCode) {
                  _.each(cartItem.product.options, function (currentOption) {
                    self.configureProduct(currentOption, productModel);
                  });
                  productModel.on('productConfigured', function () {
                    price = productModel.get('price');
                    if (price.attributes.price > 0) {
                      finalProducts.push(productModel);
                      self.addRelatedEHFProduct(productModel, dfd, finalProducts);
                    } else {
                      dfd.resolve(finalProducts);
                    }
                  });
                } else {
                  price = productModel.get('price');
                  if (price.attributes.price > 0) {
                    finalProducts.push(productModel);
                    self.addRelatedEHFProduct(productModel, dfd, finalProducts);
                  } else {
                    dfd.resolve(finalProducts);
                  }
                }
              }).promise();
            });
          }
        });
      });
    },
    configureProduct: function ($optionEl, currentModel) {
      var newValue = $optionEl.value,
        oldValue,
        id = $optionEl.attributeFQN,
        isPicked = true,
        option = currentModel.get('options').findWhere({
          'attributeFQN': id
        });
      if (option) {
        if (option.get('attributeDetail').inputType === 'YesNo') {
          option.set('value', isPicked);
        } else if (isPicked) {
          oldValue = option.get('values');
          if (oldValue !== newValue && !(oldValue === undefined && newValue === '')) {
            option.set('value', newValue);
          }
        }
      }
    },
    addRelatedEHFProduct: function (productModel, dfd, finalProducts) {
      var productCode, preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
      var relatedEHF;
      if (productModel.get('variationProductCode')) {
        productCode = productModel.get('variationProductCode');
      } else {
        productCode = productModel.get('productCode');
      }
      if (entityListItems && entityListItems.items.length > 0) {
        relatedEHF = _.findWhere(entityListItems.items, {
          'productCode': productCode,
          'province': preferredStore.address.stateOrProvince
        });
      }
      var ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
      var ehfOption = productModel.get('options').findWhere({
        'attributeFQN': ehfAttributeFQN
      });
      if (ehfOption && ehfOption.get('shopperEnteredValue')) {
        ehfOption.set('shopperEnteredValue', ''); //remove previous EHF
        ehfOption.set('value', '');
      }
      if (relatedEHF && ehfOption) {
        ehfOption.set('shopperEnteredValue', relatedEHF.feeAmt);
        ehfOption.set('value', relatedEHF.feeAmt);
        dfd.resolve(finalProducts);
      } else {
        dfd.resolve(finalProducts);
      }
    },
    checkShiptoHomeItemsAvailibility: function(){
      var items = this.model.get('items').models;
      var self = this;
      var sthItem = _.find(items, function(item){
        return item.get('fulfillmentMethod') === "Ship";
      });
      if(sthItem){
        self.model.set("isShipToHomeItemsPresent",true);
        if(overSizeConfig && overSizeConfig.shippingThreshold){
          self.model.set("thresholdAmount",overSizeConfig.shippingThresholdAmount);
        }
      }else{
        self.model.set("isShipToHomeItemsPresent",false);
      }
    },
    render: function () {
      var me = this,
        estimatedTotal=0,
        estimatedTaxval=0,
        estimated_tax_perc=0,
        contextSiteId = apiContext.headers['x-vol-site'],
        shipItems = me.getShipItems(),
        frSiteId = Hypr.getThemeSetting('frSiteId');
        var finalOversizeCharge,
        customerPostalCode = me.model.get('sthfResponse') ? me.model.get('sthfResponse').postalCode:'';
        if(overSizeConfig && overSizeConfig.configId && me.model.apiModel.data.items) {
          finalOversizeCharge = oversizeCalculator.getOversizeCharges(overSizeConfig, me.model.apiModel.data.items);
        } 
      if (frSiteId === contextSiteId) {
        me.model.set('isFrenchSite', true);
      }
      if (quantityExceededProducts.length) me.setItemMaxQtyFlag();
      this.setOrderType();

      if (localStorage.getItem('preferredStore')) {
        //Set store name for change fulfillment section
        var preferredStoreDetails = $.parseJSON(localStorage.getItem('preferredStore'));
        this.model.set('currentStoreName', preferredStoreDetails.name);
        this.model.set('storeAllowsShipToHome', !!_.find(preferredStoreDetails.attributes,
          function predicate(attribute) {
            return (attribute.fullyQualifiedName === Hypr.getThemeSetting('shipToHomeStoreFlagAttrFQN') &&
              attribute.values[0]);
          }));

          var updatedZipCode= me.$el.find($(".btnPostalCode")).val() || (me.model.get('sthfResponse') ? me.model.get('sthfResponse').postalCode: undefined);
          updatedZipCode = "" + updatedZipCode;
          var EHFTotal = me.model.get('EHFTotal') ? me.model.get('EHFTotal') : 0;
          var startingletter=updatedZipCode.charAt(0);
        if(me.model.get('orderType')!=='shipToStoreOrder') {
          me.getShippingCost(finalOversizeCharge);
        }
        _.each(Hypr.getThemeSetting('canadaStates'), function (province) {
         if(me.model.get('orderType')!=='shipToStoreOrder' && updatedZipCode !== "undefined") {
              //if not ship to store,find updated Zip code 1st Character and check it if present in postalcodeletters array
              var postalcodeArray = province.postalcodeletters;
              var ifmatch_postalcodeletter = postalcodeArray.includes(startingletter);
              if(ifmatch_postalcodeletter===true && me.model.get('shippingCost')) {
                estimated_tax_perc=province.tax; 
                estimatedTotal = parseFloat(BigNumber.sum(me.model.get('discountedSubtotal'), EHFTotal, me.model.get('shippingCost')).toFixed(2));
                estimatedTaxval = parseFloat(new BigNumber(estimatedTotal).multipliedBy(estimated_tax_perc)).toFixed(2);
                me.model.set("estimatedTaxvalue",estimatedTaxval);
              }
          } else if(me.model.get('orderType')==='shipToStoreOrder') {
            me.model.set("finalOversizeCharge",0);
            if(preferredStoreDetails.address.stateOrProvince===province.key) {
                estimated_tax_perc=province.tax;
                estimatedTotal = parseFloat(BigNumber.sum(me.model.get('discountedSubtotal'), EHFTotal).toFixed(2));
                estimatedTaxval = parseFloat(new BigNumber(estimatedTotal).multipliedBy(estimated_tax_perc)).toFixed(2);
                me.model.set("estimatedTaxvalue",estimatedTaxval);
            }
          } 
        });
      }
      me.checkShiptoHomeItemsAvailibility();
      if(navigator.userAgent.match(/(iPhone)|(iPod)|(android)|(webOS)|(ipad)/i)){
        me.model.set("isDesktop",false);
      }else{
          me.model.set("isDesktop",true);
      }
      var scrollPosition = $(window).scrollTop();
      if(showMessage){
        me.$el.find($("#header-notifications-cart")).addClass('sticky');
        me.model.set("showMessage",true);
        if(!quantityUpdate){
          $("html").animate({scrollTop: 0}, "slow");
        }else{
          $("html").animate({scrollTop: scrollPosition - 1}, "slow");
        }
      }else{
        me.$el.find($("#header-notifications-cart")).addClass('sticky');
        me.model.set("showMessage",false);
      }
      
      if (!me.model.get('isPromoApplied')) {
        var aeroplanMiles;
        aeroplanMiles = Math.floor((me.model.get('total') + 0) / 2);
        me.model.set('aeroplanMiles', aeroplanMiles);
      }
      if (me.model.get('isEmpty') && previousCartMessages) {
        messageModel.set({ message: previousCartMessages.message });
        messageModel.set({ messageType: previousCartMessages.messageType });
        messageModel.set({ productsRemoved: previousCartMessages.productsRemoved });
        MessageCollection.add(messageModel);
        me.trigger('error', previousCartMessages.message);
      }
      /* Info Popover */
      setTimeout(function(){ 
        $(function () {
          $('[data-toggle="popover"]').popover();
        });
      },500);

      if(isShipToHomeEnabledStore && isShipToHomeEnabledStore.values[0]){
        me.getShippingCost(finalOversizeCharge);
      }
      if(me.model.get('items').length === 0 || shipItems.length <= 0){
        me.model.set("showMessage",false);
        if (ua.indexOf('chrome') > -1) {
          if (/edg|edge/i.test(ua)) {
            $.cookie('fulFillmentChanged', false, { path: '/' });
          } else {
            document.cookie = "fulFillmentChanged=" + false + ";path=/;Secure;SameSite=None";
          }
        } else {
          $.cookie('fulFillmentChanged', false, { path: '/' });
        }
      }
      preserveElement(this, ['.v-button', '.p-button'], function () {
        Backbone.MozuView.prototype.render.call(this);
      });
    },

    updateQuantity: _.debounce(function (e) {
      var me = this;
      var currentProductQuantity;
      var field = $(e.currentTarget).attr('data-mz-value'); //this field is used to increment/decrement the quantity
      if (field == 'quantity') {
        _.find(me.model.get('items').models, function (val) {
          if (val.get('product').get('productCode') == $(e.currentTarget).attr('data-productCode')) {
            currentProductQuantity = val.get('quantity');
          }
        });
      }
      quantityUpdate = true;

      var quantity = $(e.currentTarget).parent().siblings('.quantity-input-field').val();

      if (field === 'qty-decrement') {
        $(e.currentTarget).parent().siblings('.quantity-input-field').val(parseInt(quantity, 10) - 1);
      }

      if (field === 'qty-increment') {
        $(e.currentTarget).parent().siblings('.quantity-input-field').val(parseInt(quantity, 10) + 1);
      }
      var $qField = '';
      if (field != 'quantity') {
        $qField = $(e.currentTarget).parent().siblings('.quantity-input-field');
      } else {
        $qField = $(e.currentTarget);
      }

      if (isNaN($qField.val()) || $qField.val() <= 0) { //if quantity is not a number or less than equal to 0 then update the quantity to 1
        e.preventDefault();
        $qField.val(1);
        $('#qty-decrement-field').addClass('is-disabled');
        var currentItemId = $qField.data('mz-cart-item'),
          currentItem = me.model.get('items').get(currentItemId);
        this.fetchAvailableInventory(currentItem, 1);
        me.model.set('showTotal', true);
      } else {
        var newQuantity = parseInt($qField.val(), 10),
          id = $qField.data('mz-cart-item'),
          item = this.model.get('items').get(id);
          this.fetchAvailableInventory(item, newQuantity);
      }
      this.calculateEhfTotal(false);
    }, 500),
    fetchAvailableInventory: function(item, newQuantity){
      var me = this,
          currentStore = $.parseJSON(localStorage.getItem('preferredStore')),
          oldQuantity = item.get('quantity');
            item.set('quantity', newQuantity);
            warehouseList.push(currentStoresWarehouse.values[0]);
      if ((currentStoresWarehouse || isCurrentStoreBoh) && currentStore && newQuantity !== oldQuantity) {
        var data = {
          'locationCodes': warehouseList,
          'productCodes': me.getFilterQuery()
        };

        if (isCurrentStoreBoh) data.locationCodes.push(currentStore.code);

        me.fetchInventories(data).then(function (response) {
          var data = me.validateQuantity(response.items, item, true, true);
          fetchedItemInventories = [];
          fetchedItemInventories = response.items;
          if (data.isQuantityValid) {
            me.saveQuantity(item, newQuantity);
          } else {
            me.setItemQuantityToMaxAvailable(item, data.availableInventory);
          }
        }, function (error) {
          console.error('failed to get location inventory', error);
        });
      }
    },
    getShipToHomeItemTotal: function(){
      var me = this,
      items = me.model.apiModel.data.items,
      itemTotal = 0;
      _.each(items,function(item){
        if(item.fulfillmentMethod === 'Ship'){
          itemTotal += item.total;
          var ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
          var ehfOption = _.findWhere(item.product.options, { 'attributeFQN': ehfAttributeFQN });
          if (ehfOption) {
            var currentProductEHFValue = ehfOption.shopperEnteredValue * item.quantity;
            itemTotal += currentProductEHFValue;
          }
        }
      });
      return itemTotal;
    },
    getShippingCost: function(finalOversizeCharge){
      var me = this;
      var total = me.getShipToHomeItemTotal(),
      shippingCost;
      finalOversizeCharge = finalOversizeCharge ? finalOversizeCharge : 0;
      if(total && total >= 50){
        shippingCost = 10 / 100 * total;
      }else{
        shippingCost = 4.99;
      }
      if(overSizeConfig.shippingThreshold && total > overSizeConfig.shippingThresholdAmount){
        shippingCost = 0;
      }
      me.model.set("finalOversizeCharge",finalOversizeCharge);
      shippingCost = shippingCost + finalOversizeCharge;
      me.model.set("shippingCost",shippingCost); 
    },
    setItemQuantityToMaxAvailable: function (item, newQuantity) {
      item.set('quantity', newQuantity);
      this.saveQuantity(item, newQuantity);
    },
    saveQuantity: function (item, newQuantity) {
      var me = this;
      item.apiUpdateQuantity(item.get('quantity')).then(function (item) {
        me.updateDataLayer(item, newQuantity);
      });
    },
    updateDataLayer: function (item, newQuantity) {
      var me = this,
      currAttribute; 
      me.updateCount();
      if(item && item.data){
        currAttribute = _.findWhere(item.data.product.properties, { 'attributeFQN': Hypr.getThemeSetting('brandDesc') });
        var productData = {
          'name': item.data.product.name,  // String. Product Name as captured in Kibo
          'id': item.data.product.productCode, // String. Any unique identifier for a product/SKU
          'price': item.data.product.price.salePrice > 0 ? parseFloat(item.data.product.price.salePrice).toFixed(2).replace('$', '').replace(',', '.').trim() : parseFloat(item.data.product.price.price).toFixed(2).replace('$', '').replace(',', '.').trim(), // String/Currency. Price with discount applied, if available
          'brand': currAttribute ? currAttribute.values[0].stringValue : '', //String. Refers to a product brand, all caps
          'category': me.setCookie(item), // String. Refers to a Product Category.  It's possible to send up to five
                                          // levels of categories to help with segmentation in reports. These levels are
                                          // sent by using the slash (/) between levels, where level 1 is the first item
                                          // in the string, level 2 the second, etc. If a full five-level product
                                          // category string is not available, please use whatever identifier is used by
                                          // Home Hardware to identify a category
          'quantity': item.data.quantity //Integer. Quantity the product was purchased.
        };
        api.get('cart', {}).then(function (cartdata) {
          var itemQty = _.findWhere(itemQtyList, { item: item.data.id });
          var saleforceData = [];
          _.each(cartdata.data.items, function (item) {
            saleforceData.push({
              'id': item.product.productCode, // String. Any unique identifier for a product/SKU
              'price': item.product.price.salePrice > 0 ? parseFloat(item.product.price.salePrice).toFixed(2).replace('$', '').replace(',', '.').trim() : parseFloat(item.product.price.price).toFixed(2).replace('$', '').replace(',', '.').trim(),
              'quantity': item.quantity
            });
          });
          if (itemQty && newQuantity > itemQty.qty) {
            console.log('QTY increased ' + newQuantity);
            dataLayer.push({
              'event': 'addToCart', // if users click to add to cart from QuickView screen then have this event name as
                                    // 'quickAddToCart'
              'metric4': cartdata.data.total, // String/Currency. This metric tracks total dollar value  that is in the
                                              // cart (for all products) after this product(s) addition
              'ecommerce': {
                'currencyCode': 'CAD',
                'add': {
                  'products': [productData]
                }
              },
              'salesForce': {
                'products': saleforceData,
                'cartRecoveryId': cartdata.data.id
              }
            });
          } else if (itemQty && newQuantity < itemQty.qty) {
            console.log('QTY decrease ' + newQuantity);
            dataLayer.push({
              'event': 'removeFromCart',
              'metric4': cartdata.data.total, // String/Currency. This metric tracks total dollar value  that is in the
                                              // cart (for all products) after this product(s) removal
              'ecommerce': {
                'remove': {
                  'products': [productData]
                }
              },
              'salesForce': {
                'products': saleforceData,
                'cartRecoveryId': cartdata.data.id
              }
            });
          }
          itemQty.qty = newQuantity;
        });
      }
    },
    validateQuantity: function (fetchedItemInventory, item, isQuantityChange, fulfillmentChange) {
      var productCode = item.get('product').get('productCode'),
          itemInventoryData = _.where(fetchedItemInventory, { productCode: productCode }),
          fulfillmentType = item.get('fulfillmentMethod'),
          wareHouseInventoryData = this.getWarehouseInventory(itemInventoryData), 
          availableInventory = this.getAvailableInventory(wareHouseInventoryData,fulfillmentType),
          isQuantityValid = true,
          quantityCondition = isQuantityChange ? availableInventory && item.get('quantity') >= availableInventory : availableInventory && item.get('quantity') > availableInventory;

      if (quantityCondition) {
        isQuantityValid = false;
        this.setMaxQuantityFlag(item, true, availableInventory, wareHouseInventoryData, fulfillmentType);
      } else {
        this.setMaxQuantityFlag(item, false, availableInventory, wareHouseInventoryData, fulfillmentType);
      }
      return {
        isQuantityValid: isQuantityValid,
        availableInventory: availableInventory,
        availableWarehouseInventory: wareHouseInventoryData
      };
    },
    getAvailableInventory: function (getAvailableInventory,fulfillmentType) {
      var availableInventory = 0;
      if (isCurrentStoreBoh && fulfillmentType === 'Pickup') {
        availableInventory = getAvailableInventory.bopisInventory;
      } else if (fulfillmentType === 'Pickup') {
        availableInventory = getAvailableInventory.bopisInventory;
      } else {
        availableInventory = getAvailableInventory.sthMaxInventory;
      }
      return availableInventory > 0 ? availableInventory : 0 ;
    },
    getWarehouseBufferInventory: function (stockAvailable) {
      var isWarehouseBufferEnabled = Hypr.getThemeSetting('enableWarehouseBuffer'),
      warehouseBufferValue = Hypr.getThemeSetting('warehouseBufferValue'),
      bufferedWarehouseInventory = 0,
      warehouseCount = stockAvailable && stockAvailable > 0 ? stockAvailable : 0;
      var warehouseInventoryCount = warehouseCount;
      if(isWarehouseBufferEnabled && warehouseBufferValue > 0 && warehouseCount > 1){
          bufferedWarehouseInventory = Math.round((warehouseCount/100) * warehouseBufferValue);
          bufferedWarehouseInventory = bufferedWarehouseInventory < 0.5 ? 1 : bufferedWarehouseInventory;
          warehouseInventoryCount = warehouseCount - bufferedWarehouseInventory;
      }
      return warehouseInventoryCount;
    },
    getWarehouseInventory: function(itemInventoryData){
      var preferredStore = $.parseJSON(localStorage.getItem('preferredStore')) ? $.parseJSON(localStorage.getItem('preferredStore')):'',
      self = this,
      currentStoresWarehouse = _.find(preferredStore.attributes, function(attribute){
        return attribute.fullyQualifiedName === Hypr.getThemeSetting('warehouseAttr');
      }),
      isBOHStore = _.find(preferredStore.attributes, function (attribute) {
        return attribute.fullyQualifiedName === Hypr.getThemeSetting('hhBOHStoreAttr');
      }),
      storeInventory;
      if(isBOHStore && isBOHStore.values[0]){
        storeInventory = _.findWhere(itemInventoryData,{'locationCode':preferredStore.code}) ? _.findWhere(itemInventoryData,{'locationCode':preferredStore.code}).stockAvailable : 0;
      }
      var warehouseInventory = _.findWhere(itemInventoryData,{'locationCode':currentStoresWarehouse.values[0]}) ? _.findWhere(itemInventoryData,{'locationCode':currentStoresWarehouse.values[0]}).stockAvailable : 0;

      var warehouseBufferInventory = self.getWarehouseBufferInventory(warehouseInventory);
      var bopisInventory = isBOHStore ? storeInventory > 0 ? storeInventory + warehouseBufferInventory : 0 + warehouseBufferInventory : warehouseBufferInventory,
      sthWarehouseInventories = [];
      warehouseList = _.unique(warehouseList).filter(function(storeCode) { return storeCode !== preferredStore.code; });
      var activeWarehouseList = activeWarehouse;
      _.each(activeWarehouseList,function(warehouse){
        var sthInventory = _.findWhere(itemInventoryData,{'locationCode':warehouse}).stockAvailable;
        var finalSthInventory = self.getWarehouseBufferInventory(sthInventory);
        sthWarehouseInventories.push(finalSthInventory);
      });
      var sthMaxInventory = _.max(sthWarehouseInventories);
      return {
        bopisInventory : bopisInventory,
        sthMaxInventory : sthMaxInventory
      };
    },
    getStoreInventory: function(itemInventoryData){
      var preferredStore = $.parseJSON(localStorage.getItem('preferredStore')),
          storeInventoryData = _.find(itemInventoryData, function(inventory){
            return inventory.locationCode === preferredStore.code;
          }),
          storeStockAvailable = storeInventoryData ? storeInventoryData.stockAvailable : 0 ;
      return storeStockAvailable > 0  ? storeStockAvailable : 0;
    },
    setMaxQuantityFlag: function (item, isMaxQty, availableInventory, wareHouseInventoryData, fulfillmentType) {
      var isItemPresent = _.findWhere(quantityExceededProducts, { productCode: item.get('product').get('productCode'), fulfillmentMethod: fulfillmentType });
      if (isItemPresent) {
        _.each(quantityExceededProducts, function (product) {
          if (product.productCode === item.get('product').get('productCode') && product.fulfillmentMethod === fulfillmentType) {
            product.isMaxQuantityError = isMaxQty;
            product.maxQtyCount = availableInventory;
            product.fulfillmentMethod = fulfillmentType;
            product.wareHouseInventoryData = wareHouseInventoryData.sthMaxInventory;
            product.combinedInventory = wareHouseInventoryData.bopisInventory;// for choose fulfillment method
          }
        });
      } else {
        quantityExceededProducts.push({
          productCode: item.get('product').get('productCode'),
          isMaxQuantityError: isMaxQty,
          maxQtyCount: availableInventory,
          wareHouseInventoryData: wareHouseInventoryData.sthMaxInventory,// for choose fulfillment method
          fulfillmentMethod : fulfillmentType,
          combinedInventory : wareHouseInventoryData.bopisInventory
        });
      }
    },
    setCookie: function (item) {
      var cookieCategoryName = '';
      if ($.cookie('BreadCrumbFlow')) {
        var FlowArray = $.parseJSON($.cookie('BreadCrumbFlow')),
          getproduct = _.findWhere(FlowArray, { 'productCode': item.data.product.productCode });
        if (getproduct) {
          cookieCategoryName = getproduct.flow;
        }
      }
      return cookieCategoryName;
    },
    updateCount: function () {
      var self = this;
      api.get('cart', {}).then(function (response) {
        var cartTotalCount = 0;
        _.each(response.data.items, function (product) {
          if (product.product.productCode != 'EHF101') {
            cartTotalCount = cartTotalCount + product.quantity;
          }
        });
        self.model.set('count', cartTotalCount);
        self.model.set('cartCount', cartTotalCount);
        _.defer(function () {
          $('#totalCartCount').text(cartTotalCount);
        });
        CartMonitor.setCount(cartTotalCount);
      });
    },
    addToWishlist: function (e) {
      var self = this;
      if (user.isAnonymous) {
        var locale = require.mozuData('apicontext').headers['x-vol-locale'];
        locale = locale.split('-')[0];
        var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
        var currentLocale = '';
        if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite) {
          currentLocale = locale === 'fr' ? '/fr' : '/en';
        }
        window.location.href = currentLocale + '/user/login?returnurl=/cart';
      } else {

        var id = $(e.currentTarget).data('mz-wishlist-item');
        var product = self.model.get('items').get(id).get('product');

        var currentItem = this.model.get('items').get(id);

        product.whenReady(function () {
          if (!product.validate()) {
            product.apiAddToWishlist({
              customerAccountId: require.mozuData('user').accountId,
              quantity: self.model.get('items').get(id).get('quantity')
            }).then(function (item) {
              self.model.removeItem(id).then(function (item) { //After product is successfully added to wishlist,
                var addedToWishlistProductDetails = {
                  'productName': product.get('name'),
                  'productUrl': product.get('url')
                }; //save its details into new variable to show added to wishlist msg on cart page.
                self.model.set('addedToWishlistProductDetails', addedToWishlistProductDetails);
                self.updateCount();
                /*Update Wishlist Count*/
                api.get('wishlist', {}).then(function (response) {
                  var items = response.data.items[0].items;
                  var totalQuantity = 0;
                  _.each(items, function (item, index) {
                    totalQuantity += item.quantity;
                  });
                  $('#wishlist-count').html('(' + totalQuantity + ')');
                });

                var currAttribute = _.findWhere(item.data.product.properties, { 'attributeFQN': Hypr.getThemeSetting('brandDesc') });
                var cookieCategoryName;
                if ($.cookie('BreadCrumbFlow')) {
                  var FlowArray = $.parseJSON($.cookie('BreadCrumbFlow'));
                  var getproduct = _.findWhere(FlowArray, { 'productCode': item.data.product.productCode });
                  if (getproduct) {
                    cookieCategoryName = getproduct.flow;
                  } else {
                    cookieCategoryName = '';
                  }
                }

                var productData = {
                  'name': item.data.product.name,  // String. Product Name as captured in Kibo
                  'id': item.data.product.productCode, // String. Any unique identifier for a product/SKU
                  'price': item.data.product.price.salePrice > 0 ? parseFloat(item.data.product.price.salePrice).toFixed(2).replace('$', '').replace(',', '.').trim() : parseFloat(item.data.product.price.price).toFixed(2).replace('$', '').replace(',', '.').trim(), // String/Currency. Price with discount applied, if available
                  'brand': currAttribute ? currAttribute.values[0].stringValue : '', //String. Refers to a product
                                                                                     // brand, all caps
                  'category': cookieCategoryName, // String. Refers to a Product Category.  It's possible to send up to
                                                  // five levels of categories to help with segmentation in reports.
                                                  // These levels are sent by using the slash (/) between levels, where
                                                  // level 1 is the first item in the string, level 2 the second, etc.
                                                  // If a full five-level product category string is not available,
                                                  // please use whatever identifier is used by Home Hardware to
                                                  // identify a category
                  'quantity': item.data.quantity //Integer. Quantity the product was purchased.
                };
                api.get('cart', {}).then(function (cartdata) {
                  var saleforceData = [];
                  _.each(cartdata.data.items, function (item) {
                    saleforceData.push({
                      'id': item.product.productCode, // String. Any unique identifier for a product/SKU
                      'price': item.product.price.salePrice > 0 ? parseFloat(item.product.price.salePrice).toFixed(2).replace('$', '').replace(',', '.').trim() : parseFloat(item.product.price.price).toFixed(2).replace('$', '').replace(',', '.').trim(),
                      'quantity': item.quantity
                    });
                  });
                  dataLayer.push({
                    'event': 'removeFromCart',
                    'metric4': cartdata.data.total, // String/Currency. This metric tracks total dollar value  that is
                                                    // in the cart (for all products) after this product(s) removal
                    'ecommerce': {
                      'remove': {
                        'products': [productData]
                      }
                    },
                    'salesForce': {
                      'products': saleforceData,
                      'cartRecoveryId': cartdata.data.id
                    }
                  });
                });
                self.render();
              });
            });
          }
        });
      }
    },
    onQuantityUpdateFailed: function (model, oldQuantity) {
      var field = this.$('[data-mz-cart-item=' + model.get('id') + ']');
      if (field) {
        field.val(oldQuantity);
      } else {
        this.render();
      }
    },
    removeItem: function (e) { 
      var self = this;
      if (require.mozuData('pagecontext').isEditMode) {
        // 65954
        // Prevents removal of test product while in editmode
        // on the cart template
        return false;
      }
      var $removeButton = $(e.currentTarget),
      id = $removeButton.data('mz-cart-item') ? $removeButton.data('mz-cart-item') : e.get('id') ? e.get('id') : '';
      self.model.set("showMessage",false);
      showMessage = false;
      var currentItem = this.model.get('items').get(id) ? this.model.get('items').get(id) : e;
      var productCodeToRemove = currentItem.attributes.product.get('productCode');
      var index = sthItems.indexOf(productCodeToRemove);
      if (index > -1) {
        sthItems.splice(index, 1);
      }
      self.model.set("itemsWithLessInventory",sthItems);
      quantityUpdate = true;
      var product = self.model.get('items').get(id).get('product'); //Get product to be removed
       if (isItemRemoveInProgress == 'No') {
         console.log("before double click");
        isItemRemoveInProgress = 'Yes'; 

      self.model.removeItem(id).then(function (item) { //After product is successfully removed from model,
        self.updateCount();
        var removedProductDetails = { 'productName': product.get('name'), 'productUrl': product.get('url') }; //save
                                                                                                              // its
                                                                                                              // details
                                                                                                              // into
                                                                                                              // new
                                                                                                              // variable
                                                                                                              // to
                                                                                                              // show
                                                                                                              // deleted
                                                                                                              // msg on
                                                                                                              // cart page.
        self.model.set('removedProductDetails', removedProductDetails);
        self.render();
        isItemRemoveInProgress = 'No';
        self.setPostalCodeCookie(self.model.get('items'));
        api.get('cart', {}).then(function (cartdata) {
          var saleforceData = [];
          _.each(cartdata.data.items, function (item) {
            saleforceData.push({
              'id': item.product.productCode, // String. Any unique identifier for a product/SKU
              'price': item.product.price.salePrice > 0 ? parseFloat(item.product.price.salePrice).toFixed(2).replace('$', '').replace(',', '.').trim() : parseFloat(item.product.price.price).toFixed(2).replace('$', '').replace(',', '.').trim(),
              'quantity': item.quantity
            });
          });
          dataLayer.push({
            'event': 'removeFromCart',
            'metric4': cartdata.data.total, // String/Currency. This metric tracks total dollar value  that is in the
                                            // cart (for all products) after this product(s) removal
            'ecommerce': {
              'remove': {
                'products': [{
                  'name': product.get('name'),  // String. Product Name as captured in Kibo
                  'id': product.get('productCode'), // String. Any unique identifier for a product/SKU
                  'price': product.get('price').get('salePrice') > 0 ? parseFloat(product.get('price').get('salePrice')).toFixed(2).replace('$', '').replace(',', '.').trim() : parseFloat(product.get('price').get('price')).toFixed(2).replace('$', '').replace(',', '.').trim(), // String/Currency. Price with discount applied, if available
                  'brand': currAttribute ? currAttribute.values[0].stringValue : '', //String. Refers to a product
                                                                                     // brand, all caps
                  'category': cookieCategoryName, // String. Refers to a Product Category.  It's possible to send up to
                                                  // five levels of categories to help with segmentation in reports.
                                                  // These levels are sent by using the slash (/) between levels, where
                                                  // level 1 is the first item in the string, level 2 the second, etc.
                                                  // If a full five-level product category string is not available,
                                                  // please use whatever identifier is used by Home Hardware to
                                                  // identify a category
                  'quantity': product.parent.get('quantity') //Integer. Quantity the product was purchased.
                }]
              }
            },
            'salesForce': {
              'products': saleforceData,
              'cartRecoveryId': cartdata.data.id
            }
          });
        });
      });

      } else {
          console.log("after double click");
        e.preventDefault();
      }
      var currAttribute = _.findWhere(product.attributes.properties, { 'attributeFQN': Hypr.getThemeSetting('brandDesc') });
      var cookieCategoryName;
      if ($.cookie('BreadCrumbFlow')) {
        var FlowArray = $.parseJSON($.cookie('BreadCrumbFlow'));
        var getproduct = _.findWhere(FlowArray, { 'productCode': product.get('productCode') });
        if (getproduct) {
          cookieCategoryName = getproduct.flow;
        } else {
          cookieCategoryName = '';
        }
      }

      return false; 
    },
    empty: function () {
      this.model.apiDel().then(function () {
        window.location.reload();
      });
    },
    initializeStorePickerDialog: function () {

      var me = this;

      var options = {
        elementId: 'mz-location-selector',
        body: '', //to be populated by makeLocationPickerBody
        hasXButton: true,
        width: '400px',
        scroll: true,
        bodyHeight: '600px',
        backdrop: 'static'

      };

      //Assures that each store select button has the right behavior
      $('#mz-location-selector').on('click', '.mz-store-select-button', function () {
        me.assignPickupLocation($(this).attr('mz-store-select-data'));
      });

      //Assures that the radio buttons reflect the accurate fulfillment method
      //if the dialog is closed before a store is picked.

      $('.modal-header').on('click', '.close', function () {
        var cartModelItems = window.cartView.cartView.model.get('items');
        var cartItemId = $(this).parent().parent().find('.modal-body').attr('mz-cart-item');
        var cartItem = me.model.get('items').get(cartItemId);
        me.render();
      });

      return modalDialog.init(options);

    },
    changeStore: function (e) {
      //click handler for change store link.launches store picker
      var cartItemId = $(e.currentTarget).data('mz-cart-item');
      var cartItem = this.model.get('items').get(cartItemId);
      var productCode = cartItem.apiModel.data.product.variationProductCode || cartItem.apiModel.data.product.productCode;
      this.setPickStore(productCode, cartItemId);
    },
    setPickStore: function (productCode, cartItemId,fulfillmentChange) {
      var self = this,
        preferredStore = $.parseJSON(localStorage.getItem('preferredStore')),
        cartItem = this.model.get('items').get(cartItemId),
        oldFulfillmentMethod = cartItem.get('fulfillmentMethod'),
        oldPickupLocation = cartItem.get('fulfillmentLocationName'),
        oldLocationCode = cartItem.get('fulfillmentLocationCode');

      cartItem.set('fulfillmentMethod', 'Pickup');
      cartItem.set('fulfillmentLocationName', preferredStore.name);
      cartItem.set('fulfillmentLocationCode', preferredStore.code);
      var validationData = self.validateQuantity(fetchedItemInventories, cartItem,true,fulfillmentChange);
      if (!validationData.isQuantityValid) {
        cartItem.set('quantity', validationData.availableWarehouseInventory.bopisInventory);
      }
      cartItem.apiUpdate().then(function (success) {
        self.updateDataLayer(cartItem, cartItem.get('quantity'));
      }, function (error) {
        cartItem.set('fulfillmentMethod', oldFulfillmentMethod);
        cartItem.set('fulfillmentLocationName', oldPickupLocation);
        cartItem.set('fulfillmentLocationCode', oldLocationCode);
        self.validateQuantity(fetchedItemInventories, cartItem,true,fulfillmentChange);
        self.render();
      });
    },
    pickStore: function (productCode, cartItemId) {
      /*
       Parent function for switching from ship to pickup from within cart
       or choosing a new pickup location from within cart. Runs a set of api
       calls using the cartItemId and that item's product code to get
       necessary inventory information and display a dialog containing that
       information.
       */
      var me = this;
      var listOfLocations = [];

      //before we get inventory data, we'll see if it's cached

      var filtered = this.fulfillmentInfoCache.filter(function (item) {
        return item.cartItemId == cartItemId;
      });
      var cachedItemInvData;

      if (filtered.length !== 0) {
        cachedItemInvData = filtered[0];
      } else {
        //NGCOM-344
        //If the filtered array is empty, it means the item we're checkoutSettings
        // was added to the cart some time after page load, probably during a BOGO
        //sale re-rendering.
        //Let's go ahead and add it to the cache, then stick it in our
        //cachedItemInvData variable.
        var newCacheData = {
          cartItemId: cartItemId,
          locations: []
        };
        me.fulfillmentInfoCache.push(newCacheData);
        cachedItemInvData = newCacheData;
      }

      var index = this.fulfillmentInfoCache.indexOf(cachedItemInvData);

      if (cachedItemInvData.locations.length === 0) {
        //The cache doesn't contain any data about the fulfillment
        //locations for this item. We'll do api calls to get that data
        //and update the cache.

        me.getInventoryData(cartItemId, productCode).then(function (inv) {
          if (inv.totalCount === 0) {
            //Something went wrong with getting inventory data.
            var $bodyElement = $('#mz-location-selector').find('.modal-body');
            me.pickerDialog.setBody(Hypr.getLabel('noNearbyLocationsProd'));
            $bodyElement.attr('mz-cart-item', cartItemId);
            me.pickerDialog.show();

          } else {
            //TO-DO: Make 1 call with GetLocations
            var invItemsLength = inv.items.length;
            inv.items.forEach(function (invItem, i) {
              me.handleInventoryData(invItem).then(function (handled) {
                  listOfLocations.push(handled);
                  me.fulfillmentInfoCache[index].locations.push({
                    name: handled.data.name,
                    code: handled.data.code,
                    locationData: handled,
                    inventoryData: invItem
                  });
                  me.model.get('storeLocationsCache').addLocation(handled.data);

                  if (i == invItemsLength - 1) {
                    //We're in the midst of asynchrony, but we want this dialog
                    //to go ahead and open right away if we're at the end of the
                    //for loop.
                    var $bodyElement = $('#mz-location-selector').find('.modal-body');
                    me.pickerDialog.setBody(me.makeLocationPickerBody(listOfLocations, inv.items, cartItemId));
                    $bodyElement.attr('mz-cart-item', cartItemId);
                    me.pickerDialog.show();
                  }
                },
                function (error) {
                  //NGCOM-337
                  //If the item had inventory information for a location that
                  //doesn't exist anymore or was disabled, we end up here.
                  //The only reason we would need to take any action here is if
                  //the errored location happened to be at the end of the list,
                  //and the above if statement gets skipped -
                  //We need to make sure the dialog gets opened anyways.
                  if (i == invItemsLength - 1) {
                    var $bodyElement = $('#mz-location-selector').find('.modal-body');
                    me.pickerDialog.setBody(me.makeLocationPickerBody(listOfLocations, inv.items, cartItemId));
                    $bodyElement.attr('mz-cart-item', cartItemId);
                    me.pickerDialog.show();
                  }

                });
            });
          }
        });

      } else {
        //This is information we've retrieved once since page load!
        //So we're skipping the API calls.
        var inventoryItems = [];
        this.fulfillmentInfoCache[index].locations.forEach(function (location) {
          listOfLocations.push(location.locationData);
          inventoryItems.push(location.inventoryData);
        });
        var $bodyElement = $('#mz-location-selector').find('.modal-body');
        me.pickerDialog.setBody(me.makeLocationPickerBody(listOfLocations, inventoryItems, cartItemId));
        me.pickerDialog.show();
      }

    },
    getInventoryData: function (id, productCode) {
      //Gets basic inventory data based on product code.
      return window.cartView.cartView.model.get('items').get(id).get('product').apiGetInventory({
        productCode: productCode
      });
    },
    handleInventoryData: function (invItem) {
      //Uses limited inventory location from product to get inventory names.
      return api.get('location', invItem.locationCode);
    },
    changeFulfillmentMethod: function (e) {
      //Called when a radio button is clicked. 
      var me = this;
      fulFillmentChangeMethod = true;
      var $radioButton = $(e.currentTarget),
      cartItemId = $radioButton.data('mz-cart-item'),
      shipItems = me.getShipItems(),
      value = $radioButton.val();
      if(!isFirstTime || shipItems.length === 1  || $.cookie('fulFillmentChanged') === "true" || value === 'Pickup'){
        var cartItem = this.model.get('items').get(cartItemId),
        fulfillmentChange = true;

        if (cartItem.get('fulfillmentMethod') == value) {
          //The user clicked the radio button for the fulfillment type that
          //was already selected so we can just quit.
          return 0;
        }

        if (value == 'Ship') {
          var oldFulfillmentMethod = cartItem.get('fulfillmentMethod');
          var oldPickupLocation = cartItem.get('fulfillmentLocationName');
          var oldLocationCode = cartItem.get('fulfillmentLocationCode');

          cartItem.set('fulfillmentMethod', value);
          cartItem.set('fulfillmentLocationName', '');
          cartItem.set('fulfillmentLocationCode', '');
          var validationData = me.validateQuantity(fetchedItemInventories, cartItem ,true,fulfillmentChange);
          if (!validationData.isQuantityValid) {
            cartItem.set('quantity', validationData.availableWarehouseInventory.sthMaxInventory);
            showMessage = true;
            me.model.set("sthQuantityUpdatedMsg",true);
            me.model.set("showFulfillmentMsg",false);
            me.model.set("itemsWithLessInventory",cartItem.attributes.product.get('productCode'));
          }
          cartItem.apiUpdate().then(function (success) {
            var customerPostalCode = me.model.get('sthfResponse').postalCode;
            me.getAndSetShippingTimelines(customerPostalCode, fetchedItemInventories);
            me.updateDataLayer(cartItem, cartItem.get('quantity'));
          }, function (error) {
            cartItem.set('fulfillmentMethod', oldFulfillmentMethod);
            cartItem.set('fulfillmentLocationName', oldPickupLocation);
            cartItem.set('fulfillmentLocationCode', oldLocationCode);
            me.validateQuantity(fetchedItemInventories, cartItem,true,fulfillmentChange);
          });

          if(me.model.attributes.sthfResponse && me.model.attributes.sthfResponse.postalCode && me.model.get('setViaPreferredStore')){
            var PostalCodeView = new postalCodeView({
              el: $("#postalCodePopupModalContainer"),
              model: new Backbone.MozuModel()
            });
            PostalCodeView.render();
            $('#postalCodePopupModalContainer').modal('show');
          }
        } else if (value == 'Pickup') {
          //first we get the correct product code for this item.
          //If the product is a variation, we want to pass that when searching for inventory.
          var productCode = cartItem.apiModel.data.product.variationProductCode || cartItem.apiModel.data.product.productCode;
          //pickStore function makes api calls, then builds/launches modal dialog
          this.setPickStore(productCode, cartItemId,fulfillmentChange);
          showMessage = false;
          if (ua.indexOf('chrome') > -1) {
            if (/edg|edge/i.test(ua)) {
              $.cookie('fulFillmentChanged', true, { path: '/' });
            } else {
              document.cookie = "fulFillmentChanged=" + true + ";path=/;Secure;SameSite=None";
            }
          } else {
            $.cookie('fulFillmentChanged', true, { path: '/' });
          }
          isFirstTime = false;
        }
      }else{
        this.changeFullFillmentPostal(fulFillmentChangeMethod);
        if (ua.indexOf('chrome') > -1) {
          if (/edg|edge/i.test(ua)) {
            $.cookie('fulFillmentChanged', true, { path: '/' });
          } else {
            document.cookie = "fulFillmentChanged=" + true + ";path=/;Secure;SameSite=None";
          }
        } else {
          $.cookie('fulFillmentChanged', true, { path: '/' });
        }
        isFirstTime = false;
      }
      me.calculateEhfTotal(false);
      me.render();
    },
    changeFullFillmentPostal: function(method){
      var self = this,
      fulFillmentSteps = self.changeFullFillmentSteps(method);
      api.steps(fulFillmentSteps).then(function () {
        self.model.apiGet().then(function(res){
          _.each(self.model.get('items').models,function(cartItem){
            var validationData = self.validateQuantity(fetchedItemInventories, cartItem ,true,true);
            if (!validationData.isQuantityValid) {
              if(cartItem.get('fulfillmentMethod') === 'Ship'){
                cartItem.set('quantity', validationData.availableWarehouseInventory.sthMaxInventory);
              }else{
                cartItem.set('quantity', validationData.availableWarehouseInventory.bopisInventory);
              }
            }
          });
          api.steps(self.getUpdateQuantitySteps()).then(function(){
           self.model.apiGet().then(function(){
            $('#content-loading').hide();
            if(method){
              showMessage = true;
              var customerPostalCode = self.model.get('sthfResponse').postalCode;
              self.getAndSetShippingTimelines(customerPostalCode, fetchedItemInventories);
           }
            self.render();
           }, function (error) {
            $('#content-loading').hide();
          });
         });
        });
      }, function () {
        self.render();
      });   
   },
   getUpdateQuantitySteps: function(){
     var self = this;
      var tasks = self.model.get("items").models.map(function (cartItem) {
        return function () {
          return cartItem.apiUpdateQuantity(cartItem.get('quantity')).then(function (item) {
            self.updateDataLayer(cartItem, cartItem.get('quantity'));
          });
        };
      });
      return tasks;
   },
   changeFullFillmentSteps: function (method) {
      var me = this,
      items = me.model.get("items").models,
      tasks = [],
      preferredStore = localStorage.getItem('preferredStore') ? $.parseJSON(localStorage.getItem('preferredStore')):'';
      if(!method){
      _.each(items,function (cartItem) {
          if(cartItem.get("fulfillmentMethod") === "Ship"){
            cartItem.set('fulfillmentMethod', 'Pickup');
            cartItem.set('fulfillmentLocationName', preferredStore.name);
            cartItem.set('fulfillmentLocationCode', preferredStore.code);
            var validationData = me.validateQuantity(fetchedItemInventories, cartItem ,true,true);
            if(cartItem.get('fulfillmentMethod') === 'Pickup' && validationData.availableWarehouseInventory.bopisInventory < 1){
              me.removeItem(cartItem);
              me.refreshCart();
            }
            if(validationData.availableWarehouseInventory.bopisInventory > 0){
              tasks.push(function () {
                return cartItem.apiUpdate();
              });
            }
          }
        });
      }else if(method && isFirstTime){
        _.each(items,function (cartItem) {
            var productProperties = cartItem.attributes.product.get('properties') && cartItem.attributes.product.get('properties').length ? cartItem.attributes.product.get('properties') : [],
            isSThAttrPresent =  productProperties.length ? _.findWhere(productProperties, {'attributeFQN': Hypr.getThemeSetting('shipToHomeEnableAttrName')}) : false,
            isProductEnabledForSTH = isSThAttrPresent ? isSThAttrPresent.values[0].value : false;
            
            if(cartItem.get("fulfillmentMethod") === "Pickup" && cartItem.get('availableWarehouseInventory') > 0 && isProductEnabledForSTH){
              cartItem.set('fulfillmentMethod', 'Ship');
              $('#content-loading').show();
              cartItem.set('fulfillmentLocationCode', "");
              if(cartItem.get('fulfillmentMethod') === 'Ship'){
                if( cartItem.get('quantity') >= cartItem.get('availableWarehouseInventory')){
                  sthItems.push(cartItem.attributes.product.get('productCode'));
                  me.model.set("sthQuantityUpdatedMsg",true);
                }
                if(!me.model.get("sthQuantityUpdatedMsg")){
                  me.model.set("sthQuantityUpdatedMsg",false);
                }
                me.model.set("showFulfillmentMsg",true);
              }
              tasks.push(function () {
                return cartItem.apiUpdate();
              });
              me.model.set("itemsWithLessInventory",sthItems);
            }
          });
        }
        return tasks;
    },
    updatePostalCode: function(){
      var me = this;
      var postalCode = me.$el.find($(".btnPostalCode")).val().trim().replace(/ /g, "").toUpperCase(),
      postalCodeChecker = new PostalCodeChecker();
      me.model.set("postalCode",postalCode);
      me.$el.find('.btnPostalCodeUpdate').addClass('isLoading');

      postalCodeChecker.checkPostalCode(postalCode).then(function(){
        me.model.set('isPostalCodeValid', true);
        me.model.set('sthfResponse', {postalCode: postalCode, status: 'Valid'});
        me.model.set("setViaPreferredStore",false);
        me.model.unset("errorCase");
        me.getAndSetShippingTimelines(postalCode, fetchedItemInventories);
        me.getEhfTemplateData(postalCode);
      },function(error){
        me.model.set("invalidUpdatedPostalCode",true);
        me.model.set("postalCode",postalCode);
        me.errorHandler(error, postalCode);
      });
    },
    errorHandler: function (error, postalCode) {
      var errorMessage = error;
      this.model.set('isPostalCodeValid', false);
      switch (errorMessage) {
          case 'Invalid Postal Code':
              this.model.set("errorCase", "invalidFormat");
              this.model.set('sthfResponse', {postalCode: postalCode, status:  this.model.get('sthfResponse').status });
              break;
          case 'Postal code not allowed':
              this.model.set("errorCase", "notAllowed");
              this.model.set('sthfResponse', {postalCode: postalCode, status: 'Invalid'});
              this.changeFullFillmentPostal(false);
              break;
          case 'Error while checking postal code':
              this.model.set("errorCase", "serverError");
              break;
      }
      this.render();
    },
    makeLocationPickerBody: function (locationList, locationInventoryInfo, cartItemId) {
      /*
       Uses a list of locations to build HTML to stick into the
       location picker. cartItemId is added as an attribute to each select
       button so that it can be used to assign the new pickup location to the
       right cart item.

       locationList should be a list of fulfillment locations with complete
       location data (what we need is the name). locationInventoryInfo will
       contain stock levels for the current product(cartItemId) by location code.

       */

      var me = this;

      var body = '';
      locationList.forEach(function (location) {
        //We find the inventory data that matches the location we're focusing on.
        var matchedInventory = locationInventoryInfo.filter(function (locationInventory) {
          return locationInventory.locationCode == location.data.code;
        });
        //matchedInventory should be a list of one item.

        var stockLevel = matchedInventory[0].stockAvailable;
        var allowsBackorder = location.data.allowFulfillmentWithNoStock;

        //Piece together UI for a single location listing
        var locationSelectDiv = $('<div>', {
          'class': 'location-select-option',
          'style': 'display:flex',
          'data-mz-cart-item': cartItemId
        });
        var leftSideDiv = $('<div>', { 'style': 'flex:1' });
        var rightSideDiv = $('<div>', { 'style': 'flex:1' });
        leftSideDiv.append('<h4 style="margin: 6.25px 0 6.25px">' + location.data.name + '</h4>');
        //If there is enough stock or the store allows backorder,
        //we'll let the user click the select button.
        //Even if these two conditions are met, the user could still be
        //halted upon trying to proceed to checkout if
        //the product isn't configured to allow for backorder.

        var address = location.data.address;

        leftSideDiv.append($('<div>' + address.address1 + '</div>'));
        if (address.address2) {
          leftSideDiv.append($('<div>' + address.address2 + '</div>'));
        }
        if (address.address3) {
          leftSideDiv.append($('<div>' + address.address3 + '</div>'));
        }
        if (address.address4) {
          leftSideDiv.append($('<div>' + address.address4 + '</div>'));
        }
        leftSideDiv.append($('<div>' + address.cityOrTown + ', ' + address.stateOrProvince + ' ' + address.postalOrZipCode + '</div>'));
        var $selectButton;
        if (stockLevel > 0 || allowsBackorder) {
          leftSideDiv.append('<p class=\'mz-locationselect-available\'>' + Hypr.getLabel('availableNow') + '</p>');
          var buttonData = {
            locationCode: location.data.code,
            locationName: location.data.name,
            cartItemId: cartItemId
          };

          $selectButton = $('<button>', {
            'type': 'button',
            'class': 'mz-button mz-store-select-button',
            'style': 'margin:25% 0 0 25%',
            'aria-hidden': 'true',
            'mz-store-select-data': JSON.stringify(buttonData)
          });
          $selectButton.text(Hypr.getLabel('selectStore'));
          rightSideDiv.append($selectButton);

        } else {
          leftSideDiv.append('<p class=\'mz-locationselect-unavailable\'>' + Hypr.getLabel('outOfStock') + '</p>');
          $selectButton = $('<button>', {
            'type': 'button',
            'class': 'mz-button is-disabled mz-store-select-button',
            'aria-hidden': 'true',
            'disabled': 'disabled',
            'style': 'margin:25% 0 0 25%'
          });
          $selectButton.text(Hypr.getLabel('selectStore'));
          rightSideDiv.append($selectButton);
        }

        locationSelectDiv.append(leftSideDiv);
        locationSelectDiv.append(rightSideDiv);
        body += locationSelectDiv.prop('outerHTML');

      });

      return body;

    },
    assignPickupLocation: function (jsonStoreSelectData) {
      //called by Select Store button from store picker dialog.
      //Makes the actual change to the item using data held by the button
      //in the store picker.
      var me = this;
      this.pickerDialog.hide();

      var storeSelectData = JSON.parse(jsonStoreSelectData);
      var cartItem = this.model.get('items').get(storeSelectData.cartItemId);
      //in case there is an error with the api call, we want to get all of the
      //current data for the cartItem before we change it so that we can
      //change it back if we need to.
      var oldFulfillmentMethod = cartItem.get('fulfillmentMethod');
      var oldPickupLocation = cartItem.get('fulfillmentLocationName');
      var oldLocationCode = cartItem.get('fulfillmentLocationCode');

      cartItem.set('fulfillmentMethod', 'Pickup');
      cartItem.set('fulfillmentLocationName', storeSelectData.locationName);
      cartItem.set('fulfillmentLocationCode', storeSelectData.locationCode);
      cartItem.apiUpdate().then(function (success) {
      }, function (error) {
        cartItem.set('fulfillmentMethod', oldFulfillmentMethod);
        cartItem.set('fulfillmentLocationName', oldPickupLocation);
        cartItem.set('fulfillmentLocationCode', oldLocationCode);
        me.render();
      });

    },
    proceedToCheckout: function () {
      //commenting  for ssl for now...
      //this.model.toOrder();
      // return false;
      var userConfirmationRequired = $.cookie("preferredStore_userConfirmationRequired") ? JSON.parse($.cookie("preferredStore_userConfirmationRequired")) : false;   
      if(window.field != "true" && !userConfirmationRequired && this.model.get('orderType') === "mixOrder"){
          this.showConfirmSplitOrderPopup();
      } else if(userConfirmationRequired && this.model.get('orderType') === "shipToStoreOrder") {
        var StoreConfirmationView = new storeConfirmationView({
          el: $("#storeConfirmationPopupModalContainer"),
          model: new Backbone.MozuModel()
        });
        StoreConfirmationView.render();
        $('#storeConfirmationPopupModalContainer').modal('show');
        return false;
      } else if(userConfirmationRequired && this.model.get('orderType') === "mixOrder") {
        this.showConfirmStoreAndSplitOrderPopup();
      } else {
        $(".checkoutBtnSection").addClass("is-loading");
          this.setShippingCookie();
          this.model.isLoading(true);
      }
      // the rest is done through a regular HTTP POST
    },
    // Set cookie to get shipping cost on checkout for prepopulate
    setShippingCookie: function(){
      var expiryDate = new Date();
      expiryDate.setYear(expiryDate.getFullYear() + 1);
      $.cookie("shippingCost", JSON.stringify({"shippingCost":this.model.get("shippingCost")}), { path: "/", expires: expiryDate });
    },
    addCoupon: function () {
      var self = this;
      this.model.addCoupon().ensure(function () {
        self.model.unset('couponCode');
        self.appliedCouponCodes();
        self.render();
      });
    },
    appliedCouponCodes: function () {
      var cartItems = this.model.get('items').models;
      //To make unique applied coupon codes array
      _.each(cartItems, function (item) {
        var productDiscount = item.get('productDiscount');
        if (productDiscount && !productDiscount.excluded && productDiscount.couponCode) {
          appliedCouponCodes.push(productDiscount.couponCode);
        }
      });
      appliedCouponCodes = _.uniq(appliedCouponCodes);
      this.model.set('appliedCouponCodes', appliedCouponCodes);
    },
    removeCoupon: function (e) {
      var couponCode = $(e.currentTarget).attr('data-mz-value');
      var self = this;
      this.model.removeCoupon(couponCode).ensure(function () {
        appliedCouponCodes = _.without(appliedCouponCodes, couponCode);
        self.model.set('appliedCouponCodes', appliedCouponCodes);
        self.render();
      });
    },
    onEnterCouponCode: function (model, code) {
      if (code && !this.codeEntered) {
        this.codeEntered = true;
        this.$el.find('#cart-coupon-code').prop('disabled', false);
      }
      if (!code && this.codeEntered) {
        this.codeEntered = false;
        this.$el.find('#cart-coupon-code').prop('disabled', true);
      }
    },
    autoUpdate: [
      'couponCode'
    ],
    handleEnterKey: function () {
      this.addCoupon();
    },
    guestCheckoutLogin: function () {
       /**
       * Below code checks for 'preferredStore_userConfirmationRequired' flag which comes through arc
       * If this flag is there and its true then we show the popup and if false then we help user to continue checkout process.
       * Ticket - Main Ticket - 1202, Sub ticket - IWM-1471
       * All based on the cookie which is set through Arc (IGN_HH_IP_BASED_STORE_LOCATOR.1.0.0)
       * @Date - 19/04/2021
	    */
        var userConfirmationRequired = $.cookie("preferredStore_userConfirmationRequired") ? JSON.parse($.cookie("preferredStore_userConfirmationRequired")) : false;
        var locale = require.mozuData('apicontext').headers['x-vol-locale'];
        locale = locale.split('-')[0];
        var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
        var currentLocale = '';
        if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite) {
          currentLocale = locale === 'fr' ? '/fr' : '/en';
        }
        this.setShippingCookie();
        if(!userConfirmationRequired && this.model.get('orderType') === "mixOrder"){
          this.showConfirmSplitOrderPopup();
        } else if(userConfirmationRequired && this.model.get('orderType') === "shipToStoreOrder") {
          /**
           * Below code checks for 'preferredStore_userConfirmationRequired' flag which comes through arc
           * If this flag is there and its true then we show the popup and if false then we help user to continue checkout process.
           * Ticket - Main Ticket - 1202, Sub ticket - IWM-1471
           * All based on the cookie which is set through Arc (IGN_HH_IP_BASED_STORE_LOCATOR.1.0.0)
           * @Date - 19/04/2021
          */
          var StoreConfirmationView = new storeConfirmationView({
            el: $("#storeConfirmationPopupModalContainer"),
            model: new Backbone.MozuModel()
          });
          StoreConfirmationView.render();
          $('#storeConfirmationPopupModalContainer').modal('show');
        } else if(userConfirmationRequired && this.model.get('orderType') === "mixOrder"){
          this.showConfirmStoreAndSplitOrderPopup();
        } else{
          window.location.href = currentLocale + '/user/login?returnurl=/cart/checkout';
        }
      return false;
    },
    setPreferredStore: function () {
      var self = this;
      if(localStorage.getItem('preferredStore')){
        self.model.set({ 'preferredStore': $.parseJSON(localStorage.getItem('preferredStore')) });
      }
    },
    changePreferredStore: function (e) {
      e.preventDefault();
      var locale = require.mozuData('apicontext').headers['x-vol-locale'];
      locale = locale.split('-')[0];
      var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
      var currentLocale = '';
      if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite) {
        currentLocale = locale === 'fr' ? '/fr' : '/en';
      }
      window.location.href = currentLocale + '/store-locator?returnUrl=' + window.location.pathname;
    },
    callStore: function (e) {
      var screenWidth = window.matchMedia('(max-width: 767px)');
      if (!screenWidth.matches) {
        e.preventDefault();
      }
    },
    showConfirmSplitOrderPopup: function(){
      window.cartView.cartView.render();
      var ConfirmSplitOrderView = new confirmSplitOrderView({
        el: $('#confirmSplitOrderModalContainer'),
        model: new Backbone.MozuModel(window.cartView.cartView.model.get('items').parent.apiModel.data)
      });
      ConfirmSplitOrderView.render();
      $('#confirmSplitOrderModalContainer').modal('show');
    },
    showConfirmStoreAndSplitOrderPopup: function(){
      window.cartView.cartView.render();
      var ConfirmStoreAndSplitOrderView = new confirmStoreAndSplitOrderView({
        el: $('#confirmStoreAndSplitOrderModalContainer'),
        model: new Backbone.MozuModel(window.cartView.cartView.model.get('items').parent.apiModel.data)
      });
      ConfirmStoreAndSplitOrderView.render();
      $('#confirmStoreAndSplitOrderModalContainer').modal('show');
    },
    sendDataLayerInfo: function (removedItems, previousItems) {
      var self = this;
      var dataLayerData = [], salesForceData = [];
      _.each(removedItems, function (removedItem) {
        var currAttribute = _.findWhere(removedItem.properties, { 'attributeFQN': Hypr.getThemeSetting('brandDesc') });
        var cookieCategoryName;
        var previousQuantity = _.findWhere(previousItems, { 'productCode': removedItem.productCode });
        if ($.cookie('BreadCrumbFlow')) {
          var FlowArray = $.parseJSON($.cookie('BreadCrumbFlow'));
          var getproduct = _.findWhere(FlowArray, { 'productCode': removedItem.productCode });
          if (getproduct) {
            cookieCategoryName = getproduct.flow;
          } else {
            cookieCategoryName = '';
          }
        }
        var productData = {
          'name': removedItem.name,  // String. Product Name as captured in Kibo
          'id': removedItem.productCode, // String. Any unique identifier for a product/SKU
          'price': previousQuantity.salePrice > 0 ? parseFloat(previousQuantity.salePrice).toFixed(2).replace('$', '').replace(',', '.').trim() : parseFloat(previousQuantity.price).toFixed(2).replace('$', '').replace(',', '.').trim(), // String/Currency. Price with discount applied, if available
          'brand': currAttribute ? currAttribute.values[0].stringValue : '', //String. Refers to a product brand, all
                                                                             // caps
          'category': cookieCategoryName, // String. Refers to a Product Category.  It's possible to send up to five
                                          // levels of categories to help with segmentation in reports. These levels
                                          // are sent by using the slash (/) between levels, where level 1 is the first
                                          // item in the string, level 2 the second, etc. If a full five-level product
                                          // category string is not available, please use whatever identifier is used
                                          // by Home Hardware to identify a category
          'quantity': previousQuantity.quantity   //Integer. Quantity the product was purchased.
        };
        dataLayerData.push(productData);
      });
      setTimeout(function () {
        api.get('cart', {}).then(function (cartdata) {
          var saleforceData = [];
          _.each(cartdata.data.items, function (item) {
            saleforceData.push({
              'id': item.product.productCode, // String. Any unique identifier for a product/SKU
              'price': item.product.price.salePrice > 0 ? parseFloat(item.product.price.salePrice).toFixed(2).replace('$', '').replace(',', '.').trim() : parseFloat(item.product.price.price).toFixed(2).replace('$', '').replace(',', '.').trim(),
              'quantity': item.quantity
            });
          });
          dataLayer.push({
            'event': 'removeFromCart-switch',
            'metric4': cartdata.data.total, // String/Currency. This metric tracks total dollar value  that is in the
                                            // cart (for all products) after this product(s) removal
            'ecommerce': {
              'remove': {
                'products': dataLayerData
              }
            },
            'salesForce': {
              'products': saleforceData,
              'cartRecoveryId': cartdata.data.id
            }
          });
        });
      }, 2000);
    }
  });
  /**
       * Below code provides a store-confirmation pop-up on cart checkout. when user clicks checkout button.
       * Ticket - Main Ticket - 1202, Sub ticket - IWM-1471
       * All based on the cookie which is set through Arc (IGN_HH_IP_BASED_STORE_LOCATOR.1.0.0)
       * @Date - 19/04/2021
	*/
  var storeConfirmationView = Backbone.MozuView.extend({
    templateName: 'modules/cart/store-confirmation-modal',
    additionalEvents: {
      'click .changePreferredStore': 'changePreferredStore',
      'click #storeConfirm-GuestCheckout': 'proceedToCheckoutGuest',
      'click #storeConfirm': 'confirmStore'
    },
    proceedToCheckoutGuest: function (e) {
      $.removeCookie('preferredStore_userConfirmationRequired', { path: '/' });
      var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
      var currentLocale = '';
      var locale = require.mozuData('apicontext').headers['x-vol-locale'];
      locale = locale.split('-')[0];
      if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite) {
        currentLocale = locale === 'fr' ? '/fr' : '/en';
      }
      window.location.href = currentLocale + '/user/login?returnurl=/cart/checkout';
    },
    changePreferredStore: function(e) {
      window.cartView.cartView.changePreferredStore(e);
    },
    confirmStore: function() {
      /**
       * Below code removes a flag stored in a cookie. This cookie is set through Arc (IGN_HH_IP_BASED_STORE_LOCATOR)
       * When a user selects a store manually, we need to remove this flag, so that at cart checkout store confirmation pop-up disappears.
       * Ticket - Main Ticket - 1202, Sub ticket - IWM-1471
       * @Date - 19/04/2021
			*/
      $.removeCookie('preferredStore_userConfirmationRequired', { path: '/' });
      $('#storeConfirmationPopupModalContainer').modal('hide');
    },
    render: function () {
      var preferredStore = JSON.parse(localStorage.getItem('preferredStore'));
      this.model.set('storeAddess',preferredStore.address);
      this.model.set('phone',preferredStore.phone);
      this.model.set('name',preferredStore.name);
      Backbone.MozuView.prototype.render.apply(this, arguments);
    }
  });
  var postalCodeView = Backbone.MozuView.extend({
    templateName: 'modules/cart/postal-code-modal',
    additionalEvents: {
      'click .changePreferredStore': 'changePreferredStore',
      'click .postal-continue-btn': 'savePostalCode',
      'click .close':'modalClose'
    },  
    savePostalCode: function (e) {
      var me = this;
      var postalCode = me.$el.find($("#postal-code")).val(),
      postalCodeChecker = new PostalCodeChecker();
      me.$el.find('.postal-continue-btn').addClass('isLoading');

      postalCodeChecker.checkPostalCode(postalCode).then(function(){
        window.cartView.cartView.model.set('sthfResponse', {postalCode: postalCode, status: 'Valid'});
        me.model.set('sthfResponse', {postalCode: postalCode, status: 'Valid'});
        me.model.set('isPostalCodeValid', true);
        me.model.set("setViaPreferredStore",false);
        window.cartView.cartView.model.set('setViaPreferredStore',false);
        $('#postalCodePopupModalContainer').modal('hide');
        me.model.unset("errorCase");
        window.cartView.cartView.render();
      },function(error){
        me.errorHandler(error, postalCode);
      });
    },
    errorHandler: function (error, postalCode) {
      var errorMessage = error;
      this.model.set('isPostalCodeValid', false);
      switch (errorMessage) {
          case 'Invalid Postal Code':
              this.model.set("errorCase", "invalidFormat");
              this.model.set('sthfResponse', {postalCode: postalCode });
              break;
          case 'Postal code not allowed':
              this.model.set("errorCase", "notAllowed");
              this.model.set('sthfResponse', {postalCode: postalCode, status: 'Invalid'});
              window.cartView.cartView.model.set('sthfResponse', {postalCode: postalCode, status: 'Invalid'});
              break;
          case 'Error while checking postal code':
              this.model.set("errorCase", "serverError");
              break;
      }
      this.render();
    },
    modalClose: function(){
      var me = this;
      me.model.set("invalidPostalCode",false);
      $(".modal-backdrop").remove();
      me.render();
      $('#postalCodePopupModalContainer').modal('hide');
    },
    bindModalCloseEvent: function () {
      var self = this;
      $('#postalCodePopupModalContainer').on('hidden.bs.modal', function () {
          if(self.model.get('sthfResponse') && self.model.get('sthfResponse').status === "Invalid"){
            window.cartView.cartView.changeFullFillmentPostal();
          }
      });
    },
    render: function () {
      Backbone.MozuView.prototype.render.apply(this, arguments);
      this.bindModalCloseEvent();
    }
  });
  function renderVisaCheckout(model) {

    var visaCheckoutSettings = HyprLiveContext.locals.siteContext.checkoutSettings.visaCheckout;
    var apiKey = visaCheckoutSettings.apiKey;
    var clientId = visaCheckoutSettings.clientId;

    //In case for some reason a model is not passed
    if (!model) {
      model = CartModels.Cart.fromCurrent();
    }

    function initVisa() {
      var delay = 200;
      if (window.V) {
        window.V.init({
          apikey: apiKey,
          clientId: clientId,
          paymentRequest: {
            currencyCode: model ? model.get('currencyCode') : 'USD',
            subtotal: '' + model.get('subtotal')
          }
        });
        return;
      }
      _.delay(initVisa, delay);
    }

    initVisa();

  }

  /* begin visa checkout */
  function initVisaCheckout() {
    if (!window.V) {
      //console.warn( 'visa checkout has not been initilized properly');
      return false;
    }

    // on success, attach the encoded payment data to the window
    // then turn the cart into an order and advance to checkout
    window.V.on('payment.success', function (payment) {
      // payment here is an object, not a string. we'll stringify it later
      var $form = $('#cartform');

      _.each({

        digitalWalletData: JSON.stringify(payment),
        digitalWalletType: 'VisaCheckout'

      }, function (value, key) {

        $form.append($('<input />', {
          type: 'hidden',
          name: key,
          value: value
        }));

      });

      $form.submit();

    });
  }
  var confirmSplitOrderView = Backbone.MozuView.extend({
    templateName: 'modules/cart/confirm-split-order-modal',
    additionalEvents: {
      'click #guest-checkout-new': 'proceedToCheckoutGuest',
      'click #cart-checkout-new': 'proceedToCheckoutLoggedin',
      'click .clsoeModal':'modalClose'
    },
    proceedToCheckoutGuest: function (e) {
      var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
      var currentLocale = '';
      var locale = require.mozuData('apicontext').headers['x-vol-locale'];
      locale = locale.split('-')[0];
      if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite) {
        currentLocale = locale === 'fr' ? '/fr' : '/en';
      }
      window.location.href = currentLocale + '/user/login?returnurl=/cart/checkout';
    },
    proceedToCheckoutLoggedin: function(e){
      $('#confirmSplitOrderModalContainer').modal('hide');
      var field = $(e.target).attr('data-mz-ismodal');
      window.field = field;
      $("#cart-checkout").click();
      $(".checkoutBtnSection").addClass("is-loading");
    },
    modalClose: function(){
      var me = this;
      $(".modal-backdrop").remove();
      me.render();
      $('#confirmSplitOrderModalContainer').modal('hide');
    },
    render: function () {
      Backbone.MozuView.prototype.render.apply(this, arguments);
    }
  });

  var confirmStoreAndSplitOrderView = Backbone.MozuView.extend({
    templateName: 'modules/cart/confirm-store-and-split-order-modal',
    additionalEvents: {
      'click #guest-checkout-new': 'proceedToCheckoutGuest',
      'click #cart-checkout-new': 'proceedToCheckoutLoggedin',
      'click .changePreferredStore': 'changePreferredStore'
    },
    proceedToCheckoutGuest: function (e) {
      var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
      var currentLocale = '';
      var locale = require.mozuData('apicontext').headers['x-vol-locale'];
      locale = locale.split('-')[0];
      if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite) {
        currentLocale = locale === 'fr' ? '/fr' : '/en';
      }
      $.removeCookie('preferredStore_userConfirmationRequired', { path: '/' });
      window.location.href = currentLocale + '/user/login?returnurl=/cart/checkout';
    },
    proceedToCheckoutLoggedin: function(e){
      $('#confirmStoreAndSplitOrderModalContainer').modal('hide');
      $.removeCookie('preferredStore_userConfirmationRequired', { path: '/' });
      var field = $(e.target).attr('data-mz-ismodal');
      window.field = field;
      $("#cart-checkout").click();
      $(".checkoutBtnSection").addClass("is-loading");
    }, 
    changePreferredStore: function(e) {
      window.cartView.cartView.changePreferredStore(e);
    },
    render: function () {
      var preferredStore = JSON.parse(localStorage.getItem('preferredStore'));
      this.model.set('storeAddess',preferredStore.address);
      this.model.set('phone',preferredStore.phone);
      this.model.set('name',preferredStore.name);
      Backbone.MozuView.prototype.render.apply(this, arguments);
      $('.ship-to-store-item, ship-to-home-item').hide();
      $('.ship-to-store-item').hide();
      var numOfStoreItems = $('.ship-to-store-item').length;
      if (numOfStoreItems > 2) {
        var displayStoreCount = numOfStoreItems - 2;
        $('.pickup-instore-row').find('.ship-to-store-item:lt(2)').show();
        $('.ShowMoreStoreItem').html(Hypr.getLabel("view")+' '+displayStoreCount+' '+Hypr.getLabel("more")+' <i class="far fa-caret-circle-down"></i>');
        $(".ShowLessStoreItem").hide();

        $('.ShowMoreStoreItem').click(function(ev) {
          $(ev.currentTarget).parent().find('.ship-to-store-item').show();
          $(".ShowMoreStoreItem").hide();
          $(".ShowLessStoreItem").show();
          $('.ShowLessStoreItem').html(Hypr.getLabel("viewLessText")+' <i class="far fa-caret-circle-up"></i>');
        });

        $('.ShowLessStoreItem').click(function(ev) {
          $(ev.currentTarget).parent().find('.ship-to-store-item').not(':lt(2)').hide();
          $(".ShowLessStoreItem").hide();
          $(".ShowMoreStoreItem").show();
          $('.ShowMoreStoreItem').html(Hypr.getLabel("view")+' '+displayStoreCount+' '+Hypr.getLabel("more")+' <i class="far fa-caret-circle-down"></i>');
        });
      } else {
        $('.ship-to-store-item').show();
        $(".ShowMoreStoreItem, .ShowLessStoreItem").hide();
      }
      $('.ship-to-home-item').hide();
      var numOfHomeItems = $('.ship-to-home-item').length;
      if (numOfHomeItems > 2) {
        var displayHomeCount = numOfHomeItems - 2;
        $('.ship-to-home-row').find('.ship-to-home-item:lt(2)').show();
        $('.ShowMoreHomeItem').html(Hypr.getLabel("view")+' '+displayHomeCount+' '+Hypr.getLabel("more")+' <i class="far fa-caret-circle-down"></i>');
        $(".ShowLessHomeItem").hide();

        $('.ShowMoreHomeItem').click(function(ev) {
          $(ev.currentTarget).parent().find('.ship-to-home-item').show();
          $(".ShowMoreHomeItem").hide();
          $(".ShowLessHomeItem").show();
          $('.ShowLessHomeItem').html(Hypr.getLabel("viewLessText")+' <i class="far fa-caret-circle-up"></i>');
        });

        $('.ShowLessHomeItem').click(function(ev) {
          $(ev.currentTarget).parent().find('.ship-to-home-item').not(':lt(2)').hide();
          $(".ShowLessHomeItem").hide();
          $(".ShowMoreHomeItem").show();
          $('.ShowMoreHomeItem').html(Hypr.getLabel("view")+' '+displayHomeCount+' '+Hypr.getLabel("more")+' <i class="far fa-caret-circle-down"></i>');
        });
      } else {
        $('.ship-to-home-item').show();
        $(".ShowMoreHomeItem, .ShowLessHomeItem").hide();
      }
    }
  });
  /* end visa checkout */

  $(document).ready(function () {
    var cartModel = CartModels.Cart.fromCurrent(),
      cartViews = {

        cartView: new CartView({
          el: $('#cart'),
          model: cartModel,
          messagesEl: $('[data-mz-message-bar]')
        })

      };

    cartModel.on('ordercreated', function (order) {
      cartModel.isLoading(true);
      window.location = (HyprLiveContext.locals.siteContext.siteSubdirectory || '') + '/checkout/' + order.prop('id');
    });

    cartModel.on('sync', function () {
      CartMonitor.setCount(cartModel.get('count'));
    });

    window.cartView = cartViews;

    CartMonitor.setCount(cartModel.get('count'));
    cartViews.cartView.setPreferredStore();
    _.invoke(cartViews, 'render');

    renderVisaCheckout(cartModel);

    _.each(cartModel.get('items').models, function (item) {
      var map = {};
      map.item = item.get('id');
      map.qty = item.get('quantity');
      itemQtyList.push(map);
    });

    setTimeout(function () {
      $(document).on('click', '.data-layer-productClick', function (e) {
        e.stopPropagation();
        var breadcrumbStr = '';
        var cookieCategoryArray = [];
        var clickedProductCode = $(e.currentTarget).closest('.datalayer-parent-element').attr('data-floodlight-product-code');
        var actionFieldList = $(e.currentTarget).attr('href').split('?page=')[1];
        var getPrice = $(e.currentTarget).closest('.datalayer-parent-element').attr('data-floodlight-product-sale-price') ? $(e.currentTarget).closest('.datalayer-parent-element').attr('data-floodlight-product-sale-price') : $(e.currentTarget).closest('.datalayer-parent-element').attr('data-floodlight-product-price');
        var getBrand = $(e.currentTarget).closest('.datalayer-parent-element').find('.product-tile-brand').text();
        if ($.cookie('BreadCrumbFlow')) {
          cookieCategoryArray = $.parseJSON($.cookie('BreadCrumbFlow'));
          var Prod = _.findWhere(cookieCategoryArray, { 'productCode': clickedProductCode });
          breadcrumbStr = Prod ? Prod.flow : '';
        }
        if (!breadcrumbStr) {
          breadcrumbStr = '';
          var currentParentId = -1;
                    
          if(e.currentTarget && $(e.currentTarget).closest("[data-mz-categoryId]").length > 0){
              var dataCategoryID = $(e.currentTarget).closest("[data-mz-categoryId]").attr('data-mz-categoryId');

              if(dataCategoryID){
                  currentParentId = parseInt(dataCategoryID.split('|')[1], 10);
              }
          }

          if(currentParentId && currentParentId > 0){
              window.getBreadcrumbflow(currentParentId, window.allCategories, breadcrumbStr, e);
          }
        } else {
          if (actionFieldList) {
            if (actionFieldList.indexOf('&rrec=true') !== -1) {
              actionFieldList = actionFieldList.split('&rrec=true')[0];
            }
          }
          dataLayer.push({
            'event': 'productClick',
            'ecommerce': {
              'click': {
                'actionField': { 'list': actionFieldList },
                'products': [{
                  'name': $.trim($(e.currentTarget).closest('.datalayer-parent-element').attr('data-floodlight-product-name')),
                  'id': $.trim($(e.currentTarget).closest('.datalayer-parent-element').attr('data-floodlight-product-code')),
                  'price': $.trim(getPrice) ? $.trim(getPrice.replace('$', '').replace(',', '.')) : '',
                  'brand': $.trim(getBrand) ? $.trim(getBrand) : '',
                  'category': breadcrumbStr,
                  'position': $(e.currentTarget).closest('.datalayer-parent-element').attr('data-mz-position')
                }]
              }
            }
          });
        }
      });
    }, 2000);
    $(window).scroll(function() {
      if(navigator.userAgent.match(/(iPhone)|(iPod)|(android)|(webOS)/i)){
        var scrollPosition = $(window).scrollTop();
        if(scrollPosition >= 160){
          $(".cart-checkout-action").find($(".checkoutBtnSection")).addClass("fixedCheckoutBtn");
        }
        if(scrollPosition  > 90) {
          $("#header-notifications-cart").addClass('sticky');
        } else{
          $("#header-notifications-cart").removeClass('sticky');
        }
      }else{
        var height = $(window).scrollTop();
        if(height  > 173) {
          $("#header-notifications-cart").addClass('sticky');
        } else{
          $("#header-notifications-cart").removeClass('sticky');
        }
      }
    });
  });
});