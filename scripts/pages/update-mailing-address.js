require(["modules/jquery-mozu", 
	"underscore", 
	"hyprlive", 
	"modules/backbone-mozu", 
	'modules/api',
	'hyprlivecontext'], 
	function ($, _, Hypr, Backbone, api, HyprLiveContext) {
	var clearFields = function(){
		$("#idCode, #oldFirstName, #oldLastName, #oldAddress, #oldCity, #oldPostalCode, #newFirstName, #newLastName, #newAddress, #newCity, #newPostalCode, #emailAddress").val('');
		$("#specialoffers").attr("checked", false);
		$("#oldProvince, #newProvince").val('0');
	};
	$(document).ready(function() {	
		var emailReg = Backbone.Validation.patterns.email;
		$('#update-mailing-address-button').on('click',function(){
			var errors = false;
			if (!$("#oldFirstName").val() || $("#oldFirstName").val() === ""){
				$("#oldFirstName").addClass('is-invalid');
				$('[data-mz-validationmessage-for="oldFirstName"]').text(Hypr.getLabel('fnameMissing'));
				errors = true;
			}else{
				$("#oldFirstName").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="oldFirstName"]').text('');
			}
			
			if (!$("#oldLastName").val() || $("#oldLastName").val() === ""){
				$("#oldLastName").addClass('is-invalid');
				$('[data-mz-validationmessage-for="oldLastName"]').text(Hypr.getLabel('lnameMissing'));
				errors = true;
			}else{
				$("#oldLastName").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="oldLastName"]').text('');
			}
			if (!$("#oldAddress").val() || $("#oldAddress").val() === ""){
				$("#oldAddress").addClass('is-invalid');
				$('[data-mz-validationmessage-for="oldAddress"]').text(Hypr.getLabel('addressMissing'));
				errors = true;
			}else{
				$("#oldAddress").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="oldAddress"]').text('');
			}

			if (!$("#oldCity").val() || $("#oldCity").val() === ""){
				$("#oldCity").addClass('is-invalid');
				$('[data-mz-validationmessage-for="oldCity"]').text(Hypr.getLabel('cityMissing'));
				errors = true;
			}else{
				$("#oldCity").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="oldCity"]').text('');
			}
			if (!$("#oldPostalCode").val() || $("#oldPostalCode").val() === ""){
				$("#oldPostalCode").addClass('is-invalid');
				$('[data-mz-validationmessage-for="oldPostalCode"]').text(Hypr.getLabel('postalCodeMissing'));
				errors = true;
			}else{
				$("#oldPostalCode").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="oldPostalCode"]').text('');
			}
			
			var oldProvince = $('#oldProvince option:selected').val();
			if(!oldProvince || oldProvince === ""){
				$("#oldProvince").addClass('is-invalid');
				$('[data-mz-validationmessage-for="oldProvince"]').text(Hypr.getLabel('provinceMissing'));
				errors = true;
			}else{
				$("#oldProvince").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="oldProvince"]').text('');
			}
			
			
			if (!$("#newFirstName").val() || $("#newFirstName").val() === ""){
				$("#newFirstName").addClass('is-invalid');
				$('[data-mz-validationmessage-for="newFirstName"]').text(Hypr.getLabel('fnameMissing'));
				errors = true;
			}else{
				$("#newFirstName").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="newFirstName"]').text('');
			}
			
			if (!$("#newLastName").val() || $("#newLastName").val() === ""){
				$("#newLastName").addClass('is-invalid');
				$('[data-mz-validationmessage-for="newLastName"]').text(Hypr.getLabel('lnameMissing'));
				errors = true;
			}else{
				$("#newLastName").removeClass('is-invalid');
				$	('[data-mz-validationmessage-for="newLastName"]').text('');
			}
			if (!$("#newAddress").val() || $("#newAddress").val() === ""){
				$("#newAddress").addClass('is-invalid');
				$('[data-mz-validationmessage-for="newAddress"]').text(Hypr.getLabel('addressMissing'));
				errors = true;
			}else{
				$("#newAddress").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="newAddress"]').text('');
			}

			if (!$("#newCity").val() || $("#newCity").val() === ""){
				$("#newCity").addClass('is-invalid');
				$('[data-mz-validationmessage-for="newCity"]').text(Hypr.getLabel('cityMissing'));
				errors = true;
			}else{
				$("#newCity").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="newCity"]').text('');
			}
			if (!$("#newPostalCode").val() || $("#newPostalCode").val() === ""){
				$("#newPostalCode").addClass('is-invalid');
				$('[data-mz-validationmessage-for="newPostalCode"]').text(Hypr.getLabel('postalCodeMissing'));
				errors = true;
			}else{
				$("#newPostalCode").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="newPostalCode"]').text('');
			}
			
			var newProvince = $('#newProvince option:selected').val();
			if(!newProvince || newProvince === ""){
				$("#newProvince").addClass('is-invalid');
				$('[data-mz-validationmessage-for="newProvince"]').text(Hypr.getLabel('provinceMissing'));
				errors = true;
			}else{
				$("#newProvince").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="newProvince"]').text('');
			}
			
			
			if($("#emailAddress").val() === ""){
				$("#emailAddress").addClass('is-invalid');
				$('[data-mz-validationmessage-for="emailAddress"]').text(Hypr.getLabel('emailMissing'));
				errors = true;                            
			}else if(!emailReg.test($("#emailAddress").val())){
				$("#emailAddress").addClass('is-invalid');
				$('[data-mz-validationmessage-for="emailAddress"]').text(Hypr.getLabel('emailMissing'));
				errors = true;
			}else{
				 $("#emailAddress").removeClass('is-invalid');
				 $('[data-mz-validationmessage-for="emailAddress"]').text('');
			}
			

			if(!$('#g-recaptcha-response').val()){
				$('[data-mz-validationmessage-for="captcha"]').text(Hypr.getLabel('missingCaptcha'));
				errors = true;
			}else{
				$('[data-mz-validationmessage-for="captcha"]').text('');
			}
			
			if(!errors) {
				$('#update-mailing-form').addClass('is-loading');
				var language = require.mozuData('apicontext').headers["x-vol-locale"];
				if(language === 'en-US') {
					language = 'English';
				} else if(language === 'fr-CA'){
					language = 'French';
				}
				var data = {
						id: $("#idCode").val(),
						email: HyprLiveContext.locals.pageContext.user.email,
						language: language,
						isNewsletterSync: $('#specialoffers').is(':checked'),
						oldContact:{
							firstName: $("#oldFirstName").val(),
							lastName: $("#oldLastName").val(),
							address: $("#oldAddress").val(),
							city: $("#oldCity").val(),
							province: $('#oldProvince option:selected').val(),
							postalCode: $("#oldPostalCode").val()
							},
						newContact:{
							firstName: $("#newFirstName").val(),
							lastName: $("#newLastName").val(),
							address: $("#newAddress").val(),
							city: $("#newCity").val(),
							province: $('#newProvince option:selected').val(),
							postalCode: $("#newPostalCode").val()
							}
						};
				
				
				$.ajax({
					method: 'POST',
            		contentType: 'application/json; charset=utf-8',
            		url: Hypr.getThemeSetting('formPostingUrl') + 'updateMailingAddress',
            		data: JSON.stringify(data),     
            		success:function(response){
            			$('.sucess-email').show();
     	        	   	$('#update-mailing-form').removeClass('is-loading');
     	        	   	clearFields();
        			},
        			error: function (error) {
        				$('.error-email').show();
        			}
				 });
			 }
			
		});
		
		  	  
	});
});