define(['modules/api',
        'modules/backbone-mozu',
        'underscore',
        'modules/jquery-mozu',
        'modules/confirmation-models-orders',
        'hyprlivecontext',
        'hyprlive',
        'modules/preserve-element-through-render',
        'modules/models-tax-calculator'
    ],
    function (api, Backbone, _, $, OrderModels, HyprLiveContext, Hypr, preserveElement, TaxCalculatorModel) {
        /*
        Our Order Confirmation page doesn't involve too much logic, but our
        order model doesn't include enough details about pickup locations.
        We are running an api call with the fulfillmentLocationCodes of the
        items in the order. The model on the confirmation page will then
        include an array of locationDetails.
        */
        var apiContext = require.mozuData('apicontext');
        var parentCategoryId;
        var categoryData = [];
        var ua = navigator.userAgent.toLowerCase();
        var taxCalculatorModel = new TaxCalculatorModel();
        var ConfirmationView = Backbone.MozuView.extend({
            templateName: 'modules/confirmation/confirmation-detail',
            additionalEvents: {
                "click #confirmation-order-link": 'redirectMyaccount',
                'click .data-layer-productClick': 'sendDataLayer'
            },
            sendDataLayer: function (e) {
                var breadcrumbStr = "";
                var cookieCategoryArray = [];
                var clickedProductCode = $(e.currentTarget).closest(".datalayer-parent-element").attr("data-mz-product");
                var actionFieldList = $(e.currentTarget).attr("href").split('?page=')[1];
                var getPrice = "";
                var hasSalePrice = $(e.currentTarget).closest(".datalayer-parent-element").find(".mz-item-price.is-saleprice") && $(e.currentTarget).closest(".datalayer-parent-element").find(".mz-item-price.is-saleprice").length > 0;
                if (hasSalePrice) {
                    getPrice = $(e.currentTarget).closest(".datalayer-parent-element").find(".is-saleprice").text().split('/')[0];
                } else {
                    getPrice = $(e.currentTarget).closest(".datalayer-parent-element").find(".unit-price").text().split('/')[0];
                }
                var getBrand = $(e.currentTarget).closest(".datalayer-parent-element").find(".product-tile-brand").text();
                if ($.cookie('BreadCrumbFlow')) {
                    cookieCategoryArray = $.parseJSON($.cookie("BreadCrumbFlow"));
                } else {
                    cookieCategoryArray = window.cookieData;
                }

                var Prod = _.findWhere(cookieCategoryArray, {'productCode': clickedProductCode});
                breadcrumbStr = Prod.flow;
                if (actionFieldList) {
                    if (actionFieldList.indexOf('&rrec=true') !== -1) {
                        actionFieldList = actionFieldList.split('&rrec=true')[0];
                    }
                }
                dataLayer.push({
                    'event': 'productClick',
                    'ecommerce': {
                        'click': {
                            'actionField': {'list': actionFieldList},
                            'products': [{
                                'name': $.trim($(e.currentTarget).closest(".datalayer-parent-element").attr("data-mz-productName")),
                                'id': $.trim(clickedProductCode),
                                'price': getPrice ? $.trim(getPrice.replace('$', '').replace(',', '.')) : '',
                                'brand': getBrand ? $.trim(getBrand) : '',
                                'category': $.trim(breadcrumbStr),
                                'position': $(e.currentTarget).closest(".datalayer-parent-element").attr("data-mz-position")
                            }]
                        }
                    }
                });
            },
            redirectMyaccount: function () {
                window.location = "/myaccount?click=true";
            },
            getCookieValue: function (cookieName) {
                var b = document.cookie.match('(^|;)\\s*' + cookieName + '\\s*=\\s*([^;]+)');
                return b ? b.pop() : '';
            },
            setCookie: function (cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                var expires = "expires=" + d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            },
            initialize: function () {
                var EHFTotal = 0;
                var me = this;
                var currentCategoryId, filter, parentCategoryId, categoryCode;
                var allCategories = [];
                var availableInCookie = false, isDatalayerSent = false;
                var cookieCategoryArray;
                if ($.cookie('BreadCrumbFlow')) {
                    cookieCategoryArray = $.parseJSON($.cookie("BreadCrumbFlow"));
                    var itemsArray = me.model.get('items').models;
                    for (var i = 0; i < itemsArray.length; i++) {
                        var available = _.findWhere(cookieCategoryArray, {'productCode': itemsArray[i].get('product').get('productCode')});
                        if (!available) {
                            availableInCookie = false;
                            break;
                        } else {
                            availableInCookie = true;
                        }
                    }
                    if (availableInCookie) {
                        categoryData = cookieCategoryArray;
                        var orderCoupon;
                        var productDataList = [];
                        _.each(itemsArray, function (item) {
                            var catname = _.findWhere(categoryData, {'productCode': item.get('product').get('productCode')});
                            catname = catname.flow;
                            var itemCoupon;
                            if (item.get('productDiscount')) {
                                if (item.get('productDiscount').couponCode !== '') {
                                    itemCoupon = item.get('productDiscount').couponCode;
                                }
                            } else {
                                if (me.model.get('orderDiscounts') && me.model.get('orderDiscounts').length > 0) {
                                    orderCoupon = me.model.get('orderDiscounts')[0].couponCode;
                                }
                            }
                            var brand;
                            _.find(item.get('product').get('properties'), function (prop) {
                                if (prop.attributeFQN == Hypr.getThemeSetting('brandDesc')) {
                                    brand = prop.values[0].stringValue;
                                }
                            });
                            var qty = parseInt(item.get('quantity'), 10);
                            var productPrice = item.get('discountedTotal') / qty;
                            var productData = {
                                'name': item.get('product').get('name'),
                                'id': item.get('product').id,
                                'price': productPrice.toFixed(2).replace('$', '').replace(',', '.').trim(),
                                'brand': brand,
                                'category': catname ? catname : '',
                                'variant': '',
                                'quantity': qty,
                                'coupon': itemCoupon ? itemCoupon : ''
                            };
                            productDataList.push(productData);
                        });


                        var totalCartValue = me.model.get('total');
                        var orderId = me.model.get('orderNumber');
                        var taxTotal = me.model.get('taxTotal');
                        orderCoupon = orderCoupon ? orderCoupon : '';
                        var preferredStoreCookie;
                        if (localStorage.getItem('preferredStore')) {
                            preferredStoreCookie = JSON.parse(localStorage.getItem('preferredStore'));
                        }

                        var orderNumber = me.getCookieValue('orderNumber');
                        if (orderId != orderNumber) {
                            dataLayer.push({
                                'event': 'purchase',
                                'ecommerce': {
                                    'currencyCode': 'CAD',
                                    'purchase': {
                                        'actionField': {
                                            'id': orderId,
                                            'affiliation': preferredStoreCookie ? preferredStoreCookie.name : 'Home Hardware',
                                            'revenue': totalCartValue.toFixed(2),
                                            'tax': taxTotal.toFixed(2),
                                            'shipping': 0,
                                            'coupon': orderCoupon ? orderCoupon : ''
                                        },
                                        'products': productDataList
                                    }
                                }
                            });
                            me.setCookie("orderNumber", orderId, 1);
                            console.log('dataLayer sent.');
                            var cookieData = $.cookie("BreadCrumbFlow");
                            $.removeCookie('BreadCrumbFlow', {path: '/'});
                            var expiryDate = new Date();
                            Date.prototype.addHours = function (h) {
                                this.setTime(this.getTime() + (h * 60 * 60 * 1000));
                                return this;
                            };

                            expiryDate = new Date().addHours(4);
                            if (ua.indexOf('chrome') > -1) {
                                if (/edg|edge/i.test(ua)) {
                                    $.cookie('BreadCrumbFlow', cookieData, {path: '/', expires: expiryDate});
                                } else {
                                    document.cookie = "BreadCrumbFlow=" + cookieData + ";expires=" + expiryDate + ";path=/;Secure;SameSite=None";
                                }
                            } else {
                                $.cookie('BreadCrumbFlow', cookieData, {path: '/', expires: expiryDate});
                            }
                            isDatalayerSent = true;
                        } else {
                            console.log('Confirmation page refreshed. Not sending dataLayer.');
                        }
                    }
                }

                if (!isDatalayerSent) {
                    if ((!$.cookie('BreadCrumbFlow') || !availableInCookie)) {
                        api.get('categories', {pageSize: 200, startIndex: 0}).then(function (result1) {
                            if (result1.data.items.length > 0)
                                allCategories.push(result1.data.items);
                            api.get('categories', {pageSize: 200, startIndex: 201}).then(function (result2) {
                                if (result2.data.items.length > 0) {
                                    var currentCategories = $.merge(allCategories[0], result2.data.items);
                                    allCategories[0] = currentCategories;
                                }
                                api.get('categories', {pageSize: 200, startIndex: 401}).then(function (result3) {
                                    if (result3.data.items.length > 0) {
                                        var currentCategories = $.merge(allCategories[0], result3.data.items);
                                        allCategories[0] = currentCategories;
                                    }
                                    parentCategoryId = Hypr.getThemeSetting('newLBMAndHardlinesCategoryId');
                                    var items = me.model.get('items').models;
                                    var itemIsInCookie = false;

                                    _.each(items, function (item) {
                                        if ($.cookie('BreadCrumbFlow')) {
                                            cookieCategoryArray = $.parseJSON($.cookie("BreadCrumbFlow"));
                                            var avail = _.findWhere(cookieCategoryArray, {'productCode': item.get('product').get('productCode')});
                                            if (!avail) {
                                                itemIsInCookie = false;
                                            } else {
                                                itemIsInCookie = true;
                                            }
                                        }
                                        if (itemIsInCookie) {
                                            cookieCategoryArray = $.parseJSON($.cookie("BreadCrumbFlow"));
                                            _.find(cookieCategoryArray, function (val) {
                                                if (val.productCode === item.get('product').get('productCode')) {
                                                    categoryData.push(val);
                                                }
                                            });
                                        } else {
                                            if (!$.cookie('BreadCrumbFlow') || !itemIsInCookie) {
                                                var currentCategoryData = {};
                                                currentCategoryId = item.attributes.product.attributes.categories[0].id;
                                                _.find(allCategories[0], function (val) {
                                                    if (val.categoryId === currentCategoryId) {
                                                        var currentCategoryName = val.content.name;
                                                        var productcode = item.get('product').get('productCode');
                                                        var currentParentId = val.parentCategory.categoryId;
                                                        currentCategoryData = {
                                                            'productCode': productcode,
                                                            'flow': currentCategoryName
                                                        };
                                                        categoryData.push(currentCategoryData);
                                                        if (parentCategoryId != currentParentId) {
                                                            //if current category is not NEW LBM And Hardlines then do the same process for current category
                                                            me.getParentCategory(parentCategoryId, currentParentId, productcode, allCategories[0]);
                                                        } else {
                                                            console.log(categoryData);
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    });
                                    var orderCoupon;
                                    var productDataList = [];
                                    var orderNumber = me.getCookieValue('orderNumber');
                                    var orderId = me.model.get('orderNumber');
                                    if (orderId != orderNumber) {
                                        _.each(items, function (item) {
                                            var catname = _.findWhere(categoryData, {'productCode': item.get('product').get('productCode')});
                                            catname = catname.flow;
                                            var itemCoupon;
                                            if (item.get('productDiscount')) {
                                                if (item.get('productDiscount').couponCode !== '') {
                                                    itemCoupon = item.get('productDiscount').couponCode;
                                                }
                                            } else {
                                                if (me.model.get('orderDiscounts') && me.model.get('orderDiscounts').length > 0) {
                                                    orderCoupon = me.model.get('orderDiscounts')[0].couponCode;
                                                }
                                            }
                                            var brand;
                                            _.find(item.get('product').get('properties'), function (prop) {
                                                if (prop.attributeFQN == Hypr.getThemeSetting('brandDesc')) {
                                                    brand = prop.values[0].stringValue;
                                                }
                                            });
                                            var productData = {
                                                'name': item.get('product').get('name'),
                                                'id': item.get('product').id,
                                                'price': item.get('discountedTotal').replace('$', '').replace(',', '.').trim(),
                                                'brand': brand,
                                                'category': catname ? catname : '',
                                                'variant': '',
                                                'quantity': parseInt(item.get('quantity'), 10),
                                                'coupon': itemCoupon ? itemCoupon : ''
                                            };
                                            productDataList.push(productData);
                                        });

                                        var totalCartValue = me.model.get('total');
                                        var taxTotal = me.model.get('taxTotal');
                                        orderCoupon = orderCoupon ? orderCoupon : '';
                                        var preferredStoreCookie;
                                        if (localStorage.getItem('preferredStore')) {
                                            preferredStoreCookie = JSON.parse(localStorage.getItem('preferredStore'));
                                        }
                                        dataLayer.push({
                                            'event': 'purchase',
                                            'ecommerce': {
                                                'currencyCode': 'CAD',
                                                'purchase': {
                                                    'actionField': {
                                                        'id': orderId,
                                                        'affiliation': preferredStoreCookie ? preferredStoreCookie.name : 'Home Hardware',
                                                        'revenue': totalCartValue.toFixed(2),
                                                        'tax': taxTotal.toFixed(2),
                                                        'shipping': 0,
                                                        'coupon': orderCoupon ? orderCoupon : ''
                                                    },
                                                    'products': productDataList
                                                }
                                            }
                                        });

                                        me.setCookie("orderNumber", orderId, 1);
                                        console.log('dataLayer sent.');
                                        var cookieData = $.cookie("BreadCrumbFlow");
                                        $.removeCookie('BreadCrumbFlow', {path: '/'});
                                        var expiryDate = new Date();
                                        Date.prototype.addHours = function (h) {
                                            this.setTime(this.getTime() + (h * 60 * 60 * 1000));
                                            return this;
                                        };
                                        expiryDate = new Date().addHours(4);

                                        if (ua.indexOf('chrome') > -1) {
                                            if (/edg|edge/i.test(ua)) {
                                                $.cookie('BreadCrumbFlow', cookieData, {
                                                    path: '/',
                                                    expires: expiryDate
                                                });
                                            } else {
                                                document.cookie = "BreadCrumbFlow=" + cookieData + ";expires=" + expiryDate + ";path=/;Secure;SameSite=None";
                                            }
                                        } else {
                                            $.cookie('BreadCrumbFlow', cookieData, {path: '/', expires: expiryDate});
                                        }
                                    } else {
                                        console.log('Confirmation page refreshed. Not sending dataLayer.');
                                    }
                                });
                            });
                        });
                    }
                }
                //calculate the quantity of total items to show on order confirmation
                var items = this.model.get('items').toJSON();
                var firstItemQuantity = 0;
                _.each(items, function (item, index) {
                    if (item.product.productCode != 'EHF101') {
                        firstItemQuantity += item.quantity;
                    }
                });
                this.model.set('totalItemsQuantity', firstItemQuantity);
                _.each(this.model.get('items').models, function (item) {
                    var ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
                    var ehfOption = item.attributes.product.attributes.options.findWhere({
                        'attributeFQN': ehfAttributeFQN
                    });
                    var currentProductEHFValue = 0;
                    if (ehfOption) {
                        if(item.get('fulfillmentMethod') === "Pickup") {
                            currentProductEHFValue = ehfOption.get('shopperEnteredValue') * item.attributes.quantity;
                        }
                    }
                    if(item.get('fulfillmentMethod') === "Ship"){
                        var prices = item.get('unitPrice');
                        var itemPrice = prices.saleAmount ? prices.saleAmount : prices.listAmount;
                        if(prices.overrideAmount && prices.overrideAmount !== itemPrice){
                            var ehfFees = prices.overrideAmount - itemPrice;
                            currentProductEHFValue = ehfFees * item.attributes.quantity;
                        }
                    }
                    EHFTotal += currentProductEHFValue;
                });
                this.model.set('EHFTotal', EHFTotal);
                this.model.set('EHFTotalToSubtract', EHFTotal);
                var contextSiteId = apiContext.headers["x-vol-site"];
                var frSiteId = Hypr.getThemeSetting("frSiteId");
                if (frSiteId === contextSiteId) {
                    this.model.set("isFrenchSite", true);
                }
                var orderType = this.setOrderType();
                if ((orderType === "mixOrder" || orderType === "shipToHomeOrder") && this.model.get('taxData')) this.setTaxData(orderType);
                if (ua.indexOf('chrome') > -1) {
                    if (/edg|edge/i.test(ua)) {
                      $.cookie('fulFillmentChanged', false, { path: '/' });
                    } else {
                      document.cookie = "fulFillmentChanged=" + false + ";path=/;Secure;SameSite=None";
                    }
                  } else {
                    $.cookie('fulFillmentChanged', false, { path: '/' });
                  }

            /**
             * Removing localStorage items set during contact information fill up.
             */
                localStorage.removeItem("checkoutOrderNote");
                localStorage.removeItem("subscribeNotificationCheck");
            },
            setOrderType: function () {
                var self = this;
                var isPickUp = 0,
                    isShip = 0,
                    orderType = '',
                    shipToStoreItems = [],
                    shipToHomeItems = [];
                _.each(this.model.get('items').models, function (item) {
                    if (item.get('fulfillmentMethod') === "Pickup") {
                        shipToStoreItems.push(item.toJSON());
                        isPickUp++;
                    } else if (item.get('fulfillmentMethod') === "Ship") {
                        self.setEhfAmount(item);
                        shipToHomeItems.push(item.toJSON());
                        isShip++;
                    }
                });

                if (isPickUp > 0 && isShip > 0) {
                    orderType = 'mixOrder';
                } else if (isPickUp > 0) {
                    orderType = 'shipToStoreOrder';
                } else if (isShip > 0) {
                    orderType = 'shipToHomeOrder';
                }

                this.model.set('shipToStoreItems', shipToStoreItems);
                this.model.set('shipToHomeItems', shipToHomeItems);
                this.model.set('orderType', orderType);
                return orderType;
            },
            setEhfAmount: function(item) {
                var prices = item.get('unitPrice');
                var itemPrice = prices.saleAmount ? prices.saleAmount : prices.listAmount;
                if(prices.overrideAmount && prices.overrideAmount !== itemPrice){
                    var ehfFees = prices.overrideAmount - itemPrice;
                    var ehfOption = {
                        name: "EHF Fees",
                        value: ehfFees,
                        shopperEnteredValue: ehfFees,
                        attributeFQN: "tenant~ehf-fees"
                    };
                    item.get('product').get('options').reset(ehfOption);
                } else {
                    item.get('product').get('options').reset([]);
                }
            },
            setTaxData: function (orderType) {
                var taxData = this.model.get('taxData');
                    var customTaxes = taxCalculatorModel.getCustomTaxes(taxData, orderType);
                    this.model.set('customTaxData', customTaxes);
            },
            getParentCategory: function (parentCategoryId, currentCategoryId, productcode, allCategories) {
                var me = this;
                var parentCategoryData = [];
                _.find(allCategories, function (val) {
                    if (val.categoryId === currentCategoryId) {
                        var parentCategoryName = val.content.name;
                        var categoryDataToUpdate = _.findWhere(categoryData, {'productCode': productcode});
                        categoryDataToUpdate.flow = parentCategoryName + '/' + categoryDataToUpdate.flow;
                        _.find(categoryData, function (category, index) {
                            if (category.productcode === productcode) {
                                categoryData[index] = categoryDataToUpdate;
                            }
                        });
                        if (parentCategoryId != val.parentCategory.categoryId) {
                            //if current category is not NEW LBM And Hardlines then do the same process for current category
                            me.getParentCategory(parentCategoryId, val.parentCategory.categoryId, productcode, allCategories);
                        } else {
                            //if current category is NEW LBM And Hardlines then do stop the process and set the ccategory flow in model
                            me.model.set("categoryData", categoryData);
                        }
                    }
                });
            },
            render: function () {
                var me = this;
                var apicontext = require.mozuData('apicontext'),
                locale = apicontext.headers['x-vol-locale'],
                site = apicontext.headers['x-vol-site'];
                this.model.set("currentLocale", locale);
                this.model.set("site", site);
                Backbone.MozuView.prototype.render.apply(this);

            },
            callStore: function (e) {
                var screenWidth = window.matchMedia("(max-width: 767px)");
                if (!screenWidth.matches) {
                    e.preventDefault();
                }
            }
        });

        var ConfirmationModel = OrderModels.Order.extend({
            getLocationData: function () {
                var codes = [];
                var items = this.get('items');

                items.forEach(function (item) {
                    if (codes.indexOf(item.get('fulfillmentLocationCode')) == -1)
                        codes.push(item.get('fulfillmentLocationCode'));
                });

                var queryString = "";
                codes.forEach(function (code, index) {
                    if (index != codes.length - 1) {
                        queryString += "code eq " + code + " or ";
                    } else {
                        queryString += "code eq " + code;
                    }
                });
                return api.get('locations', {filter: queryString});
            }
        });

        function timeConvert(time) {
            // Check correct time format and split into components
            time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

            if (time.length > 1) { // If time format correct
                time = time.slice(1); // Remove full string match value
                time[5] = +time[0] < 12 ? ' am' : ' pm'; // Set AM/PM
                time[0] = +time[0] % 12 || 12; // Adjust hours
            }
            return time.join(''); // return adjusted time or original string
        }

        $(document).ready(function () {
            var confModel = ConfirmationModel.fromCurrent();
            confModel.getLocationData().then(function (response) {
                if (response.data.items.length) {
                   /*  _.each(response.data.items[0].regularHours, function (day) {
                        if (day.isClosed !== true) {
                            if (typeof day === 'object' && day) {
                                day.openTime = timeConvert(day.openTime);
                                day.closeTime = timeConvert(day.closeTime);
                            }
                        }
                    }); */
                    confModel.set('locationDetails',(response.data.items[0].code === '1' || response.data.items[0].code === '2' || response.data.items[0].code === '4') ? response.data.items[1] ? response.data.items[1] : response.data.items[0]:response.data.items[0]);
                }
                renderConfirmationView(confModel);
            });

            checkAndDeleteCheckoutCookies();
        });

        function renderConfirmationView(confModel) {
            var confirmationView = new ConfirmationView({
                el: $('#confirmation-container'),
                model: confModel
            });
            confirmationView.render();
        }

        function checkAndDeleteCheckoutCookies() {
            if ($.cookie('shippingMethodCompleted') || $.cookie('shippingStepCompleted')) {
                $.removeCookie('shippingMethodCompleted', {path: '/'});
                $.removeCookie('shippingStepCompleted', {path: '/'});
                $.removeCookie('addressReset', {path: '/'});
            }
        }
    });
