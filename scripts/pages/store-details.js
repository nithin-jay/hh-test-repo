require([
		'modules/jquery-mozu',
		'hyprlive',
		'modules/api',
		'modules/models-cart',
		'modules/models-customer',
		'underscore',
		'modules/backbone-mozu',
		'modules/cookie-utils'
	],
	function ($, Hypr, api, CartModels, CustomerModels, _, Backbone, CookieUtils) {

		var user = require.mozuData('user'),
			contextSiteId = require.mozuData('apicontext').headers["x-vol-site"],
			homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId"),
			locationList = Hypr.getThemeSetting("storeDocumentListFQN");
		var locationCode, isHomeFurnitureSite = false, itemsInCart = [],
		ua = navigator.userAgent.toLowerCase();

		var StoreDetails = Backbone.MozuView.extend({
			templateName: "modules/common/store-info",
			initialize: function(){
				var self = this;
				var locale = require.mozuData('apicontext').headers['x-vol-locale'];
				var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
				locale = locale.split('-')[0];
				var currentLocale = locale === 'fr' ? '/fr' : '/en';
				self.model.set("currentLocale",currentLocale);
				self.model.set("currentSite",currentSite);
    
			},
			render:function(){
				Backbone.MozuView.prototype.render.apply(this, arguments);
			}
		});
		
		var returnUrl ='/';
			var locale = require.mozuData('apicontext').headers['x-vol-locale'];
			locale = locale.split('-')[0];
			var currentLocale = '';
		if (Hypr.getThemeSetting('homeFurnitureSiteId') != contextSiteId){
			returnUrl = locale === 'fr' ? '/fr' : '/en';
		}
		if (homeFurnitureSiteId === contextSiteId) {
			isHomeFurnitureSite = true;
		}

		/*
			This function updates the authenticated user's preferred store. i.e customer attribute
		*/
		function updateCustomerAttribute() {
			//update customer attribute here with preferred store
			var preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
			var customer = new CustomerModels.EditableCustomer();
			customer.set("id", user.accountId);
			customer.fetch().then(function (response) {
				var attribute;
				if (isHomeFurnitureSite) {
					attribute = _.findWhere(response.apiModel.data.attributes, {
						fullyQualifiedName: Hypr.getThemeSetting('hfPreferredStore')
					});
				} else {
					attribute = _.findWhere(response.apiModel.data.attributes, {
						fullyQualifiedName: Hypr.getThemeSetting('preferredStore')
					});
				}
				var attributeData = {
					attributeFQN: attribute.fullyQualifiedName,
					attributeDefinitionId: attribute.attributeDefinitionId,
					values: [preferredStore.code]
				};
				response.apiUpdateAttribute(attributeData).then(function () {
					window.location.href = returnUrl;
				});
			});
		}

		function continueWithNewStore() {
			$.ajax({
				url: "/set-purchase-location",
				data: {
					"purchaseLocation": locationCode
				},
				type: "GET",
				success: function (response) {
						api.get("location", {
							code: locationCode
						}).then(function (response) {
							var selectedStore = response.data;
							CookieUtils.setPreferredStore(selectedStore);
							//$.cookie("sessionStore",JSON.stringify(selectedStore.code),{path:'/'});
							if(itemsInCart.length > 0){
								var previousItemQuantity = [];
								_.each(itemsInCart, function(item){
									var prodObj = {
										'quantity' : item.quantity,
										'productCode' : item.product.productCode,
										'price': item.product.price.price,
										'salePrice': item.product.price.salePrice
									};
									previousItemQuantity.push(prodObj);
								});

								if (ua.indexOf('chrome') > -1) {
									if (/edg|edge/i.test(ua)){
										$.cookie("previousItems",JSON.stringify(previousItemQuantity),{path:'/'});
									}else{
										document.cookie = "previousItems="+JSON.stringify(previousItemQuantity)+";path=/;Secure;SameSite=None";
									}
								} else {
									$.cookie("previousItems",JSON.stringify(previousItemQuantity),{path:'/'});
								}
							}
							if (user.isAuthenticated && !user.isAnonymous) {
								updateCustomerAttribute();
							} else {
								window.location.href = returnUrl;
							}
						});
				},
				error: function (error) {
					console.log(error);
				}
			});
		}
		var LoadResources = {
			load: function(src, callback) {
			  var script = document.createElement('script'),
				  loaded;
			  script.setAttribute('src', src);
			  if (callback) {
				script.onreadystatechange = script.onload = function() {
				  if (!loaded) {
					callback();
				  }
				  loaded = true;
				};
			  }
			  document.getElementsByTagName('head')[0].appendChild(script);
			}
		};
		function setStoreData(storeData){
			var storeDetails = new StoreDetails({
				el: $("#store-info"),
				model: new Backbone.MozuModel(storeData)
			});
			storeDetails.render();
		}
		function timeConvert(time) {
			// Check correct time format and split into components
			time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

			if (time.length > 1) { // If time format correct
				time = time.slice(1); // Remove full string match value
				time[5] = +time[0] < 12 ? ' am' : ' pm'; // Set AM/PM
				time[0] = +time[0] % 12 || 12; // Adjust hours
			}
			return time.join(''); // return adjusted time or original string
			}
			var storeCode = window.location.pathname.split("/").pop();
			var storeData;
			api.get("location",storeCode).then(function(response){
				if(response && response.data){
					storeData = response.data;
					_.each(response.data.regularHours, function(day) {
						if(day.isClosed !== true){
							if(typeof day === 'object' && day){                                                                       
								day.openTime = timeConvert(day.openTime);
								day.closeTime = timeConvert(day.closeTime);
							}
						}
					});
					setStoreData(storeData);
				}
			});
		$(document).ready(function () {

			LoadResources.load("/scripts/unwired-gl.js", function(){
				var map, bounds, infoWindow, pos, marker, icon, currentIcon;

				var selectedStore = $(".store-details-top-section").data("mz-store");
				
				unwired.key = mapboxgl.accessToken = Hypr.getThemeSetting('mapApiKey');
				//Define the map and configure the map's theme
				
				map = new mapboxgl.Map({
					container: document.getElementById('store-map'), // container id
					style: unwired.getLayer("streets"), // stylesheet location
					center: [parseFloat($(".store-details-top-section").data("mz-longitude")), parseFloat($(".store-details-top-section").data("mz-latitude"))], // starting position [lng, lat]
					zoom: Hypr.getThemeSetting('storeDetailsMapZoomLevel') // starting zoom
				});

				//Add Navigation controls to the map to the top-right corner of the map
				var nav = new mapboxgl.NavigationControl();
				map.addControl(nav, 'top-right');
				
				//Add a Scale to the map
				map.addControl(new mapboxgl.ScaleControl({
					maxWidth: 80,
					unit: 'metric' //imperial for miles
				}));

				if (localStorage.getItem('preferredStore')) {
					var preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
					if (parseInt(preferredStore.code, 10) === selectedStore) {
						currentIcon = 'url('+Hypr.getThemeSetting("sirvResourceImageUrl")+'/marker-selection.png)';
					} else {
						currentIcon = 'url(' +Hypr.getThemeSetting("sirvResourceImageUrl")+'/marker-' + (isHomeFurnitureSite ? 'hf' : 'hh') + '.png'+')';
					}
				}else{
					currentIcon = 'url('+Hypr.getThemeSetting("sirvResourceImageUrl")+'/marker-' + (isHomeFurnitureSite ? 'hf' : 'hh') + '.png'+')';	
				}

				var el = document.createElement('div');
				
					el.style.backgroundImage = currentIcon;
					el.style.width = isHomeFurnitureSite ? '27px' : '33px'; 
					el.style.height = isHomeFurnitureSite ? '28px' : '40px';
					el.style.backgroundSize = 'cover';
					
				// create the marker and add to map
				marker = new mapboxgl.Marker(el).setLngLat([parseFloat($(".store-details-top-section").data("mz-longitude")), parseFloat($(".store-details-top-section").data("mz-latitude"))]).addTo(map);
			});

			/* make this my store functionality */
			$("#makeThisMyStoreBtn").on("click", function (e) {
				var currentTarget = e.currentTarget;
				locationCode = $(currentTarget).data("mzStore");
				var cartModelData = new CartModels.Cart();
				var cartCount = null;
				/**
				 * Below code removes a flag stored in a cookie. This cookie is set through Arc (IGN_HH_IP_BASED_STORE_LOCATOR.1.0.0)
				 * When a user selects a store manually, we need to remove this flag, so that at cart checkout store confirmation pop-up disappears.
				 * Ticket - Main Ticket - 1202, Sub ticket - IWM-1471
				 * @Date - 19/04/2021
				 */
				 if($.cookie('preferredStore_userConfirmationRequired')) {
					$.removeCookie('preferredStore_userConfirmationRequired', { path: '/' });
				 }
				if (window.location.search.indexOf('storeService') > 0) {
					var returnParam = (window.location.search.split('storeService=')[1] || '');
					var queryString = "properties.documentKey eq " + locationCode;
					$("#content-loading").show();
					if (decodeURIComponent(returnParam) === "Home Installs") {
						api.get('documentView', {
							listName: locationList,
							filter: queryString
						}).then(function (response) {
							if (response && response.data.items.length > 0) {
								var storeDetails = response.data.items[0];
								var homeInstallStoreInfo = {
									"code": storeDetails.name,
									"name": storeDetails.properties.storeName,
									"homeInstallEmail": storeDetails.properties.homeInstallsEmail,
									"address1": storeDetails.properties.address1,
									"address2": storeDetails.properties.address2,
									"city": storeDetails.properties.city,
									"province": storeDetails.properties.province[0],
									"postalCode": storeDetails.properties.postalCode,
									"phone": storeDetails.properties.phone
								};
								if (ua.indexOf('chrome') > -1) {
									if (/edg|edge/i.test(ua)){
										$.cookie("homeInstallStore", JSON.stringify(homeInstallStoreInfo), {
											path: '/'
										});
									}else{
										document.cookie = "homeInstallStore="+JSON.stringify(homeInstallStoreInfo)+";path=/;Secure;SameSite=None";
									}
								} else {
									$.cookie("homeInstallStore", JSON.stringify(homeInstallStoreInfo), {
										path: '/'
									});
								}
								$("#content-loading").show();
								window.location.href = "/home-install";
							}
						});
					} else {
						api.get('documentView', {
							listName: locationList,
							filter: queryString
						}).then(function (response) {
							if (response && response.data.items.length > 0) {
								var storeDetails = response.data.items[0];
								var commercialMaintenanceStore = {
									"code": storeDetails.name,
									"commercialMaintenanceEmail": storeDetails.properties.commercialMaintenanceEmail,
									"name": storeDetails.properties.storeName,
									"address1": storeDetails.properties.address1,
									"address2": storeDetails.properties.address2,
									"city": storeDetails.properties.city,
									"province": storeDetails.properties.province[0],
									"postalCode": storeDetails.properties.postalCode,
									"phone": storeDetails.properties.phone
								};

								if (ua.indexOf('chrome') > -1) {
									if (/edg|edge/i.test(ua)){
										$.cookie("commercialMaintenanceStore", JSON.stringify(commercialMaintenanceStore), {
											path: '/'
										});
									}else{
										document.cookie = "commercialMaintenanceStore="+JSON.stringify(commercialMaintenanceStore)+";path=/;Secure;SameSite=None";
									}
								} else {
									$.cookie("commercialMaintenanceStore", JSON.stringify(commercialMaintenanceStore), {
										path: '/'
									});
								}
								$("#content-loading").show();
								window.location.href = "/commercial-maintenance";
							}
						});
					}
				} else {
					cartModelData.on('sync', function () {
						cartCount = cartModelData.count();
					});
					cartModelData.apiGet().then(function (response) {
						if (cartCount > 0) {
							itemsInCart = response.data.items;
							api.get("location", {
								code: locationCode
							}).then(function (response) {
								var selectedStore = response.data;
								var isEcomStore = _.findWhere(selectedStore.attributes, {
									"fullyQualifiedName": Hypr.getThemeSetting("isEcomStore")
								});
								if (isEcomStore && isEcomStore.values[0] === 'Y') {
									$('#store-change-confirmation-modal').modal().show();
								} else {
									$('#non-ecom-store-confirmation-modal').modal().show();
								}
							});
						} else {
							continueWithNewStore();
						}
					});
				}
			});
			/*continue with new store if items in cart*/
			$("#continueWithNewStoreBtn").on("click", function () {
				continueWithNewStore();
			});
			$("#switchToNewStore").on("click", function () {
				continueWithNewStore();
			});
			$(".store-tel").on("click", function (e) {
				var screenWidth = window.matchMedia("(max-width: 767px)");
				if (!screenWidth.matches) {
					e.preventDefault();
				}
			});
		});
		return {
			StoreDetails : StoreDetails
		};
	});