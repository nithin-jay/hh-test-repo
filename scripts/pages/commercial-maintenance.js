require(["modules/jquery-mozu", 
	"underscore", 
	"hyprlive", 
	"modules/backbone-mozu", 
	'modules/api',
	'hyprlivecontext'], 
	function ($, _, Hypr, Backbone, api, HyprLiveContext) {
	var clearFields = function(){
		$("#firstName, #lastName, #emailAddress, #number1, #number2, #number3, #message").val('');
		$("input:checkbox").attr("checked", false);
		$("#type-of-industry").val('0');
	};
	$(document).ready(function() {	
		clearFields();
		var locale = require.mozuData('apicontext').headers['x-vol-locale'];
		
		var CommercialMaintenanceView = Backbone.MozuView.extend({
	        templateName: 'modules/common/commercial-maintenance-view'	        
		});
		 
		var commercialMaintenanceStore = $.cookie('commercialMaintenanceStore');
		if (commercialMaintenanceStore) {
			commercialMaintenanceStore = JSON.parse(commercialMaintenanceStore);
			commercialMaintenanceStore.locale = locale;
			var commercialMaintenanceView = new CommercialMaintenanceView({
			       el: $('#commercial-maintenance-store'),
			       model: new Backbone.Model(commercialMaintenanceStore)
				});
			commercialMaintenanceView.render();
		} else if(HyprLiveContext.locals.pageContext.purchaseLocation && HyprLiveContext.locals.pageContext.purchaseLocation.code){
			var storeCode = HyprLiveContext.locals.pageContext.purchaseLocation.code;
			var filtersString = "properties.commercialMaintenance eq true and properties.documentKey eq " + storeCode;
			var locationList = Hypr.getThemeSetting("storeDocumentListFQN");
			
			api.get('documentView', {
				listName: locationList,
				filter:filtersString
			}).then(function(response) {		
				if(response.data.items.length > 0) {
					
					var item = response.data.items[0];
					commercialMaintenanceStore = {
							code: item.name,
							commercialMaintenanceEmail: item.properties.commercialMaintenanceEmail,
							name: item.properties.storeName,
							address2: item.properties.address2,
							city: item.properties.city,
							province: item.properties.province[0],
							postalCode:item.properties.postalCode,
							phone:item.properties.phone,
							locale: locale
					};
					
					var commercialMaintenanceView = new CommercialMaintenanceView({
					       el: $('#commercial-maintenance-store'),
					       model: new Backbone.Model(commercialMaintenanceStore)
						});
					commercialMaintenanceView.render();
				}
			});

		}
		
		
		var emailReg = Backbone.Validation.patterns.email;
		$('#commercial-maintenance-button').on('click',function(){
			var errors = false;
			if (!$("#firstName").val() || $("#firstName").val().trim() === ""){
				$("#firstName").addClass('is-invalid');
				$('[data-mz-validationmessage-for="firstName"]').text(Hypr.getLabel('fnameMissing'));
				errors = true;
			}else{
				$("#firstName").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="firstName"]').text('');
			}
			
			if (!$("#lastName").val() || $("#lastName").val().trim() === ""){
				$("#lastName").addClass('is-invalid');
				$('[data-mz-validationmessage-for="lastName"]').text(Hypr.getLabel('lnameMissing'));
				errors = true;
			}else{
				$("#lastName").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="lastName"]').text('');
			}
			
			var emailValue = $("#emailAddress").val().trim();
			if($('input[name="preferredContactMethod"]:checked').val() == 'emailAddress') {
				$('[data-mz-validationmessage-for="phoneNumber"]').text('');
				$(".phone-numbercontainer input").removeClass('is-invalid');
				if(emailValue === ""){
					$("#emailAddress").addClass('is-invalid');
					$('[data-mz-validationmessage-for="emailAddress"]').text(Hypr.getLabel('emailMissing'));
					errors = true;                            
				}else if(!emailReg.test(emailValue)){
					$("#emailAddress").addClass('is-invalid');
					$('[data-mz-validationmessage-for="emailAddress"]').text(Hypr.getLabel('emailMissing'));
					errors = true;
				}else{
					 $("#emailAddress").removeClass('is-invalid');
					 $('[data-mz-validationmessage-for="emailAddress"]').text('');
				}
			}
			
			 digitLength();
			 if($('input[name="preferredContactMethod"]:checked').val() == 'phoneNumber') {
				 $('[data-mz-validationmessage-for="emailAddress"]').text('');
				 $("#emailAddress").removeClass('is-invalid');
				 if (!$("#phoneNumber").val() || $("#phoneNumber").val() === ""){
					$(".phone-numbercontainer input").addClass('is-invalid');
					$('[data-mz-validationmessage-for="phoneNumber"]').text(Hypr.getLabel('phoneNumberMissing'));
					errors = true;
				}else{
					$(".phone-numbercontainer input").removeClass('is-invalid');
					$('[data-mz-validationmessage-for="phoneNumber"]').text('');
				}
			 }
			 
			
			var typeOfIndustry = $('#type-of-industry option:selected').val();
			if(!typeOfIndustry || typeOfIndustry === "0"){
				$("#type-of-industry").addClass('is-invalid');
				$('[data-mz-validationmessage-for="typeOfIndustry"]').text(Hypr.getLabel('subjectMissing'));
				errors = true;
			}else{
				$("#type-of-industry").removeClass('is-invalid');
				$('[data-mz-validationmessage-for="typeOfIndustry"]').text('');
			}
			
			if(!commercialMaintenanceStore) {
				$('[data-mz-validationmessage-for="commercialMaintenanceStore"]').text(Hypr.getLabel('commercialMaintenanceStoreErorMessage'));
				errors = true;
			} else {
				$('[data-mz-validationmessage-for="commercialMaintenanceStore"]').text('');
			}
			
			var offers = $('input[name="commercialMaintenanceOffers"]').is(":checked");
			
			if(offers) {
				if(!emailValue) {
					$('[data-mz-validationmessage-for="commercialMaintenanceOffers"]').text(Hypr.getLabel('commercialMaintenanceOffersErorMessage'));
					errors = true;
				} else {
					$('[data-mz-validationmessage-for="commercialMaintenanceOffers"]').text('');
				}
			}

			if(!$('#g-recaptcha-response').val()){
				$('[data-mz-validationmessage-for="captcha"]').text(Hypr.getLabel('missingCaptcha'));
				errors = true;
			}else{
				$('[data-mz-validationmessage-for="captcha"]').text('');
			}
			
			 
			 if(!errors) {
				 $('#commercial-maintenance-form').addClass('is-loading');
				 var data = {
						 firstName: $("#firstName").val().trim(),
						 lastName: $("#lastName").val().trim(),
						 emailAddress: $("#emailAddress").val().trim(),
						 phoneNumber: $('#phoneNumber').val(),
						 industryType: $('#type-of-industry option:selected').val(),
						 preferredContactMethod: $('input[name="preferredContactMethod"]:checked').val(),
						 nameOfBusiness: $('#nameOfBusiness').val(),
						 commercialMaintenanceOffers: $('input[name="commercialMaintenanceOffers"]').is(":checked"),
						 commercialMaintenanceStoreCode: commercialMaintenanceStore.code,
						 commercialMaintenanceStoreName: commercialMaintenanceStore.name,
						 commercialMaintenanceStoreEmail: commercialMaintenanceStore.commercialMaintenanceEmail,
						 locale: commercialMaintenanceStore.locale
					 };
				 $.ajax({
					method: 'POST',
            		contentType: 'application/json; charset=utf-8',
            		url: Hypr.getThemeSetting('formPostingUrl') + 'commercial-maintenance',
            		data: JSON.stringify(data),     
            		success:function(response){
            			$('#commercial-maintenance-store').hide();
            			$('.commercial-maintenance-form').hide();
     	        	   	$('#commercial-maintenance-form').removeClass('is-loading');
     	        	   	$('.sucess-email-template').show();
     	        	   $('html, body').scrollTop($('#goto-thankyou').offset().top);
     	        	   	var storeDetailStr = "<p class='agenda-bold store-name'>" + commercialMaintenanceStore.name +"</p>"+
							"<p class=''address'>" + commercialMaintenanceStore.address2 +"</p>"+
							"<p class='address'>" + commercialMaintenanceStore.city +", " + commercialMaintenanceStore.province +"</p>"+
							"<p class='address'>" + commercialMaintenanceStore.postalCode +"</p>"+
							"<p class='address'>" + commercialMaintenanceStore.phone +"</p>";
     	        	   	$(".store-info-details").append(storeDetailStr);
        			},
        			error: function (error) {
        				$('.error-email').show();
        			}
				 });
			 }
		});
		
		$(".digit-length").keypress(function(){
			  digitLength();
		  });
		  
		  
		  var digitLength = function(){ 
			//starts phonenumber 3 and 4 fields value
		        var inputQuantity = [];
		        $(function() {
		          $(".digit-length").each(function(i) {
		            inputQuantity[i]=this.defaultValue;
		             $(this).data("idx",i);
		          });
		          $(".digit-length").on("keyup", function (e) {
		            var $field = $(this),
		                val=this.value,
		                $thisIndex=parseInt($field.data("idx"),10); 
		            if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
		            	$('[data-mz-validationmessage-for="phoneNumber"]').text(Hypr.getLabel('invalidPhoneNumber'));
		                this.value = inputQuantity[$thisIndex];
		                return;
		            } 
		            if (val.length > Number($field.attr("maxlength"))) {
		              val=val.slice(0, 5);
		              $field.val(val);
		            }
		            inputQuantity[$thisIndex]=val;
		            if($(this).val().length==$(this).attr("maxlength")){
		                $(this).next().focus();
		            }
		            
		          });  
		          	var num1 = $('#number1').val();
		            var num2 = $('#number2').val();
		            var num3 = $('#number3').val();
		            var number = num1 + num2 + num3;
		            if(num1 && num2 && num3) {
		            	$('#phoneNumber').val(number);
		            	$('[data-mz-validationmessage-for="phoneNumber"]').text('');
		            } else {
		            	$('#phoneNumber').val('');
		            }
		        });
			
		    };
		
		
		  	  
	});
});