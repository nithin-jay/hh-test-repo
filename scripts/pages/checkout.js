require(["modules/jquery-mozu",
      "underscore", "hyprlive",
      "modules/backbone-mozu",
      "modules/models-checkout",
      "modules/views-messages",
      "modules/cart-monitor",
      'hyprlivecontext',
      'modules/editable-view',
      'modules/preserve-element-through-render',
      'modules/xpress-paypal',
      'modules/models-customer',
      'modules/api',
      'modules/views-cart-quantity-alert-modal',
      'modules/models-tax-calculator',
      'modules/models-shipping-time-lines',
      'creditcardvalidator',
      'modules/models-postal-code-checker',
      'shim!bootstrap[modules/jquery-mozu=jQuery]>jQuery=jQuery>jQuery',
      'pages/preferred-store'],
    function ($, _, Hypr, Backbone, CheckoutModels, messageViewFactory, CartMonitor, HyprLiveContext, EditableView, preserveElements, PayPal, CustomerModels, api, QuantityAlertModalView, TaxCalculatorModel, ShippingTimelineModel, creditcardvalidator,PostalCodeChecker) {

      var user = require.mozuData('user');
      window.currentuser = user;
      var siteContext = HyprLiveContext.locals.siteContext;
      var appliedCouponCodes = [];
      var monerisCheckoutEnvironment = 'QA';
      var monerisPaymentSettings = siteContext.checkoutSettings.externalPaymentWorkflowSettings.find(function (paymentSetting) {
        return paymentSetting.name === Hypr.getThemeSetting('monerisPaymetWorkflowId');
      });
      if (monerisPaymentSettings) {
        monerisCheckoutEnvironment = monerisPaymentSettings.credentials.find(function (credential) {
          return credential.apiName === 'monerisCheckoutEnvironment';
        });

        monerisCheckoutEnvironment = monerisCheckoutEnvironment ? monerisCheckoutEnvironment.value : 'QA';
      }

      var apiContext = require.mozuData('apicontext'),
          ua = navigator.userAgent.toLowerCase(),
          orderType = '',
          shippingTimeline;
      var taxCalculatorModel = new TaxCalculatorModel(),
          warehouseList = Hypr.getThemeSetting('warehousesForSTH'),
          activeWarehouse = warehouseList ? warehouseList.slice(0) : [];

      // EDIT - Moneris Checkout
      var monerisCheckoutInstance = new window.monerisCheckout();
      monerisCheckoutInstance.setMode(monerisCheckoutEnvironment === 'QA' ? 'qa': 'prod');
      monerisCheckoutInstance.setCheckoutDiv("monerisCheckout");

      var CheckoutStepView = EditableView.extend({
        edit: function () {
          var me = this;
          if (this.model.get('phoneNumberMissing')) {
            setTimeout(function () {
              me.model.edit();
              $('.newcredit-card-phone-number').removeClass('hidden');
              me.model.set('customerPhoneNumber', me.model.get('billingContact.phoneNumbers.home'));
              $('.new-address-creditcard-phonenumber').val(me.model.get('billingContact.phoneNumbers.home'));
            }, 500);
          } else {
            this.model.edit();
          }

        },
        next: function (e) {
          // wait for blur validation to complete
          var me = this;
          /* me.editing.savedCard = false; */

          if ((orderType === 'shipToStoreOrder' || orderType === 'mixOrder') && '#step-payment-info' === this.$el.selector) {
            var secondPersonEmailAttr = _.find(me.model.parent.get('orderAttributes'), function (attribute) {
              return attribute.attributeCode === Hypr.getThemeSetting('secondPersonEmailCode');
            });
            //Check if secondPersonEmail field is invalid due to autofill -> set it to N/A
            if ((secondPersonEmailAttr && secondPersonEmailAttr.value && !Backbone.Validation.patterns.email.test(secondPersonEmailAttr.value)) ||
                ($('#checkout-attribute-secondPersonEmail').val() && !Backbone.Validation.patterns.email.test($('#checkout-attribute-secondPersonEmail').val())) ||
                (me.model.parent.get('secondPersonEmail') && !Backbone.Validation.patterns.email.test(me.model.parent.get('secondPersonEmail')))) {
              $('#checkout-attribute-secondPersonEmail').val('N/A');
              me.model.parent.set('secondPersonEmail', 'N/A');
              var newAttr = [];
              me.model.parent.get('orderAttributes').forEach(function (attribute) {
                if (attribute.attributeCode === Hypr.getThemeSetting('secondPersonEmailCode')) {
                  attribute.value = 'N/A';
                  newAttr.push({
                    'fullyQualifiedName': attribute.attributeFQN,
                    'values': [attribute.value]
                  });
                }
              });
              me.model.parent.apiUpdateAttributes(newAttr).then(function (response) {
                me.model.set('attributes', response);
              });
            }
          }
          var storeIdAttr = _.find(me.model.parent.get('orderAttributes'), function (attribute) {
            return attribute.attributeCode === Hypr.getThemeSetting('storeIdCode');
          });
          //Check if secondPersonEmail field is invalid due to autofill -> set it to N/A
          if (storeIdAttr) {
            var newAttrbt = [];
            //var ehfOption = _.findWhere(item.product.options, { 'attributeFQN': ehfAttributeFQN });
            me.model.parent.get('orderAttributes').forEach(function (attribute) {
              if (attribute.attributeCode === Hypr.getThemeSetting('storeIdCode')) {
                if (localStorage.getItem('preferredStore')) {
                  var currentStore = $.parseJSON(localStorage.getItem('preferredStore'));
                  var storeId = currentStore.code;
                  var addDash = storeId.replace(/(\d{4})(?=\d)/g, '$1-');
                  attribute.value = addDash;

                  newAttrbt.push({
                    'fullyQualifiedName': attribute.attributeFQN,
                    'values': [attribute.value]
                  });
                }
              }
              if(attribute.attributeCode === Hypr.getThemeSetting('isSalesOrder')) {
                attribute.value =true;
                newAttrbt.push({
                  'fullyQualifiedName': attribute.attributeFQN,
                  'values': [attribute.value]
                });
              }
            });
            me.model.parent.apiUpdateAttributes(newAttrbt).then(function (response) {
              me.model.set('attributes', response);
            });
          }
          if ('#step-payment-info' === this.$el.selector) {
            if ($('#paypalPhoneNumber').length > 0 && me.model.get('paymentType') === 'PayPalExpress2') {
              if ($.trim($('#paypalPhoneNumber').val()) === '') {
                $('#paypalPhoneNumber').css({
                  "border": "1px solid #cccccc",
                  "background": "#f2dede"
                });
                $('#paypalPhoneNumber').siblings('.mz-validationmessage').text(Hypr.getLabel("phoneMissing"));
                $("body").animate({ scrollTop: $('.paypal-label').offset().top }, "slow");
                return false;
              } else {
                var phoneNumberRegEx = new RegExp(/(^\d{10}$)|(^\+\d{11}$)|(\([0-9]{3}\)+[0-9]{3}\-[0-9]{4})|(^\d{3}-\d{3}-\d{4}$)|(^\d{3}\.\d{3}\.\d{4}$)|(^\d{3}\ \d{3}\ \d{4}$)|(^\(\d{3}\)\ \d{3}\ \d{4}$)|(^\(\d{3}\)\-\d{3}\-\d{4}$)|(^\+\d{1}\ \d{3}\ \d{3}\ \d{4}$)|(^\+\d{1}\-\d{3}\-\d{3}\-\d{4}$)|(^\+\d{1}\.\d{3}\.\d{3}\.\d{4}$)|(^\d{4}\ \d{3}\-\d{4}$)|(^\d{4}\ \d{3}\.\d{4}$)|(^\d{4}\ \d{3}\ \d{4}$)|(^\d{4}\-\d{3}\-\d{4}$)|(^\d{4}\.\d{3}\.\d{4}$)|(^\+\d{1} \(\d{3}\)\d{3}\.\d{4}$)|(^\+\d{1} \(\d{3}\)\d{3}\-\d{4}$)|(^\+\d{1}\(\d{3}\)\d{3}\-\d{4}$)|(^\+\d{1}\(\d{3}\)\d{3}\.\d{4}$)|(^\+\d{1} \(\d{3}\) \d{3}\.\d{4}$)|(^\+\d{1} \(\d{3}\) \d{3}\-\d{4}$)|(^\+\d{1}\(\d{3}\) \d{3}\-\d{4}$)|(^\+\d{1}\(\d{3}\) \d{3}\.\d{4}$)|(^\(\d{3}\) \d{3}\-\d{4}$)/);
                var isPhoneNumberValid = phoneNumberRegEx.test($('#paypalPhoneNumber').val());
                if (!isPhoneNumberValid) {
                  $('#paypalPhoneNumber').css({
                    "border": "1px solid #cccccc",
                    "background": "#f2dede"
                  });
                  $('#paypalPhoneNumber').siblings('.mz-validationmessage').text(Hypr.getLabel("invalidPhoneNumber"));
                  $("body").animate({ scrollTop: $('.paypal-label').offset().top }, "slow");
                  return false;
                } else {
                  me.model.get('billingContact').set('phoneNumbers.home', $('#paypalPhoneNumber').val());
                  me.model.parent.get('billingInfo').get('billingContact').get('address').set('phoneNumbers.home', $('#paypalPhoneNumber').val());
                  me.model.parent.get('fulfillmentInfo').get('fulfillmentContact').get('address').set('phoneNumbers.home', $('#paypalPhoneNumber').val());
                  me.model.setPayPalPhoneNumber($('#paypalPhoneNumber').val());
                }
              }
            }
            var billingPhoneNumber = $.trim($('.creditcard-phonenumber').val());
            var billingPhoneNumberRegEx = new RegExp(/(^\d{10}$)|(^\+\d{11}$)|(\([0-9]{3}\)+[0-9]{3}\-[0-9]{4})|(^\d{3}-\d{3}-\d{4}$)|(^\d{3}\.\d{3}\.\d{4}$)|(^\d{3}\ \d{3}\ \d{4}$)|(^\(\d{3}\)\ \d{3}\ \d{4}$)|(^\(\d{3}\)\-\d{3}\-\d{4}$)|(^\+\d{1}\ \d{3}\ \d{3}\ \d{4}$)|(^\+\d{1}\-\d{3}\-\d{3}\-\d{4}$)|(^\+\d{1}\.\d{3}\.\d{3}\.\d{4}$)|(^\d{4}\ \d{3}\-\d{4}$)|(^\d{4}\ \d{3}\.\d{4}$)|(^\d{4}\ \d{3}\ \d{4}$)|(^\d{4}\-\d{3}\-\d{4}$)|(^\d{4}\.\d{3}\.\d{4}$)|(^\+\d{1} \(\d{3}\)\d{3}\.\d{4}$)|(^\+\d{1} \(\d{3}\)\d{3}\-\d{4}$)|(^\+\d{1}\(\d{3}\)\d{3}\-\d{4}$)|(^\+\d{1}\(\d{3}\)\d{3}\.\d{4}$)|(^\+\d{1} \(\d{3}\) \d{3}\.\d{4}$)|(^\+\d{1} \(\d{3}\) \d{3}\-\d{4}$)|(^\+\d{1}\(\d{3}\) \d{3}\-\d{4}$)|(^\+\d{1}\(\d{3}\) \d{3}\.\d{4}$)|(^\(\d{3}\) \d{3}\-\d{4}$)/);
            var isBillingPhoneNumberValid = billingPhoneNumberRegEx.test(billingPhoneNumber);
            if ($('.new-address-creditcard-phonenumber').length > 0) {
              billingPhoneNumber = $.trim($('.new-address-creditcard-phonenumber').val());
              isBillingPhoneNumberValid = billingPhoneNumberRegEx.test(billingPhoneNumber);
              if (!billingPhoneNumber) {
                $('.new-address-creditcard-phonenumber').css({
                  "border": "1px solid #cccccc",
                  "background": "#f2dede"
                });
                $('.phone-number-msg').css('display', 'block');
                $('.new-address-creditcard-phonenumber').siblings('.mz-validationmessage').text(Hypr.getLabel("phoneMissing"));
              } else {
                if (!isBillingPhoneNumberValid) {
                  $('.new-address-creditcard-phonenumber').css({
                    "border": "1px solid #cccccc",
                    "background": "#f2dede"
                  });
                  $('.phone-number-msg').css('display', 'block');
                  $('.new-address-creditcard-phonenumber').siblings('.mz-validationmessage').text(Hypr.getLabel("invalidPhoneNumber"));
                } else {
                  $('.new-address-creditcard-phonenumber').css({
                    "background": "none"
                  });
                  $('.phone-number-msg').css('display', 'none');
                }
              }
            }
            if (isBillingPhoneNumberValid) {
              this.model.set('billingContact.phoneNumbers.home', billingPhoneNumber);
            }
            $.removeCookie('modalcustomerPhoneNumber', { path: '/' });

            if (!$('#aeroplanInfo').hasClass('hidden')) {
              var isValid = me.validateFields('input[data-mz-validate-aeroplan="validate"]');
              if (isValid) {
                me.model.parent.get('orderAttributes').forEach(function (attribute) {
                  me.setOrderAttributeValue(attribute);
                });
                var updateAttrs = [];
                me.model.parent.get('orderAttributes').forEach(function (attribute) { //push attributes to be updated in updateAttrs array
                  var attrVal = attribute.value;
                  if (attrVal) {
                    updateAttrs.push({
                      'fullyQualifiedName': attribute.attributeFQN,
                      'values': [attrVal]
                    });
                  }
                });
                me.model.parent.apiUpdateAttributes(updateAttrs).then(function (response) {
                  me.model.set('attributes', response);
                });

                //update aeroplan number in customer attribute
                var customer = new CustomerModels.EditableCustomer();
                if (window.order.get("customer").get('id')) {
                  customer.set("id", window.order.get("customer").get('id'));
                  customer.fetch().then(function (response) {
                    if (response.get('attributes')) {
                      var isAeroplanNumberAttribute = _.find(response.get('attributes').toJSON(), function (attribute) {
                        return attribute.fullyQualifiedName === Hypr.getThemeSetting('isAeroplanMember');
                      });
                      if (isAeroplanNumberAttribute) {
                        response.updateAttribute(isAeroplanNumberAttribute.fullyQualifiedName, isAeroplanNumberAttribute.attributeDefinitionId, [true]);
                      }
                      var aeroplanNumberAttribute = _.find(response.get('attributes').toJSON(), function (attribute) {
                        return attribute.fullyQualifiedName === Hypr.getThemeSetting('aeroplanMemberNumber');
                      });
                      if (aeroplanNumberAttribute) {
                        response.updateAttribute(aeroplanNumberAttribute.fullyQualifiedName, aeroplanNumberAttribute.attributeDefinitionId, [me.model.get('aeroplanNumber')]);
                      }
                    }
                  });
                }
              } else return false;
            }
            me.updateReCapchaAttribute();
            me.updateLocationSalesTaxAttribute();
          }
          if ('#step-shipping-address' === this.$el.selector) {
            $.cookie('shippingStepCompleted', 'true', { path: '/' });
            if(me.model.get('address').get('postalOrZipCode')){
              var postalCode = me.model.get('address').get('postalOrZipCode').trim().toUpperCase();
              me.model.get('address').set('postalOrZipCode', postalCode);
            }
            me.selectProvince();
          }
          if ('#step-shipping-method' === this.$el.selector) $.cookie('shippingMethodCompleted', 'true', { path: '/' });
          _.defer(function () {
            me.model.next();
          });
        },
        selectProvince: function(){
          var me = this;
          if(me.model.get('address').get('countryCode') === 'CA') {
            var currentProvince = me.model.get('address').get('stateOrProvince');
            var currentProvinceText = me.$el.find('#stateOrProvinceCA-  option:selected').text();
            var canadaProvinceKeys = [];
            var canadaProvinceNames = [];
            _.each(Hypr.getThemeSetting('canadaStates'), function(province) {
              canadaProvinceKeys.push(province.key);
              canadaProvinceNames.push(province.name);
            });
            var isCanadaProvince = _.contains(canadaProvinceKeys,currentProvince);
            var isCanadaProvinceText = _.contains(canadaProvinceNames,currentProvinceText);
            if(!isCanadaProvince) {
              if(isCanadaProvinceText){
                var valofText = me.$el.find("#stateOrProvinceCA-" + " option").filter(function() {
                  return this.text == currentProvinceText;
                }).val();
                $('#stateOrProvinceCA-').val(valofText);
                currentProvince = me.$el.find('#stateOrProvinceCA-  option:selected').val();
                me.model.get('address').set('stateOrProvince', currentProvince);
              } else {
                _.each($('.mz-addressform-state').children(), function(ele){
                  if($(ele).hasClass('mz-validationmessage')){
                    me.model.get('address').set('stateOrProvince', '');
                    $(ele).siblings('.select-icon-cont').find('select').val(Hypr.getLabel('selectProvince'));
                  }
                });
              }
            }
          }
        },
        updateReCapchaAttribute: function() {
          var invisibleSitekey = Hypr.getThemeSetting('invisibleSitekey');
          grecaptcha.ready(function() {
            grecaptcha.execute(invisibleSitekey, {action: 'eCommerce'}).then(function(token){
              window.order.set('key', token);
            });
          });
        },
        updateLocationSalesTaxAttribute: function(){
          var self = this,
              storeDetails = self.getStoreDetails(),
              locationAttributeData = storeDetails !== '' ? _.findWhere(storeDetails.attributes,{ fullyQualifiedName : Hypr.getThemeSetting('locationSalesTaxAttrFQN')}) : null,
              orderLocationAttribute = self.model.parent.get('orderAttributes') ? _.findWhere(self.model.parent.get('orderAttributes'),{ attributeFQN : Hypr.getThemeSetting('orderLocationSalesTaxAttrFQN')}) : null,
              storeDetailsAttribute = self.model.parent.get('orderAttributes') ? _.findWhere(self.model.parent.get('orderAttributes'),{ attributeFQN : Hypr.getThemeSetting('orderStoreDetailsAttrFQN')}) : null,
              shippingTimelineAttribute = self.model.parent.get('orderAttributes') ? _.findWhere(self.model.parent.get('orderAttributes'),{ attributeFQN : Hypr.getThemeSetting('shippingTimelineAttrFQN')}) : null,
              storeData = self.getStoreData(storeDetails),
              updateAttr = [];

          if(storeData && storeDetailsAttribute){
            updateAttr.push({
              'fullyQualifiedName': storeDetailsAttribute.attributeFQN,
              'values': [storeData]
            });
          }

          if(shippingTimelineAttribute && shippingTimeline){
            updateAttr.push({
              'fullyQualifiedName': shippingTimelineAttribute.attributeFQN,
              'values': [shippingTimeline]
            });
          }

          if(orderLocationAttribute){
            updateAttr.push({
              'fullyQualifiedName': orderLocationAttribute.attributeFQN,
              'values': locationAttributeData ? locationAttributeData.values : 'N/A'
            });
            self.model.parent.apiUpdateAttributes(updateAttr).then(function(response) {
              self.model.set('attributes', response);
            },function(error){
              console.error('Failed to update location sales tax id',error);
            });
          }
        },
        getStoreData: function(storeDetails){
          var storeAddress = storeDetails ? storeDetails.address : null,
              storeDetailsData = '';
          if(storeAddress){
            var storeName = storeDetails.name,
                address1 = storeAddress.address1 !== 'N/A' && storeAddress.address1 !== '' ? storeAddress.address1 : null,
                address2 = storeAddress.address2 !== 'N/A' && storeAddress.address2 !== '' ? storeAddress.address2 : null,
                address3 = storeAddress.address3 !== 'N/A' && storeAddress.address3 !== '' ? storeAddress.address3 : null,
                address4 = storeAddress.address4 !== 'N/A' && storeAddress.address4 !== '' ? storeAddress.address4 : null,
                city = storeAddress.cityOrTown + ' ' + storeAddress.stateOrProvince + ' ' + storeAddress.postalOrZipCode,
                phoneNumber = storeDetails.phone;
            storeDetailsData = storeName + '|' + (address1 ? address1 + '|' : '' ) + (address2 ? address2 + '|' : '') + (address3 ? address3 + '|' : '') + (address4 ? address4 + '|' : '') + city + '|' + phoneNumber;
          }

          return storeDetailsData;
        },
        getStoreDetails: function () {
          var storeData = localStorage.getItem('preferredStore') ? $.parseJSON(localStorage.getItem('preferredStore')) : null;
          return storeData;
        },
        choose: function () {
          var me = this;
          me.model.choose.apply(me.model, arguments);
        },
        constructor: function () {
          var me = this;
          EditableView.apply(this, arguments);
          me.resize();
          setTimeout(function () {
            me.$('.mz-panel-wrap').css({ 'overflow-y': 'hidden' });
          }, 250);
          me.listenTo(me.model, 'stepstatuschange', me.render, me);
          me.$el.on('keypress', 'input', function (e) {
            if (e.which === 13) {
              me.handleEnterKey(e);
              return false;
            }
          });
        },
        initStepView: function () {
          this.model.initStep();
        },
        handleEnterKey: function (e) {
          this.model.next();
        },
        render: function () {
          this.$el.removeClass('is-new is-incomplete is-complete is-invalid is-undefined').addClass('is-' + this.model.stepStatus());
          EditableView.prototype.render.apply(this, arguments);
          this.resize();
        },
        setOrderAttributeValue: function (attribute) {
          var me = this;
          switch (attribute.attributeCode) {
            case Hypr.getThemeSetting('aeroplanMilesCode') :
              if (me.model.get('aeroplanPromoCode')) {
                var milesFactor = me.model.get('aeroplanMilesFactor');
                if (typeof (milesFactor) === 'string') { //if miles are already calculated with promotion and now user editing the step
                  var milesFactorToMultiply = milesFactor.split('X');
                  var aeroplanMiles = (Math.floor(me.model.parent.get('discountedTotal') / 2)) * milesFactorToMultiply[0];
                  me.model.set('aeroplanMiles', aeroplanMiles);
                  $(document).trigger('applyAeroPlanPoints', aeroplanMiles);
                }
              }
              attribute.value = me.model.get('aeroplanMiles').toString();
              break;
            case Hypr.getThemeSetting('aeroplanNumberCode') :
              var aeroplanNumber = $('#checkoutAeroplanNumberInput').val();
              attribute.value = aeroplanNumber;
              var aeroplanNumberPart = '******' + aeroplanNumber.slice(-3);
              me.model.set('aeroplanNumberPart', aeroplanNumberPart);
              break;
            case Hypr.getThemeSetting('aeroplanPromoCode') :
              if (me.model.get('aeroplanPromoCode')) {
                attribute.value = me.model.get('aeroplanPromoCode');
              } else {
                attribute.value = '';
              }
              break;
            case Hypr.getThemeSetting('aeroplanBonusMessage') :
              if (me.model.get('aeroplanPromoCode')) {
                attribute.value = Hypr.getLabel('withPromoAeroplanMsg', me.model.get('aeroplanMiles'));
              } else {
                attribute.value = Hypr.getLabel('defaultAeroplanMsg');
              }
              break;
            case Hypr.getThemeSetting('loyalty_program_points1') :
              attribute.value = (Math.floor(me.model.parent.get('discountedTotal') / 2)).toString();
              break;
            case Hypr.getThemeSetting('loyalty_program_points2') :
              if (me.model.get('aeroplanPromoCode')) {
                var bonusMiles = me.model.get('aeroplanMiles') - Math.floor(me.model.parent.get('discountedTotal') / 2);
                attribute.value = bonusMiles.toString();
              }
              break;
            case Hypr.getThemeSetting('loyalty_promotion_code1') :
              attribute.value = Hypr.getLabel('loyalty_promotion_code1_value');
              break;
          }
        },
        validateFields: function (inputToValidate) {
          var isValid = true;
          $(inputToValidate).each(function () {
            var isAeroplanNumberValid = true;
            if (this.hasAttribute('data-mz-validate-aeroplan-number')) {
              if (!Backbone.Validation.patterns.number.test($(this).val()) || $(this).val().length != 9) {
                isValid = false;
                isAeroplanNumberValid = false;
                $(this).css({
                  "border": "1px solid #EB0029"
                });
                $(this).siblings('.mz-validationmessage').text($(this).attr('validation-msg'));
              }
            }

            if ($.trim($(this).val()) === '' || $(this).val() === 'N/A' || $(this).val() === 'n/a') {
              isValid = false;
              $(this).css({
                "border": "1px solid #EB0029"
              });
              $(this).siblings('.mz-validationmessage').text($(this).attr('validation-msg'));
            } else {
              if (isAeroplanNumberValid) {
                $(this).css({
                  "border": ""
                });
                $(this).siblings('.mz-validationmessage').text('');
              }
            }
          });
          return isValid;
        },
        resize: _.debounce(function () {
          this.$('.mz-panel-wrap').animate({ 'height': this.$('.mz-inner-panel').outerHeight() });
        }, 200)
      });

      var OrderAndCartSummaryView = Backbone.MozuView.extend({
        templateName: 'modules/checkout/checkout-ordercart-summary',
        autoUpdate: [
          'couponCode'
        ],
        additionalEvents: {
          'click .data-layer-productClick': 'sendDataLayer',
          'click .promocode-section': 'showHideCouponSection'
        },
        showHideCouponSection: function (e) {
          if (this.$el.find('.promocode-applysection').css('visibility') == 'hidden') {
            this.$el.find(".promocode-applysection").removeClass("hidden");
            this.$el.find(".promocode-applysection").addClass("show");
          }
          else{
            this.$el.find(".promocode-applysection").removeClass("show");
            this.$el.find(".promocode-applysection").addClass("hidden");
          }
        },
        sendDataLayer: function (e) {
          var breadcrumbStr = "";
          var cookieCategoryArray = [];
          var clickedProductCode = $(e.currentTarget).closest(".datalayer-parent-element").attr("data-mz-product");
          var actionFieldList = $(e.currentTarget).attr("href").split('?page=')[1];
          var getPrice = "";
          var hasSalePrice = $(e.currentTarget).closest(".datalayer-parent-element").find(".mz-item-price.is-saleprice") && $(e.currentTarget).closest(".datalayer-parent-element").find(".mz-item-price.is-saleprice").length > 0;
          if (hasSalePrice) {
            getPrice = $(e.currentTarget).closest(".datalayer-parent-element").find(".is-saleprice").text().split('/')[0];
          } else {
            getPrice = $(e.currentTarget).closest(".datalayer-parent-element").find(".unit-price").text().split('/')[0];
          }
          var getBrand = $(e.currentTarget).closest(".datalayer-parent-element").find(".product-tile-brand").text();
          if ($.cookie('BreadCrumbFlow')) {
            cookieCategoryArray = $.parseJSON($.cookie("BreadCrumbFlow"));
            var Prod = _.findWhere(cookieCategoryArray, { 'productCode': clickedProductCode });
            breadcrumbStr = Prod.flow;
          }
          if (actionFieldList) {
            if (actionFieldList.indexOf('&rrec=true') !== -1) {
              actionFieldList = actionFieldList.split('&rrec=true')[0];
            }
          }
          dataLayer.push({
            'event': 'productClick',
            'ecommerce': {
              'click': {
                'actionField': { 'list': actionFieldList },
                'products': [{
                  'name': $.trim($(e.currentTarget).closest(".datalayer-parent-element").attr("data-mz-productName")),
                  'id': $.trim(clickedProductCode),
                  'price': getPrice ? $.trim(getPrice.replace('$', '').replace(',', '.')) : '',
                  'brand': getBrand ? $.trim(getBrand) : '',
                  'category': $.trim(breadcrumbStr),
                  'position': $(e.currentTarget).closest(".datalayer-parent-element").attr("data-mz-position")
                }]
              }
            }
          });
        },
        initialize: function () {
          var me = this;
          this.bindRefreshEvent();
          var locale = require.mozuData('apicontext').headers['x-vol-locale'];
          var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
          locale = locale.split('-')[0];
          var currentLocale = '';
          if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite) {
            currentLocale = locale === 'fr' ? '/fr' : '/en';
          }
          me.model.set("currentLocale", locale);
          me.model.set("currentSite", currentSite);
          if (localStorage.getItem('preferredStore')) {
            //Set store name for change fulfillment section
            var preferredStoreDetails = $.parseJSON(localStorage.getItem('preferredStore'));
            this.model.set('storeAllowsShipToHome', !!_.find(preferredStoreDetails.attributes,
                function predicate(attribute) {
                  return (attribute.fullyQualifiedName === Hypr.getThemeSetting('shipToHomeStoreFlagAttrFQN') &&
                      attribute.values[0]);
                }));
          }

          var contextSiteId = apiContext.headers["x-vol-site"];
          var frSiteId = Hypr.getThemeSetting("frSiteId");
          if (frSiteId === contextSiteId) {
            me.model.set("isFrenchSite", true);
          }
          this.listenTo(this.model.get('billingInfo'), 'orderPayment', this.onOrderCreditChanged, this);
          if(orderType === "mixOrder" || orderType === "shipToHomeOrder"){
            var shippingCost = $.cookie('shippingCost') ? $.parseJSON($.cookie("shippingCost")).shippingCost : 0;
            this.model.set('shippingCost',shippingCost);
          }

          if (this.model.get('couponCodes').length > 0) {
            this.model.set('appliedCouponCodes', this.model.get('couponCodes'));
            var apiCouponCodesArray = this.model.get('appliedCouponCodes'),
                appliedCouponCode, isStackable,
                orderDiscounts = this.model.get('orderDiscounts');
            apiCouponCodesArray.forEach(function (code,index) {
              appliedCouponCodes.push(code);
              /**
               * Ticket - IWM-2002, IWM-2003 and IWM-1949
               * As per current coupon requirements product discounts logic is not considered for line-item level
               * coupon stacking.
               *
               * Below logic is to hide the stacking coupon UI and just to show applied coupon on the site.
               * If stackable feature is enabled, then according to logic -  stacking coupon UI will be displayed.
               * isStackable true refers stackable feature is enabled.
               */
              if(orderDiscounts.length > 0 ) {
                if(orderDiscounts.length == 1) {
                  appliedCouponCode = orderDiscounts[0].couponCode;
                  isStackable = false;
                } else {
                  isStackable = true;
                  appliedCouponCode = false;
                }
              }
            });
            if(appliedCouponCode) {
              this.model.set("appliedCouponCode", appliedCouponCode);
              this.model.set("isStackable", isStackable);
            } else {
              this.model.set("appliedCouponCode", appliedCouponCode);
              this.model.set("isStackable", isStackable);
            }
          }
          this.listenTo(this.model, 'change:couponCode', this.onEnterCouponCode, this);
          this.codeEntered = !!this.model.get('couponCode');
          me.$el.on('keypress', 'input', function (e) {
            if (e.which === 13) {
              if (me.codeEntered) {
                me.handleEnterKey();
              }
              return false;
            }
          });
        },
        render: function(){
          var me = this, EHFTotal = 0;
          _.each(me.model.get('items'), function (item) {
            var ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
            var ehfOption = _.findWhere(item.product.options, { 'attributeFQN': ehfAttributeFQN });
            var currentProductEHFValue = 0;
            if (ehfOption) {
              if(item.fulfillmentMethod === "Pickup") {
                currentProductEHFValue = ehfOption.shopperEnteredValue * item.quantity;
              }
            }
            if(item.fulfillmentMethod === "Ship"){
              var prices = item.unitPrice;
              var itemPrice = prices.saleAmount ? prices.saleAmount : prices.listAmount;
              if(prices.overrideAmount && prices.overrideAmount !== itemPrice){
                var ehfFees = prices.overrideAmount - itemPrice;
                currentProductEHFValue = ehfFees * item.quantity;
              }
            }
            EHFTotal += currentProductEHFValue;
          });
          me.model.set('EHFTotal', EHFTotal);
          me.model.set('EHFTotalToSubtract', EHFTotal);
          if((orderType === "mixOrder" || orderType === "shipToHomeOrder") && this.model.get('taxData')){
            var customTaxes = taxCalculatorModel.getCustomTaxes(this.model.get('taxData'), orderType);
            this.model.set('customTaxData', customTaxes);
          }

          var items = me.model.get('items');
          var firstItemQuantity = 0,
              shipToStoreItemQuantity = 0,
              shipToHomeItemQuantity = 0,
              shipToStoreItems = [],
              shipToHomeItems = [];
          _.each(items, function (item, index) {
            if (item.product.productCode != 'EHF101') {
              firstItemQuantity += item.quantity;
            }
            if (item.fulfillmentMethod == 'Ship') {
              me.setEhfAmount(item);
              shipToHomeItems.push(item);
              shipToHomeItemQuantity += item.quantity;
            } else {
              shipToStoreItems.push(item);
              shipToStoreItemQuantity += item.quantity;
            }
          });
          me.model.set('totalItemsQuantity', firstItemQuantity);
          me.model.set('shipToStoreItems', shipToStoreItems);
          me.model.set('shipToHomeItems', shipToHomeItems);

          me.model.set('totalShipToStoreItemQuantity', shipToStoreItemQuantity);
          me.model.set('totalShipToHomeItemQuantity', shipToHomeItemQuantity);

          Backbone.MozuView.prototype.render.apply(this);
        },
        onEnterCouponCode: function (model, code) {
          if (code && !this.codeEntered) {
            this.codeEntered = true;
            this.$el.find('.apply-btn').prop('disabled', false);
          }
          if (!code && this.codeEntered) {
            this.codeEntered = false;
            this.$el.find('.apply-btn').prop('disabled', true);
          }
        },
        handleEnterKey: function () {
          this.addCoupon();
        },
        addCoupon: function (e) {
          var self = this;
          this.$el.find('.apply-btn').addClass('is-loading');
          this.model.set('couponCode', this.model.get('couponCode'));
          this.model.addCoupon().ensure(function () {
            self.$el.find('.apply-btn').removeClass('is-loading');
            if (!self.model.get('invalidCouponCode')) {
              appliedCouponCodes.push(self.model.get('couponCode'));
              self.model.set('appliedCouponCodes', appliedCouponCodes);
            }
            self.model.unset('couponCode');
            self.render();
          });
        },
        removeCoupon: function (e) {
          var couponCode = $(e.currentTarget).attr('data-mz-value');
          var self = this,appliedCouponCode, isStackable;
          this.$el.find('.apply-btn').addClass('is-loading');
          this.model.removeCoupon(couponCode).ensure(function () {
            self.$el.find('.apply-btn').removeClass('is-loading');
            if (appliedCouponCodes) {
              appliedCouponCodes = _.without(appliedCouponCodes, couponCode);
              self.model.set('appliedCouponCodes', appliedCouponCodes);
              var orderDiscounts = self.model.get('orderDiscounts');
              /**
               * Ticket - IWM-2002, IWM-2003 and IWM-1949
               * As per current coupon requirements product discounts logic is not considered for line-item level
               * coupon stacking.
               *
               * Below logic is to hide the stacking coupon UI and just to show applied coupon on the site.
               * If stackable feature is enabled, then according to logic -  stacking coupon UI will be displayed.
               * isStackable true refers stackable feature is enabled.
               */
              if(orderDiscounts.length > 0 ) {
                if(orderDiscounts.length == 1) {
                  appliedCouponCode = orderDiscounts[0].couponCode;
                  isStackable = false;
                } else {
                  isStackable = true;
                  appliedCouponCode = false;
                }
              }
              if(appliedCouponCode) {
                self.model.set("appliedCouponCode", appliedCouponCode);
                self.model.set("isStackable", isStackable);
              } else {
                self.model.set("appliedCouponCode", appliedCouponCode);
                self.model.set("isStackable", isStackable);
                self.model.set("invalidCoupon", false);
              }
            } else {
              self.model.set('appliedCouponCodes', '');
            }
            self.render();
          });
        },
        editCart: function () {
          window.location = (HyprLiveContext.locals.siteContext.siteSubdirectory || '') + "/cart";
        },

        onOrderCreditChanged: function (order, scope) {
          this.render();
        },

        setEhfAmount: function(item) {
          var prices = item.unitPrice;
          var itemPrice = prices.saleAmount ? prices.saleAmount : prices.listAmount;
          if(prices.overrideAmount && prices.overrideAmount !== itemPrice){
            var ehfFees = prices.overrideAmount - itemPrice;
            item.ehfAmt = {
              shopperEnteredValue : ehfFees
            };
          }
        },
        bindRefreshEvent: function () {
          var self = this;
          $(document).on('refreshOrderSummery',function(){
            self.render();
          });
          $(document).on('enableSubmitOrderBtn',function(event, data){
            self.applyDataAndRender(data, 'btnEnable');
          });
          $(document).on('applyAeroPlanPoints',function(event, data){
            self.applyDataAndRender(data, 'aeroplan');
          });
        },
        applyDataAndRender: function(data, eventType) {
          if(eventType === 'aeroplan') {
            this.model.set('aeroplanMiles', data);
          } else {
            this.model.set('showSubmitOrderBtn', data);
          }
          Backbone.MozuView.prototype.render.apply(this);
        },
        submit: function () {
          var self = this;
          this.setOrderContactEmail();
          if(this.model.get('minOrderAmount') > this.model.get('total')){
            return false;
          } else {
            self.checkItemAvailableInventory();
          }
        },
        setOrderContactEmail: function() {
          var emailFQN = _.findWhere(this.model.get('attributes'), { "fullyQualifiedName": Hypr.getThemeSetting("firstPersonEmailFQN") }),
              email = '';
          if(emailFQN) {
            email = _.first(emailFQN.values);
          } else if (!user.isAnonymous && user.isAuthenticated) {
            email = user.email;
          }
          this.model.set('email', email);
        },
        checkItemAvailableInventory: function(){
          var self = this;
          if(localStorage.getItem('preferredStore')){
            var currentStore = $.parseJSON(localStorage.getItem('preferredStore')),
                isBOHStore = _.find(currentStore.attributes, function(attribute){
                  return attribute.fullyQualifiedName === Hypr.getThemeSetting('hhBOHStoreAttr');
                }),
                currentStoresWarehouse = _.find(currentStore.attributes, function(attribute){
                  return attribute.fullyQualifiedName === Hypr.getThemeSetting('warehouseAttr');
                }),
                productCodesQuery = this.getFilterQuery(),
                isCurrentStoreBoh = isBOHStore ? isBOHStore.values[0] : false;
            self.model.set("isCurrentStoreBoh", isCurrentStoreBoh);

            warehouseList.push(currentStoresWarehouse.values[0]);
            if(currentStoresWarehouse || isCurrentStoreBoh) {
              var data = {
                "locationCodes": warehouseList,
                "productCodes": productCodesQuery
              };

              if(isCurrentStoreBoh) data.locationCodes.push(currentStore.code);

              self.fetchInventories(data).then(function(response){
                var invalidQuantityItemCount = self.checkQuantity(response.items);
                if(invalidQuantityItemCount){
                  self.renderQuantityAlertModal();
                }else{
                  self.submitOrderNew();
                }
              },function(error){
                console.error('failed to get location inventory', error);
              });
            }
          }
        },
        getFilterQuery: function(){
          return this.model.apiModel.data.items.map(function(item) {
            return item.product.productCode;
          });
        },
        fetchInventories : function(data){
          var inventoryApi = '/api/commerce/catalog/storefront/products/locationinventory';
          return api.request("POST", inventoryApi, data);
        },
        //HHOUT-162 Check quantity on cart page, so user can able to place order with available products only.
        checkQuantity: function(fetchedItemInventories){
          var self = this,
              quantityErrorItemsCount = 0;
          _.each(this.model.get('items'),function(item){
            var isQuantityValid = self.validateQuantity(fetchedItemInventories, item);
            if(!isQuantityValid) quantityErrorItemsCount++;
          });
          return quantityErrorItemsCount;
        },
        renderQuantityAlertModal: function(){
          var quantityAlertModalView = new QuantityAlertModalView({
            el: $('#quantityAlertModalContainer'),
            model: new Backbone.Model({
              apiContext: apiContext
            })
          });
          quantityAlertModalView.render();
          $('#quantityAlertModal').modal('show');
        },
        validateQuantity: function(fetchedItemInventory, item){
          var itemInventoryData = _.where(fetchedItemInventory,{ productCode: item.product.productCode }),
              fulfillmentType = item.fulfillmentMethod,
              sthInventory = this.getWarehouseInventory(itemInventoryData),
              bopisInventory = this.getBopisInventory(itemInventoryData),
              isQuantityValid = true;

          if((fulfillmentType === "Ship" && item.quantity > sthInventory) || (fulfillmentType === "Pickup" && item.quantity > bopisInventory)){
            isQuantityValid = false;
          }
          return isQuantityValid;
        },
        getWarehouseBufferInventory: function (stockAvailable) {
          var isWarehouseBufferEnabled = Hypr.getThemeSetting('enableWarehouseBuffer'),
              warehouseBufferValue = Hypr.getThemeSetting('warehouseBufferValue'),
              bufferedWarehouseInventory = 0,
              warehouseCount = stockAvailable && stockAvailable > 0 ? stockAvailable : 0;
          var warehouseInventoryCount = warehouseCount;
          if(isWarehouseBufferEnabled && warehouseBufferValue > 0 && warehouseCount > 1){
            bufferedWarehouseInventory = Math.round((warehouseCount/100) * warehouseBufferValue);
            bufferedWarehouseInventory = bufferedWarehouseInventory < 0.5 ? 1 : bufferedWarehouseInventory;
            warehouseInventoryCount = warehouseCount - bufferedWarehouseInventory;
          }
          return warehouseInventoryCount;
        },
        getWarehouseInventory: function (itemInventoryData) {
          var preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
          var self = this,
              sthWarehouseInventories = [];
          warehouseList = _.unique(warehouseList).filter(function(storeCode) { return storeCode !== preferredStore.code; });
          var activeWarehouseList = activeWarehouse;
          _.each(activeWarehouseList,function(warehouse){
            var sthInventory = _.findWhere(itemInventoryData,{'locationCode':warehouse}).stockAvailable;
            var finalSthInventory = self.getWarehouseBufferInventory(sthInventory);
            sthWarehouseInventories.push(finalSthInventory);
          });
          var sthMaxInventory = _.max(sthWarehouseInventories);
          return sthMaxInventory;
        },
        getBopisInventory: function(itemInventoryData){
          var preferredStore = $.parseJSON(localStorage.getItem('preferredStore')) ? $.parseJSON(localStorage.getItem('preferredStore')):'',
              self = this,
              currentStoresWarehouse = _.find(preferredStore.attributes, function(attribute){
                return attribute.fullyQualifiedName === Hypr.getThemeSetting('warehouseAttr');
              }),
              isBOHStore = _.find(preferredStore.attributes, function (attribute) {
                return attribute.fullyQualifiedName === Hypr.getThemeSetting('hhBOHStoreAttr');
              }),
              storeInventory;
          if(isBOHStore && isBOHStore.values[0]){
            storeInventory = _.findWhere(itemInventoryData,{'locationCode':preferredStore.code}) ? _.findWhere(itemInventoryData,{'locationCode':preferredStore.code}).stockAvailable : 0;
          }
          var warehouseInventory = _.findWhere(itemInventoryData,{'locationCode':currentStoresWarehouse.values[0]}) ? _.findWhere(itemInventoryData,{'locationCode':currentStoresWarehouse.values[0]}).stockAvailable : 0;

          var warehouseBufferInventory = self.getWarehouseBufferInventory(warehouseInventory);
          var bopisInventory = isBOHStore ? storeInventory > 0 ? storeInventory + warehouseBufferInventory : 0 + warehouseBufferInventory : warehouseBufferInventory;
          return bopisInventory;
        },
        submitOrderNew: function(){
          var self = this;
          this.model.set('agreeToTerms', true);
          $.removeCookie('inStorePickUpStep', { path: '/' });  //remove cookie value of inStorePickUpStep once order is placed
          this.model.get('shopperNotes').set('comments', $('#checkoutOrderComments').val());
          _.defer(function () {
            var reCaptchaAttribute = _.find(self.model.get('orderAttributes'), function (attribute) {
              return attribute.attributeCode === Hypr.getThemeSetting('reCaptchaResponseCode');
            });
            if(reCaptchaAttribute) {
              var reCaptchaAttributeValues = [];
              reCaptchaAttribute.value = window.order.get('key');
              reCaptchaAttributeValues.push({
                'fullyQualifiedName': reCaptchaAttribute.attributeFQN,
                'values': [reCaptchaAttribute.value]
              });
              self.model.apiUpdateAttributes(reCaptchaAttributeValues).then(function (response) {
                self.model.set('attributes', response);
              });
            }
            var secondPersonEmailAttr = _.find(self.model.get('orderAttributes'), function (attribute) {
              return attribute.attributeCode === Hypr.getThemeSetting('secondPersonEmailCode');
            });
            //Check if secondPersonEmail field is invalid due to autofill -> set it to N/A
            if ((secondPersonEmailAttr.value && !Backbone.Validation.patterns.email.test(secondPersonEmailAttr.value)) ||
                ($('#checkout-attribute-secondPersonEmail').val() && !Backbone.Validation.patterns.email.test($('#checkout-attribute-secondPersonEmail').val())) ||
                (self.model.get('secondPersonEmail') && !Backbone.Validation.patterns.email.test(self.model.get('secondPersonEmail')))) {
              $('#checkout-attribute-secondPersonEmail').val('N/A');
              self.model.set('secondPersonEmail', 'N/A');
              var newAttr = [];
              self.model.get('orderAttributes').forEach(function (attribute) {
                if (attribute.attributeCode === Hypr.getThemeSetting('secondPersonEmailCode')) {
                  attribute.value = 'N/A';
                  newAttr.push({
                    'fullyQualifiedName': attribute.attributeFQN,
                    'values': [attribute.value]
                  });
                }
              });
              self.model.apiUpdateAttributes(newAttr).then(function (response) {
                self.model.set('attributes', response);
              });
            }
            self.model.submit();
            if (self.model.get('showProcessing')) {
              $("#main-content-loader").show();
              $('.order-loading-msg').show();
              $('body').animate({
                scrollTop: 0
              }, 800);
            }
          });
        }
      });

      var InStorePickUpInfoSummaryView = Backbone.MozuView.extend({
        templateName: 'modules/checkout/instore-pickup-info',
        autoUpdate: [
          'firstPersonFirstName',
          'firstPersonLastName',
          'firstPersonEmail',
          'secondPersonFirstName',
          'secondPersonLastName',
          'secondPersonEmail'

        ],
        additionalEvents: {
          "change [data-mz-second-person-info]": 'showSecondPersonInfo',
          "change [data-mz-order-note-info]": 'showOrderNoteInfo',
          "change #emailNotificationInfo": 'checkSubscribeEmailNotification',
          "click #changeMyStore": "changeMyStore"
        },
        initialize: function () {
          //set inStorePickUpStep value in model based on which pickup information form will be populated on click of edit
          if (this.model.get('attributes') && this.model.get('attributes').length > 0 && this.checkPickupPersonInfo()) {
            this.model.set('inStorePickUpStep', 'complete');
          } else if ($.cookie('inStorePickUpStep')) {
            this.model.set('inStorePickUpStep', $.cookie('inStorePickUpStep'));
            if(!user.isAnonymous && user.isAuthenticated && $.cookie('inStorePickUpStep') === "incomplete") {
              this.updateAttributes();
            }
          } else {
            this.model.set('inStorePickUpStep', 'incomplete');
            if(!user.isAnonymous && user.isAuthenticated ) {
              this.updateAttributes();
            }
          }

          var secondPerson = _.find(this.model.get('attributes'), function (attribute) {
            return attribute.fullyQualifiedName == 'tenant~' + Hypr.getThemeSetting('secondPersonFirstNameCode');
          });
          //set secondPersonRequired value in model to show second pickup person values pre-filled
          if (secondPerson && secondPerson.values[0] != 'N/A') {
            this.model.set('secondPersonRequired', true);
          } else {
            this.model.set('secondPersonRequired', false);
          }
          this.model.set("orderType", this.model.get("orderType"));
          /**
           * Set Subscribe notification check in model to show prefilled value on reload.
           *
           */
          this.model.set('subscribeNotificationCheck', JSON.parse(localStorage.getItem("subscribeNotificationCheck")));
          /**
           * Set orderNoteRequired value in model to show Order Note values pre-filled
           */
          if (_.isEmpty(localStorage.getItem("checkoutOrderNote")) && localStorage.getItem("checkoutOrderNote") != 'N/A') {
            this.model.set('orderNoteRequired', false);
          } else {
            this.model.set('orderNoteRequired', true);
            if(localStorage.getItem("checkoutOrderNote")){
              this.model.get("shopperNotes").set("comments", localStorage.getItem("checkoutOrderNote"));
            }
          }

          if (this.model.get('attributes') && this.model.get('attributes').length > 0 && (this.model.get('inStorePickUpStep') === 'complete')) {
            this.$el.addClass('is-complete');
          } else {
            this.$el.addClass('is-incomplete');
          }
          //set values in orderAttributes to show instore pickup form prefilled if user refreshesh the page in between
          var me = this;
          if (this.model.get('attributes') && this.model.get('attributes').length > 0) {
            me.model.get('attributes').forEach(function (attribute) {
              me.setOrderAttributeValueOnPageReload(attribute);
            });
          }
          if (localStorage.getItem('preferredStore')) {
            var currentStore = $.parseJSON(localStorage.getItem('preferredStore'));
            me.model.set('preferredStore', currentStore);
          }
        },
        updateAttributes: function () {
          var updateAttrs = [], me = this;
          me.model.get('orderAttributes').forEach(function (attribute) { //push attributes to be updated in updateAttrs array
            var attrVal = attribute.value;
            if (attrVal) {
              updateAttrs.push({
                'fullyQualifiedName': attribute.attributeFQN,
                'values': [attrVal]
              });
            } else {
              if(attribute.adminName === "firstPersonFirstName"){
                updateAttrs.push({
                  'fullyQualifiedName': attribute.attributeFQN,
                  'values': [user.firstName]
                });

              } else if(attribute.adminName === "firstPersonLastName") {
                updateAttrs.push({
                  'fullyQualifiedName': attribute.attributeFQN,
                  'values': [user.lastName]
                });

              } else if (attribute.adminName === "firstPersonEmail") {
                updateAttrs.push({
                  'fullyQualifiedName': attribute.attributeFQN,
                  'values': [user.email]
                });

              }
            }
          });
          me.model.apiUpdateAttributes(updateAttrs).then(function (response) {
            me.model.set('attributes', response);
            me.model.set('inStorePickUpStep', 'complete');
            $.cookie('inStorePickUpStep', 'complete', { path: '/' }); //set this value in cookie to access it on page reload
            me.$el.removeClass('is-incomplete').addClass('is-complete');
            me.render();
            _.invoke(window.checkoutViews.steps, 'initStepView');
          });
        },
        checkPickupPersonInfo: function() {
          var firstNameFQN = _.findWhere(this.model.get('attributes'), { "fullyQualifiedName": Hypr.getThemeSetting("firstPersonFirstNameFQN") });
          var firstPerson = firstNameFQN &&_.first(firstNameFQN.values) ? _.first(firstNameFQN.values) : false;
          var lastNameFQN = _.findWhere(this.model.get('attributes'), { "fullyQualifiedName": Hypr.getThemeSetting("firstPersonLastNameFQN") });
          var lastNamePerson = lastNameFQN &&_.first(lastNameFQN.values) ? _.first(lastNameFQN.values) : false;
          var emailFQN = _.findWhere(this.model.get('attributes'), { "fullyQualifiedName": Hypr.getThemeSetting("firstPersonEmailFQN") });
          var email = emailFQN && _.first(emailFQN.values) ? _.first(emailFQN.values) : false;
          return firstPerson && lastNamePerson && email;
        },
        setOrderAttributeValueOnPageReload: function (attribute) {
          this.model.get('orderAttributes').forEach(function (currAttribute) {
            if (currAttribute.attributeFQN === attribute.fullyQualifiedName) {
              currAttribute.value = attribute.values[0];
            }
          });
        },
        setOrderAttributeValue: function (attribute) {
          var me = this;
          switch (attribute.attributeCode) {
            case Hypr.getThemeSetting('firstPersonFirstNameCode') :
              attribute.value = me.model.get('firstPersonFirstName') ? me.model.get('firstPersonFirstName') : $('#checkout-attribute-firstPersonFirstName').val();
              break;
            case Hypr.getThemeSetting('firstPersonLastNameCode') :
              attribute.value = me.model.get('firstPersonLastName') ? me.model.get('firstPersonLastName') : $('#checkout-attribute-firstPersonLastName').val();
              break;
            case Hypr.getThemeSetting('firstPersonEmailCode') :
              attribute.value = me.model.get('firstPersonEmail') ? me.model.get('firstPersonEmail') : $('#checkout-attribute-firstPersonEmail').val();
              break;
            case Hypr.getThemeSetting('secondPersonFirstNameCode') :
              attribute.value = me.model.get('secondPersonFirstName');
              break;
            case Hypr.getThemeSetting('secondPersonLastNameCode') :
              attribute.value = me.model.get('secondPersonLastName');
              break;
            case Hypr.getThemeSetting('secondPersonEmailCode') :
              attribute.value = me.model.get('secondPersonEmail');
              break;
          }
        },
        validateFields: function (inputToValidate) { //validations for pickup form fields using JQuery
          var isValid = true;

          $(inputToValidate).each(function () {
            var isEmailValid = true;
            if (this.hasAttribute('data-mz-validate-email-field')) {
              if (!Backbone.Validation.patterns.email.test($(this).val())) {
                isValid = false;
                isEmailValid = false;
                $(this).css({
                  "border": "1px solid #eb0029"
                });
                $(this).siblings('.mz-validationmessage').text(Hypr.getLabel('emailMissing'));
              }
            }
            if ($.trim($(this).val()) === '' || $(this).val() === 'N/A' || $(this).val() === 'n/a') {
              isValid = false;
              $(this).css({
                "border": "1px solid #eb0029"
              });
              $(this).siblings('.mz-validationmessage').text($(this).attr('validation-msg'));
            } else {
              $(this).css({
                "border": "",
                "background": ""
              });
              if (isEmailValid) {
                $(this).siblings('.mz-validationmessage').text('');
              }
            }
          });
          return isValid;
        },
        edit: function () {
          this.model.set('inStorePickUpStep', 'incomplete');
          $('.submit-order').prop('disabled', true);
          $.cookie('inStorePickUpStep', 'incomplete', { path: '/' }); //set this value in cookie to access it on page reload
          var secondPerson = _.find(this.model.get('attributes'), function (attribute) {
            return attribute && attribute.fullyQualifiedName == 'tenant~' + Hypr.getThemeSetting('secondPersonFirstNameCode');
          });
          if (secondPerson && secondPerson.values[0] != 'N/A') {
            this.model.set('secondPersonRequired', true);
          } else {
            this.model.set('secondPersonRequired', false);
          }

          this.$el.removeClass('is-complete').addClass('is-incomplete');
          this.model.set("orderType", this.model.get("orderType"));
          this.render();
        },
        next: function () {
          var me = this;
          var isValid = false;
          $('.submit-order').prop('disabled', true);
          me.model.set('firstPersonFirstName', $('#checkout-attribute-firstPersonFirstName').val());
          me.model.set('firstPersonLastName', $('#checkout-attribute-firstPersonLastName').val());
          me.model.set('firstPersonEmail', $('#checkout-attribute-firstPersonEmail').val());
          me.model.set('subscribeNotificationCheck', $('#emailNotificationInfo').is(":checked"));
          localStorage.setItem("subscribeNotificationCheck", $('#emailNotificationInfo').is(":checked"));
          if (!$('#second-person-info').hasClass('hidden')) {
            me.model.set('secondPersonFirstName', $('#checkout-attribute-secondPersonFirstName').val());
            me.model.set('secondPersonLastName', $('#checkout-attribute-secondPersonLastName').val());
            me.model.set('secondPersonEmail', $('#checkout-attribute-secondPersonEmail').val());
          }
          me.model.get('orderAttributes').forEach(function (attribute) {
            me.setOrderAttributeValue(attribute);
          });
          isValid = me.validateFields('input[data-mz-validate-field="validate"]');

          if (!$('#second-person-info').hasClass('hidden') && isValid) {
            /**
             * Below code is for following scenario -
             * If Add second person check box is selected and all the fields are empty then
             * no validation is needed.           *
             */
            var isEmpty = true;
            $.each($('input[data-mz-validate-second-person-field="validate"]'),function(index,item){
              if(item.value) {
                isEmpty = false;
              }
            });
            if(!isEmpty) {
              isValid = me.validateFields('input[data-mz-validate-second-person-field="validate"]');
            } else {
              isValid = true;
              me.model.set('secondPersonRequired', false);
            }
          }
          if (!isValid) {
            return false;
          } else {
            this.updateAttributes();
          }
          var customer = new CustomerModels.EditableCustomer();
          if (window.order.get("customer").get('id')) {
            customer.set("id", window.order.get("customer").get('id'));
            var isNameUpdated = false;
            customer.fetch().then(function (response) {
              if (response.get('lastName') === 'N/A' || response.get('lastName') === 'n/a') {
                response.set('lastName', me.model.get('firstPersonLastName'));
                isNameUpdated = true;
              }
              if (response.get('firstName') === 'N/A' || response.get('firstName') === 'n/a') {
                response.set('firstName', me.model.get('firstPersonFirstName'));
                isNameUpdated = true;
              }
              if (isNameUpdated) {
                customer.apiUpdate();
              }
            });
          }
          var productDataList = [];
          var user = require.mozuData('user');
          var cartItems = window.order.get('items');
          for (var i = 0; i < cartItems.length; i++) {
            // addItem should be called for every item in the shopping cart.
            var item = cartItems[i];
            var currAttribute = _.findWhere(item.product.properties, { 'attributeFQN': Hypr.getThemeSetting('brandDesc') });
            var categoryflow;
            var itemIsInCookie = false;
            if ($.cookie('BreadCrumbFlow')) {
              var cookieCategoryArray = $.parseJSON($.cookie("BreadCrumbFlow"));
              var avail = _.findWhere(cookieCategoryArray, { 'productCode': item.product.productCode });
              if (!avail) {
                itemIsInCookie = false;
              } else {
                itemIsInCookie = true;
                categoryflow = avail.flow ? avail.flow : "";
              }

            }
            var qty = parseInt(item.quantity, 10);
            var productPrice = item.discountedTotal / qty;
            var productData = {
              'name': item.product.name,
              'id': item.product.productCode,
              'price': productPrice.toString().replace('$', '').replace(',', '.').trim(),
              'brand': currAttribute ? currAttribute.values[0].stringValue : '',
              'category': itemIsInCookie ? categoryflow : '',
              'quantity': qty
            };
            productDataList.push(productData);
          }
          dataLayer.push({
            'event': 'checkout',
            'ecommerce': {
              'checkout': {
                'actionField': {
                  'step': 2,
                  'option': 'Credit Card'
                },
                'products': productDataList
              }
            }
          });

          /**
           * Adding Order Notes data as shopper notes.
           * Since the order note is not part of order note update,
           * due to which after refresh the order note data is lost. To resolve this shopper note value is stored
           * in local storage.
           */
          if($("#orderNoteInfo").is(":checked") && $('#order-note-text').val()) {
            me.model.get('shopperNotes').set('comments', $('#order-note-text').val());
            localStorage.setItem("checkoutOrderNote", $('#order-note-text').val());
          } else {
            me.model.get('shopperNotes').set('comments', '');
            localStorage.setItem("checkoutOrderNote", '');
          }
          this.model.set("orderType", this.model.get("orderType"));

          /**
           * Subscribe to email notification for promotions.
           */
          if($("#emailNotificationInfo").is(':checked')) {
            var relationOneAuthData = this.relationOneAuthData(),
                relationOneRequestData = this.relationOneRequestData();
            if(localStorage.getItem('preferredStore')){
              if(relationOneRequestData) {
                $.ajax({
                  method: 'POST',
                  headers: relationOneAuthData.headers,
                  contentType: 'application/json; charset=utf-8',
                  url: relationOneAuthData.relation1InsertRecord,
                  data: JSON.stringify(relationOneRequestData),
                  success:function(response){
                    console.info("Suscribed to email notification!", response);
                  },
                  error:function(error){
                    console.log(error);
                  }
                });
              }
            }
          }
        },
        modifiedLocale: function () {
          var locale = require.mozuData('apicontext').headers['x-vol-locale'];
          locale = locale.split('-')[0];
          if(locale === "fr"){
            locale = "French";
          } else {
            locale = "English";
          }
          return locale;
        },
        relationOneAuthData: function() {
          var authData={},b_token;
          authData.headers = {};
          if(Hypr.getThemeSetting('selectedEnvironment') === 'PROD') {
            authData.relation1InsertRecord = Hypr.getThemeSetting('relation1InsertRecordPROD');
            b_token =  Hypr.getThemeSetting('bTokenRelation1Prod');
          } else {
            authData.relation1InsertRecord = Hypr.getThemeSetting('relation1InsertRecordDEV');
            b_token =  Hypr.getThemeSetting('bTokenRelation1Dev');
          }
          authData.headers.Authorization = b_token;
          return authData;
        },
        relationOneRequestData: function() {
          var email = $('#checkout-attribute-firstPersonEmail').val(),me=this,
              emailReg = Backbone.Validation.patterns.email,
              emailFormat = emailReg.test(email),
              postalCode = $.parseJSON(localStorage.getItem('preferredStore')).address.postalOrZipCode,
              currentDate = new Date(), timeStamp = currentDate.getFullYear() + '-' + (currentDate.getMonth() + 1) + '-' + (currentDate.getDate()) + "-" + currentDate.getHours() + ':' + ((currentDate.getMinutes() < 10) ? ("0" + currentDate.getMinutes()) : (currentDate.getMinutes())) + ':' + ((currentDate.getSeconds() < 10) ? ("0" + currentDate.getSeconds()) : (currentDate.getSeconds())),
              requestData = {};
          if(email && emailFormat) {
            requestData.EmailAddress = email;
            requestData.FirstName = " ";
            requestData.PostalCode = postalCode;
            requestData.Preferred_Language = me.modifiedLocale();
            requestData.Is_Contractor = "No";
            requestData.Source = "HH Website";
            requestData.ConsentType = 'explicit';
          }
          return requestData;
        },
        showSecondPersonInfo: function (e) {
          var me = this;
          $('#second-person-info').toggleClass('hidden');
          if ($('#second-person-info').hasClass('hidden')) { //if user unchecks second person info in edit, set attributes values as "N/A" and hide on UI
            me.model.set('secondPersonRequired', false);
            me.model.set('secondPersonFirstName', 'N/A');
            me.model.set('secondPersonLastName', 'N/A');
            me.model.set('secondPersonEmail', 'N/A');
          } else {
            me.model.set('secondPersonRequired', true);
            me.model.set('secondPersonFirstName', '');
            me.model.set('secondPersonLastName', '');
            me.model.set('secondPersonEmail', '');
            me.model.get('orderAttributes').forEach(function (attribute) {
              me.setOrderAttributeValue(attribute);
            });
          }
          me.render();
        },
        showOrderNoteInfo: function () {
          var me = this;
          $('#order-note').toggleClass('hidden');
          if ($('#order-note').hasClass('hidden')) {
            me.model.set('orderNoteRequired', false);
          } else {
            me.model.set('orderNoteRequired', true);
          }
        },
        checkSubscribeEmailNotification: function (e) {
          var me = this;
          if (this.$el.find("#emailNotificationInfo").is(":checked")) {
            me.model.set('subscribeNotificationCheck', true);
          } else {
            me.model.set('subscribeNotificationCheck', false);
          }
        },
        changeMyStore: function (e) {
          e.preventDefault();
          var locale = require.mozuData('apicontext').headers['x-vol-locale'];
          locale = locale.split('-')[0];
          var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
          var currentLocale = '';
          if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite) {
            currentLocale = locale === 'fr' ? '/fr' : '/en';
          }
          window.location.href = currentLocale + "/store-locator?returnUrl=/cart";
        },
        callStore: function (e) {
          var screenWidth = window.matchMedia("(max-width: 767px)");
          if (!screenWidth.matches) {
            e.preventDefault();
          }
        }
      });

      var ShippingAddressView = CheckoutStepView.extend({
        templateName: 'modules/checkout/step-shipping-address',
        autoUpdate: [
          'firstName',
          'lastNameOrSurname',
          'address.address1',
          'address.address2',
          'address.address3',
          'address.cityOrTown',
          'address.countryCode',
          'address.stateOrProvince',
          'address.postalOrZipCode',
          'address.addressType',
          'phoneNumbers.home',
          'contactId',
          'email'
        ],
        renderOnChange: [
          'address.countryCode',
          'contactId'
        ],
        additionalEvents: {
          "keyup [name='shippingphone']": "phoneNumberFormatter",
          "keyup #postal-code-": "postalCodeFormat",
          "keyup #address-line-1-": "removeAutocomplete"
        },
        beginAddContact: function () {
          this.model.set('contactId', 'new');
        },
        initialize: function() {
          var stateOrProvince = this.model.get('address') ? this.model.get('address').get('stateOrProvince') : null;
          if(stateOrProvince && orderType !== "shipToStoreOrder"){
            if ((user.isAnonymous && !user.isAuthenticated) || (!this.model.contacts() || this.model.contacts().length === 0)){
              this.model.parent.parent.set('isSameBillingShippingAddress', true);
              this.model.set("isCheckboxChecked", true);
              this.model.set('isAddressSubmit', false);
              this.initiateUpdateItemPrice();
            }else{
              this.model.parent.parent.set('isSameBillingShippingAddress', false);
              this.model.set("isCheckboxChecked", false);
              this.model.set('isAddressSubmit', true);
              this.completeShippingStep();
            }
          }
          var savedPhoneNumber = this.model.get('phoneNumbers').get('home');
          if(savedPhoneNumber){
            this.phoneNumberFormatter('',savedPhoneNumber);
          }
          var savedPostalCode = this.model.get('address').get('postalOrZipCode');
          if(savedPostalCode){
            this.postalCodeFormat('',savedPostalCode);
          }
        },
        render: function () {
          var self = this;
          var user = require.mozuData('user'),
              locale = require.mozuData('apicontext').headers['x-vol-locale'];
          this.model.set('stepNumber', orderType === 'mixOrder' ? 2 : 2);
          this.model.set('orderType', orderType);
          this.model.set('currentLocale', locale);
          this.model.get('address').set("countryCode",'CA');

          if ((user.isAnonymous && !user.isAuthenticated) || (!this.model.contacts() || this.model.contacts().length === 0)) {
            this.model.set('showAddressComplete', true);
            this.checkAddressExistsInModel();
          }

          CheckoutStepView.prototype.render.apply(self, arguments);
          if(orderType !== "shipToStoreOrder") this.initializeAddressComplete(this);
        },

        phoneNumberFormatter : function(e,phoneNumber){
          var phoneNumberString = phoneNumber ? phoneNumber : this.$el.find("#phonenumber-").val();
          var cleaned = ('' + phoneNumberString).replace(/\D/g, ''),
              match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
          if (match) {
            var phone = '(' + match[1] + ') ' + match[2] + '-' + match[3];
            this.$el.find("#phonenumber-").val(phone);
            this.model.get('phoneNumbers').set('home',phone);
          }
          return null;
        },
        postalCodeFormat : function(e,postalCodeChanged){
          var postalCode = postalCodeChanged ? postalCodeChanged : this.$el.find("#postal-code-").val();
          if (postalCode && e.keyCode != 8) {
            var postalCodeFormatted = postalCode.toUpperCase().replace(/\W/g,'').replace(/(...)/,'$1 ');
            this.$el.find("#postal-code-").val(postalCodeFormatted);
            this.model.get('address').set('postalOrZipCode',postalCodeFormatted);
            this.checkInvalidPostalError();
            this.validationToPostalCode();
          }
        },
        removeAutocomplete: function(){
          this.$el.find("#address-line-1-").attr('autocomplete','random');
        },
        completeShippingStep: function(){
          var contactObj = this.model.contacts();
          var isPrimaryaddress = contactObj[0].types[0].isPrimary;
          if(isPrimaryaddress){
            this.model.set('address',contactObj[0].address);
            this.model.get('phoneNumbers').set('home',contactObj[0].phoneNumbers.home);
            this.phoneNumberFormatter(contactObj[0].phoneNumbers.home);
            this.postalCodeFormat(contactObj[0].address.postalOrZipCode);
          }
          this.initiateUpdateItemPrice();
        },
        checkAddressExistsInModel: function(){
          if(!$.cookie('addressReset') && orderType !== "shipToStoreOrder"){
            this.model.get('address').clear();
            this.model.get('phoneNumbers').clear();
            this.model.unset('id');
            this.model.unset('firstName');
            this.model.unset('lastNameOrSurname');
            $.cookie('addressReset', true, { path: '/' });
            this.model.stepStatus('incomplete');
          }
        },
        initializeAddressComplete: function (self) {
          $('.pcaautocomplete.pcatext, .pcatext.pcanotification').remove();
          var pca = window.pca;
          window.pca.addressComplete.destroy();
          var fields = [
            { element: "address-line-1-", field: "", mode: pca.fieldMode.SEARCH },
            { element: "country-", field: "CountryName", mode: pca.fieldMode.COUNTRY },
            { element: "city-", field: "City", mode: pca.fieldMode.POPULATE },
            { element: "stateOrProvinceCA-", field: "ProvinceName", mode: pca.fieldMode.POPULATE },
            { element: "postal-code-", field: "PostalCode", mode: pca.fieldMode.POPULATE }
          ];

          var options = {
            key: Hypr.getThemeSetting('cpAddressCompleteKey'),
            countries: { codesList: "CAN" },
            bar: { visible: true, showCountry: true, showLogo: true },
            culture: "en",
            prompt: true,
            search: { maxSuggestions: 5, maxResults: 100 },
            setCursor: true,
            manualEntry: true
          };

          var control = new pca.Address(fields, options);

          pca.addressComplete.listen("options", function (addressOptions) {
            addressOptions.search = {
              maxSuggestions: 5, //the number of initial search results to return
              maxResults: 100  //the maximum number of clickable addresses to return
            };
            addressOptions.minItems = 1; //the minimum size of the list
            addressOptions.maxItems = 100; //the maximum size of the list
          });

          control.listen("populate", function (address) {
            var addressLine1 = address.BuildingNumber + ' ' + address.Street,
                addressLine2 = address.SubBuilding !== "" ? Hypr.getLabel('unit') + ' ' + address.SubBuilding : '';
            self.$el.find('#address-line-1-').val(addressLine1);
            self.$el.find('#address-line-2-').val(addressLine2);
            var addressComplete = {
              address1: addressLine1,
              address2: addressLine2,
              cityOrTown: address.City,
              stateOrProvince: address.ProvinceCode,
              postalOrZipCode: address.PostalCode,
              countryCode: address.CountryIso2,
              countryName: address.CountryName,
              isManualSet: true
            };
            self.model.set('address', addressComplete);

            $('#country-').trigger('change');
            _.delay(function() {
              // Delay is required to set the postal code and state as they are refreshed on country selection.
              $("#stateOrProvinceCA-").val(address.ProvinceCode);
              $("#postal-code-").val(address.PostalCode);
              self.model.set('address', addressComplete);
            }, 200);
          });
          $('#addressComplete').attr('autocomplete','none');
        },
        continueToShippingDetailsStep : function(){
          var validationObj = this.model.validate();
          if (validationObj) {
            this.$el.find(".invalid-postalcode").addClass("hidden");
            this.$el.find(".postalcode-back-to-cart").addClass("hidden");
            this.$el.find(".mz-addressform-phone").removeClass("mz-addressform-phone-number");
            Object.keys(validationObj).forEach(function(key){
              this.trigger('error', {message: validationObj[key]});
            }, this);
            return false;
          }
          this.checkPostalCodeRestrition();
        },
        checkPostalCodeRestrition : function(){
          var self = this;
          var postalCode = self.$el.find("#postal-code-").val(),
              postalCodeChecker = new PostalCodeChecker();
          postalCodeChecker.checkPostalCode(postalCode).then(function(){
            self.submitShippingAddress();
          },function(error){
            error = postalCode;
            postalCode = 'Invalid';
            if(postalCode == 'Invalid'){
              self.$el.find(".invalid-postalcode").removeClass("hidden");
              self.$el.find(".postalcode-back-to-cart").removeClass("hidden");
              self.$el.find("#postal-code-").addClass("is-invalid");
              self.$el.find(".mz-addressform-phone").addClass("mz-addressform-phone-number");
            }
          });
        },
        checkInvalidPostalError:function(){
          var postalCodeError = this.$el.find("#postal-code-").find('.is-invalid');
          if(postalCodeError){
            this.$el.find(".invalid-postalcode").addClass("hidden");
            this.$el.find(".postalcode-back-to-cart").addClass("hidden");
            this.$el.find("#postal-code-").removeClass("is-invalid");
            this.$el.find(".mz-addressform-phone").removeClass("mz-addressform-phone-number");
          }
        },
        validationToPostalCode: function(){
          var postalCode = this.$el.find(".validationMessage").text();
          if(postalCode) this.$el.find("#postal-code-").addClass("is-invalid");
        },
        submitShippingAddress : function(){
          this.$el.find(".invalid-postalcode").addClass("hidden");
          this.$el.find(".postalcode-back-to-cart").addClass("hidden");
          this.$el.find(".mz-addressform-phone").removeClass("mz-addressform-phone-number");
          var shippingAddress = this.model.get('address');
          if(shippingAddress && shippingAddress.get('stateOrProvince')&& this.model.get('orderType') !== "shipToStoreOrder"){
            this.model.set('isAddressSubmit', true);
            this.$el.find('.shipping-next-btn').addClass('is-loading');
            this.initiateUpdateItemPrice();
          } else {
            this.next();
            $('.submit-order').prop('disabled', true);
          }
        },
        checkIsSameBillingShippingAddress:function(){
          var isCheckboxChecked = this.$el.find("#billing-info-checkbox").is(':checked');
          if(isCheckboxChecked){
            this.model.set("isCheckboxChecked",true);
            this.model.parent.parent.set('isSameBillingShippingAddress',true);
          }else{
            this.model.set("isCheckboxChecked",false);
            this.model.parent.parent.set('isSameBillingShippingAddress',false);
          }
        },
        initiateUpdateItemPrice: function(){
          var shippingAddress = this.model.get('address');
          if(shippingAddress && shippingAddress.get('stateOrProvince')){
            var shippingProvince = shippingAddress && shippingAddress.get('stateOrProvince') ? shippingAddress.get('stateOrProvince') :'';
            var shipItems = this.getShipItems();
            if(shipItems.length){
              this.getEhfTemplateData(shippingProvince, shipItems);
            } else {
              this.$el.find('.shipping-next-btn').removeClass('is-loading');
              if(this.model.get('isAddressSubmit')){
                this.next();
                $('.submit-order').prop('disabled', true);
              }
            }
          }
        },
        getShipItems: function() {
          var shipItems = _.filter(this.model.parent.parent.get('items'), function(item){
            return item.fulfillmentMethod === "Ship";
          });
          return shipItems;
        },
        getEhfTemplateData: function(shippingProvince, shipItems) {
          var self = this;
          var entityListForEHF = Hypr.getThemeSetting('entityListForEHF');
          var productFilterQuery = shipItems.map(function (orderItem) {
            return 'productCode eq ' + orderItem.product.productCode;
          }).join(' or ');
          var finalQuery = productFilterQuery + ' and province eq ' + shippingProvince;
          api.get('entityList', {
            listName: entityListForEHF,
            filter: finalQuery
          }).then(function (response) {
            console.log('ehfTemplate', response.data.items);
            self.updateOrderItemPrice(response.data.items, shippingProvince, shipItems);
          }, function(err) {
            self.$el.find('.shipping-next-btn').removeClass('is-loading');
            if(self.model.get('isAddressSubmit'))self.next();
            $('.submit-order').prop('disabled', true);
            console.log('Error while fetching ehf templates', err);
          });
        },
        updateOrderItemPrice: function(ehfTemplates, shippingProvince, shipItems) {
          var self = this;
          var requestPayload = this.getPayload(ehfTemplates, shippingProvince, shipItems),
              requestBody = {
                url: Hypr.getThemeSetting("updateOrderItemDuty"),
                data: JSON.stringify(requestPayload),
                type: "POST",
                cors: true,
                contentType: 'application/json'
              };
          if(requestPayload.items.length) {
            $.ajax(requestBody).done(function(response){
              console.log('updateDuty', response);
              _.delay(function () {
                self.model.parent.parent.apiGet().then(function () {
                  $(document).trigger('getOrder');
                }, function(err) {
                  console.log('Error while getting Order', err);
                });
            }, 4000);
            self.$el.find('.shipping-next-btn').removeClass('is-loading');
              if(self.model.get('isAddressSubmit'))self.next();
            }).fail(function(error){
              self.$el.find('.shipping-next-btn').removeClass('is-loading');
              if(self.model.get('isAddressSubmit'))self.next();
              console.log('Error while updating Item duty', error);
            });
          } else {
            self.$el.find('.shipping-next-btn').removeClass('is-loading');
            if(self.model.get('isAddressSubmit'))self.next();
          }
        },
        getPayload: function(ehfTemplates, shippingProvince, shipItems) {
          var self = this;
          var itemsToUpdate = [];
          _.each(shipItems, function(orderItem){
            var itemEhfData = _.findWhere(ehfTemplates, {id : orderItem.product.productCode +'-'+ shippingProvince});
            var newItemPrice = self.getItemsPriceToUpdate(itemEhfData, orderItem);
            if(newItemPrice) {
              itemsToUpdate.push ({
                orderItemId: orderItem.id,
                price: newItemPrice
              });
            }
          });
          return {
            checkoutOrderId: this.model.parent.parent.get('id'),
            items: itemsToUpdate
          };
        },
        getItemsPriceToUpdate: function(orderItemEhf, orderItem) {
          var itemEHFTotal = orderItemEhf ? parseFloat(orderItemEhf.feeAmt) : 0;
          var newItemPrice = 0;
          if (orderItemEhf) {
            if (orderItem.product.price.salePrice) {
              newItemPrice = parseFloat(orderItem.product.price.salePrice + itemEHFTotal).toFixed(2);
            } else {
              newItemPrice = parseFloat(orderItem.product.price.price + itemEHFTotal).toFixed(2);
            }
          } else if (orderItem.product.price.tenantOverridePrice) {
            if (orderItem.product.price.salePrice) {
              newItemPrice = parseFloat(orderItem.product.price.salePrice);
            } else {
              newItemPrice = parseFloat(orderItem.product.price.price);
            }
          }
          return newItemPrice;
        }
      });

      var ShippingInfoView = CheckoutStepView.extend({
        templateName: 'modules/checkout/step-shipping-method',
        renderOnChange: [
          'availableShippingMethods'
        ],
        additionalEvents: {
          "change [data-mz-shipping-method]": "updateShippingMethod"
        },
        initialize: function () {
          var orderType = this.model.parent.get('orderType'),
              locale = require.mozuData('apicontext').headers['x-vol-locale'];
          this.model.set('currentLocale', locale);
          this.model.set('orderType', orderType);
          this.model.set('isFetchingTimelines', false);
          this.bindRefreshEvent();
        },
        bindRefreshEvent: function () {
          var self = this;
          $(document).on('getOrder',function(){
            self.model.parent.apiGet().then(function () {
              $(document).trigger('refreshOrderSummery');
            });
          });
        },
        render: function () {
          var self = this,
              postalOrZipCode = this.model.get('fulfillmentContact').get('address').get('postalOrZipCode');
          CheckoutStepView.prototype.render.apply(this, arguments);
          var shippingMethodChanged = this.model.get("shippingMethodChanged");
          if(postalOrZipCode && this.model._stepStatus != 'new' && (!this.model.get('isFetchingTimelines') || postalOrZipCode !== this.model.get('previousPostalCode'))){
            this.model.set('isFetchingTimelines', true);
            this.model.set('previousPostalCode', postalOrZipCode);
            this.fetchInventories().then(function(res){
              self.getAndSetShippingTimelines(postalOrZipCode, res.items);
            }, function(error){
              console.log('Error while getting inventory', error);
            });
          }
          if(this.model.get('orderType') != "shipToStoreOrder") this.postalCodeCookieSet();
        },
        postalCodeCookieSet : function(postalOrZipCode){
          postalOrZipCode = this.model.get('fulfillmentContact').get('address').get('postalOrZipCode');
          if(this.model._stepStatus != 'new' && postalOrZipCode){
            var expiryDate = new Date(),
                cookieStatus = this.model.get("availableShippingMethods").length > 0 ? "Valid" : "Invalid";
            expiryDate.setYear(expiryDate.getFullYear() + 1);
            $.cookie("sthPostalCode",JSON.stringify({ postalCode: postalOrZipCode.toUpperCase(), status: cookieStatus }),{ path: "/" , expires: expiryDate});
          }
        },
        fetchInventories: function () {
          var requestPayload = {
            'locationCodes': Hypr.getThemeSetting('warehousesForSTH'),
            'productCodes': this.getFilterQuery()
          };
          var inventoryApi = '/api/commerce/catalog/storefront/products/locationinventory?time=' + Date.now();
          return api.request('POST', inventoryApi, requestPayload);
        },
        getAndSetShippingTimelines: function(customerPostalCode, shipInventory){
          var self = this,
              shippingModel = new ShippingTimelineModel(),
              shipItems = this.model.get('shipItems');
          shippingModel.getShippingTimeline(customerPostalCode, shipInventory, 'checkout', shipItems).then(function(timelineData){
            console.log('shippingTimeline', timelineData);
            shippingTimeline = timelineData.shippingTimeline;
            self.model.set('sthShippingTimeline', timelineData.shippingTimeline);
            CheckoutStepView.prototype.render.apply(self, arguments);
          }, function(error){
            console.log('Error while getting shipping timelines', error);
          });
        },
        getFilterQuery: function () {
          var productCodes = [],
              shipItems = [];
          _.each(this.model.parent.apiModel.data.items, function(item){
            if(item.fulfillmentMethod === "Ship"){
              productCodes.push(item.product.productCode);
              shipItems.push(item);
            }
          });
          this.model.set('shipItems', shipItems);
          return productCodes;
        },
        updateShippingMethod: function () {
          this.model.updateShippingMethod(this.$('[data-mz-shipping-method]:checked').val());
        },
        checkShippingMethod: function () {
          var ele = this.$el;
          if (ele.hasClass('is-complete')) {
            ele.removeClass('is-complete');
            ele.addClass('is-incomplete');
          }
        }
      });

      var poCustomFields = function () {

        var fieldDefs = [];

        var isEnabled = HyprLiveContext.locals.siteContext.checkoutSettings.purchaseOrder &&
            HyprLiveContext.locals.siteContext.checkoutSettings.purchaseOrder.isEnabled;

        if (isEnabled) {
          var siteSettingsCustomFields = HyprLiveContext.locals.siteContext.checkoutSettings.purchaseOrder.customFields;
          siteSettingsCustomFields.forEach(function (field) {
            if (field.isEnabled) {
              fieldDefs.push('purchaseOrder.pOCustomField-' + field.code);
            }
          }, this);
        }

        return fieldDefs;
      };

      var visaCheckoutSettings = HyprLiveContext.locals.siteContext.checkoutSettings.visaCheckout;
      var pageContext = require.mozuData('pagecontext');
      var currentCardElementId = '';
      var currentSelectedCardId = '';

      var currentContact;
      var BillingInfoView = CheckoutStepView.extend({
        templateName: 'modules/checkout/step-payment-info',
        autoUpdate: [
          'savedPaymentMethodId',
          'paymentType',
          'card.paymentOrCardType',
          'card.cardNumberPartOrMask',
          'card.nameOnCard',
          'card.expireMonth',
          'card.expireYear',
          'card.cvv',
          'card.isCardInfoSaved',
          'card.isDefaultPayMethod',
          'check.nameOnCheck',
          'check.routingNumber',
          'check.checkNumber',
          'isSameBillingShippingAddress',
          'billingContact.firstName',
          'billingContact.lastNameOrSurname',
          'billingContact.address.address1',
          'billingContact.address.address2',
          'billingContact.address.address3',
          'billingContact.address.cityOrTown',
          'billingContact.address.countryCode',
          'billingContact.address.stateOrProvince',
          'billingContact.address.postalOrZipCode',
          'billingContact.phoneNumbers.home',
          'billingContact.email',
          'creditAmountToApply',
          'digitalCreditCode',
          'purchaseOrder.purchaseOrderNumber',
          'purchaseOrder.paymentTerm',
          'couponCode',
          'aeroplanNumber',
          'aeroplanLastName',
          'monerisCardSelection',
          'monerisPaymentCaptured',
          'giftCardNumber',
          'giftCardSecurityCode'
        ].concat(poCustomFields()),
        renderOnChange: [
          'paymentType',
          'isSameBillingShippingAddress',
          'usingSavedCard',
          'savedPaymentMethodId',
          'savedMonerisCards',
          'monerisCardSelection'
        ],
        additionalEvents: {
          "change [data-mz-digital-credit-enable]": "enableDigitalCredit",
          "change [data-mz-digital-credit-amount]": "applyDigitalCredit",
          "change [data-mz-digital-add-remainder-to-customer]": "addRemainderToCustomer",
          "click [name='paymentType']": "resetPaymentData",
          "change [data-mz-purchase-order-payment-term]": "updatePurchaseOrderPaymentTerm",
          "click [data-mz-value='savedPaymentMethodId']": "setCurrentCreditCardNumber",
          "change [data-mz-existing-billing-addr]": 'setBillingAddress',
          "change [data-mz-new-billing-addr]": 'newBillingAddress',
          "change [data-mz-new-billing-addr1]": 'newBillingAddressInModal',
          "change [data-mz-value='card.isDefaultPayMethod']": 'makeDefaultCard',
          "change [data-mz-value='card.expireYear']": "validateExpireYear",
          "change [data-mz-value='card.expireMonth']": "validateExpireMonth",
          "change [data-mz-value='billingContact.address.countryCode']": "changeProvince",
          "keyup [data-mz-value='card.cardNumberPartOrMask']": "detectCardType",
          "change [data-mz-aeroplan-miles]": 'showAeroplanForm',
          "click .paypal-label": "selectPaypal",
          "click .credit-card-form-section": "selectCreditCardMethod",
          "click #payPalRadio": "triggerPaypalScript",
          "click #paypalPhoneNumber": "showPapaylSelected",
          "click #MonerisCheckout": "radioClicked",
          "change [data-mz-gift-card-amount]": "applyGiftCard",
          "click [data-mz-gift-card-enable]": "removeGiftCard",
          "click .gift-cardLink": "toggleGiftCardSection"
        },
        initialize: function () {
          // this.addPOCustomFieldAutoUpdate();
          // var address = this.model.parent.get('fulfillmentInfo').get('fulfillmentContact').get('address').toJSON();
          // this.model.set('billingContact.address', address);
          this.calculateAeroPlanPoints();
          if (!user.isAnonymous && user.isAuthenticated){
            this.fetchSavedCards(true);
            this.model.set('hidePaymentStepEle', true);
          }
          this.attachMonerisEvents();
          this.listenTo(this.model, 'change:paymentType', this.getSavedPaymentMethods, this);

          var customerPhoneNumber = $.trim(this.model.get('billingContact.phoneNumbers.home'));
          this.model.set('initialCustomerPhoneNumber', customerPhoneNumber);
          this.listenTo(this.model, 'change:digitalCreditCode', this.onEnterDigitalCreditCode, this);
          this.listenTo(this.model, 'change:giftCardNumber', this.onEnterGiftCardInfo, this);
          this.listenTo(this.model, 'change:giftCardSecurityCode', this.onEnterGiftCardInfo, this);
          this.listenTo(this.model, 'orderPayment', function (order, scope) {
            this.render();
          }, this);
          this.listenTo(this.model, 'billingContactUpdate', function (order, scope) {
            this.render();
          }, this);
          this.listenTo(this.model, 'change:savedPaymentMethodId', function (order, scope) {
            if (currentSelectedCardId) {
              this.model.set('savedPaymentMethodId', currentSelectedCardId);
              this.model.set('usingSavedCard', true);
            }
            $('[data-mz-saved-cvv]').val('').change();
            this.render();
          }, this);
          this.codeEntered = !!this.model.get('digitalCreditCode');

          if (this.model.parent.get('couponCodes').length > 0) {
            this.model.set('appliedCouponCodes', this.model.parent.get('couponCodes'));
            var apiCouponCodesArray = this.model.get('appliedCouponCodes'),
                appliedCouponCode, isStackable,
                orderDiscounts = this.model.parent.get('orderDiscounts');
            apiCouponCodesArray.forEach(function (code,index) {
              appliedCouponCodes.push(code);
              /**
               * Ticket - IWM-2002, IWM-2003 and IWM-1949
               * As per current coupon requirements product discounts logic is not considered for line-item level
               * coupon stacking.
               *
               * Below logic is to hide the stacking coupon UI and just to show applied coupon on the site.
               * If stackable feature is enabled, then according to logic -  stacking coupon UI will be displayed.
               * isStackable true refers stackable feature is enabled.
               *  */
              if(orderDiscounts.length > 0 ) {
                if(orderDiscounts.length == 1) {
                  appliedCouponCode = orderDiscounts[0].couponCode;
                  isStackable = false;
                } else {
                  isStackable = true;
                  appliedCouponCode = false;
                }
              }
            });
            if(appliedCouponCode) {
              this.model.set("appliedCouponCode", appliedCouponCode);
              this.model.set("isStackable", isStackable);
            } else {
              this.model.set("appliedCouponCode", appliedCouponCode);
              this.model.set("isStackable", isStackable);
            }
          }
          this.listenTo(this.model, 'change:couponCode', this.onEnterCouponCode, this);
          this.codeEntered = !!this.model.parent.get('couponCode');
          var me = this;
          me.$el.on('keypress', 'input', function (e) {
            if (e.which === 13) {
              if (me.codeEntered) {
                me.handleEnterKey(e);
              }
              return false;
            }
          });

          var currentCardIndicator = $.cookie('currentCardIndicator');
          if (currentCardIndicator) {
            var self = this;
            self.model.set('currentSavedCard', currentCardIndicator); //set current saved card's id to use it to show saved_indicator
            setTimeout(function () {
              $.removeCookie('currentCardIndicator', { path: '/' });
            }, 1000);
          }
          if (me.model.get('paymentType') === 'PayPalExpress2' && _.last(me.model.parent.get('payments')).status === 'Voided') {
            me.model.set('isPaypalStatusVoided', true);
          } else {
            me.model.set('isPaypalStatusVoided', false);
          }
          if ((!me.model.savedPaymentMethods() && me.model.get('paymentType') !== 'PayPalExpress2') || (me.model.get('paymentType') === 'PayPalExpress2' && _.last(me.model.parent.get('payments')).status === 'Voided')) {
            me.model.set('hideCreditCardLink', true);
          }
          //To show first saved credit card selected on page reload
          if ((me.model.savedPaymentMethods() && me.model.get('paymentType') !== 'PayPalExpress2') || (me.model.savedPaymentMethods() && me.model.get('paymentType') === 'PayPalExpress2' && _.last(me.model.parent.get('payments')).status === 'Voided')) {
            setTimeout(function () {
              $('#savedCredits1').attr('checked', true);
              $('#saved-credit-card-info1').addClass('in');
              currentCardElementId = 'saved-credit-card-info1';
              me.model.set('savedPaymentMethodId', $('#saved-payments1').attr('value'));
              me.model.set('usingSavedCard', true);
              me.model.set('hideCreditCardLink', false);
              setTimeout(function () {
                $('.creditcard-phonenumber').on('focus', function () {
                  $('form').attr('autocomplete', 'off');
                });
              }, 500);
            }, 500);
          } else if (me.model.get('paymentType') === 'PayPalExpress2' && _.last(me.model.parent.get('payments')).status !== 'Voided') {
            setTimeout(function () {
              //IF paymentType is paypal then remove checked class of other payment methods and select paypal
              $('#' + currentCardElementId).removeClass('in');
              $('#' + currentCardElementId).parent().find('.payment-method-input').attr('checked', false);
              $('.newCreditCardRadio').prop('checked', false);
              $('.paypal-radio').prop('checked', true);
            }, 1000);
          } else if (me.model.get('paymentType') === 'PayPalExpress2' && _.last(me.model.parent.get('payments')).status === 'Voided') {
            me.model.set('paymentType', 'CreditCard');
            me.model.selectPaymentType(me.model, 'CreditCard');
          }

          if (!this.model.get('billingContact.address')) {
            this.model.set('billingContact.address.countryCode', 'CA'); //for anonymous user
          }

          if (me.model.get('billingContact.phoneNumbers.home') === 'N/A') {
            me.model.get('billingContact').set('phoneNumbers.home', '');
          }
        },
        calculateAeroPlanPoints: function() {
          var me = this;
          var aeroplanMiles = Math.floor((this.model.parent.get('discountedTotal')) / 2);
          this.model.set('aeroplanMiles', aeroplanMiles);
          this.model.set('aeroplanMilesFactor', aeroplanMiles);
          var documentListForAeroplanPromo = Hypr.getThemeSetting('documentListForAeroplanPromo');
          api.get('documentView', { listName: documentListForAeroplanPromo }).then(function (response) {
            if (response.data.items.length > 0) {
              var aeroplanMilesFactor = response.data.items[0].properties.markupFactor;
              me.model.set('aeroplanMilesFactor', aeroplanMilesFactor);
              var promoCode = response.data.items[0].properties.promoCode;
              me.model.set('aeroplanPromoCode', promoCode);
              var milesFactor = aeroplanMilesFactor;
              if (typeof (milesFactor) === 'string') { //if miles are already calculated with promotion and now user editing the step
                var milesFactorToMultiply = milesFactor.split('X');
                var aeroplanMiles = (Math.floor(me.model.parent.get('discountedTotal') / 2)) * milesFactorToMultiply[0];
                me.model.set('aeroplanMiles', aeroplanMiles);
                me.model.parent.set('aeroplanMiles', aeroplanMiles);
              }
              me.render();
            }
          });
          me.model.parent.set('aeroplanMiles', aeroplanMiles);

          var customer = new CustomerModels.EditableCustomer();
          if (window.order.get("customer").get('id')) {
            customer.set("id", window.order.get("customer").get('id'));
            customer.fetch().then(function (response) {
              me.model.set('aeroplanLastName', response.get('lastName'));
              if (response.get('attributes')) {
                var aeroplanNumberAttribute = _.find(response.get('attributes').toJSON(), function (attribute) {
                  return attribute.fullyQualifiedName === Hypr.getThemeSetting('aeroplanMemberNumber');
                });
                if (aeroplanNumberAttribute) {
                  me.model.set('aeroplanNumber', aeroplanNumberAttribute.values[0]);
                  me.model.set('aeroplanLastName', response.get('lastName'));
                  me.model.set('aeroplanNumberPart', '******' + aeroplanNumberAttribute.values[0].slice(-3));
                  me.render();
                }
              }
            });
          }
        },
        editStep: function() {
          var isSavedCards = user.isAnonymous && !user.isAuthenticated ? true : this.model.get('savedMonerisCards').length === 0;
          if(this.model.activePayments().length > 0 && ( isSavedCards && this.model.activePayments().length > 0)){
            this.model.set('monarisPayment', false);
            this.model.set('userAddedNewCard', false);
          } else {
            this.model.set('monarisPayment', true);
          }
          this.edit();
        },
        radioClicked: function() {
          this.model.set('monarisPayment', true);
        },
        showPapaylSelected: function (e) {
          e.stopPropagation();
          e.preventDefault();
        },
        showAeroplanForm: function () {
          var me = this;
          $('#aeroplanInfo').toggleClass('hidden');
          if ($('#aeroplanInfo').hasClass('hidden')) {
            me.model.set('aeroplanNumber', 'N/A');
          } else {
            me.model.set('aeroplanNumber', ' ');
            if (me.model.get('aeroplanLastName')) {
              me.model.set('aeroplanLastName', me.model.get('aeroplanLastName'));
            } else {
              me.model.set('aeroplanLastName', ' ');
            }
          }
        },
        onEnterCouponCode: function (model, code) {
          if (code && !this.codeEntered) {
            this.codeEntered = true;
            this.$el.find('button').prop('disabled', false);
          }
          if (!code && this.codeEntered) {
            this.codeEntered = false;
            this.$el.find('button').prop('disabled', true);
          }
        },
        deleteAllCardClasses: function (e) {
          var classNames = $(e.currentTarget).siblings('.defaultCardImg').attr("class").split(" ");
          var newclasses = [];
          var classToRemove = '';
          for (var i = 0; i < classNames.length; i++) {
            classToRemove = classNames[i].search(/show+/);
            if (classToRemove) newclasses[newclasses.length] = classNames[i];
          }
          return newclasses;
        },

//    	mastercard 51-55,2221-2720
//    	visa 4
//    	amex 34,37
//    	discover 6011, 622126-622925, 644-649, 65
        addCoupon: function (e) {
          // add the default behavior for loadingchanges
          // but scoped to this button alone
          var self = this;
          this.$el.addClass('is-loading');
          this.model.parent.set('couponCode', this.model.get('couponCode'));
          this.model.parent.addCoupon().ensure(function () {
            self.$el.removeClass('is-loading');
            if (!self.model.get('invalidCouponCode')) {
              appliedCouponCodes.push(self.model.get('couponCode'));
              self.model.set('appliedCouponCodes', appliedCouponCodes);
            }
            self.model.unset('couponCode');
            self.render();
          });
        },
        removeCoupon: function (e) {
          var couponCode = $(e.currentTarget).attr('data-mz-value');
          var self = this,appliedCouponCode, isStackable;
          this.model.parent.removeCoupon(couponCode).ensure(function () {
            if (appliedCouponCodes) {
              appliedCouponCodes = _.without(appliedCouponCodes, couponCode);
              self.model.set('appliedCouponCodes', appliedCouponCodes);
              var orderDiscounts = self.model.parent.get('orderDiscounts');
              /**
               * Ticket - IWM-2002, IWM-2003 and IWM-1949
               * As per current coupon requirements product discounts logic is not considered for line-item level
               * coupon stacking.
               *
               * Below logic is to hide the stacking coupon UI and just to show applied coupon on the site.
               * If stackable feature is enabled, then according to logic -  stacking coupon UI will be displayed.
               * isStackable true refers stackable feature is enabled.
               *  */
              if(orderDiscounts.length > 0 ) {
                if(orderDiscounts.length == 1) {
                  appliedCouponCode = orderDiscounts[0].couponCode;
                  isStackable = false;
                } else {
                  isStackable = true;
                  appliedCouponCode = false;
                }
              }
              if(appliedCouponCode) {
                self.model.set("appliedCouponCode", appliedCouponCode);
                self.model.set("isStackable", isStackable);
              } else {
                self.model.set("appliedCouponCode", appliedCouponCode);
                self.model.set("isStackable", isStackable);
                self.model.set("invalidCoupon", false);
              }
            } else {
              self.model.set('appliedCouponCodes', '');
            }
            self.render();
          });
        },
        resetPaymentData: function (e) {
          $('.paypal-radio').prop('checked', false);
          if (e.target !== $('[data-mz-saved-credit-card]')[0]) {
            $("[name='savedPaymentMethods']").val('0');
          }
          var aeroplanNumber = '', aeroplanLastName = '', aeroplanMiles = '';
          aeroplanMiles = this.model.get('aeroplanMiles');
          if (this.model.get('aeroplanNumber') && this.model.get('aeroplanNumber') != 'N/A') {
            aeroplanNumber = this.model.get('aeroplanNumber');
            aeroplanLastName = this.model.get('aeroplanLastName');
          }
          this.model.clear();
          if (aeroplanNumber) {
            this.model.set('aeroplanNumber', aeroplanNumber);
            this.model.set('aeroplanLastName', aeroplanLastName);
            $('#aeroplanInfo').removeClass('hidden');
            $('#aeroplanMember').prop('checked', true);
          }
          this.model.set('aeroplanMiles', aeroplanMiles);
          this.model.set('usingSavedCard', false);
          this.model.set('paymentType', 'CreditCard');
          this.model.set('card.isCardInfoSaved', true);
          this.model.set('hideCreditCardLink', true);
          $('#saveCreditCcard').attr('checked', true);
          this.model.set('customerAddresses', window.order.get("customer").toJSON()); //set customerAddresses to show all the billing addresses in credit card form
          $('#' + currentCardElementId).removeClass('in');
          $('#' + currentCardElementId).parent().find('.payment-method-input').attr('checked', false);
          this.model.resetAddressDefaults();

          var activePayment = this.model.activePayments();
          if (activePayment.length > 0) {
            this.model.getOrder().get('billingInfo').set('billingContact', activePayment[0].billingInfo.billingContact);
            this.model.get('billingContact').set('contactId', activePayment[0].billingInfo.billingContact.id);
            this.model.set('billingContact', activePayment[0].billingInfo.billingContact);
          }
          if (HyprLiveContext.locals.siteContext.checkoutSettings.purchaseOrder.isEnabled) {
            this.model.resetPOInfo();
          }


          var contactId = this.model.get('billingContact').get('contactId');
          var customerAddresses = this.model.get('customerAddresses');
          currentContact = _.find(customerAddresses.contacts, function (contact) {
            return contact.id == contactId;
          });
          var me = this;
          if (currentContact) {
            setTimeout(function () {
              var modalcurrentContact = $.trim(currentContact.phoneNumbers.home);
              if (modalcurrentContact) {
                $('.modal-phone-number-field').addClass('hidden');
                $('.newcredit-card-phone-number').addClass('hidden');
                me.model.set('phoneNumberMissing', false);
              } else {
                $('.modal-phone-number-field').removeClass('hidden');
                $('.newcredit-card-phone-number').removeClass('hidden');
                me.model.set('phoneNumberMissing', true);
              }
            }, 500);
          }
        },
        setBillingAddress: function (e) {
          var me = this;
          var contactId = $(e.currentTarget).attr('value');
          var customerAddresses = this.model.get('customerAddresses');
          currentContact = _.find(customerAddresses.contacts, function (contact) {
            return contact.id == contactId;
          });
          if ($(e.currentTarget).closest('.modal-dialog').find('.modal-body').length === 0) {
            this.model.getOrder().get('billingInfo').set('billingContact', currentContact);
            this.model.get('billingContact').set('contactId', currentContact.id);
            this.model.set('billingContact', currentContact);
          }
          this.model.set('isAddressFormOpen', false);
          if ($(e.currentTarget).parents('.mz-contactselector ').siblings('.new-billing-address-form').length === 0) { //for edit card modal
            $(e.currentTarget).parents('.mz-contactselector ').siblings('.billing-new-address-container').children('.new-billing-address-form').children('[id^=newBillingAddressInModal]').addClass('hidden');
          } else {
            $(e.currentTarget).parents('.mz-contactselector ').siblings('.new-billing-address-form').children('#newBillingAddress').addClass('hidden');
          }
          var modalcurrentContact = $.trim(currentContact.phoneNumbers.home);
          if (modalcurrentContact) {
            $('.modal-phone-number-field').addClass('hidden');
            $('.newcredit-card-phone-number').addClass('hidden');
            this.model.set('phoneNumberMissing', false);
          } else {
            $('.modal-phone-number-field').removeClass('hidden');
            $('.newcredit-card-phone-number').removeClass('hidden');
            this.model.set('phoneNumberMissing', true);
          }
        },
        resetAddress: function () {
          var address = this.model.get('billingContact').get('address');
          address.set('address1', '');
          address.set('address2', '');
          address.set('cityOrTown', '');
          address.set('stateOrProvince', '');
          address.set('postalOrZipCode', '');
          this.model.get('billingContact').set('phoneNumbers.home', '');
          this.model.get('billingContact').set('firstName', '');
          this.model.get('billingContact').set('lastNameOrSurname', '');
        },
        newBillingAddress: function (e) {
          this.model.get('billingContact').set('contactId', 'new');
          this.resetAddress();
          this.model.set('isAddressFormOpen', true);  //set this value so that on country change form will be shown open after render
          this.render();
          $('#addNewAddress').attr('checked', true);
          $('#newBillingAddress').removeClass('hidden');
        },
        newBillingAddressInModal: function (e) {
          $('.modal-phone-number-field').addClass('hidden');
          this.model.get('billingContact').set('contactId', 'new');
          $(e.currentTarget).parents('.new-billing-address-form').find('input').val('');
          $(e.currentTarget).parents('.new-billing-address-form').find('#country').val('CA');
          $(e.currentTarget).parents('.new-billing-address-form').find('div[class*="province-"]').addClass('hidden');
          $(e.currentTarget).parents('.new-billing-address-form').find('.province-ca').removeClass('hidden').find('select').val('');
          this.resetAddress();
          this.model.set('billingContact.address.countryCode', 'CA');
          $(e.currentTarget).parent().siblings('[id^=newBillingAddressInModal]').toggleClass('hidden');
        },
        beginEditCard: function (e) {
          this.model.set('customerAddresses', window.order.get("customer").toJSON());
          if (this.model.get('initialCustomerPhoneNumber') === '') {
            this.model.set('billingContact.phoneNumbers.home', '');
            setTimeout(function () {
              $('.modal-phone-number-field').removeClass('hidden');
            }, 500);
          }
          this.render();
        },
        cancelEditCard: function (e) {
          $('.step-payment-continue-btn-div').addClass('is-loading');
          window.location.reload();
        },
        updatePurchaseOrderPaymentTerm: function (e) {
          this.model.setPurchaseOrderPaymentTerm(e.target.value);
        },
        showBillingAddresses: function () {
          $('.billing-address-summary').addClass('hidden');
          $('.billing-address-section').toggleClass('hidden');
          setTimeout(function () {
            var selectedBillingNumber = $.trim($('#newBillingContactSelector').find('#autoSelectedBillingAddress').text());
            if (selectedBillingNumber === '') {
              $('.modal-phone-number-field').removeClass('hidden');
            }
          }, 400);
        },
        selectPaypal: function (e) {
          $('.paypal-radio').prop('checked', false);
        },
        triggerPaypalScript: function () {
          $('#btn_xpressPaypal').trigger('click');
        },
        selectCreditCardMethod: function (e) {
          $('.newCreditCardRadio').prop('checked', true);
          $('.paypal-radio').prop('checked', false);
        },
        render: function () {
          var me = this;
          var customerPhoneNumber = $.trim(this.model.get('billingContact.phoneNumbers.home'));
          var orderType = this.model.parent.get('orderType');
          this.model.set('stepNumber', orderType === 'mixOrder' ? 4 : orderType === 'shipToStoreOrder' ? 2 : 4);
          this.model.set('initialCustomerPhoneNumber', customerPhoneNumber);
          this.model.set('currentLocale',window.order.get("currentLocale"));

          if(this.model.set('hidePaymentStepEle')) {
            this.$el.find('.mz-paymentselector').addClass('hidden');
            this.$el.find('#aeroplanSection').addClass('hidden');
            this.model.set('hidePaymentStepEle', false);
          }
          // Moneris flag for already captured payments
          this.model.set('monerisPaymentCaptured', false);

          preserveElements(this, ['.v-button', '.p-button'], function () {
            CheckoutStepView.prototype.render.apply(this, arguments);
          });
          var billingContactEmail = _.find(this.model.parent.get('attributes'), function (attribute) {
            return attribute && attribute.fullyQualifiedName == 'tenant~' + Hypr.getThemeSetting('firstPersonEmailCode');
          });

          var status = this.model.stepStatus();
          if (visaCheckoutSettings.isEnabled && !this.visaCheckoutInitialized && this.$('.v-button').length > 0) {
            window.onVisaCheckoutReady = _.bind(this.initVisaCheckout, this);
            require([pageContext.visaCheckoutJavaScriptSdkUrl]);
            this.visaCheckoutInitialized = true;
          }

          if (this.$(".p-button").length > 0)
            PayPal.loadScript();

          if ('PayPalExpress2' === this.model.get('paymentType')) {
            $('.paypal-radio').prop('checked', true);
          }

          if (this.model.get('usingSavedCard')) {
            $('#' + currentCardElementId).addClass('in');
            $('#' + currentCardElementId).parent().find('.payment-method-input').attr('checked', true);
          }

          if (this.model.parent.get('attributes') && this.model.parent.get('attributes').length > 0 ) {
            this.$el.find('.mz-formstep-body').removeClass('hidden');
          } else {
            this.$el.find('.mz-formstep-body').addClass('hidden');
            this.$el.find('.mz-formstep-edit').addClass('hidden');
            this.$el.removeClass('is-incomplete');
          }


          if (this.model.get('isAddressFormOpen')) {
            $('#addNewAddress').attr('checked', true);
            $('#newBillingAddress').removeClass('hidden');
          }
          if (this.model.get('aeroplanNumber') === 'N/A') {
            $('#checkoutAeroplanNumberInput').val('');
          }
          var cookie = $.cookie('modalcustomerPhoneNumber');
          if (!cookie) {
            me.model.set('initialCustomerPhoneNumber', customerPhoneNumber);
          } else {
            $('.creditcard-phonenumber').val(cookie);
            me.model.set('customerPhoneNumber', cookie);
          }

          if (this.model.get('monerisCardSelection') === 'newMonerisCard') {
            this.initializeMoneris();
          } else if (this.monerisIframeInitialized) {
            this.monerisIframeInitialized = false;
            monerisCheckoutInstance.closeCheckout(this.model.get('monerisCheckoutToken'));
          }
          var is_safari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
          if (is_safari) {
            $(".moneris_checkout").find(".apple-pay").removeClass("hidden");
          }
        },
        savePayment: function(e) {
          var currentTarget = $(e.currentTarget),
              id = currentTarget.attr('id'),
              isChecked = currentTarget.prop('checked');
          if(id == "savePayment" ) {
            this.model.set('saveNewPayment', isChecked);
            if(!isChecked) {
              this.$el.find('#makeDefaultPayment').prop('checked', false);
            }
          } else {
            this.model.set('makeThisPaymentDefault', isChecked);
            if(isChecked){
              this.$el.find('#savePayment').prop('checked', true);
              this.model.set('saveNewPayment', true);
            }
          }
        },
        attachMonerisEvents: function () {
          var me = this;
          monerisCheckoutInstance.setCallback("error_event", function(errorResponse) {
            var response = JSON.parse(errorResponse);
            $('#moneris-billing-address').addClass('hidden');
            $('#moneris-payment-not-avail').removeClass('hidden');
            monerisCheckoutInstance.closeCheckout(response.ticket || me.model.get('monerisCheckoutToken'));
          });

          monerisCheckoutInstance.setCallback("payment_complete", function(paymentSuccessResponse) {
            var response = JSON.parse(paymentSuccessResponse);

            // Receipt API - Check if payment was accepted in moneris or not.
            // If receipt api fails then reinitialize the iframe.
            var isbillingIframe = me.model.parent.get('orderType') === "shipToStoreOrder" ? true : me.model.parent.get('isSameBillingShippingAddress') ? false : true;
            me.$el.find(".new-btn-primary").addClass('is-loading');
            api.request('POST', '/hh/api/preloadOrReceiptApi', {
              amountToCharge: me.model.parent.get('total'),
              isReceiptRequest: true,
              orderNumber: me.model.parent.get('orderNumber'),
              monerisToken: response.ticket,
              checkoutWithBillingAddress: isbillingIframe
            }).then(function successCallback(apiResponse) {
              var paymentDeclined = false;
              if (apiResponse && apiResponse.response && apiResponse.response.receipt &&
                  apiResponse.response.receipt.result === 'a') {
                if (apiResponse.response.receipt.cc &&
                    apiResponse.response.receipt.cc.result &&
                    parseInt(apiResponse.response.receipt.cc.result.response_code, 10) >= 50) {
                  paymentDeclined = true;
                } else if (apiResponse.response.receipt.cc &&
                    !apiResponse.response.receipt.cc.result &&
                    parseInt(apiResponse.response.receipt.cc.response_code, 10) >= 50) {
                  paymentDeclined = true;
                } else if (apiResponse.response.receipt.cc &&
                    apiResponse.response.receipt.cc.fraud.avs.code === 'N') {
                  paymentDeclined = true;
                } else {
                  var expmonth = apiResponse.response.receipt.cc.expiry_date.substring(0,2);
                  var expyear = apiResponse.response.receipt.cc.expiry_date.substring(2,4);
                  apiResponse.response.request.cc.maskedpan = "**** **** **** "+apiResponse.response.receipt.cc.first6last4.substring(6,10);
                  apiResponse.response.request.cc.expireDate = (expmonth+"/"+expyear).toString();
                  apiResponse.response.request.kiboOrder_no = me.model.parent.get('orderNumber');
                  me.model.set('paidViaMoneris', me.model.parent.get('total'));
                  monerisCheckoutInstance.closeCheckout(response.ticket || me.model.get('monerisCheckoutToken'));
                  console.log('apiResponse', apiResponse);
                  me.model.set('newCardMonerisResponse', apiResponse);
                  me.model.set('monerisToken', response.ticket);
                  if (user.isAnonymous && !user.isAuthenticated) {
                    me.model.set('monerisPaymentCaptured',true);
                    me.model.set('cardLastFourDigits', apiResponse.response.receipt.cc.first6last4.substring(6,10));
                    me.model.set('cardExpiry',(expmonth+"/"+expyear).toString());
                    me.model.set('userAddedNewCard', true);
                    me.model.unset('monerisCardSelection');
                    Backbone.MozuView.prototype.render.apply(me, arguments);
                    var isMobile = window.matchMedia("(max-width: 768px)");
                    if(isMobile.matches) me.$el.find(".new-btn-primary").focus();
                  } else {
                    me.reSetSavedCards(false, apiResponse);
                  }
                  me.$el.find(".new-btn-primary").removeClass('is-loading');
                }
              } else {
                paymentDeclined = true;
              }

              if (paymentDeclined) {
                me.$el.find(".new-btn-primary").removeClass('is-loading');
                $('#moneris-billing-address').addClass('hidden');

                // show error that card was declined by moneris.
                var locale = require.mozuData('apicontext').headers['x-vol-locale'];
                me.model.trigger('error', {
                  autoFade: true,
                  message: locale == "fr-CA" ? 'Paiement refusé, veuillez réessayer.' : 'Payment declined please try again.'
                });

                // Payment not accepted - Close and re-open moneris iframe
                monerisCheckoutInstance.closeCheckout(response.ticket || me.model.get('monerisCheckoutToken'));
                me.initializeMoneris();
              }
            }, function errorCallback(error) {
              $('#moneris-billing-address').addClass('hidden');
              $('#moneris-payment-not-avail').removeClass('hidden');

              // Payment not accepted - Close and re-open moneris iframe
              monerisCheckoutInstance.closeCheckout(response.ticket || me.model.get('monerisCheckoutToken'));
              me.initializeMoneris();
            });
          });

          monerisCheckoutInstance.setCallback("cancel_transaction", function (cancelTransactionResponse) {
            var response = JSON.parse(cancelTransactionResponse);
            console.log('cancel_transaction');
            console.log(response);
            monerisCheckoutInstance.closeCheckout(response.ticket);
          });

          monerisCheckoutInstance.setCallback("page_loaded", function(pageLoadResponse) {
            var response = JSON.parse(pageLoadResponse);

            if (response.response_code === '001') {
              var iframeStyle = me.$el.find('#monerisCheckout iframe').attr('style');
              var iframeStyleWithoutHeight = iframeStyle.replace('height: 1890px;', '');
              var isBillingrequiredInIframe = me.model.get('isBillingAddressReq');
              var isMobile = window.matchMedia("(max-width: 768px)");
              var iframeHeight = isBillingrequiredInIframe ? 1430 : 875;
              var iframeHeightMobile = isBillingrequiredInIframe ? 1440 : 890;
              me.$el.find('#monerisCheckout iframe').attr('style', iframeStyleWithoutHeight + 'height:'+ (isMobile.matches ? iframeHeightMobile : iframeHeight) +'px;');
              me.$el.find('#monerisContainer').find('.content-loading').addClass('hidden');
              me.$el.find('#monerisCheckout').attr('style','background-color:white;');
              me.$el.find('#monerisContainer').attr('style', 'margin-bottom:0px;');
              me.$el.find('#moneris-payment-not-avail').addClass('hidden');
            } else {
              me.$el.find('#moneris-payment-not-avail').removeClass('hidden');
            }
          });
        },
        initializeMoneris: _.throttle(function () {
          var me = this;
          this.monerisIframeInitialized = true;
          me.model.set('monerisCheckoutToken', null);
          if (me.model.get('paymentType') !== Hypr.getThemeSetting('monerisPaymetWorkflowId') ||
              !document.getElementById('monerisCheckout')) {
            return;
          }

          // return if amount remaining to capture is 0
          /* if (me.model.parent.get('amountRemainingForPayment') <= 0) {
          return;
        }*/
          api.request('POST', '/hh/api/preloadOrReceiptApi', me.getRequestBody()).then(function successCallback(apiResponse) {
            try {
              var initMonerisCheckout = apiResponse.response && apiResponse.response.ticket;
              if (initMonerisCheckout) {
                console.log('Initializing the moneris checkout flow.');
                monerisCheckoutInstance.startCheckout(apiResponse.response.ticket);
                me.$el.find('#monerisContainer').attr('style', 'margin-bottom:28px');
                me.$el.find('#monerisContainer').find('.content-loading').removeClass('hidden');
                me.model.set('monerisCheckoutToken', apiResponse.response.ticket);
              } else {
                // Show error message on screen
                $('#moneris-billing-address').addClass('hidden');
                $('#moneris-payment-not-avail').removeClass('hidden');
              }
            } catch (err) {
              console.error('Moneris checkout failure');
              console.error(err.stack);

              // Show error message on screen
              $('#moneris-billing-address').addClass('hidden');
              $('#moneris-payment-not-avail').removeClass('hidden');
            }
          }, function monerisGetTokenError(error) {
            console.error(error);

            // Show error message on screen
            $('#moneris-billing-address').addClass('hidden');
            $('#moneris-payment-not-avail').removeClass('hidden');
          });
        }, 100),
        getRequestBody: function() {
          var isBillingAddressIframe = this.model.parent.get('orderType') === "shipToStoreOrder" ? true : this.model.parent.get('isSameBillingShippingAddress') ? false : true;
          var payload = {
            amountToCharge: this.model.parent.get('total'),
            orderNumber: this.model.parent.get('orderNumber'),
            contactDetails: this.contactDetails(),
            checkoutWithBillingAddress: isBillingAddressIframe
          };
          this.model.set('isBillingAddressReq', isBillingAddressIframe);
          if(!isBillingAddressIframe) payload.billingDetails = this.getShippingAddress();
          return payload;
        },
        getShippingAddress: function() {
          var shippingAddressData = this.model.parent.get('fulfillmentInfo').get('fulfillmentContact').get('address'),
              address = shippingAddressData.apiModel.data;
          return {
            address_1: address.address1,
            address_2: address.address2 ? address.address2 : '',
            city: address.cityOrTown,
            province: address.stateOrProvince,
            country: address.countryCode,
            postal_code: address.postalOrZipCode.trim().replace(/ /g, "")
          };
        },
        contactDetails: function() {
          var firstNameFQN = _.findWhere(this.model.parent.get('attributes'), { "fullyQualifiedName": Hypr.getThemeSetting("firstPersonFirstNameFQN") }),
              lastNameFQN = _.findWhere(this.model.parent.get('attributes'), { "fullyQualifiedName": Hypr.getThemeSetting("firstPersonLastNameFQN") }),
              emailFQN = _.findWhere(this.model.parent.get('attributes'), { "fullyQualifiedName": Hypr.getThemeSetting("firstPersonEmailFQN") }),
              firstName = _.first(firstNameFQN.values),
              lastNameOrSurname = _.first(lastNameFQN.values),
              phoneNumber =  this.model.parent.get('orderType') === "shipToStoreOrder" ? '' : this.model.parent.get('fulfillmentInfo').get('fulfillmentContact').get('phoneNumbers').get('home'),
              email = _.first(emailFQN.values);
          return {
            first_name: firstName,
            last_name: lastNameOrSurname,
            email: email,
            phone: phoneNumber
          };
        },
        getSavedPaymentMethods: function () {
          var me = this;
          if (user.isAnonymous && !user.isAuthenticated) {
            console.log('User is not authenticated');
            me.model.set('monerisCardSelection','newMonerisCard');
          }
          if (this.model.get('paymentType') !== Hypr.getThemeSetting('monerisPaymetWorkflowId') ||
              (user.isAnonymous && !user.isAuthenticated)) {
            console.log('User is not authenticated');
            return;
          }

          // return if amount remaining to capture is 0
          /*if (me.model.parent.get('amountRemainingForPayment') <= 0) {
          me.model.set('savedMonerisCards', []);
          return;
        }*/
          this.$el.find('.saved_moneris_cards').addClass('loading');
          this.$el.find('.saved_moneris_cards').find('.content-loading').removeClass('hidden');
          // Set payment type only if moneris option is selected.
          me.model.set({ paymentType: Hypr.getThemeSetting('monerisPaymetWorkflowId') });
          me.model.set({ paymentWorkflow: Hypr.getThemeSetting('monerisPaymetWorkflowId') });
          me.fetchSavedCards(false);
        },
        fetchSavedCards: function(isSubmitPayment) {
          var me = this;
          api.request('POST', '/hh/api/getSavedPaymentMethods').then(function(apiResponse) {
            // show saved cards
            if (apiResponse.status === 'SUCCESS') {
              var savedCardsArray = apiResponse.response.filter(function (cardResp) {
                return cardResp.response && cardResp.response.receipt &&
                    cardResp.response.receipt.ResponseCode &&
                    parseInt(cardResp.response.receipt.ResponseCode, 10) <= 50;
              });
              if (savedCardsArray.length) {
                var activePayments = me.model.parent.apiModel.getActivePayments(),
                    dataKey = '';
                if(activePayments && activePayments.length == 1 && activePayments[0].paymentType == "MonerisCheckout") {
                  dataKey = activePayments[0].data.dataKey ? activePayments[0].data.dataKey : activePayments[0].data.response.receipt.cc.tokenize.datakey;
                }
                for (var s = 0; s < savedCardsArray.length; s++) {
                  var cardData = savedCardsArray[s].response.receipt.ResolveData;
                  savedCardsArray[s].cardNumberPart = cardData.masked_pan.replace(/^.{4}/g, '****');
                  savedCardsArray[s].expireMonth = cardData.expdate.substring(2, 4);
                  savedCardsArray[s].expireYear = cardData.expdate.substring(0, 2);
                  if(savedCardsArray[s].response.receipt.DataKey == dataKey) me.model.set('monerisCardSelection', dataKey);
                }
                me.model.set('monarisPayment', true);
                me.model.set('savedMonerisCards', savedCardsArray);
                if (isSubmitPayment) me.initiateSubmitPayment(savedCardsArray);
              } else {
                me.model.set('savedMonerisCards', []);
                me.model.set('monerisCardSelection', 'newMonerisCard');
                me.addOrRemoveClasses();
              }
            } else {
              me.model.set('savedMonerisCards', []);
              me.model.set('monerisCardSelection', 'newMonerisCard');
              me.addOrRemoveClasses();
            }
            me.reSetSavedCards(true);
          }, function () {
            // Error occurred while fetching saved cards
            console.error(new Error('Error occurred while fetching saved cards'));
            me.model.set('savedMonerisCards', []);
          });
        },
        reSetSavedCards: function(isSessionCheckRequired, monarisApiRes) {
          var sessionCardData;
          if(isSessionCheckRequired) {
            sessionCardData = sessionStorage.getItem('s_d') ? JSON.parse(decodeURIComponent(sessionStorage.getItem('s_d'))) : null;
            if(!sessionCardData) return false;
          }
          var monerisSavedCards = this.model.get('savedMonerisCards') ? this.model.get('savedMonerisCards') : [],
              monarisData = isSessionCheckRequired ? sessionCardData.m_d : monarisApiRes,
              newCard = this.buildCardData(monarisData);
          monerisSavedCards.push(newCard);
          this.model.set('monerisPaymentCaptured', false);
          this.model.set('monerisCardSelection', newCard.response.receipt.DataKey);
          this.model.set('newCardAdded', true);
          this.model.set('newCardMonerisResponse', monarisData);
          this.model.set('savedMonerisCards', monerisSavedCards);
        },
        buildCardData: function(monarisData) {
          var dataKey = monarisData.response.receipt.cc.tokenize.datakey,
              expmonth = monarisData.response.receipt.cc.expiry_date.substring(0,2),
              expyear = monarisData.response.receipt.cc.expiry_date.substring(2,4),
              newCard = {
                newCard: true,
                cardNumberPart: monarisData.response.receipt.cc.first6last4.substring(6,10),
                expireMonth: expmonth,
                expireYear: expyear,
                response: {
                  receipt: {
                    DataKey: dataKey
                  }
                }
              };
          return newCard;
        },
        initiateSubmitPayment: function(savedCards) {
          var activePayments = this.model.parent.apiModel.getActivePayments();
          if(!activePayments.length || (activePayments[0] && activePayments[0].paymentType !== "MonerisCheckout" && activePayments[0].paymentType !== "PayPalExpress2")){
            var validCards = this.getValidCards(savedCards);
            if (!validCards && validCards.length === 0) return false;
            var defaultCard = validCards.filter(function(card) {
              return card.isDefaultCard === true;
            });
            var dataKey = '';

            if(defaultCard instanceof Array && defaultCard.length ){
              dataKey = defaultCard[0].response.receipt.DataKey;
            } else {
              defaultCard = _.last(validCards);
              dataKey = defaultCard.response.receipt.DataKey;
            }
            this.model.set('monerisCardSelection', dataKey);
            this.payWithMonerisSavedCard();
          }
        },
        getValidCards: function(savedCards) {
          var self = this;
          var validCards = savedCards.filter(function(card) {
            return self.checkCardExpiry(card.expireMonth, card.expireYear) === true;
          });
          return validCards;
        },
        submitStep: function() {
          var self = this;
          var cardSelected = this.model.get('monerisCardSelection');
          var cardType = this.$el.find("#saved-card-button-" + cardSelected).data('mz-cardtype');
          var dateKey = cardType === "new" || self.model.get('userAddedNewCard') ? this.model.get('newCardMonerisResponse').response.receipt.cc.tokenize.datakey : this.model.get('monerisCardSelection');
          var isSamePayment = this.checkForSamePayment(dateKey);
          this.$el.find(".new-btn-primary").addClass('is-loading');
          if((cardType === "new" || self.model.get('userAddedNewCard')) && !isSamePayment) {
            var monerisResponse = this.model.get('newCardMonerisResponse');
            var ticket = this.model.get('monerisToken');
            self.model.saveMonerisPaymentDetails(ticket, monerisResponse).then(function(){
              self.next();
              self.$el.find(".new-btn-primary").removeClass('is-loading');
              console.log('Payment Created for new card....!!!');
              if(!self.model.get('saveNewPayment')) self.setDataToSession(cardType, monerisResponse);
              if(cardType === "new" && self.model.get('saveNewPayment')) self.saveCardInformation(monerisResponse);
            }, function(){
              self.model.parent.onCheckoutError();
              self.$el.find(".new-btn-primary").removeClass('is-loading');
            });
          } else if(cardType === "saved" && cardSelected !== '' && !isSamePayment) {
            self.payWithMonerisSavedCard();
          } else {
            self.next();
            if(self.model.get('saveNewPayment')) self.saveCardInformation(this.model.get('newCardMonerisResponse'));
            self.$el.find(".new-btn-primary").removeClass('is-loading');
          }
        },
        saveCardInformation: function(monarisData) {
          var self = this;
          var receipt = monarisData.response.receipt;
          receipt.isDefaultCard = this.model.get('makeThisPaymentDefault');
          api.request('POST', Hypr.getThemeSetting("savedCardDataInEntity"), { receipt: receipt }).then(function() {
            var savedCards = self.model.get('savedMonerisCards'),
                modifiedCards = [];
            _.each(savedCards, function(card) {
              if(card.newCard === true){
                card.newCard = false;
              }
              modifiedCards.push(card);
            });
            self.model.set('newCardAdded', false);
            self.model.set('savedMonerisCards', modifiedCards);
            sessionStorage.removeItem('s_d');
          }, function(error) {
            console.log('Error while saving card data', error);
          });
        },
        setDataToSession: function(cardType, monarisData) {
          var data;
          if (cardType === "new") {
            data = {
              s_c:  true,
              m_t : monarisData.response.receipt.cc.tokenize.datakey,
              m_d: monarisData
            };
          } else {
            var expmonth = monarisData.response.receipt.cc.expiry_date.substring(0,2);
            var expyear = monarisData.response.receipt.cc.expiry_date.substring(2,4);
            data = {
              s_c:  false,
              m_c_l: monarisData.response.receipt.cc.first6last4.substring(6,10),
              m_e: (expmonth+"/"+expyear),
              m_d: monarisData
            };
          }
          sessionStorage.setItem("s_d", encodeURIComponent(JSON.stringify(data)));
        },
        checkForSamePayment: function(currentDateKey) {
          var activePayments = this.model.parent.apiModel.getActivePayments();
          if(!activePayments.length || activePayments[0].paymentType === "CreditCard" || activePayments[0].paymentType === "PayPalExpress2") return false;
          var isSavedPayment = activePayments[0].data.isSavedPayment ? activePayments[0].data.isSavedPayment : false;
          var dataKey = isSavedPayment ? activePayments[0].data.dataKey : activePayments[0].data.response.receipt.cc.tokenize.datakey;
          return currentDateKey === dataKey ? true :  false;
        },
        payWithMonerisSavedCard: function(e) {
          var me = this;
          // Use order id as we dont have external transaction for saved cards
          var externalTransId = this.model.parent.get('id');
          var savedMonerisCards = this.model.get('savedMonerisCards') || [];
          var selectedSavedCard = savedMonerisCards.find(function(savedCard) {
            return savedCard.response.receipt.DataKey === me.model.get('monerisCardSelection');
          });

          var customerId = '';
          try {
            customerId = selectedSavedCard ? selectedSavedCard.response.receipt.ResolveData.cust_id : '';
          } catch (err) {
            customerId = '';
          }

          // check valid Expired Date
          var carddata = selectedSavedCard.response.receipt.ResolveData;
          var currentStore = $.parseJSON($.cookie("preferredStore"));
          var storeId = currentStore.code;
          this.model.set('paidViaMoneris', this.model.parent.get('total'));
          var expireMonth = parseInt(carddata.expdate.substring(2, 4), 10);
          var expireYear = parseInt(carddata.expdate.substring(0, 2), 10);
          var expDateValidation = this.checkCardExpiry(expireMonth, expireYear);
          if (expDateValidation) {
            var dataKey = this.model.get('monerisCardSelection');
            var orderNumber = me.model.parent.get('orderNumber');
            var amount = me.model.parent.get('amountRemainingForPayment');
            var request = { externalTransId: externalTransId, dataKey: dataKey, orderNumber: orderNumber, amount: amount, customerId: customerId, isSavedPayment: true, selectedSavedCard: selectedSavedCard};
            me.model.saveMonerisPaymentDetails(externalTransId, request).then(function(){
              console.log('Payment Created via saved card....!!!');
              me.next();
              me.addOrRemoveClasses();
            }, function() {
              me.model.parent.onCheckoutError();
              me.addOrRemoveClasses();
            });
            me.model.set('monerisPaymentCaptured',false);
          } else {
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            me.model.trigger('error', {
              autoFade: true,
              message: locale == "fr-CA" ? 'Informations de la carte invalides.' : 'Invalid Card Details!.'
            });
            me.addOrRemoveClasses();
          }
        },
        enableGiftCard: function(e){
          var isEnabled = $(e.currentTarget).prop('checked') === true,
              giftCardId = $(e.currentTarget).attr('data-mz-payment-id'),
              targetAmtEl = this.$el.find("input[data-mz-gift-card-target='" + giftCardId + "']"),
              me = this;
  
          if (isEnabled) {
            targetAmtEl.prop('disabled', false);
            me.model.applyGiftCard(giftCardId, null, true);
          } else {
            targetAmtEl.prop('disabled', true);
            me.model.applyGiftCard(giftCardId, 0, false);
          }
        },
        getGatewayGiftCard: function (e) {
          var self = this;
          this.$el.addClass('is-loading');
          this.model.getGatewayGiftCard().ensure(function() {
               self.$el.removeClass('is-loading');
           });
        },
        onEnterGiftCardInfo: function(model) {
          if (model.get('giftCardNumber') && model.get('giftCardSecurityCode')){
            this.$el.find('button.step-gift-card-payment').prop('disabled', false);
          } else {
            this.$el.find('button.step-gift-card-payment').prop('disabled', true);
          }
        },
        applyGiftCard: function(e) {
          var self = this,
              val = $(e.currentTarget).prop('value'),
              giftCardId = $(e.currentTarget).attr('data-mz-gift-card-target');
          if (!giftCardId) {
            return;
          }
          var amtToApply = this.stripNonNumericAndParseFloat(val);
          this.$el.addClass('is-loading');
          return this.model.applyGiftCard(giftCardId, amtToApply, true).then(function(){
              self.$el.removeClass('is-loading');
              this.render();
          }, function(error){
              self.$el.removeClass('is-loading');
          });
        },
        /* getGatewayGiftCard: function (e) {
          var self = this;
          this.$el.addClass('is-loading');
          this.model.getGatewayGiftCard().ensure(function() {
               self.$el.removeClass('is-loading');
           });
        }, */
        removeGiftCard: function(e){
          var giftCardId = $(e.currentTarget).attr('data-mz-payment-id'), me = this;
              me.model.removeGiftCard(e,giftCardId);
        },
        checkCardExpiry: function(expireMonth, expireYear) {
          var currentMonth = new Date().getMonth() + 1;
          var currentYear = new Date().getFullYear().toString().substring(2, 4);
          var isValid = true;
          if (expireYear < currentYear) {
            isValid = false;
          } else if (expireYear == currentYear) {
            if (currentMonth > expireMonth) {
              isValid = false;
            }
          }
          return isValid;
        },
        addOrRemoveClasses: function() {
          this.$el.find(".new-btn-primary").removeClass('is-loading');
          this.$el.find('.mz-paymentselector').removeClass('hidden');
          this.$el.find('#aeroplanSection').removeClass('hidden');
        },
        updateAcceptsMarketing: function (e) {
          this.model.getOrder().set('acceptsMarketing', $(e.currentTarget).prop('checked'));
        },
        updatePaymentType: function (e) {
          var newType = $(e.currentTarget).val();
          this.model.set('usingSavedCard', e.currentTarget.hasAttribute('data-mz-saved-credit-card'));
          this.model.set('paymentType', newType);
        },
        beginEditingCard: function () {
          var me = this;
          if (!this.model.isExternalCheckoutFlowComplete()) {
            this.editing.savedCard = true;
            this.render();
          } else {
            this.cancelExternalCheckout();
          }
        },
        beginEditingExternalPayment: function () {
          var me = this;
          if (this.model.isExternalCheckoutFlowComplete()) {
            this.doModelAction('cancelExternalCheckout').then(function () {
              me.editing.savedCard = true;
              me.render();
            });
          }
        },
        beginEditingBillingAddress: function () {
          this.editing.savedBillingAddress = true;
          this.render();
        },
        beginApplyCredit: function () {
          this.model.beginApplyCredit();
          this.render();
        },
        cancelApplyCredit: function () {
          this.model.closeApplyCredit();
          this.render();
        },
        cancelExternalCheckout: function () {
          var me = this;
          var aeroplanNumber, aeroplanLastName;
          if (me.model.get('aeroplanNumber'))
            aeroplanNumber = me.model.get('aeroplanNumber');
          if (me.model.get('aeroplanLastName'))
            aeroplanLastName = me.model.get('aeroplanLastName');
          this.doModelAction('cancelExternalCheckout').then(function () {
            me.editing.savedCard = false;
            if (me.model.savedPaymentMethods()) {
              $('#savedCredits1').attr('checked', true);
              $('#saved-credit-card-info1').addClass('in');
              currentCardElementId = 'saved-credit-card-info1';
              me.model.set('savedPaymentMethodId', $('#saved-payments1').attr('value'));
              me.model.set('usingSavedCard', true);
            } else {
              $('.paypal-radio').prop('checked', false);
            }
            var aeroplanMiles = Math.floor((me.model.parent.get('discountedTotal')) / 2);
            me.model.set('aeroplanMiles', aeroplanMiles);
            me.model.set('aeroplanNumber', aeroplanNumber);
            me.model.set('aeroplanLastName', aeroplanLastName);
            var documentListForAeroplanPromo = Hypr.getThemeSetting('documentListForAeroplanPromo');
            api.get('documentView', { listName: documentListForAeroplanPromo }).then(function (response) {
              if (response.data.items.length > 0) {
                var aeroplanMilesFactor = response.data.items[0].properties.markupFactor;
                me.model.set('aeroplanMiles', aeroplanMilesFactor);
                var promoCode = response.data.items[0].properties.promoCode;
                me.model.set('aeroplanPromoCode', promoCode);
              }
            });

            var customer = new CustomerModels.EditableCustomer();
            if (window.order.get("customer").get('id')) {
              customer.set("id", window.order.get("customer").get('id'));
              customer.fetch().then(function (response) {
                me.model.set('aeroplanLastName', response.get('lastName'));
                if (response.get('attributes')) {
                  var aeroplanNumberAttribute = _.find(response.get('attributes').toJSON(), function (attribute) {
                    return attribute.fullyQualifiedName === Hypr.getThemeSetting('aeroplanMemberNumber');
                  });
                  if (aeroplanNumberAttribute) {
                    me.model.set('aeroplanNumber', aeroplanNumberAttribute.values[0]);
                    me.model.set('aeroplanLastName', response.get('lastName'));
                    me.render();
                  }
                }
              });
            }
            if (!me.model.savedPaymentMethods() && me.model.get('paymentType') !== 'PayPalExpress2') {
              me.model.set('hideCreditCardLink', true);
            }
            me.render();
          });
        },
        finishApplyCredit: function () {
          var self = this;
          this.model.finishApplyCredit().then(function () {
            self.render();
          });
        },
        removeCredit: function (e) {
          var self = this,
              id = $(e.currentTarget).data('mzCreditId');
          this.model.removeCredit(id).then(function () {
            self.render();
          });
        },
        getDigitalCredit: function (e) {
          var self = this;
          this.$el.addClass('is-loading');
          this.model.getDigitalCredit().ensure(function () {
            self.$el.removeClass('is-loading');
          });
        },
        stripNonNumericAndParseFloat: function (val) {
          if (!val) return 0;
          var result = parseFloat(val.replace(/[^\d\.]/g, ''));
          return isNaN(result) ? 0 : result;
        },
        applyDigitalCredit: function (e) {
          var val = $(e.currentTarget).prop('value'),
              creditCode = $(e.currentTarget).attr('data-mz-credit-code-target');  //target
          if (!creditCode) {
            //console.log('checkout.applyDigitalCredit could not find target.');
            return;
          }
          var amtToApply = this.stripNonNumericAndParseFloat(val);

          this.model.applyDigitalCredit(creditCode, amtToApply, true);
          this.render();
        },
        onEnterDigitalCreditCode: function (model, code) {
          if (code && !this.codeEntered) {
            this.codeEntered = true;
            this.$el.find('input#digital-credit-code').siblings('button').prop('disabled', false);
          }
          if (!code && this.codeEntered) {
            this.codeEntered = false;
            this.$el.find('input#digital-credit-code').siblings('button').prop('disabled', true);
          }
        },
        enableDigitalCredit: function (e) {
          var creditCode = $(e.currentTarget).attr('data-mz-credit-code-source'),
              isEnabled = $(e.currentTarget).prop('checked') === true,
              targetCreditAmtEl = this.$el.find("input[data-mz-credit-code-target='" + creditCode + "']"),
              me = this;

          if (isEnabled) {
            targetCreditAmtEl.prop('disabled', false);
            me.model.applyDigitalCredit(creditCode, null, true);
          } else {
            targetCreditAmtEl.prop('disabled', true);
            me.model.applyDigitalCredit(creditCode, 0, false);
            me.render();
          }
        },
        addRemainderToCustomer: function (e) {
          var creditCode = $(e.currentTarget).attr('data-mz-credit-code-to-tie-to-customer'),
              isEnabled = $(e.currentTarget).prop('checked') === true;
          this.model.addRemainingCreditToCustomerAccount(creditCode, isEnabled);
        },
        handleEnterKey: function (e) {
          var source = $(e.currentTarget).attr('data-mz-value');
          if (!source) return;
          switch (source) {
            case "creditAmountApplied":
              return this.applyDigitalCredit(e);
            case "digitalCreditCode":
              return this.getDigitalCredit(e);
          }
        },
        /* begin visa checkout */
        initVisaCheckout: function () {
          var me = this;
          var visaCheckoutSettings = HyprLiveContext.locals.siteContext.checkoutSettings.visaCheckout;
          var apiKey = visaCheckoutSettings.apiKey || '0H1JJQFW9MUVTXPU5EFD13fucnCWg42uLzRQMIPHHNEuQLyYk';
          var clientId = visaCheckoutSettings.clientId || 'mozu_test1';
          var orderModel = this.model.getOrder();


          if (!window.V) {
            //console.warn( 'visa checkout has not been initilized properly');
            return false;
          }
          // on success, attach the encoded payment data to the window
          // then call the sdk's api method for digital wallets, via models-checkout's helper
          window.V.on("payment.success", function (payment) {
            //console.log({ success: payment });
            me.editing.savedCard = false;
            me.model.parent.processDigitalWallet('VisaCheckout', payment);
          });

          window.V.init({
            apikey: apiKey,
            clientId: clientId,
            paymentRequest: {
              currencyCode: orderModel.get('currencyCode'),
              subtotal: "" + orderModel.get('subtotal')
            }
          });
        }
        /* end visa checkout */
      });

      var CouponView = Backbone.MozuView.extend({
        templateName: 'modules/checkout/coupon-code-field',
        handleLoadingChange: function (isLoading) {
          // override adding the isLoading class so the apply button
          // doesn't go loading whenever other parts of the order change
        },
        initialize: function () {
          var me = this;
          this.listenTo(this.model, 'change:couponCode', this.onEnterCouponCode, this);
          this.codeEntered = !!this.model.get('couponCode');
          this.$el.on('keypress', 'input', function (e) {
            if (e.which === 13) {
              if (me.codeEntered) {
                me.handleEnterKey();
              }
              return false;
            }
          });
        }/*,
      onEnterCouponCode: function (model, code) {
        if (code && !this.codeEntered) {
          this.codeEntered = true;
          this.$el.find('button').prop('disabled', false);
        }
        if (!code && this.codeEntered) {
          this.codeEntered = false;
          this.$el.find('button').prop('disabled', true);
        }
      },
      autoUpdate: [
        'couponCode'
      ],
      addCoupon: function (e) {
        // add the default behavior for loadingchanges
        // but scoped to this button alone
        var self = this;
        this.$el.addClass('is-loading');
        this.model.addCoupon().ensure(function () {
          self.$el.removeClass('is-loading');
          self.model.unset('couponCode');
          self.render();
        });
      },
      handleEnterKey: function () {
        this.addCoupon();
      }*/
      });

      var CommentsView = Backbone.MozuView.extend({
        templateName: 'modules/checkout/comments-field',
        autoUpdate: ['shopperNotes.comments']
      });

      var attributeFields = function () {
        var me = this;

        var fields = [];

        var storefrontOrderAttributes = require.mozuData('pagecontext').storefrontOrderAttributes;
        if (storefrontOrderAttributes && storefrontOrderAttributes.length > 0) {

          storefrontOrderAttributes.forEach(function (attributeDef) {
            fields.push('orderAttribute-' + attributeDef.attributeFQN);
          }, this);

        }

        return fields;
      };

      var ReviewOrderView = Backbone.MozuView.extend({
        templateName: 'modules/checkout/step-review',
        autoUpdate: [
          'createAccount',
          'agreeToTerms',
          'firstName',
          'lastName',
          'phoneNumber',
          'emailAddress',
          'password',
          'confirmPassword',
          'shopperNotes.comments'
//        ].concat(attributeFields()),
        ],
        additionalEvents: {
          "click #checkoutCommentsLink": "focusTextArea",
          "click #checkoutCreateAccountFlag": "updatePickupInfo"
        },
        renderOnChange: [
          'createAccount',
          'isReady'
        ],
        initialize: function () {
          var me = this;
          this.$el.on('keypress', 'input', function (e) {
            if (e.which === 13) {
              me.handleEnterKey();
              return false;
            }
          });
          this.model.on('passwordinvalid', function (message) {
            me.$('[data-mz-validationmessage-for="password"]').text(message);
          });
          this.model.on('userexists', function (user) {
            me.$('[data-mz-validationmessage-for="emailAddress"]').html(Hypr.getLabel("customerAlreadyExists", user, encodeURIComponent(window.location.pathname)));
          });

        },
        render: function(){
          this.setMinimumOrderAmount();
          Backbone.MozuView.prototype.render.apply(this, arguments);
        },
        setMinimumOrderAmount: function(){
          var messageConfig = $(".mz-configuarble-message-container").attr('data-mz-message-config'),
              configurableMessageResponse = messageConfig ? JSON.parse(messageConfig) : '',
              currentStore = $.cookie("preferredStore") ? $.parseJSON(localStorage.getItem('preferredStore')) : null,
              isProvinceMatched = currentStore ? configurableMessageResponse !== '' ? _.findWhere(configurableMessageResponse.msgContent, {province: currentStore.address.stateOrProvince}) : false : false,
              minimumOrderAmount = isProvinceMatched ? parseInt(isProvinceMatched.minimumOrderAmount, 10) : parseInt(Hypr.getThemeSetting('minimumOrderAmt').trim(), 10);
          this.model.set("minOrderAmount", minimumOrderAmount);
          console.log("minOrderAmount", minimumOrderAmount);
        },
        focusTextArea: function () {
          if ($('#checkoutOrderComments').hasClass('hidden')) {
            $('#checkoutOrderComments').removeClass('hidden');
            $('#checkoutCommentsLink').removeClass('collapsed');
            $('#checkoutOrderComments').focus();
          } else {
            $('#checkoutOrderComments').addClass('hidden');
            $('#checkoutCommentsLink').addClass('collapsed');
          }
        },
        submit: function () {
          console.log('order Submit called.');
          var self = this;
          if(this.model.get('minOrderAmount') > this.model.get('total')){
            console.log('return false');
            return false;
          } else {
            self.checkItemAvailableInventory();
          }
        },
        submitOrder: function(){
          console.log('submitting order...!!');
          var self = this;
          this.model.set('agreeToTerms', true);
          $.removeCookie('inStorePickUpStep', { path: '/' });  //remove cookie value of inStorePickUpStep once order is placed
          this.model.get('shopperNotes').set('comments', $('#checkoutOrderComments').val());
          _.defer(function () {
            var reCaptchaAttribute = _.find(self.model.get('orderAttributes'), function (attribute) {
              return attribute.attributeCode === Hypr.getThemeSetting('reCaptchaResponseCode');
            });
            if(reCaptchaAttribute) {
              var reCaptchaAttributeValues = [];
              reCaptchaAttribute.value = window.order.get('key');
              reCaptchaAttributeValues.push({
                'fullyQualifiedName': reCaptchaAttribute.attributeFQN,
                'values': [reCaptchaAttribute.value]
              });
              self.model.apiUpdateAttributes(reCaptchaAttributeValues).then(function (response) {
                self.model.set('attributes', response);
              });
            }
            var secondPersonEmailAttr = _.find(self.model.get('orderAttributes'), function (attribute) {
              return attribute.attributeCode === Hypr.getThemeSetting('secondPersonEmailCode');
            });
            //Check if secondPersonEmail field is invalid due to autofill -> set it to N/A
            if ((secondPersonEmailAttr.value && !Backbone.Validation.patterns.email.test(secondPersonEmailAttr.value)) ||
                ($('#checkout-attribute-secondPersonEmail').val() && !Backbone.Validation.patterns.email.test($('#checkout-attribute-secondPersonEmail').val())) ||
                (self.model.get('secondPersonEmail') && !Backbone.Validation.patterns.email.test(self.model.get('secondPersonEmail')))) {
              $('#checkout-attribute-secondPersonEmail').val('N/A');
              self.model.set('secondPersonEmail', 'N/A');
              var newAttr = [];
              self.model.get('orderAttributes').forEach(function (attribute) {
                if (attribute.attributeCode === Hypr.getThemeSetting('secondPersonEmailCode')) {
                  attribute.value = 'N/A';
                  newAttr.push({
                    'fullyQualifiedName': attribute.attributeFQN,
                    'values': [attribute.value]
                  });
                }
              });
              self.model.apiUpdateAttributes(newAttr).then(function (response) {
                self.model.set('attributes', response);
              });
            }
            self.model.submit();
            if (self.model.get('showProcessing')) {
              $("#main-content-loader").show();
              $('.order-loading-msg').show();
              $('body').animate({
                scrollTop: 0
              }, 800);
            }
          });
        },
        handleEnterKey: function () {
          this.submit();
        },
        updatePickupInfo: function () {
          var firstNameFQN = _.findWhere(this.model.get('attributes'), { "fullyQualifiedName": Hypr.getThemeSetting("firstPersonFirstNameFQN") });
          this.model.set("firstName", firstNameFQN ? _.first(firstNameFQN.values) : this.model.get('fulfillmentInfo').get('fulfillmentContact').get('firstName'));
          var lastNameFQN = _.findWhere(this.model.get('attributes'), { "fullyQualifiedName": Hypr.getThemeSetting("firstPersonLastNameFQN") });
          this.model.set("lastName", lastNameFQN ? _.first(lastNameFQN.values) : this.model.get('fulfillmentInfo').get('fulfillmentContact').get('lastNameOrSurname'));
          var emailFQN = _.findWhere(this.model.get('attributes'), { "fullyQualifiedName": Hypr.getThemeSetting("firstPersonEmailFQN") });
          this.model.set('emailAddress', emailFQN ? _.first(emailFQN.values) : this.model.get('fulfillmentInfo').get('fulfillmentContact').get('email'));
          if (this.model.get('orderType') !== "shipToStore") this.model.set('phoneNumber', this.model.get('fulfillmentInfo').get('fulfillmentContact').get('phoneNumbers').attributes.home);
        },
        /* Function is used to check item available inventory so user will not place order if inventory is not available for items*/
        checkItemAvailableInventory: function(){
          var self = this;
          if(localStorage.getItem('preferredStore')){
            var currentStore = $.parseJSON(localStorage.getItem('preferredStore')),
                isBOHStore = _.find(currentStore.attributes, function(attribute){
                  return attribute.fullyQualifiedName === Hypr.getThemeSetting('hhBOHStoreAttr');
                }),
                currentStoresWarehouse = _.find(currentStore.attributes, function(attribute){
                  return attribute.fullyQualifiedName === Hypr.getThemeSetting('warehouseAttr');
                }),
                productCodesQuery = this.getFilterQuery(),
                isCurrentStoreBoh = isBOHStore ? isBOHStore.values[0] : false;
            self.model.set("isCurrentStoreBoh", isCurrentStoreBoh);

            warehouseList.push(currentStoresWarehouse.values[0]);
            if(currentStoresWarehouse || isCurrentStoreBoh) {
              var data = {
                "locationCodes": warehouseList,
                "productCodes": productCodesQuery
              };

              if(isCurrentStoreBoh) data.locationCodes.push(currentStore.code);

              self.fetchInventories(data).then(function(response){
                console.log("fetchedInventories",response);
                var invalidQuantityItemCount = self.checkQuantity(response.items);
                console.log('invalidQuantityItemCount:::'+invalidQuantityItemCount);
                if(invalidQuantityItemCount){
                  console.log('invalidQuantityItemCount:::');
                  self.renderQuantityAlertModal();
                }else{
                  console.log('order submit process....!!');
                  self.submitOrder();
                }
              },function(error){
                console.error('failed to get location inventory', error);
              });
            }
          }
        },
        getFilterQuery: function(){
          return this.model.apiModel.data.items.map(function(item) {
            return item.product.productCode;
          });
        },
        fetchInventories : function(data){
          var inventoryApi = '/api/commerce/catalog/storefront/products/locationinventory';
          return api.request("POST", inventoryApi, data);
        },
        //HHOUT-162 Check quantity on cart page, so user can able to place order with available products only.
        checkQuantity: function(fetchedItemInventories){
          var self = this,
              quantityErrorItemsCount = 0;
          _.each(this.model.get('items'),function(item){
            var isQuantityValid = self.validateQuantity(fetchedItemInventories, item);
            if(!isQuantityValid) quantityErrorItemsCount++;
          });
          return quantityErrorItemsCount;
        },
        renderQuantityAlertModal: function(){
          var quantityAlertModalView = new QuantityAlertModalView({
            el: $('#quantityAlertModalContainer'),
            model: new Backbone.Model({
              apiContext: apiContext
            })
          });
          quantityAlertModalView.render();
          $('#quantityAlertModal').modal('show');
        },
        validateQuantity: function(fetchedItemInventory, item){
          var itemInventoryData = _.where(fetchedItemInventory,{ productCode: item.product.productCode }),
              fulfillmentType = item.fulfillmentMethod,
              sthInventory = this.getWarehouseInventory(itemInventoryData),
              bopisInventory = this.getBopisInventory(itemInventoryData),
              isQuantityValid = true;

          if((fulfillmentType === "Ship" && item.quantity > sthInventory) || (fulfillmentType === "Pickup" && item.quantity > bopisInventory)){
            isQuantityValid = false;
          }
          return isQuantityValid;
        },
        getWarehouseBufferInventory: function (stockAvailable) {
          var isWarehouseBufferEnabled = Hypr.getThemeSetting('enableWarehouseBuffer'),
              warehouseBufferValue = Hypr.getThemeSetting('warehouseBufferValue'),
              bufferedWarehouseInventory = 0,
              warehouseCount = stockAvailable && stockAvailable > 0 ? stockAvailable : 0;
          var warehouseInventoryCount = warehouseCount;
          if(isWarehouseBufferEnabled && warehouseBufferValue > 0 && warehouseCount > 1){
            bufferedWarehouseInventory = Math.round((warehouseCount/100) * warehouseBufferValue);
            bufferedWarehouseInventory = bufferedWarehouseInventory < 0.5 ? 1 : bufferedWarehouseInventory;
            warehouseInventoryCount = warehouseCount - bufferedWarehouseInventory;
          }
          return warehouseInventoryCount;
        },
        getWarehouseInventory: function (itemInventoryData) {
          var preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
          var self = this,
              sthWarehouseInventories = [];
          warehouseList = _.unique(warehouseList).filter(function(storeCode) { return storeCode !== preferredStore.code; });
          var activeWarehouseList = activeWarehouse;
          _.each(activeWarehouseList,function(warehouse){
            var sthInventory = _.findWhere(itemInventoryData,{'locationCode':warehouse}).stockAvailable;
            var finalSthInventory = self.getWarehouseBufferInventory(sthInventory);
            sthWarehouseInventories.push(finalSthInventory);
          });
          var sthMaxInventory = _.max(sthWarehouseInventories);
          return sthMaxInventory;
        },
        getBopisInventory: function(itemInventoryData){
          var preferredStore = $.parseJSON(localStorage.getItem('preferredStore')) ? $.parseJSON(localStorage.getItem('preferredStore')):'',
              self = this,
              currentStoresWarehouse = _.find(preferredStore.attributes, function(attribute){
                return attribute.fullyQualifiedName === Hypr.getThemeSetting('warehouseAttr');
              }),
              isBOHStore = _.find(preferredStore.attributes, function (attribute) {
                return attribute.fullyQualifiedName === Hypr.getThemeSetting('hhBOHStoreAttr');
              }),
              storeInventory;
          if(isBOHStore && isBOHStore.values[0]){
            storeInventory = _.findWhere(itemInventoryData,{'locationCode':preferredStore.code}) ? _.findWhere(itemInventoryData,{'locationCode':preferredStore.code}).stockAvailable : 0;
          }
          var warehouseInventory = _.findWhere(itemInventoryData,{'locationCode':currentStoresWarehouse.values[0]}) ? _.findWhere(itemInventoryData,{'locationCode':currentStoresWarehouse.values[0]}).stockAvailable : 0;

          var warehouseBufferInventory = self.getWarehouseBufferInventory(warehouseInventory);
          var bopisInventory = isBOHStore ? storeInventory > 0 ? storeInventory + warehouseBufferInventory : 0 + warehouseBufferInventory : warehouseBufferInventory;
          return bopisInventory;
        },
        getSameProductQuantity: function(productCode, fulfillmentType){
          var item = _.find(this.model.apiModel.data.items, function(item){
            return item.product.productCode === productCode && item.fulfillmentMethod === fulfillmentType;
          });
          return item ? item.quantity : 0;
        }
      });

      var ParentView = function (conf) {
        var gutter = parseInt(Hypr.getThemeSetting('gutterWidth'), 10);
        if (isNaN(gutter)) gutter = 15;
        var mask;
        conf.model.on('beforerefresh', function () {
          killMask();
          conf.el.css('opacity', 0.5);
          var pos = conf.el.position();
          mask = $('<div></div>', {
            'class': 'mz-checkout-mask'
          }).css({
            width: conf.el.outerWidth() + (gutter * 2),
            height: conf.el.outerHeight() + (gutter * 2),
            top: pos.top - gutter,
            left: pos.left - gutter
          }).insertAfter(conf.el);
        });

        function killMask() {
          conf.el.css('opacity', 1);
          if (mask) mask.remove();
          $("#main-content-loader").hide();
          $('.order-loading-msg').hide();
          conf.model.set('showProcessing', false);
        }

        conf.model.on('refresh', killMask);
        conf.model.on('error', killMask);
        return conf;
      };
      $(document).ready(function () {
        var $checkoutView = $('#checkout-form'),
            checkoutData = require.mozuData('checkout'),
            checkoutModel = window.order = new CheckoutModels.CheckoutPage(checkoutData),
            orderType = getOrderType(checkoutModel),
            isOrderInValid= checkPurchaseLocation(checkoutModel),
            checkoutViews = {
              parentView: new ParentView({
                el: $checkoutView,
                model: checkoutModel
              }),
              steps: {
                shippingAddress: new ShippingAddressView({
                  el: $('#step-shipping-address'),
                  model: checkoutModel.get('fulfillmentInfo').get('fulfillmentContact')
                }),
                shippingInfo: new ShippingInfoView({
                  el: $('#step-shipping-method'),
                  model: checkoutModel.get('fulfillmentInfo')
                }),
                paymentInfo: new BillingInfoView({
                  el: $('#step-payment-info'),
                  model: checkoutModel.get('billingInfo')
                })
              },
              inStorePickUpInfoSummary: new InStorePickUpInfoSummaryView({ //separate view for instore pickup (first step of checkout with instore pickup)
                el: $('#instore-pickup-info'),
                model: checkoutModel
              }),
              orderAndCartSummary: new OrderAndCartSummaryView({
                el: $('#ordercart-summary'),
                model: checkoutModel
              }),
              couponCode: new CouponView({
                el: $('#coupon-code-field'),
                model: checkoutModel
              }),
              comments: Hypr.getThemeSetting('showCheckoutCommentsField') && new CommentsView({
                el: $('#comments-field'),
                model: checkoutModel
              }),

              reviewPanel: new ReviewOrderView({
                el: $('#step-review'),
                model: checkoutModel
              }),
              messageView: messageViewFactory({
                el: $checkoutView.find('[data-mz-message-bar]'),
                model: checkoutModel.messages
              })
            };

        window.checkoutViews = checkoutViews;

        checkoutModel.on('complete', function () {
          CartMonitor.setCount(0);
          window.location = (HyprLiveContext.locals.siteContext.siteSubdirectory || '') + "/checkout/" + checkoutModel.get('id') + "/confirmation";
        });

        var $reviewPanel = $('#step-review');
        checkoutModel.on('change:isReady', function (model, isReady) {
          if (isReady) {
            setTimeout(function () {
              window.scrollTo(0, $reviewPanel.offset().top);
            }, 750);
          }
        });
        //render these views initially to get the values on the page load which have set in initialize method of these views
        checkoutViews.inStorePickUpInfoSummary.render();
        checkoutViews.orderAndCartSummary.render();
        if (checkoutModel.get('attributes') && checkoutModel.get('attributes').length > 0) {
          _.invoke(checkoutViews.steps, 'initStepView');
        }

        if (window.order) {
          if (isOrderInValid) {
            $("#checkoutStoreChangeModal").modal({
              show: 'true',
              backdrop: 'static',
              keyboard: false
            });
          }
        }

        $checkoutView.noFlickerFadeIn();

        /**
         * Called when the user begins the checkout process.
         * @param {Array} cart An array representing the user's shopping cart.
         */
        var productDataList = [];
        var cartItems = window.order.get('items');
        var i;
        for (i = 0; i < cartItems.length; i++) {
          // addItem should be called for every item in the shopping cart.
          var item = cartItems[i];
          var currAttribute = _.findWhere(item.product.properties, { 'attributeFQN': Hypr.getThemeSetting('brandDesc') });

          var categoryflow;
          var itemIsInCookie = false;
          if ($.cookie('BreadCrumbFlow')) {
            var cookieCategoryArray = $.parseJSON($.cookie("BreadCrumbFlow"));
            var avail = _.findWhere(cookieCategoryArray, { 'productCode': item.product.productCode });
            if (!avail) {
              itemIsInCookie = false;
            } else {
              itemIsInCookie = true;
              categoryflow = avail.flow;
            }
          }
          var qty = parseInt(item.quantity, 10);
          var productPrice = item.discountedTotal / qty;
          var productData = {
            'name': item.product.name,
            'id': item.product.productCode,
            'price': productPrice.toString().replace('$', '').replace(',', '.').trim(),
            'brand': currAttribute ? currAttribute.values[0].stringValue : '',
            'category': itemIsInCookie ? categoryflow : '',
            'quantity': qty
          };
          productDataList.push(productData);
        }

        if (($.cookie("inStorePickUpStep") && $.cookie("inStorePickUpStep") == "complete") || $(".instore-pickup-info-container").hasClass("is-complete")) {
          dataLayer.push({
            'event': 'checkout',
            'ecommerce': {
              'checkout': {
                'actionField': {
                  'step': 2,
                  'option': 'Credit Card'
                },
                'products': productDataList
              }
            }
          });
        } else {
          dataLayer.push({
            'event': 'checkout',
            'ecommerce': {
              'checkout': {
                'actionField': {
                  'step': 1,
                  'option': user.isAnonymous !== true && user.isAuthenticated === true ? 'signed in' : 'guest'
                },
                'products': productDataList
              }
            }
          });
        }

      });

      function getOrderType(checkoutModel) {
        var isPickUp = 0,
            isShip = 0;
        _.each(checkoutModel.get('items'), function (item) {
          if (item.fulfillmentMethod === "Pickup") {
            isPickUp++;
          } else if (item.fulfillmentMethod === "Ship") {
            isShip++;
          }
        });

        if (isPickUp > 0 && isShip > 0) {
          orderType = 'mixOrder';
        } else if (isPickUp > 0) {
          orderType = 'shipToStoreOrder';
        } else if (isShip > 0) {
          orderType = 'shipToHomeOrder';
        }
        checkoutModel.set('orderType', orderType);
        return orderType;
      }

      function checkPurchaseLocation(checkoutModel){
        var prefrredStoreCode = $.parseJSON(localStorage.getItem('preferredStore')).code,
            misMatchCount = 0;
        _.each(checkoutModel.get('items'), function (item) {
          var fulfillmentLocationCode = item.fulfillmentLocationCode,
              purchaseLocationCode = item.purchaseLocation,
              fulfillmentMethod = item.fulfillmentMethod;
          if(fulfillmentMethod === "Pickup" && (prefrredStoreCode !== fulfillmentLocationCode || prefrredStoreCode !==  purchaseLocationCode || fulfillmentLocationCode !== purchaseLocationCode )){
            misMatchCount++;
          }else if(fulfillmentMethod === "Ship" && prefrredStoreCode !== purchaseLocationCode) {
            misMatchCount++;
          }
        });
        return misMatchCount > 0 ? true : false;
      }
    });
