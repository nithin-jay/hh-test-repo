define(['modules/jquery-mozu', "underscore","modules/views-collections", 'hyprlive', "modules/backbone-mozu",
'modules/api','vendor/timezonesupport/timezonesupport','modules/check-nearby-store', 'vendor/jquery/moment'], 
function($, _,CollectionViewFactory, Hypr,Backbone, api, timeZoneSupport,CheckNearbyStoreModal,moment) {
	var preferredStoreCookie;
	var pageContext = require.mozuData('pagecontext'),isMapboxEnabled = false,
		today = new Date(),
		filtersString = "",
		days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
		ua = navigator.userAgent.toLowerCase();
		var apiContext = require.mozuData('apicontext');

	$(document).ready(function() {
		if(window.location.search.indexOf(Hypr.getThemeSetting('clearanceOnSale')) < 0){
			$('.mz-breadcrumbs').removeClass('hidden');
		}
        window.facetingViews = CollectionViewFactory.createFacetedCollectionViews({
            $body: $('[data-mz-category]'),
            template: "category-interior"
        });
		
        $('.result-view-toggle li a').on('click', function (e){
        	e.preventDefault();
        	$(this).closest('.result-view-toggle').find('li').removeClass('is-current');
        	$(this).closest('li').addClass('is-current');
        	
        	if($(this).data('toggle-type') == 'list') {
        		window.gridStatus = "list";
        		$(document).find('.mz-productlist').addClass('list');
        	} else {
        		window.gridStatus = "";
        		$(document).find('.mz-productlist').removeClass('list');
        	}
		});
        if ($("#mz-drop-zone-expert-advice-bropzone > .mz-cms-row > .mz-cms-col-12-12").html() && $("#mz-drop-zone-expert-advice-bropzone > .mz-cms-row > .mz-cms-col-12-12").html().length > 0) {
        	$("#mz-drop-zone-expert-advice-bropzone").addClass('isdropped');
        } else {
        	$("#mz-drop-zone-expert-advice-bropzone").removeClass('isdropped');
          }   

       
        $(document).find('.filter-button').on('click', function(e) {
        	$(document).find('.filters').removeClass('open');
        });
	   
		
		 
		$(document).on('click', '.data-layer-productClick',function(e){
			if($(e.currentTarget).closest('[data-ign-gtm-impression]') && $(e.currentTarget).closest('[data-ign-gtm-impression]').length > 0){
				var breadcrumbs = $(".mz-breadcrumb-link:not('.is-first')");
				var breadcrumbstring = '';
				for(var i=0; i<breadcrumbs.length; i++) {
					if(i !== breadcrumbs.length-1) {
						breadcrumbstring += $.trim(breadcrumbs[i].text) + '/';
					}else {
						breadcrumbstring += $.trim(breadcrumbs[i].text);
					}
				}
				if(pageContext.pageType=='category' && !$(e.currentTarget).closest(".mz-productlisting").attr("data-mz-categoryname")){
					var currentSelectedCatergory = $('.current-breadcrumb').text() ? $('.current-breadcrumb').text() : $('.mz-breadcrumb-current').text();
					breadcrumbstring = breadcrumbstring + '/' + currentSelectedCatergory;
				}else if(pageContext.pageType=='category' && $(e.currentTarget).closest(".mz-productlisting").attr("data-mz-categoryname")){
					breadcrumbstring = $(e.currentTarget).closest(".mz-productlisting").attr("data-mz-categoryname");
				}

				var hasSalePrice = $(e.currentTarget).closest(".mz-productlisting").find(".mz-price").hasClass("is-saleprice");
				var getPrice = hasSalePrice ? $(e.currentTarget).closest(".mz-productlisting").find(".is-saleprice").text().split('/')[0] : $(e.currentTarget).closest(".mz-productlisting").find(".mz-price").text().split('/')[0];
				var getBrand = $(e.currentTarget).closest(".mz-productlisting").find(".brand-name").text();
				var actionFieldList = $(e.currentTarget).attr("href").split('?page=')[1].split('#ccode=')[0];
				if(actionFieldList) {
		           	if(actionFieldList.indexOf('&rrec=true') !== -1){
		           		actionFieldList = actionFieldList.split('&rrec=true')[0];
		           	}
				}

				var pagecontext = require.mozuData('pagecontext');
				var category = _.findWhere(window.allCategories,{"categoryId":pagecontext.categoryId});
                var updateListName = '';
				if(category !== undefined && category !== "" && category.parentCategory !== undefined ){
					if(category.parentCategory.categoryId == Hypr.getThemeSetting('brandsCategoryId')){
						updateListName = '_brand_'+ category.content.name;
					}else if(category.parentCategory.categoryId == Hypr.getThemeSetting('dynamicCategoryId')){
						updateListName = '_landingPage_'+ category.content.name;
					}else{                            
						updateListName = '_site-hierarchy_'+ category.content.name;
					}
				}

				dataLayer.push({
					'event': 'productClick',
					'ecommerce': {
						'click': {
						'actionField': {'list': actionFieldList + updateListName},
						'products': [{
							'name': $(e.currentTarget).closest(".mz-productlisting").attr("data-mz-productName"),
							'id': $(e.currentTarget).closest(".mz-productlisting").attr("data-mz-product"),
		                    'price': $.trim(getPrice) ? $.trim(getPrice.replace('$', '').replace(',', '.')) : '',
		                    'brand': $.trim(getBrand) ? $.trim(getBrand) : '',
		                    'category': breadcrumbstring,
		                    'position':$(e.currentTarget).closest(".mz-productlisting").attr("data-mz-position")
						}] 
					  }
					}
				});
			}else {
				var breadcrumbStr = "";
				var currentParentId = parseInt($(e.currentTarget).closest("[data-mz-categoryId]").attr('data-mz-categoryId').split('|')[1], 10);
				window.getBreadcrumbflow(currentParentId, window.allCategories, breadcrumbStr, e);
			}
			
		});
        /*Code for moretag on facets - Start*/
        var facetHeight = Hypr.getThemeSetting('facetDefaultHeight');
        var facetHeightWithPX = Hypr.getThemeSetting('facetDefaultHeight')+'px';
        $('.mz-facetingform').find('.mz-l-sidebaritem .facet-details > ul').each(function(){        	
        	if($(this).height() > facetHeight) {
        		$(this).parent().find('.moretag').css('display', 'inline-block');
        		if($(this).closest('.mz-l-sidebaritem').hasClass('for-caregory')){
        			$(this).css({'height': '205px', 'overflow-y': 'hidden'});
        		}else {
        			$(this).css({'height': facetHeightWithPX, 'overflow-y': 'hidden'});
        		}
        		
        	}
        });
        
        
        $('.mz-facetingform').find('.mz-l-sidebaritem #Price > ul').css({'height': 'auto', 'overflow-y': 'hidden'});
		
		$(document).on('click', '.moretag', function(event){
        	event.preventDefault();
        	var viewLess = Hypr.getLabel('viewLess');
        	var viewMore = Hypr.getLabel('viewMore');
        	if($(this).find('span').text() === viewMore) {
        		$(this).parent().find('.mz-facetingform-facet').css('height', 'auto');
            	$(this).find('span').text(viewLess);
        	} else {
        		if($(this).closest('.mz-l-sidebaritem').hasClass('for-caregory')){
        			$(this).parent().find('.mz-facetingform-facet').css('height', '205px');
        		}else {
        			$(this).parent().find('.mz-facetingform-facet').css('height', facetHeightWithPX);
        		}
            	$(this).find('span').text(viewMore);
        	}
        	
		});
		/*Code for moretag on facets - End*/
		
		var clearanceCategoryId = Hypr.getThemeSetting('clearanceCategoryId');
		if(clearanceCategoryId == pageContext.categoryId){
			if(localStorage.getItem('preferredStore')){
				var mycurrentStore = JSON.parse(localStorage.getItem('preferredStore'));
				 if(mycurrentStore) {
					 var attribute = _.findWhere(mycurrentStore.attributes, {"fullyQualifiedName":Hypr.getThemeSetting("isEcomStore")});
							
					if(attribute){
						var isEcomStore = attribute.values[0] === "Y" ? true : false; 
						if(isEcomStore){
							$(".contentIfStoreNotEcom").addClass("hidden");
						}else{
							$(".contentIfStoreNotEcom").removeClass("hidden");
							$(".contentIfStoreSelected").addClass("hidden");
							$(".contentIfStoreNotSelected").addClass("hidden");
						}
					}
				}  
			}
		}

		$(document).on('click','.clearanceChangeStore', function(e){
			e.preventDefault();
			var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            locale = locale.split('-')[0];
            var currentLocale = locale === 'fr' ? '/fr' : '/en';			 
			window.location.href= currentLocale + "/store-locator?returnUrl=" + currentLocale + window.location.pathname;
		});
		
		var productsElements = $(".mz-productlist .mz-productlisting .saved-price").find(".promo-till");
		var torontoTimeZone = timeZoneSupport.findTimeZone('America/Toronto');
		var today = new Date();
		_.each(productsElements,function(item){
			var promotEndDate = item.getAttribute("data-promo-end-date");
			var promoItemZonedTime = timeZoneSupport.getZonedTime(new Date(promotEndDate), torontoTimeZone);
			var promoItemDate = new Date(promoItemZonedTime.year, promoItemZonedTime.month-1, promoItemZonedTime.day, promoItemZonedTime.hours, promoItemZonedTime.minutes, promoItemZonedTime.seconds, promoItemZonedTime.milliseconds);
			if(today <= promoItemDate){
				$(item).closest('.saved-price').removeClass('hidden');  
				$(item).text(formatDate(promoItemDate));	
			}
		});
		var compareItems = $('.mz-compareitem').find(".compare-item-container");
		if (compareItems.length === 0) {
			var checkedBoxes = $('.compare-checkbox:checked');
			_.each(checkedBoxes, function(checkedBox){
				$(checkedBox).attr('checked', false);
			});
		}
    });
    
    $(document).on('click','.mz-facetingform-value', function(e){
    	if($(e.currentTarget).attr('checked') !== 'checked'){
    		var facetName, facetValue;
    		if($(e.currentTarget).attr('data-mz-facet') === 'tenant~on-sale') {
    			facetName = Hypr.getLabel('onSale');
    			facetValue = true;
    		}else {
    			facetName = $(e.currentTarget).closest('.facet-details').siblings('h4').text();
            	facetValue = $(e.currentTarget).parent().siblings('.mz-facetingform-valuelabel').text().split(' (')[0];
    		}
    	}
    	
	});

	function formatDate(date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('-');
	}

	function getCookieValue(cookieName) {
	    var b = document.cookie.match('(^|;)\\s*' + cookieName + '\\s*=\\s*([^;]+)');
	    return b ? b.pop() : '';
	}

	var CrumbUrl = window.location.href;
	var currentBreadcrumb = $(".mz-breadcrumb-current").text();
	var newUrl = $(".current-breadcrumb").text();
    $(document).on('click', '.mz-facetingform-link', function(e) {
    	var subCategoryName = $(e.currentTarget).text().split(' (')[0];
		if(localStorage.getItem('preferredStore')){
			preferredStoreCookie = JSON.parse(localStorage.getItem('preferredStore'));
		}
		if(subCategoryName != currentBreadcrumb && newUrl != subCategoryName){
			$(".mz-breadcrumb-current").html("");
			$(".mz-breadcrumb-current").html("<a href="+CrumbUrl+" class='mz-breadcrumb-link hidden-xs' title="+currentBreadcrumb+">"+currentBreadcrumb+"</a><span class='mz-breadcrumb-separator hidden-xs'>|</span><span class='mz-breadcrumb-current current-breadcrumb capitalize '>"+ subCategoryName +"</span>");
		}
		
		if (window.history && window.history.pushState) {
			$(window).on('popstate', function() {
				$(".mz-breadcrumb-current").html("<span class='mz-breadcrumb-current current-breadcrumb capitalize '>"+ currentBreadcrumb +"</span>");
			});
		}
	});
	var productData =[];
	var removeProductionFromPreview = function(productCode){
		var indexOfProductToRemove;
		productData.forEach(function(product, index){ 
			if(product.productCode === productCode) {
				indexOfProductToRemove = index;
				$('.compare-checkbox[data-mz-productcode='+product.productCode+']').attr('checked', false);
			}
		});
		productData.splice(indexOfProductToRemove, 1);
		CompareProductView.model.set('data', productData);
		var productModelLength = CompareProductView.model.get('data').length;
		if (productModelLength < 4) {
			_.each($('.compare-checkbox'), function(checkbox) {
				if(!$(checkbox).prop('checked')) {
					$(checkbox).attr('disabled',false);
					$(checkbox).closest('.compare-section').next().removeClass('disabled');
				}
			});
		}
		CompareProductView.render();
	};
	var ComapreProductModal = Backbone.MozuView.extend({
		templateName: 'modules/product/compare-product-modal',
		initialize:function(){
		},
		removeProduct: function(e) {
			var indexOfProductToRemove;
			this.model.get('data').forEach(function(product, index){ 
				if(product.productCode === $(e.currentTarget).attr('data-mz-productcode')) {
					indexOfProductToRemove = index;
					$('.compare-checkbox[data-mz-productcode='+product.productCode+']').attr('checked', false);
				}
			});
			this.model.get('data').splice(indexOfProductToRemove, 1);
			var modelLength = this.model.get('data').length;
			if (modelLength < 4) {
				_.each($('.compare-checkbox'), function(checkbox) {
					if(!$(checkbox).prop('checked')) {
						$(checkbox).attr('disabled',false);
						$(checkbox).closest('.compare-section').next().removeClass('disabled');
					}
				});
			}
			removeProductionFromPreview($(e.currentTarget).attr('data-mz-productcode'));
			if (indexOfProductToRemove === 0  && modelLength === 0) {
				this.model.set('data',[]);
				productData = [];
				$("#compare-product-modal").modal('hide');
			}
			this.render();
		}
	});
	
	
	var compareProductView = Backbone.MozuView.extend({
		templateName: 'modules/product/compare-products-preview',
		clearAll: function() {
			this.model.set('data',[]);
			productData = [];
			var checkedBoxes = $('.compare-checkbox:checked');
			_.each(checkedBoxes, function(checkedBox){
				$(checkedBox).attr('checked', false);
			});
			_.each($('.compare-checkbox'), function(checkbox) {
				$(checkbox).attr('disabled',false);
				$(checkbox).closest('.compare-section').next().removeClass('disabled');
			});
			this.render();
		},
		render: function() {
			Backbone.MozuView.prototype.render.apply(this, arguments);
		},
		removeProduct: function(e) {
			removeProductionFromPreview($(e.currentTarget).attr('data-mz-productcode'));
			this.render();
		},
		viewCompareDetails: function() {
			if(this.model.get('data').length === 1){
				this.model.set('error', Hypr.getLabel('selectTwoProductsError'));
				this.render();
			}else {
				this.model.set('error', '');
				this.render();
				$('#compare-product-modal').modal().show();
				var filterProductCode = _.map(this.model.get('data'), function(product) { return "productCode eq " + product.productCode; }).join(' or ');
		
			    var productModel = Backbone.MozuModel.extend({
			        mozuType: 'products'
			    });
			    
			    var ProductModel = new productModel();
			    
			    ProductModel.set('filter',  filterProductCode);
			    ProductModel.set("responseFields", "items(productCode, properties, content, options, price, measurements)");
				ProductModel.fetch().then(function(responseObject) {
					var productResponse = responseObject.apiModel.data.items;
							var count=0;
							var countValue =0;
							var isHideValue=false;
					for(var i=0;i< productResponse.length;i++){
						isHideValue=false;
						for(var j=0;j<productResponse[i].properties.length;j++){
							if(productResponse[i].properties[j].attributeFQN==Hypr.getThemeSetting("supplierDirectItem")){
									count++;
								for(var k=0;k<productResponse[i].properties[j].values.length;k++){
									if(productResponse[i].properties[j].values[k].value==Hypr.getThemeSetting("supplierDirectItemValue")){
										countValue++;	
									}
								}
								if(count==countValue){
									isHideValue=true;
									CompareProductModalView.model.set('isHide', isHideValue);
								}else{
									CompareProductModalView.model.set('isHide', isHideValue);
								}
							}
							
						}
					}
					var productResponseInSequence = [];
					_.each(productResponse, function(product, index) {
						productResponseInSequence[productData.indexOf(_.find(productData, function(p){return p.productCode === product.productCode;}))] = product;
					});
					CompareProductModalView.model.set('data', productResponseInSequence);
					var locale = require.mozuData('apicontext').headers['x-vol-locale'];
		            locale = locale.split('-')[0];
		            var currentLocale = locale === 'fr' ? '/fr' : '/en';
					var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
					CompareProductModalView.model.set('currentLocale', currentLocale);
					CompareProductModalView.model.set('currentSite', currentSite);
					
					var propertyData = [];
					_.each(productResponse, function(product) {
						if (product.properties && Hypr.getThemeSetting("showProductDetailProperties")){
							_.each(product.properties, function(property){
								if(
								property.values && !property.isHidden && property.attributeDetail.inputType != "YesNo" && 
								property.attributeFQN != Hypr.getThemeSetting("modelAttr") && property.attributeFQN != Hypr.getThemeSetting("brandCode") && 
								property.attributeFQN != Hypr.getThemeSetting("itemDescription") && 
								property.attributeFQN != Hypr.getThemeSetting("hhSellingUOM") && 
								property.attributeFQN != Hypr.getThemeSetting("hhLumberInd") && 
								property.attributeFQN != Hypr.getThemeSetting("hhHardwareInd") && 
								property.attributeFQN != Hypr.getThemeSetting("hhFurnitureInd") &&  
								property.attributeFQN != Hypr.getThemeSetting("supplierDirectItem") && 
								property.attributeFQN != Hypr.getThemeSetting("hhWarrantyExchangeInd") && 
								property.attributeFQN != Hypr.getThemeSetting("limitedQtyInd") && 
								property.attributeFQN != Hypr.getThemeSetting("displayItem") && 
								property.attributeFQN != Hypr.getThemeSetting("hhBulkyCourierInd") && 
								property.attributeFQN != Hypr.getThemeSetting("hhFragileCourierInd") && 
								property.attributeFQN != Hypr.getThemeSetting("comments") && 
								property.attributeFQN != Hypr.getThemeSetting("productVideo") && 
								property.attributeFQN != Hypr.getThemeSetting("relatedProducts") && 
								property.attributeFQN != Hypr.getThemeSetting("productRating") && 
								property.attributeFQN != Hypr.getThemeSetting("productUpsell") && 
								property.attributeFQN != Hypr.getThemeSetting("fineClass") && 
								property.attributeFQN != Hypr.getThemeSetting("ehfFees") && 
								property.attributeFQN != Hypr.getThemeSetting("actualProductCode") && 
								property.attributeFQN != Hypr.getThemeSetting("productWeight") && 
								property.attributeFQN != Hypr.getThemeSetting("pilarType") && 
								property.attributeFQN != Hypr.getThemeSetting("marketingDescription") && 
								property.attributeFQN != Hypr.getThemeSetting("ingredients") && 
								property.attributeFQN != Hypr.getThemeSetting("promoItemType") && 
								property.attributeFQN != Hypr.getThemeSetting("imageId") && 
								property.attributeFQN != Hypr.getThemeSetting("mktKeyword") && property.attributeFQN != Hypr.getThemeSetting("homeExclusiveInd") && 
								property.attributeFQN != Hypr.getThemeSetting("onlyAtHH") && property.attributeFQN != Hypr.getThemeSetting("availability") && 
								property.attributeFQN != Hypr.getThemeSetting("priceListEntityType") && 
								property.attributeFQN != Hypr.getThemeSetting("gender") && 
								property.attributeFQN != Hypr.getThemeSetting("disclaimerAttr") && 
								property.attributeFQN != Hypr.getThemeSetting("onsaleAttr") && 
								property.attributeFQN != Hypr.getThemeSetting("soldItemAttr")
								){
									var propertyName = property.attributeDetail.name.split('_');
									if(propertyName[0] != "DynAttribute" && propertyName[0] != "DynAttribut") {
										var data = {};
										if(propertyData.length > 0){
											var isPropertyPresent = false;
											_.each(propertyData, function(currentProperty) {
												if(currentProperty.attributeFQN === property.attributeFQN) {
													isPropertyPresent = true;
												}
											});
											if(!isPropertyPresent) {
												data = {'attributeFQN': property.attributeFQN, 'value' : property.attributeDetail.name};
												propertyData.push(data);
											}
											
										}else {
											data = {'attributeFQN': property.attributeFQN, 'value' : property.attributeDetail.name};
											propertyData.push(data);
										}
									}
								}
							});
						}
					});
					CompareProductModalView.model.set('propertyData', propertyData);
					if (localStorage.getItem('preferredStore')){
						CompareProductModalView.model.set({"preferredStore":$.parseJSON(localStorage.getItem('preferredStore'))});
					}
					CompareProductModalView.render();
				});
			}
		}
	});
	$('#compare-product-modal').on('hidden.bs.modal', function () {
		CompareProductModalView.model.set('data', []);
		CompareProductModalView.model.set('propertyData', []);
		CompareProductModalView.render();
	});
	$('#compare-product-modal').bind('mousewheel', function(e){
		var modalHeight = $("#compare-product-modal").find('.modal-body').height(),
        	imgHeight = $('#product-img-row').height(),
        	nameHeight = $('#product-name-row').height(),
        	scrollPosition = $("#compare-product-modal").find('.modal-body').scrollTop();
        	var totalHeight = modalHeight-(imgHeight+nameHeight);
        if (e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) {
        	if (scrollPosition < imgHeight){
            	$('.top-menu-element.img-section').addClass('hide');
            }
        }
        else{
        	if (scrollPosition > totalHeight){
        		$('.top-menu-element.img-section').removeClass('hide');
        	}
            
        }
    });
    if(navigator.userAgent.toLowerCase().indexOf("firefox") > -1){
		$('#compare-product-modal').bind('wheel', function(e){
	        if(e.originalEvent.deltaY < 0){
	            $('.top-menu-element.img-section').addClass('hide');
	        }
	        else{
	            $('.top-menu-element.img-section').removeClass('hide');
	        }
	    });
	} else {
		var lastY;
		$('#compare-product-modal').bind('touchstart', function(e) {
			lastY = e.originalEvent.touches ? e.originalEvent.touches[0].pageY : e.pageY;
			console.log(lastY);
		});
	    $('#compare-product-modal').bind('touchmove', function (e) {
	    	var currentY = e.originalEvent.touches ?  e.originalEvent.touches[0].pageY : e.pageY;
	    	if (Math.abs(currentY-lastY) < 15) { return; }
		    if (currentY > lastY) {
		    	$('.top-menu-element.img-section').addClass('hide');
		    } else {
		    	$('.top-menu-element.img-section').removeClass('hide');
		    }
		});
	}  

	$(document).on('change',".compare-checkbox", function(e){
		var checkedBoxes = $('.compare-checkbox:checked');
		if (checkedBoxes.length == 4) {
			_.each($('.compare-checkbox'), function(checkbox) {
				if(!$(checkbox).prop('checked')) {
					$(checkbox).attr('disabled',true);
					$(checkbox).closest('.compare-section').next().addClass('disabled');
				}
			});
		} else {
			_.each($('.compare-checkbox'), function(checkbox) {
				if(!$(checkbox).prop('checked')) {
					$(checkbox).attr('disabled',false);
					$(checkbox).closest('.compare-section').next().removeClass('disabled');
				}
			});
		}
		if(checkedBoxes.length > 0){
			if(checkedBoxes.length <= 4) {
				if($(e.currentTarget).prop('checked')){
					var product = {
						productCode: $(e.currentTarget).attr('data-mz-productcode'),
						imgUrl: $(e.currentTarget).attr('data-mz-img')
					};
					productData.push(product);
					productData = _.uniq(productData, function(product){ return product.productCode; });
				}else{
					var indexOfProductToRemove;
					productData.forEach(function(product, index){ 
						if(product.productCode === $(e.currentTarget).attr('data-mz-productcode')) {
							indexOfProductToRemove = index;
						}
					});
					productData.splice(indexOfProductToRemove, 1);
				}
			}
		}else {
			productData = [];
		}
		CompareProductView.model.set('data', productData);
		if(CompareProductView.model.get('data').length === 2){
			CompareProductView.model.set('error', '');
		}
		CompareProductView.render();
	});
	var CompareProductView = new compareProductView({
		el:$('#compare-product-container'),
		model: new Backbone.Model()
	});
	var CompareProductModalView = new ComapreProductModal({
		el: $('#compare-product-modal'),
		model: new Backbone.Model()
	});

	/***
	 * 
	 * Below code is used for widgets, since partial_cache tag is used to display faceted_products due to which
	 * under partial_cache tag - require_script tag won't work. This is the reason why widgets js is been written within this file.
	 * 
	 * - New Category Description Widget
	 * - Category Description Widget
	 * Ticket no - IWM-1650
	 * Author - @Yogeet
	 */
	/**
	 * 
	 * New Category Description Widget Js
	 */
		$(document).on('click', '.categoryToggle', function(e){	
			$(e.currentTarget).closest(".description-container-outer").toggleClass("is-collapsed");
			$(e.currentTarget).closest(".category-toggle-container").toggleClass("is-collapsed");
			var iscollapsed = $(".description-container-outer").hasClass("is-collapsed");
			if(iscollapsed){
				$(e.currentTarget).attr("title" , Hypr.getLabel('readLess'));
				$(e.currentTarget).parent().addClass('readLessAlignment');
				$(e.currentTarget).text(Hypr.getLabel('readLess'));
				$(e.currentTarget).parent().siblings('div.description-container-inner').children().find('p').first().addClass("line-clamp");
			}
			else{
				$(e.currentTarget).parent().removeClass('readLessAlignment');
				$(e.currentTarget).attr("title" , Hypr.getLabel('readMore'));
				$(e.currentTarget).text(Hypr.getLabel('readMore'));
				$(e.currentTarget).parent().siblings('div.description-container-inner').children().find('p').first().removeClass("line-clamp");
			}
		});
		
		//calculates description content height & hides less/more button
		var collapseDescriptionHeight = $(".description").height();
		var categoryDescriptionHeight = function(){
			$(".description").css('min-height','auto');
			$(".category-toggle-container").hide();
		};
		if($(window).width() < 768){
			if(collapseDescriptionHeight < 200){
				categoryDescriptionHeight();
			}
		}
		if($(window).width() > 767){
			if(collapseDescriptionHeight < 130){
				categoryDescriptionHeight();
			}
		}
		//end
		var categoryDescriptionParagraphCount = $('.description-container-inner p').length;
		var categoryDescriptionCharacterCount = $(".description-container-inner p").eq(0).text().length;
		var categoryDescriptionMinCount;
		var isMobile = navigator.userAgent.match(/(iPhone)|(iPod)|(android)|(webOS)/i),
		isiPad = ua.indexOf('ipad') > -1 || ua.indexOf('macintosh') > -1 && 'ontouchend' in document;
		if(isiPad) {
			categoryDescriptionMinCount = 385;
		}else if(isMobile){
			categoryDescriptionMinCount = 200;
		}else{
			categoryDescriptionMinCount = 448;
		}
		if(categoryDescriptionParagraphCount > 1 || categoryDescriptionCharacterCount > categoryDescriptionMinCount){
			$(".description-container-outer .category-toggle-container").css('display','block');
			$('.description-container-inner p').first().removeClass("line-clamp");
		}else{
			$(".description-container-outer .category-toggle-container").css('display','none');
			$('.description-container-inner p').first().addClass("line-clamp");
		}
	/**
	 * End of New Category description Widget Js
	 */

	/**
	 * 
	 * Category Description Widget Js
	 */
		$(document).on('click', '.toggle-btn', function(e){	
			$(e.currentTarget).closest(".description-container").toggleClass("is-collapsed");
			$(e.currentTarget).closest(".toggle-container").toggleClass("is-collapsed");
			var iscollapsed = $(".description-container").hasClass("is-collapsed");
			if(iscollapsed){
				$(e.currentTarget).attr("title" , Hypr.getLabel('readLess'));
				$(e.currentTarget).text(Hypr.getLabel('readLess'));
				$('.category-description p').first().addClass("line-clamp");
			}
			else{
				$(e.currentTarget).attr("title" , Hypr.getLabel('readMore'));
				$(e.currentTarget).text(Hypr.getLabel('readMore'));
				$('.category-description p').first().removeClass("line-clamp");
			}
		});
		
		//calculates description content height & hides less/more button
		var collapseSectionHeight = $(".description-content").height();
		var descriptionHeight = function(){
			$(".description-content").css('min-height','auto');
			$(".toggle-container").hide();
		};
		if($(window).width() < 768){
			if(collapseSectionHeight < 200){
				descriptionHeight();
			}
		}
		if($(window).width() > 767){
			if(collapseSectionHeight < 130){
				descriptionHeight();
			}
		}
		//end
		var numberOfParagarph = $('.category-description p').length;
		var characterCount = $(".category-description p").eq(0).text().length;
		var minCount;
		isMobile = navigator.userAgent.match(/(iPhone)|(iPod)|(android)|(webOS)/i);
		isiPad = ua.indexOf('ipad') > -1 || ua.indexOf('macintosh') > -1 && 'ontouchend' in document;
		if(isiPad) {
			minCount = 385;
		}else if(isMobile){
			minCount = 200;
		}else{
			minCount = 448;
		}
		if(numberOfParagarph > 1 || characterCount > minCount){
			$(".description-container .toggle-container").css('display','block');
			$('.category-description p').first().removeClass("line-clamp");
		}else{
			$(".description-container .toggle-container").css('display','none');
			$('.category-description p').first().addClass("line-clamp");
		}
	/**
	 * End of New Category description Widget Js
	 */
	 /** Feature Image widget JS **/
	  var pagecontext = require.mozuData('pagecontext');
    var mozuGridColumns = $('div').hasClass('mozuGridColumns');
    var imageWidgetArray, mozuGrid;
    if(pagecontext.pageType == "web_page" || pagecontext.cmsContext.template.path == "parent-category" || pagecontext.cmsContext.template.path == "lavelOneCategory" || pagecontext.pageType == "category" && mozuGridColumns) {
        imageWidgetArray = $('.mozuGridColumns');
        imageWidgetArray.each(function(index,item){
            mozuGrid = $(this).parents("div[class^='mz-cms-col-']");
            if(mozuGrid[0]) {
                $(this).parents("div[class^='mz-cms-col-']").parent().addClass('manageMobileDropZone manageDropZone');
                $(this).parents("div[class^='mz-cms-col-']").parent().addClass('flex flex-wrap');
                $(this).parents("div[class^='mz-cms-col-']").addClass('manageMobileDropZoneWidth');
                $(this).removeClass('hide-on-site');
            } else {
                $(this).parents('.mz-cms-content').parent().addClass('manageMobileDropZone manageDropZone');
                $(this).removeClass('hide-on-site');
            }
        });  
    }
    /** End of Feature Image widget JS **/
});
