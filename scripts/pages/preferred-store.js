define([
	'modules/jquery-mozu',
	'hyprlive',
	"underscore",
	'modules/backbone-mozu',
	'modules/api',
	'pages/mystore-details',
	'modules/models-customer',
	'widgets/image-slider',
	'widgets/merchandised-products',
	'widgets/featured-products',
	'modules/cookie-utils'],
	function ($, Hypr, _, Backbone, api, StoreViews, CustomerModels, ImageSlider, MerchandisedWidget,
		FeatureProductWidget, CookieUtils) {

		//Page Context
		var pageContext = require.mozuData('pagecontext');
		var user = require.mozuData('user');
		var apiContext = require.mozuData('apicontext');
		var ua = navigator.userAgent.toLowerCase();

		var isHomeFurnitureSite = false;
		if (Hypr.getThemeSetting("homeFurnitureSiteId") === apiContext.headers["x-vol-site"]) {
			isHomeFurnitureSite = true;
		}

		var preferredStore = localStorage.getItem('preferredStore') ? $.parseJSON(localStorage.getItem('preferredStore')) : undefined;		
		if (!pageContext.isEditMode) {
			if (user.isAuthenticated && !user.isAnonymous) {
				var shopperStore, attribute;
				var customer = new CustomerModels.EditableCustomer();
				customer.set("id", user.accountId);
				customer.fetch().then(function (response) {
					if (isHomeFurnitureSite) {
						attribute = _.findWhere(response.apiModel.data.attributes, { fullyQualifiedName: Hypr.getThemeSetting('hfPreferredStore') });
					} else {
						attribute = _.findWhere(response.apiModel.data.attributes, { fullyQualifiedName: Hypr.getThemeSetting('preferredStore') });
					}
					if (attribute) {
						shopperStore = _.first(attribute.values);
					}
					if (shopperStore) {
						if (preferredStore) {
							if (shopperStore === 'N/A') {
								shopperStore = preferredStore;
								updateCustomerAttribute();
								renderPreferredStoreView();
								sendDatalayer(pageContext.user.userId, preferredStore);
							} else {
								compareStores(shopperStore, preferredStore.code);
							}
						} else {
							if (window.location.search.indexOf('selectNewStore=') > 0) {
								diplayClosedStoreLink();
							} else if (shopperStore != 'N/A') {
								// set user purchase location 
								setPurchaseLocation(shopperStore);
							} else {
								$("#noStoreSelected").removeClass("hidden");
							}
						}
					} else {
						if (preferredStore) {
							updateCustomerAttribute();
							sendDatalayer(pageContext.user.userId, preferredStore);
						} else {
							if (window.location.search.indexOf('selectNewStore=') > 0) {
								diplayClosedStoreLink();
							} else {
								$("#noStoreSelected").removeClass("hidden");
							}
							changeStore("");
						}
					}
				});
			} else {
				//set preferred store from 'myStore' parameter of the URL based on store in link from email (currently implemented for anonymous user)
				if (window.location.search.indexOf('myStore') > 0 && window.location.search.indexOf('selectNewStore') == -1) {
					var myStore = window.location.search.split('myStore=')[1].split('&')[0];
					if (preferredStore) {
						if (myStore != preferredStore.code) {
							setPurchaseLocation(myStore);
						}
					} else {
						setPurchaseLocation(myStore);
					}
				} else {
					if (!preferredStore) {
						if (window.location.search.indexOf('selectNewStore=') > 0) {
							diplayClosedStoreLink();
							document.cookie = "showDialog=" + false + ";path=/;Secure;SameSite=None";
						} else {
							document.cookie = "showDialog=" + true + ";path=/;Secure;SameSite=None";
							$("#noStoreSelected").removeClass("hidden");
						}
						// check navigation enabled or not
						if (navigator.geolocation) {
							navigator.geolocation.getCurrentPosition(locationFound, locationError);
						} else {
							useDefaultStoreAsPreferredStore();
						}
					} else {
						if (!pageContext.purchaseLocation) {
							setPurchaseLocationWithoutReload(preferredStore.code);
						} else {
							console.log('Current purchaseLocation', pageContext.purchaseLocation.code);
							renderPreferredStoreView();
						}
						sendDatalayer(pageContext.user.userId, preferredStore);
					}
				} 
			}
		}

		function useIpBasedStoreAsPreferredStore() {
			// turn off/on IP location store selection-IWM-1863
			if (!Hypr.getThemeSetting('ipAddressStoreLocator')) {
				console.log("The IP-based Store Locator functionality has been disabled.");
				useDefaultStoreAsPreferredStore();
				return;
			}
			$.ajax({
				url: "/get-geo-location-by-ip",
				type: "GET",
				success: function (response) {
					console.log("GET /get-geo-location-by-ip response: " + response);
					var geoLocation = {
						"lat": response.latitude,
						"lng": response.longitude
					};
					lookupAndSetPurchaseLocation(geoLocation);
				},
				error: function (error) {
					console.log("GET /get-geo-location-by-ip error: " + error);
					useDefaultStoreAsPreferredStore();
				}
			});
		}
    
		function useDefaultStoreAsPreferredStore() {
			var defaultStore = Hypr.getThemeSetting('defaultStoreCode');
			if (defaultStore) {
				console.log('Using default store ' + defaultStore + ' as preferred store.');
				setPurchaseLocation(defaultStore, true);
			} else {
				console.log('No default store is configured.');
				document.cookie = "showDialog=" + true + ";path=/;Secure;SameSite=None";
				$("#noStoreSelected").removeClass("hidden");
			}
		}

		/*
		This function initialize and render preferred store views  in header 
		*/

		function getCookieValue(cookieName) {
			var b = document.cookie.match('(^|;)\\s*' + cookieName + '\\s*=\\s*([^;]+)');
			return b ? b.pop() : '';
		}

		function sendDatalayer(userId, preferredStoreCookie) {
			function getParameterByName(name, url) {
				if (!url) url = window.location.href;
				name = name.replace(/[\[\]]/g, '\\$&');
				var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'), results = regex.exec(url);
				if (!results) return null;
				if (!results[2]) return '';
				return decodeURIComponent(results[2].replace(/\+/g, ' '));
			}

			var r1emi = getParameterByName('r1emi', window.location.href);
			if (user.isAnonymous === true && user.isAuthenticated === false) {
				dataLayer.push({
					'storeId': preferredStoreCookie ? preferredStoreCookie.code : 'No Store Selected',
					'dimension2': r1emi ? r1emi : '',
					'storeName': preferredStoreCookie ? preferredStoreCookie.name : 'No Store Selected'
				});
			} else {
				dataLayer.push({
					'userId': user.userId,
					'storeId': preferredStoreCookie ? preferredStoreCookie.code : 'No Store Selected',
					'dimension2': r1emi ? r1emi : '',
					'storeName': preferredStoreCookie ? preferredStoreCookie.name : 'No Store Selected'
				});
			}
		}

		function renderPreferredStoreView() {
			var storeDetails = window.storeDetails = new StoreViews.MyStoreDetails({
				el: $("#mystore-details"),
				model: new Backbone.MozuModel($.parseJSON(localStorage.getItem('preferredStore')))
			});
			var myStore = window.myStore = new StoreViews.MyStore({
				el: $("#my-store"),
				model: new Backbone.MozuModel($.parseJSON(localStorage.getItem('preferredStore')))
			});
			var todayHoursMobile = window.todayHoursMobile = new StoreViews.StoreTodayHours({
				el: $("#todays-hours-mobile"),
				model: new Backbone.MozuModel($.parseJSON(localStorage.getItem('preferredStore')))
			});
			var locale = require.mozuData('apicontext').headers['x-vol-locale'];
			locale = locale.split('-')[0];
			api.get("location", { code: myStore.model.get('code') }).then(function (response) {
				var selectedStore = response.data;
				var isStoreOpen = checkStoreIsOpen(selectedStore);
				if (isStoreOpen) {
					myStore.setNotes(selectedStore.note, locale);
					myStore.render();

					//PB-STARTS IWM-135 Mobile Store Header Changes to Include Store Contact Info
					storeDetails.setNotes(selectedStore.note, locale);
					storeDetails.render();
					//PB-ENDS IWM-135 Mobile Store Header Changes to Include Store Contact Info
					todayHoursMobile.render();
				}
			}, function (error) {
				console.error("location api Error: ", error);
				if (error.errorCode === "ITEM_NOT_FOUND") {
					$("#noStoreSelected").removeClass("hidden");
					$.removeCookie('preferredStore', { path: '/' });
					updateCustomerAttributeToNewStore("");
				}
			});
		}

		function checkStoreIsOpen(selectedStore) {
			var isStoreClosed = _.findWhere(selectedStore.attributes, { "fullyQualifiedName": Hypr.getThemeSetting("storeClosed") });
			var newStoreNumber = _.findWhere(selectedStore.attributes, { "fullyQualifiedName": Hypr.getThemeSetting("newStoreNumber") });
			var storeClosed = false;
			var storeTransferred = false;
			if (isStoreClosed && isStoreClosed.values[0]) {
				document.cookie = "showDialog=" + false + ";path=/;Secure;SameSite=None";
				if (newStoreNumber && newStoreNumber.values.length > 0) {
					var newStore = JSON.stringify(newStoreNumber.values[0]);
					console.log("Store is transfered to newStoreNumber:" + newStore);
					storeTransferred = true;
					updateCustomerAttributeToNewStore(newStore);
				} else {
					console.log("Store is closed");
					storeClosed = true;
					$.removeCookie('preferredStore', { path: '/' });
					if (user.isAuthenticated && !user.isAnonymous) {
						changeStore(selectedStore.address.postalOrZipCode);
					} else {
						$("#noStoreSelected").removeClass("hidden");
						setPurchaseLocation("");
					}
				}
			}
			if (!storeClosed && !storeTransferred) {
				return true;
			} else {
				if (storeClosed && !storeTransferred) {
					diplayClosedStoreLink();
				}
				return false;
			}
		}

		function diplaySelectStoreLink() {
			$("#noStoreSelected").removeClass("hidden");
			$("#no-store-dialog").show();
			setTimeout(function () {
				$("#no-store-dialog").fadeOut();
			}, 8000);
		}

		function diplayClosedStoreLink() {
			$("#noStoreSelected").removeClass("hidden");
			var storewidth = $("#selectStore").width();
			$(".store-selected-popup").css("left", 80 + storewidth);
			$(".home-furniture-content .store-selected-popup").css("left", 168 + storewidth);
			$("#closed-store-dialog").show();
			setTimeout(function () {
				$("#closed-store-dialog").fadeOut();
			}, 8000);
		}

		/*
		This function accept input parameter as store code and call Arc action to set purchase location. 
		On success - api call to fetch location with purchase location. Save data on success in 
		cookie then reoad page.
		*/
		function setPurchaseLocation(store, userConfirmationRequired) {
			$.ajax({
				url: "/set-purchase-location",
				data: { "purchaseLocation": store },
				type: "GET",
				success: function (response) {
					console.log("setPurchaseLocation response:" + response);
					if (store === "") {
						changeStore("");
					}
					api.get("location", { code: store }).then(function (response) {
						var selectedStore = response.data;
						var isStoreOpen = checkStoreIsOpen(selectedStore);
						if (isStoreOpen) {
							CookieUtils.setPreferredStore(selectedStore);
							if(userConfirmationRequired) {
								CookieUtils.setCookie('preferredStore_userConfirmationRequired', true); /** IWM-1471 */
							}
							document.cookie = "showDialog=" + true + ";path=/;Secure;SameSite=None";
							if (window.location.search.indexOf('myStore') > 0) {
								changeStore("");
							} else {
								window.location.reload();
							}
						}
					}, function (error) {
						console.error("location api Error: ", error);
						if (error.errorCode === "ITEM_NOT_FOUND") {
							$("#noStoreSelected").removeClass("hidden");
							$.removeCookie('preferredStore', { path: '/' });
							updateCustomerAttributeToNewStore("");
						}
						return;
					});
				},
				error: function (error) {
					// if arc action failed
				}
			});
			console.log("store:: " + store);
		}

		function setPurchaseLocationWithoutReload(store) {
			$.ajax({
				url: "/set-purchase-location",
				data: { "purchaseLocation": store },
				type: "GET",
				success: function (response) {
					api.get("location", { code: store }).then(function (response) {
						var selectedStore = response.data;
						CookieUtils.setPreferredStore(selectedStore);
						sendDatalayer(pageContext.user.userId, selectedStore);
						document.cookie = "showDialog=" + true + ";path=/;Secure;SameSite=None";
						renderPreferredStoreView();
						if (pageContext.cmsContext.page.path == 'home') {
							var numberOfBanner = $(".mz-image-slider-container .bannerimg ").not(".slick-cloned");
							_.each(numberOfBanner, function (element, index) {
								var renderEle = $(element).find(".image-slide-products-view");
								var imageSlideProductsView = new ImageSlider.ImageSlideProductsView({
									el: $(renderEle),
									model: new Backbone.MozuModel()
								});
								imageSlideProductsView.render();
							});

							var numMerWidgets = $(".merchandises-products");

							_.each(numMerWidgets, function (element, index) {
								var renderEle = $(element).find(".merchandised-product-widet-view");
								var merchandisedProductView = new MerchandisedWidget.MerchandisedProductViews({
									el: $(renderEle),
									model: new Backbone.MozuModel()
								});
								merchandisedProductView.render();
							});

							var numFeatureWidgets = $(".featured-products-widget");
							_.each(numFeatureWidgets, function (element, index) {
								var renderEle = $(element).find(".feature-product-widget-view");
								var feartureProductsView = new FeatureProductWidget.FeartureProductsViews({
									el: $(renderEle),
									model: new Backbone.MozuModel()
								});
								feartureProductsView.render();
							});
						}
					});
				},
				error: function (error) {
					// if arc action failed
				}
			});
		}
		/*
		This function compares currently set purchase location with preferred store in cookie
		If equal - render views else make api call to fetch location with current purchase location
		on success of api call save response in cookie render view.
		If user authenticated update customer attribute
		If purchase location is not set and cookie is present call arc to to purchase location.
		*/
		function compareStores(shopperStore, preferredStore) {
			var preferredStoreCookie = $.parseJSON(localStorage.getItem('preferredStore'));
			if (pageContext.purchaseLocation) {
				if (shopperStore) {
					if (shopperStore === preferredStore) {
						renderPreferredStoreView();
						sendDatalayer(pageContext.user.userId, preferredStoreCookie);
					} else {
						setPurchaseLocation(shopperStore);
					}
				} else {
					var purchaseLocation = pageContext.purchaseLocation.code;
					if (preferredStore === purchaseLocation) {
						renderPreferredStoreView();
					} else {
						//get purchase location and set cookie with updated store
						api.get("location", { code: purchaseLocation }).then(function (response) {
							var selectedStore = response.data;
							CookieUtils.setPreferredStore(selectedStore);
							$.removeCookie('preferredStore_userConfirmationRequired', { path: '/' });
							renderPreferredStoreView();
							sendDatalayer(pageContext.user.userId, preferredStoreCookie);
						});
					}
				}
			} else {
				if (pageContext.cmsContext.page.path == 'home') {
					if (user.isAuthenticated && !user.isAnonymous) {
						setPurchaseLocationWithoutReload(shopperStore);
					} else {
						setPurchaseLocationWithoutReload(preferredStore);
					}
				} else {
					if (user.isAuthenticated && !user.isAnonymous) {
						setPurchaseLocation(shopperStore);
					} else {
						setPurchaseLocation(preferredStore);
					}
				}
			}
		}
		/*
		This function updates the authenticated user's preferred store. i.e customer attribute
		*/
		function updateCustomerAttribute() {
			//update customer attribute here with preferred store
			var preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
			var customer = new CustomerModels.EditableCustomer();
			customer.set("id", user.accountId);
			customer.fetch().then(function (response) {
				var attribute;
				if (isHomeFurnitureSite) {
					attribute = _.findWhere(response.apiModel.data.attributes, { fullyQualifiedName: Hypr.getThemeSetting('hfPreferredStore') });
				} else {
					attribute = _.findWhere(response.apiModel.data.attributes, { fullyQualifiedName: Hypr.getThemeSetting('preferredStore') });
				}
				var attributeData = attribute;
				response.updateAttribute(attributeData.fullyQualifiedName, attributeData.attributeDefinitionId, [preferredStore.code]);
			});
		}

		function updateCustomerAttributeToNewStore(newStore) {
			var customer = new CustomerModels.EditableCustomer();
			if (user.isAuthenticated && !user.isAnonymous) {
				customer.set("id", user.accountId);
				customer.fetch().then(function (response) {
					var attribute;
					if (isHomeFurnitureSite) {
						attribute = _.findWhere(response.apiModel.data.attributes, { fullyQualifiedName: Hypr.getThemeSetting('hfPreferredStore') });
					} else {
						attribute = _.findWhere(response.apiModel.data.attributes, { fullyQualifiedName: Hypr.getThemeSetting('preferredStore') });
					}
					var attributeData = attribute;
					response.updateAttribute(attributeData.fullyQualifiedName, attributeData.attributeDefinitionId, [newStore]);
					setPurchaseLocation(newStore);
				});
			} else {
				setPurchaseLocation(newStore);
			}
		}

		/*
		This is geo location success call back function. This function set user's current preferred store according to their 
		location i.e nearest location 
		*/
		function locationFound(position) {
			$('#loading-store').removeClass('hidden');
			$('.loader-section').removeClass('hidden');
			var shopperPos = {
				"lat": position.coords.latitude,
				"lng": position.coords.longitude
			};
			if (ua.indexOf('chrome') > -1) {
				if (/edg|edge/i.test(ua)) {
					$.cookie("currentPosition", JSON.stringify(shopperPos), { path: '/' });
				} else {
					document.cookie = "currentPosition=" + JSON.stringify(shopperPos) + ";path=/;Secure;SameSite=None";
				}
			} else {
				$.cookie("currentPosition", JSON.stringify(shopperPos), { path: '/' });
			}
			lookupAndSetPurchaseLocation(shopperPos);
		}

		function lookupAndSetPurchaseLocation(geoLocation) {
			var storeDistance = Hypr.getThemeSetting("storeLookupDistance");
			var filter = 'geo near(' + geoLocation.lat + "," + geoLocation.lng + "," + storeDistance * 1000 + ')';
			if (isHomeFurnitureSite) {
				filter = filter + ' and ' + Hypr.getThemeSetting("customStoreType") + ' eq "Home Furniture"';
			} else {
				filter = filter + ' and ' + Hypr.getThemeSetting("customStoreType") + ' ne "Home Furniture"';
			}
			filter = filter + ' and ' + Hypr.getThemeSetting("displayOnline") + ' eq true and locationType.Code eq "PH"';
			api.get("locations", { filter: filter }).then(function (response) {
				if (response && response.data.items.length > 0) {
					console.log('locationFound: store code: ' + response.data.items[0].code);
					setPurchaseLocation(response.data.items[0].code, true);
				} else {
					console.log('No store found within ' + storeDistance + 'km of given location.');
					useDefaultStoreAsPreferredStore();
					$('#loading-store').hide();
					$('.loader-section').hide();
					setTimeout(function () {
						if ($.cookie("showDialog")) {
							$.removeCookie('showDialog', { path: '/' });
						}
						$(".no-store-box , .store-selected-popup").fadeOut();
					}, 8000);
				}
			});
		}

		/*
		This is geo location Failure call back function. This function set purchase location to default as user blocks
		their location or browser doest not allow to access location.
		*/
		function locationError(error) {
			switch (error.code) {
				case error.PERMISSION_DENIED:
					console.log("locationError: User denied the request for Geolocation.");
					break;
				case error.POSITION_UNAVAILABLE:
					console.log("locationError: Location information is unavailable.");
					break;
				case error.TIMEOUT:
					console.log("locationError: The request to get user location timed out");
					break;
				case error.UNKNOWN_ERROR:
					console.log("locationError: An unknown error occurred.");
					break;
			}
			//user blocked their location
			if (error.code === error.PERMISSION_DENIED) {
				useIpBasedStoreAsPreferredStore();
			} else {
				useDefaultStoreAsPreferredStore();
			}
			setTimeout(function () {
				if ($.cookie("showDialog")) {
					$.removeCookie('showDialog', { path: '/' });
				}
				$(".no-store-box , .store-selected-popup").fadeOut();
			}, 8000);
		}

		function changeStore(newStorePostalCode) {
			var locale = require.mozuData('apicontext').headers['x-vol-locale'];
			locale = locale.split('-')[0];
			var currentLocale = locale === 'fr' ? '/fr' : '/en';
			var pageContext = require.mozuData('pagecontext');
			var redirectUrl = '';
			if (user.isAuthenticated && !user.isAnonymous) {
				var queryStr = 'selectNewStore=true';
				queryStr = newStorePostalCode === '' ? queryStr : 'postalCode=' + newStorePostalCode + '&' + queryStr;
				if (window.location.pathname.length > 1) {
					if (pageContext.pageType == 'search') {
						var url = window.location.search.split('&')[0];
						redirectUrl = currentLocale + "/store-locator?" + queryStr + "&returnUrl=" + window.location.pathname + url;
					} else {
						if ((window.location.pathname !== (currentLocale + '/store-locator')) && (window.location.pathname.indexOf("/store/") === -1)) {
							redirectUrl = currentLocale + "/store-locator?" + queryStr + "&returnUrl=" + window.location.pathname;
						}
					}
				} else {
					if ((window.location.pathname !== (currentLocale + '/store-locator')) && (window.location.pathname.indexOf("/store/") === -1)) {
						redirectUrl = currentLocale + "/store-locator?" + queryStr;
					}
				}
			} else {
				if (window.location.search.indexOf('?') < 0) {
					redirectUrl = window.location.pathname + '?selectNewStore=true';
				} else {
					if (window.location.search.indexOf('selectNewStore=') < 0) {
						redirectUrl = window.location.pathname + window.location.search + '&selectNewStore=true';
						//	 				window.location.assign(redirectUrl);
					} else {
						window.location.reload();
					}
				}
			}
			if (redirectUrl !== '') {
				setTimeout(function () { window.location.href = redirectUrl; }, 1000);
			}
		}

		window.setPreferredStoreFromCookie = function () {
			if(!localStorage.getItem('preferredStore') && $.cookie('preferredStore')) {
				setPurchaseLocation(JSON.parse($.cookie("preferredStore")).code);
			} else if(!localStorage.getItem('preferredStore') && !$.cookie('preferredStore') && pageContext.purchaseLocation && pageContext.purchaseLocation.code) {
				setPurchaseLocation(pageContext.purchaseLocation.code);
			}
		};

		window.setPreferredStoreFromCookie();

		$(document).ready(function () {
			$(".week-hour").each(function () {
				var getContent = $(this).text();
				var newString = getContent.replace('(', '<br />(');
				$(this).html(newString);
			});
			var location = require.mozuData('apicontext').headers["x-vol-locale"];
			if (location === 'fr-CA') {
				var getText = $("#todays-hours-mobile div.visible-xs").text();
				var newText = getText.replace('(Curbside', ' <br />(Curbside');
				$("#todays-hours-mobile div.visible-xs").html(newText);
			} else {
				var getEnText = $("#todays-hours-mobile div.visible-xs").text();
				var newEnText = getEnText.replace('(Curbside', ' <br />(Curbside');
				$("#todays-hours-mobile div.visible-xs").html(newEnText);
			}
		});
	});