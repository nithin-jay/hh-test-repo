﻿require([
	"modules/jquery-mozu", 
	"underscore", 
	"hyprlive", 
	"modules/backbone-mozu", 
	"modules/cart-monitor",
	"modules/models-product", 
	"modules/views-productimages",  
	"hyprlivecontext",
    'modules/api',
    'modules/check-nearby-store',
    'vendor/jquery/moment',
    'vendor/timezonesupport/timezonesupport',
    'modules/cookie-utils',
	'slick',
	'cloud',
	'shim!vendor/jquery/rrssb.min[jQuery=jquery]'
    ], 
	function ($, _, Hypr, Backbone, CartMonitor, ProductModels, ProductImageViews, HyprLiveContext, api,
        CheckNearbyStoreModal, moment, timeZoneSupport, CookieUtils) {
	var user = require.mozuData('user');
    var apiContext = require.mozuData('apicontext');
    var breadCrumbCategory = [];
    var filtersString = "", 
    today = new Date(),
    isHomeFurnitureSite = false,
    isMapboxEnabled = false;
    //delete this variable once pdp arc is in action
    var usePDPArcFunction = Hypr.getThemeSetting("usePDPArcFunction");
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    ua = navigator.userAgent.toLowerCase();
    
    if (Hypr.getThemeSetting("homeFurnitureSiteId") === require.mozuData('apicontext').headers["x-vol-site"]) {
        filtersString = "properties.storeType in['Home Furniture']";
        isHomeFurnitureSite = true;
    } else {
        if (window.location.search.indexOf('storeFilter') < 0)
            filtersString = "properties.storeType in['Home Hardware','Home Building Centre', 'Home Hardware Building Centre'] ";
    }
    
    /* Adding display online filter */
    if (!filtersString) {
        filtersString = "properties.displayOnline eq true ";
    } else {
        filtersString += "and properties.displayOnline eq true ";
    }

    var contextSiteId = apiContext.headers["x-vol-site"];
    var homeFurnitureSiteId = Hypr.getThemeSetting("homeFurnitureSiteId");

    var ProductView = Backbone.MozuView.extend({
        templateName: 'modules/product/product-detail',
        additionalEvents: {
            "change [data-mz-product-option]": "onOptionChange",
            "blur [data-mz-product-option]": "onOptionChange",
            "click [data-mz-product-option]": "onOptionChange",
            "change [data-mz-value='quantity']": "onQuantityChange",
            "click [data-mz-value='qty-increment']": "onQuantityChange",
            "click [data-mz-value='qty-decrement']": "onQuantityChange",
            "keyup input[data-mz-value='quantity']": "onQuantityChange",
            "click .changePreferredStore": "changePreferredStore",
            "click #checkNearByStore": "checkNearbyStores"
        },
        render: function () {
            var me = this;
            $("#main-content-loader").hide();
            //set EHF value in 
            var ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
            var ehfOption = me.model.get('options').findWhere({'attributeFQN': ehfAttributeFQN});
            if(ehfOption && me.model.get('ehfValue')) {
            	ehfOption.set('shopperEnteredValue', me.model.get('ehfValue'));
            	ehfOption.set('value', me.model.get('ehfValue'));
            }
            if(navigator.userAgent.match(/(iPhone)|(iPod)|(android)|(webOS)|(ipad)/i)){
                me.model.set("isDesktop",false);
            }else{
                me.model.set("isDesktop",true);
            } 
            Backbone.MozuView.prototype.render.apply(this);
            // var productImagesView = new ProductImageViews.ProductPageImagesView({
            //     el: $('[data-mz-productimages]'),
            //     model: me.model
            // });
            
            // productImagesView.render(); 
            this.$('[data-mz-is-datepicker]').each(function (ix, dp) {
                $(dp).dateinput().css('color', Hypr.getThemeSetting('textColor')).on('change  blur', _.bind(me.onOptionChange, me));
            });
            var prodDesc  = $(".collapse-section").find("ul");
            _.each(prodDesc, function(e){
                var listElm = $(e).find("li");
                if(listElm.length == 1){
                    $(e).addClass("no-column-count");
                }
            });
            setTimeout(function(){
                $(".business-messages").removeClass('hidden');
                if(homeFurnitureSiteId !== contextSiteId){
                    $("#marketingProposition").removeClass("hidden");
                }
            },700);
            /* Info Popover */
            $('[data-toggle="popover"]').popover();
            var dataAttribute = $('.mz-productdetail-btns-container').find('button').attr('data-mz-action');
            if(dataAttribute != 'addToCartForPickup'){
                $('.mz-productdetail-btns-container').addClass('full-width-btn');
            }
        },
	 onOptionChange: function (e) {
            return this.configure($(e.currentTarget));
        },
        onQuantityChange: _.debounce(function (e) {
        	if(isNaN($(e.currentTarget).val()) || $(e.currentTarget).val() < 0) {
        		e.preventDefault();
        		$(e.currentTarget).val(1);
        		$(".decrement").addClass("is-disabled");
        	}else {
	        	if($('#add-to-wishlist').prop('disabled')){
	        		$('#add-to-wishlist').text(Hypr.getLabel('saveToList'));	
	        		$('#add-to-wishlist').prop('disabled',false);
	        	}
	        	var field = $(e.currentTarget).attr('data-mz-value'); //this field is used to increment/decrement the quantity                
	        	var quantity = $(e.currentTarget).siblings('.mz-productdetail-qty').val();    
	        	
	        	if(field === 'qty-decrement'){    
	        		$(e.currentTarget).siblings('.mz-productdetail-qty').val(parseInt(quantity, 10) - 1); 
	        	}
	        	if(field === 'qty-increment'){
	        		$(e.currentTarget).siblings('.mz-productdetail-qty').val(parseInt(quantity, 10) + 1);
	        	}    
	        	var $qField = "";    
	        	if(field != 'quantity'){    
	        		$qField = $(e.currentTarget).siblings('.mz-productdetail-qty');    
	        	}else {    
	        		$qField = $(e.currentTarget);    
	        	}
	        	
	            var newQuantity = parseInt($qField.val(), 10);
	            
	            if(newQuantity <= 1 || isNaN(newQuantity)){
	            	$qField.val(1);
	            	newQuantity = 1;
	            	$(".decrement").addClass("is-disabled");
	            }else {
	            	$(".decrement").removeClass("is-disabled");
	            }
	            
	            if (!isNaN(newQuantity)) {
	                this.model.updateQuantity(newQuantity);
	                $qField.val(newQuantity); //fix to set new value excluding decimal point
	            }
        	}
        },500),
        configure: function ($optionEl) {
            var newValue = $optionEl.val(),
                oldValue,
                id = $optionEl.data('mz-product-option'),
                optionEl = $optionEl[0],
                isPicked = (optionEl.type !== "checkbox" && optionEl.type !== "radio") || optionEl.checked,
                option = this.model.get('options').findWhere({'attributeFQN':id});
            if (option) {
                if (option.get('attributeDetail').inputType === "YesNo") {
                    option.set("value", isPicked);
                } else if (isPicked) {
                    oldValue = option.get('value');
                    if (oldValue !== newValue && !(oldValue === undefined && newValue === '')) {
                        option.set('value', newValue);
                    }
                }
            }
            var productDetailView = new ProductDetailView({
                    el: $('#product-header-detail'),
                    model: this.model
                });
            productDetailView.render();

        },
        addToCart: function () {
        	
            this.model.addToCart();
        },
        //to be changed when we integrate Ship to store functionality
        addToCartForPickup: function () {
        	var quantity = this.model.get("quantity");
        	var preferredStore = $.parseJSON(localStorage.getItem('preferredStore'));
        	this.model.addToCartForPickup(preferredStore.code, preferredStore.name, quantity);
        },
        addToWishlist: function () {
        	var me = this;
        	if (user.isAnonymous) {
        		var locale = require.mozuData('apicontext').headers['x-vol-locale'];
        		var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
                locale = locale.split('-')[0];
                var currentLocale = '';
                if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
                	currentLocale = locale === 'fr' ? '/fr' : '/en';
                }
				window.location.href=currentLocale + "/user/login?productCode="+me.model.get('productCode');
        	} else {
        		this.model.addToWishlist();
        	}
            
        },
        checkLocalStores: function (e) {
            var me = this;
            e.preventDefault();
            this.model.whenReady(function () {
                var $localStoresForm = $(e.currentTarget).parents('[data-mz-localstoresform]'),
                    $input = $localStoresForm.find('[data-mz-localstoresform-input]');
                if ($input.length > 0) {
                    $input.val(JSON.stringify(me.model.toJSON()));
                    $localStoresForm[0].submit();
                }
            });
        },
        changePreferredStore: function(e){
            e.preventDefault();
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            locale = locale.split('-')[0];
            var currentLocale = '';
            if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
            	currentLocale = locale === 'fr' ? '/fr' : '/en';
            }
            window.location.href=currentLocale + "/store-locator?returnUrl="+window.location.pathname;
        },
        setAeroplanMiles: function(aeroplanMiles) {
        	var me = this;
        	if(!usePDPArcFunction){
	        	var documentListForAeroplanPromo = Hypr.getThemeSetting('documentListForAeroplanPromo');
	        	api.get('documentView', {listName: documentListForAeroplanPromo}).then(function(response) {
	            	if(response.data.items.length > 0) {
	            		var aeroplanMilesFactor = response.data.items[0].properties.markupFactor;
	            		me.model.set('aeroplanMiles', aeroplanMilesFactor);
	            		var promoCode = response.data.items[0].properties.promoCode;
	                    me.model.set('aeroplanPromoCode', promoCode);
	                    var startDate = new Date(response.data.items[0].activeDateRange.startDate);
	                    var endDate = new Date(response.data.items[0].activeDateRange.endDate);
	                    me.model.set('aeroplanPromoDate', true);
	            		me.model.set('aeroplanStartDate', startDate.getDate());
	                    me.model.set('aeroplanStartMonth', startDate.getMonth() + 1);
	                    me.model.set('aeroplanStartYear', startDate.getFullYear());
	                    me.model.set('aeroplanEndDate', endDate.getDate());
	                    me.model.set('aeroplanEndMonth', endDate.getMonth() + 1);
	                    me.model.set('aeroplanEndYear', endDate.getFullYear());
	            		me.render();
	            	}else {
	            		me.model.set('aeroplanMiles', aeroplanMiles);
	            		me.render();
	            	}
	        	});
        	} else {
        		if(!me.model.get('aeroplanMiles')){
        			me.model.set('aeroplanMiles', aeroplanMiles);
        		}
        	}
        },
        initialize: function () {
            // handle preset selects, etc
            var me = this;
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            /**
             * 
             * Below Code helps to identify whether there is a spin or not for a product.
             * 
             * */
         	 var url = Hypr.getThemeSetting("sirvUrl")+"/products/"+me.model.get('productCode').substring(0,3)+"/"+me.model.get('productCode')+"/spin/"+"spin.spin";
             var xhr = new XMLHttpRequest();
         	 xhr.onreadystatechange = function(event) {
              if (event.target.readyState == 4) {
                if (event.target.status == 200 || event.target.status === 0) {
                	me.model.set("isSpinAvailable", true);
                } else{
                	me.model.set("isSpinAvailable", false);
                }
                window.productImagesView.render();
              }
            };
	            xhr.open('GET', url, true);
	            xhr.send();
        	locale = locale.split('-')[0];
        	me.model.set("currentLocale", locale);
			me.model.set("currentSite", currentSite);
			if(usePDPArcFunction){
        		console.log("********usePDPArcFunction***********" + usePDPArcFunction);
        		var pdpArcStr = me.model.get('mfgPartNumber');
        		if(pdpArcStr){
        			var pdpArcData = JSON.parse(pdpArcStr);
        			me.model.set('aeroplanMiles', pdpArcData.aeroplanMiles);
        			me.model.set('aeroplanPromoCode', pdpArcData.aeroplanPromoCode);
        			me.model.set('aeroplanPromoDate', pdpArcData.aeroplanPromoDate);
        			me.model.set('aeroplanStartDate', pdpArcData.aeroplanStartDate);
        			me.model.set('aeroplanStartMonth', pdpArcData.aeroplanStartMonth);
        			me.model.set('aeroplanStartYear', pdpArcData.aeroplanStartYear);
        			me.model.set('aeroplanEndDate', pdpArcData.aeroplanEndDate);
        			me.model.set('aeroplanEndMonth', pdpArcData.aeroplanEndMonth);
        			me.model.set('aeroplanEndYear', pdpArcData.aeroplanEndYear);
        			
        			me.model.set('ehfValue', pdpArcData.ehfValue);
        			
        			me.model.set('stockAvailable', pdpArcData.stockAvailable);
        			me.model.set('isInventoryAvail', pdpArcData.isInventoryAvail);
        			me.model.set('isLimitedQuantity', pdpArcData.isLimitedQuantity);
        			
        			me.model.set('isItemRestricted', pdpArcData.isItemRestricted);
        			if(pdpArcData.isWebsiteInd){
        				me.model.set('isWebsiteInd', pdpArcData.isWebsiteInd);
        			}
        			
        			console.log("********pdpArcData***********" + pdpArcData);
        		}
        	}
            this.$('[data-mz-product-option]').each(function (index) {
            	if(index === 0){
            		var $this = $(this), isChecked, wasChecked;
	                if ($this.val()) {
	                    switch ($this.attr('type')) {
	                        case "checkbox":
	                        case "radio":
	                            isChecked = $this.prop('checked');
	                            wasChecked = !!$this.attr('checked');
	                            if ((isChecked && !wasChecked) || (wasChecked && !isChecked)) {
	                                me.configure($this);
	                            }
	                            break;
	                        default:
	                            me.configure($this);
	                    }
	                }
            	}
            });
            
            /* Hiding More/Less Toggle button based on container height*/
            if($(".collapse-section > .collapse-content").height() < 100){
                $(".toggle-container").hide();
            }else{
                $(".toggle-container").show();
            }
            if(localStorage.getItem('preferredStore')) {
        		me.model.set({"preferredStore":$.parseJSON(localStorage.getItem('preferredStore'))});
        	}
            var aeroplanMiles;
            me.model.on("productConfigured", function(response){
            	if(response.get('price.salePrice')) {
                	aeroplanMiles = Math.floor(response.get('price.salePrice')/2); 
                }else {
                	aeroplanMiles = Math.floor(response.get('price.price')/2); 
                }
                me.setAeroplanMiles(aeroplanMiles);
            });
            
            if(!me.model.get('hasPriceRange')) {
            	if(me.model.get('price.salePrice')) {
                	aeroplanMiles = Math.floor(me.model.get('price.salePrice')/2); 
                }else {
                	aeroplanMiles = Math.floor(me.model.get('price.price')/2); 
                }
                me.setAeroplanMiles(aeroplanMiles);
            }
            
            var recentlyViewedItems = $.cookie('recentlyViewd');
            if(recentlyViewedItems) {
            	if (me.model.get('productType') != 'Articles') {
            		recentlyViewedItems = recentlyViewedItems + ',' + me.model.get('productCode');
            	}
            	recentlyViewedItems = recentlyViewedItems.split(',');
            	if(recentlyViewedItems.length > 6){
            		recentlyViewedItems = recentlyViewedItems.slice(Math.max(recentlyViewedItems.length - 6, 1)); 
            	}
            	recentlyViewedItems = _.unique(recentlyViewedItems);
            	recentlyViewedItems = recentlyViewedItems.join(',');
            }else {
            	 if (me.model.get('productType') != 'Articles') {
            		 recentlyViewedItems = me.model.get('productCode');
            	 }
            }
            if (ua.indexOf('chrome') > -1) {
                if (/edg|edge/i.test(ua)){
                    $.cookie('recentlyViewd', recentlyViewedItems, {path: '/' });
                }else{
                    document.cookie = "recentlyViewd="+recentlyViewedItems+";path=/;Secure;SameSite=None";
                }
            } else {
                $.cookie('recentlyViewd', recentlyViewedItems, {path: '/' });
            }

            var frSiteId = Hypr.getThemeSetting("frSiteId");
            if(homeFurnitureSiteId === contextSiteId){
                me.model.set("isHomeFurnitureSite", true);
            }
            if(frSiteId === contextSiteId){
                me.model.set("isFrenchSite", true);   
            }

            if(localStorage.getItem('preferredStore')){
                var currentStore = $.parseJSON(localStorage.getItem('preferredStore')); 
                var attribute = _.findWhere(currentStore.attributes, {"fullyQualifiedName":Hypr.getThemeSetting("isEcomStore")});   
            
                if(attribute){
                    var isEcomStore = attribute.values[0] === "Y" ? true : false; 
                    me.model.set("isEcomStore", isEcomStore);
                }
                
                //check EHF for current product
            	var entityListForEHF = Hypr.getThemeSetting('entityListForEHF');
            	var productCode;
            	if(me.model.get('variations') && me.model.get('variations').length > 0) {
            		//from current variation find the variationProductCode
            		var productOptionValue = $('.mz-productoptions-option').val();
            		var variation = _.find(me.model.get('variations'), function(variation) {
            			return _.findWhere(variation.options, {'value': productOptionValue});
            		});
            		productCode = variation.productCode;
            	}else {
            		productCode = me.model.get('productCode');
            	}
            	var filterQuery = 'productCode eq ' + productCode + ' and province eq ' + currentStore.address.stateOrProvince;
            	if(!usePDPArcFunction){
	            	var ehfAttributeFQN = Hypr.getThemeSetting('ehfFees');
	            	api.get('entityList', { listName: entityListForEHF, filter: filterQuery }).then(function(response) {
	            		if(response.data.items.length > 0) { //if EHF is applied for current product
	            			me.model.set('ehfValue', response.data.items[0].feeAmt);
	            			me.render();
	            		}
	            	});
				}
            	
            	//entityListForItemRestricted
            	filterQuery = 'productCode eq ' + productCode + ' and (province eq ' + currentStore.address.stateOrProvince + ' or store eq ' + currentStore.code + ')';
            	if(!usePDPArcFunction){
	            	var entityListForItemRestricted = Hypr.getThemeSetting('entityListForItemRestricted');
	            	api.get('entityList', { listName: entityListForItemRestricted, filter: filterQuery}).then(function(response) {
	            		if(response.data.items.length > 0) {
	                        if(response.data.items[0].ecommerce_ind === "Y"){
	                            me.model.set('isItemRestricted', true);
	                        }else {
	                            me.model.set('isItemRestricted', false);
	                        }
	                        var websiteInd = response.data.items[0].website_ind;
	                        if(websiteInd === 'N'){
	            			     me.model.set('isWebsiteInd', true);
	                        }
	                        me.render();
	            		}else{
	                        me.model.set('isItemRestricted', true);
	                        me.render();
	                    }
	            	});
            	}
            	
                
                var isBOHStore = _.find(currentStore.attributes, function(attribute){
            		return attribute.fullyQualifiedName === Hypr.getThemeSetting('hhBOHStoreAttr');
                });

                if(!usePDPArcFunction){
                	var currentStoresWarehouse = _.find(currentStore.attributes, function(attribute){
                		return attribute.fullyQualifiedName === Hypr.getThemeSetting('warehouseAttr');
                	});
	            	if(currentStoresWarehouse) {
	            		var inventoryApi = '/api/commerce/catalog/storefront/products/' + productCode + '/locationinventory?locationCodes=' + currentStoresWarehouse.values[0];
	                	api.request("GET", inventoryApi).then(function(response){
	                		if(response && response.items.length > 0){
	                			if(response.items[0].stockAvailable > 0){
	                				me.model.set('isInventoryAvail', true);
	                                if(response.items[0].stockAvailable == 1 || response.items[0].stockAvailable == 2 || response.items[0].stockAvailable == 3){
	                                    me.model.set("isLimitedQuantity",true); 
	                                }
	                                me.render();
	                			}
	                		}
	                	});
	                }
                }

                //store level inventory fetch
                if(isBOHStore && isBOHStore.values[0]){
                    api.request("GET", '/api/commerce/catalog/storefront/products/' + productCode + '/locationinventory?locationCodes=' + currentStore.code).then(function(response){
                        if(response && response.items.length > 0){
                            me.model.set("bohStore",true); 
                            me.model.set("storeInventoryData",response.items[0]);
                            me.model.set("showBOH",true);
                            me.render(); 
                        }else{
                            me.model.set("bohStore",true);
                            me.model.set("showBOH",true);
                            me.render(); 
                        }
                    });
                }else{
                    api.get("location",{code:currentStore.code}).then(function(response){
                        if(response){
                            isBOHStore = _.find(response.data.attributes, function(attribute){
                                return attribute.fullyQualifiedName === Hypr.getThemeSetting('hhBOHStoreAttr');
                            });
                            if(isBOHStore && isBOHStore.values[0]){
                                var selectedStore = response.data;
							    CookieUtils.setPreferredStore(selectedStore);
                                api.request("GET", '/api/commerce/catalog/storefront/products/' + productCode + '/locationinventory?locationCodes=' + currentStore.code).then(function(response){
                                    if(response && response.items.length > 0){
                                        me.model.set("bohStore",true); 
                                        me.model.set("storeInventoryData",response.items[0]);
                                        me.model.set("showBOH",true);
                                        me.render(); 
                                    }else{
                                        me.model.set("bohStore",true);
                                        me.model.set("showBOH",true);
                                        me.render(); 
                                    }
                                });
                            }else{
                                me.model.set("bohStore",false);
                                me.model.set("showBOH",true);
                                me.render(); 
                            }    
                        }
                    }); 
                }

                //Set date for product availability (as of) and pickup date at store (current date + 15)
                me.model.set("stockAvailableAsOF",me.formatDate(today)); 
                var todayDate = new Date();
                //me.model.set("pickUpAtStoreBy",me.formatDate(todayDate.setDate(todayDate.getDate() + 15)));
            	//IWM-222 Remove get it by messaging on PDP & NRTI Check Nearby Store Locator
                var torontoTimeZone = timeZoneSupport.findTimeZone('America/Toronto');
                var promoItemZonedTime = timeZoneSupport.getZonedTime(new Date(me.model.get("updateDate")), torontoTimeZone);
                var promoItemDate = new Date(promoItemZonedTime.year, promoItemZonedTime.month-1, promoItemZonedTime.day, promoItemZonedTime.hours, promoItemZonedTime.minutes, promoItemZonedTime.seconds, promoItemZonedTime.milliseconds);
                if(today <= promoItemDate){
                    me.model.set("isPromoEnabled",true);
                    me.model.set("promoTillDate",me.formatDate(promoItemDate));
                }
            }else{
                me.model.set("showBOH",true);
            }
            var screenWidth = window.matchMedia("(max-width: 767px)");
            var createDate = me.model.get("createDate");
            var newTagDate = moment(createDate).add(30,'d');
            if(moment(createDate) <= moment(today) && moment(today) <= moment(newTagDate)){
                me.model.set("newTagPromotion",true);
            }
            var onlineOnlyProducts = Hypr.getThemeSetting('productCodes');
            var onlineOnlyProductsList = onlineOnlyProducts.split(",");
            var onlineOnly =  _.find(onlineOnlyProductsList, function(product){ return product === me.model.get("productCode"); });
            var promoStartsAt = new Date(Hypr.getThemeSetting("onlineOnlyMessageStartDate"));
            var promoEndsAt = new Date(Hypr.getThemeSetting("onlineOnlyMessageEndDate"));

            if((onlineOnly) && (promoStartsAt <= today && today <= promoEndsAt)){
                me.model.set("isOnlineOnlyProduct",true);
            }
            if(screenWidth.matches){
                me.model.set("isMobileView",true);
            }
        },
        callStore: function(e){
            var screenWidth = window.matchMedia("(max-width: 767px)");
            if(!screenWidth.matches){
                e.preventDefault();
            }
        },
        checkNearbyStores: function(e){
            e.preventDefault();
            $('#check-nearby-store-modal').modal().show();
            $("#check-nearby-loading").show();
            var locationList = Hypr.getThemeSetting("storeDocumentListFQN");
            var self = this,
            locationInventory = null;
            var productCode = self.model.get('productCode');
            var isEcomEnabled = _.findWhere(self.model.attributes.properties, {'attributeFQN':Hypr.getThemeSetting('soldItemAttr')});
            if(!isMapboxEnabled){
                var mapboxGlCSS = document.createElement('link');
                mapboxGlCSS.setAttribute( 'href', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.44.2/mapbox-gl.css');
                mapboxGlCSS.setAttribute( 'rel', 'stylesheet');
                mapboxGlCSS.setAttribute( 'type', 'text/css');
                document.head.appendChild( mapboxGlCSS );
                
                var unwiredGlCSS = document.createElement('link');
                unwiredGlCSS.setAttribute( 'href', 'https://tiles.unwiredmaps.com/v2/js/unwired-gl.css?v=0.1.6');
                unwiredGlCSS.setAttribute( 'rel', 'stylesheet');
                unwiredGlCSS.setAttribute( 'type', 'text/css');
                document.head.appendChild( unwiredGlCSS );
                isMapboxEnabled = true;
            }  

            api.request('GET', '/api/content/documentlists/' + locationList + '/views/default/documents?filter='+filtersString+'&pageSize=1100', {
                cache: false
            }).then(function (response) {
                var curStore = $.parseJSON(localStorage.getItem('preferredStore'));
                var range = Hypr.getThemeSetting("storeDistanceInkm");
                var nearStoresList = [];
                if (response && response.items.length > 0) {
                    nearStoresList = findNearStores(response.items, curStore.geo, range);
                    var prefStore = _.findWhere(nearStoresList, {
                        name: curStore.code
                    });
                    if (prefStore) {
                        nearStoresList = _.without(nearStoresList, _.findWhere(nearStoresList, {
                            name: curStore.code
                        }));
                        nearStoresList.unshift(prefStore);
                    }

                    nearStoresList = nearStoresList.slice(0, 49);
                    _.each(nearStoresList, function (store) {
                        setStoreStatus(store, days[today.getDay()]);
                    });

                    var locationCodes = "";

                    _.each(nearStoresList,function(item,index){
                        if(index != nearStoresList.length-1){
                            locationCodes += item.name+",";
                          }else{
                            locationCodes += item.name;
                        }
                    });
                    
                    api.request('GET','/api/commerce/catalog/storefront/products/'+productCode+'/locationinventory?pageSize=1100&locationCodes='+locationCodes).then(function(response){
                        locationInventory = response.items;
                        var currentDate = new Date();
                        _.each(nearStoresList,function(item){
                            var storeInventory = _.findWhere(locationInventory, {
                                locationCode: item.name
                            });
                            if(storeInventory){
                                item.locationInventory = storeInventory;
                                //item.getItBy = me.formatDate(new Date().setDate(currentDate.getDate() + 15));
                            	//IWM-222 Remove get it by messaging on PDP & NRTI Check Nearby Store Locator
                                if(isEcomEnabled){
                                    item.itemEcomEnabled = true; 
                                }
                            }else{
                            	//item.getItBy = me.formatDate(new Date().setDate(currentDate.getDate() + 15));
                            	//IWM-222 Remove get it by messaging on PDP & NRTI Check Nearby Store Locator
                                if(isEcomEnabled){
                                    item.itemEcomEnabled = true;
                                }
                            }
                        });

                        if (ua.indexOf('chrome') > -1) {
                            if (/edg|edge/i.test(ua)){
                                $.cookie("quickViewProduct",JSON.stringify({'productCode':productCode, 'isEcomEnabled': isEcomEnabled ? true : false}),{path:'/'});
                            }else{
                                document.cookie = "quickViewProduct="+JSON.stringify({'productCode':productCode, 'isEcomEnabled': isEcomEnabled ? true : false})+";path=/;Secure;SameSite=None";
                            }
                        } else {
                            $.cookie("quickViewProduct",JSON.stringify({'productCode':productCode, 'isEcomEnabled': isEcomEnabled ? true : false}),{path:'/'});
                        }
                        
                        if($.cookie('fullAssortmentProduct')){
                            document.cookie = "fullAssortmentProduct="+ false +";Secure;SameSite=None";
                        }else{
							document.cookie = "fullAssortmentProduct="+ false +";Secure;SameSite=None";
						}

                        var storeData = {
                            "stores": nearStoresList
                        };
                        LoadResources.load("/scripts/unwired-gl.js", function(){
                            var checkNearbyStoreView = new CheckNearbyStoreModal.CheckNearbyStore({
                                el: $('#check-nearby-store-modal'),
                                model: new Backbone.Model(storeData)
                            });
                            checkNearbyStoreView.render();
                        });
                    });

                }
            }, function (error) {
                console.error("API Error: ", error);
            });
        },
        formatDate: function(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }
    });

    var ProductModalView = Backbone.MozuView.extend({
        templateName: 'modules/product/product-modal',
        render: function () {
        	var me = this;
            var contextSiteId = apiContext.headers["x-vol-site"];
            var frSiteId = Hypr.getThemeSetting("frSiteId");
            if(frSiteId === contextSiteId){
                me.model.set("isFrenchSite", true);   
            }
            Backbone.MozuView.prototype.render.apply(this);
        },
        initialize: function () {
            var me = this;
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            locale = locale.split('-')[0];
            var currentLocale = '';
	            if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
				      currentLocale = locale === 'fr' ? '/fr' : '/en';
			}
            me.model.set("currentLocale", locale);
            me.model.set("currentSite", currentSite);
        }
    });

    var parentCategoryId;
    var ProductDetailView = Backbone.MozuView.extend({
        templateName: 'modules/product/product-header-detail',
        initialize: function () {
        	var me = this;
        	var allProductCategories = [];
    		parentCategoryId = Hypr.getThemeSetting('newLBMAndHardlinesCategoryId');
        	if(window.location.href.indexOf('ccode') > 0) {
        		var categoryCode = window.location.href.split('#ccode=')[1];
        		if(categoryCode === 'clearance'){
        			me.model.set('isCategoryCodePresent', false);
        			setTimeout(function(){
	        			me.render();
	                    $('.pdp-breadcrumbs').removeClass('hidden');
	                    me.pushDatalayer(me);
        			},3000);
        		}else {
                    var locale = require.mozuData('apicontext').headers['x-vol-locale'];
                    me.model.set('isCategoryCodePresent', true);
                    me.model.set('locale', locale);
                    me.model.set('siteId', contextSiteId);
	            	var categoryFromCurrentProduct = _.findWhere(me.model.get('categories'),{'categoryCode':categoryCode});
                    if(categoryFromCurrentProduct && (categoryFromCurrentProduct.parentCategoryId == Hypr.getThemeSetting('dynamicCategoryId') || categoryFromCurrentProduct.parentCategoryId == Hypr.getThemeSetting('brandsCategoryId'))){
                        me.model.set('isCategoryCodePresent', false);
                        setTimeout(function(){
    	        			me.render();
    	                    $('.pdp-breadcrumbs').removeClass('hidden');
    	                    me.pushDatalayer(me);
            			},3000);
                    }else{
                        if(!categoryFromCurrentProduct) {
                            var filterQuery = 'categoryCode eq ' + categoryCode;
                            api.get('categories',{filter: filterQuery}).then(function(category){
                            	breadCrumbCategory.push({'categoryCode':category.data.items[0].categoryCode, 'categoryName': category.data.items[0].content.name});
                            	me.getCategoryForBreadcrumb(category.data.items[0].parentCategory.categoryId);
                            });
                        }else {
                            breadCrumbCategory.push({'categoryCode':categoryFromCurrentProduct.categoryCode, 'categoryName': categoryFromCurrentProduct.content.name});
                            if(parentCategoryId !== categoryFromCurrentProduct.parentCategoryId){
                                me.getCategoryForBreadcrumb(categoryFromCurrentProduct.parentCategoryId);
                            }else {
                                me.model.set('breadcrumbs', breadCrumbCategory);
                                me.render();
                                $('.pdp-breadcrumbs').removeClass('hidden');
                                me.pushDatalayer(me);
                            }
                        }
                    }
        		}
            }else {
                $('.pdp-breadcrumbs').removeClass('hidden');
                me.pushDatalayer(me);
            }
            setTimeout(function(){
                if(me.model.attributes.price.attributes.onSale){
                    $(".product-badge").removeClass("hidden");
                }
            },3000);
        },
        pushDatalayer:function(me){
            var breadcrumbs = $(".mz-breadcrumb-link:not('.is-first')");
            var breadcrumbstring = '';
            var GTMListParam = '';
            if (window.location.search.indexOf('page') > 0){
                GTMListParam = decodeURIComponent(window.location.search.split('page=')[1]);
            }
            var currAttribute = _.findWhere(me.model.attributes.properties, {'attributeFQN':Hypr.getThemeSetting('brandDesc')});
            var j;
            if(window.location.href.indexOf('ccode') > 0) {
                if(window.location.href.split('#ccode=')[1] == "clearance"){
                    var getClearanceCategory = _.findWhere(me.model.get("categories"), {'categoryCode' : Hypr.getThemeSetting('clearanceCategoryCode') });
                    breadcrumbstring = getClearanceCategory.content.name;
                }else{
                    var categoryCode = window.location.href.split('#ccode=')[1];
                    var getcategory = _.findWhere(me.model.get("categories"), {'categoryCode' : categoryCode });
                    if(getcategory && getcategory.parentCategoryId == Hypr.getThemeSetting("dynamicCategoryId")){
                        breadcrumbstring = getcategory.content.name;
                    }
                    else{
                        for(j=0; j<breadcrumbs.length; j++) {
                            if(j !== breadcrumbs.length-1) {
                                breadcrumbstring += $.trim(breadcrumbs[j].text) + '/'; 
                            }else {
                                breadcrumbstring += $.trim(breadcrumbs[j].text);
                            }
                        }
                    }
                }
            }
            else{
                for(j=0; j<breadcrumbs.length; j++) {
                    if(j !== breadcrumbs.length-1) {
                        breadcrumbstring += $.trim(breadcrumbs[j].text) + '/'; 
                    }else {
                        breadcrumbstring += $.trim(breadcrumbs[j].text);
                    }
                }
            }
            if(GTMListParam) {
            	if(GTMListParam.indexOf('&rrec=true') !== -1){
            		GTMListParam = GTMListParam.split('&rrec=true')[0];
            	}
            }

            if(me.model.get('productType') != 'Articles'){
                dataLayer.push({
                    'event': 'detailview',
                    'ecommerce': {
                    'detail': {
                        'actionField': {'list': GTMListParam},
                        'products': [{
                            'name': me.model.get('content').get('productName'),
                            'id': me.model.get('productCode'),
                            'price':me.model.get('price').get('salePrice') > 0 ? parseFloat(me.model.get('price').get('salePrice')).toFixed(2).replace('$', '').replace(',', '.').trim() : parseFloat(me.model.get('price').get('price')).toFixed(2).replace('$', '').replace(',', '.').trim(),
                            'brand': currAttribute ? currAttribute.values[0].stringValue : '',
                            'category': breadcrumbstring
                        }] 
                    }
                    }
                });
            }
        },
        getCategoryForBreadcrumb: function(categoryId) {
        	var me = this;
        	
        	api.request('GET','/api/commerce/catalog/storefront/categories/'+categoryId).then(function(parentCategory){
        		breadCrumbCategory.push({'categoryCode':parentCategory.categoryCode, 'categoryName':parentCategory.content.name});
        		if(parentCategoryId !== parentCategory.parentCategory.categoryId){
        			me.getCategoryForBreadcrumb(parentCategory.parentCategory.categoryId);
        		}
        		var newBreadCrumbCategory = [];
        		for(var i = breadCrumbCategory.length - 1; i >= 0; i-- ) {
        			newBreadCrumbCategory.push(breadCrumbCategory[i]);
        		}
                me.model.set('breadcrumbs', newBreadCrumbCategory);
                me.render();
                if(parentCategoryId == parentCategory.parentCategory.categoryId){
                	$('.pdp-breadcrumbs').removeClass('hidden');
                	me.pushDatalayer(me);
                }
        	});
        }
    });
    
    var configureCart = function(product, cartitem) {
    	api.get('cart',{}).then(function(response){
        	var cartTotalAmount = response.data.total;
        	var cartTotalCount = 0;
        	_.each(response.data.items, function(product){
        		if(product.product.productCode != 'EHF101') {
        			cartTotalCount = cartTotalCount + product.quantity;
        		}
        	});
        	cartitem = new Backbone.Model(cartitem);
        	cartitem.set('cartTotalAmount', cartTotalAmount);
        	cartitem.set('cartTotalCount', cartTotalCount);
        	cartitem.set('preferredStore', product.get('preferredStore'));
        	cartitem.set('quantity',product.get('quantity'));
        	var productModalView = new ProductModalView({
                el: $('#product-modal-container'),
                model: cartitem
            });
            productModalView.render();
            $('.view-cart-link').parent().removeClass('is-loading');
        });
    };
    
    var ProductDetailMobileView = Backbone.MozuView.extend({
        templateName: 'modules/product/product-header-detail-mobile',
        initialize: function () {
            var me = this;
            if(localStorage.getItem('preferredStore')){
                var currentStore = $.parseJSON(localStorage.getItem('preferredStore')); 
                var attribute = _.findWhere(currentStore.attributes, {"fullyQualifiedName":Hypr.getThemeSetting("isEcomStore")});   
            	var productCode;
            	if(me.model.get('variations') && me.model.get('variations').length > 0) {
            		//from current variation find the variationProductCode
            		var productOptionValue = $('.mz-productoptions-option').val();
            		var variation = _.find(me.model.get('variations'), function(variation) {
            			return _.findWhere(variation.options, {'value': productOptionValue});
            		});
            		productCode = variation.productCode;
            	}else {
            		productCode = me.model.get('productCode');
            	}
            	var currentStoresWarehouse = _.find(currentStore.attributes, function(attribute){
            		return attribute.fullyQualifiedName === Hypr.getThemeSetting('warehouseAttr');
                });

            	if(currentStoresWarehouse) {
            		var inventoryApi = '/api/commerce/catalog/storefront/products/' + productCode + '/locationinventory?locationCodes=' + currentStoresWarehouse.values[0];
                	api.request("GET", inventoryApi).then(function(response){
                		if(response && response.items.length > 0){
                			if(response.items[0].stockAvailable > 0){
                				me.model.set('isInventoryAvail', true);
                                if(response.items[0].stockAvailable == 1 || response.items[0].stockAvailable == 2 || response.items[0].stockAvailable == 3){
                                    me.model.set("isLimitedQuantity",true); 
                                }
                                me.render();
                            }
                            else{
                                $(".mobile-show-sale").removeClass("hidden");
                                me.render();
                            }
                		}
                	});
                }
                
            }
            var createDate = me.model.get("createDate");
            var newTagDate = moment(createDate).add(30,'d');
            if(moment(createDate) <= moment(today) && moment(today) <= moment(newTagDate)){
                me.model.set("newTagPromotion",true);
            }
        }
    });
    
    var certonaTitle = '';
    var CertonaRecommendationsViewAddToCart = Backbone.MozuView.extend({
		templateName: 'Widgets/cms/certona-recommended-products',
		initialize: function() {
			var me = this;
			var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
        	me.model.set("currentLocale", locale);
			me.model.set("currentSite", currentSite);
			setTimeout(function(){
				var currentSceme;
				console.log("********Certona Response***********");
				console.log(document.certonaHome);
				var queryStringCertona = "", sorting = [];
				var currentItems = _.find(document.certonaHome.resonance.schemes, function(object){
					return object.scheme === 'addtocart1_rr';
				});
				if(currentItems){
					me.model.set('promoName', currentItems.explanation);
					currentItems.items.forEach(function(item,index){
		              if(index != currentItems.items.length-1){
		            	  queryStringCertona += "productCode eq "+item.ID+" or ";
		              }else{
		            	  queryStringCertona += "productCode eq "+item.ID;
		              }
		              sorting.push(item.ID);
		            });
				}
				if(queryStringCertona) {
					api.get("search",{filter: queryStringCertona}).then(function(certonaResponse){
						var newData = [];
						if(certonaResponse && certonaResponse.data) {
							var newSortingArray= sorting.filter(function(item) {
							    return certonaResponse.data.items.some(function(certonaItem) { return item === certonaItem.productCode; });
							});
							_.each(certonaResponse.data.items, function(item){
								newData[newSortingArray.indexOf(item.productCode)] = item;
							});
							me.model.set('items', newData);
							me.model.set('currentSceme', 'addtocart1_rr'); 
							me.model.set("isWidget",true);
							me.model.set("isCertonaProduct", true);
							me.setElement($(".certona-regular-addtocart1_rr"));
							$('.content-loading').addClass('hidden'); 
							me.render();
							if(certonaResponse.data.items.length > 0){
								var explaination="";
								var selectedScheme = _.find(document.certonaHome.resonance.schemes, function(scheme){
									return scheme.scheme === 'addtocart1_rr';
								});
								if(selectedScheme) {explaination = selectedScheme.explanation;}
								$(".certona-title-addtocart1_rr").text(explaination);
								
								var certonaInnerHtml = document.getElementById("product-certona-container");
				                var innerHtml = certonaInnerHtml.innerHTML;
				                certonaTitle = $("#product-certona-container").find('.certona-title').text();
				                $("#product-certona-container").find(".certona-regular-addtocart1_rr").addClass('oldCertonaAddToCartData').removeClass("certona-regular-addtocart1_rr").removeClass('slick-initialized').removeClass('slick-slider');
				                $("#product-certona-container").find('.certona-title').addClass('hidden');
				                $('.certona-innerHTML-container').append(innerHtml);
				                $('.certona-innerHTML-container').find('div[id^=mz-drop-zone-certona]').removeClass('hidden');
				                $('.certona-innerHTML-container').find('.certona-title').text(certonaTitle);
				            	$(".certona-regular-addtocart1_rr").removeClass('hidden').removeClass('slick-initialized').removeClass('slick-slider'); 
				            	$('.certona-innerHTML-container').find('.certona-title').removeClass('hidden');
				            	var productImpressions = $('#product-certona-container').find('.ign-data-product-impression');
				            	window.sendGtmDataLayer(productImpressions, true);
				            	$(".certona-regular-addtocart1_rr").not('.slick-initialized').slick({ 
					                 dots: false,
					                 infinite: false,
					                 slidesToShow: 4,
					                 slidesToScroll: 4,
					                 variableWidth:true,
					                 responsive: [
					                  {
					                    breakpoint: 1024,
					                    settings: {
					                      slidesToShow: 4,
					                      slidesToScroll: 4
					                    }
					                  },
					                  {
					                    breakpoint: 600,
					                    settings: {
					                      infinite: true,
					                      arrows: false,
					                      slidesToShow: 2,
					                      slidesToScroll: 1
					                    }
					                  }
					                ]
					            });//Slider End 
				            	$("#product-certona-container").find('.oldCertonaAddToCartData').addClass("certona-regular-addtocart1_rr").addClass('slick-initialized').addClass('slick-slider');
							}
						}
					});
				}
				
			}, 2000);
		}
	});
    
    
    var LoadResources = {
        load: function(src, callback) {
          var script = document.createElement('script'),
              loaded;
          script.setAttribute('src', src);
          if (callback) {
            script.onreadystatechange = script.onload = function() {
              if (!loaded) {
                callback();
              }
              loaded = true;
            };
          }
          document.getElementsByTagName('head')[0].appendChild(script);
        }
    };

    /*This function finds stores near preferred store in 50km range by default*/
    function findNearStores(storeList, location, range) {
        var nearStores = [];
        _.each(storeList, function (store) {
            var distance = getDistanceFromLatLongInKm(store.properties.latitude, store.properties.longitude, location.lat, location.lng);
            if (distance <= range) {
                var storeData = {
                    distance: parseFloat(distance.toFixed(3)),
                    modelData: store
                };
                nearStores.push(storeData);
            }
        });
        nearStores = _.sortBy(nearStores, "distance");
        var resultArray = [];
        _.each(nearStores, function (store) {
            resultArray.push(store.modelData);
        });
        return resultArray;
    }
    /*This function calculates and return the distance between preferred store and input store*/
    function getDistanceFromLatLongInKm(lat1, lng1, lat2, lng2) {
        var earthRadius = 6371; // Radius of the earth in km
        var dLat = deg2rad(lat2 - lat1); // deg2rad below
        var dLng = deg2rad(lng2 - lng1);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var distance = earthRadius * c * 0.62137119; // Distance in Mi
        return distance;
    }
    /*this funtion converts deg to rad*/
    function deg2rad(deg) {
        return deg * (Math.PI / 180);
    }

    /*this function set working hours status */
    function setStoreStatus(store, day) {
        store.storeStatus = getStoreStatus(store, day);
    }

    /*this function calculates and returns status object with working hours*/
    function getStoreStatus(store, day) {
        var workingHours = "",
            statusData;
        var start = new Date(),
            end = new Date();
        switch (day) {

            case "Sunday":
                workingHours = store.properties.sundayHours;
                break;
            case "Monday":
                workingHours = store.properties.mondayHours;
                break;
            case "Tuesday":
                workingHours = store.properties.tuesdayHours;
                break;
            case "Wednesday":
                workingHours = store.properties.wednesdayHours;
                break;
            case "Thursday":
                workingHours = store.properties.thursdayHours;
                break;
            case "Friday":
                workingHours = store.properties.fridayHours;
                break;
            case "Saturday":
                workingHours = store.properties.saturdayHours;
                break;
        }
        var currentTime = moment(),
            startTime, endTime;
        if (workingHours !== "Closed") {
            var workHours = workingHours.split("-");
            startTime = moment(workHours[0], "HH:mm a");
            endTime = moment(workHours[1], "HH:mm a");
        }
        if (currentTime.isBetween(startTime, endTime)) {
            statusData = {
                "status": "open",
                "hours": workingHours
            };
        } else {
            statusData = {
                "status": "closed",
                "hours": workingHours
            };
        }
        return statusData;
    }

    $(document).ready(function () {
        var product = ProductModels.Product.fromCurrent();
        var currAttribute = _.findWhere(product.attributes.properties, {'attributeFQN':Hypr.getThemeSetting('brandDesc')});
        var breadcrumbstring = '';
        product.on('addedtocart', function (cartitem) {
            if (cartitem && cartitem.prop('id')) {
            	var sorting = [];
                product.isLoading(false);
    			var certonaRecommendationsViewAddToCart = new CertonaRecommendationsViewAddToCart({
    		        model: new Backbone.Model()
    		    });
                CartMonitor.addToCount(product.get('quantity'));
                $('#product-modal-container').modal().show();
                configureCart(product, cartitem); 
                var breadcrumbs = $(".mz-breadcrumb-link:not('.is-first')");
                var i;
                if(window.location.href.indexOf('ccode') > 0) {
                    if(window.location.href.split('#ccode=')[1] == "clearance"){
                        var getClearanceCategory = _.findWhere(product.get("categories"), {'categoryCode' : Hypr.getThemeSetting('clearanceCategoryCode') });
                        breadcrumbstring = getClearanceCategory.content.name;
                    }else{
                        var categoryCode = window.location.href.split('#ccode=')[1];
                        var getcategory = _.findWhere(product.get("categories"), {'categoryCode' : categoryCode });
                        if(getcategory.parentCategoryId == Hypr.getThemeSetting("dynamicCategoryId")){
                            breadcrumbstring = getcategory.content.name;
                        }
                        else{
                            for(i=0; i<breadcrumbs.length; i++) {
                                if(i !== breadcrumbs.length-1) {
                                    breadcrumbstring += $.trim(breadcrumbs[i].text) + '/'; 
                                }else {
                                    breadcrumbstring += $.trim(breadcrumbs[i].text);
                                }
                            }
                        }
                    }
                }
                else{
                    for(i=0; i<breadcrumbs.length; i++) {
                        if(i !== breadcrumbs.length-1) {
                            breadcrumbstring += $.trim(breadcrumbs[i].text) + '/'; 
                        }else {
                            breadcrumbstring += $.trim(breadcrumbs[i].text);
                        }
                    }
                }    

                var crumbFlowArray; 
                var data={
                   "productCode":product.get('productCode'),
                   "flow":breadcrumbstring
                };
                var expiryDate = new Date();
                expiryDate.setYear(expiryDate.getFullYear() + 1);
                
                if($.cookie('BreadCrumbFlow')){
                    crumbFlowArray = $.parseJSON($.cookie("BreadCrumbFlow"));
                    var isavailable = _.findWhere(crumbFlowArray, {'productCode' : data.productCode});
                            if(!isavailable){
                            	if(data.flow){
                            		crumbFlowArray.push(data);
                            	}
                            }else{
                                _.find(crumbFlowArray, function(prod, index) {
                                    if(prod.productCode == data.productCode) {
                                        crumbFlowArray[index] = data;
                                    }
                                });
                            } 
                            if (ua.indexOf('chrome') > -1) {
                                if (/edg|edge/i.test(ua)){
                                    $.cookie('BreadCrumbFlow', JSON.stringify(crumbFlowArray),{path: '/',expires: expiryDate});
                                }else{
                                    document.cookie = "BreadCrumbFlow="+JSON.stringify(crumbFlowArray)+";expires="+expiryDate+";path=/;Secure;SameSite=None";
                                }
                            } else {
                                $.cookie('BreadCrumbFlow', JSON.stringify(crumbFlowArray),{path: '/',expires: expiryDate});
                            }
                }else{
                	if(data.flow){
	                    crumbFlowArray = [];
	                    crumbFlowArray.push(data);
	                    if (ua.indexOf('chrome') > -1) {
                            if (/edg|edge/i.test(ua)){
                                $.cookie('BreadCrumbFlow', JSON.stringify(crumbFlowArray),{path: '/',expires: expiryDate});
                            }else{
                                document.cookie = "BreadCrumbFlow="+JSON.stringify(crumbFlowArray)+";expires="+expiryDate+";path=/;Secure;SameSite=None";
                            }
                        } else {
                            $.cookie('BreadCrumbFlow', JSON.stringify(crumbFlowArray),{path: '/',expires: expiryDate});
                        }
                    }
                    
                }

                api.get('cart',{}).then(function(cartdata){
                    console.log(cartdata.data.subtotal);
                    var productPrice = product.get('price').get('salePrice') ? product.get('price').get('salePrice') : product.get('price').get('price');
                    var saleforceData = [];
                    _.each(cartdata.data.items,function(item){
                      saleforceData.push({
                        'id': item.product.productCode, // String. Any unique identifier for a product/SKU
                        'price': item.product.price.salePrice > 0 ? parseFloat(item.product.price.salePrice).toFixed(2).replace('$', '').replace(',', '.').trim() : parseFloat(item.product.price.price).toFixed(2).replace('$', '').replace(',', '.').trim(),
                        'quantity': item.quantity
                      });
                    });
                    dataLayer.push({
                        'event': 'addToCart',
                        'metric4':cartdata.data.total, // String/Currency. This metric tracks total dollar value  that is in the cart (for all products) after this product(s) addition 
                        'ecommerce': {
                          'currencyCode': 'CAD',
                          'add': {
                            'products': [{                            
                            	'name': product.get('content').get("productName"),
                            	'id': product.get('productCode'),
                            	'price': productPrice.toFixed(2).replace('$', '').replace(',', '.').trim(),
                            	'brand': currAttribute ? currAttribute.values[0].stringValue : '',
                            	'category': breadcrumbstring, 
                            	'quantity': parseInt(product.get("quantity"),10) 
                             }]
                          	}
                          },
                          'salesForce': {
                        	  'products': saleforceData,
                              'cartRecoveryId': cartdata.data.id
	                      }
                      });
                });
            } else {
                product.trigger("error", { message: Hypr.getLabel('unexpectedError') });
            }

        });
        product.on('addedtowishlist', function (cartitem) {
            $('#add-to-wishlist').prop('disabled', 'disabled').text(Hypr.getLabel('savedToList'));
            
            /*Update Wishlist Count*/
            api.get('wishlist',{}).then(function(response){        	
            	var items = response.data.items[0].items;
                var totalQuantity = 0;
                _.each(items, function(item, index) {
                	totalQuantity += item.quantity;
                });
                $('#wishlist-count').html('('+totalQuantity+')');
            });
        });
        
        /*click listener on video thumbnail*/
        $(document).on("click touchstart","#main-video-thumb",function(e){
            if($("#videoContainer").css("display") == "none"){
                $("#videoContainer").toggleClass("show-video");
                $("#imageContainer").hide();
                $(".mz-productimages-thumbimage").removeClass("is-selected");
                $(".ign-product-videothumb").addClass("is-selected");    
            }

            if ($(".ign-product-videothumb").hasClass("is-selected")) {
                $(".flag-container").addClass("hidden");
            }
            var videoSrc = e.currentTarget.getAttribute("src");
            var srcArray = videoSrc.split('/');
            
            $(".product-main-video").attr("id", "vzvd-"+ srcArray[3]);
            $(".product-main-video").attr("name", "vzvd-"+ srcArray[3]);
            $(".product-main-video").attr("src", "https://view.vzaar.com/" + srcArray[3] + "/player"); 
            
        });
        $("span[data-mz-productimage-thumb]").on("touchstart",function(e){ 
            if($("#videoContainer").hasClass("show-video")){
                $(".flag-container").removeClass("hidden");
            }
        });

        /*Specification toggle functionality */
        $(".toggle-btn").on("click",function(e){
        	e.preventDefault();
        	$(this).closest(".toggle-container").toggleClass("is-collapsed");
        	$(this).closest(".collapse-section").toggleClass("is-collapsed");
        	if($(this).closest(".toggle-container").hasClass("is-collapsed")){
                $(this).text(Hypr.getLabel('less'));
                $(this).attr({"title":Hypr.getLabel('less')});
        	}else{
                $(this).text(Hypr.getLabel('more'));
                $(this).attr({"title":Hypr.getLabel('more')});
        	}
        });
        
        /*View all related products functionality*/
        $('.related-list-toggle').on('click', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();

            var $toggle = $(e.target).closest('.related-list-toggle');
            var $parent = $toggle.closest('.related-products-link');
            var expanded = $parent.attr('aria-expanded') === 'true';
            expanded = ! expanded;
            
            $toggle.attr('aria-label', $toggle.attr(expanded ? 'data-label-less' : 'data-label-more'));
            $parent.attr('aria-expanded', expanded);
        });
        
        var productDetailView = new ProductDetailView({ 
            el: $('#product-header-detail'),
            model: product
        });

        
    
        var productView = new ProductView({
            el: $('#product-detail'),
            model: product,
            messagesEl: $('[data-mz-message-bar]')
        });
        
        var mediaWidth = window.matchMedia("(max-width: 767px)");
        if(mediaWidth.matches){
            var productDetailMobileView = new ProductDetailMobileView({
                el: $('#product-mobile-header-detail'),
                model: product
            });
            productDetailMobileView.render();
        }
        window.productView = productView;        
        $.browser = {};
        (function () {
            $.browser.msie = false;
            $.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                $.browser.msie = true;
                $.browser.version = RegExp.$1;
            }
        })();
        productView.render();	
        
        var addToWishlist  = window.location.hash;
        if(addToWishlist == "#addToWishlist") {
        	productView.addToWishlist();
        }
        
        var allCategoriesPDP = window.allCategories = [],parentCategoryId,currentCategoryFlow, productImpressionArray = [], productImpression = {};
        var AllProductsImpressions = [];
        function getParentCategory(parentCategoryId, currentCategoryId, allCategoriesPDP, impression){
            var parentCategoryData = [];
          _.find(allCategoriesPDP,function(val){
            if(val.categoryId === currentCategoryId){
      	    	var parentCategoryName = val.content.name;
      	    	currentCategoryFlow = parentCategoryName + '/' + currentCategoryFlow;
      	    	if(parentCategoryId != val.parentCategory.categoryId){
      	    		//if current category is not NEW LBM And Hardlines then do the same process for current category
      	    		getParentCategory(parentCategoryId, val.parentCategory.categoryId, allCategoriesPDP, impression);
      	    	}else{
      	    		//if current category is NEW LBM And Hardlines then do stop the process and set the ccategory flow in model
	                  	var price = $(impression).find('.mz-price.is-saleprice').length>0 ? $(impression).find('.mz-price.is-saleprice').text() : $(impression).find('.mz-price').text();
	                  	if(price && price.indexOf('/') > 0) {
                      	  price = price.split('/')[0];
                        }
	                  	var listAttr = '';
                        if($(impression).find('.mz-productlisting-title') && $(impression).find('.mz-productlisting-title').length>0) {
                      	  listAttr = $(impression).find('.mz-productlisting-title').attr('href').split('?page=')[1];
                        }else if($(impression).find('.product-title') && $(impression).find('.product-title').length>0) { //for related products
                      	  listAttr = $(impression).find('.product-title').children().attr('href').split('?page=')[1];
                        }else if($(impression).find('.productTile-title-link') && $(impression).find('.productTile-title-link').length>0){ //for image slider widget
                          	listAttr = $(impression).find('.productTile-title-link').attr('href').split('?page=')[1];
                        }
                        if(listAttr) {
                        	if(listAttr.indexOf('&rrec=true') !== -1){
                        		listAttr = listAttr.split('&rrec=true')[0];
                        	}
                        }
	                  	productImpression = {
		                  	'name': $(impression).attr('data-mz-productname'),
		                  	'id' : $(impression).attr('data-mz-product'),
		                  	'price' : $.trim(price.replace('$', '').replace(',', '.')),
		                  	'brand' : $.trim($(impression).find('.brand-name').text()),
		                  	'category' : currentCategoryFlow,
		                  	'position' : $(impression).attr('data-mz-position'),
		                  	'list' : listAttr
	                    };
	                    productImpressionArray.push(productImpression);
      	    	}
            }
          });
         }
        
        
        setTimeout(function(){
        	var productImpressions = [];
			productImpressions = createImpressionArray();
			if(productImpressions.length > 0){
				if(sessionStorage.getItem('allCategories')){
	        		allCategoriesPDP = window.allCategories = JSON.parse(sessionStorage.getItem('allCategories'));
					sendGTMDatalayer(productImpressions);
	        	}else{
	        		var date = new Date();
	                    api.request('GET', '/api/commerce/catalog/storefront/categories/tree', {
							cache: false
						}).then(function(result1){
	                    if(result1.items.length > 0){
	                        _.each(result1.items[1].childrenCategories, function(children){
	                      	  allCategoriesPDP.push(children);
	                      	 _.each(children.childrenCategories, function(subChildren){
	                            allCategoriesPDP.push(subChildren);
	                      		 _.each(subChildren.childrenCategories, function(subSubChildren){
	                                allCategoriesPDP.push(subSubChildren);
	                      			 _.each(subSubChildren.childrenCategories, function(subSubSubChildren){
	                                    allCategoriesPDP.push(subSubSubChildren);
	                      			 });
	                      		 });
	                      	 });
	                        });

							var pluckedArray = [];
							_.map(allCategoriesPDP, function(category){
								pluckedArray.push(_.pick(category, 'categoryId', 'parentCategory', 'content'));
							});
							window.allCategories = pluckedArray;
							sessionStorage.setItem('allCategories', JSON.stringify(pluckedArray));
	                        sendGTMDatalayer(productImpressions);
	                    }
	            	});
	        	}
			}
        }, 4000);
        function sendGTMDatalayer(productImpressions) {
        	parentCategoryId = Hypr.getThemeSetting('newLBMAndHardlinesCategoryId');
        	_.each(productImpressions, function(impression){
            	  var productCategory;
            	  if($(impression).attr('data-mz-categoryId')){
            		  productCategory = parseInt($(impression).attr('data-mz-categoryId').split('|')[0], 10);
            	  }
            	  _.find(allCategoriesPDP,function(val){
                      if(val.categoryId === productCategory){
                        var currentCategoryName = val.content.name;
                        var currentParentId = parseInt($(productImpressions[0]).attr('data-mz-categoryId').split('|')[1], 10);
                        currentCategoryFlow = currentCategoryName;
                        if(parentCategoryId != currentParentId){
                          //if current category is not NEW LBM And Hardlines then do the same process for current category
                          getParentCategory(parentCategoryId, currentParentId, allCategoriesPDP, impression);
                        }else {
                          var price = $(impression).find('.mz-price.is-saleprice').length>0 ? $(impression).find('.mz-price.is-saleprice').text() : $(impression).find('.mz-price').text();
                          if(price && price.indexOf('/') > 0) {
                            price = price.split('/')[0];
                          }
                          var listAttr = '';
                            if($(impression).find('.mz-productlisting-title') && $(impression).find('.mz-productlisting-title').length>0) {
                          	  listAttr = $(impression).find('.mz-productlisting-title').attr('href').split('?page=')[1];
                            }else if($(impression).find('.product-title') && $(impression).find('.product-title').length>0) { //for related products
                          	  listAttr = $(impression).find('.product-title').children().attr('href').split('?page=')[1];
                            }else if($(impression).find('.productTile-title-link') && $(impression).find('.productTile-title-link').length>0){ //for image slider widget
                                	listAttr = $(impression).find('.productTile-title-link').attr('href').split('?page=')[1];
                            }
                          productImpression = {
                        	'name': $(impression).attr('data-mz-productname'),
                        	'id' : $(impression).attr('data-mz-product'),
                        	'price' : $.trim(price.replace('$', '').replace(',', '.')),
                        	'brand' : $.trim($(impression).find('.brand-name').text()),
                        	'category' : currentCategoryFlow,
                        	'position' : $(impression).attr('data-mz-position'),
                        	'list' : listAttr
                          };
                          productImpressionArray.push(productImpression);
                        }
                      }
                   });
              });
                          
              //send datalayer
              
             // Product impressions can be sent on the pageview event.
              if(productImpressions && productImpressions.length > 0) {
                  dataLayer.push({ 
                    'event': 'impressionview',
                    'ecommerce': {
                      'currencyCode': 'CAD',
                      'impressions': productImpressionArray
                    }
                  });
              }
          
        }
        function createImpressionArray() {
        	var productImpressions = $('.ign-data-product-impression').filter(function(){ 
        		return $(this).closest('.certona-products-container').length === 0; 
        	});
//			create impression without certona products as the separate impression arrey for certona will be created from certona-content.js file
            var imageSliderProductImpression = $('.ign-data-product-impression-image-slider');
            if(imageSliderProductImpression && imageSliderProductImpression.length > 0) {
        	  _.each($('.bannerimg.slick-slide[aria-describedby]'), function(banner) {
        		  _.each($(banner).find('.ign-data-product-impression-image-slider'), function(productOnBanner) {
        			  productImpressions.push(productOnBanner);
        		  });
        	  });
            }
            return productImpressions;
        }
        
        
        if( /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        	$('#morelikethis .certona-container .certona-products-container').addClass("hidden");
        }

        function tabControl() {
            var tabs = $('.product-information').find('.prod-info-tab');
            var attrTitleLength = $('.productdetails-attributes').find('.attribute-title').length;
            var attrValueLength = $('.productdetails-attributes').find('.attribute-value').length;
            var detailsSectionLength = $('#details').find('#prodDetails ul').length;
            var prodIngredientsLength = $('#details').find('.product-ingredients').length;
            var prodDocumentLength = $('#details').find('.product-document').length;
            if(detailsSectionLength === 0 && prodIngredientsLength === 0 && prodDocumentLength === 0){
                $('#detailsTab').addClass('hidden').removeClass('tablist');
                if($(window).width() < 768){
                    $('#details').addClass('hidden');
                }
            }
            if($('#overviewTab').hasClass('hidden') && $('#detailsTab').hasClass('hidden')){
                $("#specification").addClass('active');
                $("#details").removeClass('active');
            }
            if(attrTitleLength === 0 && attrValueLength === 0){
                $('#specificationTab').removeClass('tablist active').addClass('hidden');
                if($(window).width() < 768){
                    $('#specification').addClass('hidden');
                }
            }
            else{
                $('#specificationTab').removeClass('hidden').addClass('tablist');
                if($(window).width() < 768){
                    $('#specification').removeClass('hidden');
                }
            }
            if($("#overviewTab").hasClass('hidden') && $("#detailsTab").hasClass('hidden')){
                $("#specificationTab").addClass('active');
            }
            if($("#overviewTab").hasClass('hidden') && $("#specificationTab").hasClass("hidden") && $("#detailsTab").hasClass('hidden')){
                $("#MoreLikeThisTab").addClass('active');
                if($(window).width() > 767){
                    $("#specification").removeClass('active');
                    $('#morelikethis').addClass('active');
                }
            }
            if(tabs.is(':visible')) {
                tabs.find('.tablist').on('click', function(event) {
                event.preventDefault();
                var me = this;
                var target = $(me).attr('href'),
                    tabs = $(me).parents('.prod-info-tab'),
                    buttons = tabs.find('a'),
                    item = tabs.parents('.product-information').find('.item');
                buttons.removeClass('active');
                item.removeClass('active');
                var getClass = $(event.currentTarget).hasClass('hidden');
                    if(!getClass){
                        $(me).addClass('active');
                        $(target).addClass('active');
                    }
                });
            }
        }
        tabControl();
    });

    $(".morelikethis-title").on("click",function(){
        $('#mz-drop-zone-rti-recommended-product').toggleClass("rti-toggle ");
        $(".morelikethis-title").toggleClass("morelikethis-collapse");
        if($('#morelikethis .certona-container .certona-products-container').hasClass("hidden")){
        	$('#morelikethis .certona-container .certona-products-container').removeClass("hidden");
        }else{
        	$('#morelikethis .certona-container .certona-products-container').addClass("hidden");
        }
    }); 

    $('.popupover').popover();
    $("body").on("click touchstart", '.popupover', function() {
        if($(window).width() < 400){
            $(this).popover("show");
            $('.popupover').not(this).popover("hide"); // hide other popovers
            return false;
        }
    });
    $("body").on("click touchstart", function() {
        if($(window).width() < 400){
            $('.popupover').popover("hide"); // hide all popovers when clicked on body
        }
    });
});
