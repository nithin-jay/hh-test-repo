define(['modules/jquery-mozu',
	"modules/backbone-mozu",
	'underscore',
	'hyprlive'], function($, Backbone, _, Hypr) {
	
	var StoreFlyerView = Backbone.MozuView.extend({
		templateName: 'pages/store-flyer-live',
		additionalEvents:{
			"click #selectFlyerStore":"changeStore"
		},
		changeStore: function(e){
            e.preventDefault();
            var locale = require.mozuData('apicontext').headers['x-vol-locale'];
            var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
            locale = locale.split('-')[0];
            var currentLocale = '';
            if (Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
            	currentLocale = locale === 'fr' ? '/fr' : '/en';
            }
            window.location.href=currentLocale + "/store-locator?returnUrl="+window.location.pathname;
        }
	});
	
	$.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results===null){
           return null;
        }
        else{
           return decodeURI(results[1]) || 0;
        }
    };
    
    
	$(document).ready(function() {
		var locationCode = "";	
		var locationID = "";
		var locationName = "";
		var getStoreType = $.urlParam('storeType');
		var getlocationId = $.urlParam('locationId');
		var currentSite = require.mozuData('apicontext').headers['x-vol-site'];
		var locale = require.mozuData('apicontext').headers['x-vol-locale'],
		accessToken = Hypr.getThemeSetting("accessToken"),
		merchantId = Hypr.getThemeSetting("merchantId"),
		merchantNameIdentifier = Hypr.getThemeSetting("merchantNameIdentifier"),
		HFaccessToken = Hypr.getThemeSetting("HFaccessToken"),
		HFmerchantId = Hypr.getThemeSetting("HFmerchantId"),
		HFmerchantNameIdentifier = Hypr.getThemeSetting("HFmerchantNameIdentifier"),
		preferredStoreCode = localStorage.getItem('preferredStore') ? $.parseJSON(localStorage.getItem('preferredStore')).code : '',
		storeCode,storefront,
		preferredStorePostalCode;
		if(preferredStoreCode){
			storeCode = preferredStoreCode.substring(0, 4) + "-" + preferredStoreCode.substring(4, preferredStoreCode.length);
		}else{
			preferredStorePostalCode = localStorage.getItem('preferredStore') ? $.parseJSON(localStorage.getItem('preferredStore')).address.postalOrZipCode : '';
		}

		locale = locale.split('-')[0];
		if(locale === "en"){
			locale = "en-CA";
		}else if(locale === "fr"){
			locale = "fr-CA";
		}

		var flippContainer = document.querySelector("#storeflyer");
		if(Hypr.getThemeSetting('homeFurnitureSiteId') != currentSite){
			storefront = new Flipp.Storefront({
				accessToken: accessToken,
				merchantId: merchantId,
				merchantNameIdentifier: merchantNameIdentifier,
				locale: locale,
				storeCode: storeCode ? storeCode : preferredStorePostalCode
			});
		} else {
			storefront = new Flipp.Storefront({
				accessToken: HFaccessToken,
				merchantId: HFmerchantId,
				merchantNameIdentifier: HFmerchantNameIdentifier, 
				locale: locale,
				storeCode: storeCode ? storeCode : preferredStorePostalCode
			});
		}	
		storefront.updateStoreCode();
		storefront.setHeaderElement($(".header-container-wrapper"));
		storefront.renderStorefront(flippContainer);
/*jshint ignore:end*/
		
		if(getStoreType && getlocationId){
			locationID = getlocationId.slice(0, 4) + '-' + getlocationId.slice(4);	
			locationName = getStoreType;
		} else {
			if(require.mozuData('pagecontext').purchaseLocation){
				locationID = require.mozuData('pagecontext').purchaseLocation.code;
				locationID = locationID.slice(0, 4) + '-' + locationID.slice(4);		
				locationName = _.findWhere($.parseJSON(localStorage.getItem('preferredStore')).attributes, {fullyQualifiedName: Hypr.getThemeSetting('storetype')});
				locationName = locationName.values[0];
			}
		}		
		
		if(locationName == "Home Hardware") {
			locationCode = "10032";
		}else if (locationName == "Home Building Centre") {
			locationCode = "10698";
		}else if (locationName == "Home Hardware Building Centre") {
			locationCode = "10205";
		}else if (locationName == "Home Furniture") {
			locationCode = "10206";
		}
		
		var reeBeeUrl;
		if(Hypr.getThemeSetting('selectedEnvironment') === 'PROD') {
			reeBeeUrl = Hypr.getThemeSetting('reeBeeUrlPROD');
		} else {
			reeBeeUrl = Hypr.getThemeSetting('reeBeeUrlDEV');
		}
		
		var flyerURL = reeBeeUrl + locationCode + "?clientStoreLocationId=" + locationID + "&embedClientId=6&embedClientCode=f945c21e&lang=" + locale;
		var storeFlyerView;
		if(localStorage.getItem('preferredStore')){
			storeFlyerView = new StoreFlyerView({
				el: $('#storeflyer'),
				model: new Backbone.Model({flyerURL: flyerURL})
			});
		}else{
			storeFlyerView = new StoreFlyerView({
				el: $('#storeflyer'),
				model: new Backbone.Model({noStore : Hypr.getLabel("selectStoreToSeeFlyer")})
			});
		}
		$("#storeflyer").find("#reebee-viewer").attr("data-no-redirect", "true");
		if($("#storeflyer").find(".no-flyer-store").length === 0 && $('#mobile-search #searchbox').hasClass('active')) {
			$("body").animate({ scrollTop: $('.expand-header').height()}, "slow"); 
		}
	});
});