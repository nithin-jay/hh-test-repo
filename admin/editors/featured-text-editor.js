Ext.Loader.setConfig({
    enabled:true
});
var CKEDITOR;
Ext.define('Ext.ux.CKEditor', {
    extend: 'Ext.form.field.TextArea',
    alias: 'widget.ckeditor',

    constructor: function () {
        this.callParent(arguments);
        this.addEvents("instanceReady");
    },

    initComponent: function () {
        var me = this;
        me.callParent(arguments);
                me.on("afterrender", function () {
                    me.setFieldStyle({opacity: "0"});
                    me.setLoading(true);
                    Ext.apply(me.CKConfig, {
                        height: me.getHeight(),
                        width: me.getWidth()
                    });
                    Ext.Loader.loadScript({
                        url: 'https://cdn.ckeditor.com/4.16.1/full-all/ckeditor.js',
                        onLoad: function() {
                            CKEDITOR = this.CKEDITOR;
                            CKEDITOR.config.baseFloatZIndex = 200000;
                            me.editor = CKEDITOR.replace(me.inputEl.id, me.CKConfig);
                            me.editor.name = me.name;
                            if(CKEDITOR.config.font_names.search('Agenda') == -1 && CKEDITOR.config.font_names.search('Agenda_bold')) {
                                var myFonts = [];
                                myFonts = ["Agenda, sans-serif","Agenda_bold, sans-serif"];
                                CKEDITOR.config.font_names = CKEDITOR.config.font_names+';'+myFonts[0]+';'+myFonts[1];
                            }
                            me.editor.on("instanceReady", function (event) {
                                event.editor.on("beforeCommandExec", function(event) {
                                    // Show the paste dialog for the paste buttons and right-click paste
                                    if (event.data.name == "paste") {
                                        event.editor._.forcePasteDialog = true;
                                    }
                                    // Don't show the paste dialog for Ctrl+Shift+V
                                    if (event.data.name == "pastetext" && event.data.commandData.from == "keystrokeHandler") {
                                        event.cancel();
                                    }
                                });
                                me.fireEvent(
                                    "instanceReady", 
                                    me, 
                                    me.editor
                                );
                                me.setLoading(false);
                            }, me); 
                        }
                    });
                }, me);
    },

    onRender: function (ct, position) {
        if (!this.el) {
            this.defaultAutoCreate = {
                tag: 'textarea',
                autocomplete: 'off'
            };
        }
        this.callParent(arguments)
    },
    
    setValue: function (value) {
        this.callParent(arguments);
        if (this.editor) {
            this.editor.setData(value);
        }
    },

    getValue: function () {
        if (this.editor) {
            return this.editor.getData();
        } 
        else {
            return ''
        }
    }
});
    Ext.widget({
        xtype: 'mz-form-widget',
        items: [
            {
                xtype: 'container',
                items: [
                        {
                            xtype: 'panel',
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'ckeditor',
                                    name: 'textEditor',
                                    width: 750,
                                    height: 400, 
                                    style: {
                                        marginTop: '20px'
                                    }
                                }
                            ]
                        }
                ]
            }
        ]        
        
    });