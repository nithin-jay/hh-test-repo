Ext.widget({
	xtype: 'mz-form-widget',
	itemId: 'featuredCategories',
	items: [
	        {
			xtype: 'container',
			id: 'mainContainer',
			items: [       
			        {
			    	 xtype: 'panel',
			    	 layout: 'hbox',
			    	 id:'mainPanel',
			    	 listeners:{
			    		 afterrender:function(cmp){
			    			 window.cmp = cmp;
			    			 var length = globalDataVariable.length();
			    			 if(length > 0){
					         		cmp.up('#featuredCategories').updatePreview(length);
			    			 }
			    		 }
			         }
			        }
		        ]
	        } 
        ],
        updatePreview: function(length){
        	var length = length;
        	for(var i=0; i<length;i++){
        		window.cmp.up('#mainContainer').add({
    				xtype: 'container',
    				style: "background:#ccb;margin:10px; padding:10px",
    				items: [
    					{
    	        			name: "categorytitle",
    					    fieldLabel: "Category Title",
    					    xtype: "mz-input-text",
    					    flex: 1,
    					    maxLength: 80,      
    					    style: {
    								marginLeft: '0px'
    							}
    	        		},
    			        {
    			        	xtype: 'panel',
    			        	layout: 'hbox',
    			        	items: [
    		        	        {
    					        	xtype: 'mz-input-image',
    					            fieldLabel: 'category Image',
    					            name: 'Images' 
    					        },
    					        	{
    					        	xtype: 'panel',
    					        	layout: 'vbox',
    					        	items: [
    					        	    {
    					        	    	xtype: 'panel',
        						        	layout: 'hbox',
        						        	items: [
    											  {
    											    name: "category1",
    											    fieldLabel: "Category 1",
    											    xtype: "mz-input-text",
    											    flex: 1,
    											    style: {
    														marginLeft: '10px'
    													}
    											  },
    											 {
    								                    xtype: 'mz-input-text',
    								                    name: 'categorylink1',
    								                    fieldLabel: 'Category Link 1',
    								                    flex: 2,
    								                    style: {
    								                        marginLeft: '10px'
    								                    }
    								              }
    											  
        						        	]
    					        	    },
    					        	    {
    							        	xtype: 'panel',
        						        	layout: 'hbox',
        						        	items: [
        						        	      {
    												    name: "category2",
    												    fieldLabel: "Category 2",
    												    xtype: "mz-input-text",
    												    flex: 1,
    												    style: {
    															marginLeft: '10px'
    														}
    												  }, 
    												  {
    												        xtype: 'mz-input-text',
    												        name: 'categorylink2',
    												        fieldLabel: 'Category Link 2',
    												        flex: 2,
    												        style: {
    												            marginLeft: '10px'
    												        }
    												  }
    						        	     ]
    							        },
    					        	    {
    							        	xtype: 'panel',
        						        	layout: 'hbox',
        						        	items: [
    											{
    											    name: "category3",
    											    fieldLabel: "Category 3",
    											    xtype: "mz-input-text",
    											    flex: 1,
    											    style: {
    														marginLeft: '10px'
    													}
    											 },
    											 {
    												    name: "categorylink3",
    												    fieldLabel: "Category Link 3",
    												    xtype: "mz-input-text",
    												    flex: 1,
    												    style: {
    															marginLeft: '10px'
    														}
    												 }
    						        	     ]
    							        },
    							        {
    							        	xtype: 'panel',
        						        	layout: 'hbox',
        						        	items: [
        						        		{
        											 name: "promoID",
        											 fieldLabel: "Promo ID",
        											 xtype: "mz-input-text",
        											 flex: 1,
        											 style: {
        													marginLeft: '10px'
        												 }
        										 },
        										 {
        											 name: "promoName",
        											 fieldLabel: "Promo Name",
        											 xtype: "mz-input-text",
        											 flex: 1,
        											 style: {
        												 	marginLeft: '10px'
        											 }
        										 }
    						        	     ]
    							        }
    								]
    					        }
    	        	        ]
    			        }
    			        
    			     ]
    			});
        	}
        },
 	   getData: function(){
    	var sliderData = this.getValues();
    	Ext.each(sliderData.Images, function(item, index){
    		if(sliderData.categorytitle[index]) {
    			item.categorytitle = sliderData.categorytitle[index];
    			
    			if(sliderData.category1) {
        			item.category1 = sliderData.category1[index];
        		}
        		if(sliderData.categorylink1) {
        			item.categorylink1 = sliderData.categorylink1[index];
        		}
        		if(sliderData.category2) {
        			item.category2 = sliderData.category2[index];
        		}
        		if(sliderData.categorylink2) {
        			item.categorylink2 = sliderData.categorylink2[index];
        		}
        		if(sliderData.category3) {
        			item.category3 = sliderData.category3[index];
        		}
        		if(sliderData.categorylink3) {
        			item.categorylink3 = sliderData.categorylink3[index];
        		}
        		if(sliderData.promoID){
        			item.promoID = sliderData.promoID[index];
        		}
        		if(sliderData.promoName){
        			item.promoName = sliderData.promoName[index];
        		}
    		} else if (item){
    			item.categorytitle = "";
    		}
    	});
    	delete sliderData.categorytitle;
    	delete sliderData.category1;
    	delete sliderData.categorylink1;
    	delete sliderData.category2;
    	delete sliderData.categorylink2;
    	delete sliderData.category3;
    	delete sliderData.categorylink3;
    	delete sliderData.promoID;
    	delete sliderData.promoName;
    	var filter = {
    			Images: []
    	};
    	Ext.each(sliderData.Images, function(item, index){
    		if(item && item.categorytitle){
    			filter.Images.push(item);
    		}
    	});
    	return Ext.applyIf(filter, this.data);
    },
        setData: function(data){
        	debugger;
        	var defaultLength = 9;
        	Ext.define('globalDataVariable',{ singleton:true, data:data,length: function(){
        		if(data.Images){
        			if(data.Images.length > 0){
            			return (defaultLength-data.Images.length);
        			}
        			else{
        				return defaultLength;
        			}
        		}
        		else{
        			return defaultLength;
        		}
        	} }); 
        	this.getForm().setValues(data);
        	if(!Ext.Object.isEmpty(data)){
        		for(var i = 0; i < data.Images.length; i++){
        			Ext.getCmp('mainContainer').add( 
        					{
		    					xtype: 'container',
		    					style: "background:#ccb;margin:10px; padding:10px",
		    					items: [
		    						{
					        			name: "categorytitle",
									    fieldLabel: "Category Title",
									    xtype: "mz-input-text",
									    value: data.Images[i].categorytitle,
									    flex: 2,
									    maxLength: 80,
									    style: {
												marginLeft: '0px'
											}
					        		},
    						        {
    						        	xtype: 'panel',
    						        	layout: 'hbox',
    						        	items: [
						        	        {
    								        	xtype: 'mz-input-image',
    								            fieldLabel: 'categoryImage',
    								            name: 'Images',
    								            value: data.Images[i]
									        },
									        	{
									        	xtype: 'panel',
		    						        	layout: 'vbox',
		    						        	items: [
		    						        	    {
		    						        	    	xtype: 'panel',
    			    						        	layout: 'hbox',
    			    						        	items: [
															  {
															    name: "category1",
															    fieldLabel: "Category 1",
															    xtype: "mz-input-text",
															    value: data.Images[i].category1,
															    flex: 1,
															    style: {
																		marginLeft: '10px'
																	}
															  },
															 {
												                    xtype: 'mz-input-text',
												                    name: 'categorylink1',
												                    fieldLabel: 'Category Link 1',
												                    value: data.Images[i].categorylink1,
												                    flex: 2,
												                    style: {
												                        marginLeft: '10px'
												                    }
												              }
															  
    			    						        	]
		    						        	    },
		    						        	    {
											        	xtype: 'panel',
    			    						        	layout: 'hbox',
    			    						        	items: [
    			    						        	      {
																    name: "category2",
																    fieldLabel: "Category 2",
																    xtype: "mz-input-text",
																    value: data.Images[i].category2,
																    flex: 1,
																    style: {
																			marginLeft: '10px'
																		}
																  }, 
																  {
																        xtype: 'mz-input-text',
																        name: 'categorylink2',
																        fieldLabel: 'Category Link 2',
																        value: data.Images[i].categorylink2,
																        flex: 2,
																        style: {
																            marginLeft: '10px'
																        }
																  }
			    						        	     ]
											        },
		    						        	    {
											        	xtype: 'panel',
    			    						        	layout: 'hbox',
    			    						        	items: [
															{
															    name: "category3",
															    fieldLabel: "Category 3",
															    xtype: "mz-input-text",
															    value:data.Images[i].category3,
															    flex: 1,
															    style: {
																		marginLeft: '10px'
																	}         
															 },
															 {
																    name: "categorylink3",
																    fieldLabel: "Category Link 3",
																    xtype: "mz-input-text",
																    value: data.Images[i].categorylink3,
																    flex: 1,
																    style: {
																			marginLeft: '10px'
																		}
																 }
			    						        	     ]
											        },
											        {
			    							        	xtype: 'panel',
			        						        	layout: 'hbox',
			        						        	items: [
			        						        		{
			        											 name: "promoID",
			        											 fieldLabel: "Promo ID",
			        											 xtype: "mz-input-text",
			        											 value: data.Images[i].promoID,
			        											 flex: 1,
			        											 style: {
			        													marginLeft: '10px'
			        												 }
			        										 },
			        										 {
			        											 name: "promoName",
			        											 fieldLabel: "Promo Name",
			        											 xtype: "mz-input-text",
			        											 value: data.Images[i].promoName,
			        											 flex: 1,
			        											 style: {
			        												 	marginLeft: '10px'
			        											 }
			        										 }
			    						        	     ]
			    							        }
												]
									        }
					        	        ]
    						        }
    						        
    						     ]
		    				}
        			);
        		}   
        	}
        	this.data = data; 
        }
});
