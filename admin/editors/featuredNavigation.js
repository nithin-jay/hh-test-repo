Ext.widget({
    xtype: 'mz-form-widget',
    itemId: 'featuredNavigationWidget',
    items: [
    	{
            xtype: 'container',
            layout: 'vbox',
            items: [
            	{
            		xtype:  'panel',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'mz-input-image',
                            name: 'LogoImage',
                            fieldLabel: 'Logo Image',
                        },
                        {
                            xtype:  'panel',
                            layout: 'vbox',
                            items: [
                                {
                                    xtype:'mz-input-text',
                                    allowBlank: true,
                                    name:'logoUrl',
                                    fieldLabel: 'Logo-Url',
                                    style: {
                                        marginLeft: '50px',
                                        marginBottom:'40px',
                                        marginTop:'10px'
                                    }
                                },
                                {
                                    xtype:'mz-input-text',
                                    allowBlank: false,
                                    name:'title',
                                    fieldLabel: 'Title',
                                    style: {
                                        marginLeft: '50px'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
            		xtype: 'panel',
                    layout: 'vbox',
                    items: [
                        {
                            xtype: 'mz-input-image',
                            name: 'FeaturedImage',
                            fieldLabel: 'Featured Image'
                        }
                    ]
                },
                {
            		xtype: 'panel',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'mz-input-color',
                            name: 'backgroundcolorPicker',
                            fieldLabel: 'Select background color'
                        },
                        {
                            xtype: 'mz-input-color',
                            name: 'linkcolorPicker',
                            fieldLabel: 'Select link color',
                            style: {
                                marginLeft: '50px'
                            }
                        }
                    ]
                },
                {
            		xtype: 'panel',
                    layout: 'hbox',
                    items: [
                        {
                            allowBlank: true,
                            fieldLabel: 'Link Name 1',
                            name:'linkName1',
                            xtype:'mz-input-text'
                        },
                        {
                            allowBlank: true,
                            fieldLabel: 'Link Url 1',
                            name:'linkUrl1',
                            xtype:'mz-input-text',
                            style: {
                                marginLeft: '50px'
                            }
                        }
                    ]
                },
                {
            		xtype: 'panel',
                    layout: 'hbox',
                    items: [
                        {
                            allowBlank: true,
                            fieldLabel: 'Link Name 2',
                            name:'linkName2',
                            xtype:'mz-input-text'
                        },
                        {
                            allowBlank: true,
                            fieldLabel: 'Link Url 2',
                            name:'linkUrl2',
                            xtype:'mz-input-text',
                            style: {
                                marginLeft: '50px'
                            }
                        }
                    ]
                },
                {
            		xtype: 'panel',
                    layout: 'hbox',
                    items: [
                        {
                            allowBlank: true,
                            fieldLabel: 'Link Name 3',
                            name:'linkName3',
                            xtype:'mz-input-text'
                        },
                        {
                            allowBlank: true,
                            fieldLabel: 'Link Url 3',
                            name:'linkUrl3',
                            xtype:'mz-input-text',
                            style: {
                                marginLeft: '50px'
                            }
                        }
                    ]
                },
                {
            		xtype: 'panel',
                    layout: 'hbox',
                    items: [
                        {
                            allowBlank: true,
                            fieldLabel: 'More-Link',
                            name:'morelink',
                            xtype:'mz-input-text',
                            style: {
                                marginBottom:'50px'
                            }
                        },
                        {
                            allowBlank: true,
                            fieldLabel: 'More-Link Url',
                            name:'morelinkUrl',
                            xtype:'mz-input-text',
                            style: {
                                marginLeft: '50px',
                                marginBottom:'50px'
                            }
                        }
                    ]
                }
            ]
    	}
    ]
});