Ext.widget({
    xtype: 'mz-form-widget',
    itemId: 'bannerForm',
    items: [{
        xtype: 'mz-input-text',
        id: 'categoryName',
        name: 'categoryName',
        fieldLabel: 'Banner Text (Max length 20 character)',
        maxLength: 20,
        enforceMaxLength: true,
        maxLengthText: "The maximum length for this field is 20"
    },
    {
        xtype: 'mz-input-color',
        name: 'fontColor',
        fieldLabel: 'Banner Font Color'
    },
    {
        xtype: 'mz-input-imageurl',
        name: 'bannerImage',
        fieldLabel: 'Banner Image'
    }
 ]

}); 
