var p1 = 0; //append this value to productCodes array to make an unique arrays
Ext.widget({
	xtype: 'mz-form-widget',
	itemId: 'imgSliderForm',
	items: [
		{
			xtype: 'container',
			id: 'mainConatiner',
			items: [
				{
					xtype: 'panel',
					layout: 'hbox',
					items: [
						{
							name: "width",
							fieldLabel: "Container Width",
							xtype: "mz-input-text",
							flex: 1,
							value: '100%',
							style: {
								marginLeft: '10px',
								marginRight: '30px'
							}
						},
						{
							xtype: 'mz-input-text',
							fieldLabel: 'Custom Class Name',
							name: 'customClass',
							itemId: 'customClassId'
						}
					]
				},
				{
					xtype: 'panel',
					layout: 'hbox',
					items: [
						{
							xtype: 'mz-input-number',
							fieldLabel: 'Number of fields',
							id: 'noOfFields',
							name: 'NoOfFields',
							value: 1,
							style: {
								marginLeft: '10px'
							}
						},
						{
							xtype: 'button',
							text: 'Add',
							name: 'AddBtn',
							style: {
								marginTop: '20px'
							},
							listeners: {
								click: function (e) {
									var noOfFields = Ext.getCmp('noOfFields').getValue();

									for (var i = 0; i < noOfFields; i++) {
										e.up('form').add(
											{
												xtype: 'container',
												style: {
													background: '#ccb',
													padding: '10px',
													marginLeft: '30px',
													marginTop: '30px'
												},
												items: [
													{
														xtype: 'panel',
														layout: 'hbox',
														items: [
															{
																xtype: 'mz-input-image',
																fieldLabel: 'Desktop Image',
																name: 'Images'
															},
															{
																xtype: 'panel',
																layout: 'vbox',
																items: [
																	{
																		xtype: 'panel',
																		layout: 'hbox',
																		items: [
																			{
																				name: "deskWidth",
																				fieldLabel: "Width",
																				xtype: "mz-input-text",
																				flex: 1,
																				value: '100%',
																				style: {
																					marginLeft: '10px'
																				}
																			},
																			{
																				name: "linktoopen",
																				fieldLabel: "Link To Open",
																				xtype: "mz-input-text",
																				flex: 1,
																				style: {
																					marginLeft: '10px'
																				}
																			}
																		]
																	},
																	{
																		xtype: 'panel',
																		layout: 'hbox',
																		items: [
																			{
																				name: "openInNewTab",
																				fieldLabel: "Open In New Tab",
																				xtype: "mz-input-dropdown",
																				flex: 1,
																				store: [
																					'Yes',
																					'No'
																				],
																				style: {
																					marginLeft: '10px'
																				}
																			},
																			{
																				name: "promoID",
																				fieldLabel: "Promo ID",
																				xtype: "mz-input-text",
																				flex: 1,
																				style: {
																					marginLeft: '10px'
																				}
																			}
																		]
																	},
																	{
																		xtype: 'panel',
																		layout: 'hbox',
																		items: [
																			{
																				name: "promoName",
																				fieldLabel: "Promo Name",
																				xtype: "mz-input-text",
																				flex: 1,
																				style: {
																					marginLeft: '10px'
																				}
																			},
																			{
																				name: "PromotionDate",
																				fieldLabel: "Promotion Date (start date - end date)",
																				xtype: "mz-input-text",
																				flex: 1,
																				style: {
																					marginLeft: '10px'
																				}
																			}
																		]
																	},
																	{
																		xtype: 'panel',
																		layout: 'hbox',
																		width: 500,
																		items: [
																			{
																				name: "titleText",
																				fieldLabel: "Title Text",
																				xtype: "mz-input-text",
																				flex: 1,
																				style: {
																					marginLeft: '10px'
																				}
																			},
																			{
																				name: "subCopySection",
																				fieldLabel: "Sub Copy Section",
																				xtype: "mz-input-textarea",
																				flex: 1,
																				style: {
																					marginLeft: '10px'
																				}
																			}
																		]
																	},
																	{
																		xtype: 'panel',
																		layout: 'hbox',
																		items: [
																			{
																				name: "orderNumber",
																				fieldLabel: "Position of the Field",
																				xtype: "mz-input-text",
																				flex: 1,
																				style: {
																					marginLeft: '10px'
																				}
																			}
																		]
																	}
																]
															}
														]
													},
													{
														xtype: 'panel',
														layout: 'hbox',
														items: [
															{
																xtype: 'mz-input-image',
																fieldLabel: 'Mobile Image',
																name: 'mobImages'
															},
															{
																xtype: 'panel',
																layout: 'vbox',
																items: [
																	{
																		xtype: 'panel',
																		layout: 'hbox',
																		items: [
																			{
																				name: "mobWidth",
																				value: '100%',
																				fieldLabel: "Width",
																				xtype: "mz-input-text",
																				flex: 1,
																				style: {
																					marginLeft: '10px'
																				}
																			}
																		]
																	}
																]
															}
														]
													},
													{
														xtype: 'panel',
														layout: 'vbox',
														items: [
															{
																xtype: 'button',
																text: 'Delete',
																name: 'DeleteBtn',
																flex: 1,
																style: {
																	marginTop: '0px',
																	marginLeft: '10px'
																},
																listeners: {
																	click: function (e) {
																		e.up('panel').up('container').destroy();
																	}
																}
															}
														]
													}

												]
											}
										);
										p1++;
									}
								}
							}
						}
					]
				}
			]
		}
	],
	getData: function () {
		var sliderData = this.getValues();
		if (!Ext.isArray(sliderData.Images)) {  //To work with only one image
			sliderData.Images = [sliderData.Images];
		}
		if (!Ext.isArray(sliderData.mobImages)) {  //To work with only one image
			sliderData.mobImages = [sliderData.mobImages];
		}
		//Removed null from array
		sliderData.Images = sliderData.Images.filter(function (el) {
			return el != null;
		});
		//Removed null from array
		// sliderData.mobImages = sliderData.mobImages.filter(function (el) {
		// 	return el != null;
		// });

		Ext.each(sliderData.Images, function (item, index) {
			if(sliderData.mobImages[index] == null) {
				sliderData.mobImages[index] = {};
				sliderData.mobImages[index].imageUrl= sliderData.Images[index].imageUrl;
				sliderData.mobImages[index].altText="";
			}
			if (sliderData.deskWidth) {
				if (Ext.isArray(sliderData.deskWidth)) {
					item.deskWidth = sliderData.deskWidth[index];
				} else {
					item.deskWidth = sliderData.deskWidth;
				}
			} else {
				item.deskWidth = '';
			}
			if (sliderData.linktoopen) {
				if (Ext.isArray(sliderData.linktoopen)) {
					item.linktoopen = sliderData.linktoopen[index];
				} else {
					item.linktoopen = sliderData.linktoopen;
				}
			} else {
				item.linktoopen = '';
			}
			if (sliderData.openInNewTab) {
				if (Ext.isArray(sliderData.openInNewTab)) {
					item.openInNewTab = sliderData.openInNewTab[index];
				} else {
					item.openInNewTab = sliderData.openInNewTab;
				}
			} else {
				item.openInNewTab = '';
			}
			if (sliderData.promoID) {
				if (Ext.isArray(sliderData.promoID)) {
					item.promoID = sliderData.promoID[index];
				} else {
					item.promoID = sliderData.promoID;
				}
			} else {
				item.promoID = '';
			}
			if (sliderData.promoName) {
				if (Ext.isArray(sliderData.promoName)) {
					item.promoName = sliderData.promoName[index];
				} else {
					item.promoName = sliderData.promoName;
				}
			} else {
				item.promoName = '';
			}
			if (sliderData.PromotionDate) {
				if (Ext.isArray(sliderData.PromotionDate)) {
					item.PromotionDate = sliderData.PromotionDate[index];
				} else {
					item.PromotionDate = sliderData.PromotionDate;
				}
			} else {
				item.PromotionDate = '';
			}
			if (sliderData.titleText) {
				if (Ext.isArray(sliderData.titleText)) {
					item.titleText = sliderData.titleText[index];
				} else {
					item.titleText = sliderData.titleText;
				}
			} else {
				item.titleText = '';
			}
			if (sliderData.subCopySection) {
				if (Ext.isArray(sliderData.subCopySection)) {
					item.subCopySection = sliderData.subCopySection[index];
				} else {
					item.subCopySection = sliderData.subCopySection;
				}
			} else {
				item.subCopySection = '';
			}
			if (sliderData.orderNumber) {
				if (Ext.isArray(sliderData.orderNumber)) {
					item.orderNumber = sliderData.orderNumber[index];
				} else {
					item.orderNumber = sliderData.orderNumber;
				}
			} else {
				item.orderNumber = '';
			}
		});

		Ext.each(sliderData.mobImages, function (item, index) {
			
			if (sliderData.mobWidth) {
				if (Ext.isArray(sliderData.mobWidth)) {
					sliderData.mobImages[index].mobWidth = sliderData.mobWidth[index];
				} else {
					sliderData.mobImages[index].mobWidth = sliderData.mobWidth;
				}
			}
			if (sliderData.linktoopen) {
				if (Ext.isArray(sliderData.linktoopen)) {
					sliderData.mobImages[index].linktoopen = sliderData.linktoopen[index];
				} else {
					sliderData.mobImages[index].linktoopen = sliderData.linktoopen;
				}
			}
			if (sliderData.openInNewTab) {
				if (Ext.isArray(sliderData.openInNewTab)) {
					sliderData.mobImages[index].openInNewTab = sliderData.openInNewTab[index];
				} else {
					sliderData.mobImages[index].openInNewTab = sliderData.openInNewTab;
				}
			}
			if (sliderData.promoID) {
				if (Ext.isArray(sliderData.promoID)) {
					sliderData.mobImages[index].promoID = sliderData.promoID[index];
				} else {
					sliderData.mobImages[index].promoID = sliderData.promoID;
				}
			}
			if (sliderData.promoName) {
				if (Ext.isArray(sliderData.promoName)) {
					sliderData.mobImages[index].promoName = sliderData.promoName[index];
				} else {
					sliderData.mobImages[index].promoName = sliderData.promoName;
				}
			}
			if (sliderData.PromotionDate) {
				if (Ext.isArray(sliderData.PromotionDate)) {
					sliderData.mobImages[index].PromotionDate = sliderData.PromotionDate[index];
				} else {
					sliderData.mobImages[index].PromotionDate = sliderData.PromotionDate;
				}
			}
			if (sliderData.titleText) {
				if (Ext.isArray(sliderData.titleText)) {
					sliderData.mobImages[index].titleText = sliderData.titleText[index];
				} else {
					sliderData.mobImages[index].titleText = sliderData.titleText;
				}
			}
			if (sliderData.subCopySection) {
				if (Ext.isArray(sliderData.subCopySection)) {
					sliderData.mobImages[index].subCopySection = sliderData.subCopySection[index];
				} else {
					sliderData.mobImages[index].subCopySection = sliderData.subCopySection;
				}
			}
			if (sliderData.orderNumber) {
				if (Ext.isArray(sliderData.orderNumber)) {
					sliderData.mobImages[index].orderNumber = sliderData.orderNumber[index];
				} else {
					sliderData.mobImages[index].orderNumber = sliderData.orderNumber;
				}
			}
		});

		delete sliderData.deskWidth;
		delete sliderData.linktoopen;
		delete sliderData.openInNewTab;
		delete sliderData.promoID;
		delete sliderData.promoName;
		delete sliderData.PromotionDate;
		delete sliderData.titleText;
		delete sliderData.mobWidth;
		delete sliderData.subCopySection;
		delete sliderData.orderNumber;

		return Ext.applyIf(sliderData, this.data);
	},
	setData: function (data) {
		this.getForm().setValues(data);
		if (!Ext.Object.isEmpty(data)) {
			for (var i = 0; i < data.Images.length; i++) {
				Ext.getCmp('mainConatiner').add(
					{
						xtype: 'container',
						style: "background:#ccb;margin:10px; padding:10px",
						items: [
							{
								xtype: 'panel',
								layout: 'hbox',
								items: [
									{
										xtype: 'mz-input-image',
										fieldLabel: 'Desktop Image',
										name: 'Images',
										value: data.Images[i]
									},
									{
										xtype: 'panel',
										layout: 'vbox',
										items: [
											{
												xtype: 'panel',
												layout: 'hbox',
												items: [
													{
														name: "deskWidth",
														fieldLabel: "Width",
														xtype: "mz-input-text",
														value: data.Images[i].deskWidth,
														flex: 1,
														style: {
															marginLeft: '10px'
														}
													},
													{
														name: "linktoopen",
														fieldLabel: "Link To Open",
														xtype: "mz-input-text",
														value: data.Images[i].linktoopen,
														flex: 1,
														style: {
															marginLeft: '10px'
														}
													}
												]
											},
											{
												xtype: 'panel',
												layout: 'hbox',
												items: [
													{
														name: "openInNewTab",
														fieldLabel: "Open In New Tab",
														xtype: "mz-input-dropdown",
														value: data.Images[i].openInNewTab,
														flex: 1,
														store: [
															'Yes',
															'No'
														],
														style: {
															marginLeft: '10px'
														}
													},
													{
														name: "promoID",
														fieldLabel: "Promo ID",
														xtype: "mz-input-text",
														value: data.Images[i].promoID,
														flex: 1,
														style: {
															marginLeft: '10px'
														}
													}
												]
											},
											{
												xtype: 'panel',
												layout: 'hbox',
												items: [
													{
														name: "promoName",
														fieldLabel: "Promo Name",
														xtype: "mz-input-text",
														value: data.Images[i].promoName,
														flex: 1,
														style: {
															marginLeft: '10px'
														}
													},
													{
														name: "PromotionDate",
														fieldLabel: "Promotion Date (start date - end date)",
														xtype: "mz-input-text",
														value: data.Images[i].PromotionDate,
														flex: 1,
														style: {
															marginLeft: '10px'
														}
													}
												]
											},
											{
												xtype: 'panel',
												layout: 'hbox',
												width: 500,
												items: [
													{
														name: "titleText",
														fieldLabel: "Title Text",
														xtype: "mz-input-text",
														value: data.Images[i].titleText,
														flex: 1,
														style: {
															marginLeft: '10px'
														}
													},
													{
														name: "subCopySection",
														fieldLabel: "Sub Copy Section",
														xtype: "mz-input-textarea",
														value: data.Images[i].subCopySection,
														flex: 1,
														style: {
															marginLeft: '10px'
														}
													}
												]
											},
											{
												xtype: 'panel',
												layout: 'hbox',
												items: [
													{
														name: "orderNumber",
														fieldLabel: "Position of the Field",
														xtype: "mz-input-text",
														value: data.Images[i].orderNumber
														,
														flex: 1,
														style: {
															marginLeft: '10px'
														}
													}
												]
											}
										]
									},
								]
							},
							{
								xtype: 'panel',
								layout: 'hbox',
								items: [
									{
										xtype: 'mz-input-image',
										fieldLabel: 'Mobile Image',
										name: 'mobImages',
										value: data.mobImages[i]
									},
									{
										xtype: 'panel',
										layout: 'vbox',
										items: [
											{
												xtype: 'panel',
												layout: 'hbox',
												items: [
													{
														name: "mobWidth",
														fieldLabel: "Width",
														xtype: "mz-input-text",
														value: data.mobImages[i].mobWidth,
														flex: 1,
														style: {
															marginLeft: '10px'
														}
													}
												]
											}
										]
									}
								]
							},
							{
								xtype: 'panel',
								layout: 'vbox',
								items: [
									{
										xtype: 'button',
										text: 'Delete',
										name: 'DeleteBtn',
										flex: 1,
										style: {
											marginTop: '0px',
											marginLeft: '10px'
										},
										listeners: {
											click: function (e) {
												e.up('panel').up('container').destroy();
											}
										}
									}
								]
							}
						]
					}
				);
			}
		}
		this.data = data;
	}
});