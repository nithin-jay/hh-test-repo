Ext.widget({
    xtype: 'mz-form-widget',
    itemId: 'BrandLogos',
    id: 'BrandLogosFormId',
    items: [
    	{
    		xtype: 'container',
            id: 'brandConatiner',
            layout: 'hbox',
            items: [
	    		{
	               xtype: 'mz-input-number',
	               fieldLabel: 'Number of fields',
	               id: 'noOfFields',
	               name: 'NoOfFields',
	               value: 1
	            },
	        	{
	               xtype: 'button',
	               text: 'Add',
	               name: 'AddBtn',
	               style: {
	                   marginTop: '30px',
	                   marginLeft: '10px'
	               },
	               listeners: {
	            	   click: function (e) {
	            		   var noOfFields = Ext.getCmp('noOfFields').getValue();
	            		   
	            		   for (var i = 0; i < noOfFields; i++) {
	            			   e.up('form').add({
	            				   xtype: 'container',
                                   layout: 'hbox',
                                   items: [
                                	   {
                                           xtype: 'mz-input-image',
                                           fieldLabel: 'Image',
                                           name: 'Images'
                                       },
                                       {
                                           xtype: 'mz-input-text',
                                           fieldLabel: 'Link To Open',
                                           name: 'LinkToOpen',
                                           itemId: 'LinkToOpenId',
                                           style: {
                                               marginLeft: '20px'
                                           },
                                           flex: 1
                                       },
                                       {
                                           xtype: 'mz-input-dropdown',
                                           fieldLabel: 'Select Target',
                                           name: 'TargetLink',
                                           itemId: 'TargetLinkId',
                                           flex: 1,
                                           store: [
	                                           '_self',
	                                           '_blank'
                                           ],
                                           style: {
                                               marginLeft: '10px'
                                           }
                                       	},
                                       	{
                                           xtype: 'button',
                                           text: 'Delete',
                                           name: 'DeleteBtn',
                                           style: {
                                               marginTop: '30px',
                                               marginLeft: '10px'
                                           },
                                           listeners: {
                                               click: function (e) {
                                                   e.up('container').destroy();
                                               }
                                           }
                                       	}
                                   ]
	            			   });
	            		   }
	            	   }
	               }
	        	}
            ]	
    	}
    ],
    getData: function () {
    	debugger;
    	var footerData = this.getValues();
    	Ext.each(footerData.Images, function (item, index) {
            footerData.Images[index].linkToOpen = footerData.LinkToOpen[index];
            footerData.Images[index].targetLink = footerData.TargetLink[index];
        });
    	 
    	 return Ext.applyIf(footerData, this.data);
    },
    
    setData: function (data) {
    	 this.getForm().setValues(data);
    	 debugger;
    	 if (!Ext.Object.isEmpty(data)) {
    		 for (var i = 0; i < data.Images.length; i++) {
        		 Ext.getCmp('BrandLogosFormId').add({
                     xtype: 'panel',
                     layout: 'hbox',
                     items: [
                    	 {
                             xtype: 'mz-input-image',
                             fieldLabel: 'Image',
                             name: 'Images',
                             value: data.Images[i]
                    	 }
                         , {
                             xtype: 'mz-input-text',
                             fieldLabel: 'Link To Open',
                             name: 'LinkToOpen',
                             itemId: 'LinkToOpenId',
                             value: data.Images[i].linkToOpen,
                             style: {
                                 marginLeft: '20px'
                             },
                             flex: 1
                         },
                         {
                             xtype: 'mz-input-dropdown',
                             fieldLabel: 'Select Target',
                             name: 'TargetLink',
                             itemId: 'TargetLinkId',
                             value: data.Images[i].targetLink,
                             flex: 1,
                             store: [
        	                     '_self',
        	                     '_blank'
                             ],
                             style: {
                                 marginLeft: '10px'
                             }
                         },
                         {
                             xtype: 'button',
                             text: 'Delete',
                             name: 'DeleteBtn',
                             style: {
                                 marginTop: '30px',
                                 marginLeft: '10px'
                             },
                             listeners: {
                                 click: function (e) {
                                     e.up('container').destroy();
                                 }
                             }
                         }
                     ]
                 });
        	 }
    	 }
    	 
    	 this.data = data;
    }
});