﻿Ext.widget({
    xtype: 'mz-form-entity',
    title: 'Order Email',
    items: [
        {
            fieldLabel: 'Add personable message',
            xtype: 'taco-htmleditor',
            name: "personalmsgone",
            enableFont: false
        },

        {
            fieldLabel: 'Add additional information',
            xtype: 'taco-htmleditor',
            name: 'personalmessagetwo',
            enableFont: false
        },

        {
            fieldLabel: 'Pick up details message',
            xtype: 'taco-htmleditor',
            name: 'pickupmsg',
            enableFont: false
        },
        {
            fieldLabel: 'Ship To Home details message',
            xtype: 'taco-htmleditor',
            name: 'shipToHomemsg',
            enableFont: false
        },
        {
            fieldLabel: 'Important Detail About Order',
            xtype: 'taco-htmleditor',
            name: 'importantOrdermsg',
            enableFont: false
        },
        {
            fieldLabel: 'Email Subject',
            xtype: 'textfield',
            name: 'subject'
        },
        {
            fieldLabel: 'Header html 1',
            xtype: 'taco-htmleditor',
            name: 'html_1',
            enableFont: false
        },
        {
            fieldLabel: 'Header html 2',
            xtype: 'taco-htmleditor',
            name: 'html_2',
            enableFont: false
        },
        {
            fieldLabel: 'Custom Field 1',
            xtype: 'textfield',
            name: 'custom1'
        }
    ]
});