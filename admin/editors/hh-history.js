Ext.widget({
	xtype: 'mz-form-widget',
	itemId: 'historyForm',
	items: [
		{
			xtype: 'container',
			id: 'mainConatiner',
			items:[
			       {
	            	xtype: 'form',
	            	name: 'headerForm',
	            	id: 'headerForm',
	            	items: [
	                        {
	                            xtype: 'panel',
	                            border: false,
	                            layout: 'hbox',
	                            name: 'headerPanel',
	                            items: [
	                                {
	                                	xtype: 'mz-input-number',
	                        			fieldLabel: 'Number of Pages',
	                        			id: 'noOfVideos',
	                        			name: 'noOfVideos',
	                        			value: 1,
	                        			style: {
									        marginLeft: '10px'
									    }
	                                },
	                                {
	                                	xtype: 'button',
	                    			    text: 'Add',
	                    			    name: 'AddBtn',
	                    			    style: {
	                    			        marginTop: '30px',
	                    			        marginLeft: '10px'
	                    			    },
	                    			    listeners: {
	                    			    	click: function(e){
	                    			    		var noOfFields = Ext.getCmp('noOfVideos').getValue();
	                    			    		for(var i = 0; i < noOfFields; i++){
	                    			    			e.up('form').add(
	                    			    					{
		                    			    					xtype: 'container',
		                    			    					layout: 'hbox',
	                    			    						items: [
																		{
																			xtype: 'mz-input-imageurl',
																			fieldLabel: 'Select Image',
																			name: 'historyImage',
																			itemId: 'history-image',
																			flex: 2
																		},
	                    			    						        {
	                    			    						        	xtype: 'mz-input-richtext',
	                    			    					    			fieldLabel: 'Description',
	                    			    					    			name: 'description',
	                    			    					    			itemId: 'descriptionid',
	                    			    					    			flex: 2,
	                    			    					    			style: {
	                    			    					    				marginLeft: '10px'
	                    			    					    			},
	                    			    						        },
	                    			    						        {
	                    			    						        	xtype: 'button',
	                    			                        			    text: 'Delete',
	                    			                        			    name: 'DeleteBtn',
	                    			                        			    style: {
	                    			                        			        marginTop: '30px',
	                    			                        			        marginLeft: '10px'
	                    			                        			    },
	                    			                        			    listeners: {
	                    			                        			    	click: function(e){
	                    			                        			    		e.up('container').destroy();
	                    			                        			    	}
	                    			                        			    }
	                    			    						        }
	            			    						        ]
	            			    							}
	            			    					);
	                    			    		}
	                    			    	}
	                    			    }
	                                }
	                            ]
	                        }
	                    ]
			       }
			   ]
			}
		],
		getData: function() {
	    	var historyData = this.getValues();
	    	
	    	if(Ext.isArray(historyData.description)){
	    		historyData.hhValues = historyData.description.map(function(item, index){
	        		return { description: historyData.description[index], historyImage: historyData.historyImage[index]};
	        	});
	    	}else{
	    		historyData.hhValues = [];
	    		historyData.hhValues.push({ description: historyData.description, historyImage: historyData.historyImage })
	    	}

	    	delete historyData.description;
	    	delete historyData.historyImage;
	    	
	    	return Ext.applyIf(historyData, this.data);
	    },
	    setData: function (data) {	        
	        if (!Ext.Object.isEmpty(data)) {
	            for (var i = 0; i < data.hhValues.length; i++) {
	                Ext.getCmp('headerForm').add({
	                    xtype: 'container',
	                    layout: 'hbox',
	                    items: [
							{
								xtype: 'mz-input-imageurl',
								fieldLabel: 'Select Image',
								name: 'historyImage',
								itemId: 'history-image',
								flex: 2,
								value: data.hhValues[i].historyImage,
							},
	                        {
								xtype: 'mz-input-richtext',
				    			fieldLabel: 'Description',
				    			name: 'description',
				    			itemId: 'descriptionid',
				    			flex: 2,
	                            value: data.hhValues[i].description,
	                            style: {
	                                marginLeft: '10px'
	                            },
	                            allowBlank: false
					        },
	                        {
	                            xtype: 'button',
	                            text: 'Delete',
	                            name: 'DeleteBtn',
	                            style: {
	                                marginTop: '30px',
	                                marginLeft: '10px'
	                            },
	                            flex: 1,
	                            listeners: {
	                                click: function (e) {
	                                    e.up('container').destroy();
	                                }
	                            }
					        }
				        ]
	                });
	            }
	        }
	        this.data = data;
	    }
});