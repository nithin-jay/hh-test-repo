Ext.widget({
	xtype: 'mz-form-widget',
	itemId: 'accordionform',
    items: [
            {
				xtype: 'container',
				id: 'accordionContainer',
				items:[
				    {
				    	xtype: 'panel',
						layout: 'hbox',
						items: [
								{
									xtype: 'mz-input-text',
									fieldLabel: 'Header',
									name: 'Header'
								},
								{
									xtype: 'mz-input-text',
									fieldLabel: 'Custom Class Name',
									name: 'customClass',
									itemId: 'customClassId',
									style: {
								        marginLeft: '10px',
								        marginRight: '10px'
								    }
								},
								{
									xtype: 'mz-input-text',
									fieldLabel: 'Unique Name Without space*',
									name: 'uniqueId'
								},
					        ]
				    }
		       ]
	        },
            {
            	xtype: 'form',
            	name: 'headerForm',
            	items: [
                        {
                            xtype: 'panel',
                            border: false,
                            layout: 'hbox',
                            name: 'headerPanel',
                            items: [
                                {
                                	xtype: 'mz-input-number',
                        			fieldLabel: 'Number of fields',
                        			id: 'noOfFields',
                        			name: 'NoOfFields',
                        			value: 1
                        			
                                },
                                {
                                	xtype: 'button',
                    			    text: 'Add',
                    			    name: 'AddBtn',
                    			    style: {
                    			        marginTop: '30px',
                    			        marginLeft: '10px'
                    			    },
                    			    listeners: {
                    			    	click: function(e){
                    			    		var noOfFields = Ext.getCmp('noOfFields').getValue();
                    			    		for(var i = 0; i < noOfFields; i++){
                    			    			e.up('form').add(
                    			    					{
	                    			    					xtype: 'container',
	                    			    					layout: 'hbox',
                    			    						items: [
                    			    						        {
                    			    						        	xtype: 'mz-input-text',
                    			    					    			fieldLabel: 'Accordion Title',
                    			    					    			name: 'accordiontitle',
                    			    					    			itemId: 'accordiontitle',
                    			    					    			flex: 2,
                    			    					    			allowBlank: false
                    			    						        },
                    			    						        {
                    			    						        	xtype: 'mz-input-textarea',
                    			    					    			fieldLabel: 'Accordion description',
                    			    					    			name: 'accordionDescription',
                    			    					    			itemId: 'accordionDescription',
                    			    					    			style: {
                    			    					    				marginLeft: '10px',
                    			    					    				marginBottom: '30px'
                    			    					    			},
                    			    					    			flex: 2
                    			    						        },
                    			    						        {
                    			    						        	xtype: 'button',
                    			                        			    text: 'Delete',
                    			                        			    name: 'DeleteBtn',
                    			                        			    flex: 1,
                    			                        			    style: {
                    			                        			        marginTop: '30px',
                    			                        			        marginLeft: '10px'
                    			                        			    },
                    			                        			    listeners: {
                    			                        			    	click: function(e){
                    			                        			    		e.up('container').destroy();
                    			                        			    	}
                    			                        			    }
                    			    						        }
            			    						        ]
            			    							}
            			    					);
                    			    		}
                    			    	}
                    			    }
                                }
                            ]
                        }
                    ]
            }
    ],
    getData: function() {
    	var accordionData = this.getValues();
    	if(Ext.isArray(accordionData.accordiontitle)){
    		var out = accordionData.accordiontitle.map(function(item, index){
        		return { title: item, description: accordionData.accordionDescription[index]};
        	});
    		accordionData.accordionContent = out;
    	}else{
    		accordionData.accordionContent = [];
    		accordionData.accordionContent.push({title: accordionData.accordiontitle, description: accordionData.accordionDescription})
    	}
    	
    	delete accordionData.accordiontitle;
    	delete accordionData.accordionDescription;
    	
    	return Ext.applyIf(accordionData, this.data);
    },
    setData: function(data) {
    	this.getForm().setValues(data);
    	if(!Ext.Object.isEmpty(data)){
    		for(var i = 0; i < data.accordionContent.length; i++){
    			Ext.getCmp('accordionContainer').add(
    					{
	    					xtype: 'container',
	    					layout: 'hbox',
    						items: [
    						        {
    						        	xtype: 'mz-input-text',
    					    			fieldLabel: 'Accordion title',
    					    			name: 'accordiontitle',
    					    			itemId: 'accordiontitle',
    					    			value: data.accordionContent[i].title,
    					    			flex: 2,
    					    			allowBlank: false
    						        },
    						        {
    						        	xtype: 'mz-input-textarea',
    					    			fieldLabel: 'Accordion Description',
    					    			name: 'accordionDescription',
    					    			itemId: 'accordionDescription',
    					    			value: data.accordionContent[i].description,
    					    			style: {
    					    				marginLeft: '10px',
    					    				marginBottom: '30px'
    					    			},
    					    			flex: 2
    						        },
    						        {
    						        	xtype: 'button',
                        			    text: 'Delete',
                        			    name: 'DeleteBtn',
                        			    flex: 1,
                        			    style: {
                        			        marginTop: '30px',
                        			        marginLeft: '10px'
                        			    },
                        			    listeners: {
                        			    	click: function(e){
                        			    		e.up('container').destroy();
                        			    	}
                        			    }
    						        }
					        ]
						}
					);
    		}
    		
    	}
    	this.data = data;
    }
});

