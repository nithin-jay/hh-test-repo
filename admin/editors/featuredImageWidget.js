Ext.widget({
    xtype: 'mz-form-widget',
    items: [ 
        {
        	xtype: 'tabpanel',
        	style: "padding:10px margin-top:10px;",
            tabPosition: 'top',
            tabBar : {
                    layout : {
                        pack : 'end'
                    }
                },
        	items: [
                {
                	xtype: 'panel',
                	layout: 'hbox',
                	title:'Image',
                	items: [
            		        {
					        	xtype: 'panel',
					        	layout: 'vbox',
					        	style: "margin-top:41px;",
					        	items: [
					        	    {
							        	xtype: 'panel',
    						        	layout: 'hbox',
    						        	items: [
                                                {
                                                    xtype: 'mz-input-image',
                                                    fieldLabel: 'Image',
                                                    name: 'Images' 
                                                },
                                                {
                                                    xtype: 'panel',
                                                    layout: 'vbox',
                                                    style: "margin-top:41px;",
                                                    items: [
                                                        {
                                                            xtype: 'panel',
                                                            layout: 'hbox',
                                                            items: [
                                                                {
                                                                    xtype: "mz-input-dropdown",
                                                                    name: "borderWidth",
                                                                    fieldLabel: "Border Width",
                                                                    store: [
                                                                        '1px',
                                                                        '2px',
                                                                        '3px'
                                                                    ],
                                                                    flex: 1,
                                                                    style: {
                                                                            marginLeft: '10px'
                                                                    }
                                                                },
                                                                {
                                                                    xtype: "mz-input-dropdown",
                                                                    name: "borderStyle",
                                                                    fieldLabel: "Border Style",
                                                                    store: [
                                                                        'Solid',
                                                                        'dashed',
                                                                        'Dotted',
                                                                        'None'
                                                                    ],
                                                                    flex: 2,
                                                                    style: {
                                                                            marginLeft: '10px'
                                                                    }
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            xtype: 'panel', 
                                                            layout: 'hbox',
                                                            items: [
                                                                    {
                                                                        xtype: "mz-input-color",
                                                                        name: "borderColor",
                                                                        fieldLabel: "Border Color",
                                                                        flex: 1,
                                                                        style: {
                                                                                marginLeft: '10px'
                                                                        }
                                                                    }
                                                                ]
                                                        },
                                                        {
                                                            xtype: 'panel', 
                                                            layout: 'hbox',
                                                            items: [
                                                                    {
                                                                        xtype: "mz-input-text",
                                                                        name: "imageLink",
                                                                        fieldLabel: "Image Link",
                                                                        flex: 1,
                                                                        style: {
                                                                                marginLeft: '10px'
                                                                        }
                                                                    }
                                                                ]
                                                        }
                                                    ]
                                                }
    						        	    ]
					        	    },
                                    {
							        	xtype: 'panel',
    						        	layout: 'hbox',
    						        	items: [
                                            {
                                                xtype: 'mz-input-image',
                                                fieldLabel: 'Mobile Image',
                                                name: 'mobileImage' 
                                            }
                                        ]
                                    }
					        	]
            		        }
        		        ]
                },
                {
                	xtype: 'panel',
                	layout: 'hbox',
                	title:'Title',
                	items: [
                	        {
					        	xtype: 'panel',
					        	layout: 'vbox',
					        	style: "margin-top:41px;",
					        	items: [
					        	    {
                                        xtype: "mz-input-text",
                                        name: "title",
                                        fieldLabel: "Title",
                                        emptyText: "Please enter title text",
                                        allowBlank:false,
                                        blankText: "This field is required!"
                                    },
					        	    {
							        	xtype: 'panel',
    						        	layout: 'hbox',
    						        	items: [
                                                {
                                                    xtype: "mz-input-dropdown",
                                                    name: "titleFontWeight",
                                                    fieldLabel: "Font Weight",
                                                    store: [
                                                        '100',
                                                        '200',
                                                        '300',
                                                        '400',
                                                        '500',
                                                        '600',
                                                        '700',
                                                        '800',
                                                        '900'
                                                    ],
												    flex: 1,
												    style: {
															marginLeft: '10px'
													}
                                                },
                                                {
                                                    xtype: "mz-input-dropdown",
                                                    name: "titleFontSize",
                                                    fieldLabel: "Font Size",
                                                    store: [
                                                        '100%',
                                                        '150%',
                                                        '200%',
                                                        '250%',
                                                        '300%',
                                                        '350%'
                                                    ],
												    flex: 2,
												    style: {
															marginLeft: '10px'
													}
                                                }
                                                
    						        	 ]
					        	    },
                                    {
							        	xtype: 'panel',
    						        	layout: 'hbox',
    						        	items: [
                                                {
                                                    "xtype": "mz-input-checkbox",
                                                    "name": "titleBlackShadowCheck",
                                                    "fieldLabel": "Black Shadow",
                                                    "hideIf": "titleWhiteShadowCheck",
                                                    flex: 1,
												    style: {
															marginLeft: '10px'
													}
                                                },
                                                {
                                                    "xtype": "mz-input-checkbox",
                                                    "name": "titleWhiteShadowCheck",
                                                    "fieldLabel": "White Shadow",
                                                    "hideIf": "titleBlackShadowCheck",
                                                    flex: 2,
												    style: {
															marginLeft: '10px'
													}
                                                }
                                                
    						        	 ]
					        	    },
                                    {
							        	xtype: 'panel',
    						        	layout: 'hbox',
    						        	items: [
                                                {
                                                    xtype: "mz-input-dropdown",
                                                    name: "titleBlackShadow",
                                                    fieldLabel: "Black Shadow",
                                                    store: [
                                                        'Black Shadow Style 1',
                                                        'Black Shadow Style 2',
                                                    ],
												    flex: 1,
												    style: {
															marginLeft: '10px'
													},
                                                    "showIf": "titleBlackShadowCheck"
                                                },
                                                {
                                                    xtype: "mz-input-dropdown",
                                                    name: "titleWhiteShadow",
                                                    fieldLabel: "White Shadow",
                                                    store: [
                                                        'White Shadow Style 1',
                                                        'White Shadow Style 2',
                                                    ],
												    flex: 2,
												    style: {
															marginLeft: '10px'
													},
                                                    "showIf": "titleWhiteShadowCheck"
                                                }
                                                
    						        	 ]
					        	    }
					        	 ]
            		        }
                	    ]
                },
                {
                	xtype: 'panel',
                	layout: 'hbox',
                	title:'Sub-Title ',
                	items: [
                	        {
					        	xtype: 'panel',
					        	layout: 'vbox',
					        	style: "margin-top:41px;",
					        	items: [
					        	    {
                                        xtype: "mz-input-text",
                                        name: "subTitle",
                                        fieldLabel: "Sub-Title",
                                        emptyText: 'Please enter subTitle Text',
                                    },
					        	    {
							        	xtype: 'panel',
    						        	layout: 'hbox',
    						        	items: [
                                                {
                                                    xtype: "mz-input-dropdown",
                                                    name: "subTitleFontWeight",
                                                    fieldLabel: "Font Weight",
                                                    store: [
                                                        '100',
                                                        '200',
                                                        '300',
                                                        '400',
                                                        '500',
                                                        '600',
                                                        '700',
                                                        '800',
                                                        '900'
                                                    ],
												    flex: 1,
												    style: {
															marginLeft: '10px'
													}
                                                },
                                                {
                                                    xtype: "mz-input-dropdown",
                                                    name: "subTitleFontSize",
                                                    fieldLabel: "Font Size",
                                                    store: [
                                                        '100%',
                                                        '150%',
                                                        '200%',
                                                        '250%',
                                                        '300%',
                                                        '350%'
                                                    ],
												    flex: 2,
												    style: {
															marginLeft: '10px'
													}
                                                }
                                                
    						        	 ]
					        	    },
                                    {
							        	xtype: 'panel',
    						        	layout: 'hbox',
    						        	items: [
                                                {
                                                    "xtype": "mz-input-checkbox",
                                                    "name": "subTitleBlackShadowCheck",
                                                    "fieldLabel": "Black Shadow",
                                                    "hideIf": "subTitleWhiteShadowCheck",
                                                    flex: 1,
												    style: {
															marginLeft: '10px'
													}
                                                },
                                                {
                                                    "xtype": "mz-input-checkbox",
                                                    "name": "subTitleWhiteShadowCheck",
                                                    "fieldLabel": "White Shadow",
                                                    "hideIf": "subTitleBlackShadowCheck",
                                                    flex: 2,
												    style: {
															marginLeft: '10px'
													}
                                                }
                                                
    						        	 ]
					        	    },
                                    {
							        	xtype: 'panel',
    						        	layout: 'hbox',
    						        	items: [
                                                {
                                                    xtype: "mz-input-dropdown",
                                                    name: "subTitleBlackShadow",
                                                    fieldLabel: "Black Shadow",
                                                    store: [
                                                        'Black Shadow Style 1',
                                                        'Black Shadow Style 2',
                                                    ],
												    flex: 1,
												    style: {
															marginLeft: '10px'
													},
                                                    "showIf": "subTitleBlackShadowCheck"
                                                },
                                                {
                                                    xtype: "mz-input-dropdown",
                                                    name: "subTitleWhiteShadow",
                                                    fieldLabel: "White Shadow",
                                                    store: [
                                                        'White Shadow Style 1',
                                                        'White Shadow Style 2',
                                                    ],
												    flex: 2,
												    style: {
															marginLeft: '10px'
													},
                                                    "showIf": "subTitleWhiteShadowCheck"
                                                }
                                                
    						        	 ]
					        	    }
					        	 ]
            		        }
                	    ]
                },
                {
                	xtype: 'panel',
                	layout: 'hbox',
                	title:'CTA-Text ',
                	items: [
                	        {
					        	xtype: 'panel',
					        	layout: 'vbox',
					        	style: "margin-top:41px;",
					        	items: [
					        	    {
                                        xtype: "mz-input-text",
                                        name: "ctaText",
                                        fieldLabel: "CTA Text",
                                        emptyText: 'Please enter button text',
                                        blankText: "This field is required!"
                                    },
					        	    {
							        	xtype: 'panel',
    						        	layout: 'hbox',
    						        	items: [
                                                {
                                                    xtype: "mz-input-dropdown",
                                                    name: "ctaFontWeight",
                                                    fieldLabel: "Font Weight",
                                                    store: [
                                                        '100',
                                                        '200',
                                                        '300',
                                                        '400',
                                                        '500',
                                                        '600',
                                                        '700',
                                                        '800',
                                                        '900'
                                                    ],
												    flex: 1,
												    style: {
															marginLeft: '10px'
													}
                                                }
                                                
    						        	 ]
					        	    },
                                    {
							        	xtype: 'panel',
    						        	layout: 'hbox',
    						        	items: [
                                                {
                                                    xtype: "mz-input-color",
                                                    name: "ctaTextColor",
                                                    fieldLabel: "CTA Text Color",
                                                    flex: 1,
                                                    style: {
                                                            marginLeft: '10px'
                                                    }
                                                },
                                                {
                                                    xtype: "mz-input-color",
                                                    name: "ctaBackgroundColor",
                                                    fieldLabel: "CTA Background Color",
                                                    flex: 1,
                                                    style: {
                                                            marginLeft: '10px'
                                                    }
                                                },
                                                {
                                                    xtype: "mz-input-color",
                                                    name: "ctaBorderColor",
                                                    fieldLabel: "CTA Border Color",
                                                    flex: 1,
                                                    style: {
                                                            marginLeft: '10px'
                                                    }
                                                }
                                                
    						        	 ]
					        	    }
					        	 ]
            		        }
                	    ]
                },
                {
                	xtype: 'panel',
                	layout: 'hbox',
                	title:'Style',
                	items: [
                	        {
					        	xtype: 'panel',
					        	layout: 'vbox',
					        	style: "margin-top:41px;",
					        	items: [
					        	    {
                                        xtype: "mz-input-color", 
                                        name: "color",
                                        fieldLabel: "Color",
                                        flex: 1,
                                        style: {
                                                marginLeft: '10px'
                                        }
                                    },
                                    {
                                        xtype: "mz-input-dropdown",
                                        name: "textPosition",
                                        fieldLabel: "Text Position",
                                        store: [
                                            'Top Left',
                                            'Top Right',
                                            'Top Center',
                                            'Middle Left',
                                            'Middle Right',
                                            'Center',
                                            'Bottom Left',
                                            'Bottom Right',
                                            'Bottom Center'
                                        ],
                                        flex: 2,
                                        style: {
                                                marginLeft: '10px'
                                        }
                                    }
					        	 ]
            		        }
                	    ]
                },
            ]
        }
    ]
});