Ext.widget({
	xtype: 'mz-form-widget',
	itemId: 'configurableMessageForm',
	items: [
	        {
			xtype: 'container',
			id: 'configurableMessageContainer',
			items: [
			        {
			    	 xtype: 'panel',
			    	 layout: 'hbox',
			    	 items: [ 
						   {
		                    	xtype: 'mz-input-number',
		            			fieldLabel: 'Number of Province',
		            			id: 'noOfFields',
		            			name: 'NoOfFields',
		            			value: 1
		                    },
		                    {
		                    	xtype: 'button',
		        			    text: 'Add',
		        			    name: 'AddBtn',
		        			    style: {
		        			        marginTop: '30px',
		        			        marginLeft: '10px'
		        			    }, 
		        			    listeners: {
		        			    	click: function(e){
		        			    		var noOfFields = Ext.getCmp('noOfFields').getValue();
		        			    		
		        			    		for(var i=0; i<noOfFields; i++){
		        			    			e.up('form').add(
		        			    				{
	            			    					xtype: 'container',
	        			    						items: [
        			    						        {
        			    						        	xtype: 'panel',
        			    						        	layout: 'hbox',
        			    						        	items: [
																{
                                                                    "xtype": "mz-input-text",
                                                                    "name": "minimumOrderAmount",
                                                                    "fieldLabel": "Minimum Order Amount(without $)",
																	allowBlank: false,
																	style: {
																		marginTop: '30px'
																	}
                                                                },
                                                                {
                                                                    "xtype": "mz-input-dropdown",
                                                                    "name": "province",
                                                                    "fieldLabel": "Province",
																	allowBlank: false,
																	style: {
																		marginLeft: '10px',
																		marginTop: '30px'
																	},
                                                                    "store": [
                                                                        [
                                                                            "AB",
                                                                            "Alberta"
                                                                        ],
                                                                        [
                                                                            "BC",
                                                                            "British Columbia"
                                                                        ],
																		[
																			"MB",
																			"Manitoba"
																		],
																		[
																			"NB",
																			"New Brunswick"
																		],
																		[
																			"NL",
																			"Newfoundland and Labrador"
																		],
																		[
																			"NT",
																			"Northwest Territories"
																		],
																		[
																			"NS",
																			"Nova Scotia"
																		],
																		[
																			"NU",
																			"Nunavut"
																		],
																		[
																			"ON",
																			"Ontario"
																		],
																		[
																			"PE",
																			"Prince Edward Island"
																		],
																		[
																			"QC",
																			"Quebec"
																		],
																		[
																			"SK",
																			"Saskatchewan"
																		],
																		[
																			"YT",
																			"Yukon"
																		]
                                                                    ]
                                                                },
																{
																xtype: 'button',
																text: 'Delete',
																name: 'DeleteBtn',
																style: {
																	marginTop: '65px',
																	marginLeft:'10px'
																},
																listeners: {
																	click: function(e){
																		e.up('panel').up('container').destroy();
																	}
																}
															}
                                                            ]
                                                        },
														{
															xtype: 'panel',
        			    						        	layout: 'hbox',
        			    						        	items: [
																{
																	xtype: 'panel',
																	layout: 'hbox',
																	items: [
																		{
																			"xtype": "mz-input-textarea",
																			"name": "pdpOrderVolumeMsg",
																			"fieldLabel": "PDP Order Volume Message(PDP Message Block)",
																			allowBlank: false,
																			style: {
																				marginTop: '30px',
																				width:"60%"
																			}
																		},
																		{
																			"xtype": "mz-input-textarea",
																			"name": "cartPageMessage",
																			"fieldLabel": "Minimum Order Message(cart page)",
                                                                            allowBlank: false,
																			style: {
																				marginTop: '30px',
																				width:"60%",
																				marginLeft: '10px',
																			}
																		}
																	]
																}
															]
														}
                                                    ]
                                                }
		        			    			);
		        			    		}
		        			    	} 
		        			    }
		                    }
						]
			        }
		        ]
	        } 
        ],
        getData: function(){
        	var msgData = this.getValues();
			//debugger;
			if(msgData.NoOfFields && typeof msgData.province === 'object' && msgData.province.length > 1){
				var out = msgData.province.map(function(item, index){
					return {minimumOrderAmount: msgData.minimumOrderAmount[index], province: msgData.province[index], cartPageMessage: msgData.cartPageMessage[index],pdpOrderVolumeMsg: msgData.pdpOrderVolumeMsg[index] };
				});
				msgData.msgContent = out;
			}else{
				msgData.msgContent = [];
				msgData.msgContent.push({minimumOrderAmount: msgData.minimumOrderAmount, province: msgData.province, cartPageMessage: msgData.cartPageMessage,pdpOrderVolumeMsg: msgData.pdpOrderVolumeMsg })
			}
			delete msgData.minimumOrderAmount;
			delete msgData.province;
			delete msgData.cartPageMessage;
			delete msgData.pdpOrderVolumeMsg;

        	return Ext.applyIf(msgData, this.data);
        },
        setData: function(data){
			this.getForm().setValues(data);
			//debugger;
			if(!Ext.Object.isEmpty(data)){
				for(var i = 0; i < data.msgContent.length; i++){
					Ext.getCmp('configurableMessageContainer').add(
						{
							xtype: 'container',
							items: [
								{
									xtype: 'panel',
									layout: 'hbox',
									items: [
										{
											"xtype": "mz-input-text",
											"name": "minimumOrderAmount",
											"fieldLabel": "Minimum Order Amount(without $)",
											"value": data.msgContent[i].minimumOrderAmount,
											allowBlank: false,
											style: {
												marginTop: '30px'
											}
										},
										{
											"xtype": "mz-input-dropdown",
											"name": "province",
											"fieldLabel": "Province",
											"value": data.msgContent[i].province,
											allowBlank: false,
											style: {
												marginLeft: '10px',
												marginTop: '30px'
											},
											//use setValue callback to have unique constraint for provinces.
											"store": [
												[
													"AB",
													"Alberta"
												],
												[
													"BC",
													"British Columbia"
												],
												[
													"MB",
													"Manitoba"
												],
												[
													"NB",
													"New Brunswick"
												],
												[
													"NL",
													"Newfoundland and Labrador"
												],
												[
													"NT",
													"Northwest Territories"
												],
												[
													"NS",
													"Nova Scotia"
												],
												[
													"NU",
													"Nunavut"
												],
												[
													"ON",
													"Ontario"
												],
												[
													"PE",
													"Prince Edward Island"
												],
												[
													"QC",
													"Quebec"
												],
												[
													"SK",
													"Saskatchewan"
												],
												[
													"YT",
													"Yukon"
												]
											]
										},
										{
										xtype: 'button',
										text: 'Delete',
										name: 'DeleteBtn',
										style: {
											marginTop: '65px',
											marginLeft:'10px'
										},
										listeners: {
											click: function(e){
												e.up('panel').up('container').destroy();
											}
										}
									}
									]
								},
								{
									xtype: 'panel',
									layout: 'hbox',
									items: [
										{
											xtype: 'panel',
											layout: 'hbox',
											items: [
												{
													"xtype": "mz-input-textarea",
													"name": "pdpOrderVolumeMsg",
													"fieldLabel": "PDP Order Volume Message(PDP Message Block)",
													"value": data.msgContent[i].pdpOrderVolumeMsg,
													allowBlank: false,
													style: {
														marginTop: '30px',
														width:"60%",
														marginLeft: '10px',
													}
												},
												{
													"xtype": "mz-input-textarea",
													"name": "cartPageMessage",
													"fieldLabel": "Minimum Order Message(cart page)",
													"value": data.msgContent[i].cartPageMessage,
													allowBlank: false,
													style: {
														marginTop: '30px',
														width:"60%",
														marginLeft: '10px',
													}
												}
											]
										}
									]
								}
							]
						}
					);
				}
				
			}
			this.data = data;
        }
});