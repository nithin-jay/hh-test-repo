Ext.widget({
    xtype: 'mz-form-widget',
    items: [ 
        {
			xtype: 'container',
			id: 'mainContainer',
			items: [
                {
                    name: "sectionHeader",
                    fieldLabel: "Section Header",
                    xtype: "mz-input-text",
                    flex: 1,
                    style: {
                        marginLeft: '10px'
                    }
                },
                {
                    xtype: 'container',
                    style: {
                        background: '#ccb',
                        padding: '10px',
                        marginLeft: '30px',
                        marginTop: '30px'
                    },
                    items: [
                        {
                            xtype: 'mz-input-color',
                            fieldLabel: 'Background Color',
                            name: 'backgroundColor',
                            value: "rgba(255, 255, 255, 1)"
                        },
                        {
                            xtype: 'panel',
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'mz-input-image-nostyle',
                                    fieldLabel: 'Desktop Image',
                                    allowBlank: false,
                                    name: 'Images'
                                },
                                {
                                    xtype: 'panel',
                                    layout: 'vbox',
                                    items: [
                                        {
                                            xtype: 'panel',
                                            layout: 'hbox',
                                            items: [
                                                {
                                                    name: "topLineColour",
                                                    fieldLabel: "Top Line Colour",
                                                    xtype: "mz-input-color",
                                                    allowBlank: false,
                                                    value: "rgba(228, 0, 43, 1)",
                                                    flex: 1,
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                },
                                                {
                                                    name: "textColour",
                                                    fieldLabel: "Text Colour",
                                                    xtype: "mz-input-color",
                                                    value: "rgba(0,0,0,1)",
                                                    flex: 1,
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'panel',
                                            layout: 'hbox',
                                            width: 500,
                                            items: [
                                                {
                                                    name: "title",
                                                    fieldLabel: "Title/HeadLine",
                                                    xtype: "mz-input-text",
                                                    flex: 1,
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                },
                                                {
                                                    name: "subTitle",
                                                    fieldLabel: "Sub-head Copy",
                                                    xtype: "mz-input-text",
                                                    flex: 1,
                                                    store: [
                                                        'Left',
                                                        'Right'
                                                    ],
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'panel',
                                            layout: 'hbox',
                                            width: 500,
                                            items: [
                                                {
                                                    name: "saveStory",
                                                    fieldLabel: "Save Story",
                                                    xtype: "mz-input-text",
                                                    flex: 1,
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                },
                                                {
                                                    name: "backgroundImageAlignment",
                                                    fieldLabel: "background Image Alignment",
                                                    xtype: "mz-input-dropdown",
                                                    flex: 1,
                                                    store:[
                                                        "East",
                                                        "West",
                                                        "North",
                                                        "Center",
                                                        "South",
                                                        "NorthEast",
                                                        "NorthWest",
                                                        "SouthEast",
                                                        "SouthWest",
                                                    ],
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: 'hbox',
                            items: [
                                {
                                    name: "ctaTextField",
                                    fieldLabel: "CTA Text",
                                    xtype: "mz-input-text",
                                    allowBlank: false,
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "ctaLink",
                                    fieldLabel: "CTA link",
                                    xtype: "mz-input-text",
                                    allowBlank: false,
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "ctaTextColor",
                                    fieldLabel: "CTA Text Color",
                                    xtype: "mz-input-color",
                                    allowBlank: false,
                                    value: "rgba(228, 0, 43, 1)",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "ctaBorderColor",
                                    fieldLabel: "CTA Border Color",
                                    xtype: "mz-input-color",
                                    value: "rgba(228, 0, 43, 1)",
                                    allowBlank: false,
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: 'hbox',
                            items: [
                                {
                                    name: "logoOneImage",
                                    fieldLabel: "Logo one image",
                                    xtype: "mz-input-image-nostyle",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "logoTwoImage",
                                    fieldLabel: "Logo Two image",
                                    xtype: "mz-input-image-nostyle",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "logoPosition",
                                    fieldLabel: "Logo Position",
                                    xtype: "mz-input-dropdown",
                                    store:[
                                        "Left",
                                        "Center"
                                    ],
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'mz-input-image',
                                    fieldLabel: 'Mobile Image',
                                    name: 'mobImages'
                                },
                                {
                                    name: "productPrice",
                                    fieldLabel: "Product Price",
                                    xtype: "mz-input-text",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "oldPrice",
                                    fieldLabel: "Old Price",
                                    xtype: "mz-input-text",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    style: {
                        background: '#ccb',
                        padding: '10px',
                        marginLeft: '30px',
                        marginTop: '30px'
                    },
                    items: [
                        {
                            xtype: 'mz-input-color',
                            fieldLabel: 'Background Color',
                            name: 'backgroundColor',
                            value: "rgba(255, 255, 255, 1)",
                        },
                        {
                            xtype: 'panel',
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'mz-input-image-nostyle',
                                    fieldLabel: 'Desktop Image',
                                    name: 'Images'
                                },
                                {
                                    xtype: 'panel',
                                    layout: 'vbox',
                                    items: [
                                        {
                                            xtype: 'panel',
                                            layout: 'hbox',
                                            items: [
                                                {
                                                    name: "topLineColour",
                                                    fieldLabel: "Top Line Colour",
                                                    xtype: "mz-input-color",
                                                    value: "rgba(228, 0, 43, 1)",
                                                    flex: 1,
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                },
                                                {
                                                    name: "textColour",
                                                    fieldLabel: "Text Colour",
                                                    xtype: "mz-input-color",
                                                    value: "rgba(0,0,0,1)",
                                                    flex: 1,
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'panel',
                                            layout: 'hbox',
                                            width: 500,
                                            items: [
                                                {
                                                    name: "title",
                                                    fieldLabel: "Title/HeadLine",
                                                    xtype: "mz-input-text",
                                                    flex: 1,
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                },
                                                {
                                                    name: "subTitle",
                                                    fieldLabel: "Sub-head Copy",
                                                    xtype: "mz-input-text",
                                                    flex: 1,
                                                    store: [
                                                        'Left',
                                                        'Right'
                                                    ],
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'panel',
                                            layout: 'hbox',
                                            width: 500,
                                            items: [
                                                {
                                                    name: "saveStory",
                                                    fieldLabel: "Save Story",
                                                    xtype: "mz-input-text",
                                                    flex: 1,
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                },
                                                {
                                                    name: "backgroundImageAlignment",
                                                    fieldLabel: "background Image Alignment",
                                                    xtype: "mz-input-dropdown",
                                                    flex: 1,
                                                    store:[
                                                        "East",
                                                        "West",
                                                        "North",
                                                        "South",
                                                        "Center",
                                                        "NorthEast",
                                                        "NorthWest",
                                                        "SouthEast",
                                                        "SouthWest",
                                                    ],
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: 'hbox',
                            items: [
                                {
                                    name: "ctaTextField",
                                    fieldLabel: "CTA Text",
                                    xtype: "mz-input-text",
                                    allowBlank: false,
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "ctaLink",
                                    fieldLabel: "CTA link",
                                    xtype: "mz-input-text",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "ctaTextColor",
                                    fieldLabel: "CTA Text Color",
                                    xtype: "mz-input-color",
                                    value: "rgba(228, 0, 43, 1)",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "ctaBorderColor",
                                    fieldLabel: "CTA Border Color",
                                    value: "rgba(228, 0, 43, 1)",
                                    xtype: "mz-input-color",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: 'hbox',
                            items: [
                                {
                                    name: "logoOneImage",
                                    fieldLabel: "Logo one image",
                                    xtype: "mz-input-image-nostyle",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "logoTwoImage",
                                    fieldLabel: "Logo Two image",
                                    xtype: "mz-input-image-nostyle",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "logoPosition",
                                    fieldLabel: "Logo Position",
                                    xtype: "mz-input-dropdown",
                                    store:[
                                        "Left",
                                        "Center"
                                    ],
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'mz-input-image',
                                    fieldLabel: 'Mobile Image',
                                    name: 'mobImages'
                                },
                                {
                                    name: "productPrice",
                                    fieldLabel: "Product Price",
                                    xtype: "mz-input-text",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "oldPrice",
                                    fieldLabel: "Old Price",
                                    xtype: "mz-input-text",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }
        
    ],

    getData: function () {
		var sliderData = this.getValues();
		if (!Ext.isArray(sliderData.Images)) {  //To work with only one image
			sliderData.Images = [sliderData.Images];
		}
		if (!Ext.isArray(sliderData.mobImages)) {  //To work with only one image
			sliderData.mobImages = [sliderData.mobImages];
		}
		//Removed null from array
		sliderData.Images = sliderData.Images.filter(function (el) {
			return el != null;
		});
		//Removed null from array
		// sliderData.mobImages = sliderData.mobImages.filter(function (el) {
		// 	return el != null;
		// });

		Ext.each(sliderData.Images, function (item, index) {
			if(sliderData.mobImages[index] == null) {
				sliderData.mobImages[index] = {};
				sliderData.mobImages[index].imageUrl= sliderData.Images[index].imageUrl;
				sliderData.mobImages[index].altText="";
			}
			if (sliderData.backgroundColor) {
				if (Ext.isArray(sliderData.backgroundColor)) {
                    if (sliderData.backgroundColor[index] === "rgba(0, 0, 0, 1)"){
                        item.backgroundColor = "rgba(255, 255, 255, 1)";
                    } else{
                        item.backgroundColor = sliderData.backgroundColor[index];
                    }
				} else {
					item.backgroundColor = sliderData.backgroundColor;
				}
			} else {
				item.backgroundColor = "rgba(255, 255, 255, 1)";
			}
			if (sliderData.ctaBorderColor) {
				if (Ext.isArray(sliderData.ctaBorderColor)) {
					item.ctaBorderColor = sliderData.ctaBorderColor[index];
				} else {
					item.ctaBorderColor = sliderData.ctaBorderColor;
				}
			} else {
				item.ctaBorderColor = 'rgba(255, 211, 13, 1)';
			}
			if (sliderData.ctaLink) {
				if (Ext.isArray(sliderData.ctaLink)) {
					item.ctaLink = sliderData.ctaLink[index];
				} else {
					item.ctaLink = sliderData.ctaLink;
				}
			} else {
				item.ctaLink = '';
			}
			if (sliderData.ctaTextColor) {
				if (Ext.isArray(sliderData.ctaTextColor)) {
					item.ctaTextColor = sliderData.ctaTextColor[index];
				} else {
					item.ctaTextColor = sliderData.ctaTextColor;
				}
			} else {
				item.ctaTextColor = "rgba(228, 0, 43, 1)";
			}
			if (sliderData.ctaTextField) {
				if (Ext.isArray(sliderData.ctaTextField)) {
					item.ctaTextField = sliderData.ctaTextField[index];
				} else {
					item.ctaTextField = sliderData.ctaTextField;
				}
			} else {
				item.ctaTextField = '';
			}
            if (sliderData.productPrice) {
				if (Ext.isArray(sliderData.productPrice)) {
					item.productPrice = sliderData.productPrice[index];
				} else {
					item.productPrice = sliderData.productPrice;
				}
			} else {
				item.productPrice = '';
			}
            if (sliderData.oldPrice) {
				if (Ext.isArray(sliderData.oldPrice)) {
					item.oldPrice = sliderData.oldPrice[index];
				} else {
					item.oldPrice = sliderData.oldPrice;
				}
			} else {
				item.oldPrice = '';
			}
			if (sliderData.logoOneImage) {
				if (Ext.isArray(sliderData.logoOneImage)) {
					item.logoOneImage = sliderData.logoOneImage[index];
				} else {
					item.logoOneImage = sliderData.logoOneImage;
				}
			} else {
				item.logoOneImage = '';
			}
			if (sliderData.logoPosition) {
				if (Ext.isArray(sliderData.logoPosition)) {
					item.logoPosition = sliderData.logoPosition[index];
				} else {
					item.logoPosition = sliderData.logoPosition;
				}
			} else {
				item.logoPosition = 'Left';
			}
			if (sliderData.logoTwoImage) {
				if (Ext.isArray(sliderData.logoTwoImage)) {
					item.logoTwoImage = sliderData.logoTwoImage[index];
				} else {
					item.logoTwoImage = sliderData.logoTwoImage;
				}
			} else {
				item.logoTwoImage = '';
			}
            if (sliderData.saveStory) {
				if (Ext.isArray(sliderData.saveStory)) {
					item.saveStory = sliderData.saveStory[index];
				} else {
					item.saveStory = sliderData.saveStory;
				}
			} else {
				item.saveStory = '';
			}
            if (sliderData.backgroundImageAlignment) {
				if (Ext.isArray(sliderData.backgroundImageAlignment)) {
					item.backgroundImageAlignment = sliderData.backgroundImageAlignment[index];
				} else {
					item.backgroundImageAlignment = sliderData.backgroundImageAlignment;
				}
			} else {
				item.backgroundImageAlignment = 'Center';
			}
            if (sliderData.subTitle) {
				if (Ext.isArray(sliderData.subTitle)) {
					item.subTitle = sliderData.subTitle[index];
				} else {
					item.subTitle = sliderData.subTitle;
				}
			} else {
				item.subTitle = '';
			}
            if (sliderData.title) {
				if (Ext.isArray(sliderData.title)) {
					item.title = sliderData.title[index];
				} else {
					item.subTitle = sliderData.title;
				}
			} else {
				item.title = '';
			}
            if (sliderData.topLineColour) {
				if (Ext.isArray(sliderData.topLineColour)) {
					item.topLineColour = sliderData.topLineColour[index];
				} else {
					item.topLineColour = sliderData.topLineColour;
				}
			} else {
				item.topLineColour = '';
			}
            if (sliderData.textColour) {
				if (Ext.isArray(sliderData.textColour)) {
					item.textColour = sliderData.textColour[index];
				} else {
					item.textColour = sliderData.textColour;
				}
			} else {
				item.textColour = '';
			}
		});

		Ext.each(sliderData.mobImages, function (item, index) {
			
			if (sliderData.mobWidth) {
				if (Ext.isArray(sliderData.mobWidth)) {
					sliderData.mobImages[index].mobWidth = sliderData.mobWidth[index];
				} else {
					sliderData.mobImages[index].mobWidth = sliderData.mobWidth;
				}
			}
			if (sliderData.backgroundColor) {
				if (Ext.isArray(sliderData.backgroundColor)) {
					sliderData.mobImages[index].backgroundColor = sliderData.backgroundColor[index];
				} else {
					sliderData.mobImages[index].backgroundColor = sliderData.backgroundColor;
				}
			} else {
				sliderData.mobImages[index].backgroundColor = "rgb(255,255,255,1)";
			}
			if (sliderData.ctaBorderColor) {
				if (Ext.isArray(sliderData.ctaBorderColor)) {
					sliderData.mobImages[index].ctaBorderColor = sliderData.ctaBorderColor[index];
				} else {
					sliderData.mobImages[index].ctaBorderColor = sliderData.ctaBorderColor;
				}
			} else {
				sliderData.mobImages[index].ctaBorderColor = 'rgba(255, 211, 13, 1)';
			}
			if (sliderData.ctaLink) {
				if (Ext.isArray(sliderData.ctaLink)) {
					sliderData.mobImages[index].ctaLink = sliderData.ctaLink[index];
				} else {
					sliderData.mobImages[index].ctaLink = sliderData.ctaLink;
				}
			} else {
				sliderData.mobImages[index].ctaLink = '';
			}
            if (sliderData.productPrice) {
				if (Ext.isArray(sliderData.productPrice)) {
					sliderData.mobImages[index].productPrice = sliderData.productPrice[index];
				} else {
					sliderData.mobImages[index].productPrice = sliderData.productPrice;
				}
			} else {
				sliderData.mobImages[index].productPrice = '';
			}
            if (sliderData.oldPrice) {
				if (Ext.isArray(sliderData.oldPrice)) {
					sliderData.mobImages[index].oldPrice = sliderData.oldPrice[index];
				} else {
					sliderData.mobImages[index].oldPrice = sliderData.oldPrice;
				}
			} else {
				sliderData.mobImages[index].oldPrice = '';
			}
			if (sliderData.ctaTextColor) {
				if (Ext.isArray(sliderData.ctaTextColor)) {
					sliderData.mobImages[index].ctaTextColor = sliderData.ctaTextColor[index];
				} else {
					sliderData.mobImages[index].ctaTextColor = sliderData.ctaTextColor;
				}
			} else {
				sliderData.mobImages[index].ctaTextColor = "rgba(228, 0, 43, 1)";
			}
			if (sliderData.ctaTextField) {
				if (Ext.isArray(sliderData.ctaTextField)) {
					sliderData.mobImages[index].ctaTextField = sliderData.ctaTextField[index];
				} else {
					sliderData.mobImages[index].ctaTextField = sliderData.ctaTextField;
				}
			} else {
				sliderData.mobImages[index].ctaTextField = '';
			}
			if (sliderData.logoOneImage) {
				if (Ext.isArray(sliderData.logoOneImage)) {
					sliderData.mobImages[index].logoOneImage = sliderData.logoOneImage[index];
				} else {
					sliderData.mobImages[index].logoOneImage = sliderData.logoOneImage;
				}
			} else {
				sliderData.mobImages[index].logoOneImage = '';
			}
			if (sliderData.logoPosition) {
				if (Ext.isArray(sliderData.logoPosition)) {
					sliderData.mobImages[index].logoPosition = sliderData.logoPosition[index];
				} else {
					sliderData.mobImages[index].logoPosition = sliderData.logoPosition;
				}
			} else {
				sliderData.mobImages[index].logoPosition = 'Left';
			}
			if (sliderData.logoTwoImage) {
				if (Ext.isArray(sliderData.logoTwoImage)) {
					sliderData.mobImages[index].logoTwoImage = sliderData.logoTwoImage[index];
				} else {
					sliderData.mobImages[index].logoTwoImage = sliderData.logoTwoImage;
				}
			} else {
				sliderData.mobImages[index].logoTwoImage = '';
			}
            if (sliderData.saveStory) {
				if (Ext.isArray(sliderData.saveStory)) {
					sliderData.mobImages[index].saveStory = sliderData.saveStory[index];
				} else {
					sliderData.mobImages[index].saveStory = sliderData.saveStory;
				}
			} else {
				sliderData.mobImages[index].saveStory = '';
			}
            if (sliderData.backgroundImageAlignment) {
				if (Ext.isArray(sliderData.backgroundImageAlignment)) {
					sliderData.mobImages[index].backgroundImageAlignment = sliderData.backgroundImageAlignment[index];
				} else {
					sliderData.mobImages[index].backgroundImageAlignment = sliderData.backgroundImageAlignment;
				}
			} else {
				sliderData.mobImages[index].backgroundImageAlignment = 'Center';
			}
            if (sliderData.subTitle) {
				if (Ext.isArray(sliderData.subTitle)) {
					sliderData.mobImages[index].subTitle = sliderData.subTitle[index];
				} else {
					sliderData.mobImages[index].subTitle = sliderData.subTitle;
				}
			} else {
				sliderData.mobImages[index].subTitle = '';
			}
            if (sliderData.title) {
				if (Ext.isArray(sliderData.title)) {
					sliderData.mobImages[index].title = sliderData.title[index];
				} else {
					sliderData.mobImages[index].subTitle = sliderData.title;
				}
			} else {
				sliderData.mobImages[index].title = '';
			}
            if (sliderData.topLineColour) {
				if (Ext.isArray(sliderData.topLineColour)) {
					sliderData.mobImages[index].topLineColour = sliderData.topLineColour[index];
				} else {
					sliderData.mobImages[index].topLineColour = sliderData.topLineColour;
				}
			} else {
				sliderData.mobImages[index].topLineColour = '';
			}
            if (sliderData.textColour) {
				if (Ext.isArray(sliderData.textColour)) {
					sliderData.mobImages[index].textColour = sliderData.textColour[index];
				} else {
					sliderData.mobImages[index].textColour = sliderData.textColour;
				}
			} else {
				sliderData.mobImages[index].textColour = '';
			}
		});

        delete sliderData.backgroundColor;
		delete sliderData.ctaBorderColor;
		delete sliderData.ctaLink;
        delete sliderData.ctaTextColor;
		delete sliderData.ctaTextField;
		delete sliderData.logoOneImage;
		delete sliderData.logoPosition;
		delete sliderData.logoTwoImage;
		delete sliderData.saveStory;
		delete sliderData.subTitle;
		delete sliderData.title;
		delete sliderData.topLineColour;
        delete sliderData.textColour;
        delete sliderData.backgroundImageAlignment;

		return Ext.applyIf(sliderData, this.data);
	},

    setData: function (data) {
		 this.getForm().setValues(data);
         var self = this;
		if (!Ext.Object.isEmpty(data)) {
			for (var i = 0; i < data.Images.length; i++) {
                Ext.destroy(self.down("container").down("container"));
				Ext.getCmp('mainContainer').add(
					{
                        xtype: 'container',
                        style: {
                            background: '#ccb',
                            padding: '10px',
                            marginLeft: '30px',
                            marginTop: '30px'
                        },
                        items: [
                            {
                                xtype: 'mz-input-color',
                                fieldLabel: 'Background Color',
                                name: 'backgroundColor',
                                value: data.Images[i].backgroundColor
                            },
                            {
                                xtype: 'panel',
                                layout: 'hbox',
                                items: [
                                    {
                                        xtype: 'mz-input-image-nostyle',
                                        fieldLabel: 'Desktop Image',
                                        allowBlank: false,
                                        name: 'Images',
                                        value: data.Images[i]
                                    },
                                    {
                                        xtype: 'panel',
                                        layout: 'vbox',
                                        items: [
                                            {
                                                xtype: 'panel',
                                                layout: 'hbox',
                                                items: [
                                                    {
                                                        name: "topLineColour",
                                                        fieldLabel: "Top Line Colour",
                                                        xtype: "mz-input-color",
                                                        allowBlank: false,
                                                        value: data.Images[i].topLineColour,
                                                        flex: 1,
                                                        style: {
                                                            marginLeft: '10px'
                                                        }
                                                    },
                                                    {
                                                        name: "textColour",
                                                        fieldLabel: "Text Colour",
                                                        xtype: "mz-input-color",
                                                        value: data.Images[i].textColour,
                                                        flex: 1,
                                                        style: {
                                                            marginLeft: '10px'
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: 'panel',
                                                layout: 'hbox',
                                                width: 500,
                                                items: [
                                                    {
                                                        name: "title",
                                                        fieldLabel: "Title/HeadLine",
                                                        xtype: "mz-input-text",
                                                        value: data.Images[i].title,
                                                        flex: 1,
                                                        style: {
                                                            marginLeft: '10px'
                                                        }
                                                    },
                                                    {
                                                        name: "subTitle",
                                                        fieldLabel: "Sub-head Copy",
                                                        xtype: "mz-input-text",
                                                        value: data.Images[i].subTitle,
                                                        flex: 1,
                                                        store: [
                                                            'Left',
                                                            'Right'
                                                        ],
                                                        style: {
                                                            marginLeft: '10px'
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: 'panel',
                                                layout: 'hbox',
                                                width: 500,
                                                items: [
                                                    {
                                                        name: "saveStory",
                                                        fieldLabel: "Save Story",
                                                        xtype: "mz-input-text",
                                                        value: data.Images[i].saveStory,
                                                        flex: 1,
                                                        style: {
                                                            marginLeft: '10px'
                                                        }
                                                    },
                                                    {
                                                        name: "backgroundImageAlignment",
                                                        fieldLabel: "background Image Alignment",
                                                        xtype: "mz-input-dropdown",
                                                        value: data.Images[i].backgroundImageAlignment,
                                                        flex: 1,
                                                        store:[
                                                            "East",
                                                            "West",
                                                            "North",
                                                            "South",
                                                            "Center",
                                                            "NorthEast",
                                                            "NorthWest",
                                                            "SouthEast",
                                                            "SouthWest",
                                                        ],
                                                        style: {
                                                            marginLeft: '10px'
                                                        }
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                xtype: 'panel',
                                layout: 'hbox',
                                items: [
                                    {
                                        name: "ctaTextField",
                                        fieldLabel: "CTA Text",
                                        xtype: "mz-input-text",
                                        value: data.Images[i].ctaTextField,
                                        flex: 1,
                                        allowBlank: false,
                                        style: {
                                            marginLeft: '10px'
                                        }
                                    },
                                    {
                                        name: "ctaLink",
                                        fieldLabel: "CTA link",
                                        xtype: "mz-input-text",
                                        allowBlank: false,
                                        value: data.Images[i].ctaLink,
                                        flex: 1,
                                        style: {
                                            marginLeft: '10px'
                                        }
                                    },
                                    {
                                        name: "ctaTextColor",
                                        fieldLabel: "CTA Text Color",
                                        xtype: "mz-input-color",
                                        allowBlank: false,
                                        value: data.Images[i].ctaTextColor,
                                        flex: 1,
                                        style: {
                                            marginLeft: '10px'
                                        }
                                    },
                                    {
                                        name: "ctaBorderColor",
                                        fieldLabel: "CTA Border Color",
                                        xtype: "mz-input-color",
                                        allowBlank: false,
                                        value: data.Images[i].ctaBorderColor,
                                        flex: 1,
                                        style: {
                                            marginLeft: '10px'
                                        }
                                    }
                                ]
                            },
                            {
                                xtype: 'panel',
                                layout: 'hbox',
                                items: [
                                    {
                                        name: "logoOneImage",
                                        fieldLabel: "Logo one image",
                                        xtype: "mz-input-image-nostyle",
                                        value: data.Images[i].logoOneImage,
                                        flex: 1,
                                        style: {
                                            marginLeft: '10px'
                                        }
                                    },
                                    {
                                        name: "logoTwoImage",
                                        fieldLabel: "Logo Two image",
                                        xtype: "mz-input-image-nostyle",
                                        value: data.Images[i].logoTwoImage,
                                        flex: 1,
                                        style: {
                                            marginLeft: '10px'
                                        }
                                    },
                                    {
                                        name: "logoPosition",
                                        fieldLabel: "Logo Position",
                                        xtype: "mz-input-dropdown",
                                        value: data.Images[i].logoPosition,
                                        store:[
                                            "Left",
                                            "Center"
                                        ],
                                        flex: 1,
                                        style: {
                                            marginLeft: '10px'
                                        }
                                    }
                                ]
                            },
                            {
                                xtype: 'panel',
                                layout: 'hbox',
                                items: [
                                    {
                                        xtype: 'mz-input-image',
                                        fieldLabel: 'Mobile Image',
                                        name: 'mobImages',
                                        value: data.mobImages[i]
                                    },
                                    {
                                        name: "productPrice",
                                        fieldLabel: "Product Price",
                                        xtype: "mz-input-text",
                                        value: data.Images[i].productPrice,
                                        flex: 1,
                                        style: {
                                            marginLeft: '10px'
                                        }
                                    },
                                    {
                                        name: "oldPrice",
                                        fieldLabel: "Old Price",
                                        xtype: "mz-input-text",
                                        value: data.Images[i].oldPrice,
                                        flex: 1,
                                        style: {
                                            marginLeft: '10px'
                                        }
                                    }
                                ]
                            }
                        ]
                    }
				);
			}
		}
		this.data = data;
	}
});