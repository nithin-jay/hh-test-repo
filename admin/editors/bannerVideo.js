Ext.widget({
    xtype: 'mz-form-widget',
	itemId: 'footerForm',
    items: [
        {
            xtype: 'container',
            id: 'mainContainer',
            items:[
                {
                    xtype: 'panel',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'mz-input-text',
                            fieldLabel: 'Default Video Link',
                            name: 'DefaultBannerVideoLink',
                            itemId: 'DefaultBannerVideoLinkId',
                            flex: 2,
                            style: {
                                marginLeft: '15px'
                            },
                            allowBlank: false
                        },
                        {
                            xtype: 'mz-input-text',
                            fieldLabel: "Default Video's Poster Link",
                            name: 'DefaultBannerVideoPosterLinks',
                            itemId: 'DefaultBannerPosterFieldId',
                            flex: 2,
                            style: {
                                marginLeft: '10px'
                            },
                            allowBlank: false
                        }
                        
                    ]
                },
                {
                    xtype: 'panel',
                    layout: 'hbox',
                    items: [
                    		{
                                xtype: 'mz-input-text',
                                fieldLabel: 'Destination URL',
                                name: 'DestinationURL',
                                itemId: 'DestinationURLId',
                                style: {
                                    marginLeft: '15px'
                                }
                            },
                            {
                                xtype: "mz-input-dropdown",
                                name:"aspectRatio",
                                fieldLabel: "Aspect Ratio",
	                             value: "16:9",
                                store: [
                                    "1:1",
                                    "16:9",
                                    "4:3",
                                    "3:2",
                                    "8:5"
                                ]
                            }
                    	]
                },
                {
                    xtype: 'panel',
                    layout: 'hbox',
                    items: [
                            {
                                xtype: "mz-input-checkbox",
                                name: "autoplay",
                                id:"autoplay",
                                fieldLabel: "Autoplay",
                                style: {
                                    marginLeft: '10px',
                                    marginTop: '25px'
                                },
                                flex: 2
                            },
                            {
                                xtype: "mz-input-checkbox",
                                name: "repeat",
                                id:"repeat",
                                fieldLabel: "Repeat video",
                                style: {
                                    marginLeft: '10px',
                                    marginTop: '25px' 
                                },
                                flex: 2
                            },
                            {
                                xtype: "mz-input-checkbox",
                                name: "videoControls",
                                id:"videocontrols",
                                fieldLabel: "Video Controls",
                                style: {
                                    marginLeft: '10px',
                                    marginTop: '25px'
                                },
                                flex: 2
                            },
                            {
                                xtype: "mz-input-text",
                                name: "Width",
                                id:"width",
                                fieldLabel: "Width",
                                style: {
                                    marginLeft: '10px',
                                    marginTop: '25px' 
                                },
                                flex: 2
                            },
                            {
                                xtype: "mz-input-text",
                                name: "Height",
                                id:"height",
                                fieldLabel: "Height",
                                style: {
                                    marginLeft: '10px',
                                    marginTop: '25px' 
                                },
                                flex: 2
                            }
                        ]
                    },
                    {
                    xtype: 'panel',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'mz-input-textarea',
                            fieldLabel: 'Default Video Description',
                            name: 'DefaultVideoDescriptions',
                            itemId: 'DefaultVideoDescriptionId',
                            style: {
                                marginLeft: '10px'
                            },
                            flex: 2
                        }
                    ]
                }
            ]
        }
    ],
    getData: function(){
    	var bannerVideoAttributes = this.getValues();
    	
    	if(bannerVideoAttributes.aspectRatio == "1:1") {
    		bannerVideoAttributes.aspectRatio = "100%";
        } else if(bannerVideoAttributes.aspectRatio == "16:9") {
        	bannerVideoAttributes.aspectRatio = "56.25%";
          } else if(bannerVideoAttributes.aspectRatio == "4:3") {
        	  bannerVideoAttributes.aspectRatio = "75%";
          } else if(bannerVideoAttributes.aspectRatio == "8:5") {
        	  bannerVideoAttributes.aspectRatio = "62.5%";
          } else if(bannerVideoAttributes.aspectRatio == "3:2") {
        	  bannerVideoAttributes.aspectRatio = "66.66%";
          }

    	return Ext.applyIf(bannerVideoAttributes, this.data);
    },
    setData: function(data){
    	if(!Ext.Object.isEmpty(data)){
    		if(data.aspectRatio == "100%") {
                data.aspectRatio = "1:1";
            } else if(data.aspectRatio == "56.25%") {
            	data.aspectRatio = "16:9";
              } else if(data.aspectRatio == "75%") {
            	  data.aspectRatio = "4:3";
              } else if(data.aspectRatio == "62.5%") {
            	  data.aspectRatio = "8:5";
              } else if(data.aspectRatio == "66.66%") {
            	  data.aspectRatio = "3:2";
              }
    	}
    	this.getForm().setValues(data);
    	this.data = data;
    }
});