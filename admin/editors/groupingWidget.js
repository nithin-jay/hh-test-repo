Ext.widget({
	xtype: 'mz-form-widget',
	itemId: 'groupingWidget',
	items: [
			{
			    xtype: 'container',
			    layout: 'hbox',
			    items: [
					{
						xtype: "mz-input-text",
						name: "categoryName",
						id:"categoryName",
						fieldLabel: "Title",
						flex: 2
					},
					{
						xtype: "mz-input-color",
						name: "titleFontColor",
						id:"titleFontColor",
						fieldLabel: "Title Font color",
						flex: 2,
	                    style: {
	                        marginLeft: '10px'
	                    }
					}
				]
		},
		{
		    xtype: 'container',
		    layout: 'hbox',
		    items: [
					{
						xtype: "mz-input-textarea",
						name: "categoryDesc",
						id:"categoryDesc",
						fieldLabel: "Description",
						flex: 2,
					},
					{
						xtype: "mz-input-color",
						name: "categoryDescFontColor",
						id:"categoryDescFontColor",
						fieldLabel: "Description Font Color",
						flex: 2,
						style: {
					        marginLeft: '10px'
					    }
					}
			]
		},
		{
		    xtype: 'container',
		    layout: 'hbox',
		    items: [
					{
						xtype: "mz-input-text",
						name: "buttonText",
						id:"buttonText",
						fieldLabel: "Button Text",
						flex: 2
					},
					{
						xtype: "mz-input-text",
						name: "buttonLink",
						id:"buttonLink",
						fieldLabel: "Button Link",
						flex: 2,
						style: {
					        marginLeft: '10px'
					    }
					},
				]
		},
    
		{
			xtype: "container",
			layout: "hbox",
			items: [
				{
					xtype: "mz-input-checkbox",
					name: "isCentered",
					id:"isCentered",
					fieldLabel: "Content centered",
					flex: 2,
				}
	
			]
		},
		{
            xtype: 'container',
            layout: 'hbox',
            items: [
            	{
					 name: "promoID",
					 fieldLabel: "Promo ID",
					 xtype: "mz-input-text",
					 flex: 1
				 },
				 {
					 name: "promoName",
					 fieldLabel: "Promo Name",
					 xtype: "mz-input-text",
					 flex: 1,
					 style: {
						 	marginLeft: '10px'
					 }
				 }
            ]
        }
	]
});
