Ext.widget({
    xtype: 'mz-form-widget',
    itemId: 'HhhNavigationform',
    items: [
            {
                xtype: 'container',
                id: 'hhhContainer',
                name: 'hhhContainer',
                items: [
                        {
                            xtype: 'panel',
                            border: false,
                            layout: 'hbox',
                            name: 'headerPanel',
                            items: [
                                {
                                    xtype: 'mz-input-text',
                                    fieldLabel: 'Header',
                                    name: 'Header'
                                },
                                {
                                    xtype: 'mz-input-number',
                                    fieldLabel: 'Number of fields',
                                    id: 'noOfFields',
                                    name: 'NoOfFields',
                                    value: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: 'Add',
                                    name: 'AddBtn',
                                    style: {
                                        marginTop: '30px',
                                        marginLeft: '10px'
                                    },
                                    listeners: {
                                        click: function(e){
                                            var noOfFields = Ext.getCmp('noOfFields').getValue();
                                            for(var i = 0; i < noOfFields; i++){
                                                e.up('form').add(
                                                        {
                                                            xtype: 'container',
                                                            layout: 'vbox',
                                                            items: [
                                                                {
                                                                    xtype: 'panel',
                                                                    layout: 'hbox',
                                                                    items: [
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Main Menu Text',
                                                                            name: 'mainmenuText',
                                                                            itemId: 'mainmenuText',
                                                                            flex: 2,
                                                                            allowBlank: false
                                                                        },
                                                                        {
                                                                            xtype: "mz-input-text",
                                                                            name: "mainMenuCategoryPicker",
                                                                            fieldLabel: "Main Menu Category Input",
                                                                            itemId: 'mainMenuCategoryPicker', 
                                                                            flex: 2,
                                                                            style: {
                                                                                marginLeft: '10px'
                                                                            }
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'panel',
                                                                    layout: 'hbox',
                                                                    items: [
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Text1',
                                                                            name: 'submenuText1',
                                                                            itemId: 'submenuText1',
                                                                            flex: 2,
                                                                        },
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Link1',
                                                                            name: 'submenuLink1',
                                                                            itemId: 'submenuLink1',
                                                                            flex: 2,
                                                                            style: {
                                                                                marginLeft: '10px'
                                                                            }
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'panel',
                                                                    layout: 'hbox',
                                                                    items: [
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Text2',
                                                                            name: 'submenuText2',
                                                                            itemId: 'submenuText2',
                                                                            flex: 2,
                                                                        },
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Link2',
                                                                            name: 'submenuLink2',
                                                                            itemId: 'submenuLink2',
                                                                            flex: 2,
                                                                            style: {
                                                                                marginLeft: '10px'
                                                                            }
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'panel',
                                                                    layout: 'hbox',
                                                                    items: [
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Text3',
                                                                            name: 'submenuText3',
                                                                            itemId: 'submenuText3',
                                                                            flex: 2,
                                                                        },
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Link3',
                                                                            name: 'submenuLink3',
                                                                            itemId: 'submenuLink3',
                                                                            flex: 2,
                                                                            style: {
                                                                                marginLeft: '10px'
                                                                            }
                                                                        }
                                                                    ]
                                                                }, 
                                                                {
                                                                    xtype: 'panel',
                                                                    layout: 'hbox',
                                                                    items: [
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Text4',
                                                                            name: 'submenuText4',
                                                                            itemId: 'submenuText4',
                                                                            flex: 2,
                                                                        },
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Link4',
                                                                            name: 'submenuLink4',
                                                                            itemId: 'submenuLink4',
                                                                            flex: 2,
                                                                            style: {
                                                                                marginLeft: '10px'
                                                                            }
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'panel',
                                                                    layout: 'hbox',
                                                                    items: [
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Text5',
                                                                            name: 'submenuText5',
                                                                            itemId: 'submenuText5',
                                                                            flex: 2,
                                                                        },
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Link5',
                                                                            name: 'submenuLink5',
                                                                            itemId: 'submenuLink5',
                                                                            flex: 2,
                                                                            style: {
                                                                                marginLeft: '10px'
                                                                            }
                                                                        },
                                                                        {
                                                                            xtype: 'button',
                                                                            text: 'Delete',
                                                                            name: 'DeleteBtn',
                                                                            flex: 1,
                                                                            style: {
                                                                                marginTop: '30px',
                                                                                marginLeft: '10px',
                                                                                align: 'right'
                                                                            },
                                                                            listeners: {
                                                                                click: function(e){
                                                                                    e.up('panel').up('container').destroy();
                                                                                }
                                                                            }
                                                                        }
                                                                    ]
                                                                }
                                                            ]
                                                        }
                                                );
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    ]
            }
    ],
    getData: function() {
        var hhhData = this.getValues();
        if(Ext.isArray(hhhData.mainmenuText)){
            var out = hhhData.mainmenuText.map(function(item, index){
               var chknum=isNaN(hhhData.mainMenuCategoryPicker[index]);
               var chknumSublink1=isNaN(hhhData.submenuLink1[index]);
               var chknumSublink2=isNaN(hhhData.submenuLink2[index]);
               var chknumSublink3=isNaN(hhhData.submenuLink3[index]);
               var chknumSublink4=isNaN(hhhData.submenuLink4[index]);
               var chknumSublink5=isNaN(hhhData.submenuLink5[index]);
                return { maintitle: item, chklink:chknum, chksublink1:chknumSublink1, chksublink2:chknumSublink2, chksublink3:chknumSublink3, chksublink4:chknumSublink4, chksublink5:chknumSublink5, mainlink: hhhData.mainMenuCategoryPicker[index], subtitle1: hhhData.submenuText1[index],sublink1: hhhData.submenuLink1[index], subtitle2: hhhData.submenuText2[index], sublink2: hhhData.submenuLink2[index], subtitle3: hhhData.submenuText3[index], sublink3: hhhData.submenuLink3[index], subtitle4: hhhData.submenuText4[index], sublink4: hhhData.submenuLink4[index], subtitle5: hhhData.submenuText5[index], sublink5: hhhData.submenuLink5[index] };
            });
            hhhData.hhhContent = out;
        }else{
            hhhData.hhhContent = [];
            var chknum=isNaN(hhhData.mainMenuCategoryPicker);
            var chknumSublink1=isNaN(hhhData.submenuLink1);
            var chknumSublink2=isNaN(hhhData.submenuLink2);
            var chknumSublink3=isNaN(hhhData.submenuLink3);
            var chknumSublink4=isNaN(hhhData.submenuLink4);
            var chknumSublink5=isNaN(hhhData.submenuLink5);
            hhhData.hhhContent.push({maintitle: hhhData.mainmenuText, chklink:chknum, chksublink1:chknumSublink1, chksublink2:chknumSublink2, chksublink3:chknumSublink3, chksublink4:chknumSublink4, chksublink5:chknumSublink5, mainlink: hhhData.mainMenuCategoryPicker, subtitle1: hhhData.submenuText1, sublink1: hhhData.submenuLink1, subtitle2: hhhData.submenuText2, sublink2: hhhData.submenuLink2, subtitle3: hhhData.submenuText3, sublink3: hhhData.submenuLink3, subtitle4: hhhData.submenuText4, sublink4: hhhData.submenuLink4, subtitle5: hhhData.submenuText5, sublink5: hhhData.submenuLink5})
        }
        
        delete hhhData.mainmenuText;
        delete hhhData.mainMenuCategoryPicker;
        delete hhhData.submenuText1;
        delete hhhData.submenuLink1;
        delete hhhData.submenuText2;
        delete hhhData.submenuLink2;
        delete hhhData.submenuText3;
        delete hhhData.submenuLink3;
        delete hhhData.submenuText4;
        delete hhhData.submenuLink4;
        delete hhhData.submenuText5;
        delete hhhData.submenuLink5;
        
        return Ext.applyIf(hhhData, this.data);
    },
    setData: function(data) {
        this.getForm().setValues(data);
        if(!Ext.Object.isEmpty(data)){
            for(var i = 0; i < data.hhhContent.length; i++){
                Ext.getCmp('hhhContainer').add(
                        {
                            xtype: 'container',
                            layout: 'vbox',
                            items: [
                                {
                                    xtype: 'panel',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Main Menu Text',
                                            name: 'mainmenuText',
                                            itemId: 'mainmenuText',
                                            value: data.hhhContent[i].maintitle,
                                            flex: 2,
                                            allowBlank: false
                                        },
                                        {
                                            xtype: "mz-input-text",
                                            name: "mainMenuCategoryPicker",
                                            fieldLabel: "Main Menu Category Input",
                                            itemId: 'mainMenuCategoryPicker',
                                            value: data.hhhContent[i].mainlink,
                                            style: {
                                                marginLeft: '10px'
                                            },
                                            flex: 2
                                        },
                                    ]
                                },
                                {
                                    xtype: 'panel',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Text1',
                                            name: 'submenuText1',
                                            itemId: 'submenuText1',
                                            value: data.hhhContent[i].subtitle1,
                                            flex: 2,
                                        },
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Link1',
                                            name: 'submenuLink1',
                                            itemId: 'submenuLink1',
                                            value: data.hhhContent[i].sublink1,
                                            style: {
                                                marginLeft: '10px'
                                            },
                                            flex: 2
                                        }
                                    ]
                                },
                                {
                                    xtype: 'panel',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Text2',
                                            name: 'submenuText2',
                                            itemId: 'submenuText2',
                                            value: data.hhhContent[i].subtitle2,
                                            flex: 2,
                                        },
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Link2',
                                            name: 'submenuLink2',
                                            itemId: 'submenuLink2',
                                            value: data.hhhContent[i].sublink2,
                                            style: {
                                                marginLeft: '10px'
                                            },
                                            flex: 2
                                        }
                                    ]
                                },
                                {
                                    xtype: 'panel',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Text3',
                                            name: 'submenuText3',
                                            itemId: 'submenuText3',
                                            value: data.hhhContent[i].subtitle3,
                                            flex: 2,
                                        },
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Link3',
                                            name: 'submenuLink3',
                                            itemId: 'submenuLink3',
                                            value: data.hhhContent[i].sublink3,
                                            style: {
                                                marginLeft: '10px'
                                            },
                                            flex: 2
                                        }
                                    ]
                                },
                                {
                                    xtype: 'panel',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Text4',
                                            name: 'submenuText4',
                                            itemId: 'submenuText4',
                                            value: data.hhhContent[i].subtitle4,
                                            flex: 2,
                                        },
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Link4',
                                            name: 'submenuLink4',
                                            itemId: 'submenuLink4',
                                            value: data.hhhContent[i].sublink4,
                                            style: {
                                                marginLeft: '10px'
                                            },
                                            flex: 2
                                        }
                                    ]
                                },
                                {
                                    xtype: 'panel',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Text5',
                                            name: 'submenuText5',
                                            itemId: 'submenuText5',
                                            value: data.hhhContent[i].subtitle5,
                                            flex: 2,
                                        },
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Link5',
                                            name: 'submenuLink5',
                                            itemId: 'submenuLink5',
                                            value: data.hhhContent[i].sublink5,
                                            style: {
                                                marginLeft: '10px'
                                            },
                                            flex: 2
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Delete',
                                            name: 'DeleteBtn',
                                            flex: 1,
                                            style: {
                                                marginTop: '30px',
                                                marginLeft: '10px',
                                                align: 'right'
                                            },
                                            listeners: {
                                                click: function(e){
                                                   e.up('panel').up('container').destroy();
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    );
            }
            
        }
        this.data = data;
    }
});