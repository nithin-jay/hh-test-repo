Ext.widget({
    xtype: 'mz-form-widget',
    items: [ 
        {
			xtype: 'container',
			id: 'mainContainer',
			items: [
                {
                    name: "sectionHeader",
                    fieldLabel: "Section Header",
                    xtype: "mz-input-text",
                    flex: 1,
                    style: {
                        marginLeft: '10px'
                    }
                },
                {
                    xtype: 'container',
                    style: {
                        background: '#ccb',
                        padding: '10px',
                        marginLeft: '30px',
                        marginTop: '30px'
                    },
                    items: [
                        {
                            xtype: 'mz-input-color',
                            fieldLabel: 'Background Color',
                            name: 'backgroundColor',
                            value: "rgba(255, 255, 255, 1)"
                        },
                        {
                            xtype: 'panel',
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'mz-input-image-nostyle',
                                    fieldLabel: 'Desktop Image',
                                    name: 'Images'
                                },
                                {
                                    xtype: 'panel',
                                    layout: 'vbox',
                                    items: [
                                        {
                                            xtype: 'panel',
                                            layout: 'hbox',
                                            items: [
                                                {
                                                    name: "topLineColour",
                                                    fieldLabel: "Top Line Colour",
                                                    xtype: "mz-input-color",
                                                    allowBlank: false,
                                                    value: "rgba(228, 0, 43, 1)",
                                                    flex: 1,
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                },
                                                {
                                                    name: "textColour",
                                                    fieldLabel: "Text Colour",
                                                    xtype: "mz-input-color",
                                                    value: "rgba(0,0,0,1)",
                                                    flex: 1,
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'panel',
                                            layout: 'hbox',
                                            width: 500,
                                            items: [
                                                {
                                                    name: "title",
                                                    fieldLabel: "Title/HeadLine",
                                                    xtype: "mz-input-text",
                                                    flex: 1,
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                },
                                                {
                                                    name: "subTitle",
                                                    fieldLabel: "Sub-head Copy",
                                                    xtype: "mz-input-text",
                                                    flex: 1,
                                                    store: [
                                                        'Left',
                                                        'Right'
                                                    ],
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'panel',
                                            layout: 'hbox',
                                            items: [
                                                {
                                                    name: "saveStory",
                                                    fieldLabel: "Save Story",
                                                    xtype: "mz-input-text",
                                                    flex: 1,
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: 'hbox',
                            items: [
                                {
                                    name: "ctaTextField",
                                    fieldLabel: "CTA Text",
                                    xtype: "mz-input-text",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "ctaLink",
                                    fieldLabel: "CTA link",
                                    xtype: "mz-input-text",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "ctaTextColor",
                                    fieldLabel: "CTA Text Color",
                                    xtype: "mz-input-color",
                                    value: "rgba(228, 0, 43, 1)",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "ctaBorderColor",
                                    fieldLabel: "CTA Border Color",
                                    xtype: "mz-input-color",
                                    value: "rgba(228, 0, 43, 1)",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: 'hbox',
                            items: [
                                {
                                    name: "logoOneImage",
                                    fieldLabel: "Logo one image",
                                    xtype: "mz-input-image-nostyle",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "logoTwoImage",
                                    fieldLabel: "Logo Two image",
                                    xtype: "mz-input-image-nostyle",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "logoPosition",
                                    fieldLabel: "Logo Position",
                                    xtype: "mz-input-dropdown",
                                    store:[
                                        "Left",
                                        "Center"
                                    ],
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'mz-input-image',
                                    fieldLabel: 'Mobile Image',
                                    name: 'mobImages'
                                },
                                {
                                    name: "productPrice",
                                    fieldLabel: "Product Price",
                                    xtype: "mz-input-text",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    name: "oldPrice",
                                    fieldLabel: "Old Price",
                                    xtype: "mz-input-text",
                                    flex: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ],
    getData: function () {
        var stageData = this.getValues();
        if(!stageData.mobImages) {
            stageData.mobImages = stageData.Images;
        }
        return Ext.applyIf(stageData, this.data);
    },
    setData: function (data) {
        this.getForm().setValues(data);
        this.data = data;
    }
});