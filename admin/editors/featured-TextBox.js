Ext.widget({
    xtype: 'mz-form-widget',
	items: [
        {
            xtype: 'panel',
            html: ['<div class="alert bg-info">',
            '<strong>Instructions to use the widget!</strong>',
            '<ul>',
            '<li class="text-warning">1. While using this widget, we need to first set Grid Columns.</li>',
            '<li class="text-warning">2. After Grid Columns are set, then in No of fields text box, Add required number of fields in box and press add.</li>',
            '<li class="text-warning">3. You will get those many number of Image and text description fields in each block.</li>',
            '<li class="text-warning">4. If Grid Column is set to 1, and if you have entered more than 1 (No of field), then all the blocks will be considered as single block</li>',
            '<li class="text-warning">5. If Grid Column is set to 2, and if you have entered more than 2 (No of field), then only 2 blocks will be considered and displayed and remaining blocks will be ignored.</li>',
            '<li class="text-warning">6. If Grid Column is set to 3, and if you have entered more than 3 (No of field), then only 3 blocks will be considered and displayed and remaining blocks will be ignored.</li>',
            '</ul>',
            '</div>'].join("\n")
         },
        {
        xtype: 'container',
        id: 'mainContainer',
        items: [
                {
                xtype: 'panel',
                layout: 'hbox',
                items: [
                    {
                        "anchor": "100%",
                        "xtype": "mz-input-color",
                        fieldLabel:'Background color',
                        "name": "backgroundColor",
                        flex: 1,
                        style: {
                            marginLeft: '10px'
                        }
                    },
                    {
                        "anchor": "100%",
                        xtype: "mz-input-dropdown",
                        name: "gridColumns",
                        fieldLabel: "Grid Columns",
                        flex: 2,
                        style: {
                            marginLeft: '10px'
                        },
                        store: [
                            1,
                            2,
                            3
                        ],
                        listeners: {
                            change: function(element, record, idx) {
                                if(element.value == 2 || element.value == 3 ) {
                                    Ext.each(Ext.ComponentQuery.query('[name="gridBackgroundColor"]'), function (field) {
                                       field.show();
                                    });
                                } else {
                                    Ext.each(Ext.ComponentQuery.query('[name="gridBackgroundColor"]'), function (field) {
                                       field.hide();
                                    });
                                }
                            }
                        }
                    }
                ]
                },
                {
                 xtype: 'panel',
                 layout: 'hbox',
                 items: [
                       {
                            xtype: 'mz-input-number',
                            fieldLabel: 'Number of fields',
                            id: 'noOfFields',
                            name: 'NoOfFields',
                            value: 1,
                            style: {
                                marginLeft: '30px'
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Add',
                            name: 'AddBtn',
                            style: {
                                marginTop: '30px',
                                marginLeft: '10px'
                            }, 
                            listeners: {
                                click: function(e){
                                    var noOfFields = Ext.getCmp('noOfFields').getValue();
                                    
                                    for(var i=0; i<noOfFields; i++){
                                        e.up('form').add(
                                            {
                                                xtype: 'container',
                                                style: "background:#ccb;margin-top:15px;",
                                                items: [
                                                    {
                                                        "anchor": "100%",
                                                        "xtype": "mz-input-color",
                                                        "fieldLabel":'Grid Background color',
                                                        "name": "gridBackgroundColor",
                                                        "hidden": true
                                                    },
                                                    {
                                                        xtype: 'panel',
                                                        layout: 'vbox',
                                                        items: [
                                                            {
                                                                xtype: 'panel',
                                                                layout: 'hbox',
                                                                style: "margin-top:20px;",
                                                                items: [
                                                                    {
                                                                        "anchor": "100%",
                                                                        xtype: "mz-input-dropdown",
                                                                        name: "imagePosition",
                                                                        fieldLabel: "Image Position",
                                                                        style: {
                                                                            marginLeft: '10px'
                                                                        },
                                                                        store: [
                                                                            'Right',
                                                                            'Left'
                                                                        ]
                                                                    }
                                                                ]
                                                            },
                                                            {
                                                                xtype: 'panel',
                                                                layout: 'hbox',
                                                                style: "margin-top:20px;",
                                                                items: [
                                                                    {
                                                                        "anchor": "100%",
                                                                        "xtype": "mz-input-image-nostyle",
                                                                        "name": "imageData",
                                                                        flex: 1,
                                                                        style: {
                                                                            marginLeft: '10px'
                                                                        }
                                                                    },
                                                                    {
                                                                        "anchor": "100%",
                                                                        "xtype": "mz-input-richtext",
                                                                        "name": "textEditorSection",
                                                                        enableSourceEdit: true,
                                                                        enableColors: true,
                                                                        enableLinks: true,
                                                                        enableFontSize: true,
                                                                        flex: 2,
                                                                        style: {
                                                                            marginLeft: '10px'
                                                                        }
                                                                    }
                                                                    
                                                                ]
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'panel',
                                                        layout: 'hbox',
                                                        items: [
                                                            {
                                                                xtype: 'button',
                                                                text: 'Delete',
                                                                name: 'DeleteBtn',
                                                                flex: 1,
                                                                style: {
                                                                    marginTop: '30px',
                                                                    marginLeft: '10px'
                                                                },
                                                                listeners: {
                                                                    click: function(e){
                                                                        e.up('panel').up('container').destroy();
                                                                    }
                                                                }
                                                            }
                                                        ]
                                                    } 
                                                  ]
                                            });
                                    }
                                    /**
                                     * Below code is used to display grid Background Colours if newly field blocks are added.
                                     */
                                    debugger;
                                    if(Ext.ComponentQuery.query('[name="gridColumns"]')[0].value != null && Ext.ComponentQuery.query('[name="gridColumns"]')[0].value != 1) {
                                        Ext.each(Ext.ComponentQuery.query('[name="gridBackgroundColor"]'), function (field) {
                                               field.show();
                                        });
                                    } else {
                                        Ext.each(Ext.ComponentQuery.query('[name="gridBackgroundColor"]'), function (field) {
                                            field.hide();
                                     });
                                    }
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ],
    getData: function() {
        var textBoxData = this.getValues();
        	if(!Ext.isArray(textBoxData.textEditorSection)){  //To work with only one image
        		textBoxData.textEditorSection = [textBoxData.textEditorSection];
        	}
            textBoxData.textSectionContent = [];
            textBoxData.textSectionContent.NoOfFields = textBoxData.NoOfFields;
            textBoxData.textSectionContent.gridColumns = textBoxData.gridColumns;
            textBoxData.textSectionContent.backgroundColor = textBoxData.backgroundColor;
        	Ext.each(textBoxData.textEditorSection, function(item, index){
                textBoxData.textSectionContent[index] = {};
                textBoxData.textSectionContent[index].textEditorSection = item;
                textBoxData.textSectionContent[index].gridBackgroundColor = textBoxData.gridBackgroundColor[index];
                if(textBoxData.imageData) {
        			if(Ext.isArray(textBoxData.imageData)){
                        if(textBoxData.imageData[index]) {
                            textBoxData.textSectionContent[index].imageData = textBoxData.imageData[index];
                        } else {
                            textBoxData.textSectionContent[index].imageData = null;
                        }
        			}else{
                        if(textBoxData.imageData) {
                            textBoxData.textSectionContent[index].imageData = textBoxData.imageData;
                        } else {
                            textBoxData.textSectionContent[index].imageData = null;
                        }
        			}
        		}
        		if(textBoxData.imagePosition) {
        			if(Ext.isArray(textBoxData.imagePosition)){
                        if(textBoxData.imagePosition[index]) {
                            textBoxData.textSectionContent[index].imagePosition = textBoxData.imagePosition[index];
                        } else {
                            textBoxData.textSectionContent[index].imagePosition = 'Right';
                        }
        			}else{
                        if(textBoxData.imagePosition) {
                            textBoxData.textSectionContent[index].imagePosition = textBoxData.imagePosition;
                        } else {
                            textBoxData.textSectionContent[index].imagePosition = 'Right';
                        }
        			}
        		}
                if(textBoxData.gridBackgroundColor) {
        			if(Ext.isArray(textBoxData.gridBackgroundColor)){
                        if(textBoxData.gridBackgroundColor[index]) {
                            textBoxData.textSectionContent[index].gridBackgroundColor = textBoxData.gridBackgroundColor[index];
                        } else {
                            textBoxData.textSectionContent[index].gridBackgroundColor = 'rgba(0, 0, 0, 1)';
                        }
        			}else{
                        if(textBoxData.imagePosition) {
                            textBoxData.textSectionContent[index].gridBackgroundColor = textBoxData.gridBackgroundColor;
                        } else {
                            textBoxData.textSectionContent[index].gridBackgroundColor = 'rgba(0, 0, 0, 1)';
                        }
        			}
        		}
        	});
        	delete textBoxData.imagePosition;
        	delete textBoxData.imageData;
            delete textBoxData.gridBackgroundColor;
        	return Ext.applyIf(textBoxData, this.data);
    },
    setData: function(data){
        this.getForm().setValues(data);
        	if(!Ext.Object.isEmpty(data)){
        		for(var i = 0; i < data.textSectionContent.length; i++){
        			Ext.getCmp('mainContainer').add(
                        {
                        xtype: 'container',
                        style: "background:#ccb;margin-top:15px;",
                        items: [
                            {
                                "anchor": "100%",
                                "xtype": "mz-input-color",
                                "fieldLabel":'Grid Background color',
                                "name": "gridBackgroundColor",
                                value: data.textSectionContent[i].gridBackgroundColor
                            },
                            {
                                xtype: 'panel',
                                layout: 'vbox',
                                items: [
                                    {
                                        xtype: 'panel',
                                        layout: 'hbox',
                                        style: "margin-top:20px;",
                                        items: [
                                            {
                                                "anchor": "100%",
                                                xtype: "mz-input-dropdown",
                                                name: "imagePosition",
                                                fieldLabel: "Image Position",
                                                value: data.textSectionContent[i].imagePosition ? data.textSectionContent[i].imagePosition : 'Right',
                                                style: {
                                                    marginLeft: '10px'
                                                },
                                                store: [
                                                    'Right',
                                                    'Left'
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'panel',
                                        layout: 'hbox',
                                        style: "margin-top:20px;",
                                        items: [
                                            {
                                                "anchor": "100%",
                                                "xtype": "mz-input-image-nostyle",
                                                "name": "imageData",
                                                value: data.textSectionContent[i].imageData,
                                                flex: 1,
                                                style: {
                                                    marginLeft: '10px'
                                                }
                                            },
                                            {
                                                "anchor": "100%",
                                                "xtype": "mz-input-richtext",
                                                "name": "textEditorSection",
                                                value: data.textSectionContent[i].textEditorSection,
                                                enableSourceEdit: true,
                                                enableColors: true,
                                                enableLinks: true,
                                                enableFontSize: true, 
                                                flex: 2,
                                                style: {
                                                    marginLeft: '10px'
                                                }
                                            }
                                            
                                        ]
                                    }
                                ]
                            },
                            {
                                xtype: 'panel',
                                layout: 'hbox',
                                items: [
                                    {
                                        xtype: 'button',
                                        text: 'Delete',
                                        name: 'DeleteBtn',
                                        flex: 1,
                                        style: {
                                            marginTop: '30px',
                                            marginLeft: '10px'
                                        },
                                        listeners: {
                                            click: function(e){
                                                e.up('panel').up('container').destroy();
                                            }
                                        }
                                    }
                                ]
                            }
                          ]
                    });
                }
            }
            /**
             * Below code is used to display grid Background Colours if newly field blocks are added.
             */
             if(Ext.ComponentQuery.query('[name="gridColumns"]')[0].value != null && Ext.ComponentQuery.query('[name="gridColumns"]')[0].value != 1) {
                Ext.each(Ext.ComponentQuery.query('[name="gridBackgroundColor"]'), function (field) {
                       field.show();
                });
            } else {
                Ext.each(Ext.ComponentQuery.query('[name="gridBackgroundColor"]'), function (field) {
                    field.hide();
             });
            }
            this.data = data;
    }
});