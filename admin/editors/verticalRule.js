Ext.widget({
    xtype: 'mz-form-widget',
    itemId: 'vrRuleForm',
    defaults: {
        xtype: 'combobox',
        editable: false,
        listeners: {
            change: function (cmp) {
               cmp.up('#vrRuleForm').updatePreview();
            }
        }
    },

    items: [
        {
            fieldLabel: 'Thickness',
            name: 'vrBorderWidth',
            store: ['1px', '2px', '3px', '4px', '5px', '6px', '7px', '8px', '9px', '10px', '11px', '12px', '13px', '14px', '15px', '16px', '17px', '18px', '19px', '20px']
        }, {
            xtype: 'colorfield',
            fieldLabel: 'Color',
            name: 'vrBorderColor'
        }, {
            xtype: 'radiogroup',
            fieldLabel: 'Style',
            defaults: {
                name: 'vrBorderStyle'
            },
            items: [
                {
                    inputValue: 'solid',
                    boxLabel: 'solid'
                }, {
                    inputValue: 'dashed',
                    boxLabel: 'dashed'
                }, {
                    inputValue: 'dotted',
                    boxLabel: 'dotted'
                }
            ]
        }, {
            fieldLabel: 'Spacing Above',
            name: 'vrMarginTop',
            store: [['0px', 'None'], ['4px', 'Small (4px)'], ['8px', 'Medium (8px)'], ['12px', 'Large (12px)']]
        }, {
            fieldLabel: 'Spacing Below',
            name: 'vrMarginBottom',
            store: [['0px', 'None'], ['4px', 'Small (4px)'], ['8px', 'Medium (8px)'], ['12px', 'Large (12px)']]
        },
        {
            xtype: 'mz-input-text',
            fieldLabel: 'Height (Only number)',
            name: 'vrBorderHeight'
        },{
            xtype: 'component',
            width: '100%',
            padding: 0,
            html: 'Preview',
            cls: 'x-form-item-label-top'
        }, {
            xtype: 'container',
            width: '100%',
            padding: '20 0 20 0',
            itemId: 'preview-container',
            cls: Taco.baseCSSPrefix + 'vr-preview',
            items: [
                {
                    xtype: 'component',
                    itemId: 'preview',
                    autoEl: 'vr'
                }
            ]
        }
    ],


    listeners: {
        afterrender: function (cmp) {
            cmp.updatePreview();
        }
    },


    updatePreview: function () {


        var previewEl = this.down('#preview').getEl(),
            formValues = this.getForm().getValues(),
            newStyles = {};

        if (previewEl) {
            newStyles['border-top-width'] = formValues.vrBorderWidth;
            newStyles['border-color'] = formValues.vrBorderColor;
            newStyles['border-style'] = formValues.vrBorderStyle;
            newStyles['margin-top'] = formValues.vrMarginTop;
            newStyles['margin-bottom'] = formValues.vrMarginBottom;
            newStyles['height'] = formValues.vrBorderHeight;
            
            previewEl.applyStyles(newStyles);
        }
    }
});