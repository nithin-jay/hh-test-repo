Ext.widget({
    xtype: 'mz-form-widget',
    itemId: 'expertAdvice',
    items: [
    	{
            xtype: 'container',
            layout: 'vbox',
            items: [
            	{
                    xtype: 'mz-input-image',
                    name: 'expertImage',
                    fieldLabel: 'Banner Image'
                },{
                    xtype: 'mz-input-text',
                    name: 'expertTitle',
                    fieldLabel: 'Title'
                },{
                    xtype: 'mz-input-text',
                    name: 'expertTitleLink',
                    fieldLabel: 'Title Link'
                },{
                    xtype: 'mz-input-textarea',
                    name: 'expertDesc',
                    fieldLabel: 'Short Description'
                },{
                    xtype: 'mz-input-text',
                    name: 'expertBottomLinkText',
                    fieldLabel: 'Bottom Link Text'
                },{
                    xtype: 'mz-input-text',
                    name: 'expertBottomLink',
                    fieldLabel: 'Bottom Link'
                }
            ]
    	}
	]
});