Ext.widget({
    xtype: 'mz-form-widget',
    itemId: 'certonaWidgetForm',
    items: [{
        xtype: 'mz-input-text',
        id: 'Title',
        name: 'certonaTitle',
        fieldLabel: 'Title'
    },
    {
        xtype: 'mz-input-dropdown',
        fieldLabel: 'Page Type',
        name: 'pageType',
        itemId: 'pageTypeId',
        store: [
                'Home Page',
                'No Search Results Page',
                'Search Page',
                'Category Page Level 1',
                'Category Page',
                'Product Page',
                'Cart',
                'Wishlist'
                ]
    },
    {
        xtype: 'mz-input-text',
        name: 'schemeName',
        fieldLabel: 'Scheme Name'
    },
    {
		 name: "promoID",
		 fieldLabel: "Promo ID",
		 xtype: "mz-input-text"
	 },
	 {
		 name: "promoName",
		 fieldLabel: "Promo Name",
		 xtype: "mz-input-text"
	 }
 ]
}); 
