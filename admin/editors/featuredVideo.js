Ext.widget({
    xtype: 'mz-form-widget',
    items: [
        {
            xtype: 'container',
            id: 'mainContainer',
            items:[
                    {
                        xtype: 'panel',
                        layout: 'hbox',
                        items: [
                            {
                                xtype: 'mz-input-text',
                                fieldLabel: 'Default Video Link',
                                name: 'DefaultBannerVideoLink',
                                flex: 2,
                                style: {
                                    marginLeft: '10px'
                                },
                                allowBlank: false
                            },
                            {
                                xtype: 'mz-input-text',
                                fieldLabel: "Default Video's Poster Link",
                                name: 'DefaultBannerVideoPosterLinks',
                                flex: 2,
                                style: {
                                    marginLeft: '10px'
                                }
                            }
                            
                        ]
                    },
                    {
                        xtype: 'panel',
                        layout: 'hbox',
                        items: [
                                {
                                    xtype: 'mz-input-text',
                                    fieldLabel: 'Destination URL',
                                    name: 'DestinationURL',
                                    style: {
                                        marginLeft: '15px'
                                    }
                                }
                            ]
                    },
                    {
                        xtype: 'panel',
                        layout: 'hbox',
                        items: [
                                {
                                    xtype: "mz-input-checkbox",
                                    name: "autoplay",
                                    fieldLabel: "Autoplay",
                                    style: {
                                        marginLeft: '10px',
                                        marginTop: '25px'
                                    }
                                },
                                {
                                    xtype: "mz-input-checkbox",
                                    name: "repeat",
                                    fieldLabel: "Repeat video",
                                    style: {
                                        marginLeft: '10px',
                                        marginTop: '25px' 
                                    }
                                },
                                {
                                    xtype: "mz-input-checkbox",
                                    name: "videoControls",
                                    fieldLabel: "Video Controls",
                                    style: {
                                        marginLeft: '10px',
                                        marginTop: '25px'
                                    }
                                }
                            ]
                        },
                        {
                        xtype: 'panel',
                        layout: 'hbox',
                        items: [
                            {
                                xtype: 'mz-input-textarea',
                                fieldLabel: 'Default Video Description',
                                name: 'DefaultVideoDescriptions',
                                style: {
                                    marginLeft: '10px'
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    });