var p1 = 0; //append this value to productCodes array to make an unique arrays
Ext.widget({
	xtype: 'mz-form-widget',
	itemId: 'imgSliderForm',
	items: [
		{
			xtype: 'container',
			id: 'mainConatiner',
			items: [
				{
					xtype: 'panel',
					layout: 'hbox',
					items: [
						{
							xtype: 'mz-input-number',
							fieldLabel: 'Number of fields',
							id: 'noOfFields',
							name: 'NoOfFields',
							value: 1,
							style: {
								marginLeft: '30px'
							}
						},
						{
							xtype: 'button',
							text: 'Add',
							name: 'AddBtn',
							style: {
								marginTop: '30px',
								marginLeft: '10px'
							},
							listeners: {
								click: function(e){
									var noOfFields = Ext.getCmp('noOfFields').getValue();

									for(var i=0; i<noOfFields; i++){
										e.up('form').add(
											{
												xtype: 'container',
												style: "background:#ccb;margin:10px; padding:10px",
												items: [
													{
														xtype: 'panel',
														layout: 'hbox',
														items: [
															{
																xtype: 'mz-input-image',
																fieldLabel: 'brandImage',
																name: 'Images'
															},
														]
													},
													{
														xtype: 'panel',
														layout: 'vbox',
														items: [
															{
																xtype: 'panel',
																layout: 'hbox',
																items: [
																	{
																		name: "brandImageUrl",
																		fieldLabel: "Brand Url",
																		xtype: "mz-input-text",
																		flex: 1,
																		style: {
																			marginLeft: '50px'
																		}
																	},
																]
															},
															{
																xtype: 'panel',
																layout: 'hbox',
																items: [
																	{
																		xtype: 'button',
																		text: 'Delete Field',
																		name: 'DeleteBtn',
																		flex: 1,
																		style: {
																			marginTop: '30px',
																			marginLeft: '50px'
																		},
																		listeners: {
																			click: function(e){
																				e.up('panel').up('container').destroy();
																			}
																		}
																	}
																]
															}
														]
													},
												]
											}
											);
											p1++;
										}
									}
								}
							}
						]
					}
				]
			}
		],
		getData: function(){
			var sliderData = this.getValues();
			if(!Ext.isArray(sliderData.Images)){  //To work with only one image
				sliderData.Images = [sliderData.Images];
			}
			Ext.each(sliderData.Images, function(item, index){
				if(sliderData.brandImageUrl) {
					if(Ext.isArray(sliderData.brandImageUrl)){
						item.brandImageUrl = sliderData.brandImageUrl[index];
					}else{
						item.brandImageUrl = sliderData.brandImageUrl;
					}
				}

				item.productCodes = sliderData["productCodes"+index];
			});
			delete sliderData.brandImageUrl;
			return Ext.applyIf(sliderData, this.data);
		},
		setData: function(data){
			this.getForm().setValues(data);
			if(!Ext.Object.isEmpty(data)){
				for(var i = 0; i < data.Images.length; i++){
					Ext.getCmp('mainConatiner').add(
						{
							xtype: 'container',
							style: "background:#ccb;margin:10px; padding:10px",
							items: [
								{
									xtype: 'panel',
									layout: 'hbox',
									items: [
										{
											xtype: 'mz-input-image',
											fieldLabel: 'Image',
											name: 'Images',
											value: data.Images[i]
										},
										{
											xtype: 'panel',
											layout: 'vbox',
											items: [
												{
													xtype: 'panel',
													layout: 'hbox',
													items: [
														{
															name: "brandImageUrl",
															fieldLabel: "Brand Url",
															xtype: "mz-input-text",
															value: data.Images[i].brandImageUrl,
															flex: 1,
															style: {
																marginLeft: '50px'
															}
														},
													]
												},
												{
													xtype: 'panel',
													layout: 'hbox',
													items: [
														{
															xtype: 'button',
															text: 'Delete Field',
															name: 'DeleteBtn',
															flex: 1,
															style: {
																marginTop: '30px',
																marginLeft: '50px'
															},
															listeners: {
																click: function(e){
																	e.up('panel').up('container').destroy();
																}
															}
														}
													]
												}
											]
										}
									]
								},
							]
						}
						);
					}
				}
				this.data = data;
			}
		});