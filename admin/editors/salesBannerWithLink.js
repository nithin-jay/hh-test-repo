Ext.widget({
    xtype: 'mz-form-widget',
    itemId: 'salesLinkBanner',
    items: [
    	{
            xtype: 'container',
            layout: 'vbox',
            items: [
            	{
            		xtype: 'panel',
                    layout: 'hbox',
                    items: [
                    	{
                    		xtype: 'mz-input-text',
                            name: 'salesLinkBannerTitle',
                            fieldLabel: 'Title:',
                            flex: 2
                        },{
                        	xtype: 'mz-input-color',
                            name: 'salesLinkBannerTitleFontColor',
                            fieldLabel: 'Title Font Color:',
                            flex: 2,
                            style:{
                            	marginLeft: '20px'
                            }
                        }
                    ]
            	},
            	{
            		xtype: 'panel',
                    layout: 'hbox',
                    items: [
                    	{
                    		xtype: 'mz-input-textarea',
                            name: 'salesLinkBannerDesc',
                            fieldLabel: 'Description:',
                            flex: 2
                        },{
                        	xtype: 'mz-input-color',
                            name: 'salesLinkBannerDescFontColor',
                            fieldLabel: 'Description Font Color:',
                            flex: 2,
                            style:{
                            	marginLeft: '20px'
                            }
                        }
                    ]
            	},
            	{
            		xtype: 'panel',
                    layout: 'hbox',
                    items: [
                    	{
                    		xtype: 'mz-input-text',
                            name: 'salesLinkBannerlinktext',
                            fieldLabel: 'Link Text:',
                            flex: 2
                        },{
                    		xtype: 'mz-input-text',
                            name: 'salesLinkBannerLink',
                            fieldLabel: 'Link URL:',
                            flex: 2,
                            style:{
                            	marginLeft: '20px'
                            }
                        },{
                        	xtype: 'mz-input-color',
                            name: 'salesLinkBannerLinkFontColor',
                            fieldLabel: 'Link Font Color:',
                            flex: 2,
                            style:{
                            	marginLeft: '20px'
                            }
                        }
                    ]
            	}
            ]
    	}
	]
});