Ext.widget({
    xtype: 'mz-form-widget',
    itemId: 'featuredBrands',
    items: [
        {
            xtype: 'container',
            layout: 'hbox',
            items: [
                {
                    xtype: 'mz-input-dropdown',
                    fieldLabel: 'Select Button Type',
                    name: 'buttonType',
                    listeners: {
                        select: function(combo, record, index) {
                            var tertiaryWidth = Ext.ComponentQuery.query('[name="buttonWidth"]');
                          if (record[0].data.field1 == "tertiary") {                           
                            tertiaryWidth[0].setValue(0);
                          } else {
                            tertiaryWidth[0].setValue("40px");
                          }
                        }
                      },
                    store: [
                        'primary',
                        'secondary',
                        'tertiary'
                    ]
                },
                {
                    xtype: 'mz-input-dropdown',
                    fieldLabel: 'Select Button Position',
                    name: 'buttonPosition',
                    store: [
                        'left',
                        'center',
                        'right'
                    ]
                }
            ]
        },
        {
            xtype: 'container',
            layout: 'hbox',
            items: [
                {
                    xtype: 'mz-input-text',
                    fieldLabel: 'Width of the button',
                    name: 'buttonWidth',
                    id: 'buttonWidth',
                    value: "40px"
                },
                {
                    xtype: 'mz-input-text',
                    fieldLabel: 'Height of the button',
                    name: 'buttonHeight',
                    id: 'buttonHeight',
                    value: "10px",
                    style: {
                        marginLeft: '15px'
                    }
                },
                {
                    xtype: 'mz-input-text',
                    fieldLabel: 'Border Radius',
                    name: 'buttonRadius',
                    id: 'buttonRadius',
                    value: "24px",
                    style: {
                        marginLeft: '15px'
                    }
                } 
            ]
        },
        {
            xtype: 'container',
            layout: 'hbox',
            items: [
                {
                    xtype: "mz-input-color",
                    name: "primaryTextColor",
                    value:"#FFFFFF",
                    fieldLabel: "Primary Text Color:",
                    flex: 2
                }, {
                    xtype: "mz-input-color",
                    name: "primaryBorderColor",
                    value:"#e4002b",
                    fieldLabel: "Primary Border Color:",
                    flex: 2,
                    style: {
                        marginLeft: '15px'
                    }
                },{
                    xtype: "mz-input-color",
                    name: "primaryBackgroundColor",
                    value:"#e4002b",
                    fieldLabel: "Primary Background Color:",
                    flex: 2,
                    style: {
                        marginLeft: '15px'
                    }
                }
                
            ]
        }, {
            xtype: 'container',
            layout: 'hbox',
            items: [
                {
                    xtype: "mz-input-color",
                    name: "secondaryTextColor",
                    value:"#e4002b",
                    fieldLabel: "Secondary Text Color:",
                    flex: 2
                }, {
                    xtype: "mz-input-color",
                    name: "secondaryBorderColor",
                    value:"#e4002b",
                    fieldLabel: "Secondary Border Color:",
                    flex: 2,
                    style: {
                        marginLeft: '15px'
                    }
                },{
                    xtype: "mz-input-color",
                    name: "secondaryBackgroundColor",
                    value:"#FFFFFF",
                    fieldLabel: "Secondary Background Color:",
                    flex: 2,
                    style: {
                        marginLeft: '15px'
                    }
                }
            ]
        },
        {
            xtype: 'container',
            layout: 'hbox',
            items: [
            	{
                    xtype: "mz-input-color",
                    name: "tertiaryTextColor",
                    value:"#000000",
                    fieldLabel: "Tertiary Text Color:",
					flex: 1
				},
				{
                    xtype: "mz-input-color",
                    name: "tertiaryArrowColor",
                    value:"#e4002b",
                    fieldLabel: "Tertiary Arrow Text Color:",
                    flex: 1,
                    style: {
                        marginLeft: '15px'
                    }
				}
            ]
        },
        {
            xtype: 'container',
            layout: 'vbox',
            items: [
                {
                    xtype: 'mz-input-text',
                    fieldLabel: 'Enter button name:',
                    name: 'buttonName',
                }
            ]
        },
        {
            xtype: 'container',
            layout: 'vbox',
            items: [
                {
                    xtype: 'mz-input-text',
                    fieldLabel: 'Enter redirecting URL:',
                    name: 'buttonUrl',
                }
            ]
        }
    ]

});
