Ext.widget({
    xtype: 'mz-form-widget',
    itemId: 'footerFormWithIcons',
    id: 'footerFormId',
    items: [
        {
            xtype: 'container',
            id: 'mainConatiner', 
            items: [
                {
                    xtype: 'panel',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'mz-input-text',
                            fieldLabel: 'Header',
                            name: 'Header'
                        },
                        {
                            xtype: 'mz-input-text',
                            fieldLabel: 'Custom Class Name',
                            name: 'customClass',
                            itemId: 'customClassId',
                            style: {
                                marginLeft: '10px'
                            }
                        },
                        {
                            xtype: 'mz-input-dropdown',
                            fieldLabel: 'Select Alignment',
                            name: 'Alignment',
                            itemId: 'AlignmentId',
                            flex: 2,
                            store: [
                                    'Vertical',
                                    'Horizontal'
                                    ],
                            style: {
                                marginLeft: '10px'
                            }
                        }        
                    ]
                }
            ]
        }, 
        {
         	xtype: 'form',
         	name: 'headerForm',
         	id: 'headerForm',
         	items: [
                {
                    xtype: 'panel',
                    border: false,
                    layout: 'hbox',
                    name: 'headerPanel',
                    items: [
                        {
                            xtype: 'mz-input-number',
                            fieldLabel: 'Number of fields',
                            id: 'noOfFields',
                            name: 'NoOfFields',
                            value: 1
                        },
                        {
                            xtype: 'button',
                            text: 'Add',
                            name: 'AddBtn',
                            style: {
                                marginTop: '30px',
                                marginLeft: '10px'
                            },
                            listeners: {
                                click: function (e) {
                                    var noOfFields = Ext.getCmp('noOfFields').getValue();
                                    for (var i = 0; i < noOfFields; i++) {
                                        e.up('form').add({
                                            xtype: 'container',
                                            layout: 'hbox',
                                            items: [
                                                {
                                                    xtype: 'mz-input-image',
                                                    fieldLabel: 'Image',
                                                    name: 'Images',
                                                    allowBlank: false
                                                },
                                                {
                                                    xtype: 'mz-input-text',
                                                    fieldLabel: 'Image Header',
                                                    name: 'ImageHeader',
                                                    itemId: 'ImageHeaderId',
                                                    style: {
                                                        marginLeft: '20px'
                                                    },
                                                    flex: 1
                                                },
                                                {
                                                    xtype: 'mz-input-text',
                                                    fieldLabel: 'Link To Open',
                                                    name: 'LinkToOpen',
                                                    itemId: 'LinkToOpenId',
                                                    style: {
                                                        marginLeft: '20px'
                                                    },
                                                    flex: 1
                                                },
                                                {
                                                    xtype: 'mz-input-dropdown',
                                                    fieldLabel: 'Select Target',
                                                    name: 'TargetLink',
                                                    itemId: 'TargetLinkId',
                                                    flex: 1,
                                                    store: [
                                                            '_self',
                                                            '_blank'
                                                            ],
                                                    style: {
                                                        marginLeft: '10px'
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Delete',
                                                    name: 'DeleteBtn',
                                                    style: {
                                                        marginTop: '30px',
                                                        marginLeft: '10px'
                                                    },
                                                    listeners: {
                                                        click: function (e) {
                                                            e.up('container').destroy();
                                                        }
                                                    }
                                                }
                                           ]
                                    	});
                                	}// end for loop
                                }
                           }//end listeners
                    	}
                   ]//panel items
               }
           ]
        }
	],
    getData: function () {
        var footerData = this.getValues();
        if(footerData.ImageHeader){
	        if (Ext.isArray(footerData.ImageHeader)) {
	            Ext.each(footerData.ImageHeader, function (item, index) {
	                footerData.Images[index].imageHeader = item;
	                footerData.Images[index].linkToOpen = footerData.LinkToOpen[index];
	                footerData.Images[index].targetLink = footerData.TargetLink[index];
	                footerData.Images[index].width = footerData.Images[index].width ? footerData.Images[index].width : '30';
	                footerData.Images[index].height = footerData.Images[index].height ? footerData.Images[index].height : '30';
	            });
	        } else {
	            footerData.Images.imageHeader = footerData.ImageHeader;
	            footerData.Images.linkToOpen = footerData.LinkToOpen ? footerData.LinkToOpen : ' ' ;
	            footerData.Images.targetLink = footerData.TargetLink ? footerData.TargetLink : ' ';
	            footerData.Images.width = footerData.Images.width ? footerData.Images.width : '30';
	            footerData.Images.height = footerData.Images.height ? footerData.Images.height : '30';
	            footerData.Images = footerData.Images;
	            var tempData = [];
	        	tempData.push(footerData.Images);
	        	footerData.Images = tempData;
	        }
        }else {
        	var tempData = [];
        	tempData.push(footerData.Images);
        	footerData.Images = tempData;
        }
        return Ext.applyIf(footerData, this.data);
    },
    setData: function (data) {
        this.getForm().setValues(data);

        if (!Ext.Object.isEmpty(data)) {
            for (var i = 0; i < data.Images.length; i++) {
                Ext.getCmp('headerForm').add({
                    xtype: 'container',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'mz-input-image',
                            fieldLabel: 'Image',
                            name: 'Images',
                            value: data.Images[i],
                            allowBlank: false
				        },
                        {
                            xtype: 'mz-input-text',
                            fieldLabel: 'Image Header',
                            name: 'ImageHeader',
                            itemId: 'ImageHeaderId',
                            value: data.Images[i].imageHeader,
                            style: {
                                marginLeft: '20px'
                            },
                            flex: 1
				        },
                        {
                            xtype: 'mz-input-text',
                            fieldLabel: 'Link To Open',
                            name: 'LinkToOpen',
                            itemId: 'LinkToOpenId',
                            value: data.Images[i].linkToOpen,
                            style: {
                                marginLeft: '20px'
                            },
                            flex: 1
				        },
                        {
                            xtype: 'mz-input-dropdown',
                            fieldLabel: 'Select Target',
                            name: 'TargetLink',
                            itemId: 'TargetLinkId',
                            value: data.Images[i].targetLink,
                            flex: 1,
                            store: [
			    			        '_self',
			    			        '_blank'
			    			        ],
                            style: {
                                marginLeft: '10px'
                            }
				        },
                        {
                            xtype: 'button',
                            text: 'Delete',
                            name: 'DeleteBtn',
                            style: {
                                marginTop: '30px',
                                marginLeft: '10px'
                            },
                            listeners: {
                                click: function (e) {
                                    e.up('container').destroy();
                                }
                            }
				        }
			        ]
                });
            }
        }
        this.data = data;
    }
});
 