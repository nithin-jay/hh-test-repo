var p1 = 0; //append this value to productCodes array to make an unique arrays
Ext.widget({
	xtype: 'mz-form-widget',
	itemId: 'imgSliderForm',
	items: [
	        {
			xtype: 'container',
			id: 'mainConatiner',
			items: [
					{
					xtype: 'panel',
					layout: 'hbox',
					items: [
						{
							xtype: 'mz-input-number',
							fieldLabel: 'Time to slide image in milisecond e.g for 5sec add 5000',
							name: 'timeToSlideImage',
							itemId: 'timeToSlideId',
						},
					]
					},
			        {
			    	 xtype: 'panel',
			    	 layout: 'hbox',
			    	 items: [ 
			    	       {
							xtype: 'mz-input-text',
							fieldLabel: 'Custom Class Name',
							name: 'customClass',
							itemId: 'customClassId'
						   },
						   {
		                    	xtype: 'mz-input-number',
		            			fieldLabel: 'Number of fields',
		            			id: 'noOfFields',
		            			name: 'NoOfFields',
		            			value: 1,
		            			style: {
							        marginLeft: '30px'
							    }
		                    },
		                    {
		                    	xtype: 'button',
		        			    text: 'Add',
		        			    name: 'AddBtn',
		        			    style: {
		        			        marginTop: '30px',
		        			        marginLeft: '10px'
		        			    }, 
		        			    listeners: {
		        			    	click: function(e){
		        			    		var noOfFields = Ext.getCmp('noOfFields').getValue();
		        			    		
		        			    		for(var i=0; i<noOfFields; i++){
		        			    			e.up('form').add(
		        			    				{
	            			    					xtype: 'container',
	            			    					style: "background:#ccb;margin:10px; padding:10px",
	        			    						items: [
        			    						        {
        			    						        	xtype: 'panel',
        			    						        	layout: 'hbox',
        			    						        	items: [
    			    						        	        {
    													        	xtype: 'mz-input-image',
    													            fieldLabel: 'Image',
    													            name: 'Images' 
    													        },
    													        	{
    													        	xtype: 'panel',
    	        			    						        	layout: 'vbox',
    	        			    						        	items: [
    	        			    						        	    {
    	        			    						        	    	xtype: 'panel',
    	    	        			    						        	layout: 'hbox',
    	    	        			    						        	items: [
																				  {
																				    name: "bannertitle",
																				    fieldLabel: "Banner Title",
																				    xtype: "mz-input-text",
																				    flex: 1,
																				    style: {
																							marginLeft: '10px'
																						}
																				  },
    																			 {
																	                    xtype: 'mz-input-color',
																	                    name: 'bannertitlecolor',
																	                    fieldLabel: 'Banner Title Color',
																	                    flex: 2,
																	                    style: {
																	                        marginLeft: '10px'
																	                    }
																	              }
																				  
    	    	        			    						        	]
    	        			    						        	    },
    	        			    						        	    {
    	    													        	xtype: 'panel',
    	    	        			    						        	layout: 'hbox',
    	    	        			    						        	items: [
    	    	        			    						        	      {
																					    name: "bannersubtitle",
																					    fieldLabel: "Banner Subtitle",
																					    xtype: "mz-input-text",
																					    flex: 1,
																					    style: {
																								marginLeft: '10px'
																							}
																					  }, 
																					  {
																					        xtype: 'mz-input-color',
																					        name: 'bannersubtitlecolor',
																					        fieldLabel: 'Banner subtitle Color',
																					        flex: 2,
																					        style: {
																					            marginLeft: '10px'
																					        }
																					  }
    		        			    						        	     ]
    	    													        },
    	        			    						        	    {
    	    													        	xtype: 'panel',
    	    	        			    						        	layout: 'hbox',
    	    	        			    						        	items: [
    																			{
    																			    name: "bannerbutton",
    																			    fieldLabel: "Banner Button Text",
    																			    xtype: "mz-input-text",
    																			    flex: 1,
    																			    style: {
    																						marginLeft: '10px'
    																					}
    																			 },
    																			 {
     																			    name: "linktoopen",
     																			    fieldLabel: "Link To Open",
     																			    xtype: "mz-input-text",
     																			    flex: 1,
     																			    style: {
     																						marginLeft: '10px'
     																					}
     																			 }
    		        			    						        	     ]
    	    													        },
    	    													        {
    	    													        	xtype: 'panel',
    	    	        			    						        	layout: 'hbox',
    	    	        			    						        	items: [    	    	        			    						        		
	   																			 {
	   																				 name: "isActive",
	   																				 fieldLabel: "Active/In-active",
	   																				 xtype: "mz-input-checkbox",
	   																				 flex: 1,
	   																				 style: {
	   																					 	marginTop:'20px',
	   																					 	marginLeft: '8px'
	   																				 }
	   																			 },
    																			 {
    																			    name: "bannerposition",
    																			    fieldLabel: "Banner Position",
    																			    xtype: "mz-input-text",
    																			    style: {
    																						marginLeft: '140px'
    																					}
    																			  }
    		        			    						        	     ]
    	    													        },
    	    													        {
    	    													        	xtype: 'panel',
    	    	        			    						        	layout: 'hbox',
    	    	        			    						        	items: [    	    	        			    						        		
	   																			 {
	   																				 name: "promoID",
	   																				 fieldLabel: "Promo ID",
	   																				 xtype: "mz-input-text",
	   																				 flex: 1,
	   																				 style: {
	   																					 	marginLeft: '10px'
	   																				 }
	   																			 },
	   																			 {
	   																				 name: "promoName",
	   																				 fieldLabel: "Promo Name",
	   																				 xtype: "mz-input-text",
	   																				 flex: 1,
	   																				 style: {
	   																					 	marginLeft: '10px'
	   																				 }
	   																			 }
    		        			    						        	     ]
    	    													        },
    	    													        {
    	    													        	xtype: 'panel',
    	    	        			    						        	layout: 'hbox',
    	    	        			    						        	items: [  
    	    	        			    						        		{
	   																				 name: "PromotionDate",
	   																				 fieldLabel: "Promotion Date (start date - end date)",
	   																				 xtype: "mz-input-text",
	   																				 flex: 1,
	   																				 style: {
	   																					 	marginLeft: '10px'
	   																				 }
	   																			 }, 
	   																			 {
	   																				 name: "openInNewTab",
	   																				 fieldLabel: "Open In New Tab",
	   																				 xtype: "mz-input-dropdown",
	   																				 flex: 1,
	   																				 store: [
	   																					'Yes',
	   																				    'No'
	                    			    					    			         ],
	   																				 style: {
	   																					 	marginLeft: '10px'
	   																				 }
	   																			 }
    		        			    						        	     ]
    	    													        }
																	]
    													        }
			    						        	        ]
        			    						        },   
        			    						        {
        			    						        	xtype: 'panel',
        			    						        	layout: 'hbox',
        			    						        	items: [
																{
																    fieldLabel: "Products",
																    name: "productCodes" + p1,
																    xtype: "mz-input-productmulti",
																    flex: 3
																 },
                 			    						        {
                 			    						        	xtype: 'button',
                 			                        			    text: 'Delete',
                 			                        			    name: 'DeleteBtn',
                 			                        			    flex: 1,
                 			                        			    style: {
                 			                        			        marginTop: '30px',
                 			                        			        marginLeft: '10px'
                 			                        			    },
                 			                        			    listeners: {
                 			                        			    	click: function(e){
                 			                        			    		e.up('panel').up('container').destroy();
                 			                        			    	}
                 			                        			    }
                 			    						        } 
			    						        	        ]
        			    						        }
        			    						        
        			    						     ]
		        			    				}
		        			    			);
		        			    			p1++;
		        			    		}
		        			    	} 
		        			    }
		                    }
						]
			        }
		        ]
	        } 
        ],
        getData: function(){
        	var sliderData = this.getValues();
        	if(!Ext.isArray(sliderData.Images)){  //To work with only one image
        		sliderData.Images = [sliderData.Images];
        	}
        	Ext.each(sliderData.Images, function(item, index){
        		if(sliderData.bannertitle) {
        			if(Ext.isArray(sliderData.bannertitle)){
        				item.bannertitle = sliderData.bannertitle[index];
        			}else{
        				item.bannertitle = sliderData.bannertitle;
        			}
        		}
        		if(sliderData.bannertitlecolor) {
        			if(Ext.isArray(sliderData.bannertitlecolor)){
        				item.bannertitlecolor = sliderData.bannertitlecolor[index];
        			}else{
        				item.bannertitlecolor = sliderData.bannertitlecolor;
        			}
        		}
        		if(sliderData.bannersubtitle) {
        			if(Ext.isArray(sliderData.bannersubtitle)){
        				item.bannersubtitle = sliderData.bannersubtitle[index];
        			}else{
        				item.bannersubtitle = sliderData.bannersubtitle;
        			}
        		}
        		if(sliderData.bannersubtitlecolor) {
        			if(Ext.isArray(sliderData.bannersubtitlecolor)){
        				item.bannersubtitlecolor = sliderData.bannersubtitlecolor[index];
        			}else{
        				item.bannersubtitlecolor = sliderData.bannersubtitlecolor;
        			}
        		}
        		if(sliderData.bannerbutton) {
        			if(Ext.isArray(sliderData.bannerbutton)){
        				item.bannerbutton = sliderData.bannerbutton[index];
        			}else{
        				item.bannerbutton = sliderData.bannerbutton;
        			}
        		}
        		if(sliderData.linktoopen) {
        			if(Ext.isArray(sliderData.linktoopen)){
        				item.linktoopen = sliderData.linktoopen[index];
        			}else{
        				item.linktoopen = sliderData.linktoopen;
        			}
        		}
        		if(sliderData.bannerposition) {
        			if(Ext.isArray(sliderData.bannerposition)){
        				item.bannerposition = sliderData.bannerposition[index];
        			}else{
        				item.bannerposition = sliderData.bannerposition;
        			}
        		}
        		if(sliderData.isActive) {
        			if(Ext.isArray(sliderData.isActive)){
        					item.isActive = sliderData.isActive[index];
        			}else{
        				item.isActive = sliderData.isActive;
        			}
        		}
        		if(sliderData.promoID) {
        			if(Ext.isArray(sliderData.promoID)){
        					item.promoID = sliderData.promoID[index];
        			}else{
        				item.promoID = sliderData.promoID;
        			}
        		}
        		if(sliderData.promoName) {
        			if(Ext.isArray(sliderData.promoName)){
        					item.promoName = sliderData.promoName[index];
        			}else{
        				item.promoName = sliderData.promoName;
        			}
        		}
        		if(sliderData.openInNewTab) {
        			if(Ext.isArray(sliderData.openInNewTab)){
        					item.openInNewTab = sliderData.openInNewTab[index];
        			}else{
        				item.openInNewTab = sliderData.openInNewTab;
        			}
        		}
        		if(sliderData.PromotionDate) {
        			if(Ext.isArray(sliderData.PromotionDate)){
        					item.PromotionDate = sliderData.PromotionDate[index];
        			}else{
        				item.PromotionDate = sliderData.PromotionDate;
        			}
        		}
        		item.productCodes = sliderData["productCodes"+index];
        	});
        	delete sliderData.bannertitle;
        	delete sliderData.bannertitlecolor;
        	delete sliderData.bannersubtitle;
        	delete sliderData.bannersubtitlecolor;
        	delete sliderData.bannerbutton;
        	delete sliderData.linktoopen;
        	delete sliderData.bannerposition;
        	delete sliderData.isActive;
        	delete sliderData.promoID;
        	delete sliderData.promoName;
        	delete sliderData.openInNewTab;
        	delete sliderData.PromotionDate
        	return Ext.applyIf(sliderData, this.data);
        },
        setData: function(data){
        	this.getForm().setValues(data);
        	if(!Ext.Object.isEmpty(data)){
        		for(var i = 0; i < data.Images.length; i++){
        			Ext.getCmp('mainConatiner').add(
        					{
		    					xtype: 'container',
		    					style: "background:#ccb;margin:10px; padding:10px",
	    						items: [
    						        {
    						        	xtype: 'panel',
    						        	layout: 'hbox',
    						        	items: [
						        	        {
									        	xtype: 'mz-input-image',
									            fieldLabel: 'Image',
									            name: 'Images',
									            value: data.Images[i]
									        },
									        {
									        	xtype: 'panel',
		    						        	layout: 'vbox',
		    						        	items: [
		    						        	    {
		    						        	    	xtype: 'panel',
    			    						        	layout: 'hbox',
    			    						        	items: [
																{
																    name: "bannertitle",
																    fieldLabel: "Banner Title",
																    xtype: "mz-input-text",
																    value: data.Images[i].bannertitle,
																    flex: 1,
																    style: {
																			marginLeft: '10px'
																		}
																  }, 
																  {
																	    name: "bannertitlecolor",
																	    fieldLabel: "Banner Title Color",
																	    xtype: "mz-input-color",
																	    value: data.Images[i].bannertitlecolor ? data.Images[i].bannertitlecolor : 'rgba(255, 255, 255, 1)',
																	    flex: 1,
																	    style: {
																				marginLeft: '10px'
																		}
																}
															  
    			    						        	]
		    						        	    },
		    						        	    {
											        	xtype: 'panel',
    			    						        	layout: 'hbox',
    			    						        	items: [
															{
															    name: "bannersubtitle",
															    fieldLabel: "Banner Subtitle",
															    xtype: "mz-input-text",
															    value: data.Images[i].bannersubtitle,
															    flex: 1,
															    style: {
																		marginLeft: '10px'
																	}
															  }, 
															  {
																    name: "bannersubtitlecolor",
																    fieldLabel: "Banner Subtitle Color",
																    xtype: "mz-input-color",
																    value: data.Images[i].bannersubtitlecolor ? data.Images[i].bannersubtitlecolor : 'rgba(255, 255, 255, 1)',
																    flex: 1,
																    style: {
																			marginLeft: '10px'
																	}
															} 
			    						        	     ]
											        },
		    						        	    {
											        	xtype: 'panel',
    			    						        	layout: 'hbox',
    			    						        	items: [
															{
															    name: "bannerbutton",
															    fieldLabel: "Banner Button Text",
															    xtype: "mz-input-text",
															    value: data.Images[i].bannerbutton,
															    flex: 1,
															    style: {
																		marginLeft: '10px'
																	}
															 },
															 {
															    name: "linktoopen",
															    fieldLabel: "Link To Open",
															    xtype: "mz-input-text",
															    value: data.Images[i].linktoopen,
															    flex: 1,
															    style: {
																		marginLeft: '10px'
																	}
															 }
			    						        	     ]
											        },
											        {
											        	xtype: 'panel',
    			    						        	layout: 'hbox',
    			    						        	items: [
															 {
																 	name: "isActive",
																    fieldLabel: "Active/In-active",
																    xtype: "mz-input-checkbox",
																    value: data.Images[i].isActive,
																    flex: 1,
																    style: {
																    		marginTop:'30px',
																			marginLeft: '8px'
																		}
															 },
															 {
																    name: "bannerposition",
																    fieldLabel: "Banner Position",
																    xtype: "mz-input-text",
																    value: data.Images[i].bannerposition,
																    style: {
																    		marginLeft: '140px'
																		}
															  }
			    						        	     ]
											        },
											        {
											        	xtype: 'panel',
    			    						        	layout: 'hbox',
    			    						        	items: [    	    	        			    						        		
																 {
																	 name: "promoID",
																	 fieldLabel: "Promo ID",
																	 xtype: "mz-input-text",
																	 value: data.Images[i].promoID,
																	 flex: 1,
																	 style: {
																		 	marginLeft: '10px'
																	 }
																 },
																 {
																	 name: "promoName",
																	 fieldLabel: "Promo Name",
																	 xtype: "mz-input-text",
																	 value: data.Images[i].promoName,
																	 flex: 1,
																	 style: {
																		 	marginLeft: '10px'
																	 }
																 }
			    						        	     ]
											        },
											        {
											        	xtype: 'panel',
    			    						        	layout: 'hbox',
    			    						        	items: [    	
    			    						        			{
																	 name: "PromotionDate",
																	 fieldLabel: "Promotion Date (start date - end date)",
																	 xtype: "mz-input-text",
																	 value: data.Images[i].PromotionDate,
																	 flex: 1,
																	 style: {
																		 	marginLeft: '10px'
																	 }
																 },
																 {
																	 name: "openInNewTab",
																	 fieldLabel: "Open In New Tab",
																	 xtype: "mz-input-dropdown",
																	 value: data.Images[i].openInNewTab,
																	 flex: 1,
																	 store: [
																		 'Yes',
																		 'No'
																	 ],
																	 style: {
																		 	marginLeft: '10px'
																	 }
																 }
			    						        	     ]
											        }
											        
												]
									        }
					        	        ]
    						        },   
    						        {
    						        	xtype: 'panel',
    						        	layout: 'hbox',
    						        	items: [
											{
											    fieldLabel: "Products",
											    name: "productCodes" + i,
											    xtype: "mz-input-productmulti",
											    value: data.Images[i].productCodes,
											    flex: 3
											 },
		    						         {
		    						        	xtype: 'button',
		                        			    text: 'Delete',
		                        			    name: 'DeleteBtn',
		                        			    flex: 1,
		                        			    style: {
		                        			        marginTop: '30px',
		                        			        marginLeft: '10px'
		                        			    },
		                        			    listeners: {
		                        			    	click: function(e){
		                        			    		e.up('panel').up('container').destroy();
		                        			    	}
		                        			    }
		    						        }  
					        	        ]
    						        }
    						     ]
		    				}
        			);
        		}   
        	}
        	this.data = data; 
        }
});