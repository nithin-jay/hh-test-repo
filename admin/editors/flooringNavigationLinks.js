Ext.widget({
    xtype: 'mz-form-widget',
    itemId: 'flooringNavigationLinks',
    items: [
            {
                xtype: 'container',
                id: 'beautitoneContainer',
                items: [
                        {
                            xtype: 'panel',
                            border: false,
                            layout: 'hbox',
                            name: 'headerPanel',
                            items: [
                                {
                                    xtype: 'mz-input-text',
                                    fieldLabel: 'Header',
                                    name: 'Header'
                                },
                                {
                                    xtype: 'mz-input-number',
                                    fieldLabel: 'Number of fields',
                                    id: 'noOfFields',
                                    name: 'NoOfFields',
                                    value: 1,
                                    style: {
                                        marginLeft: '10px'
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: 'Add',
                                    name: 'AddBtn',
                                    style: {
                                        marginTop: '30px',
                                        marginLeft: '10px'
                                    },
                                    listeners: {
                                        click: function(e){
                                            var noOfFields = Ext.getCmp('noOfFields').getValue();
                                            for(var i = 0; i < noOfFields; i++){
                                                e.up('form').add(
                                                        {
                                                            xtype: 'container',
                                                            layout: 'vbox',
                                                            items: [
                                                                {
                                                                    xtype: 'panel',
                                                                    layout: 'hbox',
                                                                    items: [
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Main Menu Text',
                                                                            name: 'mainmenuText',
                                                                            itemId: 'mainmenuText',
                                                                            flex: 2,
                                                                            allowBlank: false
                                                                        },
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Main Menu Link',
                                                                            name: 'mainmenuLink',
                                                                            itemId: 'mainmenuLink',
                                                                            flex: 2,
                                                                            allowBlank: false,
                                                                            style: {
                                                                                marginLeft: '10px'
                                                                            }
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'panel',
                                                                    layout: 'hbox',
                                                                    items: [
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Text1',
                                                                            name: 'submenuText1',
                                                                            itemId: 'submenuText1',
                                                                            flex: 2,
                                                                        },
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Link1',
                                                                            name: 'submenuLink1',
                                                                            itemId: 'submenuLink1',
                                                                            flex: 2,
                                                                            style: {
                                                                                marginLeft: '10px'
                                                                            }
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'panel',
                                                                    layout: 'hbox',
                                                                    items: [
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Text2',
                                                                            name: 'submenuText2',
                                                                            itemId: 'submenuText2',
                                                                            flex: 2,
                                                                        },
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Link2',
                                                                            name: 'submenuLink2',
                                                                            itemId: 'submenuLink2',
                                                                            flex: 2,
                                                                            style: {
                                                                                marginLeft: '10px'
                                                                            }
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'panel',
                                                                    layout: 'hbox',
                                                                    items: [
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Text3',
                                                                            name: 'submenuText3',
                                                                            itemId: 'submenuText3',
                                                                            flex: 2,
                                                                        },
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Link3',
                                                                            name: 'submenuLink3',
                                                                            itemId: 'submenuLink3',
                                                                            flex: 2,
                                                                            style: {
                                                                                marginLeft: '10px'
                                                                            }
                                                                        }
                                                                    ]
                                                                }, 
                                                                {
                                                                    xtype: 'panel',
                                                                    layout: 'hbox',
                                                                    items: [
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Text4',
                                                                            name: 'submenuText4',
                                                                            itemId: 'submenuText4',
                                                                            flex: 2,
                                                                        },
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Link4',
                                                                            name: 'submenuLink4',
                                                                            itemId: 'submenuLink4',
                                                                            flex: 2,
                                                                            style: {
                                                                                marginLeft: '10px'
                                                                            }
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'panel',
                                                                    layout: 'hbox',
                                                                    items: [
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Text5',
                                                                            name: 'submenuText5',
                                                                            itemId: 'submenuText5',
                                                                            flex: 2,
                                                                        },
                                                                        {
                                                                            xtype: 'mz-input-text',
                                                                            fieldLabel: 'Sub Menu Link5',
                                                                            name: 'submenuLink5',
                                                                            itemId: 'submenuLink5',
                                                                            flex: 2,
                                                                            style: {
                                                                                marginLeft: '10px'
                                                                            }
                                                                        },
                                                                        {
                                                                            xtype: 'button',
                                                                            text: 'Delete',
                                                                            name: 'DeleteBtn',
                                                                            flex: 1,
                                                                            style: {
                                                                                marginTop: '30px',
                                                                                marginLeft: '10px',
                                                                                align: 'right'
                                                                            },
                                                                            listeners: {
                                                                                click: function(e){
                                                                                    e.up('panel').up('container').destroy();
                                                                                }
                                                                            }
                                                                        }
                                                                    ]
                                                                }
                                                            ]
                                                        }
                                                );
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    ]
            }
    ],
    getData: function() {
        var beautitoneData = this.getValues();
        if(Ext.isArray(beautitoneData.mainmenuText)){
            var out = beautitoneData.mainmenuText.map(function(item, index){
                return { maintitle: item, mainlink: beautitoneData.mainmenuLink[index], subtitle1: beautitoneData.submenuText1[index],sublink1: beautitoneData.submenuLink1[index], subtitle2: beautitoneData.submenuText2[index], sublink2: beautitoneData.submenuLink2[index], subtitle3: beautitoneData.submenuText3[index], sublink3: beautitoneData.submenuLink3[index], subtitle4: beautitoneData.submenuText4[index], sublink4: beautitoneData.submenuLink4[index], subtitle5: beautitoneData.submenuText5[index], sublink5: beautitoneData.submenuLink5[index] };
            });
            beautitoneData.beautitoneContent = out;
        }else{
            beautitoneData.beautitoneContent = [];
            beautitoneData.beautitoneContent.push({maintitle: beautitoneData.mainmenuText, mainlink: beautitoneData.mainmenuLink, subtitle1: beautitoneData.submenuText1, sublink1: beautitoneData.submenuLink1, subtitle2: beautitoneData.submenuText2, sublink2: beautitoneData.submenuLink2, subtitle3: beautitoneData.submenuText3, sublink3: beautitoneData.submenuLink3, subtitle4: beautitoneData.submenuText4, sublink4: beautitoneData.submenuLink4, subtitle5: beautitoneData.submenuText5, sublink5: beautitoneData.submenuLink5})
        }
        
        delete beautitoneData.mainmenuText;
        delete beautitoneData.mainmenuLink;
        delete beautitoneData.submenuText1;
        delete beautitoneData.submenuLink1;
        delete beautitoneData.submenuText2;
        delete beautitoneData.submenuLink2;
        delete beautitoneData.submenuText3;
        delete beautitoneData.submenuLink3;
        delete beautitoneData.submenuText4;
        delete beautitoneData.submenuLink4;
        delete beautitoneData.submenuText5;
        delete beautitoneData.submenuLink5;
        
        return Ext.applyIf(beautitoneData, this.data);
    },
    setData: function(data) {
        this.getForm().setValues(data);
        if(!Ext.Object.isEmpty(data)){
            for(var i = 0; i < data.beautitoneContent.length; i++){
                Ext.getCmp('beautitoneContainer').add(
                        {
                            xtype: 'container',
                            layout: 'vbox',
                            items: [
                                {
                                    xtype: 'panel',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Main Menu Text',
                                            name: 'mainmenuText',
                                            itemId: 'mainmenuText',
                                            value: data.beautitoneContent[i].maintitle,
                                            flex: 2,
                                            allowBlank: false
                                        },
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Main Menu Link',
                                            name: 'mainmenuLink',
                                            itemId: 'mainmenuLink',
                                            value: data.beautitoneContent[i].mainlink,
                                            style: {
                                                marginLeft: '10px'
                                            },
                                            flex: 2
                                        },
                                    ]
                                },
                                {
                                    xtype: 'panel',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Text1',
                                            name: 'submenuText1',
                                            itemId: 'submenuText1',
                                            value: data.beautitoneContent[i].subtitle1,
                                            flex: 2,
                                        },
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Link1',
                                            name: 'submenuLink1',
                                            itemId: 'submenuLink1',
                                            value: data.beautitoneContent[i].sublink1,
                                            style: {
                                                marginLeft: '10px'
                                            },
                                            flex: 2
                                        }
                                    ]
                                },
                                {
                                    xtype: 'panel',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Text2',
                                            name: 'submenuText2',
                                            itemId: 'submenuText2',
                                            value: data.beautitoneContent[i].subtitle2,
                                            flex: 2,
                                        },
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Link2',
                                            name: 'submenuLink2',
                                            itemId: 'submenuLink2',
                                            value: data.beautitoneContent[i].sublink2,
                                            style: {
                                                marginLeft: '10px'
                                            },
                                            flex: 2
                                        }
                                    ]
                                },
                                {
                                    xtype: 'panel',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Text3',
                                            name: 'submenuText3',
                                            itemId: 'submenuText3',
                                            value: data.beautitoneContent[i].subtitle3,
                                            flex: 2,
                                        },
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Link3',
                                            name: 'submenuLink3',
                                            itemId: 'submenuLink3',
                                            value: data.beautitoneContent[i].sublink3,
                                            style: {
                                                marginLeft: '10px'
                                            },
                                            flex: 2
                                        }
                                    ]
                                },
                                {
                                    xtype: 'panel',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Text4',
                                            name: 'submenuText4',
                                            itemId: 'submenuText4',
                                            value: data.beautitoneContent[i].subtitle4,
                                            flex: 2,
                                        },
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Link4',
                                            name: 'submenuLink4',
                                            itemId: 'submenuLink4',
                                            value: data.beautitoneContent[i].sublink4,
                                            style: {
                                                marginLeft: '10px'
                                            },
                                            flex: 2
                                        }
                                    ]
                                },
                                {
                                    xtype: 'panel',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Text5',
                                            name: 'submenuText5',
                                            itemId: 'submenuText5',
                                            value: data.beautitoneContent[i].subtitle5,
                                            flex: 2,
                                        },
                                        {
                                            xtype: 'mz-input-text',
                                            fieldLabel: 'Sub Menu Link5',
                                            name: 'submenuLink5',
                                            itemId: 'submenuLink5',
                                            value: data.beautitoneContent[i].sublink5,
                                            style: {
                                                marginLeft: '10px'
                                            },
                                            flex: 2
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Delete',
                                            name: 'DeleteBtn',
                                            flex: 1,
                                            style: {
                                                marginTop: '30px',
                                                marginLeft: '10px',
                                                align: 'right'
                                            },
                                            listeners: {
                                                click: function(e){
                                                   e.up('panel').up('container').destroy();
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    );
            }
            
        }
        this.data = data;
    }
});

