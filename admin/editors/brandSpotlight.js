Ext.widget({
	  xtype: 'mz-form-widget',
	  itemId: 'brandSpotlight',
	  items:[
     	{
     		xtype: "container",
     		layout: "hbox",
     		items :[
     			
     			{
			    	xtype: 'panel',
					layout: 'hbox',
					items: [
						{
							xtype: "mz-input-image",
							name: "brandImage",
							id:"brandImage",
							fieldLabel: "Brand Image:",
							flex: 1
						},{
							xtype: "mz-input-image",
							name: "brandLogo",
							id:"brandLogo",
							fieldLabel: "Brand Logo:",
							flex: 1,
							style: {
								marginLeft: '20px'
							}
						}
					]
     			}
     		]
     	},
     	{
     		xtype: "panel",
     		layout: "hbox",
     		items :[
     			{
					xtype: "mz-input-textarea",
					name: "brandDescription",
					id:"brandDescription",
					fieldLabel: "Brand Description:",
					flex: 2
				},{
					xtype: "mz-input-color",
					name: "brandDescFontColor",
					id:"brandDescFontColor",
					fieldLabel: "Description font Color:",
					flex:2,
					style: {
						marginLeft: '20px'
					}
				}
     		]
     	},
     	{
     		xtype: "panel",
     		layout: "hbox",
     		items :[
					{
						xtype: "mz-input-text",
						name: "buttonText",
						id:"buttonText",
						fieldLabel: "Button Text:",
						flex:2
					},{
						xtype: "mz-input-color",
						name: "linkFontColor",
						id:"linkFontColor",
						fieldLabel: "Link Font Color:",
						flex:2,
						style: {
							marginLeft: '20px'
						}
					}
     		]
     	},
     	{
     		xtype: "panel",
     		layout: "hbox",
     		items :[
     			{
					xtype: "mz-input-text",
					name: "buttonLink",
					id:"buttonLink",
					fieldLabel: "Button Link:",
					flex:2,
				},{
					xtype: "mz-input-color",
					name: "bgColor",
					id:"bgColor",
					fieldLabel: "Background Color:",
					flex:2,
					style: {
						marginLeft: '20px'
					}
				}
     		]
     	}
 	]
  
});